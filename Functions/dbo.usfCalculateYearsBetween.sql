SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[usfCalculateYearsBetween] (@inStartDate DATE,@inEndDate DATETIME) RETURNS INT WITH SCHEMABINDING AS
BEGIN
-- SELECT DBO.usfCalculateYearsBetween ('2012-12-31','2013-12-31')
DECLARE @yearend DATETIME,@startdate DATETIME;
DECLARE @leapyear INT, @returnval INT;

--SET @startdate = @inStartDate --'2012-10-29'
--SET @yearend = @inEndDate --'2013-11-01'

--SET @leapyear = CASE WHEN 
--					DATEPART(MM,DATEAdd(dd,1,CAST((CAST(DATEPART(YY,@inDate) AS CHAR(4)) + '0228') AS DATETIME))) = 2
--					THEN 1
--					ELSE 0
--				END

--SELECT @yearend = DateValue  FROM [dbo].ACOMeasureProcsDateConfig WHERE DateMarker = 'EndDate'

SET @returnval =	CASE WHEN datepart(dy,@inStartDate)  <= datepart(dy,@inEndDate) 
				THEN datediff(yy,@inStartDate,@inEndDate)
				WHEN datediff(yy,@inStartDate,dateadd(yy,-1,@inEndDate)) < 0 THEN 0 
				ELSE datediff(yy,@inStartDate,dateadd(yy,-1,@inEndDate))
				END

RETURN @returnval
END



GO
