SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel21]()
RETURNS TABLE
AS RETURN (
	-- MTMP Program Selection # 2
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		rescaled_pharm_cost_pred_risk >= 6 AND
		chronic_condition_count >= 3 AND
		active_ingredient_count >= 8 AND
		(cong_heart_fail_condition != 'NP' OR
		diabetes_condition != 'NP' OR 
		hypertension_condition != 'NP' OR
		persistent_asthma_condition != 'NP')
)
GO
