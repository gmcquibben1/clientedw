SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnGetMeasurePatient](@MeasureAbbr VARCHAR(10))
RETURNS TABLE
AS RETURN (
   SELECT det.MeasureID [MeasureId],
          det.LbPatientID [PatientId]
     FROM dbo.CMeasure_Detail det
     JOIN dbo.CMeasure_Definitions def
       ON det.MeasureID = def.MeasureID 
      AND def.EnableFlag = 'Y' 
    WHERE def.MeasureAbbr = @MeasureAbbr
      AND det.Numerator = 1
)
GO
