SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel23]()
RETURNS TABLE
AS RETURN (
	-- High Risk for Hospitalization
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		probability_ip_hosp > 0.3 AND
		total_cost_band < 8 AND
		cancer_treatment = 0 AND 
		dialysis_service = 0
)
GO
