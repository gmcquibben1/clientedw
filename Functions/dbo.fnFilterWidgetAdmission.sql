SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnFilterWidgetAdmission]
(
    @ERInd         BIT, -- Emergency Room           Indicator (1/0)
    @InpInd        BIT, -- Inpatient                Indicator (1/0)
    @ERInpInd      BIT, -- Inpatient ER             Indicator (1/0)
    @SNFInd        BIT, -- Skilled Nurcing Facility Indicator (1/0)
    @HospiceInd    BIT, -- Hospice                  Indicator (1/0)
    @ChronicInd    BIT, -- Chronic Condition        Indicator (1/0)
    @MinOccurence  INT, -- Occurred at least _x_ time(s)
    @TimeRangeFrom DATETIME, -- Time Interval: Start Date
    @TimeRangeTo   DATETIME, -- Time Interval: End Date
    @TimeInYears   INT,      -- Time Interval In Years
    @TimeInMonths  INT,      -- Time Interval In Months
    @TimeInDays    INT       -- Time Interval In Days
)
RETURNS TABLE
AS RETURN (
WITH CTE_CLAIMS AS (
     SELECT A.LbPatientId AS [PatientId],
            A.CLM_FROM_DT AS [VisitDate],
            DATEDIFF(YEAR, A.CLM_FROM_DT, GETDATE()) -
                CASE WHEN DATEPART(DAYOFYEAR, A.CLM_FROM_DT) > DATEPART(DAYOFYEAR, GETDATE()) THEN 1 ELSE 0
                END AS [TimeInYears],
            DATEDIFF(m, A.CLM_FROM_DT, GETDATE()) AS [TimeInMonths],
            DATEDIFF(d, A.CLM_FROM_DT, GETDATE()) AS [TimeInDays],
            C.ERMainClaimInd AS [ERInd],
            C.INP        AS [InpInd],
            C.ER & C.INP AS [ERInpInd],
            C.SNF        AS [SNFInd],
            C.HOSPICE    AS [HospiceInd],
            C.CHRONIC    AS [ChronicInd]
       FROM CCLF_0_PartA_CLM_TYPES C
       JOIN CCLF_1_PartA_Header A 
         ON C.CUR_CLM_UNIQ_ID = A.CUR_CLM_UNIQ_ID
      WHERE (@ERInd = 1 OR (C.ActiveIndicator = 1 OR C.ActiveMasterClaimID IS NULL))
      AND ISNULL(C.ActiveTotalPaid,1) > (
          SELECT ISNULL(TRY_CONVERT(INT, SetValue), 0)
            FROM vwPortal_SystemSettings
           WHERE SettingType = 'AnalyticsSetting' AND SettingParameter = 'MinPaidSum')
  ),
  CTE_ADMISSIONS AS (
      SELECT cte.[PatientId],
             cte.[VisitDate],
             cte.[ERInd],
             cte.[InpInd],
             cte.[ERInpInd],
             cte.[SNFInd],
             cte.[HospiceInd],
             cte.[ChronicInd]
        FROM CTE_CLAIMS cte
       WHERE (@TimeRangeFrom IS NULL OR VisitDate    >= @TimeRangeFrom)
         AND (@TimeRangeTo   IS NULL OR VisitDate    <= @TimeRangeTo  )
         AND (@TimeInYears   IS NULL OR TimeInYears  <  @TimeInYears  )
         AND (@TimeInMonths  IS NULL OR TimeInMonths <  @TimeInMonths )
         AND (@TimeInDays    IS NULL OR TimeInDays   <  @TimeInDays   )
         AND ((@ERInd        IS NULL AND
               @InpInd       IS NULL AND
               @ERInpInd     IS NULL AND
               @SNFInd       IS NULL AND
               @HospiceInd   IS NULL AND
               @ChronicInd   IS NULL) OR (
              (@ERInd        IS NOT NULL AND [ERInd]      = 1) OR
              (@InpInd       IS NOT NULL AND [InpInd]     = 1) OR
              (@ERInpInd     IS NOT NULL AND [ERInpInd]   = 1) OR
              (@SNFInd       IS NOT NULL AND [SNFInd]     = 1) OR
              (@HospiceInd   IS NOT NULL AND [HospiceInd] = 1) OR
              (@ChronicInd   IS NOT NULL AND [ChronicInd] = 1)))
  ),
  CTE_ADMISSION_OCCURENCE AS
  (
      SELECT cte.[PatientId],
             SUM(CASE WHEN cte.[ERInd]      = 1 THEN 1 ELSE 0 END) AS [ERCount],
             SUM(CASE WHEN cte.[InpInd]     = 1 THEN 1 ELSE 0 END) AS [InpCount],
             SUM(CASE WHEN cte.[ERInpInd]   = 1 THEN 1 ELSE 0 END) AS [ERInpCount],
             SUM(CASE WHEN cte.[SNFInd]     = 1 THEN 1 ELSE 0 END) AS [SNFCount],
             SUM(CASE WHEN cte.[HospiceInd] = 1 THEN 1 ELSE 0 END) AS [HospiceCount],
             SUM(CASE WHEN cte.[ChronicInd] = 1 THEN 1 ELSE 0 END) AS [ChronicCount]
        FROM CTE_ADMISSIONS cte
    GROUP BY cte.[PatientId]
  )
  SELECT cte.[PatientId]
    FROM CTE_ADMISSION_OCCURENCE cte
   WHERE (@MinOccurence IS NULL OR (
         (@ERInd      = 1 AND cte.[ERCount]      >= @MinOccurence) OR
         (@InpInd     = 1 AND cte.[InpCount]     >= @MinOccurence) OR
         (@ERInpInd   = 1 AND cte.[ERInpCount]   >= @MinOccurence) OR
         (@SNFInd     = 1 AND cte.[SNFCount]     >= @MinOccurence) OR
         (@HospiceInd = 1 AND cte.[HospiceCount] >= @MinOccurence) OR
         (@ChronicInd = 1 AND cte.[ChronicCount] >= @MinOccurence)))
)
GO
