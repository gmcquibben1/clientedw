SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnFilterWidgetEvent]
(
/*
	Procedure:		CareGapLoad

	Description:	Inline Table-Valued Function used by the Patient Events Filter Widget
					Returns a list of Patients based on Patient Events

	Version		Date		Author	Change
	-------		----------	------	------
	2.1.1		
	2.2.1					AB		Exclude ER from discharge

*/
    @PatientEventTypes XML,
    @TimeRangeFrom DATETIME,
    @TimeRangeTo   DATETIME,
    @TimeInYears   INT,
    @TimeInMonths  INT,
    @TimeInDays    INT,
    @MinOccurence  INT
)
RETURNS TABLE
AS RETURN (
    WITH CTE_EVENT_TYPES AS (
        SELECT PatientEventTypes.EventType.value('.','INT') [PatientEventTypeId]
          FROM @PatientEventTypes.nodes('/Events/EventType')
            AS PatientEventTypes(EventType)
    ),
    CTE_PATIENT_EVENT AS (
        SELECT pe.LBPatientId [PatientId],
               pe.PatientEventTypeId,
               pe.EventDateTime,
               DATEDIFF(YEAR, pe.EventDateTime, GETDATE()) -
                   CASE WHEN DATEPART(DAYOFYEAR, pe.EventDateTime) > DATEPART(DAYOFYEAR, GETDATE()) THEN 1 ELSE 0
                   END AS [TimeInYears],
               DATEDIFF( m, pe.EventDateTime, GETDATE()) -
                   CASE WHEN DAY(pe.EventDateTime) > DAY(GETDATE()) THEN 1 ELSE 0 
                   END AS [TimeInMonths],
               DATEDIFF(d, pe.EventDateTime, GETDATE()) AS [TimeInDays],
               ISNULL(ad.PatientClass, '') AS [PatientClass]
          FROM dbo.PatientEvent pe
     LEFT JOIN dbo.PatientAdmitDischarge ad
            ON ad.DischargePatientEventId = pe.PatientEventId
         WHERE pe.DeleteInd = 0
    ),
    CTE_PATIENT_EVENT_TYPE AS (
        SELECT pev.PatientId,
               pev.PatientEventTypeId,
               COUNT(*) [EventTypeCount]
          FROM CTE_PATIENT_EVENT pev
          JOIN dbo.PatientEventType pet
            ON pev.PatientEventTypeId = pet.PatientEventTypeId
           AND pet.DeleteInd = 0
         WHERE pet.PatientEventTypeId IN (SELECT PatientEventTypeId FROM CTE_EVENT_TYPES)
           AND (pev.PatientClass <> 'E') -- Exclude "E" (Emergency) HL7 Patient class Discharge records
           AND (@TimeRangeFrom IS NULL OR EventDateTime >= @TimeRangeFrom)
           AND (@TimeRangeTo   IS NULL OR EventDateTime <= @TimeRangeTo)
           AND (@TimeInYears   IS NULL OR TimeInYears   <  @TimeInYears)
           AND (@TimeInMonths  IS NULL OR TimeInMonths  <  @TimeInMonths)
           AND (@TimeInDays    IS NULL OR TimeInDays    <  @TimeInDays)
      GROUP BY pev.PatientId, pev.PatientEventTypeId
    ),
    CTE_EVENT_OCCURENCE AS
    (
        SELECT PatientId,
               MIN(EventTypeCount) [MinOccurence]
          FROM CTE_PATIENT_EVENT_TYPE
      GROUP BY PatientId
    )
    SELECT PatientId
      FROM CTE_EVENT_OCCURENCE
     WHERE (@MinOccurence IS NULL OR MinOccurence >= @MinOccurence)
)
GO
