SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel17]()
RETURNS TABLE
AS RETURN (
	-- Poly-pharmacy with Rx Gaps and No Visits
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		active_ingredient_count >= 14 AND
		unique_provider_count = 0 AND 
		total_rx_gaps >= 5
)
GO
