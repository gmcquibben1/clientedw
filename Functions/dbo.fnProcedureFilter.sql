SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnProcedureFilter] (
    @ProcedureCodes [dbo].[ProcedureCodes] READONLY, -- Procedure Code(s)
    @ValueSetName   VARCHAR(256), -- Value Set Name
    @DateFirst      DATETIME,     -- Start Date
    @DateLast       DATETIME,     -- End Date    
    @DateInterval   INT,          -- Date Interval 
    @IntervalType   CHAR(1)       -- Date Interval Type: 'm/d/y'
)
RETURNS @ResultTable TABLE( PatientId int )
AS BEGIN
   DECLARE @ProcedureSearch BIT;
    SELECT @ProcedureSearch = 1 FROM @ProcedureCodes;

   DECLARE @SearchProcedures BIT; -- Search Procedures Only
    SELECT @SearchProcedures = CASE 
      WHEN @ProcedureSearch IS NOT NULL AND @ValueSetName IS NULL THEN 1 ELSE 0 END;

   DECLARE @SearchValueSets BIT; -- Search Value Sets Only
    SELECT @SearchValueSets = CASE
      WHEN @ProcedureSearch IS NULL AND @ValueSetName IS NOT NULL THEN 1 ELSE 0 END;

   DECLARE @SearchBoth BIT; -- Search Both Procedures And Value Sets
    SELECT @SearchBoth = CASE
      WHEN @ProcedureSearch IS NOT NULL AND @ValueSetName IS NOT NULL THEN 1 ELSE 0 END;

   INSERT INTO @ResultTable(PatientId)
   SELECT DISTINCT pp.PatientId
     FROM PatientProcedure pp 
    WHERE pp.DeleteInd = 0 AND
          pp.PatientProcedureId IN
          (
             SELECT ppc.PatientProcedureId 
               FROM PatientProcedureProcedureCode ppc
               JOIN ProcedureCode pc
                 ON pc.ProcedureCodeId = ppc.ProcedureCodeId 
              WHERE pc.DeleteInd = 0
                -- Search Procedures Only
                AND (@SearchProcedures <> 1 OR pc.ProcedureCode IN (SELECT Code FROM @ProcedureCodes))
                -- Search Value Sets Only
                AND (@SearchValueSets <> 1 OR pc.ProcedureCode IN (SELECT Code FROM dbo.fnGetProcedureCodes(@ValueSetName))) 
                -- Search Both Procedures And Value Sets
                AND (@SearchBoth <> 1 OR pc.ProcedureCode IN (
                    SELECT Code FROM dbo.fnGetProcedureCodes(@ValueSetName)
                    UNION SELECT Code FROM @ProcedureCodes))
          )
        -- Search by Date Interval
      AND (ISNULL(@DateInterval, 0) = 0 OR (
                 (@IntervalType = 'y' AND DATEDIFF(yy, pp.ProcedureDateTime, GETDATE()) < @DateInterval) OR
                 (@IntervalType = 'm' AND DATEDIFF( m, pp.ProcedureDateTime, GETDATE()) < @DateInterval) OR
                 (@IntervalType = 'd' AND DATEDIFF( d, pp.ProcedureDateTime, GETDATE()) < @DateInterval)))
         -- Search by Date Range
      AND (@DateFirst IS NULL OR pp.ProcedureDateTime >= @DateFirst)
      AND (@DateLast  IS NULL OR pp.ProcedureDateTime <= @DateLast);
   RETURN; -- Return results
END;
GO
