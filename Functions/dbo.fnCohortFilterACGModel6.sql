SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel6]()
RETURNS TABLE
AS RETURN (
	-- Poorly Controlled Asthma
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		persistent_asthma_condition = 'TRT' AND 
		persistent_asthma_mpr IS NULL AND
		persistent_asthma_csa IS NULL AND 
		edc_codes  LIKE '%ALL05%' AND
		emergency_visit_count >= 2 AND
		edc_codes NOT LIKE '%REN06%' AND
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%' AND
		Medc_codes NOT LIKE '%MAL%' 
)
GO
