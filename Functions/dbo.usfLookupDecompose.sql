SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[usfLookupDecompose] 
(@stringin Varchar(MAX),@retSeg INT,@fullStr CHAR(1)='Y') RETURNS VARCHAR (MAX)
AS BEGIN
--DECLARE @stringin VARCHAR(100) = 'Max(dfdfdfdf Lookup(a,b,c)*dsafdfadf)', @retSeg INT = 3
DECLARE @retString VARCHAR(4000), @pos1 INT, @pos2 INT,  @mc  CHAR(1) 
DECLARE @matchedbrackets TABLE (Id INT IDENTITY(1,1),pos1 INT, pos2 INT,lookupstart char(1))
DECLARE @Numbers TABLE (Number INT)
;WITH
L0   AS(SELECT 1 AS c UNION ALL SELECT 1 UNION ALL SELECT 1), 
L1   AS(SELECT 1 AS c FROM L0 AS A, L0 AS B, L0 AS C), 
L2   AS(SELECT 1 AS c FROM L1 AS A, L1 AS B, L1 AS C), 
Nums AS(SELECT ROW_NUMBER() OVER(ORDER BY c) AS n FROM L2)
INSERT INTO @Numbers(Number)
SELECT n AS Number  FROM Nums ;

DECLARE MyCur CURSOR FAST_FORWARD FOR 
SELECT SUBSTRING(@stringin,N.Number,1), n.Number FROM @Numbers  N WHERE SUBSTRING(@stringin,N.Number,1) IN ('(',')') ORDER BY n.Number 
   
OPEN MyCur
FETCH NEXT FROM MyCur INTO @mc, @pos1 

WHILE @@FETCH_STATUS = 0
BEGIN
	IF @mc = '(' 
	BEGIN 
		INSERT INTO @matchedbrackets(pos1,lookupstart) VALUES (@pos1,CASE WHEN SUBSTRING(@stringin,@pos1-6,6) = 'LOOKUP' THEN 'Y' ELSE 'N' END ) 
	END 
	IF @mc = ')' 
	BEGIN 
		UPDATE m1 SET pos2 = @pos1 FROM @matchedbrackets m1 INNER JOIN (SELECT MAX(ID) ID FROM @matchedbrackets WHERE pos2 is NULL ) M2 ON M1.Id = M2.Id 
	END 
FETCH NEXT FROM MyCur INTO @mc, @pos1 
END
CLOSE MyCur 
DEALLOCATE MyCur 

SELECT @retString =  SUBSTRING(@stringin,pos1 + 1,pos2-pos1-1), @pos1 = pos1 , @pos2 = pos2  FROM @matchedbrackets WHERE Lookupstart = 'Y'
SELECT @retString = REPLACE(@retString,'~,','~')
SELECT @retString  = CASE WHEN @fullStr = 'Y' THEN SUBSTRING(@stringin,1,@pos1-7) + retString + SUBSTRING(@stringin,@pos2 + 1, 10000)  
						  ELSE retString END FROM (
SELECT SUBSTRING(@retString,N.Number, charindex(',', @retString + ',', Number) - Number) retString, ROW_NUMBER() OVER(ORDER BY Number) O FROM @Numbers  N 
WHERE  SUBSTRING(',' +  @retString,N.Number,1) = ',' 
) X WHERE O = @retSeg
SELECT @retString = REPLACE(@retString,'~',',')

RETURN ISNULL(@retString,@stringin)

END 

GO
