SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel20]()
RETURNS TABLE
AS RETURN (
	-- MTMP Program Selection # 1
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		pharmacy_cost >= 3017 AND
		chronic_condition_count >= 3 AND
		active_ingredient_count >= 8
)
GO
