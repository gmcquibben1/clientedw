SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnInterpolateVaccineTaxonomy]
(
    @ImmunizationCode varchar(10), 
	@ImmunizationCodeSystemName varchar(50)
)
RETURNS varchar(50)

/*===============================================================
	CREATED BY: 	CJL
	CREATED ON:		2017-02-2014
	INITIAL VER:	2.2.1
	MODULE:			ETL,  Patient Immunization Import
	DESCRIPTION:	This fucntion will attempt to interpolate the encoding taxonomy of the immunization code based on the format
	PARAMETERS:		
					@ImmunizationCode Immunization Code to test

	RETURN VALUE(s)/OUTPUT:	Name of the a encoding system type
	
 
	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.1.1		2017-02-04		CJL			LBETL-634	Initial version
	2.1.1       2017-03-01      CJL         LBETL-634    Corrected issue with Loinc Interpolation

=================================================================*/

AS
BEGIN

DECLARE @tmpImmunizationCodeSystemName VARCHAR(50) = LTRIM(RTRIM(ISNULL(@ImmunizationCodeSystemName, '')))
IF( LEN( @ImmunizationCodeSystemName ) > 0 )
BEGIN
	RETURN @tmpImmunizationCodeSystemName
END



DECLARE @tmpImmunizationCode varchar(10) = LTRIM(RTRIM(ISNULL(@ImmunizationCode, '')))
DECLARE @l INT = (SELECT LEN( @tmpImmunizationCode ))

IF @l = 0
BEGIN
	RETURN ''
END

IF @l <= 3
BEGIN
	RETURN 'CVX'
END

IF @l = 5
BEGIN
	RETURN 'CPT'
END


IF @l > 5 AND CHARINDEX( '-',@tmpImmunizationCode) = 0 
BEGIN
	RETURN 'SNOMED CT'
END



return  'LOINC'

END
GO
