SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnGetPatientExternalId](@PreferredIds XML)
RETURNS TABLE
AS RETURN (
   WITH CTE_PREFERRED_IDS AS (
        SELECT [Index] = T.Item.value('@Index', 'int'),
               [Name] = T.Item.value('@Name', 'varchar(100)')
          FROM @PreferredIds.nodes('/Types/Type') AS T(Item)
   ),
   CTE_PATIENT_REF AS (
        SELECT [Index],
               [LbPatientId],
               [ExternalId],
               ROW_NUMBER() OVER (
                   PARTITION BY [LbPatientId]
                   ORDER BY [Index]
               ) AS [RowNumber]
          FROM [dbo].[PatientIdReference]
          JOIN CTE_PREFERRED_IDS 
            ON [Name] = [IdTypeDesc]
         WHERE [DeleteInd] = 0
           AND [ExternalId] IS NOT NULL
    )
    SELECT [LbPatientId] AS [PatientId],
           [ExternalId]
      FROM CTE_PATIENT_REF
     WHERE [RowNumber] = 1
   )
GO
