SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnFilterWidgetACGDiagnosis]
(
    @EdcCodes XML
)
RETURNS TABLE
AS
RETURN
(
    WITH CTE_EDC_CODES AS (
        SELECT EdcCodes.Code.value('.','VARCHAR(10)') [EdcCode]
          FROM @EdcCodes.nodes('/Codes/EdcCode')
            AS EdcCodes(Code)
    )
    SELECT DISTINCT
           [LbpatientId] AS [PatientId]
      FROM [dbo].[JhacgPatientEdcCode]
     WHERE [EdcCode] IN
         (
            SELECT [EdcCode] FROM CTE_EDC_CODES
         )
)
GO
