SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel14]()
RETURNS TABLE
AS RETURN (
	-- Psychosocial Unstable
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		adg_codes LIKE '%25%' AND 
		(edc_codes LIKE '%PSY07%' OR
		edc_codes LIKE '%PSY12%' OR
		edc_codes LIKE '%PSY20%') AND 
		adg_codes NOT LIKE '%03%' AND
		adg_codes NOT LIKE '%04%' AND
		adg_codes NOT LIKE '%09%' AND
		adg_codes NOT LIKE '%11%' AND
		adg_codes NOT LIKE '%16%' AND
		adg_codes NOT LIKE '%22%' AND
		adg_codes NOT LIKE '%32%'
)
GO
