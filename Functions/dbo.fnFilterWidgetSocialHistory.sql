SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnFilterWidgetSocialHistory]
(
    @SmokingStatusCodes     XML,
    @MaritalStatusCodes     XML,
    @EmploymentStatusCodes  XML,
    @LivingArrangementCodes XML,
    @TransportationInd      BIT,
    @AlcoholUseInd          BIT,
    @DrugUseInd             BIT,
    @CaffeineUseInd         BIT,
    @SexuallyActiveInd      BIT,
    @ExerciseHoursPerWeek   INT
)
RETURNS TABLE
AS
RETURN
(
    WITH CTE_SMOKING_STATUS_CODES AS (
        SELECT SmokingStatus.Code.value('.','VARCHAR(20)') [SmokingStatusCode]
          FROM @SmokingStatusCodes.nodes('/SmokingStatus/Code')
            AS SmokingStatus(Code)
    ),
    CTE_MARITAL_STATUS_CODES AS (
        SELECT MaritalStatus.Code.value('.','VARCHAR(20)') [MaritalStatusCode]
          FROM @MaritalStatusCodes.nodes('/MaritalStatus/Code')
            AS MaritalStatus(Code)
    ),
    CTE_EMPLOYMENT_STATUS_CODES AS (
        SELECT EmploymentStatus.Code.value('.','VARCHAR(20)') [EmploymentStatusCode]
          FROM @EmploymentStatusCodes.nodes('/EmploymentStatus/Code')
            AS EmploymentStatus(Code)
    ),
    CTE_LIVING_ARRANGEMENT_CODES AS (
        SELECT LivingArrangement.Code.value('.','VARCHAR(20)') [LivingArrangementCode]
          FROM @LivingArrangementCodes.nodes('/LivingArrangement/Code')
            AS LivingArrangement(Code)
    )
    SELECT PatientID [PatientId]
      FROM [dbo].[PatientSocialHistory]
     WHERE (@SmokingStatusCodes IS NULL OR SmokingStatus IN (SELECT SmokingStatusCode FROM CTE_SMOKING_STATUS_CODES))
       AND (@MaritalStatusCodes IS NULL OR MaritalStatus IN (SELECT MaritalStatusCode FROM CTE_MARITAL_STATUS_CODES))
       AND (@EmploymentStatusCodes IS NULL OR EmploymentStatus IN (SELECT EmploymentStatusCode FROM CTE_EMPLOYMENT_STATUS_CODES))
       AND (@LivingArrangementCodes IS NULL OR LivingArrangementCode IN (SELECT LivingArrangementCode FROM CTE_LIVING_ARRANGEMENT_CODES))
       AND (@TransportationInd IS NULL OR TransportationInd = @TransportationInd)
       AND (@AlcoholUseInd IS NULL OR AlcoholUseInd = @AlcoholUseInd)
       AND (@CaffeineUseInd IS NULL OR CaffeineUseInd = @CaffeineUseInd)
       AND (@SexuallyActiveInd IS NULL OR SexuallyActiveInd = @SexuallyActiveInd)
       AND (@ExerciseHoursPerWeek IS NULL OR ExerciseHoursPerWeek <= @ExerciseHoursPerWeek)
)
GO
