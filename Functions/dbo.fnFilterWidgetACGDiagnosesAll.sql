SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnFilterWidgetACGDiagnosesAll]
(
    @EdcCodes XML
)
RETURNS TABLE
AS
RETURN
(
    WITH CTE_EDC_CODES AS (
        SELECT EdcCodes.Code.value('.','VARCHAR(10)') [EdcCode]
          FROM @EdcCodes.nodes('/Codes/EdcCode')
            AS EdcCodes(Code)
    )
    SELECT [LbpatientId] AS [PatientId]
      FROM [dbo].[JhacgPatientEdcCode] edc
     WHERE [EdcCode] IN
         (
            SELECT [EdcCode] FROM CTE_EDC_CODES
         )
    GROUP BY [LbpatientId]
    -- All Selected Diagnoses
    HAVING COUNT(DISTINCT edc.[EdcCode]) = (
    SELECT COUNT(DISTINCT [EdcCode]) FROM CTE_EDC_CODES)
)
GO
