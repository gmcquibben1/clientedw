SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel25]()
RETURNS TABLE
AS RETURN (
	-- Using the ER for Primary Care
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		emergency_visit_count > 2 AND
		unique_provider_count = 0 AND 
		inpatient_hosp_count = 0 AND 
		chronic_condition_count < 4 AND 
		edc_codes NOT LIKE '%EYE10%' AND
		edc_codes NOT LIKE '%EYE12%' AND
		edc_codes NOT LIKE '%FRE04%' AND
		edc_codes NOT LIKE '%GSU12%' AND
		edc_codes NOT LIKE '%REC02%' AND
		edc_codes NOT LIKE '%TOX02%' AND
		edc_codes NOT LIKE '%MUS04%'
)
GO
