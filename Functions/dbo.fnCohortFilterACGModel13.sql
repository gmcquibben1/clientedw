SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel13]()
RETURNS TABLE
AS RETURN (
	-- High Risk Pregnancy Using Provider Count
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		pregnancy_without_delivery = 1 AND
		unique_provider_count = 0
)
GO
