SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel12]()
RETURNS TABLE
AS RETURN (
	-- High Risk Pregnancy using EDC Codes
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		pregnancy_without_delivery = 1 AND
		(edc_codes LIKE '%PSY02%' OR
		edc_codes LIKE '%PSY03%' OR
		edc_codes LIKE '%PSY06%' OR
		edc_codes LIKE '%INF04%' OR
		edc_codes LIKE '%CAR15%')
)
GO
