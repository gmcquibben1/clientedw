SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel18]()
RETURNS TABLE
AS RETURN (
	-- High Risk Unexpected Pharmacy Cost
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		high_rsk_unexpected_pharm_cost = 1 AND 
		rxmg_codes LIKE '%GSIx040%' AND
		edc_codes LIKE '%PSY02%'
)
GO
