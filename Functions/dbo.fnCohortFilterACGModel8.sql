SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel8]()
RETURNS TABLE
AS RETURN (
	-- Patients with High Medical Needs
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		frailty_flag = 1 AND 
		hosdom_morbidity_types >= 1 AND
		chronic_condition_count >= 5 AND 
		total_cost_band < 8 AND 
		dialysis_service = 0 and 
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%' AND
		Medc_codes NOT LIKE '%MAL%' 
)
GO
