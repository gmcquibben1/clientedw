SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnFilterWidgetProcedure] (
    @ProcedureCodes XML,          -- Procedure Codes
    @ValueSetName   VARCHAR(256), -- Value Set Name
    @StartDateTime  DATETIME,     -- Start Date
    @EndDateTime    DATETIME,     -- End Date
    @DateInterval   INT,          -- Date Interval
    @IntervalType   CHAR(1)       -- Date Interval Type: 'm/d/y'
)
RETURNS TABLE
AS RETURN (
   WITH CTE_CODES AS (
        SELECT ProcedureCodes.Code.value('.','VARCHAR(20)') [Code]
          FROM @ProcedureCodes.nodes('/Codes/Code')
            AS ProcedureCodes(Code)
   ),
   CTE_PATIENT_PROC AS (
        SELECT ppr.PatientId         AS [PatientId],
               ppr.ProcedureDateTime AS [ProcedureDate],
               prc.ProcedureCode     AS [ProcedureCode],
               DATEDIFF(YEAR, ProcedureDateTime, GETDATE()) -
                   CASE WHEN DATEPART(DAYOFYEAR, ProcedureDateTime) > DATEPART(DAYOFYEAR, GETDATE()) THEN 1 ELSE 0
                   END AS [TimeInYears],
               DATEDIFF(m, ProcedureDateTime, GETDATE()) -
                   CASE WHEN DAY(ProcedureDateTime) > DAY(GETDATE()) THEN 1 ELSE 0
                   END AS [TimeInMonths],
               DATEDIFF(d, ProcedureDateTime, GETDATE()) AS [TimeInDays]
          FROM dbo.PatientProcedure ppr
          JOIN dbo.PatientProcedureProcedureCode ppc
            ON ppr.PatientProcedureId = ppc.PatientProcedureId
           AND ppr.DeleteInd = 0
          JOIN dbo.ProcedureCode prc
            ON prc.ProcedureCodeId = ppc.ProcedureCodeId 
           AND prc.DeleteInd = 0
   )
   SELECT DISTINCT PatientId
     FROM CTE_PATIENT_PROC
    WHERE ProcedureCode IN
        (-- Search by Procedure Codes (required)
            SELECT Code FROM CTE_CODES
            UNION
            SELECT Code FROM dbo.fnGetProcedureCodes(@ValueSetName)
        )
        -- Search by Date Interval (optional)
      AND (ISNULL(@DateInterval, 0) = 0 OR (
                 (@IntervalType = 'y' AND [TimeInYears]  < @DateInterval) OR
                 (@IntervalType = 'm' AND [TimeInMonths] < @DateInterval) OR
                 (@IntervalType = 'd' AND [TimeInDays]   < @DateInterval)))
         -- Search by Date Range (optional)
      AND (@StartDateTime IS NULL OR [ProcedureDate] >= @StartDateTime)
      AND (@EndDateTime   IS NULL OR [ProcedureDate] <= @EndDateTime)
)
GO
