SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel7]()
RETURNS TABLE
AS RETURN (
	-- Untreated Schizophrenia
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		schizophrenia_untreated_rx = 'Y' AND
		emergency_visit_count > 1 AND
		edc_codes NOT LIKE '%REN06%' AND
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%' AND
		Medc_codes NOT LIKE '%MAL%' 
)
GO
