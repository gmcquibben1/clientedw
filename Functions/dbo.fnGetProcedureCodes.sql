SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnGetProcedureCodes] (@ValueSetName VARCHAR(256))
RETURNS TABLE
AS RETURN (
   SELECT [Code]
     FROM [dbo].[HEDISValueSetCodes] 
    WHERE [Value Set Name] = @ValueSetName
      AND [Code System] IN (SELECT Name FROM ProcedureCodingSystemType))
GO
