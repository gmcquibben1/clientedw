SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel9]()
RETURNS TABLE
AS RETURN (
	-- Patients with Emerging Risk
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		rank_prob_high_tot_cost > 0.4 AND
		total_cost_band < 7 AND
		edc_codes NOT LIKE '%REN06%' AND
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%' AND
		Medc_codes NOT LIKE '%MAL%' 
)
GO
