SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel5]()
RETURNS TABLE
AS RETURN (
	-- COPD by Age and Treatment
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		age >= 40 AND
		persistent_asthma_condition = 'TRT' AND 
		edc_codes NOT LIKE '%RES04%' AND
		edc_codes NOT LIKE '%ALL04%' AND
		edc_codes NOT LIKE '%ALL05%' AND
		edc_codes NOT LIKE '%REN06%' AND
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%' AND
		Medc_codes NOT LIKE '%MAL%'
)
GO
