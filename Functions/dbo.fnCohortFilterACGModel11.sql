SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel11]()
RETURNS TABLE
AS RETURN (
	-- High Risk Pregnancy using ACGs
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		pregnancy_without_delivery = 1 AND
		(acg_code LIKE '1762' OR
		acg_code LIKE '1772')
)
GO
