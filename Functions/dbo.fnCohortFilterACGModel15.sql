SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel15]()
RETURNS TABLE
AS RETURN (
	-- Psychosocial Unstable with Psychotherapy
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		adg_codes LIKE '%25%' AND 
		psychotherapy_service = 1 AND
		hosdom_morbidity_types >= 1 AND 
		edc_codes NOT LIKE '%REN06%' AND
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%' AND
		Medc_codes NOT LIKE '%MAL%' 
)
GO
