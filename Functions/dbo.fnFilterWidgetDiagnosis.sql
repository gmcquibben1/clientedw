SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnFilterWidgetDiagnosis] (
    @DiagnosisCodes        XML,          -- Diagnosis Codes
    @DiagnosisStatusTypeId INT,          -- Diagnosis Status
    @ValueSetName          VARCHAR(256), -- Value Set Name
    @StartDateTime         DATETIME,     -- Start Date
    @EndDateTime           DATETIME,     -- End Date
    @DateInterval          INT,          -- Date Interval
    @IntervalType          CHAR(1)       -- Date Interval Type: 'm/d/y'
)
RETURNS TABLE
AS RETURN (
   WITH CTE_CODES AS (
        SELECT DiagnosisCodes.Code.value('.','VARCHAR(20)') [Code]
          FROM @DiagnosisCodes.nodes('/Codes/Code')
            AS DiagnosisCodes(Code)
   ),
   CTE_PATIENT_DX AS (
        SELECT pdx.PatientId AS [PatientId],
               dxc.DiagnosisCode AS [DiagnosisCode],
               pdx.DiagnosisOnsetDate AS [OnsetDate],
               DATEDIFF(YEAR, DiagnosisOnsetDate, GETDATE()) -
                   CASE WHEN DATEPART(DAYOFYEAR, DiagnosisOnsetDate) > DATEPART(DAYOFYEAR, GETDATE()) THEN 1 ELSE 0
                   END AS [TimeInYears],
               DATEDIFF(m, DiagnosisOnsetDate, GETDATE()) -
                   CASE WHEN DAY(DiagnosisOnsetDate) > DAY(GETDATE()) THEN 1 ELSE 0
                   END AS [TimeInMonths],
               DATEDIFF(d, DiagnosisOnsetDate, GETDATE()) AS [TimeInDays],
               pdx.DiagnosisStatusTypeID AS [DiagnosisStatusTypeId]
          FROM dbo.PatientDiagnosis pdx
          JOIN dbo.PatientDiagnosisDiagnosisCode pdc
            ON pdc.PatientDiagnosisId = pdx.PatientDiagnosisId
           AND pdx.DeleteInd = 0
          JOIN dbo.DiagnosisCode dxc
            ON pdc.DiagnosisCodeId = dxc.DiagnosisCodeId 
           AND dxc.DeleteInd = 0
   )
   SELECT DISTINCT PatientId
     FROM CTE_PATIENT_DX
    WHERE DiagnosisCode IN
        (-- Search by Diagnosis Codes (required)
            SELECT [Code] FROM CTE_CODES
            UNION
            SELECT [Code]
              FROM [dbo].[HEDISValueSetCodes]
             WHERE [Value Set Name] = @ValueSetName
               AND [DeleteInd] = 0
               AND [Code System] IN
                 (
                    SELECT [Name] FROM [dbo].[DiagnosisCodingSystemType] WHERE [DeleteInd] = 0
                 )
        )
        -- Search by Status
      AND (@DiagnosisStatusTypeId IS NULL OR DiagnosisStatusTypeID = @DiagnosisStatusTypeId)
        -- Search by Date Interval
      AND (ISNULL(@DateInterval, 0) = 0 OR (
                 (@IntervalType = 'y' AND [TimeInYears]  < @DateInterval) OR
                 (@IntervalType = 'm' AND [TimeInMonths] < @DateInterval) OR
                 (@IntervalType = 'd' AND [TimeInDays]   < @DateInterval)))
         -- Search by Date Range
      AND (@StartDateTime IS NULL OR [OnsetDate] >= @StartDateTime)
      AND (@EndDateTime   IS NULL OR [OnsetDate] <= @EndDateTime)
)
GO
