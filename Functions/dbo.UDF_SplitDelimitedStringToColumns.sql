SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[UDF_SplitDelimitedStringToColumns]
			  (@Data NVARCHAR(MAX), @Delimiter NVARCHAR(3))
		RETURNS @Table TABLE 
			( Data NVARCHAR(50)
			)
		AS
		BEGIN

			DECLARE @TextXml XML;
			SELECT @TextXml = CAST('<d>' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Data, '&', '&amp;'), '<', '&lt;'), '>', '&gt;'), '"', '&quot;'), '''', '&apos;'), @Delimiter, '</d><d>') + '</d>' AS XML);

			INSERT INTO @Table (Data)
			SELECT Data = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(T.split.value('.', 'nvarchar(max)'))), '&amp;', '&'), '&lt;', '<'), '&gt;', '>'), '&quot;', '"'), '&apos;', '''')
			FROM @TextXml.nodes('/d') T(Split)

			RETURN
		END
		
GO
