SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnFilterWidgetAcgHighRisk] (
	@RubNoneInd                   bit,
	@RubHealthyInd                bit,
	@RubLowInd                    bit,
	@RubModerateInd               bit,
	@RubHighInd                   bit,
	@RubVeryHighInd               bit,
	@CoordinationIssueLikelyInd   bit,
	@CoordinationIssuePossibleInd bit,
	@CoordinationIssueUnlikelyInd bit,
	@ChronicConditionCountFrom    int,
	@ChronicConditionCountTo      int,
	@ActiveIngredientCountFrom    int,
	@ActiveIngredientCountTo      int,
	@ProbabilityHighTotalCostFrom decimal(3,2),
	@ProbabilityHighTotalCostTo   decimal(3,2),
	@FrailtyInd                   bit
)
RETURNS TABLE
AS RETURN (
   SELECT [patient_id] [PatientId]
     FROM [dbo].[JhacgPatientDetail]
    WHERE ((@RubVeryHighInd IS NULL AND
            @RubHighInd     IS NULL AND
            @RubModerateInd IS NULL AND
            @RubLowInd      IS NULL AND
            @RubHealthyInd  IS NULL AND
            @RubNoneInd     IS NULL) OR (
           (@RubVeryHighInd = 1 AND resource_utilization_band = 5) OR
           (@RubHighInd     = 1 AND resource_utilization_band = 4) OR
           (@RubModerateInd = 1 AND resource_utilization_band = 3) OR
           (@RubLowInd      = 1 AND resource_utilization_band = 2) OR
           (@RubHealthyInd  = 1 AND resource_utilization_band = 1) OR
           (@RubNoneInd     = 1 AND resource_utilization_band = 0)))
        -- Coordination Risk
       AND (@CoordinationIssueLikelyInd   IS NULL OR (coordination_risk = 'LCI')) -- LCI - Likely   Coordination Issue
       AND (@CoordinationIssuePossibleInd IS NULL OR (coordination_risk = 'PCI')) -- PCI - Possible Coordination Issue
       AND (@CoordinationIssueUnlikelyInd IS NULL OR (coordination_risk = 'UCI')) -- UCI - Unlikely Coordination Issue
        -- Chronic Condition Count (Range)
       AND (@ChronicConditionCountFrom IS NULL OR (chronic_condition_count >= @ChronicConditionCountFrom))
       AND (@ChronicConditionCountTo   IS NULL OR (chronic_condition_count <= @ChronicConditionCountTo))
        -- Active Ingredient Count (Range)
       AND (@ActiveIngredientCountFrom IS NULL OR (active_ingredient_count >= @ActiveIngredientCountFrom))
       AND (@ActiveIngredientCountTo   IS NULL OR (active_ingredient_count <= @ActiveIngredientCountTo))
        -- Probability High Total Cost (Range)
       AND (@ProbabilityHighTotalCostFrom IS NULL OR (rank_prob_high_tot_cost >= @ProbabilityHighTotalCostFrom))
       AND (@ProbabilityHighTotalCostTo   IS NULL OR (rank_prob_high_tot_cost <= @ProbabilityHighTotalCostTo))
        -- Frailty Flag
       AND (@FrailtyInd IS NULL OR (@FrailtyInd = 1 AND (frailty_flag = 'Y' OR frailty_flag = '1')))
)
GO
