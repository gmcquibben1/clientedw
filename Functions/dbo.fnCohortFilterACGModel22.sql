SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel22]()
RETURNS TABLE
AS RETURN (
	-- MTMP Program Selection # 3
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		pharmacy_cost >= 3017 AND 
		active_ingredient_count >= 8 AND
		rxmg_codes LIKE '%CARx020%' AND
		rxmg_codes LIKE '%CARx030%'
)
GO
