SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnGProBusinessUnitTotalQualityPointsGet] 
/*===============================================================
	CREATED BY: 	William Keen
	CREATED ON:		2017-04-13
	INITIAL VER:	2.2.2
	MODULE:			GPro	
	DESCRIPTION:	Returns the total quality points earned by a business unit
	PARAMETERS: 
		@BusinessUnitId - The business unit id.
		@MeasureYear - The year for which to sum quality points
	PROGRAMMING NOTES: 	

	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.2		2017-04-13		WK			LBPP-2467	Initial version
=================================================================*/
(
	 @BusinessUnitId INT,
	 @MeasureYear INT
)
RETURNS  DECIMAL(9,2)
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @BusinessUnitIdHolder INT = @BusinessUnitId
	DECLARE @MeasureYearHolder INT = @MeasureYear
	RETURN (SELECT SUM(
		CASE	WHEN gpmph.CMSBenchmark IS NULL THEN 0
				WHEN gpmph.CMSBenchmark = 0 THEN 0
				ELSE gpmb.QualityPoints
		END)
	 FROM GProMeasureProgressHistory gpmph
	JOIN GProMeasureBenchmark gpmb
		ON gpmph.MeasureTypeId = gpmb.GProMeasureTypeId 
		AND gpmph.MeasureYear = gpmb.MeasureYear
		AND gpmph.CMSBenchmark = gpmb.Benchmark
	WHERE gpmph.MeasureYear = @MeasureYearHolder
	AND gpmph.BusinessUnitId = @BusinessUnitIdHolder)
END
GO
