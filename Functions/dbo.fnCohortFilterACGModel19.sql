SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel19]()
RETURNS TABLE
AS RETURN (
	-- High Pharmacy Users with Discordant Risk
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		prob_unexpected_pharm_cost < 0.2 AND
		pharmacy_cost_band > 7 AND
		total_cost_band < 6 AND
		chronic_condition_count < 2
)
GO
