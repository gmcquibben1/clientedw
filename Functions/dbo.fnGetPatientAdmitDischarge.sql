SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnGetPatientAdmitDischarge]()
RETURNS TABLE
AS RETURN (
   WITH CTE_ADMIT_DISCHARGE AS (
        SELECT [LBPatientId] AS [PatientId],
               [AdmitDate], [DischargeDate], [FacilityName],
               ROW_NUMBER() OVER (
                   PARTITION BY [LbPatientId]
                   ORDER BY [AdmitDate] DESC,
                            [DischargeDate] DESC
               ) AS [RowNumber]
          FROM [dbo].[PatientAdmitDischarge]
         WHERE [DeleteInd] = 0
    )
    SELECT [PatientId],
           [AdmitDate],
           [DischargeDate],
           [FacilityName]
      FROM CTE_ADMIT_DISCHARGE
     WHERE [RowNumber] = 1
   )
GO
