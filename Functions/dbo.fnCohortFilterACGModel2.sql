SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel2]()
RETURNS TABLE
AS RETURN (
	-- Diabetes based on Pharmacy Only
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		diabetes_condition != 'NP' AND
		edc_codes NOT LIKE '%END06%' AND
		edc_codes NOT LIKE '%END07%' AND
		edc_codes NOT LIKE '%END08%' AND
		edc_codes NOT LIKE '%END09%' AND
		edc_codes NOT LIKE '%REN06%' AND
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%' AND
		Medc_codes NOT LIKE '%MAL%' 
)
GO
