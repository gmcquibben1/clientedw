SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel24]()
RETURNS TABLE
AS RETURN (
	--  At Risk for High Utilization
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		coordination_risk LIKE '%LCI%' AND
		unplanned_inpatient_hosp_count >= 1 AND
		rank_prob_high_tot_cost >= 0.4 AND 
		edc_codes NOT LIKE '%REN06%' AND
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%'
)
GO
