SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel3]()
RETURNS TABLE
AS RETURN (
	-- Metabolic Syndrome; Pre-diabetes
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		hypertension_condition != 'NP' AND
		dis_lipid_metab_condition != 'NP' AND
		(edc_codes LIKE '%NUT03%' OR
		rxmg_codes LIKE '%ENDx070%') AND
		diabetes_condition = 'NP' AND 
		edc_codes NOT LIKE '%REN06%' AND
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%' AND
		Medc_codes NOT LIKE '%MAL%' 
)
GO
