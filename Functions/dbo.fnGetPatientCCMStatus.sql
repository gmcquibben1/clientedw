SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnGetPatientCCMStatus]()
RETURNS TABLE
AS RETURN (
   SELECT [patient_id] AS [PatientId],
          CASE WHEN [chronic_condition_count] >= 2
               THEN CONVERT(BIT, 1)
               ELSE CONVERT(BIT, 0)
          END [CCMStatusInd]
     FROM [dbo].[JhacgPatientDetail]
)
GO
