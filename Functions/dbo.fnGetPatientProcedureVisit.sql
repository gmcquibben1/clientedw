SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnGetPatientProcedureVisit]()
RETURNS TABLE
AS RETURN (
    SELECT [PatientId],
           CONVERT(DATE, [ProcedureDateTime]) AS [VisitDate],
           [RenderingProviderNPI]
      FROM [dbo].[PatientProcedure] [ppr] WITH(NOLOCK)
      JOIN [dbo].[PatientProcedureProcedureCode] [ppc] WITH(NOLOCK)
        ON [ppr].[PatientProcedureId] = ppc.[PatientProcedureId]
       AND [ppr].[DeleteInd] = 0
      JOIN [dbo].[ProcedureCode] [prc] WITH(NOLOCK)
        ON [prc].[ProcedureCodeId] = [ppc].[ProcedureCodeId]
       AND [prc].[DeleteInd] = 0
      JOIN (SELECT DISTINCT [Code] [CptCode]
              FROM [dbo].[vwHEDISValueSet] WITH(NOLOCK)
             WHERE [ValueSetName] = 'Outpatient' and [CodeSystem] = 'CPT') AS cpt
        ON cpt.[CptCode] = prc.[ProcedureCode]
)
GO
