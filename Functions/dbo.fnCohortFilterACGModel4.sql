SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCohortFilterACGModel4]()
RETURNS TABLE
AS RETURN (
	-- COPD Diagnosis
	SELECT patient_id [PatientId]
	FROM [dbo].[JhacgPatientDetail] WHERE 
		edc_codes LIKE '%RES04%' AND
		persistent_asthma_condition = 'TRT' AND
		edc_codes NOT LIKE '%REN06%' AND
		edc_codes NOT LIKE '%ADM03%' AND
		edc_codes NOT LIKE '%INF04%' AND
		Medc_codes NOT LIKE '%MAL%' 
)
GO
