SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnGetLastPatientEvent] ()
RETURNS TABLE
AS RETURN (
    WITH CTE_LAST
    AS (SELECT LBPatientId,
               DeleteInd,
               PatientEventTypeId,
               ROW_NUMBER() OVER (
                    PARTITION BY LBPatientId
                    ORDER BY ModifyDateTime DESC
               ) AS [RowNumber]
          FROM PatientEvent
    )
    SELECT cte.LBPatientId AS [PatientId],
           COALESCE(et.DisplayValue, et.Name) AS [EventName]
      FROM CTE_LAST cte
      JOIN PatientEventType et
        ON et.PatientEventTypeId = cte.PatientEventTypeId
       AND cte.RowNumber = 1
       AND et.DeleteInd = 0
       AND cte.DeleteInd = 0
)
GO
