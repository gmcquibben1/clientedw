SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[internalAttributionClinicalPCP] @LookBackMonths int = 18, @debug VARCHAR(10) = 'false'
AS
/*
	Version		Date		Author	Change
	-------		----------	------	------	
	2.2.1       2017-01-19  YL      LBETL-525 Add index to speed process
        2.2.1       2017-02-07  YL      LBETL-426 Not to use table PatientProcedureProcedureCode
*/

BEGIN
	DECLARE @AttributionTypeId int = 7

	EXEC InternalDropTempTables

	--select procedure count by lbpatient id by NPI
	SELECT 
	TRY_CONVERT(varchar(10),
	CASE
	  WHEN len(LTRIM(RTRIM(PP.RenderingProviderNPI))) = 10 AND ISNUMERIC (PP.RenderingProviderNPI) = 1 THEN LTRIM(RTRIM(PP.RenderingProviderNPI))
	  WHEN len(LTRIM(RTRIM(PP.PerformedByProviderID))) = 10 AND ISNUMERIC (PP.PerformedByProviderID) = 1 THEN LTRIM(RTRIM(PP.PerformedByProviderID))
	  WHEN len(LTRIM(RTRIM(PP.PerformedByClinician))) = 10 AND ISNUMERIC (PP.PerformedByClinician) = 1 THEN LTRIM(RTRIM(PP.PerformedByClinician))
	  WHEN len(LTRIM(RTRIM(PP.OrderedByClinician))) = 10 AND ISNUMERIC (PP.OrderedByClinician) = 1 THEN LTRIM(RTRIM(PP.OrderedByClinician))
	end)
	as RenderingProviderNPI
	, PP.PatientID, count(1) as ProcedureCount, max(PP.ProcedureDateTime) as MaxProcedureDate
	into #PASS1
	FROM PatientProcedure PP
	--JOIN PatientProcedureProcedureCode PPPC ON PP.PatientProcedureId = PPPC.PatientProcedureId
	JOIN ProcedureCode PC ON PP.ProcedureCodeId = PC.ProcedureCodeId
	JOIN HEDISValueSetCodes HVSC ON PC.ProcedureCode = HVSC.Code
		  AND HVSC.[Value Set Name] = 'Outpatient'
	WHERE DATEDIFF(Month, PP.ProcedureDateTime, getdate()) <= @LookBackMonths and PP.PatientID IS NOT NULL 
	GROUP BY CASE
	  WHEN len(LTRIM(RTRIM(PP.RenderingProviderNPI))) = 10 AND ISNUMERIC (PP.RenderingProviderNPI) = 1 THEN LTRIM(RTRIM(PP.RenderingProviderNPI))
	  WHEN len(LTRIM(RTRIM(PP.PerformedByProviderID))) = 10 AND ISNUMERIC (PP.PerformedByProviderID) = 1 THEN LTRIM(RTRIM(PP.PerformedByProviderID))
	  WHEN len(LTRIM(RTRIM(PP.PerformedByClinician))) = 10 AND ISNUMERIC (PP.PerformedByClinician) = 1 THEN LTRIM(RTRIM(PP.PerformedByClinician))
	  WHEN len(LTRIM(RTRIM(PP.OrderedByClinician))) = 10 AND ISNUMERIC (PP.OrderedByClinician) = 1 THEN LTRIM(RTRIM(PP.OrderedByClinician))
	end
	, PP.PatientID

        --add index YL 2017-01-19
	CREATE INDEX IX_PatientID_Include on  #PASS1 (PatientID) include (RenderingProviderNPI,ProcedureCount,MaxProcedureDate);

--using the vwportal_ProviderActive to exclude the provides without healthcare org assigned, updated on 04/05/2016
--determin the winner provider by the max claims amount, if claim amount tie, then by the latest procedure Date, min providerID
	INSERT INTO [dbo].[vwPortal_PatientProvider]
        ([PatientID]
        ,[ProviderID]
        ,[SourceSystemID]
        ,[ExternalReferenceIdentifier]
        ,[OtherReferenceIdentifier]
        ,[ProviderRoleTypeID]
        ,[DeleteInd]
        ,[CreateDateTime]
        ,[ModifyDateTime]
        ,[CreateLBUserId]
        ,[ModifyLBUserId]
        ,[AttributionTypeId]
		)
	SELECT 
		[PatientID]
		,ProviderID
		,1 AS [SourceSystemID]
		,NULL AS [ExternalReferenceIdentifier]			
		,NationalProviderIdentifier  AS [OtherReferenceIdentifier]
		,1 AS [ProviderRoleTypeID]
		,0 AS [DeleteInd]
		,GETDATE() AS [CreateDateTime]
		,GETDATE() AS [ModifyDateTime]
		,1 AS [CreateLBUserId]
		,1 AS [ModifyLBUserId]
		,@AttributionTypeId AS [AttributionTypeId]
	FROM(
		SELECT 
		PatientID
		,ProviderID
		,NationalProviderIdentifier
		,ROW_NUMBER() OVER(PARTITION BY PatientID ORDER BY ProcedureCount DESC,MaxProcedureDate DESC,ProviderID ) AS rowNbr
		FROM #PASS1 P1
		JOIN vwPortal_ProviderActive PP 
		ON P1.RenderingProviderNPI = PP.NationalProviderIdentifier
		JOIN NPI_Lookup NPI 
		ON PP.NationalProviderIdentifier = NPI.NPI AND NPI.Specialty IN ('08', '11', '01', 'FP','GP','IM', '37', 'OBG','OBS','GYN','16','38','FPG','IMG','MDM')
		) t WHERE rowNbr = 1

DROP TABLE #PASS1
END
/* --Commented the original code, keep it for reference
CREATE TABLE #PatientProvider (
[PatientProviderId] int IDENTITY(1, 1) NOT NULL,
[ProviderID] int NULL,
[NationalProviderIdentifier] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId] int NULL,
[ProcedureCount] int NULL,
[MaxProcedureDate] date NULL,
[Attributed] int NULL,
PRIMARY KEY CLUSTERED ([PatientProviderId] ))

--Limit to only those in vwPortal_Provider
INSERT INTO #PatientProvider (ProviderID, NationalProviderIdentifier, PatientID, ProcedureCount, MaxProcedureDate, Attributed)
SELECT ProviderID, NationalProviderIdentifier, P1.PatientID, ProcedureCount, MaxProcedureDate, 0 as Attributed
FROM #PASS1 P1
JOIN vwPortal_Provider PP ON P1.RenderingProviderNPI = PP.NationalProviderIdentifier
JOIN NPI_Lookup NPI ON PP.NationalProviderIdentifier = NPI.NPI AND NPI.Specialty IN ('08', '11', '01', 'FP','GP','IM', '37', 'OBG','OBS','GYN','16','38','FPG','IMG','MDM')

IF (@debug = 'true')
BEGIN
SELECT * FROM #PatientProvider
END

--Pick Count of Providers by PatientId (those with 1 win automatically)
SELECT PatientId, count(*) as ProviderCount
INTO #ProviderCounts
FROM #PatientProvider
GROUP BY PatientId

--Update those with 1 winner
UPDATE #PatientProvider SET Attributed = 1 
FROM #PatientProvider PP 
JOIN #ProviderCounts PC ON PP.PatientId = PC.PatientId AND ProviderCount = 1

--pick those with multiple & pick biggest count of procedures
SELECT pp.PatientID, 
Max(ProcedureCount) as MaxProcCount, MAX(PP.MaxProcedureDate) AS MaxProcDate
INTO #PatProcCount_1
FROM #PatientProvider PP 
JOIN #ProviderCounts PC ON PP.PatientId = PC.PatientId AND ProviderCount > 1 and PP.Attributed = 0
GROUP BY pp.PatientID

IF (@debug = 'true')
BEGIN
SELECT * FROM #PatProcCount_1
END

--merge the max count back into PatientProvider to get the provider w/ the max count
SELECT PPC1.PatientID, PP.NationalProviderIdentifier, PP.ProviderID
into #PatProcCount_2
FROM #PatProcCount_1 PPC1
JOIN #PatientProvider PP ON PPC1.PatientID = PP.ProviderID AND PP.ProcedureCount = PPC1.MaxProcCount
GROUP BY PPC1.PatientID, PP.NationalProviderIdentifier, PP.ProviderID

--Calc the # of matches 
SELECT PPC1.PatientID, PPC1.MaxProcCount, count(*) as MatchCount
INTO #PatProcCount
FROM #PatProcCount_1 PPC1
JOIN #PatProcCount_2 PPC2 ON PPC1.PatientID = PPC2.PatientID
GROUP BY PPC1.PatientID, PPC1.MaxProcCount

IF (@debug = 'true')
BEGIN
SELECT * FROM #PatProcCount
END

--find those w/ 1 winner, update PatientProvider
UPDATE #PatientProvider SET Attributed = 2
FROM #PatientProvider PP 
JOIN #PatProcCount PC ON PP.PatientId = PC.PatientId 
                      AND PP.ProcedureCount = PC.MaxProcCount
                      AND MatchCount = 1


IF (@debug = 'true')
BEGIN
SELECT * FROM #PatProcCount
SELECT * FROM #PatientProvider
SELECT Attributed, COUNT(*)
FROM #PatientProvider
GROUP BY Attributed
END

--PICK THOSE WITH MULTIPLE & FIND THE NEWEST
SELECT PP.PatientID,PC.MatchCount,Max(MaxProcedureDate) as MaxProcDate
INTO #PatLatestDate
FROM #PatientProvider PP 
JOIN #PatProcCount PC ON PP.PatientId = PC.PatientId 
                      AND PP.ProcedureCount = PC.MaxProcCount
                      AND MatchCount > 1
GROUP BY PP.PatientID, PC.MatchCount

--find those w/ 1 winner, update PatientProvider
SELECT * FROM #PatientProvider PP
JOIN #PatLatestDate PLD on PP.PatientId = PLD.PatientID 
                      AND PP.ProcedureCount = PLD.MatchCount AND PP.MaxProcedureDate = PLD.MaxProcDate

IF (@debug = 'true')
BEGIN
--SELECT THOSE LEFT TO MATCH
SELECT * FROM #PatLatestDate
--DUPES
END
--find pats w/ dupes

SELECT PatientID, COUNT(*) as matchcount
into #dupPats
FROM #PatientProvider
WHERE Attributed > 0
GROUP BY PatientID
HAVING COUNT(*) > 1

SELECT PatientProviderId, PP.PatientId
INTO #STILLDUPES
FROM #PatientProvider PP
JOIN #dupPats DP ON PP.PatientId = DP.PatientId

UPDATE #PatientProvider SET Attributed = 0 
FROM #PatientProvider PP 
JOIN #STILLDUPES SD ON PP.PatientProviderId = SD.PatientProviderId

UPDATE #PatientProvider SET Attributed = 0 
where PatientProviderId in (
SELECT min(PatientProviderId) as [MinPPID] FROM #STILLDUPES)

SELECT PatientID, COUNT(*) as matchcount
FROM #PatientProvider
WHERE Attributed > 0
GROUP BY PatientID
HAVING COUNT(*) > 1

--set as deleted any PatientProvider records of same type
UPDATE vwPortal_PatientProvider SET DeleteInd = 1 
WHERE AttributionTypeId = @AttributionTypeId
AND DeleteInd = 0

--COMMIT TO PatientProvider table
INSERT INTO vwPortal_PatientProvider (
   PatientID
  ,ProviderID
  ,SourceSystemID
  ,ExternalReferenceIdentifier
  ,OtherReferenceIdentifier
  ,ProviderRoleTypeID
  ,DeleteInd
  ,CreateDateTime
  ,ModifyDateTime
  ,CreateLBUserId
  ,ModifyLBUserId
  ,OverrideInd
  ,AttributionTypeId
  ,AttributedProviderInd
) 
SELECT PatientId, ProviderID, 1, NULL, NationalProviderIdentifier, 1, 0, GETDATE(), GETDATE(), 1,1,0, @AttributionTypeId, 0 
FROM #PatientProvider
WHERE Attributed > 0
GO
*/

GO
