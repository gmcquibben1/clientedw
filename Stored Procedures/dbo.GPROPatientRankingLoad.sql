SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GPROPatientRankingLoad]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2014-12-24
	INITIAL VER:	1.4.2
	MODULE:			GPRO		
	DESCRIPTION:	Import the raw ranking data to the permanent GPRO tables
	PARAMETERS:		none
	RETURN VALUE(s)/OUTPUT:	none
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.0 		2014-12-24		BR						Initial Version
	2.2.1		2016-12-28		BR			LBPP-2070	Adding the statin ranking for 2016 reporting
	2.2.1		2017-01-09		BR			LBDM-1761   Make sure the provider name isn't null, that messes up the patient list display

=================================================================*/
(
	@ImportSetId int
)
AS
BEGIN
    SET NOCOUNT ON;

	DECLARE @GProStatusTypeId AS INT
	DECLARE @MeasureProgramTypeId AS INT

	SELECT @GProStatusTypeId = GProStatusTypeId FROM GProStatusType WHERE NAME = 'Incomplete'
	-- commented by JKSELECT @MeasureProgramTypeId = MeasureProgramTypeId FROM MeasureProgramType WHERE NAME = 'ACO Measures Program-2014'  -- to be updated

	set @MeasureProgramTypeId = 1

	--------------------------------------------------------------------------------
	-- Load the data from the raw table (which holds raw data imported from the ranking XML file) into the GproPatientRanking table
	--------------------------------------------------------------------------------
	MERGE INTO [GproPatientRanking] AS target
	USING (SELECT rr.[group-tin],
		   rr.[medicare-id],
		   rr.[patient-first-name],
		   rr.[patient-last-name],
		   rr.[gender],
		   rr.[birth-date],
		   rr.[provider-npi],
		   ISNULL(rr.[provider-first-name],'') AS [provider-first-name],
		   ISNULL(rr.[provider-last-name],'') AS [provider-last-name],
		   rr.[provider-npi-two],
		   rr.[provider-two-first-name],
		   rr.[provider-two-last-name],
		   rr.[provider-npi-three],
		   rr.[provider-three-first-name],
		   rr.[provider-three-last-name],
		   rr.[clinic-identifier],
		   rr.[caremeddoc-rank],
		   rr.[carefalls-rank],
		   rr.[cad-rank],
		   rr.[dm-rank],
		   rr.[hf-rank],
		   rr.[htn-rank],
		   rr.[ivd-rank],
		   rr.[mh-rank],
		   rr.[pcmammogram-rank],
		   rr.[pccolorectal-rank],
		   rr.[pcflushot-rank],
		   rr.[pcpneumoshot-rank],
		   rr.[pcbmiscreen-rank],
		   rr.[pctobaccouse-rank],
		   rr.[pcbloodpressure-rank],
		   rr.[pcdepression-rank],
		   rr.[pcstatin-rank]
		   FROM [GPROPatientRanking_Raw] rr 
		   WHERE rr.[ImportSetId] = @ImportSetId) AS source
		ON source.[patient-first-name] = target.FirstName AND source.[patient-last-name] = target.LastName 
			AND source.[medicare-id] = target.MedicareHicn AND source.[birth-date] = target.BirthDate
	WHEN NOT MATCHED THEN
		INSERT ([MeasureProgramTypeId]
			   ,[GroupTin]
			   ,[MedicareHicn]
			   ,[FirstName]
			   ,[LastName]
			   ,[GenderCode]
			   ,[BirthDate]
			   ,[ProviderNpi1]
			   ,[ProviderFirstName1]
			   ,[ProviderLastName1]
			   ,[ProviderNpi2]
			   ,[ProviderFirstName2]
			   ,[ProviderLastName2]
			   ,[ProviderNpi3]
			   ,[ProviderFirstName3]
			   ,[ProviderLastName3]
			   ,[ClinicIdentifier]
			   ,[CareMeddocRank]
			   ,[CareFallsRank]
			   ,[CadRank]
			   ,[DmRank]
			   ,[HfRank]
			   ,[HtnRank]
			   ,[IvdRank]
			   ,[MhRank]
			   ,[PcMammogramRank]
			   ,[PcColorectalRank]
			   ,[PcFlushotRank]
			   ,[PcPneumoshotRank]
			   ,[PcbMiscreenRank]
			   ,[PcTobaccouseRank]
			   ,[PcBloodPressureRank]
			   ,[PcDepressionRank]
			   ,[PcStatinRank]
			   ,[GProStatusTypeId]
			   ,[CreateDateTime]
			   ,[ModifyDateTime])
		VALUES (@MeasureProgramTypeId,
			   source.[group-tin],
			   source.[medicare-id],
			   source.[patient-first-name],
			   source.[patient-last-name],
			   source.[gender],
			   source.[birth-date],
			   source.[provider-npi],
			   source.[provider-first-name],
			   source.[provider-last-name],
			   source.[provider-npi-two],
			   source.[provider-two-first-name],
			   source.[provider-two-last-name],
			   source.[provider-npi-three],
			   source.[provider-three-first-name],
			   source.[provider-three-last-name],
			   source.[clinic-identifier],
			   source.[caremeddoc-rank],
			   source.[carefalls-rank],
			   source.[cad-rank],
			   source.[dm-rank],
			   source.[hf-rank],
			   source.[htn-rank],
			   source.[ivd-rank],
			   source.[mh-rank],
			   source.[pcmammogram-rank],
			   source.[pccolorectal-rank],
			   source.[pcflushot-rank],
			   source.[pcpneumoshot-rank],
			   source.[pcbmiscreen-rank],
			   source.[pctobaccouse-rank],
			   source.[pcbloodpressure-rank],
			   source.[pcdepression-rank], 
			   source.[pcstatin-rank],
			   @GProStatusTypeId,
			   GETUTCDATE(),   
			   GETUTCDATE());

	--------------------------------------------------------------------------------
	-- ranking is filled, now create the associated GproPatientMeasure rows
	--------------------------------------------------------------------------------
	MERGE INTO GproPatientMeasure as target
	USING (SELECT 
		gr.GProPatientRankingId,gr.CareFallsRank,gr.CareMeddocRank,
		gr.CadRank,gr.DmRank,gr.HfRank,gr.HtnRank,
		gr.IvdRank,gr.MhRank,
		gr.PcMammogramRank,
		gr.PcColorectalRank,gr.PcFluShotRank,
		gr.PcPneumoShotRank,gr.PcBmiScreenRank,
		gr.PcTobaccouseRank,gr.PcBloodPressureRank,
		gr.PcDepressionRank,
		gr.PcStatinRank
		FROM GproPatientRanking gr) as source
		ON source.GproPatientRankingId = target.GproPatientRankingId
	WHEN NOT MATCHED THEN
		INSERT (GProPatientRankingId,MedicalRecordFound,MedicalNotQualifiedReason,MedicalNotQualifiedDate,CareFallsRank,CareFallsConfirmed,FallsScreening,CareFallsComments,CareMeddocRank,CareMeddocConfirmed,CareMeddocComments,
			CadRank,CadConfirmed,CadDiabetesLvsd,CadAceArb,CadComments,DmRank,DmConfirmed,DmHba1cTest,DmHba1cDate,DmHba1cValue,DmEyeExam,DmComments,HfRank,HfConfirmed,HfLvsd,HfBetaBlocker,HfComments,HtnRank,
			HtnConfirmed,HtnRecentBp,HtnBpDate,HtnBpSystolic,HtnBpDiastolic,HtnComments,IvdRank,IvdConfirmed,IvdAntithrombotic,IvdComments,MhRank,MhConfirmed,MhIndexPerformed,MhIndexTest,MhIndexDate,MhIndexScore,
			MhFollowupPerformed,MhFollowupTest,MhFollowupDate,MhFollowupScore,MhComments,PcMammogramRank,
			PcMammogramConfirmed,PcMammogramPerformed,PcMammogramComments,PcColorectalRank,PcColorectalConfirmed,PcColorectalPerformed,PcColorectalComments,PcFluShotRank,PcFluShotConfirmed,PcFluShotReceived,PcFluShotComments,
			PcPneumoShotRank,PcPneumoShotConfirmed,PcPneumoShotReceived,PcPneumoShotComments,PcBmiScreenRank,PcBmiScreenConfirmed,PcBmiCalculated,PcBmiNormal,PcBmiPlan,PcBmiComments,
			PcTobaccoRank,PcTobaccoConfirmed,PcTobaccoScreen,PcTobaccoCounsel,PcTobaccoComments,PcBloodPressureRank,PcBloodPressureConfirmed,PcBloodPressureScreen,PcBloodPressureNormal,PcBloodPressureFollowUp,PcBloodPressureComments,
			PcDepressionRank,PcDepressionScreen,PcDepressionConfirmed,PcDepressionPositive,PcDepressionPlan,PcDepressionComments,
			PcStatinRank,PcStatinAscvd,PcStatinLdlc,PcStatinDiabetes,PcStatinLdlcReading,PcStatinPrescribed,PcStatinComments,
			CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
		VALUES (source.GProPatientRankingId,null,null,null,source.CareFallsRank,null,null,null,source.CareMeddocRank,null,null,
			source.CadRank,null,null,null,null,source.DmRank,null,null,null,null,null,null,source.HfRank,null,null,null,null,source.HtnRank,
			null,null,null,null,null,null,source.IvdRank,null,null,null,source.MhRank,null,null,null,null,null,
			null,null,null,null,null,source.PcMammogramRank,
			null,null,null,source.PcColorectalRank,null,null,null,source.PcFluShotRank,null,null,null,
			source.PcPneumoShotRank,null,null,null,source.PcBmiScreenRank,null,null,null,null,null,
			source.PcTobaccouseRank,null,null,null,null,source.PcBloodPressureRank,null,null,null,null,null,
			source.PcDepressionRank,null,null,null,null,null,
			source.PcStatinRank,null,null,null,null,null,null,
			GETUTCDATE(),GETUTCDATE(),1,1);

	--------------------------------------------------------------------------------
	-- build the cross references for all of the ranked patients
	--------------------------------------------------------------------------------
	EXEC internalGproMeasureReferenceLoad;

	--------------------------------------------------------------------------------
	-- match these patients up with patients in the Lightbeam database
	--------------------------------------------------------------------------------
	EXEC internalGproPatientMatch;

	--------------------------------------------------------------------------------
	-- Add audit log message to show what was inserted
	--------------------------------------------------------------------------------
	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'CARE-2'
	WHERE gpm.CareFallsRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'CARE-3'
	WHERE gpm.CareMeddocRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'CAD-7'
	WHERE gpm.CadRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'DM-2'
	WHERE gpm.DmRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'DM-7'
	WHERE gpm.DmRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'HF-6'
	WHERE gpm.HfRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'HTN-2'
	WHERE gpm.HtnRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'IVD-2'
	WHERE gpm.IvdRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'MH-1'
	WHERE gpm.MhRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'PREV-5'
	WHERE gpm.PcMammogramRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'PREV-6'
	WHERE gpm.PcColorectalRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'PREV-7'
	WHERE gpm.PcFluShotRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'PREV-8'
	WHERE gpm.PcPneumoShotRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'PREV-9'
	WHERE gpm.PcBmiScreenRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'PREV-10'
	WHERE gpm.PcTobaccoRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'PREV-11'
	WHERE gpm.PcBloodPressureRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'PREV-12'
	WHERE gpm.PcDepressionRank IS NOT NULL;

	INSERT INTO GproAuditHistory
	(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,Comment,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
	SELECT
	gpm.GproPatientRankingId,gpm.GproPatientMeasureId,gmt.GproMeasureTypeId,'Initial Insert',GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm
		INNER JOIN GproMeasureType gmt ON gmt.Name = 'PREV-13'
	WHERE gpm.PcStatinRank IS NOT NULL;

END



GO
