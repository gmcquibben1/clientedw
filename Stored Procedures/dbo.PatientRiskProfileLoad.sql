SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=============================================================
--             Initial Date: 
--             Initial Author: 
-- Modified By: Lan
-- Modified Date: 2016-09-06
-- Description: switched to Jhacg11 table JhacgPatientDetail  
--=============================================================
Create PROCEDURE [dbo].[PatientRiskProfileLoad]
@LbPatientId int
AS
--=============================================================
--             Initial Date: 
--             Initial Author: 
-- Modified By: Lan
-- Modified Date: 2016-09-06
-- Description: switched to Jhacg11 table JhacgPatientDetail  
--=============================================================
BEGIN
	
	SET NOCOUNT ON;

SELECT TOP 1 
	IDR.LbPatientId,
	[12 Mo Cost] AS 'HistoricalCost',
	[Part-A Cost] AS 'FacilityCost',
	[Part-B Cost] AS 'ProfessionalCost',
	[Pharmacy Cost] AS 'PharmacyCost',
	[ER Visits] AS 'ERVisits',
	[IP Admits] AS 'InpatientAdmits',
	[IP Re-Admits] AS 'InpatientReAdmits',
	frailty_flag AS 'FrailtyFlag', 
	pregnancy_without_delivery AS 'PregnancyWithoutDelivery', 
	[hosdom_morbidity_types] AS 'HospitalDominantMorbidityCount', 
	chronic_condition_count AS 'ChronicConditionCount', 
	diagnoses_used AS 'DiagnosisUsedCount',
	[age_rel_macular_deg_condition] AS 'MacularDegeneration', 
	bipolar_disorder_condition AS 'BipolarDisorder', 
	[cong_heart_fail_condition] AS 'CongestiveHeartFailure',
	depression_condition AS 'Depression',
	diabetes_condition AS 'Diabetes',
	glaucoma_condition AS 'Glaucoma',
	[human_imm_virus_condition] AS 'HIV',
	dis_lipid_metab_condition AS 'LipidMetabolismDisorder',
	hypertension_condition AS 'Hypertension',
	hypothyroidism_condition AS 'Hypothyroidism',
	[immuno_transplant_condition] AS 'ImmunoSuppressionTransplantCondition',
	ischemic_hrt_dis_condition AS 'IschemicHeartDisease',
	osteoporosis_condition AS 'Osteoporosis',
	parkinsons_dis_condition AS 'ParkinsonsDisease',
	persistent_asthma_condition AS 'Asthma',
	[rheumatoid_arthr_condition] AS 'RheumatoidArthritis',
	schizophrenia_condition AS 'Schizophrenia',
	seizure_disorders_condition AS 'SeizureDisorder',
	[chronic_obs_pul_dis_condition] AS 'COPD',
	[chronic_renal_fail_condition] AS 'ChronicRenalFailure',
	low_back_pain_condition AS 'LowBackPain'
FROM PatientIdReference IDR
	JOIN Patient_Summary_Pivot PSP ON IDR.LbPatientId = PSP.LbPatientId
	JOIN [dbo].[JhacgPatientDetail] ON PSP.LbPatientId = dbo.[JhacgPatientDetail].patient_id
WHERE IDR.LbPatientId = @LbPatientId
END
GO
