SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create PROCEDURE [dbo].[internalMakeReadmissionStats]
AS
BEGIN

SET NOCOUNT ON


       DECLARE @EDWName VARCHAR(128) = DB_NAME();      
       DECLARE @EDWtableName VARCHAR(128) = 'CCLF_0_PartA_READMITS';
       DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
       DECLARE @ProcedureName VARCHAR(128) = 'internalMakeReadmissionStats';
       DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
       DECLARE @maxSourceIdProcessed INT = NULL;
       DECLARE @StartTime DATETIME = GETUTCDATE();
       DECLARE @EndTime DATETIME = NULL;
       DECLARE @InsertedRecord INT=0;
       DECLARE @UpdatedRecord INT=0;
       DECLARE @Comment VARCHAR(400) = '';
       DECLARE @ErrorNumber INT =0;
       DECLARE @ErrorState INT =0;
       DECLARE @ErrorSeverity INT=0;
       DECLARE @ErrorLine INT=0;
       DECLARE @ERMethod Varchar(10) = 'REV';
       --DECLARE @RecordCountBefore INT=0;
       --DECLARE @RecordCountAfter  INT=0;
       --DECLARE @RecordCountSource INT=0;

       DECLARE @Today DATE=GETUTCDATE();

IF EXISTS (SELECT SetValue FROM vwPortal_SystemSettings WHERE SettingType = 'AnalyticsSetting' AND SettingParameter = 'ERMethod')
BEGIN
SET @ERMethod = (SELECT SetValue FROM vwPortal_SystemSettings WHERE SettingType = 'AnalyticsSetting' AND SettingParameter = 'ERMethod')
END

       
       -- min start
       -- 

       BEGIN TRY

/* Create table that contains all the needed values for the CCLF_0 tables and the MM_metrics table.  Previous logic
                truncated, can be revised after testing */
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CCLF_0_PartA_CLM_TYPES')
BEGIN
Drop table CCLF_0_PartA_CLM_TYPES;
END

CREATE TABLE [dbo].CCLF_0_PartA_CLM_TYPES(
       [id] [bigint] IDENTITY(1,1) NOT NULL,
       [CUR_CLM_UNIQ_ID] [varchar](100) NULL,
       [CLM_FROM_DT] [date] NULL,
       [CLM_THRU_DT] [date] NULL,
       [LbPatientId] [int] NULL,
       [SourceFeed] [varchar](100) NULL,
       [PRNCPL_DGNS_CD] [nchar](7) NULL,
       [ICD_TYPE] [varchar](2) NULL,
		[ERMainClaimInd] [bit] null,
		[ERTotalPaid] [decimal](18,2) null,
		[ERMasterClaimID] [Varchar](100) null,
		[ActiveIndicator] [bit] null,
		[ActiveTotalPaid] [decimal](18,2) null,
		[ActiveMasterClaimID] [Varchar](100) null,
       [30Day_ReAdmit] [int] NULL,
       [60Day_ReAdmit] [int] NULL,
       [90Day_ReAdmit] [int] NULL,
       [LOS] [int] NULL,
       [INP] [bit] NULL,
       [SNF] [bit] NULL,    
       [OUT] [bit] NULL,
       [HOSPICE] [bit] NULL,
       [HH] [bit] NULL,
       [ER] [bit] NULL,
       [ER_POS] [bit] NULL,
       [ER_REV] [bit] NULL,
       [Chronic] [bit] NULL,
       [RehabRev] [bit] NULL,
       [Observation] [bit] NULL,
       [Rehab] [bit] NULL,
       [NonPay] [bit] NULL,
CONSTRAINT [PK__CCLF_0_P__3213E83F3SKIPPY] PRIMARY KEY CLUSTERED 
(
       [id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF, FILLFACTOR = 96) ON [PRIMARY],
CONSTRAINT [UNIQ_SKIPPY_CCLF_0] UNIQUE NONCLUSTERED 
(
       [CUR_CLM_UNIQ_ID] ASC, [Sourcefeed] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF, FILLFACTOR = 96) ON [PRIMARY]
) ON [PRIMARY]

;

/* Insert data into CCLF_0.  Table functions both as the old Claim types table AND as the readmits table, which will become a view
                ID field is cloned from CCLF1, so all joins between these tables will get a performance increase if they join off the ID field 
                
                Fields are grouped together by type and source (with Inpatient to Home Health being by claim type, the ERs grouped, and Acute to Rehab
                using revenue codes 
                
                The table Proc_ClaimTypesControl is used to determine the values used, and if used for groups of codes should be able to be incorporated
                with the existing user interface allowing for customer customization
                */

Set Identity_insert CCLF_0_PartA_CLM_TYPES ON --Needed to update ID field properly.

INSERT INTO CCLF_0_PartA_CLM_TYPES (
       [id],
       [CUR_CLM_UNIQ_ID],
       [CLM_FROM_DT] ,
       [CLM_THRU_DT],
       [LbPatientId],
       [SourceFeed],
       [PRNCPL_DGNS_CD],
       [ICD_TYPE],
	   	[ERMainClaimInd],
		[ERTotalPaid],
		[ERMasterClaimID],
		[ActiveIndicator],
		[ActiveTotalPaid],
		[ActiveMasterClaimID],
       [30Day_ReAdmit],
       [60Day_ReAdmit],
       [90Day_ReAdmit],
       [LOS],
       [INP],
       [SNF], 
       [OUT],
       [HOSPICE] ,
       [HH],
       [ER],
       [ER_POS],
       [ER_REV],
       [Chronic] ,
       [RehabRev],
       [Observation],
       [Rehab],
       [NonPay]
) SELECT DISTINCT
       a.[id],
       a.[CUR_CLM_UNIQ_ID],
       a.[CLM_FROM_DT] ,
       a.[CLM_THRU_DT],
       a.[LbPatientId],
       a.[SourceFeed],
       a.[PRNCPL_DGNS_CD],
       a.[ICD_TYPE],
0, --placeholders for ER Claim logic will get filled in later.
0, 
0, 
0, --placeholders for Active/Reversal/Adjustment logic will get filled in later.
0, 
0, 
0, --placeholders for 30/60/90 day readmits, will get filled in later.
0, 
0, 
       datediff(day,a.clm_from_dt, a.CLM_THRU_DT),  --Lengh of Stay Calculation
       case when b.tablename = 'InpatientClaimType' then 1 else 0 end,  --Each of these uses the Claim Type control table
       case when b.tablename = 'SNFClaimType' then 1 else 0 end,
       case when b.tablename = 'OutpatientClaimType' then 1 else 0 end,
       case when b.tablename = 'HospiceClaimType' then 1 else 0 end,
      case when b.tablename = 'HomeHealthClaimType' then 1 else 0 end,
       case when @ERMethod = 'POS' and a.PRVDR_OSCAR_NUM = '23' then 1
                                when @ERmethod = 'REV' and c.ER = 1 then 1 else 0 end,  -- Set ER flag using system variable as methodology (@ERMethod)
       case when a.PRVDR_OSCAR_NUM = '23' then 1 else 0 end,  --Used to identify ER for customers who don't provide revenue codes
       case when c.ER = 1 then 1 else 0 end, --Identfies ER by revenue codes
       case when e.[Name] is not null then 1 else 0 end, --Chronic Diagnosis Flag.  Set if value exists.
       case when c.Rehab = 1 then 1 when d.DisplayValue = 'RehabDRGType' then 1 
		when d.displayvalue = 'RehabDRGType' then 1 else 0 end,  --rehab as identified by revenue code
       case when c.Observation = 1 then 1 else 0 end, --Observation identified by Revenue Codes
       case when d.DisplayValue = 'RehabDRGType' then 1 else 0 end, --Rehab identified by DRG
       case when (a.CLM_MDCR_NPMT_RSN_CD = '' or a.CLM_MDCR_NPMT_RSN_CD IS NULL) then 0 else 1 end 
FROM CCLF_1_PartA_Header a 
--Join to CCLF 2 via Revenue Code control table. (table c) 
left outer join (select x.CUR_CLM_UNIQ_ID,
                           max (case when y.tablename ='EmergencyRevenueCodeType' then 1 else 0 end) ER,
                           max(case when y.tablename = 'RehabRevenueCodeType' then 1 else 0 end) Rehab,
                           max (case when y.tablename = 'ObservationRevenueCodeType' then 1 else 0 end) Observation
                           from 
                           CCLF_2_PartA_RCDetail x, vwPortal_AllTypeTables y
                           where y.[Name] = right(x.CLM_LINE_REV_CTR_CD,3)
                           group by x.CUR_CLM_UNIQ_ID
							) c on a.cur_clm_uniq_id = c.cur_clm_uniq_id
--Join to CCLF 1 via Claim Types control table (Table b)
left outer join vwPortal_AllTypeTables b on b.[Name] = a.[CLM_TYPE_CD] and right(b.tablename,9) = 'ClaimType'
--Join to CCLF 1 via DRG Codes control table (Table d)
left outer join vwPortal_AllTypeTables d on d.[Name] = a.DGNS_DRG_CD and right(b.tablename,7) = 'DRGType'
--Join to CCLF 1 via chronic ICDs table on code and icd type (defaulting to ICD 9 when null)
left outer join vwPortal_AllTypeTables e on e.[Name] = rtrim(a.[PRNCPL_DGNS_CD]) and left(right(e.tablename,14),1)=isnull(a.ICD_TYPE,'9') 
		and right(e.tablename,13) = 'DiagnosisType'
--go
;
Set Identity_insert CCLF_0_PartA_CLM_TYPES OFF


/* [REMOVED] START BY EMPTYING OUT THE READMITS TABLE */
-- TRUNCATE TABLE CCLF_0_PartA_READMITS; 
--go

/* Replace above logic with drop and replace logic to guarantee matching ID field for 
   enhanced performance */

/* Add ER and duplication logic to CCLF 0 table */

Update CCLF_0_PartA_CLM_TYPES 
set ERMainClaimInd = yay.ERMainClaim,
	ertotalpaid = yay.ertotalpaid,
	ermasterclaimid=yay.ermasterclaimid,
	ActiveIndicator = yay.ActiveIndicator,
	ActivetotalPaid = yay.activetotalpaid,
	ActiveMasterClaimID = yay.activemasterclaimID
From
		(select
		a.id, a.CUR_CLM_UNIQ_ID, 
		case when er.ERTotalPaid > 0 --replace with threshhold
			and er.ermasterclaimID = a.cur_clm_uniq_id then 1 else 0 end ERMainClaim, --Join on this to always get the 'main' ER claim
		er.ERTotalPaid, --Total paid for ER
		er.ERMasterClaimID, --Used to quickly see the entire chunk of ER claims (probably useful for future feature/charts, definitely for Tara)
		case when isnull(dup.ActiveMasterClaimID,a.CUR_CLM_UNIQ_ID) = a.CUR_CLM_UNIQ_ID 
			then 1 else 0 end ActiveIndicator, --This might be useful to put in CCLF 1 if it continues working this well
		dup.ActiveTotalPaid, -- worth checking to see if adjustment chains are failing
		dup.ActiveMasterClaimID --Same as above
		from cclf_1_parta_header a 
	left outer join -- Duplicates patient/date/provider logic to collate duplicate ER visits
			(select a.* from 
				(select a.id, a.CUR_CLM_UNIQ_ID, a.lbpatientid, a.FAC_PRVDR_NPI_NUM, 
				  sum(a.clm_pmt_amt) over (partition by a.lbpatientid, a.clm_from_dt, a.fac_prvdr_npi_num)  as ERTotalPaid, 
				  max(a.CUR_CLM_UNIQ_ID) over (partition by a.lbpatientid, a.clm_from_dt, a.fac_prvdr_npi_num) as ERMasterClaimID,
				  count(a.cur_clm_uniq_id)  over (partition by a.lbpatientid, a.clm_from_dt, a.fac_prvdr_npi_num) as er_count
					from cclf_1_parta_header a, CCLF_0_PartA_CLM_TYPES b
					where b.er = 1 and a.id = b.id
					) a
				where er_count > 1) ER on a.id = er.id
	left outer join -- Creates Dplicate identifying logic for adjustments/reversals/duplicates
			(select b.* from
				(select a.id, a.cur_clm_uniq_id, a.clm_from_dt, a.lbpatientid,
				sum(a.clm_pmt_amt) over 
					(partition by a.lbpatientid, a.clm_from_dt, a.fac_prvdr_npi_num, a.prncpl_dgns_cd, a.dgns_drg_cd, a.atndg_prvdr_npi_num) ActiveTotalPaid,
              first_value(a.cur_clm_uniq_id) over
                     (partition by a.lbpatientid, a.clm_from_dt, a.fac_prvdr_npi_num, a.prncpl_dgns_cd, a.dgns_drg_cd, a.atndg_prvdr_npi_num order by filedate desc, a.clm_thru_dt asc) ActiveMasterClaimID,
				count(a.cur_clm_uniq_id) over
					(partition by a.lbpatientid, a.clm_from_dt, a.fac_prvdr_npi_num, a.prncpl_dgns_cd, a.dgns_drg_cd, a.atndg_prvdr_npi_num) dup_count
				from
				cclf_1_parta_header a) b
			where b.dup_count > 1) Dup on a.id = dup.id
		) yay
where CCLF_0_PartA_CLM_TYPES.id = yay.id
;


--Update non-merged ER claims to flag positive.
Update cclf_0_parta_clm_types set
cclf_0_parta_clm_types.ERMainClaimind = 1, 
cclf_0_parta_clm_types.ertotalpaid = x.clm_pmt_amt
from cclf_0_parta_clm_types a, cclf_1_parta_header x
	where a.cur_clm_uniq_id = x.cur_clm_uniq_id
and 
a.er = 1 and a.ERMasterClaimID is null
;
--Update ER claims below thresshold to negative
Update cclf_0_parta_clm_types set
ERmainclaimind = 0
where
ertotalpaid <= (SELECT isnull(SetValue,0) FROM vwPortal_SystemSettings 
	WHERE SettingType = 'AnalyticsSetting' AND SettingParameter = 'MinPaidSum')

/* UPDATE 30-DAY READMIT FIELD */
/* Uses flags set by prior logic, changed to use lbpatientid instead of Bene_hic_num */
UPDATE CCLF_0_PartA_CLM_TYPES SET
[30Day_ReAdmit] = 1 -- int
WHERE
CUR_CLM_UNIQ_ID IN (
select b.CUR_CLM_UNIQ_ID
from CCLF_0_PartA_CLM_TYPES a, CCLF_0_PartA_CLM_TYPES b
where a.inp=1 and b.inp=1 and a.RehabRev=0 and b.RehabRev=0 and a.nonpay=0 and b.nonpay=0
       and a.lbpatientid=b.lbpatientid and a.rehab=0 and b.rehab = 0
       and a.CUR_CLM_UNIQ_ID <> b.CUR_CLM_UNIQ_ID
       and datediff(day,a.CLM_THRU_DT,b.clm_from_dt) > 1
       and datediff(day,a.clm_thru_dt,b.clm_from_dt) < 31)
;
UPDATE CCLF_0_PartA_CLM_TYPES SET
[60Day_ReAdmit] = 1 -- int
WHERE
CUR_CLM_UNIQ_ID IN (
select b.CUR_CLM_UNIQ_ID
from CCLF_0_PartA_CLM_TYPES a, CCLF_0_PartA_CLM_TYPES b
where a.inp=1 and b.inp=1 and a.RehabRev=0 and b.RehabRev=0 and a.nonpay=0 and b.nonpay=0
       and a.lbpatientid=b.lbpatientid and a.rehab=0 and b.rehab = 0      
       and a.CUR_CLM_UNIQ_ID <> b.CUR_CLM_UNIQ_ID
       and datediff(day,a.clm_thru_dt,b.clm_from_dt) > 1
       and datediff(day,a.clm_thru_dt,b.clm_from_dt) < 61)
;
UPDATE CCLF_0_PartA_CLM_TYPES SET
[90Day_ReAdmit] = 1 -- int
WHERE
CUR_CLM_UNIQ_ID IN (
select b.CUR_CLM_UNIQ_ID
from CCLF_0_PartA_CLM_TYPES a, CCLF_0_PartA_CLM_TYPES b
where a.inp=1 and b.inp=1 and a.RehabRev=0 and b.RehabRev=0 and a.nonpay=0 and b.nonpay=0
       and a.lbpatientid=b.lbpatientid and a.rehab=0 and b.rehab = 0
       and a.CUR_CLM_UNIQ_ID <> b.CUR_CLM_UNIQ_ID
       and datediff(day,a.clm_thru_dt,b.clm_from_dt) > 1
       and datediff(day,a.clm_thru_dt,b.clm_from_dt) < 91)
;


/* TRUNCATE, IMPORT, & UPDATE ALL ADMIT TYPES FOR CCLF A */
/* as above, replaced with drop-and-replace logic to maintain ID field for performance */
--  TRUNCATE TABLE CCLF_0_PartA_CLM_TYPES 
--go
   
/* Update DOS_First in CCLF Tables */
/*Part A Header */
UPDATE CCLF_1_PartA_Header SET 
 CLM_FROM_DT_1ST = DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)
WHERE CLM_FROM_DT_1ST IS NULL
   --go
   
/*Part B Physicians */
UPDATE CCLF_5_PartB_Physicians SET
CLM_FROM_DT_1ST = DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)
  WHERE CLM_FROM_DT_1ST IS NULL
--go



       -----Log Information
              --SET @InsertedRecord=@@ROWCOUNT;
       
          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
                       SET  @EndTime=GETUTCDATE();
                      EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
       
  END TRY

       BEGIN CATCH
                  --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
                           SET  @EndTime=GETUTCDATE();
                           SET  @ErrorNumber =ERROR_NUMBER();
                           SET  @ErrorState =ERROR_STATE();
                           SET  @ErrorSeverity=ERROR_SEVERITY();
                           SET  @ErrorLine=ERROR_LINE();
                           SET  @Comment =left(ERROR_MESSAGE(),400);
                           EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
                           
       END CATCH


END


GO
