SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalImmunizationCodeInsert] 
    @ImmunizationCodeId INT OUTPUT ,
	@ImmunizationCodingSystemTypeId INT ,
	@ImmunizationCode VARCHAR(255)  ,
	@ImmunizationDescription VARCHAR(255) --,
	--@ImmunizationDescriptionAlternate VARCHAR(255) = ''
AS

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @ImmunizationCodeId INT

	BEGIN TRAN

	SELECT @ImmunizationCodeId = ImmunizationCodeId FROM [ImmunizationCode]
	WHERE ImmunizationCodingSystemTypeId  = @ImmunizationCodingSystemTypeId
	AND ImmunizationCode = @ImmunizationCode

	IF @ImmunizationCodeId IS NULL
	BEGIN
		INSERT INTO [dbo].[ImmunizationCode]
			   ([ImmunizationCodingSystemTypeId]
			   ,[ImmunizationCode]
			   ,[ImmunizationDescription]
			   ,[ImmunizationDescriptionAlternate]
			   ,[DeleteInd]
			   ,[CreateDateTime]
			   ,[ModifyDateTime]
			   ,[CreateLBUserId]
			   ,[ModifyLBUserId])
		VALUES 
		(
			@ImmunizationCodingSystemTypeId,
			@ImmunizationCode,
			@ImmunizationDescription,
			@ImmunizationDescription,
			@DeleteInd,
			GetDate(),
			GetDate(),
			@LBUserId,
			@LBUserId
		)
		SELECT @intError = @@ERROR,
			@ImmunizationCodeId = SCOPE_IDENTITY()
	END

	IF @@ERROR <> 0 
	BEGIN
		ROLLBACK TRAN
	END
	ELSE
	BEGIN
		COMMIT TRAN
	END
		
	RETURN @intError

GO
