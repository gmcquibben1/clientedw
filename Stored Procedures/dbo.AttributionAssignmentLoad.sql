SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[AttributionAssignmentLoad]
	@BusinessUnitId INT = NULL,
	@LBUserId INT = NULL, 
	@AssignmentType VARCHAR(10) = NULL,
	@PatientStatus VARCHAR(MAX) = NULL
AS 
BEGIN
	DECLARE @ExternalCode varchar(50)
	SET @ExternalCode = (Select ExternalCode From vwPortal_BusinessUnit Where BusinessUnitId = @BusinessUnitId AND DeleteInd = 0)

	DECLARE @PatientStatuses VARCHAR(MAX) = '''' + REPLACE(@PatientStatus, ',' , ''',''') + ''''
	DECLARE @LevelId INT = 1
	DECLARE @SQLQUERY NVARCHAR(MAX)
	DECLARE @TableValue VARCHAR(50) = NULL
	DECLARE @BusinessUnitLevel INT = NULL
	DECLARE @params NVARCHAR(500)
	WHILE (@LevelId < 7)
	BEGIN
		SET @SQLQUERY = 'SELECT TOP 1 @tv= 1 FROM OrgHierarchy WHERE Level' + CAST(@LevelId AS VARCHAR) + 'Id = ''' + CAST(@ExternalCode AS VARCHAR) + ''''
		SET @params = '@tv varchar(50) OUTPUT'
		EXEC sp_executesql @SQLQUERY, @params, @tv = @TableValue OUTPUT	
		IF @TableValue IS NOT NULL
		BEGIN
			SET @BusinessUnitLevel = @LevelId
			SET @LevelId = 7
		END
		ELSE
			SET @LevelId = @LevelId + 1
	END

	IF(@AssignmentType = 'TIN' OR @AssignmentType = 'CCN')
	BEGIN
		SET @SQLQUERY = 
			'DECLARE @TimeLine DATETIME = (SELECT MAX(TimeLine) FROM MemberRosterArchives) 
			select * from (
      SELECT DISTINCT 
			mra.LBPatientId
			, mra.[HICNO]
			, mra.[First Name]
			, mra.[Last Name]
			, mra.Sex1
			, mra.[Birth date]
			, mra.[Deceased Beneficiary Flag2]
			, mra.[ACO Participant TIN Number]
			, mra.[Count of Primary Care Services3]
			, bu.Name, mra.TimeLine 
      , row_number() over (partition by mra.LbPatientId order by mra.Timeline DESC, mra.[Count of Primary Care Services3] DESC) AS rank
      FROM OrgHierarchy oh
			JOIN MemberRosterArchives mra ON oh.LbPatientId = mra.LbPatientId AND oh.Level2Id = mra.ACO 
			JOIN vwPortal_BusinessUnit bu ON bu.ExternalCode = CAST(mra.[ACO Participant TIN Number] AS VARCHAR)
			WHERE mra.[TimeLine] = @TimeLine AND mra.[ACO Participant Type] = ''' + @AssignmentType + ''' AND oh.Level' + CAST(@BusinessUnitLevel AS VARCHAR) + 'Id = ''' + @ExternalCode + '''
			AND oh.Patient_Status IN (' + @PatientStatuses + ')
			) X WHERE rank = 1 ORDER BY Name'
	END
	IF(@AssignmentType = 'NPI')
	BEGIN
		SET @SQLQUERY = 
			'DECLARE @TimeLine DATETIME = (SELECT MAX(TimeLine) FROM MemberRosterArchives) 
			select * from (
      SELECT DISTINCT 
			mra.LBPatientId
			, mra.[HICNO]
			, mra.[First Name]
			, mra.[Last Name]
			, mra.Sex1
			, mra.[Birth date]
			, mra.[Deceased Beneficiary Flag2]
			, mra.[ACO Participant TIN Number]
			, mra.[Count of Primary Care Services3]
			, mra.IndividualNPI,bu.Name
			, i.LastName AS NPILastName
			, i.FirstName AS NPIFirstName
			, mra.TimeLine
			, row_number() over (partition by mra.LbPatientId order by mra.Timeline DESC, mra.[Count of Primary Care Services3] DESC) AS rank
      FROM OrgHierarchy oh
			JOIN MemberRosterArchives mra ON oh.LbPatientId = mra.LbPatientId AND oh.Level2Id = mra.ACO 
			JOIN vwPortal_BusinessUnit bu ON bu.ExternalCode = CAST(mra.[ACO Participant TIN Number] AS VARCHAR)
			JOIN vwPortal_Provider pp ON mra.IndividualNPI = pp.NationalProviderIdentifier
			JOIN vwPortal_IndividualName i ON pp.IndividualId = i.IndividualId
			WHERE mra.[TimeLine] = @TimeLine AND mra.IndividualNPI IS NOT NULL AND mra.[ACO Participant Type] = ''TIN'' 
			AND oh.Level' + CAST(@BusinessUnitLevel AS VARCHAR) + 'Id = ''' + @ExternalCode + '''
			AND oh.Patient_Status IN (' + @PatientStatuses + ')
			) X WHERE rank = 1 ORDER BY NPILastName'
	END
	IF(@AssignmentType = 'LIST')
	BEGIN
		SET @SQLQUERY = 
			'DECLARE @TimeLine DATETIME = (SELECT MAX(TimeLine) FROM MemberRosterArchives) 
			select * from (
      SELECT DISTINCT 
			mra.LBPatientId
			, mra.[HICNO]
			, mra.[First Name]
			, mra.[Last Name]
			, mra.Sex1
			, mra.[Birth date]
			, mra.[Deceased Beneficiary Flag2]
			, ISNULL(mra.AssignmentFlag, '''') AS AssignmentFlag
			, ISNULL(mra.PrevAssignmentFlag, '''') AS PrevAssignmentFlag
			, mra.TimeLine
			, row_number() over (partition by mra.LbPatientId order by mra.Timeline DESC, mra.[Count of Primary Care Services3] DESC) AS rank
      FROM OrgHierarchy oh
			JOIN MemberRosterArchives mra ON oh.LbPatientId = mra.LbPatientId AND oh.Level2Id = mra.ACO 
			WHERE mra.[TimeLine] = @TimeLine
			AND oh.Level' + CAST(@BusinessUnitLevel AS VARCHAR) + 'Id = ''' + @ExternalCode + '''
			AND oh.Patient_Status IN (' + @PatientStatuses + ')
			) X WHERE rank = 1 ORDER BY [Last Name]'
	END
	IF(@AssignmentType = 'TURNOVER')
	BEGIN
		SET @SQLQUERY = 
			'DECLARE @TimeLine DATETIME = (SELECT MAX(TimeLine) FROM MemberRosterArchives) 
			select * from (
      SELECT DISTINCT 
			  mra.LBPatientId
			, mra.[HICNO]
			, mra.[First Name]
			, mra.[Last Name]
			, mra.Sex1
			, mra.[Birth date]
			, mra.[Deceased Beneficiary Flag2]
			, mra.PluralityPrimaryCare
			, mra.[PartAPartB]
			, mra.[GroupHealthPlan]
			, mra.[NotResideInUS]
			, mra.[IncludedSharedSavingsInitiative]
			, mra.[NoPhysicianVisitACO]
			, row_number() over (partition by mra.LbPatientId order by mra.Timeline DESC, mra.[Count of Primary Care Services3] DESC) AS rank
      FROM OrgHierarchy oh
			JOIN MemberRosterArchives mra ON oh.LbPatientId = mra.LbPatientId AND oh.Level2Id = mra.ACO 
			WHERE mra.[TimeLine] = @TimeLine
			AND oh.Level' + CAST(@BusinessUnitLevel AS VARCHAR) + 'Id = ''' + @ExternalCode + '''
			AND COALESCE(mra.PluralityPrimaryCare , mra.[PartAPartB], mra.[GroupHealthPlan], mra.[NotResideInUS], mra.[IncludedSharedSavingsInitiative], mra.[NoPhysicianVisitACO]) IS NOT NULL
			AND oh.Patient_Status IN (' + @PatientStatuses + ')
			) X WHERE rank = 1 ORDER BY [Last Name]'
	END
	--PRINT @SQLQUERY
	EXEC (@SQLQUERY)
END
GO
