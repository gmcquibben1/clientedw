SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalMedicationInsert] 
    @MedicationId INT OUTPUT ,
	@BrandName VARCHAR(255),
	@GenericName VARCHAR(255),
	@MedicationFormTypeID INT,
	@MedicationRouteTypeID INT,
	@MedicationDose VARCHAR(255) = NULL,
	@NDCCode VARCHAR(255)  ,
	@RxNormCode VARCHAR(255) 
AS

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1,
			@RetiredInd BIT = 0
    --DECLARE @MedicationId INT

	BEGIN TRAN

	SELECT @MedicationId = MedicationId FROM [Medication]
	WHERE ISNULL(NDCCode,'X')  = ISNULL(@NDCCode,'X')
	AND ISNULL(RxNormCode,'X') = @RxNormCode

	IF @MedicationId IS NULL
	BEGIN
		INSERT INTO [dbo].[Medication]
			   (BrandName
			   ,GenericName
			   ,MedicationFormTypeID
			   ,MedicationRouteTypeID
			   ,MedicationDose
			   ,NDCCode
			   ,RxNormCode
			   ,RetiredInd
			   ,[DeleteInd]
			   ,[CreateDateTime]
			   ,[ModifyDateTime]
			   ,[CreateLBUserId]
			   ,[ModifyLBUserId])
		VALUES 
		(
			@BrandName,
			@GenericName,
			@MedicationFormTypeID,
			@MedicationRouteTypeID,
			@MedicationDose,
			@NDCCode,
			@RxNormCode,
			@RetiredInd, 
			@DeleteInd,
			GetDate(),
			GetDate(),
			@LBUserId,
			@LBUserId
		)
		SELECT @intError = @@ERROR,
			@MedicationId = SCOPE_IDENTITY()
	END

	IF @@ERROR <> 0 
	BEGIN
		ROLLBACK TRAN
	END
	ELSE
	BEGIN
		COMMIT TRAN
	END
		
	RETURN @intError


GO
