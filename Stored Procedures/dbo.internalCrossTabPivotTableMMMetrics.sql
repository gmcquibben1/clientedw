SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalCrossTabPivotTableMMMetrics]
AS

-- ================================================================
	-- Author:		Mike
	-- Create date: 2016
	-- Description:	create MM_Metics_Pivot based on MemMonth_Metrics
	--        
	
/*
-- ================================================================
	Version		Date		Author	Change
	-------		----------	------	------
	2.3.1       2017-04-04      YL     LBETL-1134 add dbo. to the MM_Metics_Pivot
              

*/

BEGIN


SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'MM_Metics_Pivot';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalCrossTabPivotTableMMMetrics';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

BEGIN TRY


	--dropping table if exist
	IF OBJECT_ID('dbo.MM_Metics_Pivot', 'U') IS NOT NULL
	  DROP TABLE dbo.MM_Metics_Pivot

	--declaring varialbes
	DECLARE @colBottom NVARCHAR(MAX), @SQL NVARCHAR(MAX), @createTable NVARCHAR(MAX), @colTable NVARCHAR(MAX)

	--concatenating the distinct items of column title in one row using FOR XML PATH
	SET @colTable = STUFF((SELECT DISTINCT ', ['+CAST(Title AS NVARCHAR(200))+']'+ ' INT' FROM MemMonth_Metrics FOR XML PATH('')),1,1, N'');
	SET @colBottom = STUFF((SELECT DISTINCT ', ['+CAST(Title AS NVARCHAR(200))+']' FROM MemMonth_Metrics FOR XML PATH('')),1,1, N'');

	--creating a table
	SET @createTable = N'Create Table dbo.MM_Metics_Pivot (Id int IDENTITY(1,1) PRIMARY KEY, LbPatientId VARCHAR(12), DOS_First DATE, Enrollment Int '+@colTable+')'

	--populating the records in a variable and global temp table
	--the field name VALUE on line 24 next to Title is using coss the PIVOT SECTION need it visible, otherwise throws error for missing field
	SET @SQL = '(SELECT Id, LbPatientId as LbPatientId,ContractName, visitdate as DOS_First, Enrollment, '+@colBottom+' INTO dbo.MM_Metics_Pivot FROM 
				(SELECT membersMonth.ID AS Id, membersMonth.LbPatientId,membersMonth.ContractName, visitdate, 
				Enrollment,
				Title, Value
				FROM membersMonth WITH (NOLOCK)
				LEFT OUTER JOIN MemMonth_Metrics WITH (NOLOCK)
				ON membersMonth.LbPatientId = MemMonth_Metrics.LbPatientId
				AND visitdate = DOS_First
				) derivedTable
				PIVOT
				(SUM(Value) FOR Title IN ('+@colBottom+')) pivotTable)'
      
		  SELECT @SQL

		  --print @SQL

	EXECUTE sp_executesql @SQL

	--showing records
	--SELECT top 100 * FROM dbo.MM_Metics_Pivot  

	IF COL_LENGTH('dbo.MM_Metics_Pivot','Inpatient Admits') IS NULL
	 BEGIN
	 alter table dbo.MM_Metics_Pivot add [Inpatient Admits] [decimal](38, 0) null 
	END
	IF COL_LENGTH('dbo.MM_Metics_Pivot','Inpatient Re-Admits') IS NULL
	 BEGIN
	 alter table dbo.MM_Metics_Pivot add [Inpatient Re-Admits] [decimal](38, 0) null 
	END
	IF COL_LENGTH('dbo.MM_Metics_Pivot','Inpatient Acute Admits') IS NULL
	 BEGIN
	 alter table dbo.MM_Metics_Pivot add [Inpatient Acute Admits] [decimal](38, 0) null 
	END
	IF COL_LENGTH('dbo.MM_Metics_Pivot','ER Visits') IS NULL
	 BEGIN
	 alter table dbo.MM_Metics_Pivot add [ER Visits] [decimal](38, 0) null 
	END
	IF COL_LENGTH('dbo.MM_Metics_Pivot','SNF Admits') IS NULL
	 BEGIN
	 alter table dbo.MM_Metics_Pivot add [SNF Admits] [decimal](38, 0) null 
	END
	IF COL_LENGTH('dbo.MM_Metics_Pivot','Inpatient Cost') IS NULL
	 BEGIN
	 alter table dbo.MM_Metics_Pivot add [Inpatient Cost] [decimal](38, 0) null 
	END
	IF COL_LENGTH('dbo.MM_Metics_Pivot','Hospital Cost') IS NULL
	 BEGIN
	 alter table dbo.MM_Metics_Pivot add [Hospital Cost] [decimal](38, 0) null 
	END
	IF COL_LENGTH('dbo.MM_Metics_Pivot','DME Cost') IS NULL
	 BEGIN
	 alter table dbo.MM_Metics_Pivot add [DME Cost] [decimal](38, 0) null 
	END
	IF COL_LENGTH('dbo.MM_Metics_Pivot','DME Cost') IS NULL
	 BEGIN
	 alter table dbo.MM_Metics_Pivot add [DME Cost] [decimal](38, 0) null 
	END

	--INDEX CREATION
	CREATE NONCLUSTERED INDEX [idx_Nonclustered_MM_Metics_Pivot_LbPatientId]
	ON [dbo].[MM_Metics_Pivot]
	([LbPatientId])
	INCLUDE (
	 [DOS_First], [Inpatient Acute Admits], [Inpatient Admits], [ER Visits], [Inpatient Re-Admits], [SNF Admits], [Professional Cost], [Pharmacy Cost] 
	)
	WITH
	(
	PAD_INDEX = OFF,
	FILLFACTOR = 96,
	IGNORE_DUP_KEY = OFF,
	STATISTICS_NORECOMPUTE = OFF,
	ONLINE = OFF,
	ALLOW_ROW_LOCKS = ON,
	ALLOW_PAGE_LOCKS = ON,
	DATA_COMPRESSION = NONE
	)
	ON [PRIMARY];


	--TRUNCATE OUT ACO METRICS TABLE
	TRUNCATE TABLE MM_Metrics_Pivot_ACO


	INSERT INTO MM_Metrics_Pivot_ACO (
	  DOS_First
	  --,Enrollment
	  ,MemberMonths
	  ,[IP Admits]
	  ,[IP 30 Day ReAdmits]
	  ,[Acute IP Admits]
	  ,[ER Visits]
	  ,[SNF Admits]
	  )
	  SELECT  
	  M.DOS_First,
	  --M.Enrollment,
	  COUNT(M.Id) AS MM, 
	SUM(M.[Inpatient Admits]) AS [Inpatient Admits], 
	sum(M.[Inpatient Re-Admits]),
	SUM(M.[Inpatient Acute Admits]) AS [Inpatient Acute Admits], 
	SUM(M.[ER Visits]) AS [ER Visits], 
	SUM(M.[SNF Admits])
	FROM OrgHierarchy H WITH (NOLOCK)
	INNER JOIN MM_Metics_Pivot M WITH (NOLOCK)
	ON H.LbPatientId = M.LbPatientId
	GROUP BY M.DOS_First


	UPDATE MM_Metics_Pivot SET [Pharmacy Cost] = 0 WHERE [Pharmacy Cost] IS NULL

	--cleanup MemberMonths w/ no Paid Amts
	SELECT DOS_First, sum([Total Cost w/out Pharmacy]) as [TotalCost] INTO #TmpDelMM FROM MM_Metics_Pivot
	group by DOS_First

	DELETE FROM MM_Metics_Pivot WHERE DOS_First IN (SELECT DOS_First FROM #TmpDelMM
	where TotalCost = 0 or TotalCost is null)

	drop table #TmpDelMM

		SELECT @InsertedRecord=count(1)
		FROM [MM_Metics_Pivot] ;

	-----Log Information
		--SET @InsertedRecord=@@ROWCOUNT;
	 
          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH


END
GO
