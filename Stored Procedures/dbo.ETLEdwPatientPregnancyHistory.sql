SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create PROCEDURE [dbo].[ETLEdwPatientPregnancyHistory] 
AS
BEGIN
--SET NOCOUNT ON

-------------------------------------------------
-- Modified By: Youping
-- Modified Date:: 2016-06-13
-- Modification: duplications in PatientPregnancyHistory
-- due to the Null value in the merge key

-- TRIVIAL UPDATES FOR NULL COLUMNS

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId,LbPatientID, DateRecorded 
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				InterfacePatientPregnancyHistoryId
				DESC 
			) SNO
	FROM PatientPregnancyHistoryProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientPregnancyHistoryProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientPregnancyHistoryId INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientPregnancyHistoryId,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientPregnancyHistoryId,PatientID
FROM 
(	
MERGE [DBO].PatientPregnancyHistory AS target
USING (	SELECT [OrigPatientId],[PatientId],[SourceSystemID],[DateRecorded],
		   [PregnancyNumber],[GestationalAge],[EstimatedDateOfDelivery],
		   [DeliveryType],[BirthWeight],[EpisiotomyInd],[EpisiotomyTypeCode],[EpisiotomyPerinealDegreeCode],[EpisiotomyLacerationCode],
		   [FinalDeliveryDate],
		   [DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId] FROM 
		(SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,DateRecorded AS DateRecorded,
			[PregnancyNumber] AS [PregnancyNumber],[GestationalAge] AS [GestationalAge],[EstimatedDateOfDelivery] AS [EstimatedDateOfDelivery],
			[DeliveryType] AS [DeliveryType],[BirthWeight] AS [BirthWeight],[EpisiotomyInd] AS [EpisiotomyInd],[EpisiotomyTypeCode] AS [EpisiotomyTypeCode],[EpisiotomyPerinealDegreeCode] AS [EpisiotomyPerinealDegreeCode],[EpisiotomyLacerationCode] AS [EpisiotomyLacerationCode],
			FinalDeliveryDate AS [FinalDeliveryDate],
			0 AS DeleteInd, GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId,
			ROW_NUMBER() OVER (PARTITION BY LbPatientId,PregnancyNumber ORDER BY DateRecorded DESC) r
		FROM [dbo].PatientPregnancyHistoryProcessQueue PPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 ) src
		WHERE src.r = 1
	  ) AS source 
	  ([OrigPatientId],[PatientId],[SourceSystemID],[DateRecorded],
	   [PregnancyNumber],[GestationalAge],[EstimatedDateOfDelivery],
	   [DeliveryType],[BirthWeight],[EpisiotomyInd],[EpisiotomyTypeCode],[EpisiotomyPerinealDegreeCode],[EpisiotomyLacerationCode],
	   [FinalDeliveryDate],
	   [DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
ON ( target.PatientID = source.PatientID AND target.PregnancyNumber = source.PregnancyNumber -- AND target.BirthWeight = source.BirthWeight)
)
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
--[PatientId]			= source.[PatientId],
[DateRecorded]		= source.[DateRecorded],
--[PregnancyNumber]	= source.[PregnancyNumber],
[GestationalAge]				= source.[GestationalAge],
[EstimatedDateOfDelivery]			= source.[EstimatedDateOfDelivery],
[DeliveryType]		= source.[DeliveryType],
[BirthWeight]		= source.[BirthWeight],
[EpisiotomyInd]		= source.[EpisiotomyInd],
[EpisiotomyTypeCode]		= source.[EpisiotomyTypeCode],
[EpisiotomyPerinealDegreeCode]		= source.[EpisiotomyPerinealDegreeCode],
[EpisiotomyLacerationCode]		= source.[EpisiotomyLacerationCode],
[FinalDeliveryDate]				= source.[FinalDeliveryDate],
[DeleteInd]			= source.[DeleteInd],
--[CreateDateTime]		= source.[CreateDateTime],
[ModifyDateTime]		= source.[ModifyDateTime],
--[CreateLBUserId]		= source.[CreateLBUserId],
[ModifyLBUserId]		= source.[ModifyLBUserId]
WHEN NOT MATCHED THEN
INSERT ([PatientId],[DateRecorded],
		[PregnancyNumber],[GestationalAge],[EstimatedDateOfDelivery],
	    [DeliveryType],[BirthWeight],[EpisiotomyInd],[EpisiotomyTypeCode],[EpisiotomyPerinealDegreeCode],[EpisiotomyLacerationCode],
		[FinalDeliveryDate],
		[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId] )
VALUES (source.[PatientId],source.[DateRecorded],
		source.[PregnancyNumber],source.[GestationalAge],source.[EstimatedDateOfDelivery],
	    source.[DeliveryType],source.[BirthWeight],source.[EpisiotomyInd],source.[EpisiotomyTypeCode],source.[EpisiotomyPerinealDegreeCode],source.[EpisiotomyLacerationCode],
		source.[FinalDeliveryDate],
		source.[DeleteInd],source.[CreateDateTime],source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientPregnancyHistoryId,INSERTED.PatientPregnancyHistoryId) PatientPregnancyHistoryId, source.PatientID
) x ;

--IF OBJECT_ID('PatientPregnancyHistory_PICT') IS NOT NULL
--BEGIN
--	INSERT INTO PatientPregnancyHistory_PICT(PatientPregnancyHistoryId,PatientId,SwitchDirectionId)
--	SELECT RT.PatientPregnancyHistoryId, OrigPatientID, 0 
--	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
--END

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientPregnancyHistoryProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'PatientPregnancyHistory',GetDate()
FROM PatientPregnancyHistoryProcessQueue
WHERE ProcessedInd = 1 


END




GO
