SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[internalMemberMonthRosterCalc]  
/*===============================================================
	CREATED BY: 	Alex Tran, Will Holz
	CREATED ON:		11/30/2016
	INITIAL VER:	2.1.1
	MODULE:			NONE
	DESCRIPTION:	Import Member Months Roster TABLE INTO PatientRosterEnrollmentRaw format
	PARAMETERS:		NONE
	RETURN VALUE(s)/OUTPUT:	NONE
	PROGRAMMIN NOTES: NONE
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.0		      11/30/2016	    	Alex			LBAN-3375	Initial version or descrition of the changes
  2.0         1/11/2017         Mike H    LBAN-3469 Adding EtlOrgHierarchy Content into SP for efficiency
  2.2.2       2/2/2017          Mike H    LBAN-3595 Making last name Last, First (was missing the comma)
=================================================================*/
AS
BEGIN

SET NOCOUNT ON;

-- update LbPatientID
UPDATE PatientRosterMonthsRaw SET LbPatientId = IDR.LbPatientId 
FROM PatientRosterMonthsRaw B 
JOIN PatientIdReference IDR ON B.PatientExternalID = IDR.ExternalId
WHERE ISNULL(b.LbPatientId,-1) <> idr.LbPatientId

--Remove duplicates and pick the latest version of each patient-month
;WITH etc as
(
	SELECT *
	,ROW_NUMBER() OVER (PARTITION BY LbPatientId,MemberMonth ORDER BY FileDate DESC) as [Rank]
	FROM PatientRosterMonthsRaw
)
SELECT *
INTO #dedupe_temp
FROM etc C
WHERE rank = 1

--making min and max month for each patient

SELECT
LbpatientID,MIN(MemberMonth) AS startMon,MAX(MemberMonth) AS endMon
INTO #memberMon
FROM #dedupe_temp
GROUP BY LbpatientID

--DROP TABLE #memberMonStatus - fill in the period in which a patient does not actually have member-months
SELECT
LbpatientID,
MemberMonth,
SourceFeed,
CASE 
	WHEN lag(t.sourcefeed,1,0) OVER (PARTITION BY lbpatientid ORDER BY MemberMonth) <> sourcefeed THEN 'Start' 
	ELSE 'Continuing' 
END AS contractStatus,
maxSourceMon
INTO #memberMonStatus
FROM 
(
SELECT 
t1.LbpatientID, 
t2.Month_Start AS MemberMonth, 
COALESCE(t3.SourceFeed,'NoContract') AS SourceFeed,--months which are missing will be 'no contract' months
endMon AS maxSourceMon
FROM #memberMon t1
JOIN edwreferencedata.dbo.month_dim t2
ON 1 = 1
AND Month_Start >= startMon AND Month_Start <= endMon
LEFT JOIN #dedupe_temp t3
ON t1.LbPatientId = t3.LbPatientId
AND t2.Month_Start = t3.MemberMonth
) t


--DROP TABLE #memberContractStart
SELECT
LbPatientId, 
SourceFeed,
MemberMonth,
maxSourceMon
,ROW_NUMBER() OVER (PARTITION BY LbPatientId,SourceFeed ORDER BY LbPatientId,SourceFeed,MemberMonth ) as [Rank]
,ROW_NUMBER() OVER (PARTITION BY LbPatientId ORDER BY MemberMonth ) as [Rank2]
INTO #memberContractStart
FROM #memberMonStatus


--determines start and term dates for every eligibilty period for each patient
SELECT 
LbPatientId, 
SourceFeed,
maxSourceMon,
MIN(MemberMonth) AS StartDt,
MAX(MemberMonth) AS TermDt
INTO #memberContractStart2
FROM #memberContractStart
WHERE --LbpatientID = 57 AND
SourceFeed <> 'NoContract'
GROUP BY LbPatientId, SourceFeed,[Rank2]- [Rank],maxSourceMon

--if the term date = the maximum source month for that source, then term date = 2099
SELECT
t1.*, 
t2.StartDt,
--t2.TermDt,
CASE WHEN t2.TermDt = maxSourceMon THEN '2099-01-01' ELSE t2.TermDt END AS TermDt
INTO #finalPatientContractMon
FROM #dedupe_temp t1
JOIN #memberContractStart2 t2
ON t1.LbPatientId = t2.LbPatientId
AND t1.MemberMonth = t2.TermDt
AND t1.SourceFeed = t2.SourceFeed
--WHERE t1.LbpatientID = 57 

--contents of ETLOrgHierarchyMMBuild, incorporated into this sp by MH: 1/10/2017
  
SELECT distinct
t1.LbPatientID as LbPatientId,
t1.MemberMonth,
t10.[Name] AS [State], 
t9.City , 
t9.[PostalCode],
t2.[level1_buCode],
t2.[level1_buName],
t2.[level1_buType],
t2.[level2_buCode],
t2.[level2_buName],
t2.[level2_buType],
t2.[level3_buCode],
t2.[level3_buName],
t2.[level3_buType],
t2.[level4_buCode],
t2.[level4_buName],
t2.[level4_buType],
t2.[level5_buCode],
t2.[level5_buName],
t2.[level5_buType],
t2.[level6_buCode],
t2.[level6_buName],
t2.[level6_buType],
--CASE WHEN t1.sourceFeed = 'interface' then 'NoContract' else sourceFeed END,
--NULL,
LTRIM(RTRIM(t11.[ContractName]))  ContractName,
null as medicareNo,
LTRIM(RTRIM(t6.lastName)) + ', ' + LTRIM(RTRIM(t6.firstName)) MemberFullName,
--NULL,
--NULL,
--t1.medicareNo,
--'Active (WithClaims)',
t11.[ContractStatus] + ' (' + [ClaimStatus] + ')' Patient_Status,
COALESCE(t3.ProviderID,0) AS providerID,
COALESCE(LTRIM(RTRIM(t4.[LastName]))  + ', ' + LTRIM(RTRIM(t4.[FirstName])), 'unattributed') AS ProviderName,
COALESCE(t3.[NationalProviderIdentifier], '9999999999') AS ProviderNPI,
t7.BirthDate, 
t7.[GenderTypeID], 
GETDATE() LastUpdatedTimestamp, ROW_NUMBER() OVER (PARTITION BY t1.LbPatientId ORDER BY t1.ProviderNPI) AS RowRank
INTO #OrgHierarchy
FROM #dedupe_temp t1
join vwPortal_ProviderActive t3 on t1.ProviderNPI = t3.NationalProviderIdentifier
join vwPortal_ProviderHealthcareOrg pho on t3.ProviderID = pho.ProviderID 
                                        and pho.DeleteInd = 0 and pho.DefaultInd = 1
JOIN vwPortal_HealthcareOrg ho on pho.HealthcareOrgID = ho.HealthcareOrgId
JOIN [dbo].[orgHierarchyStructure] t2
 on ho.BusinessUnitId = t2.buId
LEFT JOIN [dbo].[vwPortal_IndividualName] t4
ON t3.[IndividualID] = t4.IndividualID
AND t4.DeleteInd = 0
LEFT JOIN [dbo].[vwPortal_Patient] t5
ON t1.LbPatientId = t5.PatientId
AND t5.[DeleteInd] = 0
Left JOIN [dbo].[vwPortal_IndividualName] t6
ON t5.[IndividualID] = t6.IndividualID
--and t6.DeleteInd = 0
LEFT JOIN [dbo].[vwPortal_Individual] t7
ON t5.IndividualID = t7.IndividualID
--And t7.DeleteInd = 0
--LEFT JOIN [dbo].[vwPortal_IndividualStreetAddress] t8
LEFT JOIN 
(SELECT IndividualID, MAX(StreetAddressID) AS StreetAddressID
 FROM [dbo].[vwPortal_IndividualStreetAddress]
 WHERE DeleteInd = 0 
 GROUP BY IndividualID ) t8
ON t7.IndividualID = t8.IndividualID
--And t8.DeleteInd = 0
Left JOIN [dbo].[vwPortal_StreetAddress] t9
ON t8.StreetAddressID = t9.StreetAddressID
--And t9.DeleteInd = 0
LEFT JOIN [dbo].[vwPortal_TerritoryType] t10
ON t9.[TerritoryTypeID] = t10.[TerritoryTypeID]
LEFT JOIN [dbo].[PatientContract] t11
ON t1.[LbPatientID] = t11.[LbPatientId]
AND [IsCurrentInd] = 1


---Modify by Youping
-- Date: June 1, 2016
-- Modification: Add up to 4 level of Id and IDtype description
-- used by Mike Hoxter 8/30/16 for hierarchy history build

 CREATE CLUSTERED INDEX IDX_PatientID_OrgHierarchy ON #OrgHierarchy (LbPatientId);


----get up to 4 level 
Create table #IdTypeDesc (
ID int identity(1,1),
IdTypeDesc Varchar(100)

);



DECLARE @IdTypeValue varchar(200)
SET  @IdTypeValue=( SELECT TOP 1 [SetValue]  FROM vwPortal_SystemSettings WHERE [SettingType]='Patient');

DECLARE @IdTypeDescCnt int
select @IdTypeDescCnt =1+len(@IdTypeValue)- len(replace(@IdTypeValue,',',''));

---select  @IdTypeValue,@IdTypeDescCnt;
DECLARE @I INT=1;

WHILE @I<= @IdTypeDescCnt
BEGIN
   INSERT INTO #IdTypeDesc (IdTypeDesc)
   SELECT dbo.UFN_SEPARATES_COLUMNS(@IdTypeValue,@I,',');
   SET @I=@I+1;
END

	   SELECT  DISTINCT  a.LbPatientId,a.[ExternalId],left(a.IdTypeDesc + ' - '+s.Name,125) IdTypeDesc, b.ID as sortvalue
	   INTO #tmpPatientIdType
		  FROM [dbo].[PatientIdReference] a
		  INNER JOIN #IdTypeDesc b on a.IdTypeDesc=b.IdTypeDesc
		  INNER JOIN [dbo].[SourceSystem] s on s.SourceSystemId=a.SourceSystemId


	  SELECT LbPatientId,[ExternalId],IdTypeDesc, RowNbr
	  INTO #PatientIdType
	  FROM (
	  SELECT LbPatientId,[ExternalId],IdTypeDesc,sortvalue, row_number() OVER(PARTITION BY LbPatientID ORDER BY sortvalue ) AS RowNbr
	  FROM #tmpPatientIdType
	  ) a
	  WHERE a.RowNbr<=4;

	  CREATE CLUSTERED INDEX IDX_PatientIdType ON #PatientIdType (LbPatientId,RowNbr);


	  SELECT a.LbPatientId, a.ExternalId,a.IdTypeDesc
	  , b.ExternalId AS ExternalId2 ,b.IdTypeDesc AS IdTypeDesc2
	  , c.ExternalId AS ExternalId3 ,c.IdTypeDesc AS IdTypeDesc3
	  , d.ExternalId AS ExternalId4 ,d.IdTypeDesc AS IdTypeDesc4
	  INTO #PatientIdTypeQue
	  FROM #PatientIdType a
	  left outer join #PatientIdType b on b.LbPatientId=a.LbPatientId and b.rownbr=2 and  a.RowNbr=1
	  Left outer join #PatientIdType c on c.LbPatientId=a.LbPatientId and c.rownbr=3 and  a.RowNbr=1
	  Left outer join #PatientIdType d on d.LbPatientId=a.LbPatientId and d.rownbr=4 and  a.RowNbr=1
	  WHERE a.RowNbr=1

	  CREATE CLUSTERED INDEX IDX_PatientIdType ON #PatientIdTypeQue (LbPatientId);


DELETE FROM OrgHierarchyHistory WHERE CreatedFromMemberMonths = 1

INSERT INTO OrgHierarchyHistory (
   LbPatientId
  ,State
  ,City
  ,Zip
  ,Level1Id
  ,Level1Name
  ,Level1TypeName
  ,Level2Id
  ,Level2Name
  ,Level2TypeName
  ,Level3Id
  ,Level3Name
  ,Level3TypeName
  ,Level4Id
  ,Level4Name
  ,Level4TypeName
  ,Level5Id
  ,Level5Name
  ,Level5TypeName
  ,Level6Id
  ,Level6Name
  ,Level6TypeName
  ,ContractName
  ,MemberFullName
  ,Patient_Status
  ,ProviderId
  ,ProviderName
  ,ProviderNPI
  ,BirthDate
  ,GenderId
  ,LastUpdatedTimestamp
  ,PatientId1TypeName
  ,PatientId1
  ,PatientId2TypeName
  ,PatientId2
  ,PatientId3TypeName
  ,PatientId3
  ,PatientId4TypeName
  ,PatientId4
  ,CreatedFromMemberMonths
  ,CreateDate
  ,Modifydate
)
  SELECT DISTINCT o.LbPatientId
	    ,o.[State], o.City , o.[PostalCode]
		,o.[level1_buCode],o.[level1_buName],o.[level1_buType]
		,o.[level2_buCode],o.[level2_buName],o.[level2_buType]
		,o.[level3_buCode],o.[level3_buName],o.[level3_buType]
		,o.[level4_buCode],o.[level4_buName],o.[level4_buType]
		,o.[level5_buCode],o.[level5_buName],o.[level5_buType]
		,o.[level6_buCode],o.[level6_buName],o.[level6_buType]
		,o.ContractName,o.MemberFullName
    ,o.Patient_Status
		,o.providerID
		,o.ProviderName
	    ,o.ProviderNPI
	    ,o.BirthDate 
 	    ,o.[GenderTypeID] 
	    ,o.LastUpdatedTimestamp
        ,p.IdTypeDesc
        ,p.ExternalId
	    ,p.IdTypeDesc2
	    ,p.ExternalId2 
	    ,p.IdTypeDesc3
	    ,p.ExternalId3 
	    ,p.IdTypeDesc4
	    ,p.ExternalId4, 1 as CreatedFromMemberMonths, o.MemberMonth as CreateDateTime, o.MemberMonth as ModifyDateTime
  FROM #OrgHierarchy o 
  LEFT OUTER JOIN #PatientIdTypeQue p on o.LbPatientId=p.LbPatientId
--end of OrgHierarchyHistoryBuild



--insert into roster enrollment raw for further processing
DELETE FROM PatientRosterEnrollmentRaw WHERE SourceFeed in (SELECT DISTINCT sourcefeed FROM #dedupe_temp)

INSERT INTO [dbo].[PatientRosterEnrollmentRaw]
			([SourceSystemID]
           ,[PatientExternalID]
           ,[FirstName]
           ,[LastName]
           ,[Gender]
           ,[Birthdate]
           ,[Address]
           ,[City]
           ,[State]
           ,[Zip]
           ,[Phone]
           ,[Email]
           ,[StartDate]
           ,[TermDate]
           ,[DeathDate]
           ,[ProviderNPI]
           ,[FileDate]
           ,[SourceFeed]
           ,[LbPatientId]
			)  
SELECT DISTINCT 
		[SourceSystemID]
        ,[PatientExternalID]
        ,[FirstName]
        ,[LastName]
        ,[Gender]
        ,[Birthdate]
        ,[Address]
        ,[City]
        ,[State]
        ,[Zip]
        ,[Phone]
        ,[Email]
        ,a.startdt [StartDate]
        ,a.termdt [TermDate]
        ,[DeathDate]
        ,[ProviderNPI]
        ,a.[FileDate]
        ,[SourceFeed]
        ,a.[LbPatientId]
FROM #finalPatientContractMon a
ORDER BY a.[LbPatientId],[StartDate],[TermDate]

END
GO
