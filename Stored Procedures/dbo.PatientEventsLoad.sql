SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientEventsLoad]
	@PatientId INTEGER

AS
SELECT DISTINCT
	dbo.PatientEvent.EventDateTime,
	dbo.PatientEventType.DisplayValue,
	dbo.PatientEvent.EventDetail,
	dbo.SourceSystem.Description  as SourceSystem
FROM 
	dbo.PatientEvent INNER JOIN
	dbo.PatientEventType ON PatientEvent.PatientEventTypeId = PatientEventType.PatientEventTypeId LEFT OUTER JOIN
	SourceSystem ON PatientEvent.SourceSystemID = SourceSystem.SourceSystemId
WHERE 
	PatientEvent.LBPatientId = @PatientId AND PatientEvent.DeleteInd = 0

ORDER BY
	PatientEvent.EventDateTime DESC
GO
