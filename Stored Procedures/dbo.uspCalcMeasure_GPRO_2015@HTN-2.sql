SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@HTN-2] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];

	DECLARE  @yearend date, @yearBegin date, @sixMonths date;

	SET @yearBegin = '01/01/2015';
	SET @sixMonths = '07/01/2015';
	SET @yearend   = '01/01/2016';

	-- discover the clinical-only or clinical-plus-claims source systems
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	--Initial Patient pooling, for patients that have ranking for the BCS measure. 
	SELECT r.LbPatientId,
		r.GProPatientRankingId,
		dateDiff(year,r.Birthdate,@yearBegin) as Age,
		r.Birthdate,
		GM.GproMeasureTypeID,
		r.GenderCode,
		GPM.HTNConfirmed,
		GPM.HtnRecentBp, 
		GPM.HtnBpDate, 
		GPM.HtnBpSystolic, 
		GPM.HtnBpDiastolic
	INTO #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE Name = 'HTN-2'
		AND (GPM.HTNConfirmed IS NULL OR GPM.HtnRecentBp IS NULL OR GPM.HtnBpDate IS NULL OR GPM.HtnBpDiastolic IS NULL OR GPM.HtnBpSystolic IS NULL )
   
	--------------------------------------------------------------------------------------------------------    
		-- Denominator Exclusions: Age, or evidence of ESRD/Dialysis/Renal Transplant during or before MY.
		--                         Evidence of Pregnancy during the MY.
	--------------------------------------------------------------------------------------------------------   		    
      
	IF OBJECT_ID( 'tempdb..#Exceptions' ) IS NOT NULL DROP TABLE #Exceptions;
    
    --Pregnancy Diagnosis
	SELECT DISTINCT RP.*
	INTO #Exceptions
	FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON RP.LbPatientId = pd.PatientId
			AND pd.DiagnosisDateTime >= @yearBegin
			AND pd.DiagnosisDateTime < @yearEnd
		INNER JOIN GproExclusionExceptionCode geec ON (geec.Code = pd.DiagnosisCode OR geec.Code = pd.DiagnosisCodeRaw OR geec.Code = pd.DiagnosisCodeDisplay)
			AND geec.[ModuleType]   = 'HTN'
			AND geec.[VariableName] = 'PREGNANCY_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pd.SourceSystemId

	--	JOIN [PatientDiagnosis] pd ON RP.[LbPatientId] = pd.patientid
	--	JOIN [dbo].[PatientDiagnosisDiagnosisCode] pddc ON pddc.[PatientDiagnosisId] = pd.[PatientDiagnosisId]
	--	JOIN [dbo].[DiagnosisCode] dc ON dc.[DiagnosisCodeID] = pddc.[DiagnosisCodeID]
	--	JOIN DBO.[GproExclusionExceptionCode] GEC ON  GEC.[Code]      = dc.[DiagnosisCodeRaw]
	--		AND GEC.[ModuleType]   = 'HTN'
	--		AND GEC.[VariableName] = 'PREGNANCY_CODE'
	--	JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PD.[SourceSystemID]   --DIAGS
	--WHERE Cast( PD.[DiagnosisDateTime] AS DATE ) >= '01/01/2015'--@YearBegin -- '1-1-2014' 
	--	AND Cast( PD.[DiagnosisDateTime] AS DATE ) < '01/01/2016'--@YearEnd --'12-31-2014'
                                                   
    -- ESRD/Renal/Dialysis by Diagnosis 
	INSERT INTO #Exceptions
	SELECT DISTINCT RP.*
	FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON RP.LbPatientId = pd.PatientId
			AND pd.DiagnosisDateTime < @yearEnd
		INNER JOIN GproExclusionExceptionCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = 'HTN'
			AND gec.[VariableName] = 'EX_CODE'
		INNER JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PD.[SourceSystemID]  

	--	JOIN [PatientDiagnosis] pd ON RP.[LbPatientId] = pd.patientid
	--	JOIN [dbo].[PatientDiagnosisDiagnosisCode] pddc ON  pddc.[PatientDiagnosisId] = pd.[PatientDiagnosisId]
	--	JOIN [dbo].[DiagnosisCode] dc ON  dc.[DiagnosisCodeID] = pddc.[DiagnosisCodeID]
	--	JOIN DBO.[GproExclusionExceptionCode] GEC ON  GEC.[Code] = dc.[DiagnosisCodeRaw]
	--		AND GEC.[ModuleType]   = 'HTN'
	--		AND GEC.[VariableName] = 'EX_CODE'
	--	JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PD.[SourceSystemID]   --DIAGS
	--WHERE CAST( PD.[DiagnosisDateTime] AS DATE ) > '01/01/2016'--@YearEnd --'12-31-2014'
      
    -- ESRD/Renal/Dialysis by Procedure   
    INSERT INTO #Exceptions
    SELECT DISTINCT RP.*
    FROM #RankedPatients RP
		INNER JOIN vwPatientProcedure pp ON RP.LbPatientId = pp.PatientId
			AND pp.ProcedureDateTime > @yearBegin
			AND pp.ProcedureDateTime < @yearEnd
		INNER JOIN GproExclusionExceptionCode geec ON geec.Code = pp.ProcedureCode
			AND geec.[ModuleType]   = 'HTN'
			AND geec.[VariableName] = 'EX_CODE'
		INNER JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PP.[SourceSystemID]

	--	JOIN [dbo].[PatientProcedure] PP ON RP.LbPatientId = PP.patientid
	--	JOIN [dbo].[PatientProcedureProcedureCode] PPPC	ON PPPC.[PatientProcedureID] = PP.[PatientProcedureID]
	--	JOIN [dbo].[ProcedureCode] pc ON  pc.[ProcedureCodeId] = pppc.[ProcedureCodeId]
	--	JOIN DBO.[GPROEXCLUSIONEXCEPTIONCODE] GEC ON  GEC.[Code] = PC.[ProcedureCode]
	--		AND GEC.[ModuleType]   = 'HTN'
	--		AND GEC.[VariableName] = 'EX_CODE'
	--	JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
	--WHERE CAST( PP.[ProcedureDateTime] AS DATE ) > @yearBegin
      
    UPDATE RP
    SET RP.[HTNConfirmed] = '17'
    FROM #RankedPatients RP
		JOIN #Exceptions E ON RP.LbPatientId = E.LbPatientId

	--------------------------------------------------------------------------------------------------------    
	-- Denominator Checks - Patient has either CABG, AMI, or PCI 12months prior to the measuremnet period,
	-- Or, had an active diagnosis of IVD during the measurement period.
	--------------------------------------------------------------------------------------------------------    
    
	IF OBJECT_ID( 'tempdb..#DIAG' ) IS NOT NULL 
		DROP TABLE #DIAG;

    SELECT DISTINCT RP.*
    INTO #DIAG
    FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON RP.LbPatientId = pd.PatientId
			AND pd.DiagnosisDateTime >= @yearBegin
			AND pd.DiagnosisDateTime < '07/01/2015'
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = 'HTN'
			AND gec.[VariableName] = 'DX_CODE'
		INNER JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PD.[SourceSystemID]
	UNION
    SELECT DISTINCT RP.*
    FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON RP.LbPatientId = pd.PatientId
			AND pd.DiagnosisDateTime < @yearBegin
			AND pd.ActiveInd <> 0
			AND ISNULL(pd.DiagnosisResolvedDate,'01/01/2016') > '01/01/2015'
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = 'HTN'
			AND gec.[VariableName] = 'DX_CODE'
		INNER JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = pd.[SourceSystemID]

	--    JOIN [PatientDiagnosis] pd ON RP.[LbPatientId] = pd.patientid
	--    JOIN [dbo].[PatientDiagnosisDiagnosisCode] pddc ON  pddc.[PatientDiagnosisId] = pd.[PatientDiagnosisId]
	--    JOIN [dbo].[DiagnosisCode] dc ON  dc.[DiagnosisCodeID] = pddc.[DiagnosisCodeID]
	--    JOIN DBO.[GproEvaluationCode] GEC ON  GEC.[Code]          = dc.[DiagnosisCodeRaw]
	--		AND GEC.[ModuleType]   = 'HTN'
	--		AND GEC.[VariableName] = 'DX_CODE'
	--	JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PD.[SourceSystemID]   --DIAGS
	--WHERE Cast( PD.[DiagnosisDateTime] AS DATE ) < '01/01/2015'            --@YearBegin -- '1-1-2014' 
 --     AND (ActiveInd <> 0  AND  isNULL(DiagnosisResolvedDate,'01/01/2016') > '01/01/2015' ) --@yearBegin)
      
	UPDATE RP
	SET RP.[HTNConfirmed] = '2'
	FROM #RankedPatients RP
		JOIN #DIAG D ON RP.LbPatientId = D.LbPatientId   
	
	  --OFFICE VISIT DURING MEASUREMENT YEAR
	--  IF OBJECT_ID( 'tempdb..#ENCOUNTER' ) IS NOT NULL DROP TABLE #ENCOUNTER;
	--
	--      SELECT DISTINCT D.*
	--      INTO #ENCOUNTER
	--      FROM #DIAG D
	--	    JOIN [dbo].[PatientProcedure] PP 
	--					ON D.LbPatientId = PP.patientid
	--	    JOIN [dbo].[PatientProcedureProcedureCode] PPPC	
	--					ON PPPC.[PatientProcedureID] = PP.[PatientProcedureID]
	--	    JOIN [dbo].[ProcedureCode] pc
	--					ON  pc.[ProcedureCodeId] = pppc.[ProcedureCodeId]
	--	    JOIN DBO.[GproEvaluationCode] GEC
	--					ON  GEC.[Code] = pc.[ProcedureCode]
	--					AND GEC.[VariableName] = 'ENCOUNTER_CODE'
	--          AND GEC.[ModuleType]   = 'HTN'
	--	    WHERE Cast( pp.[ProcedureDateTime] AS DATE ) BETWEEN dateADD(day,1,@yearBegin)
	--		                                               AND     @yearEnd 
                                                   
	  --updating patients without a necessary denominator hit.
	  -- No mention of Visit Requirements for this module, so limited the scope to those without a HTN diag.
	--	    UPDATE #RankedPatients
	--      SET    [HTNConfirmed] = '8'
	--      FROM   #RankedPatients RP
	--      LEFT JOIN 
	--            #DIAG D
	--            ON RP.LbPatientId = D.LbPatientId
	--      Where RP.LbPatientId IS NULL 
  
        
	--------------------------------------------------------------------------------------------------------          
	   --NUMERATOR 
	   --Patients who had a BP test in their last visit. The value check for Systolic/Diastolic is not necessary.
	--------------------------------------------------------------------------------------------------------      
   
	UPDATE rp
	SET	rp.HtnRecentBp = '2',
		rp.HtnBpDate = CONVERT(VARCHAR(10), CAST(pv.ServiceDateTime AS DATE), 101),
		rp.HtnBpSystolic = pv.BloodPressureSystolic,
		rp.HtnBpDiastolic = pv.BloodPressureDiastolic
	FROM #RankedPatients rp
		INNER JOIN (SELECT PatientId, ServiceDateTime, BloodPressureSystolic, BloodPressureDiastolic, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ServiceDateTime DESC) r FROM PatientVitals
					WHERE BloodPressureSystolic IS NOT NULL AND BloodPressureSystolic > 0 AND BloodPressureSystolic < 250
						AND BloodPressureDiastolic IS NOT NULL AND BloodPressureDiastolic > 0 AND BloodPressureDiastolic < 250) AS pv ON pv.PatientId = rp.LbPatientId 
			AND pv.r = 1
			AND pv.ServiceDateTime > @yearBegin
			AND pv.serviceDateTime < @yearEnd


  -- ;WITH [LatestBPbyPatient] AS
  -- (
		--SELECT
		--	pv.[PatientID] AS [PatientID],
		--	ROW_NUMBER () OVER (PARTITION BY pv.patientid ORDER BY CAST( pv.[ServiceDateTime] AS DATE ) DESC, CAST( pv.[BloodPressureSystolic] AS VARCHAR(15)) ASC, CAST( pv.[BloodPressureDiastolic] AS VARCHAR(15) )ASC) LatestRec,-- added by kp 11-3-2015
		--	CAST( pv.[BloodPressureSystolic]  AS VARCHAR(15))    AS [Sys],
		--	CAST( pv.[BloodPressureDiastolic] AS VARCHAR(15) )   AS [Dias],
		--	CAST( pv.[ServiceDateTime] AS DATE )  AS [ReadingDate]
		--FROM #DIAG D
		--	JOIN [dbo].[PatientVitals] pv ON  pv.[PatientID] = D.[LbPatientId]
  --  			AND pv.[ServiceDateTime] >= '1-1-2015' --@MeasurePeriodBegin ---'1-1-2015'-- 
  --  			AND pv.[ServiceDateTime] <  '1-1-2016' --@MeasurePeriodEnd --'1-1-2016'-- 
		--	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PV.[SourceSystemID]    --Vitals
		--WHERE NULLIF( pv.[BloodPressureSystolic] , 0 )  IS NOT NULL 
  --  			AND NULLIF( pv.[BloodPressureDiastolic], 0 ) IS NOT NULL
		--GROUP BY
  --  			pv.[PatientID],
		--		pv.[ServiceDateTime], 
		--		pv.[BloodPressureSystolic], 
		--		pv.[BloodPressureDiastolic]
  --  )

	
  --  --Updating the Ranked table with the BP Values from the latest visit
  --  UPDATE RP WITH (TABLOCK)
  --  SET
		--[HTNBPDate]        = BP.[ReadingDate],
		--[HTNBPSystolic]    = BP.Sys,
		--[HTNBPDiastolic]   = BP.Dias,
		--[HTNRecentBP]      = '2'
  --  FROM [#RankedPatients] RP
		--JOIN [LatestBPbyPatient] BP ON  BP.[PatientID] = RP.[LbPatientId]
  --  WHERE BP.latestrec = 1 
		--AND RP.[HTNConfirmed] IS NULL
  --  ;


	--------------------------------------------------------------------------------------------------------          
	   -- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	UPDATE gpm
	SET gpm.[HTNConfirmed]   = GPR.[HTNConfirmed], gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
    WHERE gpm.HtnConfirmed IS NULL 
		AND GPR.[HTNConfirmed] IS NOT NULL

	UPDATE gpm
	SET    gpm.[HTNRecentBP]    = GPR.[HTNRecentBP],
           gpm.[HTNBPDate]      = GPR.[HTNBPDate],	   
           gpm.[HTNBPSystolic]  = GPR.[HTNBPSystolic],
    	   gpm.[HTNBPDiastolic] = GPR.[HTNBPDiastolic],
           gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
    WHERE gpm.HtnRecentBp IS NULL 
		AND gpm.HtnBpDate IS NULL


END

     
GO
