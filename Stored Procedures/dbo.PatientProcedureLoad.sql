SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create PROCEDURE [dbo].[PatientProcedureLoad] 
	@PatientId INTEGER
AS
/*===============================================================
-- Modified by Youping
-- Modified Date: 2016-07-21
-- PlaceOfServiceCode pick one has value from PlaceOfService
-- Modified by Youping
-- Modified Date: 2016-07-27
-- Get place of service description from reference table LF_POS
-- if find Place_name displace place_name else PlaceOfService
------
	MODIFICATIONS
	Version     Date            Author      JIRA            Change
	=======     ==========      ======      =========	=========	
	2.2.1	    2017-01-04      YL          LBETL-413	retire table PatientProcedureProcedureCode 

===============================================================*/
BEGIN
    SET NOCOUNT ON;

SELECT DISTINCT       
	dbo.ProcedureCode.ProcedureCode,
	dbo.ProcedureCode.ProcedureDescription,
	dbo.ProcedureCode.ProcedureDescriptionAlternate,
	dbo.SourceSystem.Description as SourceSystem,
	dbo.PatientProcedure.ProcedureCodeModifier1,
	dbo.PatientProcedure.ProcedureCodeModifier2,
	dbo.PatientProcedure.ProcedureCodeModifier3,
	dbo.PatientProcedure.ProcedureCodeModifier4,
	dbo.PatientProcedure.ProcedureDateTime,
	dbo.PatientProcedure.ProcedureComment,
	dbo.PatientProcedure.PerformedByClinician,
	dbo.PatientProcedure.OrderedByClinician,
	dbo.PatientProcedure.PlaceOfService as PlaceOfServiceCode,
	ISNULL(POS.[Place_Name],dbo.PatientProcedure.PlaceOfService) as PlaceOfService
FROM dbo.PatientProcedure INNER JOIN
	-- dbo.PatientProcedureProcedureCode ON PatientProcedure.PatientProcedureId = PatientProcedureProcedureCode.PatientProcedureId INNER JOIN
	-- dbo.ProcedureCode ON PatientProcedureProcedureCode.ProcedureCodeId = ProcedureCode.ProcedureCodeId INNER JOIN 
	 dbo.ProcedureCode ON PatientProcedure.ProcedureCodeId = ProcedureCode.ProcedureCodeId INNER JOIN 
	 dbo.ProcedureCodingSystemType ON ProcedureCode.ProcedureCodingSystemTypeId = ProcedureCodingSystemType.ProcedureCodingSystemTypeId LEFT OUTER JOIN
 	 dbo.SourceSystem ON PatientProcedure.SourceSystemId = SourceSystem.SourceSystemId 
	 LEFT OUTER JOIN [EdwReferenceData].dbo.LU_POS POS on POS.place_of_service=try_convert(int,LEFT(dbo.PatientProcedure.PlaceOfService,2))
WHERE dbo.PatientProcedure.PatientId = @Patientid AND dbo.PatientProcedure.DeleteInd = 0 

ORDER BY PatientProcedure.ProcedureDateTime DESC, ProcedureCode.ProcedureDescription ASC 

END

GO
