SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@CAD-7] @LookbackTimestamp DATETIME=N'2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID(N'tempdb..#RANKEDPATIENTS') IS NOT NULL DROP TABLE [#RANKEDPATIENTS];
	DECLARE  @yearend date, @yearBegin date, @source VARCHAR(10);

	SET @yearBegin = N'01/01/2015';
	SET @yearend   = N'01/01/2016';

	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	-----------------------------------------------------------------------------------------------------
	--Initial Patient pooling, for patients that have ranking for the CAD-7 measure. 
	-----------------------------------------------------------------------------------------------------
	SELECT r.LBPatientID,
		r.GProPatientRankingId,
		dateDiff(year,r.Birthdate,@yearBegin) as Age,
		r.Birthdate,
		GM.GproMeasureTypeID,
		r.GenderCode,
		GPM.CadConfirmed ,
		GPM.CadDiabetesLVSD, 
		GPM.CadAceARB   
	INTO #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GproPatientRankingId = r.GproPatientRankingID
	WHERE GM.Name = N'CAD-7' 
		AND (GPM.CadConfirmed IS NULL OR GPM.CadDiabetesLvsd IS NULL OR GPM.CadAceArb IS NULL)
    
	--------------------------------------------------------------------------------------------------------    
	  -- CAD Confirmation: Diagnosis of CAD or previous cardiac surgery
	--------------------------------------------------------------------------------------------------------
      
	IF OBJECT_ID( N'tempdb..#CADConfirm' ) IS NOT NULL 
		DROP TABLE #CADConfirm;
      
	-- Active or Pre-existing Coronary Heart Disease.
    SELECT RP.*
    INTO #CADConfirm
    FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = RP.LbPatientId
			AND pd.DiagnosisDateTime < @YearEnd
			AND (pd.ActiveInd <> 0 OR (pd.DiagnosisResolvedDate IS NULL OR pd.DiagnosisResolvedDate > @yearBegin))
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = N'CAD'
			AND gec.[ModuleIndicatorGPRO] = N'CAD Confirm'
			AND gec.[VariableName] = N'DX_CODE'
		INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pd.[SourceSystemID]
      
	-- History of CAD procedures  
    INSERT INTO #CADConfirm
	SELECT DISTINCT RP.*
	FROM  #RankedPatients RP
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = RP.LbPatientId
			AND pp.ProcedureDateTime < @YearEnd
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND gec.[ModuleIndicatorGPRO] = N'CAD Confirm'
			AND gec.[ModuleType]   = N'CAD'
	        AND gec.[VariableName] = N'PROC_CODE'
		INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pp.[SourceSystemID]
	WHERE RP.CadConfirmed IS NULL

    -- Setting patients with out the requisite diagnosis to 8, as per specs.   
    --UPDATE #RankedPatients
    --SET    CADConfirmed = '8'
    --FROM   #RankedPatients RP
    --LEFT JOIN  #CADConfirm C
    --    ON RP.LBPATIENTID = C.LBPATIENTID
    --WHERE C.LBPATIENTID IS NULL
    
    -- setting patients to confirmed if they were of age and the above criteria.
    UPDATE #RankedPatients
    SET CADConfirmed = '2'
    FROM #RankedPatients RP
		JOIN #CADConfirm C ON RP.LBPATIENTID = C.LBPATIENTID
    WHERE RP.CADConfirmed IS NULL

	--------------------------------------------------------------------------------------------------------          
	   --CAD - 7 Steps
	   --Diabetes or LVSD <40% or marked as moderate/severe.
	--------------------------------------------------------------------------------------------------------
      
	IF OBJECT_ID( N'tempdb..#LVSD' ) IS NOT NULL 
		DROP TABLE #LVSD;
      
	-- Active Diabetes    
    SELECT RP.*
    INTO #LVSD
    FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = RP.LbPatientId
			AND pd.DiagnosisDateTime < @YearEnd
			AND (pd.ActiveInd <> 0 OR (pd.DiagnosisResolvedDate IS NULL OR pd.DiagnosisResolvedDate > @yearBegin))
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType] = N'CAD'
			AND gec.[ModuleIndicatorGPRO] = N'7'
		    AND gec.[VariableName] = N'DM_DX_CODE'
		INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pd.[SourceSystemID]
    WHERE RP.CADConfirmed = '2'
     
      -- Active LVSD
    INSERT INTO #LVSD
    SELECT RP.*
    FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = RP.LbPatientId
			AND pd.DiagnosisDateTime < @YearEnd
			AND (pd.ActiveInd <> 0 OR (pd.DiagnosisResolvedDate IS NULL OR pd.DiagnosisResolvedDate > @yearBegin))
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType] = N'CAD'
			AND gec.[ModuleIndicatorGPRO] = N'7'
		    AND gec.[VariableName] = N'LVSD_DX_CODE'
			AND EXISTS (SELECT TOP 1 1 
						FROM vwPatientDiagnosis pd2 
							INNER JOIN GproEvaluationCode gec2 ON (gec2.Code = pd2.DiagnosisCode OR gec2.Code = pd2.DiagnosisCodeRaw OR gec2.Code = pd2.DiagnosisCodeDisplay)
								AND gec2.[ModuleType]   = N'CAD'
								AND gec2.[ModuleIndicatorGPRO] = N'7'
								AND gec2.[VariableName] = N'SEVERITY_MODIFIER' 
						WHERE pd2.PatientId = pd.PatientId 
							AND pd2.DiagnosisDateTime = pd.DiagnosisDateTime
						)
		INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pd.[SourceSystemID]
    WHERE RP.CADConfirmed = '2'

    INSERT INTO #LVSD
    SELECT RP.*
	FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = RP.LbPatientId
			AND pd.DiagnosisDateTime < @YearEnd
			AND (pd.ActiveInd <> 0 OR (pd.DiagnosisResolvedDate IS NULL OR pd.DiagnosisResolvedDate > @yearBegin))
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType] = N'CAD'
			AND gec.[ModuleIndicatorGPRO] = N'7'
		    AND gec.[VariableName] IN (N'MOD_LVSD_CODE', N'SEV_LVSD_CODE') 
		INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pd.[SourceSystemID] 

    INSERT INTO #LVSD
    SELECT RP.*
	FROM #RankedPatients RP
		JOIN [dbo].[PatientLabOrder] plo ON  plo.[PatientID] = RP.[LbPatientID] -- Correlated subquery
		JOIN [dbo].[PatientLabResult] plr ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
			AND IsNull( Try_Convert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 ) <= 40.0
		JOIN [dbo].[GproEvaluationCode] GEC ON (GEC.[Code] = plr.[ObservationCode1] OR GEC.[Code] = plr.[ObservationCode2])
			AND GEC.[ModuleType]   = N'CAD'
			AND GEC.[ModuleIndicatorGPRO] = N'7'
			AND GEC.[VariableName] = N'EF_CODE'  
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = plo.[SourceSystemID]
   
    UPDATE #RankedPatients
    SET CADDiabetesLVSD  = N'2'
    FROM #RankedPatients RP
		JOIN #LVSD L ON RP.LBPATIENTID = L.LBPATIENTID
    WHERE RP.CADConfirmed = N'2'
    
  --  UPDATE #RankedPatients
  --  SET CADDiabetesLVSD  = N'1'
  --  FROM #RankedPatients RP
		--LEFT JOIN #LVSD L ON RP.LBPATIENTID = L.LBPATIENTID
  --  WHERE RP.CADConfirmed = N'2' 
		--AND L.LbPatientId IS NULL;

	--------------------------------------------------------------------------------------------------------          
	   --DENOMINATOR Exceptions
	   --Documented (Medical, System, or Patient) reasons for not prescribing beta-blocker therapy.
	--------------------------------------------------------------------------------------------------------      
     
	IF OBJECT_ID( 'tempdb..#Exclusions' ) IS NOT NULL 
		DROP TABLE #Exclusions;
  
     
    --Checking Diagnosis SNMs first for exclusions.
    SELECT DISTINCT RP.*,
                    ExclusionReason = (CASE WHEN GEEC.[VariableName] = 'PATIENT_REASON' THEN GEEC.[VariableName]
                                            WHEN GEEC.[VariableName] = 'SYSTEM_REASON'  THEN GEEC.[VariableName]
                                            ELSE 'Medical_Reason'
                                        END)
    INTO #Exclusions
    FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = RP.LbPatientId
			AND pd.DiagnosisDateTime < @YearEnd
			AND (pd.ActiveInd <> 0 OR (pd.DiagnosisResolvedDate IS NULL OR pd.DiagnosisResolvedDate > @yearBegin))
		INNER JOIN GproExclusionExceptionCode geec ON (geec.Code = pd.DiagnosisCodeRaw OR geec.Code = pd.DiagnosisCode OR geec.Code = pd.DiagnosisCodeDisplay)
			AND geec.[ModuleType] = N'CAD'
			AND geec.[ModuleIndicatorGPRO] = N'7'
		INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pd.[SourceSystemID] 
	WHERE RP.CadDiabetesLvsd = N'2'
      
      
    --Checking Procedure SNMs for exclusions.
    INSERT INTO #Exclusions
	SELECT DISTINCT RP.*,
                    ExclusionReason = (CASE WHEN GEEC.[VariableName] = 'PATIENT_REASON' THEN GEEC.[VariableName]
                                            WHEN GEEC.[VariableName] = 'SYSTEM_REASON'  THEN GEEC.[VariableName]
                                            ELSE 'Medical_Reason'
                                        END)
	FROM  #RankedPatients RP
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = RP.LbPatientId
			AND pp.ProcedureDateTime < @YearEnd
		INNER JOIN GproExclusionExceptionCode geec ON (geec.Code = pp.ProcedureCode)
			AND geec.[ModuleType] = N'CAD'
			AND geec.[ModuleIndicatorGPRO] = N'7'
	WHERE RP.CadDiabetesLvsd = N'2'
      
    UPDATE #RankedPatients
    SET CADAceARB =     CASE WHEN EXCLUSIONREASON = 'Medical_Reason' THEN '4'
                            WHEN EXCLUSIONREASON = 'PATIENT_REASON' THEN '5'
                            WHEN EXCLUSIONREASON = 'SYSTEM_REASON'  THEN '6'
                        END
    FROM #RankedPatients RP
		JOIN #Exclusions EX ON RP.LbPatientId = EX.LbPatientId
           
	--------------------------------------------------------------------------------------------------------          
	   --NUMERATOR 
	   --Patients who were prescribed a ACE/ARB therapy within a 12month period. 
	--------------------------------------------------------------------------------------------------------      

	IF OBJECT_ID( 'tempdb..#Num' ) IS NOT NULL 
		DROP TABLE #Num;    
		--Drugs  
    
	SELECT DISTINCT RP.*
	INTO  #NUM
	FROM  #RankedPatients RP
		INNER JOIN vwPatientMedication pm ON pm.PatientId = RP.LbPatientId
			AND ISNULL(pm.MedicationStartDate, '1900-01-01') < @yearEnd
			ANd ISNULL(pm.MedicationEndDate, @yearEnd) > @yearBegin
		INNER JOIN GproDrugCode gdc ON (gdc.Code = pm.NdcCode OR gdc.Code = pm.RxNormCode)
			AND gdc.ModuleType = 'CAD'
			AND gdc.ModuleIndicatorGPRO = '7'
			AND gdc.VariableName = 'ACE_ARB_CODE'
		INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pm.[SourceSystemID]

	--Updating patients who have All the required categories.    
	UPDATE #RankedPatients
	SET CADAceARB = 2
	FROM #NUM N
		LEFT JOIN  #Exclusions EX ON N.LbPatientId = EX.LbPatientId
		JOIN  #RankedPatients RP ON N.LbPatientId = RP.LbPatientId
	WHERE EX.LbPatientId is null
  
  
	----setting all patients who qualified for the denom without exclusions to confirmed, and with out 
	--UPDATE #RankedPatients
	--SET CADAceARB = 1
	--FROM #RankedPatients RP
	--	LEFT JOIN  #Exclusions EX ON RP.LbPatientId = EX.LbPatientId
	--WHERE EX.LbPatientId IS NULL 
	--	AND RP.CADDiabetesLVSD  = N'2'
      
	--------------------------------------------------------------------------------------------------------          
	   -- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	UPDATE gpm
	SET gpm.CadConfirmed = rPat.CadConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN #RankedPatients rPat ON rPat.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.CadConfirmed IS NOT NULL

	UPDATE gpm
	SET gpm.CadAceArb = rPat.CadAceArb, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN #RankedPatients rPat ON rPat.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.CadAceArb IS NOT NULL

	UPDATE gpm
	SET gpm.CadDiabetesLvsd = rPat.CadDiabetesLvsd, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN #RankedPatients rPat ON rPat.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.CadDiabetesLvsd IS NOT NULL


END

     
GO
