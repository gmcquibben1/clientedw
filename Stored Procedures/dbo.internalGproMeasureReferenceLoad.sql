SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalGproMeasureReferenceLoad]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2015-12-29
	INITIAL VER:	1.6.1
	MODULE:			GPRO		
	DESCRIPTION:	Creates a cross reference between the GproMeasureType table and the patients that are ranked for the measures
	PARAMETERS:		none
	RETURN VALUE(s)/OUTPUT:	none
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.6.1		12.29.2015		BR						Initial Version
	2.2.1		11.03.2016		BR						Refactored to use dynamic SQL and include the PREV-13 measure

=================================================================*/
AS
BEGIN
    SET NOCOUNT ON;

	TRUNCATE TABLE GproPatientRankingGproMeasureType;

	DECLARE @Sql VARCHAR(4000);
	DECLARE @MeasureTypeCount INT = (SELECT MAX(GproMeasureTypeId) FROM GproMeasureType);
	DECLARE @CurrentRankColumn VARCHAR(20) = NULL;
	DECLARE @CurrentMeasureName VARCHAR(20) = NULL;

	WHILE @MeasureTypeCount > 0
	BEGIN
		SET @CurrentRankColumn = (SELECT RankColumnName FROM GproMeasureType WHERE GproMeasureTypeId = @MeasureTypeCount)
		SET @CurrentMeasureName = (SELECT Name FROM GproMeasureType WHERE GproMeasureTypeId = @MeasureTypeCount)

		IF (@CurrentRankColumn IS NOT NULL)
		BEGIN
			SET @Sql = 'INSERT INTO GproPatientRankingGproMeasureType
					(GproPatientRankingId,GproMeasureTypeId,MeasureRank,DeleteInd,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
				SELECT
					gpr.GproPatientRankingId,gmt.GproMeasureTypeId,gpr.' + @CurrentRankColumn + ',0,GETUTCDATE(),GETUTCDATE(),1,1
				FROM GproPatientRanking gpr
					INNER JOIN GproMeasureType gmt ON Gmt.Name = ''' + @CurrentMeasureName + '''
				WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL';
			EXEC (@Sql);
		END

		SET @MeasureTypeCount = @MeasureTypeCount - 1;
	END


END
GO
