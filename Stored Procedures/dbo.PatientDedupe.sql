SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientDedupe]  
AS
BEGIN
	

	
/*=================================================================================================
	CREATED BY: 	SG
	CREATED ON:		2015-12-24
	INITIAL VER:	1.5.3
	MODULE:			ETL/Patient de-deuplication 
	DESCRIPTION: 


	Description:	Process to find, audit and merge duplicate patients

	   Step 1: Get a list of Duplicate Patients
	   Step 2: Select Winning Patient Id (min value of patientId for all dupes of a patient) 
	   Step 3: Loop thru Target tables to be updated
	   Step 4: Keep an audit record of each statement being issued
	   Step 5: Issue the Update against each target table with winning Patient Id
	   Step 6: Update those tables with DeleteInd flag
	   Step 7: Log all PatientIds de-duped into history table

	MODIFICATIONS
	Version     Date            Author		  JIRA  		Change
	=======     ==========      ======      =========	=========	
  2.2.2       2015-12-24		SG					  Initial Version
  2.2.2       03.07.2017		CJL		  LBAN-3490   Added logging, removed hardcoded value type of 1, handled issues in which duplicate records would be inserted. Performance tuned the data extraction queries
  2.3.1       03.07.2017		CJL		  LBAN-3429   Removed Dependency on hard coded values for the EDW and portal databse



====================================================================================================*/
	

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @PortalName VARCHAR(128) = REPLACE(DB_NAME(),  'edw', 'Lbportal')
	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientDedupe';
	DECLARE @dataSource VARCHAR(20) = 'EDW';
	DECLARE @ProcedureName VARCHAR(128) = 'PatientDedupe';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @NUMROWS INT


	BEGIN TRY


	
	------------------------------------------------------------------------------
	-- Step 1: Get a list of Duplicate Patients
	------------------------------------------------------------------------------

exec InternalDropTempTables

	--SELECT ME.FirstName, ME.LastName, ME.Gender, ME.BirthDate
	--INTO #TMP1
	--FROM (
	--	SELECT COUNT(DISTINCT PatientId) AS cnt, LastName, FirstName, BirthDate, Gender 
	--	FROM vwPatientDemographic
	--	GROUP BY LastName, FirstName, BirthDate, Gender 
	--	HAVING COUNT(distinct PatientId) > 1 
	--) ME 

	------------------------------------------------------------------------------
	-- Step 2: Select Winning Patient Id (min value of patientId for all dupes of a patient) 
	------------------------------------------------------------------------------


				
			--Get a list of the patients that are potential candidates for matching. 
			SELECT   
			  PT.PatientId, 
			  inn.FirstName,
			  inn.LastName,
			  i.BirthDate, 
			  gt.DisplayValue as  Gender,
			  i.ExternalReferenceIdentifier,
			  i.OtherReferenceIdentifier,
			  I.SourceSystemId
			INTO #TMP1 
			 FROM 
				(
						SELECT 
							inn.LastName, inn.FirstName,
							i.BirthDate,
							gt.DisplayValue as Gender, 
							COUNT(DISTINCT PatientId) AS CNT
						FROM 
							vwportal_Patient  p WITH (NOLOCK) 
							INNER JOIN vwportal_Individual i  WITH (NOLOCK) ON i.IndividualId = p.IndividualId 
							INNER JOIN vwportal_IndividualName inn  WITH (NOLOCK) ON inn.IndividualId = i.IndividualId
							LEFT OUTER JOIN  vwPortal_GenderType gt  WITH (NOLOCK) ON gt.GenderTypeId = i.GenderTypeId
						WHERE p.DeleteInd = 0 
						GROUP BY  LastName, FirstName, BirthDate , gt.DisplayValue 
						HAVING COUNT(DISTINCT PatientId) > 1
				)  T, 

				vwportal_Patient  PT WITH (NOLOCK),  vwportal_IndividualName inn  WITH (NOLOCK) ,vwportal_Individual i  WITH (NOLOCK) 
				LEFT OUTER JOIN  vwPortal_GenderType gt  WITH (NOLOCK) ON gt.GenderTypeId = i.GenderTypeId
				WHERE i.IndividualId = PT.IndividualId  AND inn.IndividualId = i.IndividualId
				AND T.LastName = inn.LastName AND  T.FirstName = inn.FirstName AND  T.Gender = gt.DisplayValue AND T.BirthDate = i.BirthDate AND PT.DeleteInd = 0 


	
	--SELECT A.* 
	--INTO #TMP2
	--FROM
	--(
	--	SELECT FirstName, LastName,BirthDate,Gender, PatientId, 
	--	MIN(PatientID) OVER(PARTITION BY FirstName, LastName,BirthDate,Gender) AS newPatientId
	--	FROM 
	--	(
	--		SELECT DISTINCT A.FirstName, A.LastName, A.PatientId, A.Gender, A.BirthDate
	--		FROM  vwPatientDemographic A
	--		INNER JOIN #TMP1 B ON A.FirstName = B.FirstName
	--			AND A.LastName = B.LastName
	--			AND A.BirthDate = B.BirthDate
	--			AND A.Gender = B.Gender
	--		) M
	--) A 
	--WHERE PatientId <> newPatientId 

	


		
		--Out of the all of the candidate values, calculate the Levenstein distance 

		SELECT * INTO #TMP2 
		FROM (
		SELECT ME.FirstName inFName , ME.LastName inLastName, ME.BirthDate inDOB, 
		MIN(ME.PatientID) OVER(PARTITION BY ME.FirstName, ME.LastName, ME.BirthDate, ME.Gender) AS newPatientId,
			ISNULL(ME.ExternalReferenceIdentifier, ME.OtherReferenceIdentifier) AS inExternalId , 
			ME.SourceSystemId AS inSourceSystemId,
		  PT.PatientId,  --Drop patientid 
		  inn.FirstName,
		  inn.LastName,
		  i.BirthDate, 
		  gt.DisplayValue as Gender,	
		  i.ExternalReferenceIdentifier,
		  i.SourceSystemID
		  FROM 
			#TMP1 ME ,
			vwportal_Patient  PT WITH (NOLOCK), 
			vwportal_IndividualName inn  WITH (NOLOCK) ,
			vwportal_Individual i  WITH (NOLOCK) 
			LEFT OUTER JOIN  vwPortal_GenderType gt  WITH (NOLOCK) ON gt.GenderTypeId = i.GenderTypeId
			WHERE i.IndividualId = PT.IndividualId  AND inn.IndividualId = i.IndividualId
				AND ME.LastName = inn.LastName AND ME.FirstName = inn.FirstName AND ME.Gender = gt.DisplayValue AND ME.BirthDate = i.BirthDate AND PT.DeleteInd = 0 
				AND ME.PatientId < PT.PatientId
		) A 
		 





	

	------------------------------------------------------------------------------
	-- Step 3: Loop thru Target tables to be updated
	------------------------------------------------------------------------------

	DECLARE @DbLocation VARCHAR(100), @DependencyTable VARCHAR(100), @TablePk VARCHAR(100),  
			@TablePKDataType VARCHAR(100),
			@UpdatingColumn VARCHAR(100),@UpdatingColumnDataType VARCHAR(100), @ResolvingForId  VARCHAR(100),
			@DynSql NVARCHAR(MAX) = '', @TOBJECT_ID BIGINT, @COBJECT_ID BIGINT ,
			@ErrorMsg varchar(4000)


	DECLARE MyCur CURSOR FAST_FORWARD FOR 
	SELECT DbLocation, 
		TargetTable, 
		PrimaryKeyName,
		PatientIdColumn as UpdatingColumn  
	FROM  EdwReferenceData..DedupeTable
	WHERE   PrimaryKeyName is not null
	ORDER BY DbLocation ASC , TargetTable ASC 

	SET @DynSql = ''
	OPEN MyCur FETCH NEXT FROM MyCur INTO @DbLocation, @DependencyTable, @TablePk, @UpdatingColumn 	

	WHILE @@FETCH_STATUS = 0
	BEGIN




	
			IF @DbLocation = 'EDW'
			BEGIN
				SET @DbLocation = @EDWName
			END
			ELSE
			BEGIN 
				SET @DbLocation = @PortalName
			END

		-- Update Audit table
		-- sample 
		/*
	 		INSERT INTO [dbo].[DedupePatientIdAudit]
			   ([DedupeTableName]
			   ,[DedupeTablePKType]
			   ,[DedupeTablePKValue]
			   ,[OldPatientId]
			   ,[NewPatientId]
			   ,[Description]
			   ,[EventDateTime])
		select 'PatientEnrollment' , 
				'Int' , 
				TGT.PatientEnrollmentId, 
				SS.PatientId , 
				SS.NewPatientId , 
				null, 
				GetUTCDate()  
		from VfpLbPortalTest.dbo.PatientEnrollment TGT 
		inner join #TMP2 SS  
		ON TGT.PatientId = SS.PatientId
		*/



		--TEST TO see if the table actually exists in the database, if so, proceed.
		SET @DynSql = N'select @rowcount= COUNT(1) from ' + @DbLocation + '.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''' + @DependencyTable + ''''
		DECLARE @rowcount INT
		exec sp_executesql @DynSql, N'@rowcount int output', @rowcount output;
		IF @rowcount <> 0 
		BEGIN


			------------------------------------------------------------------------------
			-- Step 4: Keep an audit record of each statement being issued
			------------------------------------------------------------------------------

			SET @DynSql = '	INSERT INTO [dbo].[DedupePatientIdAudit]
				   ([DedupeTableName]
				   ,[DedupeTablePKType]
				   ,[DedupeTablePKValue]
				   ,[OldPatientId]
				   ,[NewPatientId]
				   ,[Description]
				   ,[EventDateTime])
			SELECT ''' + @DependencyTable + ''' , ''' + 'Int' + ''' ,' +
			' TGT.' + @TablePk + ', SS.PatientId , SS.NewPatientId , null, GetUTCDate() '  + 
			' from ' + @DbLocation + '.dbo.' + @DependencyTable + ' TGT INNER JOIN #TMP2 SS ' + 
			' ON TGT.' + @UpdatingColumn + ' = SS.PatientId'

			--SELECT (@DynSql)
			INSERT INTO DedupeExecuteLog ( errorMsg, createTime ) SELECT @DynSql, GetUTCDate();

			BEGIN TRY
				EXEC   (@DynSql) --run it
			END TRY
			BEGIN CATCH
					SET @ErrorMsg =  'Failed : ' + @DynSql + CAST(ERROR_LINE() AS VARCHAR(100))  + ' Message=' + 
				CAST(ERROR_MESSAGE() AS VARCHAR(1000))  + ' ErrorNumber=' + 
				CAST(ERROR_NUMBER() AS VARCHAR(100)) + ' Severity=' + 
				CAST(ERROR_SEVERITY() AS VARCHAR(100)) + ' ErrState=' + 
				CAST(ERROR_STATE() AS VARCHAR(100)) 

				INSERT INTO DedupeExecuteLog ( errorMsg, createTime ) SELECT @ErrorMsg, GetUTCDate();
			END CATCH

			------------------------------------------------------------------------------
			-- Step 5:  Issue the Update against each target table with winning Patient Id
			------------------------------------------------------------------------------
		
			/* sample 
			 UPDATE VfpLbPortalTest.dbo.PatientEnrollment SET PatientId = SS.newPatientId   
			FROM VfpLbPortalTest.dbo.PatientEnrollment PP INNER JOIN #TMP2 SS   ON PP.PatientId = SS.PatientId
			*/

			SET @DynSql = ' UPDATE ' + @DbLocation + '.dbo.' + @DependencyTable +  
						  ' SET ' + @UpdatingColumn + ' = SS.newPatientId  '  + --, ModifyDateTime = Getdate()
						  ' FROM ' + @DbLocation + '.dbo.' + @DependencyTable + ' PP INNER JOIN #TMP2 SS  ' + 
						  ' ON PP.' + @UpdatingColumn + ' = SS.PatientId'  
			--SELECT (@DynSql)  
			INSERT INTO DedupeExecuteLog ( errorMsg, createTime ) SELECT @DynSql, GetUTCDate();

			BEGIN TRY
				EXEC   (@DynSql) --run it
			END TRY
			BEGIN CATCH
					SET @ErrorMsg =  'Failed : ' + @DynSql + cast(ERROR_LINE() as varchar(100))  + ' Message=' + 
				CAST(ERROR_MESSAGE() as varchar(1000))  + ' ErrorNumber=' + 
				CAST(ERROR_NUMBER() as varchar(100)) + ' Severity=' + 
				CAST(ERROR_SEVERITY() as varchar(100)) + ' ErrState=' + 
				CAST(ERROR_STATE() as varchar(100)) 

				INSERT INTO DedupeExecuteLog ( errorMsg, createTime ) SELECT @ErrorMsg, GetUTCDate();
			END CATCH

		END --IF Rowcount

		FETCH NEXT FROM MyCur INTO @DbLocation, @DependencyTable, @TablePk, @UpdatingColumn 
	
	END  --WHILE
	CLOSE MyCur 
	DEALLOCATE MyCur 

	------------------------------------------------------------------------------
	-- Step 6:  Update those tables with DeleteInd flag
	------------------------------------------------------------------------------

	UPDATE vwPortal_Patient
		SET DeleteInd = 1
	FROM vwPortal_Patient P 
		JOIN #TMP2 T ON P.PatientId = T.PatientId

	--UPDATE vwPortal_Individual
	--	SET DeleteInd = 1
	--FROM vwPortal_Individual I 
	--	JOIN vwPortal_Patient P ON I.IndividualID = P.IndividualID
	--	JOIN #TMP2 T ON P.PatientId = T.PatientId

	--UPDATE vwPortal_IndividualName
	--	SET DeleteInd = 1
	--FROM vwPortal_IndividualName I 
	--	JOIN vwPortal_Patient P ON I.IndividualID = P.IndividualID
	--	JOIN #TMP2 T ON P.PatientId = T.PatientId

	UPDATE vwPortal_Party
		SET DeleteInd = 1
	FROM vwPortal_Party  PTY
		JOIN vwPortal_Individual I ON PTY.PartyId = I.PartyId
		JOIN vwPortal_Patient P ON I.IndividualID = P.IndividualID
		JOIN #TMP2 T ON P.PatientId = T.PatientId

	------------------------------------------------------------------------------
	-- Step 7:  Log all PatientIds de-duped into history table
	------------------------------------------------------------------------------
	INSERT INTO [dbo].[DedupedPatientIdHistory]
           ([FirstName]
           ,[LastName]
           ,[BirthDate]
           ,[Gender]
           ,[PatientId]
           ,[newPatientId]
           ,[createTime])	--** should change to CreateDateTime
	SELECT [FirstName]
           ,[LastName]
           ,[BirthDate]
           ,[Gender]
           ,[PatientId]
           ,[newPatientId]
		   ,GetUTCDate() 
	FROM #TMP2

	DROP TABLE #TMP1
	DROP TABLE #TMP2



		-- update maxSourceTimeStamp with max CreateDateTime of #PatientMedicationQue
		UPDATE [Maintenance]..[EDWTableLoadTracking]
		SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex, updatedatetime = GETUTCDATE()
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] =@dataSource  AND [EDWName] = @EDWName


		-- get total records from PatientMedication after merge 
		SELECT @RecordCountAfter= (SELECT COUNT(1)  FROM [DedupedPatientIdHistory] WITH (NOLOCK));

		-- Calculate records inserted and updated counts
		SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
		SET @UpdatedRecord = 0

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;



	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH



END 
 


GO
