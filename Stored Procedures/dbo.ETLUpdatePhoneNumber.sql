SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create  PROCEDURE [dbo].[ETLUpdatePhoneNumber] 
AS


BEGIN

/* ================================================================
	 Author:		Youping Liu
	 Create date:   2016-05-02 
	 Description:	 Update patient phone number infromation


	Version		Date		Author	Change
	-------		----------	------	------	
				2016-05-02  YL			Re-write the phone number update process
				2016-08-08  YL			Comment update process when merge key matched, no need to do update   
				2016-10-24  YL LBETL-152	 Invalid due to eixting logic using PatientIdsRegistry (mismatched family ID)
											Modification to use [vwPortal_Patient] usinfg lbpatientID instead of family ID;
	2.1.2		2016-11-29	YL	LBETL-248 Fix extra space in the phone number between area code and phone number
	2.1.2		2016-12-15	YL	LBETL-291 Fix cell phone not shown due to NULL value in Phone1Description
	2.1.2		2016-02-04	CL	LBETL-743 Fixed issue in which the source system id of 2 is hard coded
	2.1.2		2016-02-04	CL	LBETL-922 Fixed Merge issue in which there a NULL sourcesystem values
===============================================================*/




	SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'vwPortal_IndividualTelephoneNumber';
	DECLARE @dataSource VARCHAR(20) = 'TelephoneNumber';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLUpdatePhoneNumber';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @LBUSERID            INT=1
	DECLARE @CurrentDateTime     DATETIME=GETUTCDATE()
	DECLARE @DeleteInd           BIT=0
	
	---------------------------------------------------

	SELECT @RecordCountBefore=COUNT(1)
	FROM vwPortal_IndividualTelephoneNumber;
	----------------------------------------------------------------------------
	-- Update Telephone Number on Portal
	----------------------------------------------------------------------------
	--SELECT family_id, idtype, min(idValue) Idvalue
	--INTO  #PatientIdsRegistry
	--FROM PatientIdsRegistry
	--WHERE idtype = 'Individualid'
	--GROUP BY Family_Id, idtype


		BEGIN TRY
		---Step 1. add Individual phone number from MemberExtract
				SELECT MedicareNo AS ExternalReferenceIdentifier
						,vp.IndividualID AS IndividualId, 
						LEFT(Homephone,3)  AS AreaCode , 
						SUBSTRING(REPLACE(REPLACE(HOmephone,'-',''),' ',''),4,10) AS PhoneNumber ,
						 case when LEN(REPLACE(REPLACE(HOmephone,'-',''),' ',''))>10 then substring(REPLACE(REPLACE(HOmephone,'-',''),' ',''),11,10) ELSE '' END AS Extension 
						 ,t.TelephoneNumberId
						 ,MedicareNo AS OtherReferenceIdentifier, SourceSystemId
						 ,row_number() over(Partition by vp.IndividualID, t.TelephoneNumberId order by MedicareNo  ) r
				INTO #tmpIndividualPhone
				FROM  MemberExtract ME
						--INNER JOIN #PatientIdsRegistry PIR ON ME.family_id = PIR.Family_Id AND PIR.IDType = 'IndividualId'
                	INNER JOIN [dbo].[vwPortal_Patient] vP ON vP.PatientId=Me.LbPatientId 

						LEFT OUTER JOIN vwPortal_TelephoneNumber t on t.AreaCode = LEFT(Homephone,3) 
						AND t.PhoneNumber = SUBSTRING(REPLACE(REPLACE(HOmephone,'-',''),' ',''),4,10) 
							AND ISNULL(t.Extension,'')=case when LEN(REPLACE(REPLACE(HOmephone,'-',''),' ',''))>10 then substring(REPLACE(REPLACE(HOmephone,'-',''),' ',''),11,10) ELSE '' END
							AND t.PhoneTypeId= 1
				WHERE   LEN(homephone) > 3 and holdback is NULL;

		-- Step 2. dedup select one record for each member and phonenumber ID
					 SELECT externalreferenceidentifier, 
								IndividualId,
								TelephoneNumberId,
								SourceSystemId,  --LBETL-743
								--1 AS PhoneTypeId, 
								--0 AS ServiceCarrierId, 
								--1 AS CountryCode,
								AreaCode, 
								PhoneNumber,
								Extension,
								OtherReferenceIdentifier
								--0 AS DeleteInd, @CurrentDateTime AS CreateDatetime, @CurrentDateTime AS ModifyDatetime, 
								--@LBUSERID AS CreateLbUserId, @LBUSERID AS ModifyLbUserid
					 INTO #IndividualPhone
					 FROM #tmpIndividualPhone WHERE r=1;

		---Step 3. merge new phone number from MemberExtract to vwPortal_TelephoneNumber 
		      create index IDX_Phonenumber on #IndividualPhone (AreaCode ,PhoneNumber , Extension) ;


				SELECT DISTINCT t.[TelephoneNumberId], 
						i.AreaCode , 
						i.PhoneNumber , 
						i.Extension 
				INTO #Member_phone
				FROM  #IndividualPhone i 
				left outer join vwPortal_TelephoneNumber t on t.AreaCode = i.AreaCode 
						AND t.PhoneNumber = i.PhoneNumber
							AND ISNULL(t.Extension,'')=i.Extension 
							AND t.PhoneTypeId= 1

				--for speed merge
				CREATE CLUSTERED INDEX  IDX_Member_phone_PhoneID ON #Member_phone (TelephoneNumberId);


				MERGE INTO vwPortal_TelephoneNumber AS target
				USING #Member_phone AS source
					ON target.[TelephoneNumberId] = source.[TelephoneNumberId]
				WHEN MATCHED THEN
					UPDATE SET DeleteInd = 0, ModifyDateTime = @CurrentDateTime, ModifyLBUserId = 1
				WHEN NOT MATCHED BY TARGET THEN
					INSERT (PhoneTypeId, ServiceCarrierId, CountryCode, AreaCode, PhoneNumber, Extension, DeleteInd, CreateDateTime, ModifyDateTime, CreateLBUserId, ModifyLBUserId)
					VALUES (1,0,1,source.AreaCode,source.PhoneNumber,source.Extension,0,@CurrentDateTime,@CurrentDateTime,1,1);


			   SELECT DISTINCT  externalreferenceidentifier, 
								IndividualId,
								t.TelephoneNumberId,
								SourceSystemId,			--LBETL-743
								1 AS PhoneTypeId, 
								0 AS ServiceCarrierId, 
								1 AS CountryCode,
								i.AreaCode, 
								i.PhoneNumber,
								i.Extension,
								i.OtherReferenceIdentifier,
								0 AS DeleteInd, @CurrentDateTime AS CreateDatetime, @CurrentDateTime AS ModifyDatetime, 
								@LBUSERID AS CreateLbUserId, @LBUSERID AS ModifyLbUserid
				INTO #IndividualPhone2
				FROM  #IndividualPhone i 
				inner join vwPortal_TelephoneNumber t ON t.AreaCode = i.AreaCode 
						AND t.PhoneNumber = i.PhoneNumber
							AND ISNULL(t.Extension,'')=i.Extension 
							AND t.PhoneTypeId= 1
							AND i.SourceSystemId IS NOT NULL

        			--for speed merge
				CREATE CLUSTERED INDEX IDX_IndividualPhonePhoneID ON #IndividualPhone2 (IndividualId,TelephoneNumberId);

		----Step 4.  Merge individual phone number from Member_Extract to vwPortal_IndividualTelephoneNumber
				MERGE INTO vwPortal_IndividualTelephoneNumber AS target
				USING   #IndividualPhone2    AS Source
				ON target.IndividualId = source.IndividualId AND target.TelephoneNumberId = source.TelephoneNumberId 
	    
				WHEN MATCHED THEN
					UPDATE SET DeleteInd = 0, ModifyDateTime =@CurrentDateTime, ModifyLbUserId = 1
				WHEN NOT MATCHED BY TARGET THEN
					INSERT (IndividualId, TelephoneNumberId, SourceSystemId, DeleteInd, createDateTime,ModifyDateTime,CreateLBUserId, ModifyLBUserId, ExternalReferenceIdentifier, OtherReferenceIdentifier)
					VALUES (source.IndividualId,source.TelephoneNumberId, source.SourceSystemId
					,0,@CurrentDateTime,@CurrentDateTime,1,1,source.ExternalReferenceIdentifier,source.OtherReferenceIdentifier);


		----Step 5. Phone number from Interface
          
				  SELECT p.IndividualId, 
						ifs.SourceSystemId, 
						i.ExternalReferenceIdentifier, 
						i.OtherReferenceIdentifier,
						replace(ip.Phone1,' ','') Phone1,
						left(LTRIM(ip.[Phone1Description]),1) Phone1Description,
						replace(ip.Phone2, ' ','') Phone2,
						left(LTRIM(ip.[Phone2Description]),1) Phone2Description
						,row_number() over(Partition by i.IndividualId order by ip.[CreateDateTime] desc ) r
				  INTO #tmpInterfaceMemberPhone
				  FROM vwPortal_InterfacePatient ip
				  --INNER JOIN vwPortal_InterfacePatientMatched ipm on  ip.InterfaceSystemId = ipm.InterfaceSystemId AND ip.PatientIdentifier = ipm.PatientIdentifier
				  INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ip.InterfaceSystemId
				  INNER JOIN dbo.PatientIdReference idRef ON idRef.SourceSystemId = ifs.SourceSystemId 
						  AND idRef.ExternalId = IP.PatientIdentifier 
						  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
						  AND idRef.DeleteInd =0
							INNER JOIN vwPortal_Patient p ON idRef.LbPatientId = p.PatientId and p.DeleteInd =0
							INNER JOIN  vwPortal_Individual i ON p.IndividualId = i.IndividualId  and i.DeleteInd =0
				  WHERE ISNULL(ip.Phone1,'') <> ''  AND ( LEN(ip.Phone1Description)>0  or LEN(ip.Phone2Description)>0 )
				  AND left(ip.Phone1,10) NOT IN ('0000000000','1111111111','2222222222','3333333333','4444444444','5555555555',
				 '6666666666','7777777777','8888888888','9999999999')
				 

				 SELECT IndividualId, 
						SourceSystemId, 
						ExternalReferenceIdentifier, OtherReferenceIdentifier,
						REPLACE(REPLACE(REPLACE(REPLACE(Phone1,'+1',''),'(',''),')',''),'-','') Phone1,
						CASE Phone1Description WHEN 'H' THEN 1 WHEN 'W' THEN 2 WHEN 'M' THEN 3  WHEN 'F' 
							THEN 4 ELSE 0 END AS  PhoneTypeId,
						 REPLACE(REPLACE(REPLACE(REPLACE(Phone2,'+1',''),'(',''),')',''),'-','') Phone2,
						CASE Phone2Description WHEN 'H' THEN 1 WHEN 'W' THEN 2 WHEN 'M' THEN 3  WHEN 'F' 
							THEN 4 ELSE 0 END AS  PhoneTypeId2
				 INTO  #InterfaceMemberPhone
				 FROM #tmpInterfaceMemberPhone
				 where r=1;


				SELECT  DISTINCT PhoneTypeId,
						left(Phone1,20) AS WholePhone
				INTO #tmpInterfacePhone		
				FROM  #InterfaceMemberPhone
				UNION
				SELECT  DISTINCT PhoneTypeId2,
						left(Phone2,20) AS WholePhone
				FROM #InterfaceMemberPhone
				WHERE ISNULL(Phone2,'') <> ''  ;
 

                create index IDX_Phonenumber on #tmpInterfacePhone	 ( PhoneTypeId ,WholePhone) ;

				SELECT DISTINCT  t.[TelephoneNumberId], i.PhoneTypeId, 
					   SUBSTRING(WholePhone,1,3) AS AreaCode,
					   SUBSTRING(WholePhone,4,7) AS PhoneNumber,  
					   Case when LEN(WholePhone)>10 then substring(	WholePhone,11,10) ELSE '' END AS  Extension 
				INTO #InterfacePhone
				FROM  #tmpInterfacePhone i
				left outer join vwPortal_TelephoneNumber t on t.AreaCode = SUBSTRING(WholePhone,1,3)
						AND t.PhoneNumber = SUBSTRING(WholePhone,4,7)
							AND ISNULL(t.Extension,'')=Case when LEN(WholePhone)>10 then substring(	WholePhone,11,10) ELSE '' END
							AND t.PhoneTypeId= i.PhoneTypeId
				;
			  CREATE CLUSTERED INDEX IDX_Interface_phone_PhoneID on #InterfacePhone (TelephoneNumberId);

			   MERGE INTO vwPortal_TelephoneNumber AS target
				USING  #InterfacePhone AS source
					ON target.[TelephoneNumberId] = source.[TelephoneNumberId]
				WHEN MATCHED THEN
					UPDATE SET DeleteInd = 0, ModifyDateTime = @CurrentDateTime, ModifyLBUserId = 1
				WHEN NOT MATCHED BY TARGET THEN
					INSERT (PhoneTypeId, ServiceCarrierId, CountryCode, AreaCode, PhoneNumber, Extension, DeleteInd, CreateDateTime, ModifyDateTime, CreateLBUserId, ModifyLBUserId)
					VALUES (source.PhoneTypeId,0,1,source.AreaCode,source.PhoneNumber,source.Extension,0,@CurrentDateTime,@CurrentDateTime,1,1);


		---Step 6. add Individual phone number from Interface
			
				 select i.IndividualId, 
						i.SourceSystemId, 
						i.ExternalReferenceIdentifier, i.OtherReferenceIdentifier,
						i.Phone1,
						i.PhoneTypeId,
						t.TelephoneNumberId
				 INTO  #InterfaceMemberPhone2
				 FROM  #InterfaceMemberPhone i
				 inner join vwPortal_TelephoneNumber t on (t.AreaCode + t.PhoneNumber + ISNULL(t.Extension,'')) = i.Phone1
				 and t.PhoneTypeId=i.PhoneTypeId
                                  WHERE ISNULL(Phone1,'') <> ''
				 UNION
				  select i.IndividualId, 
						i.SourceSystemId, 
						i.ExternalReferenceIdentifier, i.OtherReferenceIdentifier,
						i.Phone2,
						i.PhoneTypeId2,
						t.TelephoneNumberId
				 FROM  #InterfaceMemberPhone i
				 inner join vwPortal_TelephoneNumber t on (t.AreaCode + t.PhoneNumber + ISNULL(t.Extension,'')) = i.Phone2
				 and t.PhoneTypeId=i.PhoneTypeId2
                                  WHERE ISNULL(Phone2,'') <> ''
			


				--for speed merge
				CREATE CLUSTERED INDEX IDX_InterfaceMemberPhone_PhoneID on #InterfaceMemberPhone2  (IndividualId,TelephoneNumberId);

				MERGE INTO vwPortal_IndividualTelephoneNumber AS target
				USING #InterfaceMemberPhone2 AS Source
					   ON target.IndividualId = source.IndividualId AND target.TelephoneNumberId = source.TelephoneNumberId 
			           
				WHEN MATCHED THEN
					UPDATE SET DeleteInd = 0, ModifyDateTime = @CurrentDateTime, ModifyLbUserId = 1
				WHEN NOT MATCHED BY TARGET THEN
					INSERT (IndividualId, TelephoneNumberId, SourceSystemId, DeleteInd, createDateTime,ModifyDateTime,CreateLBUserId, ModifyLBUserId, ExternalReferenceIdentifier, OtherReferenceIdentifier)
					VALUES (source.IndividualId,source.TelephoneNumberId, source.SourceSystemId
					,0,@CurrentDateTime,@CurrentDateTime,1,1,source.ExternalReferenceIdentifier,source.OtherReferenceIdentifier);




		-- get total records from [dbo].PatientMedication after merge 
					 SELECT @RecordCountAfter=COUNT(1)
						FROM vwPortal_IndividualTelephoneNumber;

					  -- Calculate records inserted and updated counts
						SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
					  --  SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

					  -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
					   SET  @EndTime=GETUTCDATE();
					   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	    

		---last to clean temp tables
			DROP TABLE   #IndividualPhone,#InterfacePhone,#tmpIndividualPhone,#tmpInterfacePhone
			,#Member_phone,#InterfaceMemberPhone,#InterfaceMemberPhone2,#tmpInterfaceMemberPhone,#IndividualPhone2;



			END TRY
		BEGIN CATCH
				   --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
					SET  @EndTime=GETUTCDATE();
					SET  @ErrorNumber =ERROR_NUMBER();
					SET  @ErrorState =ERROR_STATE();
					SET  @ErrorSeverity=ERROR_SEVERITY();
					SET  @ErrorLine=ERROR_LINE();
					SET  @Comment =left(ERROR_MESSAGE(),400);
					EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
		END CATCH

END

GO
