SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GetMeasureRankResults](
		@businessUnitId INT,
		@measureId INT,
		@loggedInUserId INT,
		@transactionalDbName VARCHAR(200) = NULL
	)

AS
BEGIN
	--------------------------------------------------------------------------------
	-- create temp tables to build up the status list
	--------------------------------------------------------------------------------
	CREATE TABLE #tmpBusinessUnits(
		BusinessUnitId INT
		UNIQUE CLUSTERED (BusinessUnitId)
	)

	CREATE TABLE #tmpRankingTable(
		GproPatientRankingId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	)


	CREATE TABLE #tmpGproPatientstatusTable(
			GProPatientMeasureId	INT,	--used to find the exact row in the GProPatientMeasure table this result refers to
			GProPatientRankingId	INT,	--used to find the exact row in the GProPatientRanking table this result refers to
			MeasureId				VARCHAR(8),
			MeasureName				VARCHAR(2000),
			MeasureStatus			VARCHAR(155),
			MeasureRank				INT,
			MedicareHicn			VARCHAR(15)
	)

	--------------------------------------------------------------------------------
	-- find the name of the measure being displayed based on the id
	--------------------------------------------------------------------------------
	DECLARE @measureName varchar(20)
	SELECT @measureName= gmt.Name FROM  [GproMeasureType] gmt  WHERE gmt.GproMeasureTypeId = @measureId

	--------------------------------------------------------------------------------
	-- fill the temp table with the patients ranked for this measure and the status
	--------------------------------------------------------------------------------
	IF (@measureName = 'CARE-2')
	BEGIN
	  INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
	  SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [CareFallsConfirmed] IN (15, 19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [CareFallsConfirmed] = 2 AND [FallsScreening] IS NOT NULL AND [FallsScreening] IN (1,2,4) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.CareFallsRank,
			gpr.MedicareHicn 
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'CARE-2'
		WHERE  gpm.CareFallsRank IS NOT NULL AND gpm.CareFallsRank<> 0  
	END
	ELSE IF (@measureName = 'CARE-3')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [CareMeddocConfirmed] IN (7, 15, 19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [CareMeddocConfirmed] = 2 
					AND NOT EXISTS (SELECT 1 FROM GproPatientMedication med WHERE med.GproPatientMeasureId = gpm.GproPatientMeasureId AND (med.CareMeddocVisitConfirmed IS NULL OR (med.CareMeddocVisitConfirmed = 2 AND med.MedicationDocumented IS NULL))) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.CareMeddocRank,
			gpr.MedicareHicn 
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'CARE-3'
		WHERE gpm.CareMeddocRank IS NOT NULL AND gpm.CareMeddocRank<> 0 
	END
	ELSE IF (@measureName = 'CAD-7')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [CadConfirmed] IN (8, 15, 19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [CadConfirmed] = 2 AND [CadDiabetesLvsd] = 1 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [CadConfirmed] = 2 AND [CadDiabetesLvsd] = 2 AND [CadAceArb] IS NOT NULL AND [CadAceArb] IN (1, 2, 4, 5, 6) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.CadRank,
			gpr.MedicareHicn 
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'CAD-7'
		WHERE gpm.CadRank IS NOT NULL AND gpm.CadRank<> 0 
	END
	ELSE IF (@measureName = 'DM-2')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [DmConfirmed] IN (8, 15, 19, 20) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [DmConfirmed] = 2 AND [DmHba1cTest] IN (1, 17) THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [DmConfirmed] = 2 AND [DmHba1cTest] = 2 AND [DmHba1cDate] IS NOT NULL AND [DmHba1cValue] >=0 AND [DmHba1cValue] < 26 THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.DmRank,
			gpr.MedicareHicn 
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'DM-2'
		WHERE gpm.DmRank IS NOT NULL AND gpm.DmRank<> 0 
	END
	ELSE IF (@measureName = 'DM-7')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE
				--WHEN [MedicalRecordFound] IS NULL
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [DmConfirmed] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [DmConfirmed] = 2 AND [DmEyeExam] IN (1, 2) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.DmRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'DM-7'
		WHERE gpm.DmRank IS NOT NULL AND gpm.DmRank<> 0 
	END
	ELSE IF (@measureName = 'HF-6')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue,  
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [HfConfirmed] IN (8, 15, 19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [HfConfirmed] = 2 AND [HfLvsd] = 1 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [HfConfirmed] = 2 AND [HfLvsd] = 2 AND [HfBetaBlocker] IS NOT NULL AND [HfBetaBlocker] IN (1,2,4,5,6) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.HfRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'HF-6'
		WHERE  gpm.HfRank IS NOT NULL AND gpm.HfRank<> 0 
	END
	ELSE IF (@measureName = 'HTN-2')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [HtnConfirmed] IN (8, 15, 17, 19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [HtnConfirmed] = 2 AND [HtnRecentBp] = 0 THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [HtnConfirmed] = 2 AND [HtnRecentBp] = 1 AND [HtnBpDate] IS NOT NULL AND [HtnBpSystolic] IS NOT NULL AND [HtnBpSystolic] >= 0 AND [HtnBpSystolic] <= 350 AND [HtnBpDiastolic] >= 0 AND [HtnBpDiastolic] <= 200 THEN 
				'Complete'
				ELSE 'Incomplete'
			END,
			gpm.HtnRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'HTN-2'
		WHERE gpm.HtnRank IS NOT NULL AND gpm.HtnRank<> 0 
	END
	ELSE IF (@measureName = 'IVD-2')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [IvdConfirmed] IN (8, 15, 19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [IvdConfirmed] = 2 AND [IvdAntithrombotic] IS NOT NULL AND [IvdAntithrombotic] IN (0, 1) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.IvdRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'IVD-2'
		WHERE  gpm.IvdRank IS NOT NULL AND gpm.IvdRank<> 0 
	END
	ELSE IF (@measureName = 'MH-1')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [MhConfirmed] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [MhConfirmed] = 2 AND [MhIndexPerformed] = 1 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [MhConfirmed] = 2 AND [MhIndexPerformed] = 2 AND [MhIndexTest] = 1 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [MhConfirmed] = 2 AND [MhIndexPerformed] = 2 AND [MhIndexTest] = 2 AND [MhIndexDate] IS NOT NULL AND [MhIndexScore] IS NOT NULL 
					AND [MhFollowupPerformed] = 1 THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [MhConfirmed] = 2 AND [MhIndexPerformed] = 2 AND [MhIndexTest] = 2 AND [MhIndexDate] IS NOT NULL AND [MhIndexScore] IS NOT NULL 
					AND [MhFollowupPerformed] = 2 AND [MhFollowupTest] = 1 THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [MhConfirmed] = 2 AND [MhIndexPerformed] = 2 AND [MhIndexTest] = 2 AND [MhIndexDate] IS NOT NULL AND [MhIndexScore] IS NOT NULL 
					AND [MhFollowupPerformed] = 2 AND [MhFollowupDate] IS NOT NULL AND [MhFollowupScore] IS NOT NULL THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.MhRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'MH-1'
		WHERE gpm.MhRank IS NOT NULL AND gpm.MhRank<> 0 
	END
	ELSE IF (@measureName = 'PREV-5')
	BEGIN 
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcMammogramConfirmed] IN (15,17,18,19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcMammogramConfirmed] = 2 AND [PcMammogramPerformed] IS NOT NULL AND [PcMammogramPerformed] IN (0, 1) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.PcMammogramRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'PREV-5'
		WHERE gpm.PcMammogramRank IS NOT NULL AND gpm.PcMammogramRank<> 0 
	END
	ELSE IF (@measureName = 'PREV-6')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcColorectalConfirmed] IS NOT NULL AND [PcColorectalConfirmed] IN (15,17,19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcColorectalConfirmed] = 2 AND [PcColorectalPerformed] IS NOT NULL AND [PcColorectalPerformed] IN (0,1) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.PcColorectalRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'PREV-6'
		WHERE gpm.PcColorectalRank IS NOT NULL AND gpm.PcColorectalRank<> 0 
	END
	ELSE IF (@measureName = 'PREV-7')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcFluShotConfirmed] IS NOT NULL AND [PcFluShotConfirmed] IN (15,17,19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcFluShotConfirmed] = 2 AND [PcFluShotReceived] IS NOT NULL AND [PcFluShotReceived] IN (1,2,4,5,6) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.PcFluShotRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'PREV-7'
		WHERE gpm.PcFluShotRank IS NOT NULL AND gpm.PcFluShotRank<> 0 
	END
	ELSE IF (@measureName = 'PREV-8')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcPneumoShotConfirmed] IN (15,19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcPneumoShotConfirmed] = 2 AND [PcPneumoShotReceived] IS NOT NULL AND [PcPneumoShotReceived] IN (0,1) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.PcPneumoShotRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'PREV-8'
		WHERE gpm.PcPneumoShotRank IS NOT NULL AND gpm.PcPneumoShotRank<> 0 
	END
	ELSE IF (@measureName = 'PREV-9')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcBmiScreenConfirmed] IN (15,17,19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcBmiScreenConfirmed] = 2 AND [PcBmiCalculated] IS NOT NULL AND [PcBmiCalculated] IN (1,4,5) THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [PcBmiScreenConfirmed] = 2 AND [PcBmiCalculated] =2 AND [PcBmiNormal] = 1 THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [PcBmiScreenConfirmed] = 2 AND [PcBmiCalculated] =2 AND [PcBmiNormal] = 0 AND [PcBmiPlan] IS NOT NULL AND [PcBmiPlan] IN (0,1) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.PcBmiScreenRank,gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'PREV-9'
		WHERE gpm.PcBmiScreenRank IS NOT NULL AND gpm.PcBmiScreenRank<> 0 
	END
	ELSE IF (@measureName = 'PREV-10')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcTobaccoConfirmed] IN (15,19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcTobaccoConfirmed] = 2 AND [PcTobaccoScreen] IS NOT NULL AND [PcTobaccoScreen] IN (1,4,14) THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [PcTobaccoConfirmed] = 2 AND [PcTobaccoScreen] = 2 AND [PcTobaccoCounsel] IS NOT NULL AND [PcTobaccoCounsel] IN (0,1) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.PcTobaccoRank,gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'PREV-10'
		WHERE gpm.PcTobaccoRank IS NOT NULL AND gpm.PcTobaccoRank<> 0 
	END
	ELSE IF (@measureName = 'PREV-11')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcBloodPressureConfirmed] IN (15,17,19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcBloodPressureConfirmed] = 2 AND [PcBloodPressureScreen] IS NOT NULL AND [PcBloodPressureScreen] IN (1,4,5) THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [PcBloodPressureConfirmed] = 2 AND [PcBloodPressureScreen] = 2 AND [PcBloodPressureNormal] = 1 THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [PcBloodPressureConfirmed] = 2 AND [PcBloodPressureScreen] = 2 AND [PcBloodPressureNormal] = 0 AND [PcBloodPressureFollowUp] IS NOT NULL AND [PcBloodPressureFollowUp] IN (0,1) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.PcBloodPressureRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'PREV-11'
		WHERE  gpm.PcBloodPressureRank IS NOT NULL AND gpm.PcBloodPressureRank<> 0 
	END
	ELSE IF (@measureName = 'PREV-12')
	BEGIN
		INSERT INTO #tmpGproPatientstatusTable 
			(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
		 SELECT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
			CASE 
				WHEN [MedicalRecordFound] <> 2 THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcDepressionConfirmed] IN (15,17,19) THEN 'Skipped'
				WHEN [MedicalRecordFound] = 2 AND [PcDepressionConfirmed] = 2 AND [PcDepressionScreen] IS NOT NULL AND [PcDepressionScreen] IN (1,4,5) THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [PcDepressionConfirmed] = 2 AND [PcDepressionScreen] = 2 AND [PcDepressionPositive] = 1 THEN 'Complete'
				WHEN [MedicalRecordFound] = 2 AND [PcDepressionConfirmed] = 2 AND [PcDepressionScreen] = 2 AND [PcDepressionPositive] = 2 AND [PcDepressionPlan] IS NOT NULL AND [PcDepressionPlan] IN (1,2) THEN 'Complete'
				ELSE 'Incomplete'
			END,
			gpm.PcDepressionRank,
			gpr.MedicareHicn
		FROM [GproPatientMeasure] gpm 
			INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
			INNER JOIN [GproMeasureType] gmt ON gmt.Name = 'PREV-12'
		WHERE gpm.PcDepressionRank IS NOT NULL AND gpm.PcDepressionRank<> 0 
	END
	
	--------------------------------------------------------------------------------
	-- Find the transactional db to figure out which patients are allowed to be viewed for this business unit
	--------------------------------------------------------------------------------
	DECLARE @tranDbName VARCHAR(200);
	SET @tranDbName = LTRIM(RTRIM(@transactionalDbName));

	IF ISNULL(@tranDbName, '') = ''
	BEGIN
		DECLARE @edwDbName VARCHAR(200) = DB_NAME();

		--SELECT @tranDbName = LTRIM(RTRIM([Tx])) FROM Maintenance.Dbo.MetaSiteInfo WHERE Edw = @edwDbName;

		--IF ISNULL(@tranDbName, '') = ''
		--BEGIN
			SET @tranDbName = REPLACE(@edwDbName,'Edw','Lbportal') 
		--END
	END

	DECLARE @sql NVARCHAR(4000)
	DECLARE @useGproAttribution SMALLINT = 0;
	DECLARE @params nvarchar(500);

	SET @sql = N'SELECT @attributionPref = (CASE WHEN SetValue = ''true'' THEN 1 ELSE 0 END) 
		FROM '+ @tranDbName +'..SystemSettings WHERE SettingType = ''GPro'' AND SettingParameter = ''UseGproAttribution'''
	SET @params='@attributionPref smallint OUTPUT'
	EXEC sp_executesql @sql, @params, @attributionPref=@useGproAttribution OUTPUT

	IF (@useGproAttribution = 1)
	BEGIN
		SET @sql = 'INSERT INTO #tmpRankingTable
			SELECT gpr.GproPatientRankingId
			FROM GproPatientRanking gpr
			WHERE gpr.ClinicIdentifier IN
				(SELECT bu.ExternalCode FROM vwPortal_BusinessUnit bu WHERE bu.DeleteInd <> 1 AND bu.BusinessUnitId IN (SELECT BusinessUnitId FROM ' + @TranDbName + '..[fnGetHierarchy] (' + CAST(@businessUnitId AS VARCHAR(20)) + ')))';			
		EXEC (@sql)
	END
	ELSE
	BEGIN
		SET @sql = 'INSERT INTO #tmpRankingTable
			SELECT DISTINCT gpr.GproPatientRankingId 
			FROM GproPatientRanking gpr
				INNER JOIN vwPortal_PatientHealthcareOrg pho ON pho.PatientId = gpr.LbPatientId
				INNER JOIN vwPortal_HealthcareOrg ho ON ho.HealthcareOrgId = pho.HealthcareOrgId
					AND ho.BusinessUnitId IN (SELECT BusinessUnitId FROM ' + @tranDbName + '..[fnGetHierarchy] (' + cast(@businessUnitId AS VARCHAR(20)) + '))'
		EXEC (@sql)
	END

	--SET @sql = 'INSERT INTO #tmpBusinessUnits 
	--			SELECT DISTINCT hbuv.BusinessUnitId
	--			FROM '+ @tranDbName +'..HierarchyBusinessUnitView hbuv
	--			WHERE hbuv.LBUserId=' + cast(@LoggedInUserId AS VARCHAR(20))
	--EXEC (@sql)

	--SET @query = 'INSERT INTO #tmpPatientTable 
	--			  SELECT DISTINCT pat.PatientId FROM #tmpBusinessUnits v
	--			  INNER JOIN '+ @tranDbName +'..HealthcareOrg h ON v.BusinessUnitId = h.BusinessUnitId
	--			  INNER JOIN '+ @tranDbName +'..PatientHealthcareOrg pho ON h.HealthcareOrgId = pho.HealthcareOrgId
	--			  INNER JOIN '+ @tranDbName +'..Patient pat ON pat.PatientId = pho.PatientId
	--			  WHERE v.BusinessUnitID IN (SELECT BusinessUnitId FROM ' + @tranDbName + '..[fnGetHierarchy] (' + cast(@businessUnitId AS VARCHAR(20)) + '))'
	--EXEC (@query)

	--------------------------------------------------------------------------------
	-- return the rows as output
	--------------------------------------------------------------------------------
	IF NOT EXISTS (SELECT TOP 1 1 FROM vwPortal_BusinessUnit bu WHERE bu.BusinessUnitId = @businessUnitId AND bu.ParentBusinessUnitId IS NULL)
	BEGIN
		SELECT	DISTINCT  patRank.GProPatientRankingId
				, patRank.FirstName AS FirstName
				, patRank.LastName AS LastName
				, patRank.BirthDate AS BirthDate
				, patRank.GenderCode AS GenderCode
				, patRank.MedicareHicn AS MedicareHicn
				, tmpGproStatus.MeasureStatus AS GProStatusTypeId
				, rankMeasureType.MeasureRank AS MeasureRank
		FROM GproPatientRanking patRank
			INNER JOIN GproPatientRankingGproMeasureType rankMeasureType ON patRank.GProPatientRankingId = rankMeasureType.GproPatientRankingId
			INNER JOIN #tmpRankingTable tmpPat ON patRank.GproPatientRankingId = tmpPat.GproPatientRankingId
			INNER JOIN #tmpGproPatientstatusTable tmpGproStatus  ON patRank.MedicareHicn=tmpGproStatus.MedicareHicn
		WHERE rankMeasureType.GproMeasureTypeId = @measureId
	END
	ELSE
	BEGIN
		SELECT	DISTINCT  patRank.GProPatientRankingId
				, patRank.FirstName AS FirstName
				, patRank.LastName AS LastName
				, patRank.BirthDate AS BirthDate
				, patRank.GenderCode AS GenderCode
				, patRank.MedicareHicn AS MedicareHicn
				, tmpGproStatus.MeasureStatus AS GProStatusTypeId
				, rankMeasureType.MeasureRank AS MeasureRank
		FROM GproPatientRanking patRank
			INNER JOIN GproPatientRankingGproMeasureType rankMeasureType ON patRank.GProPatientRankingId = rankMeasureType.GproPatientRankingId
			INNER JOIN #tmpGproPatientstatusTable tmpGproStatus  ON patRank.MedicareHicn=tmpGproStatus.MedicareHicn
		WHERE rankMeasureType.GproMeasureTypeId = @measureId
	END

	--------------------------------------------------------------------------------
	-- cleanup
	--------------------------------------------------------------------------------
	DROP Table #tmpBusinessUnits
	DROP Table #tmpRankingTable
	DROP Table #tmpGproPatientstatusTable

END
GO
