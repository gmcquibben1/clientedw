SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==================================================
-- Author:		Lan Ma
-- Create date: 2015-12-18
-- Description:	set MSSP patient privder attribution
-- Modified By: Lan Ma
-- Modified Date: 2016-04-01
-- Modified By: Youping
-- Modified Date: 2016-04-21  add LbPatientId IS NOT NULL
-- =================================================


CREATE PROCEDURE [dbo].[internalAttributionMSSP]

AS
BEGIN	

update vwPortal_ContractAttributionPriority set SourceSystemId = 1 
where AttributiontypeID = 4 AND SourceSystemId <> 1

exec InternalDropTempTables 

--Step 1: update individual NPI field in MemberRosterArchives table & FILEDATE
--UPDATE MRA SET [IndividualNPI] = PT.[Individual NPI] FROM MemberRosterArchives MRA
--JOIN MemberRosterArchives_ProviderTab PT 
--ON MRA.ACO = PT.ACO AND MRA.LbPatientId = PT.LbPatientId AND MRA.FileTimeline = PT.FileTimeline 
--and MRA.[ACO Participant TIN Number] = TRY_CONVERT(int,PT.[ACO Participant TIN Number])
--AND (isnull(MRA.[IndividualNPI],0) <> PT.[Individual NPI] )

UPDATE MemberRosterArchives SET FileDate = TRY_CONVERT(DATE, Timeline) FROM MemberRosterArchives WHERE FileDate IS NULL

--rule 1: the provider who is associated with a TIN that the member recently visited most will win
--rule 2: if multi providers found by rule 1, then the provider who is in PCP category will win
--rule 3: if still multi providers tie after applying rule 1 & 2, then choose provider by NPI alphabatically

SELECT DISTINCT RTRIM(LTRIM([NationalProviderIdentifier])) AS ProviderNPI
INTO #providerList
FROM [dbo].[vwPortal_ProviderActive] 

--step 1: find the provders that is associated with a Tin that a member recently visited most
SELECT *
INTO #memberTinProvider
FROM (
		SELECT 
			HICNO , LbPatientId, [ACO Participant TIN Number] ccn_tin_id, [IndividualNPI] AS ProviderNPI,t1.Timeline, [Count of Primary Care Services3] AS serviceCount, AssignmentFlag
      ,ROW_NUMBER() OVER(PARTITION BY HICNO ORDER BY t1.[Timeline] DESC, [Count of Primary Care Services3] DESC) rankNbr		    
		FROM MemberRosterArchives t1
		JOIN #providerList t2
		ON RTRIM(LTRIM(t1.IndividualNPI)) = t2.ProviderNPI
       -- WHERE MRA.StatusDesc <> 'Dropped'
	   WHERE LbPatientId IS NOT NULL 
            and TimeLine = (SELECT MAX(TimeLine) FROM MemberRosterArchives WHERE LbPatientId IS NOT NULL)
            and t1.StatusDesc <> 'Dropped'
	) X WHERE rankNbr = 1



--step 2: get provider count per member
SELECT  HICNO, LbPatientId, COUNT(*) AS ProvCount 
INTO #memberProviderCount
FROM #memberTinProvider  
GROUP BY HICNO, LbPatientId

--step 3: members who has a single provider found
SELECT 
HICNO, LbPatientId,
ProviderNPI
INTO #memberProviderList
FROM
( 	  	
SELECT 
MP.HICNO, MP.LbPatientId,ProviderNPI
FROM #memberTinProvider MP 
INNER JOIN #memberProviderCount MC
ON MP.LbPatientId = MC.LbPatientId  
WHERE MC.ProvCount = 1
) AS t
  
--step 4: members who has a multiple providers found	
INSERT INTO #memberProviderList (HICNO, LbPatientId, ProviderNPI)
SELECT HICNO, LbPatientId, ProviderNPI 
FROM (
		SELECT 
			MP.HICNO, MP.LbpatientId, MP.ProviderNPI,
			ROW_NUMBER() OVER(PARTITION BY MP.HICNO
								ORDER BY 
								CASE WHEN MP.AssignmentFlag = 1 AND  B.Specialty IS NOT NULL 
								     THEN 1 -- MAJOR SORT:- FIRST PRIORITY IS PROVIDER WHO CLEARS THE SPECIALITY IF PATIENT HAS AFLAG = 1
									 ElSE 2					 	
								END,
								MP.ProviderNPI -- MINOR SORT 
								) rowNbr
		FROM #memberTinProvider MP 
		INNER JOIN #memberProviderCount MC ON MP.HICNO = MC.HICNO  AND MC.ProvCount > 1 -- MULTIPLE PROVIDER PER PATIENT 
		LEFT JOIN NPI_Lookup B ON MP.ProviderNPI = B.NPI 
      AND B.Specialty 
        IN ('1','01','8','08','11','31', 'FP','GP','IM', '37', 'OBG','OBS','GYN','16','38','FPG','IMG','MDM', '50') 
		) t1 WHERE rowNbr = 1 
 


--step 5: load into patientProvider temp table
SELECT 
LbPatientId AS patientID,
MAX(COALESCE(providerID,0)) as providerID,
2 AS SourcesystemID,
MAX(HICNO) as ExternalReferenceIdentifier,
MAX(COALESCE(ProviderNPI,'0')) AS [OtherReferenceIdentifier],
4 as [AttributionTypeId]
INTO #patientProvider
FROM #memberProviderList t1
--JOIN [dbo].[PatientIdReference] t2
--ON t1.HICNO = t2.ExternalId
--AND t2.LbPatientId IS NOT NULL AND [IdTypeDesc] = 'Bene_Hic_Num'  and [DeleteInd] = 0
--JOIN [dbo].[vwPortal_Provider] t3
--ON t1.ProviderNPI = t3.[NationalProviderIdentifier]
--AND t3.[DeleteInd] = 0
JOIN [dbo].[vwPortal_ProviderActive] t3
ON t1.ProviderNPI = RTRIM(LTRIM(t3.[NationalProviderIdentifier]))
GROUP BY LbPatientId


--setp 6 :

INSERT INTO vwportal_patientProvider 
(PatientID, ProviderID,SourcesystemID,[ExternalReferenceIdentifier], [OtherReferenceIdentifier],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId],[AttributionTypeId]) 
	 SELECT S.PatientID, S.ProviderID,1,S.[ExternalReferenceIdentifier], S.[OtherReferenceIdentifier],0,GETDATE(),GETDATE(), 1,1,4
   FROM #patientProvider S
;

END
GO
