SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalVitalsStageArchival] 
    @LookbackDays INT = 0  ,
	@LookbackSince DateTime = NULL,
	@ClearStage BIT = 0
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

    SELECT @LookbackSince = CASE WHEN @LookbackSince IS NULL THEN GETDATE() ELSE @LookbackSince END 

	MERGE InterfacePatientVitalsArchive AS TARGET
	USING (
			SELECT 
			  [InterfacePatientVitalsId]
			  ,[InterfacePatientID]
			  ,[InterfaceSystemId]
			  ,[ServiceDateTime]
			  ,[HeightCM]
			  ,[HeightComments]
			  ,[WeightKG]
			  ,[WeightComments]
			  ,[TemperatureCelcius]
			  ,[TemperatureComments]
			  ,[Pulse]
			  ,[PulseComments]
			  ,[Respiration]
			  ,[RespirationComments]
			  ,[BloodPressureSystolic]
			  ,[BloodPressureDiastolic]
			  ,[BloodPressureComment]
			  ,[OxygenSaturationSP02]
			  ,[OxygenSaturationComment]
			  ,[Clinician]
			  ,[PatientVitalsComment]
			  ,[CreateDateTime]
		    FROM [dbo].vwPortal_InterfacePatientVitals (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			([InterfacePatientVitalsId],[InterfacePatientID],[InterfaceSystemId],[ServiceDateTime],[HeightCM],[HeightComments],[WeightKG],[WeightComments],[TemperatureCelcius]
			,[TemperatureComments],[Pulse],[PulseComments],[Respiration],[RespirationComments],[BloodPressureSystolic],[BloodPressureDiastolic],[BloodPressureComment],[OxygenSaturationSP02]
			,[OxygenSaturationComment],[Clinician],[PatientVitalsComment],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientVitalsId] = SOURCE.[InterfacePatientVitalsId] 
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientVitalsId]		=Source.[InterfacePatientVitalsId],
			[InterfacePatientID]			=Source.[InterfacePatientID],
			[InterfaceSystemId]				=Source.[InterfaceSystemId],
			[ServiceDateTime]				=Source.[ServiceDateTime],
			[HeightCM]						=Source.[HeightCM],
			[HeightComments]				=Source.[HeightComments],
			[WeightKG]						=Source.[WeightKG],
			[WeightComments]				=Source.[WeightComments],
			[TemperatureCelcius]			=Source.[TemperatureCelcius],
			[TemperatureComments]			=Source.[TemperatureComments],
			[Pulse]							=Source.[Pulse],
			[PulseComments]					=Source.[PulseComments],
			[Respiration]					=Source.[Respiration],
			[RespirationComments]			=Source.[RespirationComments],
			[BloodPressureSystolic]			=Source.[BloodPressureSystolic],
			[BloodPressureDiastolic]		=Source.[BloodPressureDiastolic],
			[BloodPressureComment]			=Source.[BloodPressureComment],
			[OxygenSaturationSP02]			=Source.[OxygenSaturationSP02],
			[OxygenSaturationComment]		=Source.[OxygenSaturationComment],
			[Clinician]						=Source.[Clinician],
			[PatientVitalsComment]			=Source.[PatientVitalsComment],
			[CreateDateTime]				=Source.[CreateDateTime]
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientVitalsId],[InterfacePatientID],[InterfaceSystemId],[ServiceDateTime],[HeightCM],[HeightComments],[WeightKG],[WeightComments],[TemperatureCelcius]
			,[TemperatureComments],[Pulse],[PulseComments],[Respiration],[RespirationComments],[BloodPressureSystolic],[BloodPressureDiastolic],[BloodPressureComment],[OxygenSaturationSP02]
			,[OxygenSaturationComment],[Clinician],[PatientVitalsComment],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientVitalsId],Source.[InterfacePatientID],Source.[InterfaceSystemId],Source.[ServiceDateTime],Source.[HeightCM],Source.[HeightComments],Source.[WeightKG],Source.[WeightComments],Source.[TemperatureCelcius]
			,Source.[TemperatureComments],Source.[Pulse],Source.[PulseComments],Source.[Respiration],Source.[RespirationComments],Source.[BloodPressureSystolic],Source.[BloodPressureDiastolic],Source.[BloodPressureComment],Source.[OxygenSaturationSP02]
			,Source.[OxygenSaturationComment],Source.[Clinician],Source.[PatientVitalsComment],Source.[CreateDateTime]) ;


	IF @ClearStage = 1
	BEGIN
		DELETE vwPortal_InterfacePatientVitals FROM vwPortal_InterfacePatientVitals A
		INNER JOIN InterfacePatientVitalsArchive B ON A.[InterfacePatientVitalsId] = B.[InterfacePatientVitalsId]
		WHERE A.[CreateDateTime] >= GetDate() - @LookbackDays OR A.[CreateDateTime] > @LookbackSince
	END
		


GO
