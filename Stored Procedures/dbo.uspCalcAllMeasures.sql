SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[uspCalcAllMeasures] @RunType INT = 1
-- @RunType = 1 FOR INCREMENTAL , @RunType = 2 FOR FULL 
-- How to run examples :- EXEC [dbo].[uspCalcAllMeasures] @RunType = 1 OR EXEC [dbo].[uspCalcAllMeasures] @RunType = 2
/*
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2017-01-12		YL			LBETL-460	add a step to call procedure InternalCMeasureDetailHistory


*/
AS 
DECLARE @VarLookbackTimestamp DATETIME

SET @VarLookbackTimestamp = GETDATE() - 2
IF @RunType IN (2 ,99)
BEGIN
  SET @VarLookbackTimestamp = '2000-01-01 00:00:00' 
END

-- add step to call InternalCMeasureDetailHistory
IF EXISTS(SELECT NAME FROM sysobjects WHERE NAME = 'InternalCMeasureDetailHistory' AND xtype = 'P')
BEGIN
   exec InternalCMeasureDetailHistory
END

UPDATE OrgHierarchy
SET LastUpdatedTimestamp = PFT.FeedTimestamp
FROM [OrgHierarchy] O 
INNER JOIN [PatientIdReference] PIR ON O.BENE_HIC_NUM = PIR.ExternalId AND PIR.IdTypeDesc = 'BENE_HIC_NUM'
INNER JOIN 
	(SELECT PatientId, MAX([FeedTimestamp]) [FeedTimestamp] FROM 
	[PatientFeedTimestamps] PFT 
	GROUP BY PatientId) PFT ON PIR.LbPatientId = PFT.PatientId 

DECLARE @ExecStatement NVARCHAR(MAX)
DECLARE MyCur CURSOR
FAST_FORWARD FOR 
SELECT --MeasureProgram,MeasureName , StoredProcedure,
	  ' Exec ' + StoredProcedure + + ' @LookbackTimestamp = ''' + CONVERT(VARCHAR(100),@VarLookbackTimestamp,120) + ''' ' ExecStatement , StoredProcedure
FROM 
CMeasure_Definitions cdef
where cdef.measureProgram not in ('ACO Measures Program-2013','STAR Measure Program-2014') 
and cdef.StoredProcedure is not null AND cdef.StoredProcedure <> '' AND EnableFlag = 'Y'
and cdef.measureProgram = CASE when @RunType = 99 THEN 'ACO Measures Program-2014' ELSE cdef.measureProgram END 
order by MeasureProgram, StoredProcedure 

DECLARE @ProcName VARCHAR(100), @ExecMeasureProgram VARCHAR(100),@ExecMeasureName VARCHAR(100), @LogId INT

OPEN MyCur
FETCH NEXT FROM MyCur INTO @ExecStatement, @ProcName

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @ExecMeasureProgram = MeasureProgram, @ExecMeasureName = MeasureName FROM [dbo].[CMeasure_Definitions] WHERE StoredProcedure = @ProcName
	INSERT INTO Maintenance.DBO.MeasureProgramsSpExecutionLog   (ExecDatabase,ExecMeasureProgram,ExecMeasureName,ExecMeasureSPName,ExecStartedby)
														 VALUES (DB_NAME(),@ExecMeasureProgram,@ExecMeasureName,@ProcName,SUSER_SNAME())

	SELECT @LogId = SCOPE_IDENTITY()
	SELECT @ExecStatement
	EXECUTE SP_EXECUTESQL @ExecStatement

	UPDATE Maintenance.DBO.MeasureProgramsSpExecutionLog SET ExecFinishTime = GetDate() WHERE LogId = @LogId
FETCH NEXT FROM MyCur INTO @ExecStatement , @ProcName
END
CLOSE MyCur 
DEALLOCATE MyCur 

--IH 10/9/15
DELETE FROM [dbo].[CMeasure_Detail]
WHERE MeasureID IN (SELECT MeasureID FROM [dbo].CMeasure_Definitions WHERE enableFlag = 'N')




GO
