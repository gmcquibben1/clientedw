SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@PREV-8] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	---------------------------------------------------------------------------------------
	-- initialization, setting variables
	---------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];
	IF OBJECT_ID('tempdb..#ENC') IS NOT NULL 
		DROP TABLE [#ENC];

	DECLARE  @yearend datetime, @measureBeginDate datetime;

	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2015';

	--Establishing the Clinical Data sourcing indicators.
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	---------------------------------------------------------------------------------------
	--Initial Patient pooling, for patients that have ranking for the prev-8 measure.
	---------------------------------------------------------------------------------------
	SELECT r.LbPatientId,
		r.GProPatientRankingId,
		dateDiff(year,r.Birthdate,@measureBeginDate) as Age,
		r.Birthdate,
		GM.GproMeasureTypeID,
		GPM.PcPneumoShotRank,
		GPM.PcPneumoShotConfirmed,
		GPM.PcPneumoShotReceived,
		GPM.PcPneumoShotComments, 
		GETUTCDATE() as ModifyDateTime
	INTO   #RankedPatients --	Drop Table #RankedPatients
	FROM   GPROPATIENTRANKING r
	JOIN   GPROPATIENTRANKINGGPROMEASURETYPE xref ON xref.GproPatientRankingID = r.GproPatientRankingID
	JOIN   GproMeasureType GM ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
	JOIN   GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE  GM.Name = 'PREV-8' 
		AND (GPM.PcPneumoShotConfirmed IS NULL OR GPM.PcPneumoShotReceived IS NULL)

	---------------------------------------------------------------------------------------
	--Denominator, setting PcPneumoShotConfirmed
	---------------------------------------------------------------------------------------
	---CHECK IF PATIENT HAS ENCOUNTER DURING MEASURMENT YEAR AND CALC AGE AT THE TIME OF EARLIEST ENCOUNTER
	SELECT *
	INTO #ENC -- drop table #enc
	FROM (SELECT RP.LbPatientId, rp.age,  
		PP.ProcedureDateTime,
		ROW_NUMBER () OVER (PARTITION BY rp.LbPatientId ORDER by pp.ProcedureDateTime ASC) earliest_enc
		FROM #RANKEDPATIENTS RP
			JOIN dbo.patientProcedure PP ON RP.LbPatientId = PP.PATIENTID
			JOIN dbo.patientProcedureProcedureCode PPC ON pp.[PatientProcedureId] = ppc.[PatientProcedureId]
			JOIN dbo.ProcedureCode PC ON PPC.PROCEDURECODEID = PC.PROCEDURECODEID
			JOIN DBO.[GproEvaluationCode] HVSC ON PC.ProcedureCode = HVSC.CODE
							AND hvsc.[ModuleType]= 'PREV'
							AND hvsc.[ModuleIndicatorGPRO]='8'
							AND HVSC.[VariableName] IN   ('ENCOUNTER_CODE')
							AND PP.ProcedureDateTime >= @measureBeginDate --'1-1-2015'--
							AND PP.ProcedureDateTime < @yearend--'1-1-2016'--
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemId]    --PROCS
		GROUP BY RP.LbPatientId, rp.age,  PP.ProcedureDateTime
		) as sub
	WHERE sub.earliest_enc = 1
	
	--Age Check FOR PATIENT WITH ENCOUNTER YOUNGER than 65
    UPDATE RP
    SET    RP.PcPneumoShotConfirmed = '19' -- Not Confirmed - Age
    FROM #RankedPatients RP 
	  JOIN #ENC ENC ON RP.LbPatientId = ENC.LbPatientId
    WHERE RP.PcPneumoShotConfirmed IS NULL
		AND ENC.Age < 65

	--AGE confirmed 65 AND OLDER
    UPDATE RP
    SET    RP.PcPneumoShotConfirmed = '2' --yes
    FROM #RankedPatients RP 
		JOIN #ENC ENC ON RP.LbPatientId = ENC.LbPatientId
    WHERE RP.PcPneumoShotConfirmed IS NULL
		AND ENC.Age >= 65

	---------------------------------------------------------------------------------------
	--Numerator, setting PcPneumoShotReceived
	---------------------------------------------------------------------------------------

	--update numerator by procedure
	UPDATE rp
	SET rp.PcPneumoShotConfirmed = '2',
		rp.PcPneumoShotReceived = '2' -- yes
	FROM #RankedPatients rp
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = rp.LbPatientId
			--AND pp.ProcedureDateTime >= @measureBeginDate
			AND pp.ProcedureDateTime <= @yearend
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND gec.ModuleType= 'PREV'
			AND gec.ModuleIndicatorGPRO = '8'
			AND gec.VariableName = 'PNEUMO_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
	WHERE rp.PcPneumoShotReceived IS NULL
		AND (rp.PcPneumoShotConfirmed IS NULL OR rp.PcPneumoShotConfirmed = '2')

	--update numerator by immunization
	UPDATE rp
	SET rp.PcPneumoShotConfirmed = '2',
		rp.PcPneumoShotReceived = '2' -- yes
	FROM #RankedPatients rp
		INNER JOIN vwPatientImmunization imm ON imm.PatientId = rp.LbPatientId
			--AND imm.ServiceDate >= @measureBeginDate
			AND imm.ServiceDate <= @yearend
		INNER JOIN GproEvaluationCode gec ON gec.Code = imm.ImmunizationCode
			AND gec.ModuleType= 'PREV'
			AND gec.ModuleIndicatorGPRO = '8'
			AND gec.VariableName = 'PNEUMO_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = imm.SourceSystemId
	WHERE rp.PcPneumoShotReceived IS NULL
		AND (rp.PcPneumoShotConfirmed IS NULL OR rp.PcPneumoShotConfirmed = '2')

	----pneumonia
	--SELECT sub.LbPatientId,  sub.ProcedureDateTime
	--INTO #pneu -- drop table #pneu
	--FROM (SELECT 
	--		RP.LbPatientId,  
	--		PP.ProcedureDateTime,
	--		ROW_NUMBER () OVER (PARTITION BY rp.LbPatientId ORDER BY pp.ProcedureDateTime DESC) earliest_dt
	--	FROM #ENC RP
	--		JOIN dbo.patientProcedure PP ON RP.LbPatientId = PP.PATIENTID
	--		JOIN dbo.patientProcedureProcedureCode PPC ON pp.[PatientProcedureId] = ppc.[PatientProcedureId]
	--		JOIN dbo.ProcedureCode PC ON PPC.PROCEDURECODEID = PC.PROCEDURECODEID
	--		JOIN DBO.[GproEvaluationCode] HVSC ON PC.ProcedureCode = HVSC.CODE
	--			AND hvsc.[ModuleType]= 'PREV'
	--			AND hvsc.[ModuleIndicatorGPRO]='8'
	--			AND HVSC.[VariableName] IN   ('PNEUMO_CODE')
	--			AND PP.ProcedureDateTime >= @measureBeginDate --'1-1-2015'--
	--			AND PP.ProcedureDateTime  < @yearend--'1-1-2016'--
	--		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
	--		GROUP BY RP.LbPatientId,  PP.ProcedureDateTime
	--	) sub
	--WHERE sub.earliest_dt = 1 -- earliest date
	--ORDER by sub.LbPatientId
	---- flu confirmed
 --   UPDATE RP
 --   SET    RP.PcPneumoShotReceived = '2' -- yes
 --   FROM #RankedPatients RP 
	--	INNER JOIN #pneu pneu ON RP.LbPatientId = pneu.LbPatientId
	--WHERE RP.PcPneumoShotReceived IS NULL

	--------------------------------------------------------------------------------------------------------          
	-- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	UPDATE gpm
	SET gpm.PcPneumoShotConfirmed = gpr.PcPneumoShotConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcPneumoShotRank IS NOT NULL
		AND gpm.PcPneumoShotConfirmed IS NULL

	UPDATE gpm
	SET gpm.PcPneumoShotReceived = gpr.PcPneumoShotReceived, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcPneumoShotRank IS NOT NULL
		AND gpm.PcPneumoShotReceived IS NULL
		
END
		

	
GO
