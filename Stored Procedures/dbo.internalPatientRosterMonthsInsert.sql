SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==================================================
-- Author:		Alex Tran
-- Create date: 2012-12-18
-- Description:	Import Roster table into PatientRosterMomths format
--Modified By:
--Modified Date:
-- =================================================

CREATE PROCEDURE [dbo].[internalPatientRosterMonthsInsert] 
AS

BEGIN
exec InternalDropTempTables
-- drop table #pat

--drop table #PatientRosterMonths
CREATE TABLE #PatientRosterMonths(
	[Month] [datetime]  NULL,
	[SourceSystemID] [int] NULL,
	PatientExternalID [varchar](100) NULL,
	[FirstName] [varchar](100)  NULL,
	[LastName] [varchar](100)  NULL,
	[Gender] [smallint] NULL,
	[Birthdate] [datetime]  NULL,
	[MemberMonth] [date] NULL,
	[MemberMonthStatus] [varchar](50) NULL,
	[DeathDate] [datetime] NULL,
	[ProviderNPI] [varchar](10) NULL,
	[FileDate] [datetime] NULL,
	[SourceFeed] [varchar](100) NULL,
	[LbPatientId] [int] NULL,
)



-- update LbPatientID
UPDATE PatientRosterMonthsRaw SET LbPatientId = IDR.LbPatientId 
FROM PatientRosterMonthsRaw B 
JOIN PatientIdReference IDR ON B.PatientExternalID = IDR.ExternalId --and b.sourcesystemid = idr.sourcesystemid

--Get Most Recent
-- DROP TABLE #dedupe_temp
select DISTINCT LbPatientId,MemberMonth, max(ISNULL(FileDate, '1/1/1900')) as filedate
into #dedupe_temp
FROM PatientRosterMonthsRaw
group by LbPatientId,MemberMonth
--37091

--SELECT * FROM PatientRosterMonthsRaw where PatientExternalID = '000085300'

-- Remove duplicate
select DISTINCT AMC.* 
into #dedupe_temp2
from PatientRosterMonthsRaw AMC
JOIN #dedupe_temp DT ON AMC.LbPatientId = DT.LbPatientId and AMC.FileDate = DT.filedate
		and AMC.MemberMonth = DT.MemberMonth


--drop table #Months
select distinct MemberMonth Month
into #Months
from #dedupe_temp2
order by Month

--drop table #pat_Months
select distinct SourceSystemID,LbPatientId ,m.Month
into #pat_Months
from #dedupe_temp2 d
cross apply #Months m 
order by m.Month

-- Get the min Member month for each patient
--DROP TABLE #start
select distinct LbPatientId,min(MemberMonth) Start_dt
into #start
from #dedupe_temp2
group by LbPatientId


-- Get the max Member month for each patient
--DROP TABLE #end
select distinct LbPatientId,max(Month) end_dt
into #end
from #pat_Months
group by LbPatientId

--truncate table #PatientRosterMonths
-- Insert all records from #dedupe_temp2
INSERT INTO #PatientRosterMonths
           ([Month]
		   ,[SourceSystemID]
           ,PatientExternalID
           ,[FirstName]
           ,[LastName]
           ,[Gender]
           ,[Birthdate]
           ,MemberMonth
           ,[DeathDate]
           ,[ProviderNPI]
           ,[FileDate]
           ,[SourceFeed]
           ,[LbPatientId]
		   )
select distinct pm.Month
			,pm.[SourceSystemID]
           ,a.PatientExternalID
           ,a.[FirstName]
           ,a.[LastName]
           ,a.[Gender]
           ,a.[Birthdate]
           ,a.MemberMonth
           ,a.[DeathDate]
           ,a.[ProviderNPI]
           ,a.[FileDate]
           ,a.[SourceFeed]
           ,pm.[LbPatientId]
from #pat_Months pm
left join #dedupe_temp2 a on a.LbPatientId = pm.LbPatientId and a.MemberMonth = pm.Month
left join #start s on pm.LbPatientId = s.LbPatientId 
left join #end e on pm.LbPatientId = e.LbPatientId 
where pm.Month between s.Start_dt and e.end_dt
--and pm.LbPatientId = '000000006'
order by month, pm.LbPatientId 


--select *
--from #dedupe_temp2
--where LbPatientId = '000000006'
--order by month

--update pm set [MemberMonth] = NULL
----SELECT MemberMonth
--from #PatientRosterMonths pm
--WHERE pm.MemberMonth IN ('09/01/2014','10/01/2014','02/01/2015','03/01/2015')

-- update where there is not a Member month
update pm set [MemberMonthStatus] = 'DROPPED'
from #PatientRosterMonths pm
WHERE pm.MemberMonth is null

-- update where there is a Member month
update pm set [MemberMonthStatus] = 'Continuing'
from #PatientRosterMonths pm
WHERE pm.MemberMonth is not null
--Continuing

--select *  from #PatientRosterMonths where LbPatientId = '000000006'

update pm set [MemberMonthStatus] = 'NEW'
--select pm.LbPatientId, pm.MemberMonth, pm.MemberMonthStatus, s.*
from #PatientRosterMonths pm
join #start s on pm.LbPatientId = s.LbPatientId and pm.MemberMonth = s.Start_dt
--where pm.LbPatientId = '000085300'

--select * from #PatientRosterMonths  where LbPatientId = '000000006' order by month
 
--drop table #PatientRosterMonths_final
SELECT month,Cur.LbPatientId,Cur.MemberMonth, [MemberMonthStatus]
,LAG(Cur.MemberMonth) OVER (ORDER BY LbPatientId,MemberMonth) AS PrevMemberMonths
,LAG([MemberMonthStatus]) OVER (ORDER BY LbPatientId,month) AS PrevMemberMonthStatus
,LAG(LbPatientId) OVER (ORDER BY LbPatientId,month) AS PrevLbPatientId
into #PatientRosterMonths_final
FROM #PatientRosterMonths Cur 
--WHERE cur.SourceFeed is not null
order by Month,Cur.LbPatientId,Cur.MemberMonth, [MemberMonthStatus]

update a set MemberMonthStatus = 'NEW'
--select *
from #PatientRosterMonths_final a
where MemberMonthStatus = 'Continuing' and PrevMemberMonthStatus = 'DROPPED'
and LbPatientId  = PrevLbPatientId

update a set MemberMonthStatus = 'DROPPED_1'
--select *
from #PatientRosterMonths_final a
where MemberMonthStatus = 'DROPPED' and (PrevMemberMonthStatus = 'Continuing' or PrevMemberMonthStatus = 'NEW')
and LbPatientId  = PrevLbPatientId

--select *
--from #PatientRosterMonths_final a
--where LbPatientId = '000000006'
----and LbPatientId  = PrevLbPatientId
--order by Month,LbPatientId,MemberMonth, [MemberMonthStatus]

--select distinct a.Month
--			,a.[SourceSystemID]
--           ,a.PatientExternalID
--           ,a.[FirstName]
--           ,a.[LastName]
--           ,a.[Gender]
--           ,a.[Birthdate]
--           ,a.MemberMonth
--		   ,b.[MemberMonthStatus]
--           ,a.[DeathDate]
--           ,a.[ProviderNPI]
--           ,a.[FileDate]
--           ,a.[SourceFeed]
--           ,a.[LbPatientId]
--from #PatientRosterMonths a
--LEFT join #PatientRosterMonths_final b on a.LbPatientId = b.LbPatientId and a.Month = b.Month
--where a.LbPatientId = '000000006'

truncate table PatientRosterMonths

INSERT INTO PatientRosterMonths
           ([Month]
		   ,[SourceSystemID]
           ,PatientExternalID
           ,[FirstName]
           ,[LastName]
           ,[Gender]
           ,[Birthdate]
           ,MemberMonth
		   ,[MemberMonthStatus]
           ,[DeathDate]
           ,[ProviderNPI]
           ,[FileDate]
           ,[SourceFeed]
           ,[LbPatientId]
		   )
select distinct a.Month
			,a.[SourceSystemID]
           ,a.PatientExternalID
           ,a.[FirstName]
           ,a.[LastName]
           ,a.[Gender]
           ,a.[Birthdate]
           ,a.MemberMonth
		   ,b.[MemberMonthStatus]
           ,a.[DeathDate]
           ,a.[ProviderNPI]
           ,a.[FileDate]
           ,a.[SourceFeed]
           ,a.[LbPatientId]
from #PatientRosterMonths a
LEFT join #PatientRosterMonths_final b on a.LbPatientId = b.LbPatientId and a.Month = b.Month



END
GO
