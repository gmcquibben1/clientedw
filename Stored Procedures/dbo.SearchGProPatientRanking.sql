SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SearchGProPatientRanking] 
(
	@SearchInput NVARCHAR(25),
	@BusinessUnitId	INT,
	@LoggedInUserId INT,
	@TransactionalDbName VARCHAR(200) = NULL
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #tmpBusinessUnits(
		BusinessUnitId INT
		UNIQUE CLUSTERED (BusinessUnitId)
	)

	CREATE TABLE #tmpRankingTable(
		GproPatientRankingId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	)

	DECLARE @tranDbName VARCHAR(200);

	SET @tranDbName = LTRIM(RTRIM(@transactionalDbName));

	IF ISNULL(@tranDbName, '') = ''
	BEGIN
		DECLARE @edwDbName VARCHAR(200) = DB_NAME();

		--SELECT @tranDbName = LTRIM(RTRIM([Tx])) FROM Maintenance.Dbo.MetaSiteInfo WHERE Edw = @edwDbName;

		--IF ISNULL(@tranDbName, '') = ''
		--BEGIN
			SET @tranDbName = REPLACE(@edwDbName,'Edw','Lbportal') 
		--END
	END

	DECLARE @query NVARCHAR(4000)
	DECLARE @Where VARCHAR(4000) = '';
	DECLARE @useGproAttribution SMALLINT = 0;
	DECLARE @params nvarchar(500);

	SET @query = N'SELECT @attributionPref = (CASE WHEN SetValue = ''true'' THEN 1 ELSE 0 END) 
		FROM '+ @tranDbName +'..SystemSettings WHERE SettingType = ''GPro'' AND SettingParameter = ''UseGproAttribution'''
	SET @params='@attributionPref smallint OUTPUT'
	EXEC sp_executesql @query, @params, @attributionPref=@useGproAttribution OUTPUT

	IF (@useGproAttribution = 1)
	BEGIN
		SET @query = 'INSERT INTO #tmpRankingTable
			SELECT gpr.GproPatientRankingId
			FROM GproPatientRanking gpr
			WHERE gpr.ClinicIdentifier IN
				(SELECT bu.ExternalCode FROM vwPortal_BusinessUnit bu WHERE bu.DeleteInd <> 1 AND bu.BusinessUnitId IN (SELECT BusinessUnitId FROM ' + @TranDbName + '..[fnGetHierarchy] (' + CAST(@businessUnitId AS VARCHAR(20)) + ')))';			
		EXEC (@query)
	END
	ELSE
	BEGIN
		SET @query = 'INSERT INTO #tmpRankingTable
			SELECT DISTINCT gpr.GproPatientRankingId 
			FROM GproPatientRanking gpr
				INNER JOIN vwPortal_PatientHealthcareOrg pho ON pho.PatientId = gpr.LbPatientId
				INNER JOIN vwPortal_HealthcareOrg ho ON ho.HealthcareOrgId = pho.HealthcareOrgId
					AND ho.BusinessUnitId IN (SELECT BusinessUnitId FROM ' + @tranDbName + '..[fnGetHierarchy] (' + cast(@businessUnitId AS VARCHAR(20)) + '))'
		EXEC (@query)
	END

	--SET @sql = 'INSERT INTO #tmpBusinessUnits
	--			SELECT DISTINCT hbuv.BusinessUnitId
	--			FROM '+ @tranDbName +'..HierarchyBusinessUnitView hbuv
	--			WHERE hbuv.LBUserId=' + cast(@LoggedInUserId AS VARCHAR(20))

	--EXEC (@sql)

	--SET @query = 'INSERT INTO #tmpPatientTable 
	--			  SELECT DISTINCT pat.PatientId 
	--			  FROM #tmpBusinessUnits v
	--			  INNER JOIN '+ @tranDbName +'..HealthcareOrg h ON v.BusinessUnitId = h.BusinessUnitId
	--			  INNER JOIN '+ @tranDbName +'..PatientHealthcareOrg pho ON h.HealthcareOrgId = pho.HealthcareOrgId
	--			  INNER JOIN '+ @tranDbName +'..Patient pat ON pat.PatientId = pho.PatientId
	--			  WHERE v.BusinessUnitID IN (SELECT BusinessUnitId FROM ' + @tranDbName + '..[fnGetHierarchy] (' + cast(@businessUnitId AS VARCHAR(20)) + '))'

	--EXEC (@query)

	IF NOT EXISTS (SELECT TOP 1 1 FROM vwPortal_BusinessUnit bu WHERE bu.BusinessUnitId = @businessUnitId AND bu.ParentBusinessUnitId IS NULL)
	BEGIN
		-- Insert statements for procedure here
		SELECT gppr.GProPatientRankingId, gppr.MeasureProgramTypeId, gppr.GroupTin, gppr.LBPatientId, 
			gppr.LastName LastName, gppr.FirstName FirstName, gppr.MedicalRecordNumber, gppr.OtherIdentifier,
			gppr.ProviderNpi1, gppr.ProviderNpi2, gppr.ProviderNpi3, gppr.ClinicIdentifier, gppr.GeneralComments,
			gppr.CareMeddocRank, gppr.CareFallsRank, gppr.CadRank, gppr.DmRank, gppr.HfRank, gppr.HtnRank, gppr.IvdRank,
			gppr.PcMammogramRank, gppr.PcColorectalRank, gppr.PcFlushotRank, gppr.PcPneumoshotRank, gppr.PcbMiscreenRank,
			gppr.PcTobaccouseRank, gppr.PcBloodPressureRank, gppr.PcDepressionRank, GETDATE() CreateDateTime, GETDATE() ModifyDateTime,
			gppr.BirthDate BirthDate, gppr.MedicareHicn, gpst.DisplayValue GProStatusTypeId, (gppr.ProviderFirstName1 + ' ' + gppr.ProviderLastName1) Provider1,
			(gppr.ProviderFirstName2 + ' ' + gppr.ProviderLastName2) Provider2, (gppr.ProviderFirstName3 + ' ' + gppr.ProviderLastName3) Provider3,
			CASE gppr.GenderCode
				WHEN 1 THEN 'male'
				WHEN 2 THEN 'female'
				ELSE 'unknown'
			END AS GenderCode
		FROM GProPatientRanking gppr 
			INNER JOIN GProStatusType gpst ON gpst.GProStatusTypeId = gppr.GProStatusTypeId
		WHERE EXISTS (SELECT 1 FROM #tmpRankingTable tmp WHERE tmp.GproPatientRankingId = gppr.GproPatientRankingId)
			AND (gppr.LastName LIKE '%' +  @searchInput + '%' OR gppr.FirstName LIKE '%' + @searchInput + '%')
	END
	ELSE
	BEGIN
		-- Insert statements for procedure here
		SELECT gppr.GProPatientRankingId, gppr.MeasureProgramTypeId, gppr.GroupTin, gppr.LBPatientId, 
			gppr.LastName LastName, gppr.FirstName FirstName, gppr.MedicalRecordNumber, gppr.OtherIdentifier,
			gppr.ProviderNpi1, gppr.ProviderNpi2, gppr.ProviderNpi3, gppr.ClinicIdentifier, gppr.GeneralComments,
			gppr.CareMeddocRank, gppr.CareFallsRank, gppr.CadRank, gppr.DmRank, gppr.HfRank, gppr.HtnRank, gppr.IvdRank,
			gppr.PcMammogramRank, gppr.PcColorectalRank, gppr.PcFlushotRank, gppr.PcPneumoshotRank, gppr.PcbMiscreenRank,
			gppr.PcTobaccouseRank, gppr.PcBloodPressureRank, gppr.PcDepressionRank, GETDATE() CreateDateTime, GETDATE() ModifyDateTime,
			gppr.BirthDate BirthDate, gppr.MedicareHicn, gpst.DisplayValue GProStatusTypeId, (gppr.ProviderFirstName1 + ' ' + gppr.ProviderLastName1) Provider1,
			(gppr.ProviderFirstName2 + ' ' + gppr.ProviderLastName2) Provider2, (gppr.ProviderFirstName3 + ' ' + gppr.ProviderLastName3) Provider3,
			CASE gppr.GenderCode
				WHEN 1 THEN 'male'
				WHEN 2 THEN 'female'
				ELSE 'unknown'
			END AS GenderCode
		FROM GProPatientRanking gppr 
			INNER JOIN GProStatusType gpst ON gpst.GProStatusTypeId = gppr.GProStatusTypeId
		WHERE (gppr.LastName LIKE '%' +  @searchInput + '%' OR gppr.FirstName LIKE '%' + @searchInput + '%')
	END

    
END


GO
