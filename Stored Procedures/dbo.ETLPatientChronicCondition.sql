SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Youping
-- Create date: 2016-06-17
-- Description:	Load Interface PatientChronicCondition Data into EDW
--
-- Modified: 2016-06-21
-- @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
-- @fullLoadFlag =0 Merge date to the existing table
-- ===============================================================


CREATE PROCEDURE [dbo].[ETLPatientChronicCondition](@fullLoadFlag bit = 0)

AS
BEGIN	

      SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientChronicCondition';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientChronicCondition';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;


	DECLARE @Today date=GETUTCDATE();

    
    BEGIN TRY
	

		--drop table #patientDiagnosisQueue

			SELECT [InterfacepatientChronicConditionId],
			 idRef.LbPatientId AS PatientID
			,ISS.SourceSystemId
			,t1.[PrimaryCareProviderNPI]
			,LEFT( LTRIM(RTRIM(REPLACE([DiagnosisCode],'.',''))),20) DiagnosisCode
			,t1.[Description]
			,[DisplayOrder]
			,t1.[CreateDateTime]
			,DiagnosisCode AS DiagnosisCodeRaw
			INTO #PatientChronicConditionQue
			FROM [dbo].[vwPortal_InterfacePatientChronicCondition] t1	
			INNER JOIN [dbo].[vwPortal_InterfacePatient] IP ON t1.InterfacePatientId = IP.InterfacePatientId
			INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS ON IP.InterfaceSystemId = ISS.[InterfaceSystemId]
			INNER JOIN dbo.PatientIdReference idRef ON idRef.SourceSystemId = ISS.SourceSystemId 
				  AND idRef.ExternalId = IP.PatientIdentifier 
				  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
				  AND idRef.DeleteInd =0
		

	   CREATE NONCLUSTERED INDEX NCIX_#patientDiagnosis_DiagnosisCode ON #PatientChronicConditionQUE (DiagnosisCode) INCLUDE ( [PrimaryCareProviderNPI])


	   SELECT
		DiagnosisCodingSystemTypeID,
		RefCodingSystemName
		INTO #DiagnosisCodingSystemType
		FROM(
			SELECT
			DCST.DiagnosisCodingSystemTypeID,
			DCST.NAME AS RefCodingSystemName
			FROM [DiagnosisCodingSystemType] DCST 
			UNION 
			SELECT 
			DCST.DiagnosisCodingSystemTypeID,
			DCSTS.SynonymName AS RefCodingSystemName
			FROM [DiagnosisCodingSystemType] DCST 
			INNER JOIN [DiagnosisCodingSystemTypeSynonyms] DCSTS 
			ON DCST.DiagnosisCodingSystemTypeID = DCSTS.DiagnosisCodingSystemTypeID
		    ) t
          ;

		    -- merge with DiagnosisCode add new ones
			MERGE INTO DiagnosisCode AS target
			USING (SELECT * FROM 
					(SELECT DISTINCT
					dcst.DiagnosisCodingSystemTypeId AS CodingSystemId,
					pcc.DiagnosisCode,
					pcc.Description AS DiagnosisDescription,pcc.DiagnosisCodeRaw,
					ROW_NUMBER() OVER (PARTITION BY pcc.DiagnosisCode ORDER BY pcc.CreateDateTime DESC) r
				   FROM #PatientChronicConditionQue pcc
					INNER JOIN #DiagnosisCodingSystemType dcst ON dcst.RefCodingSystemName =  'ICD9' 
					WHERE pcc.DiagnosisCode IS NOT NULL AND pcc.DiagnosisCode <> '') dc
					WHERE dc.r = 1
			) AS source
			ON REPLACE(source.DiagnosisCode,'.','') = target.DiagnosisCode
			WHEN NOT MATCHED BY TARGET THEN
				INSERT (DiagnosisCodingSystemTypeId,DiagnosisCode,DiagnosisDescription,DiagnosisDescriptionAlternate,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,DiagnosisCodeRaw,DiagnosisCodeDisplay)
				VALUES (source.CodingSystemId,source.DiagnosisCode,source.DiagnosisDescription,source.DiagnosisDescription,0,GETUTCDATE(),GETUTCDATE(),1,1,source.DiagnosisCodeRaw,source.DiagnosisCodeRaw);


	   
	           -- dedup diagnosisCode
			   SELECT	  DiagnosisCode,
						  DiagnosisCodeID
				INTO #DiagnosisCode 		
				FROM ( SELECT [DiagnosisCodingSystemTypeId],
								LEFT( LTRIM(RTRIM(REPLACE([DiagnosisCode],'.',''))),20)  AS DiagnosisCode,
								DiagnosisCodeID,
								DeleteInd 
				 , ROW_NUMBER() OVER (PARTITION BY  [DiagnosisCode] ORDER BY DeleteInd, DiagnosisCodeID) AS rnum
				FROM [dbo].[DiagnosisCode]
		

				) a where rnum=1

        --SET @RecordCountSource=@@ROWCOUNT;
	
	    CREATE CLUSTERED INDEX CIX_#DiagnosisCode_DiagnosisCode ON #DiagnosisCode(DiagnosisCode) ;

		--- Truncate data
     IF (@fullLoadFlag=1)
	   BEGIN
	       Truncate table [dbo].[PatientChronicCondition];
       END
	
	   SELECT @RecordCountBefore=count(1) from PatientChronicCondition;

	   

		SELECT  PatientID
			 ,DiagnosisCodeId
		  	,ProviderId
			,[DisplayOrder]
			,0 AS [DeleteInd]
			,[CreateDateTime]
			,@Today [ModifyDateTime]
			,1 AS [CreateLBUserId]
		    ,1 AS [ModifyLBUserId]
			INTO #PatientChronicCondition
       FROM (
		SELECT 
			 t1.PatientID
			 ,t2.DiagnosisCodeId
		  	,prov.ProviderId
			,[DisplayOrder]
			,t1.[CreateDateTime]
			,ROW_NUMBER() OVER(PARTITION BY t1.PatientID,[DisplayOrder]
					ORDER BY t1.[InterfacepatientChronicConditionId] DESC ) AS rowNbr
			--
			FROM #PatientChronicConditionQue t1
			INNER JOIN #DiagnosisCode t2
			ON t2.[DiagnosisCode]=t1.[DiagnosisCode]
		    LEFT JOIN vwPortal_Provider prov ON LEFT(ISNULL(prov.NationalProviderIdentifier,''),25) =LEFT(ISNULL( [PrimaryCareProviderNPI],''),25)
		) a
		WHERE rowNbr=1
         ;
                  SET @RecordCountSource=@@ROWCOUNT;


           CREATE CLUSTERED INDEX CIX_#PatientID_DiagnosisCode ON #PatientChronicCondition([PatientId],[DisplayOrder]) ;


			MERGE INTO PatientChronicCondition AS target
			USING #PatientChronicCondition AS source
			ON source.PatientId = target.PatientId  AND 
				source.DisplayOrder = target.DisplayOrder
			WHEN MATCHED THEN
				UPDATE SET DiagnosisCodeId = source.DiagnosisCodeId, ProviderId = source.ProviderId, DisplayOrder = source.DisplayOrder, ModifyDateTime = GETUTCDATE()
			WHEN NOT MATCHED BY TARGET THEN
				INSERT (PatientId,DiagnosisCodeId,ProviderId,DisplayOrder,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)	
				VALUES (source.PatientId,source.DiagnosisCodeId,source.ProviderId,source.DisplayOrder,0,GETUTCDATE(),GETUTCDATE(),1,1);
	    	
              -- Calculate records inserted and updated counts
			    SELECT @RecordCountAfter=count(1) from PatientChronicCondition;
	            SET @InsertedRecord =@RecordCountAfter - @RecordCountBefore;
	            SET @UpdatedRecord=@RecordCountSource - @InsertedRecord
              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			   SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	      drop table #DiagnosisCodingSystemType, #DiagnosisCode, #PatientChronicCondition,#PatientChronicConditionQue
	
	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
 

END


GO
