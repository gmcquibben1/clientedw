SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLDiagnosisCode] 
AS


-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-03-23
-- Description:	Load DiagnosisCode Data into EDW
-- Modified By:  Youping
-- Modified Date: 2016-08-16
-- Modification: LBETL-35 using [EdwReferenceData].[dbo].[ICDMstr] to update 
--               diagnosis description.
-- ===============================================================

/*
-- ================================================================
	Version		Date		Author	Change
	-------		----------	------	------		
	2.1.2      2016-11-10   YL      LBETL-167 DIagnosis code display as xxx.xxx
              

*/


BEGIN	
        --get all codingSystemName and ID into temp table
		SELECT
		DiagnosisCodingSystemTypeID,
		RefCodingSystemName
		INTO #DiagnosisCodingSystemType
		FROM(
			SELECT
			DCST.DiagnosisCodingSystemTypeID,
			DCST.NAME As RefCodingSystemName
			FROM [DiagnosisCodingSystemType] DCST 
			UNION 
			SELECT 
			DCST.DiagnosisCodingSystemTypeID,
			DCSTS.SynonymName As RefCodingSystemName
			FROM [DiagnosisCodingSystemType] DCST 
			INNER JOIN [DiagnosisCodingSystemTypeSynonyms] DCSTS 
			ON DCST.DiagnosisCodingSystemTypeID = DCSTS.DiagnosisCodingSystemTypeID
		    ) t
 

	  --load new Diagnosis codes into DiagnosisCode table
		INSERT INTO [dbo].[DiagnosisCode]
			   ([DiagnosisCodingSystemTypeId]
			   ,[DiagnosisCode]
			   ,[DiagnosisCodeRaw]
			   ,[DiagnosisCodeDisplay]
			   ,[DiagnosisDescription]
			   ,[DiagnosisDescriptionAlternate]
			   ,[DeleteInd]
			   ,[CreateDateTime]
			   ,[ModifyDateTime]
			   ,[CreateLBUserId]
			   ,[ModifyLBUserId])
		SELECT --DISTINCT
		t.DiagnosisCodingSystemTypeID,
		REPLACE(t.DiagnosisCode ,'.',''),
		t.[DiagnosisCode],
		t.[DiagnosisCode],
		ISNULL(t.[DiagnosisDescription],''),
		ISNULL(t.[DiagnosisDescription],''),
		0,
		GETUTCDATE(),
		GETUTCDATE(),
		1,
		1
		FROM(
			SELECT t2.DiagnosisCodingSystemTypeID,
					t1.[DiagnosisCode],
					t1.[DiagnosisDescription],
					ROW_NUMBER() OVER(PARTITION BY t2.DiagnosisCodingSystemTypeID,LTRIM(RTRIM(REPLACE(t1.[DiagnosisCode],'.',''))) ORDER BY  InterfacePatientDiagnosisId DESC ) AS rowNbr
			FROM PatientDiagnosisProcessQueue t1
			INNER JOIN #DiagnosisCodingSystemType t2
			ON LTRIM(RTRIM(t1.[DiagnosisCodingSystemName])) = LTRIM(RTRIM(t2.RefCodingSystemName))
			LEFT JOIN [dbo].[DiagnosisCode] t3
			ON t2.[DiagnosisCodingSystemTypeID] = t3.[DiagnosisCodingSystemTypeID] 
			AND LTRIM(RTRIM(REPLACE(t1.[DiagnosisCode],'.',''))) = LTRIM(RTRIM(REPLACE(t3.[DiagnosisCode],'.',''))) -- remove the dot from both source and target data when joining tables
			WHERE t2.DiagnosisCodingSystemTypeId IS NOT NULL AND t3.DiagnosisCodingSystemTypeId IS NULL  
        ) t WHERE rowNbr = 1  -- same diagnosisCode can have different description, only pick one row to insert
	

		--UPDATE DiagnosisCode 
		--SET DiagnosisDescription = ICD9.shortdescription, 
		--DiagnosisDescriptionAlternate = ICD9.ShortDescription
		--FROM DiagnosisCode DC 
		--INNER JOIN ICD9Mstr ICD9 
		--ON DC.DiagnosisCode = ICD9.ICD9
		--WHERE DC.DiagnosisDescription LIKE 'DUMMY%' OR DiagnosisDescription = ''

		  update [DiagnosisCode]
          set [DiagnosisDescription]=b.SHORTDESCRIPTION
          from [dbo].[DiagnosisCode] a
		  INNER JOIN #DiagnosisCodingSystemType ds on ds.DiagnosisCodingSystemTypeID=a.DiagnosisCodingSystemTypeId and ds.RefCodingSystemName='ICD9' 
          inner join [EdwReferenceData].[dbo].[ICDMstr] b on a.DiagnosisCode=b.ICDCode and [icdtype]='9'
          where isnull(a.[DiagnosisDescription],'')= ''

		  update [DiagnosisCode]
          set [DiagnosisDescription]=b.SHORTDESCRIPTION
          from [dbo].[DiagnosisCode] a
		  INNER JOIN #DiagnosisCodingSystemType ds on ds.DiagnosisCodingSystemTypeID=a.DiagnosisCodingSystemTypeId and ds.RefCodingSystemName='ICD10'
          inner join [EdwReferenceData].[dbo].[ICDMstr] b on  a.DiagnosisCode=b.ICDCode and [icdtype]='10'
          where isnull(a.[DiagnosisDescription],'')= ''

		  -- update snomed description
		   update [dbo].[DiagnosisCode]
			set [DiagnosisDescription]=left(b.[Description],250)
			from [dbo].[DiagnosisCode] a
			inner join [EdwReferenceData].[dbo].[SnomedCode] b on a.DiagnosisCode=b.SnomedCode
			  where isnull(a.[DiagnosisDescription],'')='' and len(a.[DiagnosisCode])>5


  --      UPDATE diagnosiscode 
		--SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 3) + '.' + right(DiagnosisCodeDisplay, 2) 
  --      WHERE DiagnosisCodeDisplay LIKE '[0-9]%' AND LEN(DiagnosisCodeDisplay) = 5 AND DiagnosisCodeDisplay NOT LIKE '%.%'

  --      UPDATE diagnosiscode 
		--SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 3) + '.' + right(DiagnosisCodeDisplay, 1) 
  --      WHERE DiagnosisCodeDisplay LIKE '[0-9]%' AND LEN(DiagnosisCodeDisplay) = 4 AND DiagnosisCodeDisplay NOT LIKE '%.%'

		---- V CODES
		--UPDATE diagnosiscode 
		--SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 3) + '.' + right(DiagnosisCodeDisplay, 2) 
		--WHERE DiagnosisCodeDisplay LIKE '[v]%'   AND LEN(DiagnosisCodeDisplay) = 5 AND DiagnosisCodeDisplay NOT LIKE '%.%'

		--UPDATE diagnosiscode 
		--SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 3) + '.' + right(DiagnosisCodeDisplay, 1) 
		--WHERE DiagnosisCodeDisplay LIKE '[v]%'   AND LEN(DiagnosisCodeDisplay) = 4 AND DiagnosisCodeDisplay NOT LIKE '%.%'

		---- E CODES
		UPDATE diagnosiscode 
		SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 4) + '.' + right(DiagnosisCodeDisplay, 1) 
		WHERE DiagnosisCodeDisplay like '[e]%'   AND LEN(DiagnosisCodeDisplay) = 5 AND DiagnosisCodeDisplay NOT LIKE '%.%'

		--ICD9 AND ICD10
		UPDATE diagnosiscode 
		SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 3) + '.' + substring(DiagnosisCodeDisplay,4,4) 
		WHERE LEN(DiagnosisCodeDisplay) between 4 and 7 AND CHARINDEX('.', DiagnosisCodeDisplay, 1) =0
		;


END

GO
