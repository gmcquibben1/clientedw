SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ================================================================
-- Author:		Youping
-- Create date: 2016-07-12
-- Description:	Load Interface PatientPregnancyHistory Data into EDW
-- @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
-- @fullLoadFlag =0 Merge date to the existing table
-- ===============================================================

-- Modified Youping on 2016-07-28
-- Add procedure running log to [Maintenance].[dbo].[EdwProcedureRunLog]
-- ===============================================================
-- Modified Youping on 2016-08-15
-- LBETL-45 remove date recorded as part of dedup key.
-- ===============================================================

Create PROCEDURE [dbo].[ETLPatientPregnancyHistory](@fullLoadFlag bit = 0)

AS
BEGIN	
 
    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientPregnancyHistory';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientPregnancyHistory';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY

  		
			--set @fullLoadFlag =1
	
			IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] =@EDWName)
			  BEGIN
			  
					 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed

			  END

			IF @fullLoadFlag = 1
			  BEGIN
				UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				   SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME)
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
			  END

			DECLARE @lastDateTime DATETIME = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
											FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
											WHERE [EDWtableName] = @EDWtableName 
											AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	



			
			SELECT  [InterfacePatientPregnancyHistoryId]
				,ISS.SourceSystemId
				,IP.PatientIdentifier
				,DateRecorded 
				,[PregnancyNumber] 
				,[GestationalAge] 
				,[EstimatedDeliveryDate] 
				,[DeliveryType] 
				,[BirthWeight] 
				,[EpisiotomyInd] 
				,[EpisiotomyType] 
				,[EpisiotomyPerinealDegree] 
				,[EpisiotomyLaceration] 
				, [FinalDeliveryDate] 
				,IPH.[CreateDateTime]
				INTO #PatientPregnancyHistoryQue	
					  
					FROM [dbo].[vwPortal_InterfacePatientPregnancyHistory] IPH with (nolock)
					INNER JOIN [dbo].[vwPortal_InterfacePatient] IP with (nolock) ON IPH.InterfacePatientId = IP.InterfacePatientId
					INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) ON IPH.InterfaceSystemId = ISS.[InterfaceSystemId]
					WHERE IPH.CreateDateTime >= @lastDateTime 
				
				;




              --- add Index
			  Create  NONCLUSTERED INDEX IDX_PatientIdentifier_SourceSystemId ON #PatientPregnancyHistoryQue (PatientIdentifier,SourceSystemId) INCLUDE ([PregnancyNumber],DateRecorded,[BirthWeight]  );
            			
				
	-----   dedup data prepare for Merge
			 SELECT * 
			 INTO #PatientPregnancyHistory
			 FROM (
				SELECT  [InterfacePatientPregnancyHistoryId]
						,PH.SourceSystemId
						, idRef.LbPatientId AS PatientID
				,DateRecorded 
				,[PregnancyNumber] 
				,[GestationalAge] 
				,[EstimatedDeliveryDate] 
				,[DeliveryType] 
				,[BirthWeight] 
				,[EpisiotomyInd] 
				,[EpisiotomyType] 
				,[EpisiotomyPerinealDegree] 
				,[EpisiotomyLaceration] 
				, [FinalDeliveryDate] 
				,PH.[CreateDateTime]
					, ROW_NUMBER() OVER ( PARTITION BY 	 idRef.LbPatientId--, DateRecorded 
				,ISNULL([PregnancyNumber],''),ISNULL([BirthWeight],0)
					ORDER BY 	InterfacePatientPregnancyHistoryId DESC ) RowNbr
					FROM  #PatientPregnancyHistoryQue PH with (nolock)
					 INNER JOIN dbo.PatientIdReference idRef with (nolock) ON idRef.SourceSystemId = PH.SourceSystemId 
					  AND idRef.ExternalId = PH.PatientIdentifier 
					  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
					  AND idRef.DeleteInd =0
				
				) A
				WHERE A.RowNbr=1

			--- Truncate data
			 IF (@fullLoadFlag=1)
			   BEGIN
				   Truncate table [dbo].[PatientPregnancyHistory];
			   END
	
			  CREATE CLUSTERED INDEX CIX_#PatientID_DiagnosisCode ON #PatientPregnancyHistory([PatientId],[PregnancyNumber],[BirthWeight] ) ;

			SELECT @RecordCountBefore=COUNT(1)
			FROM [dbo].[PatientPregnancyHistory];


			SELECT @RecordCountSource=COUNT(1)
			FROM #PatientPregnancyHistory;	    

			  --- test select * from #PatientPregnancyHistory


				MERGE [DBO].PatientPregnancyHistory AS [target]
				USING #PatientPregnancyHistory AS [source] 

				ON ( target.PatientID =[source].PatientID  
				 AND ISNULL(target.[PregnancyNumber],'') =ISNULL([source].[PregnancyNumber],'')
				 AND ISNULL(target.[BirthWeight],0) =ISNULL([source].[BirthWeight] ,0)
					)
                WHEN MATCHED  THEN UPDATE SET 
					target.[DateRecorded]		= source.[DateRecorded]
					,target.[GestationalAge]				= source.[GestationalAge]
					,target.[EstimatedDateOfDelivery]			= source.[EstimatedDeliveryDate]
					,target.[DeliveryType]		= source.[DeliveryType]
					--,target.[BirthWeight]		= source.[BirthWeight]
					,target.[EpisiotomyInd]		= source.[EpisiotomyInd]
					,target.[EpisiotomyTypeCode]		= source.[EpisiotomyType]
					,target.[EpisiotomyPerinealDegreeCode]		= source.[EpisiotomyPerinealDegree]
					,target.[EpisiotomyLacerationCode]		= source.[EpisiotomyLaceration]
					,target.[FinalDeliveryDate]				= source.[FinalDeliveryDate]
					,target.[ModifyDateTime]		= @Today
					
WHEN NOT MATCHED THEN
INSERT ([PatientId],[DateRecorded],
		[PregnancyNumber],[GestationalAge],[EstimatedDateOfDelivery],
	    [DeliveryType],[BirthWeight],[EpisiotomyInd],[EpisiotomyTypeCode],[EpisiotomyPerinealDegreeCode],[EpisiotomyLacerationCode],
		[FinalDeliveryDate],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId] )
VALUES (source.[PatientId],source.[DateRecorded],
		source.[PregnancyNumber],source.[GestationalAge],source.[EstimatedDeliveryDate],
	    source.[DeliveryType],source.[BirthWeight],source.[EpisiotomyInd],source.[EpisiotomyType],source.[EpisiotomyPerinealDegree],source.[EpisiotomyLaceration],
		source.[FinalDeliveryDate],	0,source.[CreateDateTime],@Today,1,1);

	    
		

		    -- update lastdate
               		   
			 -- update maxSourceTimeStamp with max CreateDateTime of #PatientPregnancyHistoryQue
			 UPDATE [Maintenance]..[EDWTableLoadTracking]
			 SET [maxSourceTimeStampProcessed] = (SELECT MAX(CreateDateTime) FROM #PatientPregnancyHistoryQue ),
				 [updateDateTime] =GETUTCDATE()
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


					
		   -- get total records from PatientPregnancyHistory after merge 
		    			SELECT @RecordCountAfter=COUNT(1)
			            FROM PatientPregnancyHistory;

              -- Calculate records inserted and updated counts
	             SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	             SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			   SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
			  --clean
			  drop table  #PatientPregnancyHistory,#PatientPregnancyHistoryQue
	
 	END TRY

		BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END


GO
