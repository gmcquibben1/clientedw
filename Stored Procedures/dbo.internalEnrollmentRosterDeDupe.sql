SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalEnrollmentRosterDeDupe] 
AS

--Step 1: Truncate table
Truncate table PatientRosterEnrollment

--Step 2: Find most recent roster for each source system/patient/start Date
SELECT SourceSystemID, LbPatientId, SourceFeed, Start_date, MAX(FileDate) AS [FileDate] 
into #MostRecentRosters
FROM PatientRosterEnrollment_raw
group by SourceSystemID, LbPatientId, SourceFeed, Start_date

--Fill De-Duped Enrollmnet Table
INSERT INTO PatientRosterEnrollment
 (
   SourceSystemID
  ,PatientExternalID
  ,Pat_First_Name
  ,Pat_Last_Name
  ,Pat_Gender
  ,Pat_Birth_date
  ,Pat_Address
  ,Pat_City
  ,Pat_State
  ,Pat_Zip
  ,Pat_Phone
  ,Pat_Email
  ,Start_date
  ,Term_date
  ,Death_date
  ,PrimaryProviderNPI
  ,FileDate
  ,SourceFeed
  ,LbPatientId
  ,ContractName
) SELECT e.SourceSystemID, PatientExternalID, Pat_First_Name, Pat_Last_Name, Pat_Gender, Pat_Birth_date, Pat_Address, Pat_City, Pat_State, Pat_Zip, Pat_Phone,
  Pat_Email, e.Start_date, Term_date, Death_date, PrimaryProviderNPI, e.FileDate, e.SourceFeed, e.LbPatientId, e.ContractName
FROM PatientRosterEnrollment_raw e
JOIN #MostRecentRosters r on e.FileDate = r.FileDate and e.LbPatientId = r.LbPatientId 
            and e.SourceFeed = r.SourceFeed and e.SourceSystemID = r.SourceSystemID 
            and e.Start_date = r.Start_date
            
    drop table #MostRecentRosters
GO
