SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ClinicalDecisionPatientMedicationsLoad]
@PatientID INT
AS
 
--This stored procedure will return all the anonymous medication history information for the clinical decisions 
--Evaluation Request
--LBPP - 1463 - Introduced in version 2.01
--CJL - 02.17.2016  -- First Cut


SELECT 
pm.MedicationID, pm.MedicationStartDate, pm.MedicationEndDate, pm.NDCCode
FROM 
PatientMedication pm WITH (NOLOCK) 
WHERE pm.PatientID = @PatientId and pm.DeleteInd = 0 
AND ( pm.MedicationEndDate > SYSUTCDATETIME() OR pm.MedicationEndDate IS NULL)

GO
