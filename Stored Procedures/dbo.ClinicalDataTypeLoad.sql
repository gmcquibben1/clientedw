SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ClinicalDataTypeLoad]
@ClinicalDataTypeId  INT = NULL
AS


/*===============================================================
	CREATED BY: 	Christopher Lutz
	CREATED ON:		2017-02-15
	INITIAL VER:	2.0.1-2.2
	MODULE:			ETL Dtaa Import
	DESCRIPTION:	This stored procedure will return all of the ClinicalData types from the database or those that match the specified id
	PARAMETERS:		
					@ClinicalDataTypeId		Optional  Type id value that we want to retrieve


	RETURN VALUE(s)/OUTPUT:	 A list of  ClinicalData Type records

	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2017-02-15		CJL			LBETL-865	 Initial

=================================================================*/

DECLARE @tmpClinicalDataTypeId  INT = @ClinicalDataTypeId


SELECT 
	ClinicalDataTypeId, Name, DisplayValue
FROM 
	ClinicalDataType q  WITH (NOLOCK)
WHERE 
		DeleteInd = 0  
		AND 
		(
		(  @tmpClinicalDataTypeId IS NULL OR (  @tmpClinicalDataTypeId = ClinicalDataTypeId)) 
		)

GO
