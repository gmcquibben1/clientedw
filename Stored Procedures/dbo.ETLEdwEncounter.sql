SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwEncounter] 
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES FOR NULL COLUMNS
--UPDATE PatientEncounterProcessQueue SET EncounterDate_YYYYMMDD = '2000-01-01' WHERE EncounterDate_YYYYMMDD IS NULL

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId,LbPatientID, ISNULL(EncounterType,''), ISNULL([EncounterDate],''), [EncounterTime]  
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				InterfacePatientEncounterID
				DESC 
			) SNO
	FROM PatientEncounterProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientEncounterProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientEncounterID INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientEncounterID,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientEncounterID,PatientID
FROM 
(	
MERGE [DBO].PatientEncounter AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,[SourceEncounterId]
         ,[EncounterDate], [EncounterTime], [EncounterTypeID], [Clinician]
         ,[Location],[Comment],[ReasonForVisit_ICD9],[ReasonForVisit_ICD10] ,[ReasonForVisit_LOINC],[ReasonForVisit_SNOMED]
		 ,GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId
		FROM [dbo].PatientEncounterProcessQueue PPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  ([OrigPatientId],[PatientId],[SourceSystemID],[SourceEncounterId],[EncounterDate],[EncounterTime],
		[EncounterTypeID],[Clinician],[Location],[Comment],[ReasonForVisit_ICD9],[ReasonForVisit_ICD10],
		[ReasonForVisit_LOINC],[ReasonForVisit_SNOMED],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
ON ( target.[SourceSystemID] = source.[SourceSystemID] AND target.PatientID = source.PatientID 
     AND ISNULL(target.EncounterTypeID,0) = ISNULL(source.EncounterTypeID,0) AND ISNULL(target.[EncounterDate],'') = ISNULL(source.[EncounterDate],'') 
	 AND ISNULL(target.[EncounterTime],0) = ISNULL(source.[EncounterTime],0) 
	 )
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
--[PatientId]				=source.[PatientId],
[SourceSystemID]		=source.[SourceSystemID],
[SourceEncounterId]		=source.[SourceEncounterId],
[EncounterDate]			=source.[EncounterDate],
[EncounterTime]			=source.[EncounterTime],
[EncounterTypeID]		=source.[EncounterTypeID],
[Clinician]				=source.[Clinician],
[Location]				=source.[Location],
[Comment]				=source.[Comment],
[ReasonForVisit_ICD9]	=source.[ReasonForVisit_ICD9],
[ReasonForVisit_ICD10]	=source.[ReasonForVisit_ICD10],
[ReasonForVisit_LOINC]	=source.[ReasonForVisit_LOINC],
[ReasonForVisit_SNOMED]	=source.[ReasonForVisit_SNOMED],
--[CreateDateTime]		=source.[CreateDateTime],
[ModifyDateTime]		=source.[ModifyDateTime],
--[CreateLBUserId]		=source.[CreateLBUserId],
[ModifyLBUserId]		=source.[ModifyLBUserId]
WHEN NOT MATCHED THEN
INSERT ([PatientId],[SourceSystemID],[SourceEncounterId],[EncounterDate],[EncounterTime],
		[EncounterTypeID],[Clinician],[Location],[Comment],[ReasonForVisit_ICD9],[ReasonForVisit_ICD10],
		[ReasonForVisit_LOINC],[ReasonForVisit_SNOMED],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId]
	   )
VALUES (source.[PatientId],source.[SourceSystemID],source.[SourceEncounterId],source.[EncounterDate],source.[EncounterTime],
		source.[EncounterTypeID],source.[Clinician],source.[Location],source.[Comment],source.[ReasonForVisit_ICD9],source.[ReasonForVisit_ICD10],
		source.[ReasonForVisit_LOINC],source.[ReasonForVisit_SNOMED],source.[CreateDateTime],source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId])
		OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientEncounterID,INSERTED.PatientEncounterID) PatientEncounterID, source.PatientID
) x ;


IF OBJECT_ID('PatientEncounter_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientEncounter_PICT(PatientEncounterID,PatientId,SwitchDirectionId)
	SELECT RT.PatientEncounterID, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientEncounterProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'Encounter',GetDate()
FROM PatientEncounterProcessQueue
WHERE ProcessedInd = 1 

UPDATE A 
SET HoldingReleaseInd = 1 
FROM [dbo].[PatientEncounterProcessQueue_HoldingBay] A 
INNER JOIN PatientEncounterProcessQueue B ON A.InterfacePatientEncounterID = B.InterfacePatientEncounterID
WHERE B.ProcessedInd = 1 

--INSERT INTO[dbo].[PatientEncounterProcessQueue_HoldingBay]
--([InterfacePatientEncounterId],[InterfacePatientId],[InterfaceSystemId],[SourceEncounterId],[EncounterDate],[EncounterTime]
--,[EncounterTypeID],[EncounterType],[Clinician],[Location],[Comment],[ReasonForVisit_ICD9],[ReasonForVisit_ICD10],[ReasonForVisit_LOINC]
--,[ReasonForVisit_SNOMED],[LbPatientId],[InterfacePatientEncounterId_HoldingOverride],[InterfacePatientId_HoldingOverride]
--,[InterfaceSystemId_HoldingOverride],[SourceEncounterId_HoldingOverride],[EncounterDate_HoldingOverride],[EncounterTime_HoldingOverride],[EncounterTypeID_HoldingOverride]
--,[EncounterType_HoldingOverride],[Clinician_HoldingOverride],[Location_HoldingOverride],[Comment_HoldingOverride],[ReasonForVisit_ICD9_HoldingOverride]
--,[ReasonForVisit_ICD10_HoldingOverride],[ReasonForVisit_LOINC_HoldingOverride],[ReasonForVisit_SNOMED_HoldingOverride],[HoldingStartDate],[HoldingStartReason]
-- )
-- SELECT 
--	 A.[InterfacePatientEncounterId],A.[InterfacePatientId],A.[InterfaceSystemId],A.[SourceEncounterId],A.[EncounterDate],A.[EncounterTime]
--	,A.[EncounterTypeID],A.[EncounterType],A.[Clinician],A.[Location],A.[Comment],A.[ReasonForVisit_ICD9],A.[ReasonForVisit_ICD10],A.[ReasonForVisit_LOINC]
--	,A.[ReasonForVisit_SNOMED],A.[LbPatientId]
--	,A.[InterfacePatientEncounterId],A.[InterfacePatientId],A.[InterfaceSystemId],A.[SourceEncounterId],A.[EncounterDate],A.[EncounterTime]
--	,A.[EncounterTypeID],A.[EncounterType],A.[Clinician],A.[Location],A.[Comment],A.[ReasonForVisit_ICD9],A.[ReasonForVisit_ICD10],A.[ReasonForVisit_LOINC]
--	,A.[ReasonForVisit_SNOMED] ,GetDate(),'Duplicacy'
--FROM PatientEncounterProcessQueue  A 
--LEFT JOIN [dbo].[PatientEncounterProcessQueue_HoldingBay] B ON A.InterfacePatientEncounterID = B.InterfacePatientEncounterID
--WHERE ISNULL(A.ProcessedInd,0) <> 1 AND B.InterfacePatientEncounterID IS NULL 

END

GO
