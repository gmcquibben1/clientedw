SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalETLProfessionalFacilityClaims]
WITH EXEC AS CALLER
AS

DECLARE @EdwDbTest varchar(50)= DB_Name()
declare @LbPortalDb varchar(50) = REPLACE(@EdwDbTest,'Edw','LbPortal')



IF EXISTS (SELECT NAME FROM sysobjects WHERE NAME = 'PatientFacilityClaimStaging' AND xtype = 'U')
BEGIN
DECLARE @SQL1 VARCHAR(MAX)
SET @SQL1 = '
INSERT INTO PatientFacilityClaimStaging (
   InterfacePatientFacilityClaimStagingId
  ,LbPatientId
  ,OrgHierarchyPatientIdentifier
  ,InterfacePatientId
  ,InterfaceSystemId
  ,PatientIdentifier
  ,UniqueClaimId
  ,FromDate
  ,ThroughDate
  ,PrincipalDiagnosisCode
  ,AdmittingDiagnosisCode
  ,PlaceOfService
  ,FacilityNPI
  ,OperatingProviderNPI
  ,AttendingProviderNPI
  ,DrgCode
  ,RevenueCode
  ,ProcedureCode
  ,ProcedureModifier1
  ,ProcedureModifier2
  ,ProcedureModifier3
  ,ProcedureModifier4
  ,AdmissionTypeCode
  ,AdmissionSourceCode
  ,AdjustmentReasonCode
  ,CreateDateTime
) 
SELECT DISTINCT A.InterfacePatientFacilityClaimStagingId, B.LbPatientId, 0, A.InterfacePatientId, A.InterfaceSystemId, A.PatientIdentifier, A.UniqueClaimId, A.FromDate, A.ThroughDate, A.PrincipalDiagnosisCode, A.AdmittingDiagnosisCode, A.PlaceOfService, A.FacilityNPI, A.OperatingProviderNPI, A.AttendingProviderNPI, A.DrgCode, A.RevenueCode, A.ProcedureCode, A.ProcedureModifier1, A.ProcedureModifier2, A.ProcedureModifier3, A.ProcedureModifier4, A.AdmissionTypeCode, A.AdmissionSourceCode, A.AdjustmentReasonCode, A.CreateDateTime
FROM '+@LbPortalDb+'..InterfacePatientFacilityClaimStaging A
JOIN vwPortal_InterfacePatientMatched B ON A.PatientIdentifier = B.PatientIdentifier
LEFT JOIN PatientFacilityClaimStaging F ON A.InterfacePatientFacilityClaimStagingId = F.InterfacePatientFacilityClaimStagingId
where F.PatientFacilityClaimStagingId IS NULL'
EXEC (@SQL1)

UPDATE PatientFacilityClaimStaging SET OrgHierarchyPatientIdentifier = O.BENE_HIC_NUM
FROM PatientFacilityClaimStaging A JOIN OrgHierarchy O ON A.LbPatientId = O.LbPatientId
END


IF EXISTS (SELECT NAME FROM sysobjects WHERE NAME = 'PatientProfessionalClaimStaging' AND xtype = 'U')
BEGIN
DECLARE @SQL2 VARCHAR(MAX)
SET @SQL2 = '
INSERT INTO PatientProfessionalClaimStaging (
   InterfacePatientProfessionalClaimStagingId
  ,LbPatientId
  ,OrgHierarchyPatientIdentifier
  ,InterfacePatientId
  ,InterfaceSystemId
  ,PatientIdentifier
  ,UniqueClaimId
  ,FromDate
  ,ThroughDate
  ,ProviderSpecialtyCode
  ,ProviderTaxonomyCode
  ,PlaceOfService
  ,ProcedureCode
  ,ProcedureModifier1
  ,ProcedureModifier2
  ,ProcedureModifier3
  ,ProcedureModifier4
  ,PrincipalDiagnosisCode
  ,ProviderTaxId
  ,RenderingProviderNPI
  ,AdjustmentReasonCode
  ,TotalChargeAmount
  ,UnitQuantity
  ,DiagnosisCode2
  ,DiagnosisCode3
  ,DiagnosisCode4
  ,DiagnosisCode5
  ,DiagnosisCode6
  ,DiagnosisCode7
  ,DiagnosisCode8
  ,DiagnosisCode9
  ,CreateDateTime
) SELECT DISTINCT P.InterfacePatientProfessionalClaimStagingId, M.LbPatientId, 0,P.InterfacePatientId, P.InterfaceSystemId, P.PatientIdentifier, 
P.UniqueClaimId, P.FromDate, P.ThroughDate, P.ProviderSpecialtyCode, P.ProviderTaxonomyCode, P.PlaceOfService, P.ProcedureCode, 
P.ProcedureModifier1, P.ProcedureModifier2, P.ProcedureModifier3, P.ProcedureModifier4, P.PrincipalDiagnosisCode, 
P.ProviderTaxId, P.RenderingProviderNPI, P.AdjustmentReasonCode, P.TotalChargeAmount, P.UnitQuantity, P.DiagnosisCode2, P.DiagnosisCode3, P.DiagnosisCode4, 
P.DiagnosisCode5, P.DiagnosisCode6, P.DiagnosisCode7, P.DiagnosisCode8, P.DiagnosisCode9, P.CreateDateTime  
FROM '+@LbPortalDb+'..InterfacePatientProfessionalClaimStaging P
JOIN vwPortal_InterfacePatientMatched M ON P.PatientIdentifier = M.PatientIdentifier
LEFT JOIN PatientProfessionalClaimStaging PPCS ON P.InterfacePatientProfessionalClaimStagingId = PPCS.InterfacePatientProfessionalClaimStagingId
WHERE PPCS.PatientProfessionalClaimStagingId IS NULL'
EXEC (@SQL2)

UPDATE PatientProfessionalClaimStaging SET OrgHierarchyPatientIdentifier = O.BENE_HIC_NUM 
FROM PatientProfessionalClaimStaging PPCS JOIN OrgHierarchy O ON PPCS.LbPatientId = O.LbPatientId
END

GO
