SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE  [dbo].[internalObGynAttribution]
    @LookBackMonths INT = 18
  , @AttributionTypeId INT = 12
    WITH EXEC AS CALLER
AS
/*================================================================
 Author:		
 Create date: 
 Description:	Attribution for OB GYNs
 Modified By:	
 Modified Date:	

 	Version		Date		Author		Jira		Change
		       2017-03-13   dvp      LBETL-975	Add codes to ObGynDeliveryProcedureType table in LbPortal
			   2017-03-20	dvp		 LBETL-975  Corrected missing paranetheses.
================================================================*/

    EXEC InternalDropTempTables


--DECLARE @BeginDate DATETIME = CAST(CAST(YEAR(GETDATE()) -1 AS VARCHAR(4)) + '-01-01' AS DATETIME)
--DECLARE @ENDDate DATETIME = CAST(CAST(YEAR(GETDATE()) -1 AS VARCHAR(4)) + '-12-31' AS DATETIME)


--FIND MOST RECENT INITIAL OB OR DELIVERY PROCEDURE CODE
--SELECT TOP 1000 PP.PatientID,PC.ProcedureCode, PP.PerformedByClinician, PP.OrderedByClinician, PP.ProcedureDateTime, DATEDIFF(MONTH, PP.ProcedureDateTime , GETDATE()) AS AGING  
    SELECT  PP.PatientID
          , MAX(PP.ProcedureDateTime) AS MAXPROCEDUREDATE
    INTO    #Step1
    FROM    PatientProcedure PP --JOIN PatientProcedureProcedureCode PPPC ON PP.PatientProcedureId = PPPC.PatientProcedureId
    JOIN    ProcedureCode PC
    ON      PP.ProcedureCodeId = PC.ProcedureCodeId
--JOIN vwPortal_ProviderActive PRV ON PP.RenderingProviderNPI = PRV.NationalProviderIdentifier
    WHERE   --ProcedureDateTime BETWEEN @BeginDate and @ENDDate--
            DATEDIFF(MONTH, PP.ProcedureDateTime, GETDATE()) < @LookBackMonths
            AND (
				PC.ProcedureCode IN ( '59400', '59409', '59410', '59510',
                                      '59514', '59515', '59610', '59612',
                                      '59614', '59618', '59620', '59622',--DELIVERY CODES
                                      'EOB', 'OB', 'NOB', '99203OB', '99204OB',
                                      '99213OB', '99214OB', 'IOB', '99214MCD',
                                      '44444-NO', '99203-NO', '99214-NO',
                                      'OBDEPOSIT�', '990203TH ', '99204TH',--INITIAL ob visit CODES
                                      '58150', '58152', '58180', '58200',
                                      '58210', '58240', '58260', '58262',
                                      '58263', '58267', '58270', '58275',
                                      '58280', '58285', '58290', '58291',
                                      '58292', '58293',--HYSTERECTOMY CODE
                                      '58294', '58541', '58542', '58543',
                                      '58544', '58548', '58550', '58552',
                                      '58553', '58554', '58570', '58571',
                                      '58572', '58573', '58951', '58953',
                                      '58954', '58956' )--HYSTERECTOMY CODE
				OR		PC.ProcedureCode IN   --LBETL-975
						(
						SELECT Name
						FROM vwPortal_AllTypeTables AS vpatt
						WHERE	tablename = 'ObGynDeliveryProcedureType'
						)
				)
GROUP BY    PP.PatientID


--select * from #Step1 where patientID = 30512
--FIND THE ASSOCIATED DOCTOR WHO PERFORMED THE CODE
    SELECT  PP.PatientID
          , PC.ProcedureCode
          , PP.PerformedByClinician
          , PP.OrderedByClinician
          , PP.ProcedureDateTime
          , CASE WHEN LEN(LTRIM(RTRIM(PP.RenderingProviderNPI))) = 10
                      AND ISNUMERIC(PP.RenderingProviderNPI) = 1
                 THEN LTRIM(RTRIM(PP.RenderingProviderNPI))
                 WHEN LEN(LTRIM(RTRIM(PP.PerformedByProviderID))) = 10
                      AND ISNUMERIC(PP.PerformedByProviderID) = 1
                 THEN LTRIM(RTRIM(PP.PerformedByProviderID))
                 WHEN LEN(LTRIM(RTRIM(PP.PerformedByClinician))) = 10
                      AND ISNUMERIC(PP.PerformedByClinician) = 1
                 THEN LTRIM(RTRIM(PP.PerformedByClinician))
                 WHEN LEN(LTRIM(RTRIM(PP.OrderedByClinician))) = 10
                      AND ISNUMERIC(PP.OrderedByClinician) = 1
                 THEN LTRIM(RTRIM(PP.OrderedByClinician))
            END AS RenderingProviderNPI
--, PRV.ProviderID, 
          , PP.SourceSystemID
          , PP.PerformedByProviderID
          , PP.OrderedByProviderID
    INTO    #Step2
    FROM    PatientProcedure PP --JOIN PatientProcedureProcedureCode PPPC ON PP.PatientProcedureId = PPPC.PatientProcedureId
    JOIN    ProcedureCode PC
    ON      PP.ProcedureCodeId = PC.ProcedureCodeId
    JOIN    #Step1 S1
    ON      PP.PatientID = S1.PatientID
            AND PP.ProcedureDateTime = S1.MAXPROCEDUREDATE
--JOIN vwPortal_ProviderActive PRV ON PP.RenderingProviderNPI = PRV.NationalProviderIdentifier
    WHERE   PC.ProcedureCode IN ( '59400', '59409', '59410', '59510', '59514',
                                  '59515', '59610', '59612', '59614', '59618',
                                  '59620', '59622',--DELIVERY CODES
                                  'EOB', 'OB', 'NOB', '99203OB', '99204OB',
                                  '99213OB', '99214OB', 'IOB', '99214MCD',
                                  '44444-NO', '99203-NO', '99214-NO',
                                  'OBDEPOSIT�', '990203TH ', '99204TH',--INITIAL ob visit CODES
                                  '58150', '58152', '58180', '58200', '58210',
                                  '58240', '58260', '58262', '58263', '58267',
                                  '58270', '58275', '58280', '58285', '58290',
                                  '58291', '58292', '58293',--HYSTERECTOMY CODE
                                  '58294', '58541', '58542', '58543', '58544',
                                  '58548', '58550', '58552', '58553', '58554',
                                  '58570', '58571', '58572', '58573', '58951',
                                  '58953', '58954', '58956' )--HYSTERECTOMY CODE
	OR		PC.ProcedureCode IN --LBETL-975
			(SELECT Name
			FROM vwPortal_AllTypeTables AS vpatt
			WHERE	tablename = 'ObGynDeliveryProcedureType')

--Insert patient-provider records
    INSERT  INTO vwPortal_PatientProvider
            (
              PatientID
            , ProviderID
            , SourceSystemID
            , ExternalReferenceIdentifier
            , OtherReferenceIdentifier
            , ProviderRoleTypeID
            , DeleteInd
            , CreateDateTime
            , ModifyDateTime
            , CreateLBUserId
            , ModifyLBUserId
            , OverrideInd
            , AttributionTypeId
            , AttributedProviderInd
            )
            SELECT  PatientID
                  , ProviderID
                  , 1
                  , RenderingProviderNPI
                  , RenderingProviderNPI
                  , 1
                  , 0
                  , GETUTCDATE()
                  , GETUTCDATE()
                  , 1
                  , 1
                  , 0
                  , @AttributionTypeId
                  , 0
            FROM    #Step2 t
            JOIN    vwPortal_ProviderActive PRV
            ON      t.RenderingProviderNPI = PRV.NationalProviderIdentifier
            WHERE   ProviderID IS NOT NULL


/* Testing info, can be disregarded for processing
--SELECT * FROM #Step2
SELECT ss.[Description],
case 
when ProviderID is null then 'Not a USWHA Provider'
else 'USWHA Provider'
end,
CASE 
when ProcedureCode in ('59400','59409','59410','59510','59514','59515','59610','59612','59614','59618','59620','59622') THEN 'Delivery'
when ProcedureCode in ('EOB','OB','NOB','99203OB','99204OB','99213OB','99214OB','IOB','99214MCD','44444-NO','99203-NO','99214-NO','OBDEPOSIT�','990203TH ','99204TH','51925') THEN 'Initial Ob Visit'
ELSE 'Hysterectomy'
END, count(*)
FROM #Step2 s2
join SourceSystem ss on ss.SourceSystemId = s2.SourceSystemId
group by ss.[Description],case 
when ProviderID is null then 'Not a USWHA Provider'
else 'USWHA Provider'
end,
CASE 
when ProcedureCode in ('59400','59409','59410','59510','59514','59515','59610','59612','59614','59618','59620','59622') THEN 'Delivery'
when ProcedureCode in ('EOB','OB','NOB','99203OB','99204OB','99213OB','99214OB','IOB','99214MCD','44444-NO','99203-NO','99214-NO','OBDEPOSIT�','990203TH ','99204TH','51925') THEN 'Initial Ob Visit'
ELSE 'Hysterectomy'
END

testing breakdown by provider by type
select CASE 
when ProcedureCode in ('59400','59409','59410','59510','59514','59515','59610','59612','59614','59618','59620','59622') THEN 'Delivery'
when ProcedureCode in ('EOB','OB','NOB','99203OB','99204OB','99213OB','99214OB','IOB','99214MCD','44444-NO','99203-NO','99214-NO','OBDEPOSIT�','990203TH ','99204TH','51925') THEN 'Initial Ob Visit'
ELSE 'Hysterectomy'
END, RenderingProviderNPI,  npi.[NAME], npi.Specialty,
ProviderID as [LB Provider ID], count(*)
 FROM #Step2 s2 
 LEFT JOIN NPI_Lookup npi on  s2.RenderingProviderNPI = npi.NPI
group by CASE 
when ProcedureCode in ('59400','59409','59410','59510','59514','59515','59610','59612','59614','59618','59620','59622') THEN 'Delivery'
when ProcedureCode in ('EOB','OB','NOB','99203OB','99204OB','99213OB','99214OB','IOB','99214MCD','44444-NO','99203-NO','99214-NO','OBDEPOSIT�','990203TH ','99204TH','51925') THEN 'Initial Ob Visit'
ELSE 'Hysterectomy'
END, RenderingProviderNPI,  npi.[NAME], npi.Specialty, npi.State,
ProviderID

--testing provider info
select 
 s2.ProviderID, s2.RenderingProviderNPI, npi.[NAME], count(*)
 FROM #Step2 s2 
 LEFT JOIN NPI_Lookup npi on  s2.RenderingProviderNPI = npi.NPI
group by s2.ProviderID, s2.RenderingProviderNPI, npi.[NAME]


SELECT ss.[Description] as [SourceSystemName], o.MemberFullName, PatientID, ProcedureCode, PerformedByClinician, OrderedByClinician, PerformedByProviderID, OrderedByProviderID, ProcedureDateTime,  RenderingProviderNPI
FROM #Step2 s2
join SourceSystem ss on ss.SourceSystemId = s2.SourceSystemId
join OrgHierarchy o on o.LbPatientId = s2.PatientID
WHERE RenderingProviderNPI IS NULL
*/
GO
