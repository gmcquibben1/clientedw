SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ETLMemberExtractRebuild]
AS
BEGIN

SET NOCOUNT ON

set ANSI_WARNINGS OFF

DECLARE @LBUSERID            INT=1
DECLARE @CurrentDateTime     DATETIME=GETDATE()
DECLARE @DeleteInd           BIT=0
DECLARE @PartyDescription    VARCHAR(50)='Patient'
DECLARE @PartyStartDateTime  DATETIME=GETDATE()
DECLARE @PartyEndDateTime    DATETIME=null
DECLARE @PartyTypeID         INT=1--Patient
DECLARE @SourceSystemID      INT=2--UA Claims Feed
DECLARE @TobaccoTypeID       INT=0--UNKNOWN
DECLARE @MaritalStatusTypeID INT=0--UNKNOWN
DECLARE @ReligonTypeID       INT=0--UNKNOWN
DECLARE @MailingAddressTypeID INT=1 --Mailing
DECLARE @HomeAddressTypeID  INT=2 --Home
DECLARE   @DwellingTypeID         INT=0 --unknown
DECLARE   @TerritoryTypeId   INT=0 --unknown
DECLARE   @CountryTypeId     INT=1 --USA
DECLARE @PhoneTypeID         INT=1 --Home
DECLARE @ServiceCarrierTypeID INT=0 --unknown
DECLARE @CountryCode         INT=1 --1 IS FOR US

-----------------------------------------------------------
-- Load all Patients into MemberExtract table
-- from PatientIdsRegistryStage_Sam.
-- Skip any bad records which are missing : 
--	 * FirstName, LastName, DOB, Gender
-----------------------------------------------------------
truncate table [dbo].[MemberExtract]
--DROP INDEX [IDX_MEMBEREXTRACT_LFNAME] ON [dbo].[MemberExtract]
--DROP INDEX [IDX_MEMBEREXTRACT_DOB] ON [dbo].[MemberExtract]

/*
INSERT INTO [dbo].[MemberExtract] (
     [lastName] ,[midInit],[firstName],[sex] ,[birthDate] ,[medicareNo]
     ,mailAddrLine1,mailAddrLine2,mailCity,mailState,mailZip
     ,homeAddrLine1,homeAddrLine2,homeCity,homeState,[homeZip]
     ,homePhone,[family_id],[source]   ,[SourceFeed] , 
	 SourceSystemId)
SELECT DISTINCT
     LTRIM(RTRIM(PIRS.[LastName])) as lastName,
     LTRIM(RTRIM(PIRS.[MiddleName])),
     LTRIM(RTRIM(PIRS.[FirstName])),
     PIRS.[Gender],
	 PIRS.[DOBDateTime],
     (PIRS.[DepositingId]),
     PIRS.mailAddrLine1,PIRS.mailAddrLine2,PIRS.mailCity,PIRS.mailState,PIRS.mailZip,
     PIRS.homeAddrLine1,PIRS.homeAddrLine2,PIRS.homeCity,PIRS.homeState,PIRS.[homeZip],
     PIRS.homephone,NEWID(),PIRS.DepositingIdSourceType,PIRS.DepositingIdSourceFeed , 
	 PIRS.SourceSystemId
FROM PatientIdsRegistryStage  PIRS
WHERE ISNULL(PIRS.[DepositingId], '') <> '' 



CREATE NONCLUSTERED INDEX IDX_MEMBEREXTRACT_LFNAME
ON [dbo].[MemberExtract] ([lastName],[firstName])
INCLUDE ([sex],[birthDate],[medicareNo])

CREATE NONCLUSTERED INDEX IDX_MEMBEREXTRACT_DOB
ON [dbo].[MemberExtract] ([birthDate])
INCLUDE ([lastName],[firstName],[sex])
*/

-----------------------------------------------------------
--  Mark all Patients ID valid 
--  We'll run dedupe later to mark duplicate pids.
-----------------------------------------------------------
update vwPortal_Patient
set DeleteInd = 0
from vwPortal_Patient

UPDATE vwPortal_Individual SET DeleteInd = 0

UPDATE vwPortal_IndividualName SET DeleteInd = 0

-----------------------------------------------------------
-- Grab LbPatienId from vwPatientDemographic
-----------------------------------------------------------
/*
update [dbo].[MemberExtract]
set LbPatientId = P.patientId
from [dbo].[MemberExtract] M INNER join dbo.vwPatientDemographic P 
ON 	ltrim(rtrim(P.FirstName)) = ltrim(rtrim(M.FirstName))
	and ltrim(rtrim(P.LastName)) = ltrim(rtrim(M.LastName))
	and P.BirthDate = M.[birthDate]
	and cast( (case when P.Gender = 'Female' then 2 
			when P.Gender = 'Male' then 1
			else 0 end) as int) = M.sex 
*/
exec EtlMemberExtract

update [dbo].[MemberExtract]
set LbPatientId = P.patientId
from [dbo].[MemberExtract] M INNER join vwPortal_Party PTY
on M.medicareNo = PTY.ExternalReferenceIdentifier
INNER JOIN vwPortal_Individual I
on PTY.PartyID = I.PartyID
INNER JOIN vwPortal_Patient P
on I.IndividualID = P.IndividualID


update [dbo].[MemberExtract]
set LbPatientId = P.patientId
from [dbo].[MemberExtract] M INNER join dbo.vwPatientDemographic P 
ON 	ltrim(rtrim(P.FirstName)) = ltrim(rtrim(M.FirstName))
	and ltrim(rtrim(P.LastName)) = ltrim(rtrim(M.LastName))
	and P.BirthDate = M.[birthDate]
	and cast( (case when P.Gender = 'Female' then 2 
			when P.Gender = 'Male' then 1
			else 0 end) as int) = M.sex 

truncate table dbo.DedupedPatientIdHistory

exec dbo.PatientDedupe

------------------------------------------------------------
-- Match one more time using medicareNo 
-- to find more LbPatientIds...
------------------------------------------------------------

select  distinct MS.Firstname, MS.LastName, MS.birthDate, 
	MS.sex , MS.medicareNo 
into #NP1  from dbo.[MemberExtract] MS where LbPatientId is null

select distinct * into #NP2 from (
 select M.FirstName, M.lastName, M.Gender, M.birthDate, M.PatientId , 
 N.medicareNo
 from vwPatientDemographic M JOIN #NP1 N
 on   M.FirstName = N.FirstName
 and M.LastName = N.LastName
 and M.BirthDate = N.birthDate
 and M.ExternalReferenceIdentifier = N.medicareNo
) A 

update [dbo].[MemberExtract]
set LbPatientId = P.patientId
from [dbo].[MemberExtract] M INNER join #NP2 P 
ON M.medicareNo = P.medicareNo


exec dbo.ETLPatientIdReferenceBuild

END









GO
