SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[GproTargetPatientListLoad] 
/*===============================================================
	CREATED BY: 	AA
	CREATED ON:		2016-11-15
	INITIAL VER:	2.2.1
	MODULE:			GPRO		
	DESCRIPTION:	For the selected business unit id and measure return the target patient list.
	PARAMETERS:		BusinessUnitId, GPro measure name (optional)
	RETURN VALUE(s)/OUTPUT:	a table containing all of the available patient list. 
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		11-15-2016		AM			LBPP-2025	Initial Version
	2.2.1		11-28-2016		BR			LBPP-2025	Fixed the output to remove the individual measure name and make it a distinct row per patient
	2.2.1		2017-01-09		BR			LBPP-2157	Changed the unique index on the complete and skipped patient lists to the GproPatientRankingId since patients with NULL
														LbPatientId caused duplicates
	2.2.1		01-12-2017		BR						Added a join to the #patientList when getting the top 248 patients to calculate the remaining to target
	2.2.1		01-17-2017		BR						Fixed the previous join to #patientList so that it finds top 248 in the whole ACO, not just this BU
	2.2.1		02-01-2017		BR						The consecutive target calculation should have been >= rather than just >
=================================================================*/
(
    @BusinessUnitId INT ,
	@GproMeasureName VARCHAR (20)  = NULL
)
AS
BEGIN

	CREATE TABLE #measureList
	(
		RowNumber INT IDENTITY (1, 1)
		, MeasureName VARCHAR(20)
		, RankField VARCHAR(20)
	);

	CREATE TABLE #CompletePatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #IncompletePatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
	
	);

	CREATE TABLE #SkippedPatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	
	DECLARE @RankColumnName VARCHAR(50);



	CREATE TABLE #patientList
	(
		GproPatientRankingId INT
		
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #groupTinPatientList
	(
		GproPatientRankingId INT
		
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	INSERT INTO #patientList EXEC GproGetPatientListByBusinessUnit @businessUnitId;

	INSERT INTO #groupTinPatientList
		(GproPatientRankingId)
	SELECT gpr.GproPatientRankingId 
	FROM GproPatientRanking gpr
	WHERE gpr.GroupTin IN (SELECT DISTINCT gpr2.GroupTin FROM GproPatientRanking gpr2 INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr2.GproPatientRankingId)
	
	Create table #temp
	(
	 LBPatientId INT
	 ,GproPatientRankingId int 
	 ,FirstName varchar(50)
	 ,LastName varchar(50)
	 ,BirthDay varchar(20)
	 ,Name varchar(50)
	 ,BusinessUnitName varchar(50)
	 ,Measure varchar(50)
	)
	--------------------------------------------------------------------------------
	-- populate a temp table with the list of measures we will include
	--------------------------------------------------------------------------------
	IF @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #measureList (MeasureName, RankField) SELECT Name, RankColumnName FROM GproMeasureType;
	END
	ELSE
	BEGIN 
		INSERT INTO #measureList (MeasureName, RankField) SELECT Name, RankColumnName FROM GproMeasureType WHERE Name = @GproMeasureName;
	END


	DECLARE @SQL VARCHAR(MAX)
	--------------------------------------------------------------------------------
	-- loop through the temp table and update the counts in each loop
	--------------------------------------------------------------------------------
	DECLARE @CurrentRankColumn VARCHAR(20);
	DECLARE @CurrentMeasureName VARCHAR(20);
	DECLARE @CurrentMeasureId INT = (SELECT MAX(RowNumber) FROM #measureList);
	WHILE @CurrentMeasureId > 0
	BEGIN
		SET @CurrentRankColumn = (SELECT RankField FROM #measureList WHERE RowNumber = @CurrentMeasureId)
		SET @CurrentMeasureName = (SELECT MeasureName FROM #measureList WHERE RowNumber = @CurrentMeasureId)

		--------------------------------------------------------------------------------
		-- find the set of complete and skipped patients for this measure
		--------------------------------------------------------------------------------
		TRUNCATE TABLE #CompletePatients;
		TRUNCATE TABLE #SkippedPatients;
		TRUNCATE TABLE #IncompletePatients;
		SET @Sql = 'INSERT INTO #CompletePatients EXEC GproGetCompletePatientList ''' + @CurrentMeasureName + ''''
		EXEC (@Sql);
		SET @Sql = 'INSERT INTO #SkippedPatients EXEC GproGetSkippedPatientList ''' + @CurrentMeasureName + ''''
		EXEC (@Sql);
		SET @Sql = 'INSERT INTO #IncompletePatients (LbPatientId,MeasureRank,GproPatientRankingId,GproPatientMeasureId,GproMeasureTypeId)
			SELECT gpr.LbPatientId,gpr.CareFallsRank,gpr.GproPatientRankingId,gpm.GproPatientMeasureId,null
			FROM GproPatientMeasure gpm 
				INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.' + @CurrentRankColumn + ' <> 0
				AND NOT EXISTS (SELECT TOP 1 1 FROM #CompletePatients cp WHERE cp.GproPatientRankingId = gpr.GproPatientRankingId)
				AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GproPatientRankingId = gpr.GproPatientRankingId)';
		EXEC (@Sql);

-----------------------------    Target Patient List ---------------------------
SET @Sql=

		'INSERT INTO #TEMP (LBPatientId,GproPatientRankingId,FirstName,LastName,BirthDay,Measure) 
		SELECT gpr.LBPATIENTID ,gpr.GproPatientRankingID,gpr.FirstName,gpr.LastName,gpr.Birthdate,GMT.NAME
FROM GproPatientRanking gpr 
INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId
INNER JOIN GproPatientRankingGproMeasureType GRGM   ON GPR.GProPatientRankingId = GRGM.GProPatientRankingId
INNER JOIN GproMeasureType GMT ON GRGM.GproMeasureTypeId = GMT.GproMeasureTypeId AND  GMT.NAME  = '''+@CurrentMeasureName+'''
INNER JOIN #IncompletePatients ip ON ip.GproPatientRankingId = gpr.GproPatientRankingId
WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL 
      AND gpr.' + @CurrentRankColumn + ' <> 0
	  AND gpr.' + @CurrentRankColumn + ' <=  (SELECT MAX(s.' + @CurrentRankColumn + ') 
	                                         FROM  ( SELECT TOP 248 gpr2.' + @CurrentRankColumn + ' 
													FROM GproPatientRanking gpr2
														INNER JOIN #groupTinPatientList gtpl ON gtpl.GproPatientRankingId = gpr2.GproPatientRankingId
													WHERE gpr2.' + @CurrentRankColumn + ' IS NOT NULL 
													  AND gpr2.' + @CurrentRankColumn + ' <> 0
													  AND NOT EXISTS (  SELECT TOP 1 1 
													                    FROM #SkippedPatients sp 
																		WHERE sp.GpropatientRankingId = gpr2.GproPatientRankingId)
																		ORDER BY gpr2.' + @CurrentRankColumn + ') s)'

		--Print  @Sql													
		--------------------------------------------------------------------------------
		-- Get the total of all ranked patients for this measure select * from GproPatientRanking
		--------------------------------------------------------------------------------
		EXEC (@Sql);

		SET @CurrentMeasureId = @CurrentMeasureId - 1;
	END

	--------------------------------------------------------------------------------
	-- Return the result row with the totals
	--------------------------------------------------------------------------------
select I.LBPatientId,I.GproPatientRankingId,i.FirstName,i.LastName,i.BirthDay ,I.Measure 'Measure' ,B.Name as 'BusinessUnit' into #PatientData
from #temp I
INNER JOIN GProPatientRanking G on I.GproPatientRankingId =G.GProPatientRankingId
INNER JOIN vwPortal_BusinessUnit B on G.ClinicIdentifier = B.ExternalCode

	--------------------------------------------------------------------------------
	-- Change the measures in comma seprated measures
	--------------------------------------------------------------------------------

SELECT DISTINCT LbPatientId, GproPatientRankingId, FirstName, LastName, BirthDay, BusinessUnit
       ,STUFF(  
	    (SELECT ', ' + CAST(Measure AS VARCHAR(10) ) [text()]
         FROM #PatientData 
         WHERE lbpatientid = t.lbpatientid

         FOR XML PATH(''), TYPE
		 )
        .value('.','NVARCHAR(MAX)'),1,2,' ') MeasureList
FROM #PatientData t
ORDER BY BusinessUnit

END





GO
