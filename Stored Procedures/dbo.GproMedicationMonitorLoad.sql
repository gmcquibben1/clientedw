SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproMedicationMonitorLoad]
/*===============================================================
	CREATED BY: 	BG
	CREATED ON:		2016-11-17
	INITIAL VER:	2.2.1
	MODULE:			GPRO		
	DESCRIPTION:	Populate the medication data in GproPatientMedication table with relevant clinical data to complete the GPRO measures
	PARAMETERS:		none
	RETURN VALUE(s)/OUTPUT:	none
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-11-17		BG			LBPP-2026	Initial version 
	2.2.1		2017-01-03		BR			LBPP-1438	Fixed the in-practice and med checked flags to set to 2 for "Yes" instead of 1
	2.2.1       2017-01-03      BG                      Fixed issue where we it was comparing a date to a datetime

=================================================================*/
AS
BEGIN
    SET NOCOUNT ON;

	DECLARE @MeasurePeriodEnd DATETIME = '2017-01-01'
	DECLARE @MeasurePeriodBegin DATETIME = DateAdd( YEAR, -1, @MeasurePeriodEnd )
	DECLARE @UseClinicalDataOnlyInd bit = 1

	----------------------------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------------------------
	--Get System Setting for if we shoudl use clinical data only for GPRO 
	SELECT @UseClinicalDataOnlyInd = setvalue
	FROM vwportal_systemsettings WHERE settingtype = 'GPRO' 
	AND settingparameter = 'ClinicalOnlyEnabled'
	
	
	
		DECLARE  @SourceIdTable TABLE 
		(
			SourceSystemId	INT
		)

		IF @UseClinicalDataOnlyInd = 0 
		BEGIN
			INSERT INTO @SourceIdTable
			(SourceSystemId)
			SELECT SourceSystemId FROM SourceSystem;
		END
		ELSE 
		BEGIN
			INSERT INTO @SourceIdTable
			(SourceSystemId)
			SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
		END
	----------------------------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------------------------



	--Create a temp table to hold patient that qualify for age requirement
	IF OBJECT_ID( 'tempdb..#GPROValueSetCodes' ) IS NOT NULL DROP TABLE [#GPROValueSetCodes];
	CREATE TABLE [#GPROValueSetCodes]
	(
	   ValueSetName varchar(50),
	   CodeSystem varchar(50),
	   Code varchar(50),
	);

	----------------------------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------------------------

	---Fill in GPROValueSetCodes for the measure we are working with
	insert into [#GPROValueSetCodes]
	(
	   ValueSetName ,
	   CodeSystem ,
	   Code 
	)
	select VariableName ,
	   CodeSystem ,
	   Code
	--from [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	from vwGProValueSet
	where  [ModuleType] = 'CARE' and [ModuleIndicator] = '3' and Year = '2016'

	----------------------------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------------------------

	--Get the patients from the Medication ranking tables
	IF OBJECT_ID( 'tempdb..#TempMedRecPatientIDs' ) IS NOT NULL DROP TABLE [#TempMedRecPatientIDs];
	SELECT DISTINCT lbpatientid, GPPR.GProPatientRankingID
	INTO #TempMedRecPatientIDs
	FROM gpropatientranking GPPR
	JOIN [GproPatientMedication] GPPM
	ON GPPM.GProPatientRankingID = GPPR.GProPatientRankingID 



	IF OBJECT_ID( 'tempdb..#Measure_Stage_Enc' ) IS NOT NULL DROP TABLE [#Measure_Stage_Enc];
	CREATE TABLE [#Measure_Stage_Enc]
	(
		[LbPatientId] [int] NOT NULL,
		[VisitDate] [date] NOT NULL,
		[EncNumerator] [smallint] NOT Null,
		[ExclusionInd] [tinyint] NOT NULL,
		[ProcedureCode] varchar(20) NULL,
		[ProcedureDateTime] datetime,
		GProPatientRankingID int
	);



	INSERT INTO [#Measure_Stage_Enc] WITH (TABLOCK)
	(
		[LbPatientId],
		[VisitDate],
		[EncNumerator],
		[ExclusionInd],
		GProPatientRankingID
	)
			SELECT pp.PatientId,pp.proceduredatetime,0,0,bene.GProPatientRankingID
			FROM vwPatientProcedure PP
			JOIN #TempMedRecPatientIDs bene
			ON pp.patientid = bene.lbpatientid
			JOIN [#GPROValueSetCodes] GVSC
			ON  GVSC.[Code] = pp.[ProcedureCode]
			AND GVSC.ValueSetName IN ( 'ENCOUNTER_CODE' )
			AND PP.patientid = bene.lbpatientid 
			AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
			AND pp.[ProcedureDateTime] < @MeasurePeriodEnd
	--		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
			GROUP BY PP.[PatientID], bene.GProPatientRankingID,pp.[ProcedureDateTime]  ; 



	IF OBJECT_ID( 'tempdb..#TempCurrentMedsDoc' ) IS NOT NULL DROP TABLE [#TempCurrentMedsDoc];
			SELECT pp.patientid, pp.procedurecode, convert(date,pp.proceduredatetime,112) as proceduredatetime
			INTO #TempCurrentMedsDoc
			FROM vwPatientProcedure pp
			JOIN [#Measure_Stage_Enc] bene
			ON pp.patientid = bene.lbpatientid
			JOIN [#GPROValueSetCodes] GVSC
			ON pp.[ProcedureCode] = GVSC.[Code]
				AND GVSC.ValueSetName = 'CURRENT_MEDS_DOC'
				AND pp.[PatientID] = bene.[LBPatientID] -- Correlated subquery
				AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
				AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd
				AND convert(date,pp.proceduredatetime,112) = bene.[VisitDate] 
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pp.[SourceSystemID]


	UPDATE [#Measure_Stage_Enc] WITH (TABLOCK)
	SET
		[EncNumerator] = 1,
		[ProcedureCode] = #TempCurrentMedsDoc.procedurecode,
		[ProcedureDateTime] = #TempCurrentMedsDoc.proceduredatetime
	FROM
		[#Measure_Stage_Enc], #TempCurrentMedsDoc
	WHERE
			[#Measure_Stage_Enc].[LbPatientId] = #TempCurrentMedsDoc.patientid
			AND [#Measure_Stage_Enc].[VisitDate] = #TempCurrentMedsDoc.proceduredatetime;


	UPDATE gpropatientmedication 
	SET
		CareMeddocVisitConfirmed = 2
	FROM
		gpropatientmedication, [#Measure_Stage_Enc]
	WHERE
			[#Measure_Stage_Enc].GProPatientRankingID = gpropatientmedication.GProPatientRankingID
			AND [#Measure_Stage_Enc].visitdate = gpropatientmedication.CareMeddocVisitDate 



	UPDATE gpropatientmedication 
	SET
		 MedicationDocumented = 2
	FROM
		gpropatientmedication, [#Measure_Stage_Enc]
	WHERE
			[#Measure_Stage_Enc].GProPatientRankingID = gpropatientmedication.GProPatientRankingID
			AND [#Measure_Stage_Enc].visitdate = gpropatientmedication.CareMeddocVisitDate 
			AND [#Measure_Stage_Enc].EncNumerator = 1

END

GO
