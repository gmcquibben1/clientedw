SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalInterfacePatientPCPAttribution]
WITH EXEC AS CALLER
AS

   select distinct
    P.ProviderID, IPM.LbPatientId, P.NationalProviderIdentifier, I.SourceSystemId
    into #InterfacePatientPCPNPI
   FROM vwPortal_InterfacePatientMatched IPM 
  JOIN vwPortal_InterfacePatient IP 
    ON IPM.PatientIdentifier = IP.PatientIdentifier
 JOIN vwPortal_Provider P
    ON IP.PrimaryCareProviderNPI = P.NationalProviderIdentifier
  JOIN vwPortal_InterfaceSystem I ON IP.InterfaceSystemId = I.InterfaceSystemId  
    
    UPDATE PP SET ProviderID = PCPNPI.ProviderID, PP.SourceSystemID = PCPNPI.SourceSystemId, PP.OtherReferenceIdentifier = PCPNPI.NationalProviderIdentifier
    ,ModifyDateTime = GETDATE()
    FROM vwPortal_PatientProvider PP
    JOIN #InterfacePatientPCPNPI PCPNPI ON PP.PatientID = PCPNPI.LbPatientId
      where PP.ProviderID = 0
    
    
    INSERT INTO PrincetonMedLbportalProd..PatientProvider  (
       PatientID
      ,ProviderID
      ,SourceSystemID
      ,OtherReferenceIdentifier
      ,DeleteInd
      ,CreateDateTime
      ,ModifyDateTime
      ,CreateLBUserId
      ,ModifyLBUserId
    ) SELECT PCP.LbPatientId, PCP.ProviderID, PCP.SourceSystemId, PCP.NationalProviderIdentifier, 0, getdate(), getdate(), 1,1  FROM #InterfacePatientPCPNPI PCP
    LEFT JOIN vwPortal_PatientProvider PP ON PCP.LbPatientId = PP.PatientID
    where PP.PatientProviderID is null
    
    drop table #InterfacePatientPCPNPI
GO
