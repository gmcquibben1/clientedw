SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create PROCEDURE [dbo].[ETLPatientMedicationInterface]
	@fullLoadFlag bit = 0, 
	@BatchSize INT = 10000, 
	@InterfacePatientId VARCHAR(50) = NULL,
	@overrideMinIndex INT = NULL
AS

BEGIN	


/*================================================================
 Author:		Brian Russell
 Create date: 2016-03-16
 Description:	Load Patient Medication Data into EDW
 Modified By:Youping Liu
 Modified Date: 2016-04-05
 Notes:
        @fullLoadFlag have values of 1 or 0,  A value of 1 will rebuild the entire table
		@BatchSize INT = 10000,						Batch size 
		@InterfacePatientId VARCHAR(50) = NULL		Regenerate the data for a specific patietn
		@overrideMinIndex INT = NULL				Minimum InterfaceMedication Id value to pull data from 
 Notes: @dataSource can have these values: 'interface', 'CCLF'
       @fullLoadFlag have values of 1 or 0
 Modification:	Separate procedure for interface and CCCLF
 Main Steps: 
        1. truncate table [PatientMedicationProcessQueue]
        2. Populated data to [PatientMedicationProcessQueue] from source
        3. Call Procedures to merge dimensional tables -  [dbo].[MedicationCodeSystemType],[dbo].[MedicationFormType],[dbo].[MedicationRouteType],[dbo].[Medication]
        4. Get LBPatient ID from PatientIdReference
        5. Dedup based on InterfaceSystemId, LbPatientId, MedicationId,MedicationStartDate
        6. Merge data to PatientLabOrder
 Log Tracking:
        1. track table the last run date: [dbo].[EDWTableLoadTracking]
        2. log the procedure running status




	Version		Date		Author		Jira		Change
	-------		----------	------		------		------
				2016-04-18		YL					Populate three fields: providerID,[SIGCode],[GenericAllowedInd] from Interface
				2016-05-17		YL					[SourceSystemID] instead of [InterfaceSystemId]
				2016-05-26		YL		LBDM 747	add MedicationEndDate to the one of unique keys.
				2016-06-07		YL		LBAN 2712	timeout issue due to large amount of data for southbend
				2016-07-14		YL		LBAN 2905	Merge faile to to dup records caused by NULL value and '1900-01-01'
				2016-08-02		YL					Add ProviderNPI, skip providerID
				2016-09-05		YL		LBDM 1154	Remove MedicationEndDate to the one of unique keys.
	2.1.2       2016-12-22      YL      LBETL-357	Optimize the last two steps to speed up process time.
	2.1.2       2016-12-31      CL      LBETL-359	Fixed instruction truncation issue to 50 characters
	2.1.2       2016-12-31      CL      LBETL-360	MedicationEndDate and MedicationQuanity not propagating to EDW
										LBETL-360	There is a medication that has two end dates. They both have the same Start Date. Because of how the ETL code is written, only one is getting all the way through the processes because one gets inserted, then the other updates i
													Changed code to only overwrite the update if the value is not null
	2.1.2		2017-01-17		CJL		LBETL-360	Made changes to the stored procedure to handle the records in batches, to reduce the load on the temp tables
													Removed the sorts on create dates. Since the records in the Interface table are never updated and it has a incremental seeded key, we can use that 
													to implicitly sort. higher keys are newer records
	2.1.2		2017-02-07		CJL		LBETL-360, LBETL-740, LBETL-743   -- Corrected issue with MERGE statement with multiple record updates.  Fixed issues
													with NULL end and start dates	

	2.1.2		2017-03-05		CJL		LBETL-965	Corrected stored procedure to handle quantity values greater than 10 characters in length, corrected missing MedicationCode from Paritition On Enddate
	2.1.2		2017-04-11		CJL		LBETL-1108	Medications are not crossing over from Portal To Edw .The Med has a Start Date of '1/1'/1900', Excluded 'Unkown' code types 


	


================================================================ */

	--DECLARE
	--@fullLoadFlag bit = 0, 
	--@BatchSize INT = 10000, 
	--@InterfacePatientId VARCHAR(50) = NULL,
	--@overrideMinIndex INT = 0

	SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientMedication';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientMedicationInterface';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @NUMROWS INT
	DECLARE @MatchDate  DATETIME = CAST('2900-01-01' AS DATETIME); 

	
	BEGIN TRY






		
			--set @fullLoadFlag =1
	
			IF NOT EXISTS ( SELECT top 1 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] = @dataSource AND [EDWName] =@EDWName)
			  BEGIN
			  
					 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed

			  END

			SELECT @RecordCountBefore=count(1)
			FROM [dbo].[PatientMedication];



			--1) Retrieve the last run time id values.  We are going to pull both the date and the last run time id. If the last max id 
			--is null, calculate a best guese based on the 
			--We are changing away from using date values for query purposes as it is more efficient to query by the Id value.  Since
			--the id is a seeded, integer greater id values are implicitly newer. It also allows us to pick up records that come in while the 
			--procedure is running the next time it runs
			IF @fullLoadFlag = 1
			BEGIN
				UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
					SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME), maxSourceIdProcessed= 0
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] =@dataSource AND [EDWName] =@EDWName
			END

			 SET @lastDateTime = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
										FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
										WHERE [EDWtableName] = @EDWtableName 
										AND [dataSource] =@dataSource AND [EDWName] =@EDWName);
	
	
		
			SET  @lastIndex = (SELECT maxSourceIdProcessed
										FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
										WHERE [EDWtableName] = @EDWtableName 
										AND [dataSource] =@dataSource AND [EDWName] =@EDWName);

			IF @lastIndex IS NULL 
			BEGIN
				SET @lastIndex= (SELECT MIN(InterfacePatientMedicationId) FROM  vwportal_InterfacePatientMedication IPV WITH (NOLOCK) WHERE IPV.CreateDateTime >= @lastDateTime ) 
			END
			IF @overrideMinIndex IS NOT NULL
			BEGIN
				SET @lastIndex= @overrideMinIndex
			END


			SET @maxIndex = (SELECT MAX(InterfacePatientMedicationId) FROM  vwportal_InterfacePatientMedication IPV WITH (NOLOCK));
	

	/*

	DROP TABLE #PatientMedicationQuePatientIdentifier
	DROP TABLE #PatientMedicationQuePatientIdentifieBATCH
				DROP TABLE #PatientMedication
						DROP TABLE #PatientMedicationStaging
	*/


	
			--2) Get the full list of patient identifiers that we are going to be dealing with
			--We are going to breaking this up into batches along patient id lines.
			SELECT DISTINCT IP.PatientIdentifier, IP.InterfaceSystemId 
			INTO #PatientMedicationQuePatientIdentifier 
			FROM [dbo].[vwPortal_InterfacePatientMedication] IPV with (nolock),
					[dbo].[vwPortal_InterfacePatient] IP with (nolock), 
				 [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) 
				WHERE ( IPV.InterfacePatientMedicationId >= @lastIndex  AND IPV.InterfacePatientMedicationId <= @maxIndex )
				AND ( ( @InterfacePatientId IS NULL )  OR ( @InterfacePatientId = IP.InterfacePatientId))
				 AND IPV.InterfacePatientId = IP.InterfacePatientId  
				AND IP.InterfaceSystemId = ISS.[InterfaceSystemId] AND IP.PatientIdentifier IS NOT NULL
	  


			--3) Loop throught all of the batches to improve memory efficency
			WHILE 1 = 1
			BEGIN


				SELECT TOP (@BatchSize) PatientIdentifier, InterfaceSystemId
				 INTO #PatientMedicationQuePatientIdentifieBATCH 
					FROM #PatientMedicationQuePatientIdentifier;
				Create  NONCLUSTERED INDEX IDX_PatientMedicationQuePatientIdentifieBATCH_PatientIdentifier
				 ON #PatientMedicationQuePatientIdentifieBATCH (PatientIdentifier);
			
 			   SET @NUMROWS = ( SELECT COUNT(1) FROM #PatientMedicationQuePatientIdentifieBATCH)

				IF @NUMROWS <> 0 
				BEGIN
			
				-- 1.remove records from PatientMedicationProcessQueue
				TRUNCATE TABLE [dbo].[PatientMedicationProcessQueue]

				-- 2. populate data into PatientMedicationProcessQueue
				

			
				INSERT INTO [dbo].[PatientMedicationProcessQueue]
				   ([InterfacePatientMedicationId] ,[InterfacePatientID],[InterfaceSystemId], [LbPatientId]
					   ,[MedicationCode] ,[MedicationCodeName] ,[MedicationName]
					   ,[MedicationDose] ,[MedicationForm] ,[MedicationRoute]
					   ,[MedicationStartDate] ,[MedicationEndDate] ,[MedicationInstructions]
					   ,[MedicationQuantity] ,[MedicationRefills] ,[SampleInd]
					   ,[Clinician] ,[MedicationComment] ,[CreateDateTime]
					   ,[StatusDescription] ,[EncounterIdentifier],[SIGCode],
						[GenericAllowedInd], [PrescribedBy] ,SourceSystemId 
						,PatientIdentifier,ProviderId
					   )

						SELECT  [InterfacePatientMedicationId],ipm.[InterfacePatientID]	,
								ipm.InterfaceSystemId  AS InterfaceSystemId, idref.LbPatientId
								,[MedicationCode], CASE WHEN LEFT(MedicationCodeName, 3) = 'NDC' THEN 'NDC' WHEN LEFT(MedicationCodeName, 2) = 'RX' THEN 'RX' ELSE 'OTHER' END  as [MedicationCodeName],[MedicationName]
								,[MedicationDose],isnull([MedicationForm],'') AS [MedicationForm] ,isnull([MedicationRoute],'') AS [MedicationRoute] 
								,[MedicationStartDate],[MedicationEndDate],LEFT([MedicationInstructions],250) MedicationInstructions
								,[MedicationQuantity],[MedicationRefills],[SampleInd]
								,'' [Clinician]	,[MedicationComment],ipm.[CreateDateTime] 
								, ipm.StatusDescription, ipm.EncounterIdentifier,ipm.[SIGCode],
						        ipm.[GenericAllowedInd], ipm.[PrescribedByNPI],ISS.SourceSystemId 
								,IP.PatientIdentifier, NULL as ProviderId
						FROM [dbo].[vwPortal_InterfacePatientMedication] ipm with (nolock),
						 [dbo].[vwPortal_InterfaceSystem]  ISS  with (nolock), 
				         [dbo].[vwPortal_InterfacePatient] IP  with (nolock),
						 #PatientMedicationQuePatientIdentifieBATCH batch, 
						 PatientIdReference idRef with (nolock) 	  
						WHERE
							(ipm.InterfacePatientMedicationId >= @lastIndex  AND ipm.InterfacePatientMedicationId <= @maxIndex) 
							AND ipm.InterfaceSystemId = ISS.InterfaceSystemId AND  ipm.InterfacePatientId = IP.InterfacePatientId AND  ipm.InterfaceSystemId = IP.InterfaceSystemId  
									 AND ISNULL(ipm.MedicationCode,'') <> '' AND ipm.[MedicationCode] <> 'Unknown'
							AND IP.PatientIdentifier = batch.PatientIdentifier
							AND IP.InterfaceSystemId = batch.InterfaceSystemId	
	  							AND  idRef.SourceSystemId = ISS.SourceSystemId 
								  AND idRef.ExternalId = batch.PatientIdentifier 
								  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
								  AND idRef.DeleteInd = 0 
							



	  			SET @RecordCountSource = @RecordCountSource + (SELECT COUNT(1) FROM [PatientMedicationProcessQueue]);	             


					-- 3. call procedures[dbo].[ETLMedicationFormType],[dbo].[ETLMedicationRouteType],[dbo].[ETLMedication]
					-- to update dim tables
					EXEC [dbo].[ETLMedicationFormType];
					EXEC [dbo].[ETLMedicationRouteType];
					EXEC [dbo].[ETLMedication];

					---rebuild index
				    
					IF EXISTS(SELECT NAME FROM sys.indexes WHERE NAME = 'IDX_Patient_Medication_InterfacePatientID')
						BEGIN
								ALTER INDEX [IDX_Patient_Medication_InterfacePatientID] ON PatientMedicationProcessQueue REBUILD; 
						END
					ELSE
						BEGIN
							CREATE NONCLUSTERED INDEX [IDX_Patient_Medication_InterfacePatientID] ON [dbo].[PatientMedicationProcessQueue]
							(
								PatientIdentifier  ASC
							)
							INCLUDE ( SourceSystemId,
								[MedicationCode],
								[MedicationForm],
								[MedicationRoute],
								[EncounterIdentifier],
								[PrescribedBy],
								[MedicationCodeName]) 
					END

				   
			
					SELECT * INTO   #PatientMedicationStaging
					FROM 	
			
					(	


				
					SELECT DISTINCT  pm.LbPatientId,
								pm.SourceSystemId,	pm.MedicationCode,	pm.MedicationCodeName,  
								medName.MedicationName,	medDose.MedicationDose,	medForm.MedicationForm,
								medRoute.MedicationRoute,	pm.MedicationStartDate,	medEndDate.MedicationEndDate,
								medInstructions.MedicationInstructions, medQty.MedicationQuantity, medRefills.MedicationRefills,
								sCode.SIGCode,	Sind.SampleInd, generic.GenericAllowedInd,
								prescribed.PrescribedBy, medComment.MedicationComment,	
								statusdesc.StatusDescription,	encId.EncounterIdentifier,	provid.ProviderID
								, CASE WHEN  pm.MedicationCodeName ='NDC'  OR pm.MedicationCodeName ='OTHER' then pm.MedicationCode ELSE '' END AS [NDCCode]
								, CASE WHEN LEFT(pm.MedicationCodeName,2)='RX' THEN pm.MedicationCode ELSE '' END AS [RxNormCode]
				
						--,pm.rowNumber as pmrn
						--,medName.row_nbr as medName
						--,medDose.row_nbr as medDose
						--,medForm.row_nbr as medForm
						--,medRoute.row_nbr as medRoute
						--,medQty.row_nbr as medQty
						--,medInstructions.row_nbr as medInstructions
						--,medRefills.row_nbr as medRefills
						--,sCode.row_nbr as sCode
						--,Sind.row_nbr as Sind
						--,prescribed.row_nbr as prescribed
						--,medComment.row_nbr as medComment
						--,statusdesc.row_nbr as statusdesc
						--,generic.row_nbr as generic
						--,encId.row_nbr as encId
						--,provid.row_nbr as provid
						--,medEndDate.row_nbr as row_nbr
		
		

				
					FROM 
						(SELECT lbPatientId, SourceSystemId, MedicationCodeName, MedicationCode, MedicationStartDate, MedicationEndDate
								
								,ROW_NUMBER() OVER
								( 
									PARTITION BY SourceSystemId, lbPatientId, MedicationCode, MedicationCodeName,
									ISNULL( MedicationStartDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId DESC 

								) AS rowNumber
							FROM [dbo].[PatientMedicationProcessQueue] ) pm
			
						LEFT OUTER JOIN
									 (SELECT lbPatientId,SourceSystemId,  MedicationStartDate,MedicationEndDate, MedicationCode, MedicationCodeName,
 								   ROW_NUMBER() OVER
									( 
										PARTITION BY lbPatientId,SourceSystemId,  MedicationCode, MedicationCodeName,
											 ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
										ORDER BY 	InterfacePatientMedicationId	DESC 

									) AS row_nbr
								  FROM [PatientMedicationProcessQueue]  v7 WITH (NOLOCK)  WHERE
									 MedicationEndDate IS NOT NULL  ) medEndDate 
								  ON medEndDate.lbPatientId = pm.lbPatientId  AND medEndDate.MedicationCode = pm.MedicationCode AND  medEndDate.MedicationCodeName = pm.MedicationCodeName
								  AND  ISNULL( medEndDate.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
								  --AND  ISNULL( medEndDate.MedicationEndDate ,@MatchDate) = ISNULL(pm.MedicationEndDate ,@MatchDate)
								  AND medEndDate.SourceSystemId = pm.SourceSystemId
						LEFT OUTER JOIN
								  (SELECT lbPatientId,  SourceSystemId, MedicationStartDate, --MedicationEndDate, 
								  MedicationName, MedicationCode, MedicationCodeName
 								   ,ROW_NUMBER() OVER
									( 
										PARTITION BY lbPatientId, SourceSystemId, MedicationCode, MedicationCodeName,  
											 ISNULL( MedicationStartDate ,@MatchDate)--,  ISNULL( MedicationEndDate ,@MatchDate)
										ORDER BY 	InterfacePatientMedicationId	DESC 

									) AS row_nbr
								  FROM [PatientMedicationProcessQueue]  v3 WITH (NOLOCK)  WHERE
									 MedicationName IS NOT NULL  AND MedicationName <> '' ) medName 
								 ON medName.lbPatientId = pm.lbPatientId  
									AND medName.MedicationCode = pm.MedicationCode AND  medName.MedicationCodeName = pm.MedicationCodeName
								  AND  ISNULL( medName.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
								  --AND  ISNULL( medName.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								  AND medName.SourceSystemId = pm.SourceSystemId

						LEFT OUTER JOIN
									 (SELECT lbPatientId, SourceSystemId, MedicationStartDate ,-- MedicationEndDate,
									 MedicationDose, MedicationCode, MedicationCodeName,
 								   ROW_NUMBER() OVER
									( 
										PARTITION BY lbPatientId, SourceSystemId,    MedicationCode, MedicationCodeName,
											 ISNULL( MedicationStartDate ,@MatchDate)--,  ISNULL( MedicationEndDate ,@MatchDate)
										ORDER BY 	InterfacePatientMedicationId	DESC 

									) AS row_nbr
								  FROM [PatientMedicationProcessQueue]  v4 WITH (NOLOCK)  WHERE
									 MedicationDose IS NOT NULL  AND MedicationDose <> '' ) medDose 
								  ON medDose.lbPatientId = pm.lbPatientId  AND medDose.SourceSystemId = pm.SourceSystemId
								   AND medDose.MedicationCode = pm.MedicationCode AND  medDose.MedicationCodeName = pm.MedicationCodeName
								  AND  ISNULL( medDose.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
								  --AND  ISNULL( medDose.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								  AND medDose.SourceSystemId = pm.SourceSystemId
	
						LEFT OUTER JOIN
									 (SELECT lbPatientId,SourceSystemId, MedicationStartDate, -- MedicationEndDate,
									  MedicationForm, MedicationCode, MedicationCodeName,
 								   ROW_NUMBER() OVER
									( 
										PARTITION BY lbPatientId, SourceSystemId,    MedicationCode, MedicationCodeName, 
											 ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
										ORDER BY 	InterfacePatientMedicationId	DESC 

									) AS row_nbr
								  FROM [PatientMedicationProcessQueue]  v5 WITH (NOLOCK)  WHERE
									 MedicationForm IS NOT NULL  AND MedicationForm <> '' ) medForm 
								  ON medForm.lbPatientId = pm.lbPatientId AND  medForm.MedicationCode = pm.MedicationCode
								   AND  medForm.MedicationCodeName = pm.MedicationCodeName
								  AND  ISNULL( medForm.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
								--  AND  ISNULL( medForm.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								  AND medForm.SourceSystemId = pm.SourceSystemId
			
						LEFT OUTER JOIN
									 (SELECT lbPatientId,SourceSystemId,  MedicationStartDate,
									-- MedicationEndDate,
									 MedicationRoute, MedicationCode, MedicationCodeName,
 								   ROW_NUMBER() OVER
									( 
										PARTITION BY lbPatientId, SourceSystemId,   MedicationCode, MedicationCodeName, 
											 ISNULL( MedicationStartDate ,@MatchDate)-- , ISNULL( MedicationEndDate ,@MatchDate)
										ORDER BY 	InterfacePatientMedicationId	DESC 

									) AS row_nbr
								  FROM [PatientMedicationProcessQueue]  v6 WITH (NOLOCK)  WHERE
									 MedicationRoute IS NOT NULL  AND MedicationRoute <> '' ) medRoute 
								  ON medRoute.lbPatientId = pm.lbPatientId AND   medRoute.MedicationCode = pm.MedicationCode 
								  AND  medRoute.MedicationCodeName = pm.MedicationCodeName
								  AND  ISNULL( medRoute.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
								--  AND  ISNULL( medRoute.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								  AND medRoute.SourceSystemId = pm.SourceSystemId
				
					
						LEFT OUTER JOIN
									 (SELECT lbPatientId, SourceSystemId, 
									  MedicationStartDate,
								--	  MedicationEndDate,
									 MedicationInstructions, MedicationCode, MedicationCodeName,
 								   ROW_NUMBER() OVER
									( 
										PARTITION BY lbPatientId, SourceSystemId,  MedicationCode,  MedicationCodeName,
											 ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
										ORDER BY 	InterfacePatientMedicationId	DESC 

									) AS row_nbr
								  FROM [PatientMedicationProcessQueue]  v8 WITH (NOLOCK)  WHERE
									 MedicationInstructions IS NOT NULL AND MedicationInstructions <> ''  ) medInstructions 
								  ON medInstructions.lbPatientId = pm.lbPatientId  AND medInstructions.MedicationCode = pm.MedicationCode 
								  AND  medInstructions.MedicationCodeName = pm.MedicationCodeName
								  AND  ISNULL( medInstructions.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
								--  AND  ISNULL( medInstructions.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								  AND medInstructions.SourceSystemId = pm.SourceSystemId
						LEFT OUTER JOIN
									(SELECT lbPatientId, SourceSystemId, 
									 MedicationStartDate,
								--	 MedicationEndDate ,
									MedicationQuantity, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY lbPatientId, SourceSystemId,  MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 

								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v9 WITH (NOLOCK)  WHERE
									MedicationQuantity IS NOT NULL AND MedicationQuantity <> ''  ) medQty 
								ON medQty.lbPatientId = pm.lbPatientId  AND medQty.MedicationCode = pm.MedicationCode AND  medQty.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( medQty.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
								--AND  ISNULL( medQty.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								AND medQty.SourceSystemId = pm.SourceSystemId
						LEFT OUTER JOIN
									(SELECT lbPatientId,SourceSystemId, 
									 MedicationStartDate,
									-- MedicationEndDate,
									MedicationRefills, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY lbPatientId, SourceSystemId,  MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 

								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v10 WITH (NOLOCK)  WHERE
									MedicationRefills IS NOT NULL AND MedicationRefills <> ''  ) medRefills 
								ON medRefills.lbPatientId = pm.lbPatientId  AND medRefills.MedicationCode = pm.MedicationCode AND  medRefills.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( medRefills.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
							--	AND  ISNULL( medRefills.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								AND medRefills.SourceSystemId = pm.SourceSystemId

					LEFT OUTER JOIN
								(SELECT lbPatientId, SourceSystemId, 
								MedicationStartDate ,
							--	 MedicationEndDate ,
								SIGCode, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY lbPatientId, SourceSystemId,   MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 

								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v11 WITH (NOLOCK)  WHERE
									SIGCode IS NOT NULL AND SIGCode <> ''  ) sCode 
								ON sCode.lbPatientId = pm.lbPatientId AND sCode.MedicationCode = pm.MedicationCode AND  sCode.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( sCode.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
							--	AND  ISNULL( sCode.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								AND sCode.SourceSystemId = pm.SourceSystemId
					LEFT OUTER JOIN
								(SELECT lbPatientId, SourceSystemId, 
									 MedicationStartDate ,
									-- MedicationEndDate ,
									SampleInd, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY lbPatientId, SourceSystemId,  MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 

								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v12 WITH (NOLOCK)  WHERE
									SampleInd IS NOT NULL AND SampleInd <> ''  ) Sind 
								ON Sind.lbPatientId = pm.lbPatientId AND Sind.MedicationCode = pm.MedicationCode AND  Sind.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( Sind.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
							--	AND  ISNULL( Sind.MedicationEndDate ,@MatchDate) = ISNULL(pm.MedicationEndDate ,@MatchDate)
								AND Sind.SourceSystemId = pm.SourceSystemId
					LEFT OUTER JOIN
								(SELECT lbPatientId, SourceSystemId,
								  MedicationStartDate ,
								 -- MedicationEndDate ,
								 GenericAllowedInd, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY lbPatientId, SourceSystemId,  MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 

								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v13 WITH (NOLOCK)  WHERE
									GenericAllowedInd IS NOT NULL AND GenericAllowedInd <> ''  ) generic 
								ON generic.lbPatientId = pm.lbPatientId AND generic.MedicationCode = pm.MedicationCode AND  generic.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( generic.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
							--	AND  ISNULL( generic.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								AND generic.SourceSystemId = pm.SourceSystemId
					LEFT OUTER JOIN
								(SELECT lbPatientId,SourceSystemId,  MedicationStartDate ,
							--	 MedicationEndDate ,
								PrescribedBy, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY lbPatientId , SourceSystemId,  MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 

								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v14 WITH (NOLOCK)  WHERE
									PrescribedBy IS NOT NULL AND PrescribedBy <> ''  ) prescribed 
								ON prescribed.lbPatientId = pm.lbPatientId AND prescribed.MedicationCode = pm.MedicationCode AND  prescribed.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( prescribed.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
							--	AND  ISNULL( prescribed.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								AND prescribed.SourceSystemId = pm.SourceSystemId
					LEFT OUTER JOIN
								(SELECT lbPatientId, SourceSystemId,
								  MedicationStartDate ,
								 -- MedicationEndDate ,
								 MedicationComment, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY lbPatientId,SourceSystemId,   MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 

								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v15 WITH (NOLOCK)  WHERE
									MedicationComment IS NOT NULL AND MedicationComment <> ''  ) medComment 
								ON medComment.lbPatientId = pm.lbPatientId AND medComment.MedicationCode = pm.MedicationCode AND  medComment.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( medComment.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
							--	AND  ISNULL( medComment.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								AND medComment.SourceSystemId = pm.SourceSystemId

					LEFT OUTER JOIN
								(SELECT lbPatientId, SourceSystemId, 
									 MedicationStartDate ,
								--	 MedicationEndDate ,
									StatusDescription, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY lbPatientId, SourceSystemId,   MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate)--, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 

								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v16 WITH (NOLOCK)  WHERE
									StatusDescription IS NOT NULL AND StatusDescription <> ''  ) statusdesc 
								ON statusdesc.lbPatientId = pm.lbPatientId AND statusdesc.MedicationCode = pm.MedicationCode AND  statusdesc.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( statusdesc.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
							--	AND  ISNULL( statusdesc.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								AND statusdesc.SourceSystemId = pm.SourceSystemId
					LEFT OUTER JOIN
								(SELECT lbPatientId, SourceSystemId,
								  MedicationStartDate ,
								--  MedicationEndDate ,
								 EncounterIdentifier, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY SourceSystemId, lbPatientId,  MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate) --, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 

								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v17 WITH (NOLOCK)  WHERE
									EncounterIdentifier IS NOT NULL AND EncounterIdentifier <> ''  ) encId 
								ON encId.lbPatientId = pm.lbPatientId AND encId.MedicationCode = pm.MedicationCode AND  encId.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( encId.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
						--		AND  ISNULL( encId.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								AND encId.SourceSystemId = pm.SourceSystemId
					LEFT OUTER JOIN
								(SELECT lbPatientId, SourceSystemId, 
								 MedicationStartDate ,
							--	 MedicationEndDate ,
								ProviderId, MedicationCode, MedicationCodeName,
 								ROW_NUMBER() OVER
								( 
									PARTITION BY lbPatientId, SourceSystemId,  MedicationCode,  MedicationCodeName,
											ISNULL( MedicationStartDate ,@MatchDate) --, ISNULL( MedicationEndDate ,@MatchDate)
									ORDER BY 	InterfacePatientMedicationId	DESC 
								) AS row_nbr
								FROM [PatientMedicationProcessQueue]  v18 WITH (NOLOCK)  WHERE
									ProviderId IS NOT NULL AND ProviderId <> ''  ) provid 
								ON provid.lbPatientId = pm.lbPatientId AND provid.MedicationCode = pm.MedicationCode AND  provid.MedicationCodeName = pm.MedicationCodeName
								AND  ISNULL( provid.MedicationStartDate ,@MatchDate) = ISNULL(pm.MedicationStartDate ,@MatchDate)
						--		AND  ISNULL( provid.MedicationEndDate ,@MatchDate) = ISNULL(medEndDate.MedicationEndDate ,@MatchDate)
								AND provid.SourceSystemId = pm.SourceSystemId
					Where pm.rowNumber=1
					  AND (medName.row_nbr = 1 OR medName.row_nbr IS NULL)
					  AND (medDose.row_nbr = 1 OR medDose.row_nbr IS NULL)
					  AND (medForm.row_nbr = 1 OR medForm.row_nbr IS NULL)
					  AND (medRoute.row_nbr = 1 OR medRoute.row_nbr IS NULL)
					  AND (medEndDate.row_nbr = 1 OR medEndDate.row_nbr IS NULL)
					  AND (medInstructions.row_nbr = 1 OR medInstructions.row_nbr IS NULL)
					  AND (medQty.row_nbr = 1 OR medQty.row_nbr IS NULL)
					  AND (medRefills.row_nbr = 1 OR medRefills.row_nbr IS NULL)
					  AND (sCode.row_nbr = 1 OR sCode.row_nbr IS NULL)
					  AND (Sind.row_nbr = 1 OR Sind.row_nbr IS NULL)
					  AND (prescribed.row_nbr = 1 OR prescribed.row_nbr IS NULL)
					  AND (medComment.row_nbr = 1 OR medComment.row_nbr IS NULL)
					  AND (statusdesc.row_nbr = 1 OR statusdesc.row_nbr IS NULL)
					  AND (generic.row_nbr = 1 OR generic.row_nbr IS NULL)
					  AND (encId.row_nbr = 1 OR encId.row_nbr IS NULL)
					  AND (provid.row_nbr = 1 OR provid.row_nbr IS NULL)
					  ) A
					  					
				    CREATE INDEX IDX_NDCCode_MedicationCodeName ON #PatientMedicationStaging ([NDCCode],[RxNormCode]) 
					CREATE INDEX IDX_NDCCode_MedicationForm ON #PatientMedicationStaging (MedicationForm) 
					CREATE INDEX IDX_NDCCode_MedicationRoute ON #PatientMedicationStaging (MedicationRoute) ;
				-- merge the queued rows into the PatientMedication table
				--5 load deduped patient lab order data into temp table fisrt, data deduped by InterfaceSystemId, LbPatientId, MedicationId,MedicationStartDate 
			

					SELECT  DISTINCT
						LbPatientID, [MedicationID] ,t.SourceSystemId AS SourceSystemID,MedicationStartDate,
						[MedicationEndDate],LEFT( ISNULL( t.MedicationQuantity, ''), 10) AS MedicationQuantity,MedicationRefills,SIGCode,MedicationInstructions,SampleInd,
						ProviderId, GenericAllowedInd, 0 AS DeleteInd,MedicationComment,
						1 AS CreateLBUserId,1 AS ModifyLBUserId,
						MedicationName, ISNULL(MedicationForm, '') as MedicationForm, ISNULL(MedicationRoute, '') AS MedicationRoute, t.MedicationDose,
						t.[NDCCode], t.[RxNormCode], StatusDescription, EncounterIdentifier,PrescribedBy
						,@Today as [ModifyDateTime]
						INTO #PatientMedication
					FROM  #PatientMedicationStaging t 
						INNER JOIN [dbo].[Medication] m 
						ON ISNULL(m.NDCCode,'') = t.[NDCCode]
						and ISNULL(m.RxNormCode ,'')= t.[RxNormCode]
						LEFT JOIN MedicationFormType mft ON mft.Name = t.MedicationForm
						LEFT JOIN MedicationRouteType mrt ON mrt.Name = t.MedicationRoute
					--WHERE t.[NDCCode] <> '' AND t.[RxNormCode] <> ''
					
			       


			
      
				--	 select * from #PatientMedicationStaging where PrescribedByNPI is not null

					-- get source records counts
					SET @RecordCountSource=@@ROWCOUNT;

					---for merge key
					CREATE CLUSTERED INDEX CIX_#PatientMedication_PatientID ON  #PatientMedication (LbPatientId,MedicationId,[SourceSystemID],MedicationStartDate );



							MERGE INTO PatientMedication AS target
							USING 
									#PatientMedication  AS source
							ON 
								target.PatientId = source.LbPatientId 
								AND target.SourceSystemId = source.SourceSystemId 
								AND ( ( source.MedicationStartDate IS NOT NULL  AND  target.MedicationStartDate  = source.MedicationStartDate)
								OR (source.MedicationStartDate IS  NULL  AND  ISNULL(target.MedicationStartDate,@MatchDate) = ISNULL(source.MedicationStartDate ,@MatchDate)) )
								--AND ISNULL(target.MedicationEndDate,@MatchDate) = ISNULL(source.MedicationEndDate ,@MatchDate)
								AND (target.MedicationId = source.MedicationId )
							WHEN MATCHED THEN
								UPDATE SET NDCCode = source.NDCCode,
										   RxNormCode = source.RxNormCode,
										   MedicationId = source.MedicationId,
										   ProviderId =  ISNULL(source.ProviderId, target.ProviderId),
										   MedicationEndDate = ISNULL(source.MedicationEndDate, target.MedicationEndDate),
										   Quantity = ISNULL(source.MedicationQuantity, target.Quantity),
										   Refills = ISNULL( source.MedicationRefills,target.Refills) ,
										   SIGCode = ISNULL(source.SIGCode, target.SIGCode),
										   Instructions =  ISNULL(source.MedicationInstructions, target.Instructions) ,
										   SampleInd = ISNULL( source.SampleInd,target.SampleInd),
										   GenericAllowedInd = ISNULL(source.GenericAllowedInd, target.GenericAllowedInd),
										   Comment = ISNULL(source.MedicationComment, target.Comment),
										   ModifyDateTime = GETUTCDATE(),
										   ModifyLbUserId = 1,
										   StatusDescription = ISNULL(source.StatusDescription, target.StatusDescription),
										   EncounterIdentifier = ISNULL(source.EncounterIdentifier, target.EncounterIdentifier), 
										   MedicationNameFromSource = ISNULL(source.MedicationName, target.MedicationNameFromSource), 
										   MedicationFormFromSource =  ISNULL( source.MedicationForm, target.MedicationFormFromSource),
										   MedicationRouteFromSource =  ISNULL(source.MedicationRoute, target.MedicationRouteFromSource),
										   MedicationDoseFromSource = ISNULL(source.MedicationDose, target.MedicationDoseFromSource),
										   ProviderNPI= ISNULL(Source.PrescribedBy, target.ProviderNPI)
							WHEN NOT MATCHED BY target THEN
								INSERT (PatientID,MedicationID,SourceSystemID,EncounterID,ProviderID,MedicationStartDate,MedicationEndDate,Quantity,Refills,SIGCode,Instructions,
										SampleInd,GenericAllowedInd,DeleteInd,Comment,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,
										MedicationNameFromSource,MedicationFormFromSource,MedicationRouteFromSource,MedicationDoseFromSource,
										NDCCode,RxNormCode,StatusDescription,EncounterIdentifier,ProviderNPI)
								VALUES (source.LbPatientId,source.MedicationId,source.SourceSystemId,0,source.ProviderId,source.MedicationStartDate,source.MedicationEndDate,source.MedicationQuantity,source.MedicationRefills,source.SIGCode,source.MedicationInstructions,
										source.SampleInd,source.GenericAllowedInd,0,source.MedicationComment,GETUTCDATE(),GETUTCDATE(),1,1,
										source.MedicationName,source.MedicationForm,source.MedicationRoute,source.MedicationDose,
										source.NDCCode,source.RxNormCode,source.StatusDescription,source.EncounterIdentifier,Source.PrescribedBy);
		   
		
		
						
 							--Update the max processed time and drop the temp tables
							--SET @tmpmaxSourceTimeStampProcessed = 	(SELECT MAX(CreateDateTime) FROM #PatientMedicationQue )
							--IF @tmpmaxSourceTimeStampProcessed > @maxSourceTimeStampProcessed
							--BEGIN
							--	SET @maxSourceTimeStampProcessed = @tmpmaxSourceTimeStampProcessed
							--END


						DROP TABLE #PatientMedication
						DROP TABLE #PatientMedicationStaging
						TRUNCATE TABLE [PatientMedicationProcessQueue]

				END
		
		
			
			--Move onto the next batch
			--DELETE FROM #PatientMedicationQuePatientIdentifier WHERE PatientIdentifier IN (SELECT PatientIdentifier FROM #PatientMedicationQuePatientIdentifieBATCH) 
			
			DELETE FROM #PatientMedicationQuePatientIdentifier FROM 
					#PatientMedicationQuePatientIdentifier id, #PatientMedicationQuePatientIdentifieBATCH batch
			WHERE id.PatientIdentifier = batch.PatientIdentifier AND id.interfacesystemid  = batch.interfacesystemid
				OR id.PatientIdentifier IS NULL

			DROP TABLE #PatientMedicationQuePatientIdentifieBATCH
			IF @NUMROWS = 0 BREAK;

		END
		


		DROP table #PatientMedicationQuePatientIdentifier
		
     -- get total records from [dbo].PatientPatientMedication after merge 
		SELECT @RecordCountAfter=count(1)
		FROM [dbo].PatientMedication;

				
               		   
		-- update maxSourceTimeStamp with max CreateDateTime of #PatientMedicationQue
		UPDATE [Maintenance]..[EDWTableLoadTracking]
		SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex, updatedatetime = GETUTCDATE()
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] =@dataSource  AND [EDWName] = @EDWName


		-- get total records from PatientMedication after merge 
		SELECT @RecordCountAfter=COUNT(1)  FROM PatientMedication WITH (NOLOCK);

		-- Calculate records inserted and updated counts
		SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
		SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;



	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
	
END

GO
