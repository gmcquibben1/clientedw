SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 CREATE PROCEDURE [dbo].[internalPhysicianSummaryMetrics]
 /*===============================================================================================================================
	CREATED BY: 	Mike Hoxter
	CREATED ON:		YYYY-MM-DD
	INITIAL VER:	Initial Application Version
	MODULE:			(Optional) Module it applies to 		
	DESCRIPTION:	Description of what stored procedure does and / or why it was created.
	PARAMETERS:		(Optional) All non-obvious parameters must be described
	RETURN VALUE(s)/OUTPUT:	(Optional) Especially when returning specific values or error codes.
	PROGRAMMIN NOTES: (Optional) This is sometimes helpful to explain some implementation details that may not be obvious.
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========
	2.1.1	    2016-09-02      Youping     LBAN-3099   Change cost related fields from INT to decimal 2
	2.0.1HF	    2017-01-10		LM			LBAN-3439	remove patient status filter from summary metric calculation, also add 
	                                                    patientstatus to PhysicianSummary and PhysicianSummaryPivot
==================================================================================================================================*/
AS
BEGIN

SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PhysicianSummary';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalPhysicianSummaryMetrics';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

BEGIN TRY 
     

	--CHECK IF PhysicianSummary TABLE EXISTS
	IF OBJECT_ID(N'[dbo].[PhysicianSummary]', N'U') IS NOT NULL
		   DROP TABLE [dbo].[PhysicianSummary];

	BEGIN
		   CREATE TABLE [dbo].[PhysicianSummary](
		   [id] [int] IDENTITY(1,1) PRIMARY KEY,
		   [ProviderNPI] [varchar](50) NULL,
		   [Level1Id] [VARCHAR] (100) NULL, 
		   [Level1Name] [varchar](100) NULL, 
		   [Level2Id] [varchar](100) NULL, 
		   [Level2Name] [varchar](100) NULL, 
		   [Level3Id] [varchar](100) NULL, 
		   [Level3Name] [varchar](100) NULL, 
		   [Level4Id] [varchar](100) NULL, 
		   [Level4Name] [varchar](100) NULL, 
		   [Level5Id] [varchar](100) NULL, 
		   [Level5Name] [varchar](100) NULL,
		   [Level6Id] [varchar](100) NULL, 
		   [Level6Name] [varchar](100) NULL,
		   [ContractName] [varchar](100) NULL,
		   [PatientStatus] [varchar](50) NULL,
		   [Title] [varchar](150) NULL,
		   [Value]  Decimal(10,2) NULL
		   ) 
	END

	 SELECT Name AS patientStatus
	 INTO #patientStatus
	 FROM vwPortal_AllTypeTables 
     WHERE TableName = 'DefaultAnalyticsPatientStatusType'

	 CREATE CLUSTERED INDEX IDX_patientStatus ON #patientStatus (patientStatus);

	--INSERTING DATASET INTO TABLE
	INSERT INTO [dbo].PhysicianSummary (ProviderNPI, Level1Id, Level1Name, Level2Id, Level2Name, Level3Id, Level3Name, Level4Id, Level4Name, Level5Id, Level5Name, Level6Id, Level6Name, [ContractName], Title, Value,patientStatus)
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], 'TotalPatients', COUNT(DISTINCT(LbPatientId)),patientStatus
	FROM [dbo].OrgHierarchy O
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND 
	ProviderNPI IS NOT NULL
	GROUP BY O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--Get Claimshares =2 from MemberExtract, remove the MemberExtract from the join by Lan
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], 'ClaimShares', COUNT(DISTINCT(LbPatientId)),patientStatus
	FROM --[dbo].[MemberExtract] M INNER JOIN 	
	[dbo].OrgHierarchy O
	--ON M.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND ([Patient_Status] LIKE '%WithClaims%' or O.Patient_Status LIKE '%Opt in%') AND 
	O.ProviderNPI IS NOT NULL
	GROUP BY O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--Get Non Compliant Patients From CMeasure_Detail
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], 'NonCompliant', COUNT(DISTINCT(C.LbPatientId)),patientStatus
	FROM [dbo].[CMeasure_Detail] C
	INNER JOIN OrgHierarchy O
	ON C.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND 
	C.[Numerator] = 0 AND C.[Exclusion] = 0 AND O.ProviderNPI IS NOT NULL
	GROUP BY O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--Get 9 Month Wellness Patients From Wellness Aging Table
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], 'WellnessPatients', COUNT(C.LbPatientId),patientStatus
	FROM [dbo].[CCLF_WELLNESS_AGING] C
	INNER JOIN OrgHierarchy O
	ON C.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE C.[DAYS_SINCE_WELLNESS] > 270 AND O.ProviderNPI IS NOT NULL --AND [Patient_Status] NOT like '%Deceased%'
	GROUP BY O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus


	--Get ER Visits From MM_Metrics_Pivot & OrgHierarchy Table
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], M.[Title], COUNT(DISTINCT(M.LbPatientId)),patientStatus
	FROM [dbo].[MemMonth_Metrics] M
	INNER JOIN OrgHierarchy O
	ON M.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND 
	M.[Value] > 0 AND O.ProviderNPI IS NOT NULL AND M.[Title] = 'ErVisits' 
	AND M.DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, M.DOS_First, GETDATE()) -6 , M.DOS_First)
	GROUP BY M.[Title], O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--Get Inpatient Acute Admits From MM_Metrics_Pivot & OrgHierarchy Table
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], M.[Title], COUNT(DISTINCT(M.LbPatientId)),patientStatus
	FROM [dbo].[MemMonth_Metrics] M
	INNER JOIN OrgHierarchy O
	ON M.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND 
	M.[Value] > 0 AND O.ProviderNPI IS NOT NULL AND M.[Title] = 'InpatientAcuteAdmits' 
	AND M.DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, M.DOS_First, GETDATE()) -6 , M.DOS_First)
	GROUP BY M.[Title], O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--Get Hospice Admits From MM_Metrics_Pivot & OrgHierarchy Table
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], M.[Title], COUNT(DISTINCT(M.LbPatientId)),patientStatus
	FROM [dbo].[MemMonth_Metrics] M
	INNER JOIN OrgHierarchy O
	ON M.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND 
	M.[Value] > 0 AND O.ProviderNPI IS NOT NULL AND M.[Title] = 'HospiceAdmits' 
	AND M.DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, M.DOS_First, GETDATE()) -6 , M.DOS_First)
	GROUP BY M.[Title], O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--Get Home Health Admits From MM_Metrics_Pivot & OrgHierarchy Table
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], M.[Title], COUNT(DISTINCT(M.LbPatientId)),patientStatus
	FROM [dbo].[MemMonth_Metrics] M
	INNER JOIN OrgHierarchy O
	ON M.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND 
	M.[Value] > 0 AND O.ProviderNPI IS NOT NULL AND M.[Title] = 'HomeHealthAdmits' 
	AND M.DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, M.DOS_First, GETDATE()) -6 , M.DOS_First)
	GROUP BY M.[Title], O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--Get Chronic Patients From Patient_Summary_Pivot & OrgHierarchy Table
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], 'ChronicPatients', COUNT(DISTINCT(M.LbPatientId)),patientStatus
	FROM [dbo].[Patient_Summary_Pivot] M
	INNER JOIN OrgHierarchy O
	ON M.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND 
	O.ProviderNPI IS NOT NULL AND (M.[COPD] > 0 or M.Asthma > 0 OR M.CHF > 0 OR M.Diabetes > 0 OR M.Hypertension > 0 OR M.ESRD > 0)
	GROUP BY O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--Get total measure records where highlow = 1
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], 'MeasureDenominatorSum', sum(CD.Denominator),patientStatus 
	FROM CMeasure_Detail CD 
	INNER JOIN OrgHierarchy O ON CD.LbPatientId = O.LbPatientId
	JOIN CMeasure_Definitions DEF ON CD.MeasureID = DEF.MeasureID
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND 
	O.ProviderNPI IS NOT NULL and DEF.HighLow = 1
	GROUP BY O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--Get total Numerator = 1 measure records where highlow = 1
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], 'MeasureNumeratorSum', sum(CD.Numerator),patientStatus
	FROM CMeasure_Detail CD 
	INNER JOIN OrgHierarchy O ON CD.LbPatientId = O.LbPatientId
	JOIN CMeasure_Definitions DEF ON CD.MeasureID = DEF.MeasureID
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' AND 
	O.ProviderNPI IS NOT NULL and DEF.HighLow = 1
	GROUP BY O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--calculate avg PPPM Cost
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName], 'TotalPPPMCost', Avg(ISNULL(M.[12 Mo Cost],0))/12 ,patientStatus
	FROM [dbo].[Patient_Summary_Pivot] M
	INNER JOIN OrgHierarchy O
	ON M.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' and (O.Patient_Status LIKE '%WithClaims%' or O.Patient_Status LIKE '%Opt In%') AND
	 O.ProviderNPI IS NOT NULL 
	GROUP BY O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, [ContractName],patientStatus

	--add pppm target
	UNION ALL
	SELECT O.ProviderNPI , O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, O.[ContractName], 'PPPMCostTarget', AVG(M.[PPPM Cost Target]),patientStatus
	FROM MM_Metics_Pivot M
	INNER JOIN OrgHierarchy O
	ON M.LbPatientId = O.LbPatientId
	JOIN #patientStatus ps
	ON O.[Patient_Status] = ps.patientStatus
	WHERE --[Patient_Status] like 'Active%' and (O.Patient_Status LIKE '%WithClaims%' or O.Patient_Status LIKE '%Opt In%') AND 
	O.ProviderNPI IS NOT NULL AND DATEDIFF(MONTH, M.DOS_First, GETDATE()) < 15
	GROUP BY O.ProviderNPI, O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.Level3Id, O.Level3Name, O.Level4Id, O.Level4Name, O.Level5Id, O.Level5Name, O.Level6Id, O.Level6Name, O.[ContractName],patientStatus


	--UPDATE A
	--SET ContractName = B.ContractName
	--FROM [dbo].[PhysicianSummary] A
	--LEFT JOIN
	--(
	--SELECT DISTINCT(ProviderNPI), ContractName
	--FROM [dbo].[OrgHierarchy]
	--WHERE ProviderNPI IS NOT NULL) B
	--ON A.ProviderNPI = B.ProviderNPI
	--

	--DROPPING, IF PhysicianSummaryPivot TABLE EXISTS
	IF EXISTS(SELECT 1 FROM [dbo].[PhysicianSummary])
	BEGIN

	--DROPPING, IF PhysicianSummaryPivot TABLE EXISTS
	IF OBJECT_ID(N'[dbo].[PhysicianSummaryPivot]', N'U') IS NOT NULL
		   DROP TABLE [dbo].[PhysicianSummaryPivot];

	--declaring varialbes (added on 1/13/2015 by IH)
	DECLARE @colBottom NVARCHAR(MAX), @SQL NVARCHAR(MAX), @createTable NVARCHAR(MAX), @colTable NVARCHAR(MAX), @isNullCol NVARCHAR(MAX)

	--concatenating the distinct items of column title in one row using FOR XML PATH
	SET @colTable = STUFF((SELECT DISTINCT ', ['+CAST(Title AS NVARCHAR(200))+']'+ ' INT' FROM [dbo].[PhysicianSummary] FOR XML PATH('')),1,1, N'');
	SET @colBottom = STUFF((SELECT DISTINCT ', ['+CAST(Title AS NVARCHAR(200))+']' FROM [dbo].[PhysicianSummary] FOR XML PATH('')),1,1, N'');
	SET @isNullCol = STUFF((SELECT DISTINCT ', ISNULL(['+CAST(Title AS NVARCHAR(200))+'], 0.0) AS '+'['+Title+']' FROM [dbo].[PhysicianSummary] FOR XML PATH('')),1,1, N'');

	--creating a table
	SET @createTable = N'Create Table [dbo].[PhysicianSummaryPivot] ([ProviderNPI] [varchar](20), 
		   [Level1Id] [VARCHAR] (30) NULL, 
		   [Level1Name] [varchar](100) NULL, 
		   [Level2Id] [varchar](30) NULL, 
		   [Level2Name] [varchar](100) NULL, 
		   [Level3Id] [varchar](30) NULL, 
		   [Level3Name] [varchar](100) NULL, 
		   [Level4Id] [varchar](30) NULL, 
		   [Level4Name] [varchar](100) NULL, 
		   [Level5Id] [varchar](30) NULL, 
		   [Level5Name] [varchar](100) NULL,
		   [Level6Id] [varchar](30) NULL, 
		   [Level6Name] [varchar](100) NULL,
		   [ContractName] VARCHAR(50) NULL,
		   [PatientStatus] VARCHAR(50) NULL, '+@colTable+')'

	SET @SQL = '(SELECT [ProviderNPI], [Level1Id], Level1Name, Level2Id, Level2Name, Level3Id, Level3Name, Level4Id, Level4Name, Level5Id, Level5Name, Level6Id, Level6Name, [ContractName],[PatientStatus], '+@isNullCol+' 
				INTO [dbo].[PhysicianSummaryPivot] 
				FROM 
				(SELECT [ProviderNPI], [Level1Id], Level1Name, Level2Id, Level2Name, Level3Id, Level3Name, Level4Id, Level4Name, Level5Id, Level5Name, Level6Id, Level6Name, [ContractName],[PatientStatus], [Title], [Value]
				FROM [dbo].[PhysicianSummary]
				) derivedTable
				PIVOT
				(SUM(Value) FOR Title IN ('+@colBottom+')) pivotTable)' 

	EXECUTE sp_executesql @SQL

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'ChronicPatients')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD ChronicPatients INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'ClaimShares')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD ClaimShares INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'MeasureDenominatorSum')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD MeasureDenominatorSum INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'MeasureNumeratorSum')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD MeasureNumeratorSum INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'NonCompliant')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD NonCompliant INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'TotalPatients')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD TotalPatients INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'TotalPPPMCost')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD TotalPPPMCost INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'WellnessPatients')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD WellnessPatients INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'ErVisits')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD ErVisits INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'InpatientAcuteAdmits')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD InpatientAcuteAdmits INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'HospiceAdmits')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD HospiceAdmits INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'HomeHealthAdmits')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD HomeHealthAdmits INT NOT NULL DEFAULT(0)
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PhysicianSummaryPivot' AND COLUMN_NAME = 'PPPMCostTarget')
	BEGIN
		ALTER TABLE [PhysicianSummaryPivot] ADD PPPMCostTarget INT NOT NULL DEFAULT(0)
	END

	ALTER TABLE [dbo].[PhysicianSummaryPivot] ADD [ID] [int] IDENTITY(1,1) PRIMARY KEY

	END

	-----Log Information
		
	 
		SELECT @InsertedRecord=count(1)
		FROM [PhysicianSummary] ;

          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
