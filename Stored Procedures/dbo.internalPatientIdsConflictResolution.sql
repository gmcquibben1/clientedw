SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPatientIdsConflictResolution] 
AS
BEGIN

DECLARE @GroupFormationRuleId INT, @GroupDisambiguationRuleId INT
CREATE TABLE #PatientIdsConflictsResolutionPoints -- DROP TABLE #PatientIdsConflictsResolutionPoints
(Rooted_Id VARCHAR(2000), Group_Id UNIQUEIDENTIFIER, FAMILY_ID UNIQUEIDENTIFIER, ResolutionRule INT, ResolutionPoints INT)

-- BE SURE TO CHANGE
CREATE TABLE #AppAuditLog (AuditCategoryTypeId INT, AuditActionTypeId INT DEFAULT 3, LBUserId INT Default 1, PatientId INT, Description VARCHAR(300), EventDateTime DateTime DEFAULT GetDate())
CREATE TABLE #ResolvedIds (ROOTED_ID VARCHAR(2000), GROUP_ID UNIQUEIDENTIFIER, FAMILY_ID UNIQUEIDENTIFIER,GroupDisambiguationRuleId INT)
CREATE TABLE #IdLastActivity (FAMILY_ID UNIQUEIDENTIFIER,MedicareNo VARCHAR(100),LastActivity DateTime)
CREATE TABLE #DemergeDeletes (Family_Id UniqueIdentifier)

-- Last Activity 
INSERT INTO #IdLastActivity (FAMILY_ID,MedicareNo,LastActivity) -- SELECT * FROM #IdLastActivity
SELECT family_Id, MedicareNo, MAX(CDT) FROM 
(
	SELECT Me.Family_id,ME.MedicareNo, MAX(CLM_FROM_DT) CDT
	FROM   MemberExtract ME INNER JOIN CCLF_5_PartB_Physicians A ON Me.medicareNo = A.BENE_HIC_NUM INNER JOIN PatientIdsConflictsXReference PIXR ON ME.family_id = PIXR.Family_Id --WHERE Me.Holdback IS NOT NULL
	GROUP BY Me.Family_id,ME.MedicareNo
	UNION ALL 
	SELECT Me.Family_id,ME.MedicareNo, MAX(CLM_FROM_DT) CDT
	FROM   MemberExtract ME INNER JOIN CCLF_1_PartA_Header A ON Me.medicareNo = A.BENE_HIC_NUM INNER JOIN PatientIdsConflictsXReference PIXR ON ME.family_id = PIXR.Family_Id	--WHERE Me.Holdback IS NOT NULL
	GROUP BY Me.Family_id,ME.MedicareNo
	UNION ALL 
	SELECT Me.Family_id,ME.MedicareNo, MAX(CLM_LINE_FROM_DT) CDT
	FROM   MemberExtract ME INNER JOIN CCLF_7_PartD A ON Me.medicareNo = A.BENE_HIC_NUM INNER JOIN PatientIdsConflictsXReference PIXR ON ME.family_id = PIXR.Family_Id	--WHERE Me.Holdback IS NOT NULL
	GROUP BY Me.Family_id,ME.MedicareNo
	UNION ALL 
	SELECT Me.Family_ID, ME.MedicareNo, Max(MRA.Timeline) CDT
	FROM MemberExtract ME INNER JOIN MemberRosterArchives MRA ON Me.medicareNo = MRA.HICNO  INNER JOIN PatientIdsConflictsXReference PIXR ON ME.family_id = PIXR.Family_Id
	WHERE StatusDesc <> 'Dropped'
	GROUP BY Me.Family_id,ME.MedicareNo 
) X 
GROUP BY family_Id, MedicareNo

INSERT INTO #IdLastActivity (FAMILY_ID,MedicareNo,LastActivity) -- SELECT * FROM #IdLastActivity
SELECT X.family_Id,X.MedicareNo, MAX(CDT) FROM 
(
	SELECT ME.family_Id, ME.MedicareNo, ME.ModifyDatetime CDT FROM MemberExtract ME INNER JOIN PatientIdsConflictsXReference PIXR ON ME.family_id = PIXR.Family_Id
	UNION ALL
	SELECT Me.Family_ID, ME.MedicareNo, OH.LastUpdatedTimestamp CDT
	FROM MemberExtract ME INNER JOIN OrgHierarchy OH ON Me.medicareNo = OH.BENE_HIC_NUM INNER JOIN PatientIdsConflictsXReference PIXR ON ME.family_id = PIXR.Family_Id
) X LEFT JOIN #IdLastActivity ILA ON X.family_id = ILA.FAMILY_ID WHERE ILA.FAMILY_ID IS NULL 
GROUP BY X.family_Id,X.MedicareNo

-- DOWN FLAG ::: Explicitly noted down for demerger
DELETE PatientIdsConflictsXReference 
OUTPUT Deleted.family_id INTO #DemergeDeletes 
FROM PatientIdsConflictsXReference PIXR 
INNER JOIN PatientIdsConflictsDemergeMandate XRDM ON PIXR.family_id = XRDM.KeepOut_Family_Id AND PIXR.rooted_id = XRDM.rooted_id   ;

UPDATE MemberExtract 
SET Holdback = CASE WHEN ME.family_id = DD.Family_Id THEN NULL ELSE 1 END 
FROM MemberExtract ME INNER JOIN #DemergeDeletes DD  ON ME.family_id = DD.family_id AND Me.HoldBack IS NOT NULL 

-- DELETE SOLO RECORDS 
SELECT GROUP_ID INTO #SOLO FROM PatientIdsConflictsXReference GROUP BY GROUP_ID HAVING COUNT(*) = 1 

UPDATE MemberExtract 
SET Holdback = NULL
FROM MemberExtract ME INNER JOIN PatientIdsConflictsXReference PIXR ON ME.family_id = PIXR.Family_Id
INNER JOIN #SOLO S  ON S.GROUP_ID = PIXR.GROUP_ID AND Me.HoldBack IS NOT NULL 

DELETE PatientIdsConflictsXReference FROM PatientIdsConflictsXReference PIXR INNER JOIN #SOLO S ON PIXR.Group_Id = S.Group_Id ;

INSERT INTO #AppAuditLog (AuditCategoryTypeId, AuditActionTypeId, LBUserId, PatientId, Description, EventDateTime)
SELECT 
	(SELECT AuditCategoryTypeId FROM vwPortal_AuditCategoryType WHERE Name = 'PATIENT'),
	3,
	1,
	PIR.IdValue AS SwitchInPatientidDescription,
	'Patient Demerged from : ' + CAST ((SELECT PIR2.Idvalue
	FROM PatientIdsConflictsDemergeMandate PIXR 
	INNER JOIN PatientIdsRegistry PIR2  ON  PIXR.Head_Id = PIR2.Family_Id AND PIR2.IDType = 'LbPatientId' 
	WHERE PIXR.KeepOut_Family_Id = R.FAMILY_ID) AS VARCHAR(100)) AS SwitchInPatientidDescription,
	GETUTCDATE()
FROM #DemergeDeletes R
INNER JOIN PatientIdsRegistry PIR On R.family_id = PIR.Family_Id AND PIR.IDType = 'LbPatientId'

-- POINT INSERTS <<<< STANDARD SECTION BELOW >>>>>

-- Preference for Id on GPRO Submission  
INSERT INTO #PatientIdsConflictsResolutionPoints (Rooted_Id,Group_Id,FAMILY_ID,ResolutionRule,ResolutionPoints)
SELECT 
	A.rooted_id, A.Group_Id, A.Family_Id,Dr.GropuDisambiguationRuleId,Dr.IntraGroupPriorityOrder
FROM PatientIdsConflictsXReference A
INNER JOIN 	( -- Preference for GPRO Submission Id
	SELECT Me.Family_id 
	FROM   MemberExtract ME 
	INNER JOIN GproPatientRanking A ON Me.medicareNo = A.MedicareHicn  GROUP BY Me.Family_id
) B ON A.Family_Id = B.Family_id   
INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON A.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Preference for Id on GPRO Submission'

-- Preference for Id on Latest Claim 
INSERT INTO #PatientIdsConflictsResolutionPoints (Rooted_Id,Group_Id,FAMILY_ID,ResolutionRule,ResolutionPoints)
SELECT 
	A.rooted_id, A.Group_Id, A.Family_Id,Dr.GropuDisambiguationRuleId,Dr.IntraGroupPriorityOrder
FROM PatientIdsConflictsXReference A
INNER JOIN 	( -- Preference for Id on Latest Claim
	SELECT FAMILY_ID FROM (
	SELECT PIXR.GROUP_ID, PIXR.FAMILY_ID , ROW_NUMBER() OVER(PARTITION BY PIXR.GROUP_ID ORDER BY ILA.LastActivity DESC) SNO
	FROM PatientIdsConflictsXReference PIXR INNER JOIN #IdLastActivity ILA ON PIXR.family_id = ILA.family_id
	INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON PIXR.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Preference for Id on Latest Claim'
	) X WHERE SNO = 1 
) B ON A.Family_Id = B.Family_id   
INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON A.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Preference for Id on Latest Claim'

-- Yield preference to Claim file sources as against Interface sources
INSERT INTO #PatientIdsConflictsResolutionPoints (Rooted_Id,Group_Id,FAMILY_ID,ResolutionRule,ResolutionPoints)
SELECT 
	A.rooted_id, A.Group_Id, A.Family_Id,Dr.GropuDisambiguationRuleId,Dr.IntraGroupPriorityOrder
FROM PatientIdsConflictsXReference A
INNER JOIN 	( -- Yield preference to Claim file sources as against Interface sources
	SELECT FAMILY_ID FROM (
	SELECT PIXR.GROUP_ID, PIXR.FAMILY_ID , ROW_NUMBER() OVER(PARTITION BY PIXR.GROUP_ID ORDER BY 
			  CASE WHEN Me.Source = 'MSSP'  AND Me.SourceFeed <> 'Interface' THEN 1 
				   WHEN Me.Source = 'MSSP'  AND Me.SourceFeed =  'Interface' THEN 2 
				   WHEN Me.Source <> 'MSSP' AND Me.SourceFeed <> 'Interface' THEN 3 
				   WHEN Me.Source <> 'MSSP' AND Me.SourceFeed =  'Interface' THEN 4
				ELSE 9 END) SNO
	FROM PatientIdsConflictsXReference PIXR INNER JOIN MemberExtract Me ON PIXR.family_id = Me.family_id
	INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON PIXR.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Yield preference to Claim file sources as against Interface sources'
	) X WHERE SNO = 1 
) B ON A.Family_Id = B.Family_id   
INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON A.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Yield preference to Claim file sources as against Interface sources'

-- Preference for Id on CMS CCLF Claims
INSERT INTO #PatientIdsConflictsResolutionPoints (Rooted_Id,Group_Id,FAMILY_ID,ResolutionRule,ResolutionPoints)
SELECT 
	A.rooted_id, A.Group_Id, A.Family_Id,Dr.GropuDisambiguationRuleId,Dr.IntraGroupPriorityOrder
FROM PatientIdsConflictsXReference A
INNER JOIN 	( -- Preference for Id on CMS CCLF Claims
	SELECT Me.Family_id 
	 FROM   MemberExtract ME 
	 INNER JOIN PatientIdsConflictsXReference PIXR ON ME.family_id = PIXR.Family_Id
	 INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON PIXR.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Preference for Id on CMS CCLF Claims'
	 WHERE EXISTS (SELECT 1 FROM CCLF_5_PartB_Physicians A WHERE Me.medicareNo = A.BENE_HIC_NUM AND ISNULL(A.SourceFeed,'MSSP')  = 'MSSP') OR 
		   EXISTS (SELECT 1 FROM CCLF_1_PartA_Header	 A WHERE Me.medicareNo = A.BENE_HIC_NUM AND ISNULL(A.SourceFeed,'MSSP')  = 'MSSP')
) B ON A.Family_Id = B.Family_id   
INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON A.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Preference for Id on CMS CCLF Claims'

-- Preference for Status Quo on matched Ids
INSERT INTO #PatientIdsConflictsResolutionPoints (Rooted_Id,Group_Id,FAMILY_ID,ResolutionRule,ResolutionPoints)
SELECT 
	A.rooted_id, A.Group_Id, A.Family_Id,Dr.GropuDisambiguationRuleId,Dr.IntraGroupPriorityOrder
FROM PatientIdsConflictsXReference A
INNER JOIN 	( -- Preference for Status Quo on matched Ids
	SELECT PIXR.Family_id  
	FROM   PatientIdsConflictsXReference PIXR  WHERE PIXR.Head_Id = PIXR.Family_Id
) B ON A.Family_Id = B.Family_id   
INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON A.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Preference for Status Quo on matched Ids'

-- Manual Resolution
INSERT INTO #PatientIdsConflictsResolutionPoints (Rooted_Id,Group_Id,FAMILY_ID,ResolutionRule,ResolutionPoints)
SELECT 
	A.rooted_id, A.Group_Id, A.Family_Id,Dr.GropuDisambiguationRuleId,A.ManualOverride
FROM PatientIdsConflictsXReference A
INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON A.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Manual Resolution'

-- POINT INSERTS <<<< CUSTOM SECTION BELOW >>>>>

INSERT INTO #PatientIdsConflictsResolutionPoints (Rooted_Id,Group_Id,FAMILY_ID,ResolutionRule,ResolutionPoints)
EXEC internalComposeCustomResultset @RequestSubject = 'DisambiguationRulesAdditionalSets', @Param1 = 2

-- FINAL PROCESSING 
; WITH CTE1 AS (
	SELECT Rooted_Id, Group_Id, Family_Id, SUM(ResolutionPoints) ResolutionPoints
	FROM #PatientIdsConflictsResolutionPoints GROUP BY Rooted_Id, Group_Id, Family_Id),
CTE2 AS	(
	SELECT 	Rooted_Id, Group_Id, Family_ID, ROW_NUMBER() OVER(PARTITION BY Rooted_Id, Group_Id ORDER BY ResolutionPoints DESC) SNO FROM CTE1 ),
CTE3 AS (SELECT * FROM CTE2 WHERE SNO = 1 )
INSERT INTO #ResolvedIds (ROOTED_ID,GROUP_ID,FAMILY_ID)
SELECT 	Rooted_Id, Group_Id, Family_id FROM CTE3 B WHERE NOT EXISTS (SELECT 1 FROM PatientIdsConflictsXReference X WHERE X.Rooted_Id = B.Rooted_Id AND X.Head_ID = B.FAMILY_ID)

; WITH CTE1 AS (
	SELECT Family_Id, MAX(REPLICATE('A',ResolutionPoints) + '|' + CAST(ResolutionRule AS VARCHAR(10))) ResolutionPoints 
	FROM #PatientIdsConflictsResolutionPoints GROUP BY Family_Id ),
CTE2 AS (
	SELECT Family_Id,  SUBSTRING(ResolutionPoints,CHARINDEX('|',ResolutionPoints) + 1 , 100) ResolutionRule FROM CTE1 )
UPDATE A
SET GroupDisambiguationRuleId = B.ResolutionRule
FROM #ResolvedIds A INNER JOIN CTE2 B ON A.Family_Id = B.Family_Id 

-- MARK DOWN PatientIdsConflictsXReference AND MEMBEREXTRACT
 
UPDATE PatientIdsConflictsXReference
SET Head_Id = T.Family_ID ,
	GroupDisambiguationRuleId = T.GroupDisambiguationRuleId,
	ModifyDateTime = GetDate(),
	ResolvedById = 1 ,
	Comments = ISNULL(LTRIM(RTRIM(Comments)),'') +  CASE WHEN T.GroupDisambiguationRuleId = 9 THEN 'Manual Resolution' ELSE  'Auto Resolution' END
FROM PatientIdsConflictsXReference PIXR INNER JOIN #ResolvedIds T ON PIXR.GROUP_ID = T.GROUP_ID  

UPDATE MemberExtract 
SET Holdback = CASE WHEN ME.family_id = PIXR.Head_ID THEN NULL ELSE 1 END 
FROM MemberExtract ME INNER JOIN PatientIdsConflictsXReference PIXR  ON ME.family_id = PIXR.family_id --AND Me.HoldBack IS NOT NULL 

-- INSERT INTO AUDIT LOG 

INSERT INTO #AppAuditLog (AuditCategoryTypeId, AuditActionTypeId, LBUserId, PatientId, Description, EventDateTime)
SELECT 
	(SELECT AuditCategoryTypeId FROM vwPortal_AuditCategoryType WHERE Name = 'PATIENT'),
	3,
	1,
	PIR.IdValue AS SwitchOutPatientId,
	'Patient Merged with Id : ' + CAST (ISNULL((SELECT PIR2.Idvalue
	FROM PatientIdsRegistry PIR2  WHERE PIR2.family_id = T.FAMILY_ID AND PIR2.IDType = 'LbPatientId'),T.family_ID)AS VARCHAR(100)) SwitchInPatientidDescription,
	GETUTCDATE()
FROM #ResolvedIds T
INNER JOIN PatientIdsConflictsXReference PIXR0  ON PIXR0.GROUP_ID = T.GROUP_ID  AND T.Family_Id <> PIXR0.Family_ID 
LEFT JOIN PatientIdsRegistry PIR  ON PIR.Family_Id = PIXR0.Family_ID AND PIR.IDType = 'LbPatientId' WHERE T.GroupDisambiguationRuleId <> 1 

INSERT INTO vwPortal_AuditInfo (AuditCategoryTypeId, AuditActionTypeId, LBUserId, PatientId, Description, EventDateTime)
SELECT AuditCategoryTypeId, AuditActionTypeId, LBUserId, PatientId, Description, EventDateTime
FROM #AppAuditLog 

END 
GO
