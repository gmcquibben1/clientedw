SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalHealthcareOrgInsert] @TVP HealthcareOrgUnitProcessType READONLY
AS
BEGIN
	INSERT INTO vwPortal_PatientHealthcareOrg(PatientID
				,HealthcareOrgID
				,SourceSystemID
				,ExternalReferenceIdentifier
				,OtherReferenceIdentifier
				,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId)
	SELECT
	DISTINCT     PT.EntityId, 
				 CASE WHEN PT.QueryValueType = 'ExternalCode' THEN TRY_CONVERT(INT,X1.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Patient' THEN  TRY_CONVERT(INT,X2.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Provider' THEN  TRY_CONVERT(INT,X3.HealthcareOrgID) ELSE 0 
				 END AS HealthcareOrgId,
				 2,
				 NULL, 
				 NULL, 
				 0, GETUTCDATE(), GETUTCDATE(),  1, 1	 
	FROM @TVP PT 
	OUTER APPLY (
				SELECT HO.HealthcareOrgID FROM 
				vwPortal_BusinessUnit BU INNER JOIN 
				vwPortal_HealthcareOrg HO ON BU.BusinessUnitId = HO.BusinessUnitId
				WHERE BU.ExternalCode = PT.QueryValue AND BU.DeleteInd = 0 AND PT.QueryValueType = 'ExternalCode'
			) X1
	OUTER APPLY (
				SELECT PHO.HealthcareOrgID FROM 
				vwPortal_PatientHealthcareOrg PHO 
				WHERE PHO.PatientID = TRY_CONVERT(INT,PT.QueryValue) AND PHO.DeleteInd = 0  AND PT.QueryValueType = 'Patient'
			) X2
	OUTER APPLY (
				SELECT PHO.HealthcareOrgID FROM 
				vwPortal_ProviderHealthcareOrg PHO 
				WHERE PHO.ProviderID = TRY_CONVERT(INT,PT.QueryValue) AND PHO.DeleteInd = 0  AND PT.QueryValueType = 'Provider'
			) X3
	LEFT JOIN vwPortal_PatientHealthcareOrg PHO ON PHO.PatientID  = PT.EntityId  AND 
			  PHO.HealthcareOrgID = CASE 
					  WHEN PT.QueryValueType = 'ExternalCode' THEN TRY_CONVERT(INT,X1.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Patient'      THEN TRY_CONVERT(INT,X2.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Provider'     THEN TRY_CONVERT(INT,X3.HealthcareOrgID)
			  END
	WHERE PT.EntityName = 'Patient' AND PHO.HealthcareOrgID IS NULL 
	AND CASE WHEN PT.QueryValueType = 'ExternalCode' THEN TRY_CONVERT(INT,X1.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Patient' THEN  TRY_CONVERT(INT,X2.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Provider' THEN  TRY_CONVERT(INT,X3.HealthcareOrgID) ELSE 0 
				 END > 0
	ORDER BY PT.EntityId

	INSERT INTO vwPortal_ProviderHealthcareOrg (ProviderID
				,HealthcareOrgID
				,SourceSystemID
				,ExternalReferenceIdentifier
				,OtherReferenceIdentifier
				,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId)
	SELECT
	DISTINCT	 PT.EntityId, 
				 CASE WHEN PT.QueryValueType = 'ExternalCode' THEN TRY_CONVERT(INT,X1.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Patient' THEN  TRY_CONVERT(INT,X2.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Provider' THEN  TRY_CONVERT(INT,X3.HealthcareOrgID) ELSE 0 
				 END AS HealthcareOrgId,
				 2,
				 NULL, 
				 NULL, 
				  0, GETUTCDATE(), GETUTCDATE(),  1, 1	
	FROM @TVP PT 
	OUTER APPLY (
				SELECT HO.HealthcareOrgID FROM 
				vwPortal_BusinessUnit BU INNER JOIN 
				vwPortal_HealthcareOrg HO ON BU.BusinessUnitId = HO.BusinessUnitId
				WHERE BU.ExternalCode = PT.QueryValue AND BU.DeleteInd = 0 AND PT.QueryValueType = 'ExternalCode'
			) X1
	OUTER APPLY (
				SELECT PHO.HealthcareOrgID FROM 
				vwPortal_PatientHealthcareOrg PHO 
				WHERE PHO.PatientID = TRY_CONVERT(INT,PT.QueryValue) AND PHO.DeleteInd = 0  AND PT.QueryValueType = 'Patient'
			) X2
	OUTER APPLY (
				SELECT PHO.HealthcareOrgID FROM 
				vwPortal_ProviderHealthcareOrg PHO 
				WHERE PHO.ProviderID = TRY_CONVERT(INT,PT.QueryValue) AND PHO.DeleteInd = 0  AND PT.QueryValueType = 'Provider'
			) X3
	LEFT JOIN vwPortal_ProviderHealthcareOrg PHO ON PHO.ProviderID  = PT.EntityId  AND 
			  PHO.HealthcareOrgID = CASE 
					  WHEN PT.QueryValueType = 'ExternalCode' THEN TRY_CONVERT(INT,X1.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Patient'      THEN TRY_CONVERT(INT,X2.HealthcareOrgID)
					  WHEN PT.QueryValueType = 'Provider'     THEN TRY_CONVERT(INT,X3.HealthcareOrgID)
			  END
	WHERE PT.EntityName = 'Provider' AND PHO.HealthcareOrgID IS NULL 

END;

GO
