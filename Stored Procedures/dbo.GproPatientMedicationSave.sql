SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproPatientMedicationSave]
	    @ImportSetId int
AS
BEGIN

	--------------------------------------------------------------------------------
	-- Load the data from the raw table (which holds raw data imported from the medication XML file) into the GproPatientMedication table
	--------------------------------------------------------------------------------
	MERGE INTO [GproPatientMedication] AS target
	USING (SELECT 
				gpr.GproPatientRankingId,
				gpm.GproPatientMeasureId,
				r.[caremeddoc-visit-date]
		   FROM GproPatientMedication_Raw r
				INNER JOIN GproPatientRanking gpr ON gpr.MedicareHicn = r.[medicare-id]
				INNER JOIN GproPatientMeasure gpm ON gpm.GproPatientRankingId = gpr.GproPatientRankingId
		   WHERE r.[ImportSetId] = @ImportSetId) AS source
		ON source.GproPatientMeasureId = target.GproPatientMeasureId AND source.GproPatientRankingId = target.GproPatientRankingId
	WHEN NOT MATCHED THEN
		INSERT (GproPatientRankingId,
				GproPatientMeasureId,
				CareMeddocVisitDate,
				CareMeddocVisitConfirmed,
				MedicationDocumented,
				CreateDateTime,
				ModifyDateTime,
				CreateLbUserId,
				ModifyLbUserId)
		VALUES (source.GproPatientRankingId,
				source.GproPatientMeasureId,
				source.[caremeddoc-visit-date],
				null,
				null,
				GETUTCDATE(),   
				GETUTCDATE(),
				1,
				1);

	
	--------------------------------------------------------------------------------
	-- update the additional fields if they aren't already populated
	--------------------------------------------------------------------------------
	UPDATE m
	SET m.CareMeddocVisitConfirmed = r.[caremeddoc-visit-confirmation],
		m.ModifyDateTime = GETUTCDATE(),
		m.ModifyLbUserId = 1
	FROM GproPatientMedication m
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = m.GproPatientRankingId
		INNER JOIN GproPatientMedication_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE m.CareMeddocVisitConfirmed IS NULL
		AND r.[caremeddoc-visit-confirmation] IS NOT NULL;

	UPDATE m
	SET m.MedicationDocumented = r.[medications-documented],
		m.ModifyDateTime = GETUTCDATE(),
		m.ModifyLbUserId = 1
	FROM GproPatientMedication m
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = m.GproPatientRankingId
		INNER JOIN GproPatientMedication_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE m.MedicationDocumented IS NULL
		AND r.[medications-documented] IS NOT NULL;

	UPDATE gpm
		SET gpm.CareMeddocConfirmed = r.[caremeddoc-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMedication_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.CareMeddocConfirmed IS NULL
		AND r.[caremeddoc-confirmed] IS NOT NULL;

	UPDATE gpm
		SET gpm.CareMeddocComments = r.[caremeddoc-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMedication_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.CareMeddocComments IS NULL
		AND r.[caremeddoc-comments] IS NOT NULL;
END



GO
