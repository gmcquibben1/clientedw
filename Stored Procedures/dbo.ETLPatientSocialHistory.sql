SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




Create PROCEDURE [dbo].[ETLPatientSocialHistory]
	@fullLoadFlag bit = 0, 
	@BatchSize INT = 10000, 
	@InterfacePatientId VARCHAR(50) = NULL,
	@overrideMinIndex INT = NULL
	
AS
BEGIN	
 
/*===============================================================
	CREATED BY: 	Youping
	CREATED ON:		2016-06-27
	INITIAL VER:	2.0.1
	MODULE:			Patient Questionnaires and Assessments
	Description:	Load Interface PatientSocialHistory Data into EDW

	PARAMETERS:		
		 @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
		 @fullLoadFlag =0 Merge date to the existing table
		 @BatchSize  Number of patients to do in each batch
		 @InterfacePatientId    Id of the patient identifier if we want to run this for a specific patietn
		 @overrideMinIndex		Minimum id of the Interfacepatientsocialhistory record to start processing from. Skips the
								Value stored in the [Maintenance].[dbo].[EDWTableLoadTracking] table



	RETURN VALUE(s)/OUTPUT:	Id of the matching data dictionary field record will be be returned in @AssessmentDataDictionaryFieldId
	 


	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.0.1						YL						 Initial
	2.1.1		2016-06-27		YL						 Add tracing log to [Maintenance].[dbo].[EdwProcedureRunLog]
	2.1.1		2016-09-08		YL						 Add [RecordedDateTime] for dedup key
	2.2.1		2017-01-10		CJL			LBETL-408, LBETL-410	Addressing issues when multiple social history records are being sent 
																	on the same day and those values being overwritten with nulls. Social history records are to be "rolled" to the
																	the last Non-null value
	2.1.2		2017-01-10		CJL			LBETL-408, LBETL-410	Made changes to the stored procedure to handle the SocialHistory in batches, to reduce the load on the temp tables
																	Removed the sorts on create dates. Since the records in the Interface table are never updated and it has a incremental seeded key, we can use that 
																	to implicitly sort. higher keys are newer records
	2.2.1		2017-02-14		CJL			LBETL-612				Changed stored procedure to query on interface patient id as opposed to patient identifier, added parameters to 
																	to configure batch size and single patient




=================================================================*/




    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientSocialHistory';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientSocialHistory';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @Today date=GETUTCDATE();
	DECLARE @RowCount INT =0;
	DECLARE @NumRows INT = 0;
 	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);


	BEGIN TRY	
			--set @fullLoadFlag =1
	
		--1) Retrieve the last run time id values.  We are going to pull both the date and the last run time id. If the last max id 
		--is null, calculate a best guese based on the 
		--We are changing away from using date values for query purposes as it is more efficient to query by the Id value.  Since
		--the id is a seeded, integer greater id values are implicitly newer. It also allows us to pick up records that come in while the 
		--procedure is running the next time it runs
		IF @fullLoadFlag = 1
		BEGIN
			UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME), maxSourceIdProcessed= 0
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
		END

		 SET @lastDateTime = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	
	
		
		SET  @lastIndex = (SELECT maxSourceIdProcessed
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex= (SELECT MIN([InterfacePatientSocialHistoryId]) FROM  [vwPortal_InterfacePatientSocialHistory] IPS WITH (NOLOCK) WHERE IPS.CreateDateTime >= @lastDateTime ) 
		END

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex = 0 
		END


		if @overrideMinIndex IS NOT NULL
		BEGIN
			SET @lastIndex = @overrideMinIndex
		END 

		SET @maxIndex = (SELECT MAX([InterfacePatientSocialHistoryId]) FROM  [vwPortal_InterfacePatientSocialHistory] IPS WITH (NOLOCK));
	
		


		--2) Get the full list of patient identifiers that we are going to be dealing with
		--We are going to breaking this up into batches along patient id lines.
		SELECT  DISTINCT 
			IP.PatientIdentifier, IP.InterfaceSystemId
		INTO #PatientSocialHistoryQuePatientIdentifier	
		FROM [dbo].[vwPortal_InterfacePatientSocialHistory] IPS with (nolock)
			INNER JOIN [dbo].[vwPortal_InterfacePatient] IP with (nolock) ON IPS.InterfacePatientId = IP.InterfacePatientId
			INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) ON IP.InterfaceSystemId = ISS.[InterfaceSystemId]
		WHERE ( IPS.InterfacePatientSocialHistoryId >= @lastIndex  AND IPS.InterfacePatientSocialHistoryId <= @maxIndex )
		 AND ( @InterfacePatientId IS NULL OR (IPS.InterfacePatientId = @InterfacePatientId))
			

					
				
				--- Truncate data
			 IF (@fullLoadFlag=1)
			   BEGIN
				   Truncate table [dbo].[PatientSocialHistory];
			   END
	
	         SELECT @RecordCountBefore=COUNT(1)
			FROM [dbo].[PatientSocialHistory]  WITH (NOLOCK);

	
		--3) Loop throught all of the batches to improve memory efficency
		WHILE 1 = 1
		BEGIN				

				SELECT TOP (@BatchSize) PatientIdentifier, InterfaceSystemId INTO #PatientSocialHistoryQuePatientIdentifieBATCH FROM #PatientSocialHistoryQuePatientIdentifier;
				Create  NONCLUSTERED INDEX IDX_PatientSocialHistoryQuePatientIdentifieBATCH_PatientIdentifier ON #PatientSocialHistoryQuePatientIdentifieBATCH (PatientIdentifier, InterfaceSystemId);
			
    				SET @NUMROWS = ( SELECT COUNT(1) FROM #PatientSocialHistoryQuePatientIdentifieBATCH)

				IF @NUMROWS <> 0 
				BEGIN






				--Create working set of patient SocialHistory to process based on the first batch of patients
				SELECT  [InterfacePatientSocialHistoryId]
				  ,idRef.SourceSystemId
				  --,IP.PatientIdentifier
				  --,IP.InterfacePatientId
				  ,idRef.LBPatientId as PatientId
				  ,[SmokingStatus]
				  ,[MaritalStatus]
				  ,[EmploymentStatus]
				  ,[TransportationInd]
				  ,[LivingArrangementCode]
				  ,[AlcoholUseInd]
				  ,[DrugUseInd]
				  ,[CaffeineUseInd]
				  ,[SexuallyActiveInd]
				  ,[ExerciseHoursPerWeek]
				  ,IPS.[CreateDateTime]
				  ,IPS.[RecordedDateTime]
				INTO #PatientSocialHistoryQue	
					FROM [dbo].[vwPortal_InterfacePatientSocialHistory] IPS with (nolock),
					[dbo].[vwPortal_InterfacePatient] IP with (nolock), 
					[dbo].[vwPortal_InterfaceSystem]  ISS with (nolock),
					#PatientSocialHistoryQuePatientIdentifieBATCH batch,
  					PatientIdReference idRef with (nolock) 	  
				WHERE (IPS.InterfacePatientSocialHistoryId >= @lastIndex  AND IPS.InterfacePatientSocialHistoryId <= @maxIndex) 
				 AND  IPS.InterfacePatientId = IP.InterfacePatientId AND  IP.InterfaceSystemId = ISS.[InterfaceSystemId] 
				AND IP.PatientIdentifier = batch.PatientIdentifier AND IP.InterfaceSystemId = batch.InterfaceSystemId
				AND  idRef.SourceSystemId = ISS.SourceSystemId 
					  AND idRef.ExternalId = batch.PatientIdentifier 
					  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
					  AND idRef.DeleteInd =0				

				;





              --- add Index
			  Create  NONCLUSTERED INDEX IDX_PatientIdentifier_SourceSystemId ON #PatientSocialHistoryQue (PatientId, SourceSystemId, [RecordedDateTime]);
            			
		 			
				
			-----   dedup data prepare for Merge
			--LBETL-408, 410  It is possible that the source system will send social history information on mulitple rows.   We have to "Roll" up the rows to the last non-null value
			 SELECT * 
			 INTO #PatientSocialHistory
			 FROM (
				SELECT DISTINCT PS.SourceSystemId
						, PS.PatientID
						,smoke.[SmokingStatus]
					  ,maritial.[MaritalStatus]
					  ,employment.[EmploymentStatus]
					  ,transport.[TransportationInd]
					  ,living.[LivingArrangementCode]
					  ,alchohol.[AlcoholUseInd]
					  ,druguse.[DrugUseInd]
					  ,caffeine.[CaffeineUseInd]
					  ,sexualactivity.[SexuallyActiveInd]
					  ,exercise.[ExerciseHoursPerWeek]
					  ,PS.[CreateDateTime]
					  ,PS.[RecordedDateTime]
			FROM 
					(SELECT PatientId,  SourceSystemId, [RecordedDateTime], [CreateDateTime] ,
								ROW_NUMBER() OVER (PARTITION BY PatientId,   SourceSystemID,  ISNULL([RecordedDateTime],'1900-01-01') ORDER BY [InterfacePatientSocialHistoryId] DESC)
								rowNumber 
								FROM #PatientSocialHistoryQue WITH (NOLOCK)) PS 
					LEFT JOIN
						(SELECT v1.PatientId,v1.[SmokingStatus], v1.SourceSystemId, v1.[RecordedDateTime], ROW_NUMBER() OVER 
						(PARTITION BY v1.PatientId,  ISNULL( v1.[RecordedDateTime],'1900-01-01'), v1.SourceSystemId ORDER BY v1.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v1 WITH (NOLOCK) WHERE v1.[SmokingStatus] IS NOT NULL ) smoke
						ON smoke.PatientId = PS.PatientId AND ISNULL(smoke.[RecordedDateTime],'1900-01-01') = ISNULL(PS.[RecordedDateTime],'1900-01-01')  
						 AND smoke.SourceSystemId = PS.SourceSystemId 
					LEFT JOIN
						(SELECT v2.PatientId,v2.[MaritalStatus], v2.SourceSystemId, v2.[RecordedDateTime], ROW_NUMBER()  OVER 
						(PARTITION BY v2.PatientId,  ISNULL(v2.[RecordedDateTime],'1900-01-01'), v2.SourceSystemId ORDER BY v2.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v2 WITH (NOLOCK) WHERE v2.[MaritalStatus] IS NOT NULL ) maritial
						ON maritial.PatientId = PS.PatientId AND ISNULL(maritial.[RecordedDateTime],'1900-01-01') = ISNULL(PS.[RecordedDateTime],'1900-01-01') 
						 AND maritial.SourceSystemId = PS.SourceSystemId 
					LEFT JOIN
						(SELECT v3.PatientId,v3.[EmploymentStatus], v3.SourceSystemId, v3.[RecordedDateTime], ROW_NUMBER()  OVER 
						(PARTITION BY v3.PatientId,  ISNULL(v3.[RecordedDateTime],'1900-01-01'), v3.SourceSystemId ORDER BY v3.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v3 WITH (NOLOCK) WHERE v3.[EmploymentStatus] IS NOT NULL ) employment
						ON employment.PatientId = PS.PatientId AND ISNULL(employment.[RecordedDateTime],'1900-01-01')  = ISNULL(PS.[RecordedDateTime],'1900-01-01') 
						AND employment.SourceSystemId = PS.SourceSystemId 
					LEFT JOIN
						(SELECT v4.PatientId,v4.[TransportationInd], v4.SourceSystemId, v4.[RecordedDateTime], ROW_NUMBER()  OVER 
						(PARTITION BY v4.PatientId,  ISNULL(v4.[RecordedDateTime],'1900-01-01'), v4.SourceSystemId ORDER BY v4.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v4 WITH (NOLOCK) WHERE v4.[TransportationInd] IS NOT NULL ) transport
						ON transport.PatientId = PS.PatientId AND ISNULL(transport.[RecordedDateTime],'1900-01-01')  = ISNULL(PS.[RecordedDateTime],'1900-01-01')
						 AND transport.SourceSystemId = PS.SourceSystemId 
					LEFT JOIN
						(SELECT v5.PatientId,v5.[LivingArrangementCode], v5.SourceSystemId, v5.[RecordedDateTime], ROW_NUMBER()  OVER 
						(PARTITION BY v5.PatientId,  ISNULL(v5.[RecordedDateTime],'1900-01-01'), v5.SourceSystemId ORDER BY v5.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v5 WITH (NOLOCK) WHERE v5.[LivingArrangementCode] IS NOT NULL ) living
						ON living.PatientId = PS.PatientId AND ISNULL(living.[RecordedDateTime],'1900-01-01') = ISNULL(PS.[RecordedDateTime],'1900-01-01') 
						 AND living.SourceSystemId = PS.SourceSystemId 
					LEFT JOIN
						(SELECT v6.PatientId,v6.[AlcoholUseInd], v6.SourceSystemId, v6.[RecordedDateTime], ROW_NUMBER()  OVER 
						(PARTITION BY v6.PatientId,  ISNULL(v6.[RecordedDateTime],'1900-01-01'), v6.SourceSystemId ORDER BY v6.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v6 WITH (NOLOCK) WHERE v6.[AlcoholUseInd] IS NOT NULL ) alchohol
						ON alchohol.PatientId = PS.PatientId AND ISNULL(alchohol.[RecordedDateTime],'1900-01-01') = ISNULL(PS.[RecordedDateTime],'1900-01-01')  
						AND alchohol.SourceSystemId = PS.SourceSystemId 
					LEFT JOIN
						(SELECT v7.PatientId,v7.[DrugUseInd], v7.SourceSystemId, v7.[RecordedDateTime], ROW_NUMBER()  OVER 
						(PARTITION BY v7.PatientId,  ISNULL(v7.[RecordedDateTime],'1900-01-01'), v7.SourceSystemId ORDER BY v7.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v7 WITH (NOLOCK) WHERE v7.[DrugUseInd] IS NOT NULL ) druguse
						ON druguse.PatientId = PS.PatientId AND ISNULL(druguse.[RecordedDateTime],'1900-01-01') = ISNULL(PS.[RecordedDateTime],'1900-01-01')  
						AND druguse.SourceSystemId = PS.SourceSystemId 
					LEFT JOIN
						(SELECT v8.PatientId,v8.[SexuallyActiveInd], v8.SourceSystemId, v8.[RecordedDateTime], ROW_NUMBER()  OVER 
						(PARTITION BY v8.PatientId,  ISNULL(v8.[RecordedDateTime],'1900-01-01'), v8.SourceSystemId ORDER BY v8.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v8 WITH (NOLOCK) WHERE v8.[SexuallyActiveInd] IS NOT NULL ) sexualactivity
						ON sexualactivity.PatientId = PS.PatientId AND ISNULL(sexualactivity.[RecordedDateTime],'1900-01-01') = ISNULL(PS.[RecordedDateTime],'1900-01-01') 
						 AND sexualactivity.SourceSystemId = PS.SourceSystemId 
					LEFT JOIN
						(SELECT v9.PatientId,v9.[ExerciseHoursPerWeek], v9.SourceSystemId, v9.[RecordedDateTime], ROW_NUMBER()  OVER 
						(PARTITION BY v9.PatientId,  ISNULL(v9.[RecordedDateTime],'1900-01-01'), v9.SourceSystemId ORDER BY v9.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v9 WITH (NOLOCK) WHERE v9.[ExerciseHoursPerWeek] IS NOT NULL ) exercise
						ON exercise.PatientId = PS.PatientId AND ISNULL(exercise.[RecordedDateTime],'1900-01-01') = ISNULL(PS.[RecordedDateTime],'1900-01-01')  
						AND exercise.SourceSystemId = PS.SourceSystemId 
					LEFT JOIN
						(SELECT v10.PatientId,v10.[CaffeineUseInd], v10.SourceSystemId, v10.[RecordedDateTime], ROW_NUMBER()  OVER 
						(PARTITION BY v10.PatientId,  ISNULL(v10.[RecordedDateTime],'1900-01-01'), v10.SourceSystemId ORDER BY v10.[InterfacePatientSocialHistoryId] DESC) rowNumber 
						FROM #PatientSocialHistoryQue  v10 WITH (NOLOCK) WHERE v10.[CaffeineUseInd] IS NOT NULL ) caffeine
						ON caffeine.PatientId = PS.PatientId AND ISNULL(caffeine.[RecordedDateTime],'1900-01-01') = ISNULL(PS.[RecordedDateTime],'1900-01-01') 
						AND caffeine.SourceSystemId = PS.SourceSystemId 
					WHERE PS.rowNumber = 1
								AND (smoke.rowNumber = 1 OR smoke.rowNumber IS NULL)
								AND (maritial.rowNumber = 1 OR maritial.rowNumber IS NULL)
								AND (employment.rowNumber = 1 OR employment.rowNumber IS NULL)
								AND (transport.rowNumber = 1 OR transport.rowNumber IS NULL)
								AND (living.rowNumber = 1 OR living.rowNumber IS NULL)
								AND (alchohol.rowNumber = 1 OR alchohol.rowNumber IS NULL)
								AND (druguse.rowNumber = 1 OR druguse.rowNumber IS NULL)
								AND (caffeine.rowNumber = 1 OR caffeine.rowNumber IS NULL)
								AND (sexualactivity.rowNumber = 1 OR sexualactivity.rowNumber IS NULL)
								AND (exercise.rowNumber = 1 OR exercise.rowNumber IS NULL)
					
					) A

					SET @RecordCountSource = @RecordCountSource + (SELECT COUNT(1) FROM #PatientSocialHistory);	             


		


					SET @RecordCountSource = @RecordCountSource + (SELECT COUNT(1) FROM #PatientSocialHistory);	             


						
   	

			



			 CREATE CLUSTERED INDEX CIX_#PatientID_DiagnosisCode ON #PatientSocialHistory(SourceSystemId, [PatientId], [RecordedDateTime]) ;

			  --- test select * from #PatientSocialHistory


				MERGE [DBO].PatientSocialHistory AS [target]
				USING #PatientSocialHistory AS [source] 

				ON (target.[SourceSystemID] = [source].[SourceSystemID] AND target.PatientID =[source].PatientID 
					AND isnull(target.[RecordedDateTime],'1900-01-01') =isnull([source].[RecordedDateTime],'1900-01-01')
					
					)
                WHEN MATCHED  THEN UPDATE SET 
	
						[target].[SmokingStatus]= ISNULL([source].[SmokingStatus], [target].[SmokingStatus]),
						[target].[MaritalStatus] =  ISNULL([source].[MaritalStatus], [target].[MaritalStatus]),
						[target].[EmploymentStatus] = ISNULL([source].[EmploymentStatus], [target].[EmploymentStatus]), 
						[target].[TransportationInd] =  ISNULL( [source].[TransportationInd], [target].[TransportationInd]), 
						[target].[LivingArrangementCode] =   ISNULL( [source].[LivingArrangementCode], [target].[LivingArrangementCode] ), 
						[target].[AlcoholUseInd] = ISNULL( [source].[AlcoholUseInd], [target].[AlcoholUseInd]), 
						[target].[DrugUseInd] =  ISNULL( [source].[DrugUseInd], [target].[DrugUseInd]), 
						[target].[CaffeineUseInd] = ISNULL( [source].[CaffeineUseInd], [target].[CaffeineUseInd] ),
						[target].[SexuallyActiveInd] = ISNULL( [source].[SexuallyActiveInd], [target].[SexuallyActiveInd]), 
						[target].[ExerciseHoursPerWeek]= ISNULL( [source].[ExerciseHoursPerWeek], [target].[ExerciseHoursPerWeek] ) 
						--[target].[CreateDateTime]= ISNULL( [source].[CreateDateTime], [source].[CreateDateTime] 
						--[target].[RecordedDateTime]=[source].[RecordedDateTime]
				WHEN NOT MATCHED THEN
				INSERT ([PatientID] , [SourceSystemId] , [SmokingStatus] , 	[MaritalStatus] , [EmploymentStatus] ,
					   [TransportationInd] , [LivingArrangementCode] , 	[AlcoholUseInd], [DrugUseInd],	[CaffeineUseInd] , [SexuallyActiveInd],
					   [ExerciseHoursPerWeek] , [CreateDateTime],[RecordedDateTime]  )
				VALUES ([source].[PatientID],[source].[SourceSystemId],[source].[SmokingStatus],[source].[MaritalStatus],[source].[EmploymentStatus],
					   [source].[TransportationInd],[source].[LivingArrangementCode],[source].[AlcoholUseInd],[source].[DrugUseInd],[source].[CaffeineUseInd],[source].[SexuallyActiveInd],
					   [source].[ExerciseHoursPerWeek],[source].[CreateDateTime],[source].[RecordedDateTime]);

	    
								--Update the max processed time and drop the temp tables
							SET @tmpmaxSourceTimeStampProcessed = 	(SELECT MAX(CreateDateTime) FROM #PatientSocialHistoryQue )
							IF @tmpmaxSourceTimeStampProcessed > @maxSourceTimeStampProcessed
							BEGIN
							SET @maxSourceTimeStampProcessed = @tmpmaxSourceTimeStampProcessed
							END


						DROP TABLE #PatientSocialHistory
						DROP TABLE #PatientSocialHistoryQue

				END
			
			
			--Move onto the next batch
			DELETE FROM #PatientSocialHistoryQuePatientIdentifier WHERE PatientIdentifier IN (SELECT PatientIdentifier FROM #PatientSocialHistoryQuePatientIdentifieBATCH) OR PatientIdentifier IS NULL
			DROP TABLE #PatientSocialHistoryQuePatientIdentifieBATCH
			IF @NUMROWS = 0 BREAK;
  
		END
		
	

			DROP table #PatientSocialHistoryQuePatientIdentifier
		-- update lastdate

					
               		   
		-- update maxSourceTimeStamp with max CreateDateTime of #PatientSocialHistoryQue
		UPDATE [Maintenance]..[EDWTableLoadTracking]
		SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


		-- get total records from PatientSocialHistory after merge 
		SELECT @RecordCountAfter=COUNT(1)  FROM PatientSocialHistory WITH (NOLOCK);

		-- Calculate records inserted and updated counts
		SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
		SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	

	END TRY

		BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

			
 

END


GO
