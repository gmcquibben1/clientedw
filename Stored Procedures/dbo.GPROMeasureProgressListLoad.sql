SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GPROMeasureProgressListLoad]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2015-12-29
	INITIAL VER:	1.6.1
	MODULE:			GPRO		
	DESCRIPTION:	Loads the counts of ranked, complete, and consecutively complete GPRO patients for a given business unit in the hierarchy
	PARAMETERS:		Business Unit id
	RETURN VALUE(s)/OUTPUT:	a table containing the progress totals for all gpro measure
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.6.1		12.29.2015		BR						Initial Version
	1.6.1		01.05.2016		BR						Reworked it to display all patients at or below current BU, 
														also display patients with NULL LbPatientId if the current BU is the topmost (no parent)
	1.6.1		02.01.2016		BR						Added indexes to speed up processing
	1.6.1		02.08.2016		BR						Fixed the percentage calculations for lists less than 248
														Fixed the consecutive complete calculations for fully complete groups
	1.6.1		02.19.2016		BR						Added denominator exclusion values to some measures to count as skipped
	1.6.1		02.28.2016		BR						Changed the way skipped measures are counded for CAD, HF, and MH
	1.6.1		03.01.2016		BR						Taking out the test for the patient's status in GproPatientRanking and instead checking MedicalRecordFound directly
	2.2.1		10.24.2016		BR			LBPP-2030	Adding an additional percentage column to the output
	2.2.1		10.31.2016		BR			LBPP-2030	Refactored to use dynamic sql to loop through measure types rather than hard code status logic 
	2.2.1		11.08.2016		BR			LBPP-2030	Changed the column "PercentComplete" to "PercentConsecutive"
	2.2.1		2016-11-09		BR			LBPP-2028	Added additional column to skipped and complete sets to comply w/ new stored proc return values
	2.2.1		11-23-2016		BR			LBPP-2033	Fixed a bug by changing indexes on temp table to use GproPatientRankingId.  This is because PatientId will be NULL if
														the patient can't be matched to an existing patient
	2.2.1		2016-12-19		WK						Updating the SP to take in a parameter which dictates whether to return results or insert them into a temp table.
	2.2.1		2017-01-18		BR			LBDM-1809	Updated the "first incomplete" calculation to default to 999 instead of 0, otherwise measures with no incomplete would show 0% done
	2.2.1		2017-02-09		BR						Adding remaining to target to output
=================================================================*/
(
	@businessUnitId INT
	, @transactionalDbName VARCHAR(200) = NULL
	, @resultToTempTable BIT = 0
)
AS
BEGIN
    SET NOCOUNT ON;

	--DECLARE @resultToTempTable BIT = 0
	--DECLARE @businessUnitId INT = 834
	--------------------------------------------------------------------------------
	-- create temp tables to build up the status list
	--------------------------------------------------------------------------------
	CREATE TABLE #tmpGPROMeasureProgress
				(
					RowNumber INT IDENTITY (1, 1)
					, MeasureTypeId INT
					, MeasureTypeAbbr VARCHAR(50)
					, MeasureTypeDesc VARCHAR(500)
					, RankingTableColumnName VARCHAR(50)
					, TotalPatients INT
					, TotalComplete INT
					, ConsecutiveCount INT
					, TotalSkipped INT
					, PercentConsecutive INT
					, StatusDesc VARCHAR(20)
					, PercentTotal INT
					, ConsecutiveTarget INT
					, RemainingToTarget INT
				);

	CREATE TABLE #patientList
	(
		GproPatientRankingId INT,
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #groupTinPatientList
	(
		GproPatientRankingId INT,
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #CompletePatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #IncompletePatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #SkippedPatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	DECLARE @FirstIncompleteEntry INT;
	DECLARE @Sql NVARCHAR(4000);

	--------------------------------------------------------------------------------
	-- build a list of GPRO patients that can be viewed at this level of the hierarchy
	--------------------------------------------------------------------------------
	INSERT INTO #patientList EXEC GproGetPatientListByBusinessUnit @businessUnitId;

	INSERT INTO #groupTinPatientList
		(GproPatientRankingId)
	SELECT gpr.GproPatientRankingId 
	FROM GproPatientRanking gpr
	WHERE gpr.GroupTin IN (SELECT DISTINCT gpr2.GroupTin FROM GproPatientRanking gpr2 INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr2.GproPatientRankingId)

	--------------------------------------------------------------------------------
	-- loop through the list of measure types, insert a row w/ the counts for each type
	--------------------------------------------------------------------------------
	DECLARE @MeasureTypeCount INT = (SELECT MAX(GproMeasureTypeId) FROM GproMeasureType);
	DECLARE @CurrentRankColumn VARCHAR(20) = NULL;
	DECLARE @CurrentMeasureName VARCHAR(20) = NULL;
	WHILE @MeasureTypeCount > 0
	BEGIN
		TRUNCATE TABLE #CompletePatients;
		TRUNCATE TABLE #IncompletePatients;
		TRUNCATE TABLE #SkippedPatients;

		SET @CurrentRankColumn = (SELECT RankColumnName FROM GproMeasureType WHERE GproMeasureTypeId = @MeasureTypeCount)
		SET @CurrentMeasureName = (SELECT Name FROM GproMeasureType WHERE GproMeasureTypeId = @MeasureTypeCount)
		IF (@CurrentRankColumn IS NOT NULL)
		BEGIN
			--------------------------------------------------------------------------------
			-- insert the base row w/ the total of ranked patients
			--------------------------------------------------------------------------------
			SET @Sql = 'INSERT INTO #tmpGPROMeasureProgress
				(MeasureTypeId,MeasureTypeAbbr,MeasureTypeDesc,RankingTableColumnName,TotalPatients)
			SELECT DISTINCT
				gpmt.GproMeasureTypeId, gpmt.Name, gpmt.DisplayValue, gpmt.RankColumnName, COUNT(gpr.' + @CurrentRankColumn + ') 
			FROM GproMeasureType gpmt
				LEFT OUTER JOIN GproPatientRanking gpr ON gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.GproPatientRankingId IN (SELECT GproPatientRankingId FROM #patientList)
			WHERE
				gpmt.RankColumnName = ''' + @CurrentRankColumn + '''
				AND NOT EXISTS (SELECT TOP 1 1 FROM #tmpGPROMeasureProgress p2 WHERE p2.MeasureTypeId = gpmt.GproMeasureTypeId)
			GROUP BY 
				gpmt.GproMeasureTypeId, gpmt.Name, gpmt.DisplayValue, gpmt.RankColumnName';

			EXEC (@Sql);

			--------------------------------------------------------------------------------
			-- find the set of all complete patients and all skipped patients for this measure
			--------------------------------------------------------------------------------
			SET @Sql = 'INSERT INTO #CompletePatients EXEC GproGetCompletePatientList ''' + @CurrentMeasureName + ''''
			EXEC (@Sql);
			SET @Sql = 'INSERT INTO #SkippedPatients EXEC GproGetSkippedPatientList ''' + @CurrentMeasureName + ''''
			EXEC (@Sql);

			--------------------------------------------------------------------------------
			-- update the count of all complete patients for this BU
			--------------------------------------------------------------------------------
			SET @Sql = 'UPDATE tgmp
			SET tgmp.TotalComplete = ISNULL((SELECT COUNT(DISTINCT cp.LbPatientId)
										FROM #CompletePatients cp INNER JOIN #patientList pl ON pl.GproPatientRankingId = cp.GproPatientRankingId), 0)
			FROM #tmpGPROMeasureProgress tgmp
			WHERE tgmp.MeasureTypeAbbr = ''' + @CurrentMeasureName + '''';
			EXEC (@Sql);

			--------------------------------------------------------------------------------
			-- update the count of all skipped patients for this BU
			--------------------------------------------------------------------------------
			SET @Sql = 'UPDATE tgmp
			SET tgmp.TotalSkipped = ISNULL((SELECT COUNT(DISTINCT sp.LbPatientId)
									 FROM #SkippedPatients sp INNER JOIN #patientList pl ON pl.GproPatientRankingId = sp.GproPatientRankingId), 0)
			FROM #tmpGPROMeasureProgress tgmp
			WHERE tgmp.MeasureTypeAbbr = ''' + @CurrentMeasureName + '''';
			EXEC (@Sql);

			--------------------------------------------------------------------------------
			-- find the set of all incomplete patients for this measure
			--------------------------------------------------------------------------------
			SET @Sql = 'INSERT INTO #IncompletePatients (LbPatientId,MeasureRank,GproPatientRankingId,GproPatientMeasureId,GproMeasureTypeId)
			SELECT gpr.LbPatientId,gpr.' + @CurrentRankColumn + ',gpr.GproPatientRankingId,gpm.GproPatientMeasureId,null
			FROM GproPatientMeasure gpm 
				INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
				INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId
			WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL 
				AND NOT EXISTS (SELECT TOP 1 1 FROM #CompletePatients cp WHERE cp.GproPatientRankingId = gpr.GproPatientRankingId)
				AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GproPatientRankingId = gpr.GproPatientRankingId)';
			EXEC (@Sql);

			--------------------------------------------------------------------------------
			-- find the total of consecutively complete items this BU needs to hit to be 100%
			--------------------------------------------------------------------------------
			SET @Sql = 'UPDATE tgmp
			SET tgmp.ConsecutiveTarget = (SELECT COUNT(gpr.GproPatientRankingId) 
										  FROM GproPatientRanking gpr 
												INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId 
										  WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.' + @CurrentRankColumn + ' <> 0
												AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GpropatientRankingId = gpr.GproPatientRankingId)
												AND gpr.' + @CurrentRankColumn + ' <= (SELECT MAX(s.' + @CurrentRankColumn + ') FROM 
													(SELECT TOP 248 gpr2.' + @CurrentRankColumn + ' 
													FROM GproPatientRanking gpr2
														INNER JOIN #groupTinPatientList gtpl ON gtpl.GproPatientRankingId = gpr2.GproPatientRankingId
													WHERE gpr2.' + @CurrentRankColumn + ' IS NOT NULL AND gpr2.' + @CurrentRankColumn + ' <> 0
														AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GpropatientRankingId = gpr2.GproPatientRankingId)
													ORDER BY gpr2.' + @CurrentRankColumn + ') s))
			FROM #tmpGPROMeasureProgress tgmp
			WHERE tgmp.MeasureTypeAbbr = ''' + @CurrentMeasureName + '''';
			EXEC (@Sql);

			--------------------------------------------------------------------------------
			-- Get the remaining to target for this BU
			--------------------------------------------------------------------------------
			SET @Sql = 'UPDATE tgmp
			SET tgmp.RemainingToTarget = (SELECT COUNT(gpr.GproPatientRankingId) 
										  FROM GproPatientRanking gpr 
												INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId 
												INNER JOIN #IncompletePatients ip ON ip.GproPatientRankingId = gpr.GproPatientRankingId
										  WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.' + @CurrentRankColumn + ' <> 0
												AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GpropatientRankingId = gpr.GproPatientRankingId)
												AND gpr.' + @CurrentRankColumn + ' <= (SELECT MAX(s.' + @CurrentRankColumn + ') FROM 
													(SELECT TOP 248 gpr2.' + @CurrentRankColumn + ' 
													FROM GproPatientRanking gpr2
														INNER JOIN #groupTinPatientList gtpl ON gtpl.GproPatientRankingId = gpr2.GproPatientRankingId
													WHERE gpr2.' + @CurrentRankColumn + ' IS NOT NULL AND gpr2.' + @CurrentRankColumn + ' <> 0
														AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GpropatientRankingId = gpr2.GproPatientRankingId)
													ORDER BY gpr2.' + @CurrentRankColumn + ') s))
			FROM #tmpGPROMeasureProgress tgmp
			WHERE tgmp.MeasureTypeAbbr = ''' + @CurrentMeasureName + '''';
			EXEC (@Sql);

			--------------------------------------------------------------------------------
			-- find the first incomplete patient, use it to calculate the consecutively complete
			--------------------------------------------------------------------------------
			SET @FirstIncompleteEntry = ISNULL((SELECT MIN(ip.MeasureRank) FROM #IncompletePatients ip INNER JOIN #patientList pl ON pl.GproPatientRankingId = ip.GproPatientRankingId), 999);

			SET @Sql = 'UPDATE tgmp
			SET tgmp.ConsecutiveCount = ISNULL((SELECT COUNT(cp.LbPatientId) 
										FROM #CompletePatients cp INNER JOIN #patientList pl ON pl.GproPatientRankingId = cp.GproPatientRankingId 
										WHERE cp.MeasureRank < ' + CONVERT(VARCHAR(3),@FirstIncompleteEntry) + '),0)
			FROM #tmpGPROMeasureProgress tgmp
			WHERE tgmp.MeasureTypeAbbr = ''' + @CurrentMeasureName + '''';
			EXEC (@Sql);

		END

		SET @MeasureTypeCount = @MeasureTypeCount - 1;
	END

	--------------------------------------------------------------------------------
	-- Return all of the temp rows as output
	--------------------------------------------------------------------------------
	UPDATE tgmp
	SET tgmp.PercentTotal = (CASE WHEN tgmp.TotalPatients - tgmp.TotalSkipped <= 0 THEN 100
								  ELSE CAST(ROUND(100.0 * (tgmp.TotalComplete * 100.0) / ((tgmp.TotalPatients - tgmp.TotalSkipped) * 100.0), 2) AS numeric(36,1))
							 END),
		tgmp.PercentConsecutive = (CASE WHEN tgmp.TotalPatients >= tgmp.ConsecutiveTarget AND tgmp.ConsecutiveCount >= tgmp.ConsecutiveTarget THEN 100
									 WHEN tgmp.TotalPatients < tgmp.ConsecutiveTarget AND tgmp.TotalSkipped < tgmp.TotalPatients THEN CAST(ROUND((tgmp.ConsecutiveCount * 100.0)/((tgmp.TotalPatients - tgmp.TotalSkipped) * 100.0) * 100,2) AS NUMERIC(36,1))
									 WHEN tgmp.TotalSkipped = tgmp.TotalPatients THEN 100
									 ELSE CAST(ROUND((tgmp.ConsecutiveCount * 100.0)/(tgmp.ConsecutiveTarget * 100.0) * 100,2) AS NUMERIC(36,1)) 
								END ),
		tgmp.StatusDesc = (CASE WHEN tgmp.TotalPatients >= tgmp.ConsecutiveTarget AND tgmp.ConsecutiveCount >= tgmp.ConsecutiveTarget THEN 'Complete' 
								WHEN tgmp.TotalPatients < tgmp.ConsecutiveTarget AND tgmp.ConsecutiveCount >= (tgmp.TotalPatients - tgmp.TotalSkipped) THEN 'Complete'
								WHEN tgmp.TotalSkipped = tgmp.TotalPatients THEN 'Complete'
								ELSE 'Incomplete' 
							END)
	FROM #tmpGPROMeasureProgress tgmp


	IF(@resultToTempTable = 0)
		BEGIN
		--return
			SELECT * FROM #tmpGPROMeasureProgress
		END
	ELSE
		BEGIN
			INSERT INTO  #TempMeasureHistory 
				SELECT * FROM #tmpGPROMeasureProgress
		END
END

GO
