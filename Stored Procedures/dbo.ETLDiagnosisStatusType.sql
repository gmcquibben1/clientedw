SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-03-23
-- Description:	Load DiagnosisStatusType Data into EDW
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[ETLDiagnosisStatusType]

AS
BEGIN	
		INSERT INTO [dbo].[DiagnosisStatusType]
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
        SELECT DISTINCT
		t1.[DiagnosisStatus],
		t1.[DiagnosisStatus],
		0,
		GETUTCDATE(),
		GETUTCDATE(),
		1,
		1
        FROM PatientDiagnosisProcessQueue t1
		LEFT JOIN [dbo].[DiagnosisStatusType] t2
		ON LTRIM(RTRIM(t1.[DiagnosisStatus])) = LTRIM(RTRIM(t2.[Name]))
		WHERE t1.[DiagnosisStatus] IS NOT NULL AND t2.[Name] IS NULL

END
GO
