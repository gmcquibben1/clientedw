SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ===============================================================================================================================
-- Author:		Lan Ma
-- Create date: 2016-09-27
-- Description:	this SP builds a flat table that holds a patients' 4 dissferent patientIds from sources. This SP is created based  
--              on the codes originally written by Youping 
-- Modified By:
-- Modified Date:
-- ===============================================================================================================================

CREATE PROCEDURE [dbo].[ETLPatientIdType] 
AS
BEGIN

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientIdType';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientIdType';
	DECLARE @StartTime DATETIME = GETDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

    BEGIN TRY
		----get up to 4 level 
		Create table #IdTypeDesc (
		ID int identity(1,1),
		IdTypeDesc Varchar(100)
		);

		DECLARE @IdTypeValue varchar(200)
		SET  @IdTypeValue=( SELECT TOP 1 [SetValue]  FROM vwPortal_SystemSettings WHERE [SettingType]='Patient');

		DECLARE @IdTypeDescCnt int
		select @IdTypeDescCnt =1+len(@IdTypeValue)- len(replace(@IdTypeValue,',',''));

		---select  @IdTypeValue,@IdTypeDescCnt;
		DECLARE @I INT=1;

		WHILE @I<= @IdTypeDescCnt
		BEGIN
		INSERT INTO #IdTypeDesc (IdTypeDesc)
		SELECT dbo.UFN_SEPARATES_COLUMNS(@IdTypeValue,@I,',');
		SET @I=@I+1;
		END

		SELECT  DISTINCT  a.LbPatientId,a.[ExternalId],left(a.IdTypeDesc + ' - '+s.Name,125) IdTypeDesc, b.ID as sortvalue
		INTO #tmpPatientIdType
		FROM [dbo].[PatientIdReference] a
		INNER JOIN #IdTypeDesc b on a.IdTypeDesc=b.IdTypeDesc
		INNER JOIN [dbo].[SourceSystem] s on s.SourceSystemId=a.SourceSystemId


		SELECT LbPatientId,[ExternalId],IdTypeDesc, RowNbr
		INTO #PatientIdType
		FROM (
		SELECT LbPatientId,[ExternalId],IdTypeDesc,sortvalue, row_number() OVER(PARTITION BY LbPatientID ORDER BY sortvalue ) AS RowNbr
		FROM #tmpPatientIdType
		) a
		WHERE a.RowNbr<=4;

		CREATE CLUSTERED INDEX IDX_PatientIdType ON #PatientIdType (LbPatientId,RowNbr);


		SELECT a.LbPatientId AS PatientId, a.ExternalId,a.IdTypeDesc
		, b.ExternalId AS ExternalId2 ,b.IdTypeDesc AS IdTypeDesc2
		, c.ExternalId AS ExternalId3 ,c.IdTypeDesc AS IdTypeDesc3
		, d.ExternalId AS ExternalId4 ,d.IdTypeDesc AS IdTypeDesc4
		INTO #PatientIdTypeQue
		FROM #PatientIdType a
		left outer join #PatientIdType b on b.LbPatientId=a.LbPatientId and b.rownbr=2 and  a.RowNbr=1
		Left outer join #PatientIdType c on c.LbPatientId=a.LbPatientId and c.rownbr=3 and  a.RowNbr=1
		Left outer join #PatientIdType d on d.LbPatientId=a.LbPatientId and d.rownbr=4 and  a.RowNbr=1
		WHERE a.RowNbr=1

		CREATE CLUSTERED INDEX IDX_PatientIdType ON #PatientIdTypeQue (PatientId);

		SET @RecordCountSource=@@ROWCOUNT;

		SELECT @RecordCountBefore=COUNT(1)
		FROM PatientIdType

		MERGE PatientIdType T
		USING #PatientIdTypeQue S
		ON  (T.PatientId = S.PatientID)
		WHEN MATCHED AND (  T.[PatientId1TypeName]     <> S.IdTypeDesc OR
								T.[PatientId1]         <> S.ExternalId OR
								T.[PatientId2TypeName] <> S.IdTypeDesc2 OR
								T.[PatientId2]         <> S.ExternalId2 OR
								T.[PatientId3TypeName] <> S.IdTypeDesc3 OR
								T.[PatientId3]         <> S.ExternalId3 OR
								T.[PatientId4TypeName] <> S.IdTypeDesc4 OR
								T.[PatientId4]         <> S.ExternalId4)
		 THEN UPDATE SET T.[PatientId1TypeName] = S.IdTypeDesc,
						 T.[PatientId1]         = S.ExternalId,
						 T.[PatientId2TypeName] = S.IdTypeDesc2,
						 T.[PatientId2]         = S.ExternalId2,
						 T.[PatientId3TypeName] = S.IdTypeDesc3,
						 T.[PatientId3]         = S.ExternalId3,
						 T.[PatientId4TypeName] = S.IdTypeDesc4,
						 T.[PatientId4]         = S.ExternalId4,
						 T.[ModifyDateTime]     = GETDATE()

		WHEN NOT MATCHED BY TARGET
			 THEN INSERT(PatientID, [PatientId1TypeName],[PatientId1],[PatientId2TypeName],[PatientId2],[PatientId3TypeName],[PatientId3],[PatientId4TypeName],[PatientId4],[CreateDateTime],[ModifyDateTime]) 
			 VALUES (S.PatientID,S.IdTypeDesc,S.ExternalId,S.IdTypeDesc2,S.ExternalId2 ,S.IdTypeDesc3,S.ExternalId3 ,S.IdTypeDesc4,S.ExternalId4, GETDATE(), GETDATE())
		--WHEN NOT MATCHED BY SOURCE
		--    THEN DELETE
		;
        
		-- get total records from [dbo].PatientDiagnosis after merge 
		SELECT @RecordCountAfter=count(1)
		FROM [dbo].PatientIdType;

        -- Calculate records inserted and updated counts
	    SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	    SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

        -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	

	END TRY

	BEGIN CATCH
	    --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETDATE();
		SET  @ErrorNumber =ERROR_NUMBER();
		SET  @ErrorState =ERROR_STATE();
		SET  @ErrorSeverity=ERROR_SEVERITY();
		SET  @ErrorLine=ERROR_LINE();
		SET  @Comment =left(ERROR_MESSAGE(),400);
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

	DROP TABLE #IdTypeDesc
	DROP TABLE #tmpPatientIdType
	DROP TABLE #PatientIdType
	DROP TABLE #PatientIdTypeQue
END


GO
