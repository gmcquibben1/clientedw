SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientDiagnosisLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		pd.PatientId, 
		pd.SourceSystemId, 
		pd.DiagnosisCodingSystem,
		pd.DiagnosisCode,
		pd.DiagnosisDescription ,
		--pd.DiagnosisDescriptionAlternate,
		pd.DiagnosisType, 
		pd.DiagnosisStatus,
		pd.DiagnosisSeverity, 
		--pd.DiagnosisConfidentialityInd,
		pd.DiagnosisPriority,
		pd.DiagnosisClassification,
		pd.DiagnosisDateTime, 
		pd.DiagnosisOnsetDate,
		pd.DiagnosisResolvedDate,
		pd.DiagnosisComment, 
		pd.EncounterID ,
		pd.ProviderID , 
		pd.Clinician ,
		pd.ActiveInd ,
		pd.DiagnosisCodeRaw,
		pd.DiagnosisCodeDisplay
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (pd.CreateDateTime > @dtmStartDate OR pd.ModifyDateTime > @dtmStartDate)
 
END
GO
