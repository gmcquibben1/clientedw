SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



Create PROCEDURE [dbo].[ETLPatientAllergy]	
	@fullLoadFlag bit = 0, 
	@BatchSize INT = 10000, 
	@InterfacePatientId VARCHAR(50) = NULL,
	@overrideMinIndex INT = NULL
AS

  /*===============================================================
	CREATED BY: 	Youping
	CREATED ON:		2016-06-28
	INITIAL VER:	2.0.1
	MODULE:			ETL
	DESCRIPTION:	Load Interface PatientAllergy Data into EDW
	PARAMETERS:		
		@fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
		 @fullLoadFlag =0 Merge date to the existing table
		 @BatchSize  Number of patients to do in each batch
		 @InterfacePatientId    Id of the patient identifier if we want to run this for a specific patietn
		 @overrideMinIndex		Minimum id of the InterfacePatientAllergy record to start processing from. Skips the
								Value stored in the Maintenance.dbo.EDWTableLoadTracking table

	RETURN VALUE(s)/OUTPUT:	
		Error and logging information will be written to the EDWTableLoadTrackingInsert table

	
	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.0.1		2016-06-22		Youping					Created
	2.1.2		2017-02-06		CJL			LBETL-1096	Rolling up to the latest non-null value and addressed field truncation issue

														 

=================================================================*/
BEGIN





 
    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientAllergy';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientAllergy';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @RowCount INT =0;
	DECLARE @NumRows INT = 0;
 	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);

	
	BEGIN TRY  		

			
	
		--1) Retrieve the last run time id values.  We are going to pull both the date and the last run time id. If the last max id 
		--is null, calculate a best guese based on the 
		--We are changing away from using date values for query purposes as it is more efficient to query by the Id value.  Since
		--the id is a seeded, integer greater id values are implicitly newer. It also allows us to pick up records that come in while the 
		--procedure is running the next time it runs
		IF @fullLoadFlag = 1
		BEGIN
			UPDATE Maintenance.dbo.EDWTableLoadTracking 
				SET maxSourceTimeStampProcessed = CAST('1900-01-01' AS DATETIME), maxSourceIdProcessed= 0
				WHERE EDWtableName = @EDWtableName AND dataSource ='interface' AND EDWName =@EDWName
		END

		 SET @lastDateTime = (SELECT COALESCE(maxSourceTimeStampProcessed,CAST('1900-01-01' AS DATETIME)) 
									FROM Maintenance.dbo.EDWTableLoadTracking 
									WHERE EDWtableName = @EDWtableName 
									AND dataSource ='interface' AND EDWName =@EDWName);
	
	
		
		SET  @lastIndex = (SELECT maxSourceIdProcessed
									FROM Maintenance.dbo.EDWTableLoadTracking 
									WHERE EDWtableName = @EDWtableName 
									AND dataSource ='interface' AND EDWName =@EDWName);

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex= (SELECT MIN(InterfacePatientAllergyId) FROM  vwPortal_InterfacePatientAllergy IPS WITH (NOLOCK) WHERE IPS.CreateDateTime >= @lastDateTime ) 
		END

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex = 0 
		END


		if @overrideMinIndex IS NOT NULL
		BEGIN
			SET @lastIndex = @overrideMinIndex
		END 

		SET @maxIndex = (SELECT MAX(InterfacePatientAllergyId) FROM  vwPortal_InterfacePatientAllergy IPS WITH (NOLOCK));
	
		

		/*
		DROP TABLE #PatientAllergyQue
		DROP TABLE #tmpPatientAllergy
		DROP TABLE #ProcedureCodingSystemType
		DROP TABLE #PatientAllergyQuePatientIdentifieBATCH
		Drop table #PatientAllergyQuePatientIdentifier
		DROP TABLE #PatientAllergy
		DROP TABLE #AllergyCodingSystemType
		DROP TABLE #AllergyCode
		*/


		--2) Get the full list of patient identifiers that we are going to be dealing with
		--We are going to breaking this up into batches along patient id lines.
		SELECT  DISTINCT 
			IP.PatientIdentifier, IP.InterfaceSystemId
		INTO #PatientAllergyQuePatientIdentifier	
		FROM dbo.vwPortal_InterfacePatientAllergy IPS with (nolock)
			INNER JOIN dbo.vwPortal_InterfacePatient IP with (nolock) ON IPS.InterfacePatientId = IP.InterfacePatientId
			INNER JOIN dbo.vwPortal_InterfaceSystem  ISS with (nolock) ON IP.InterfaceSystemId = ISS.InterfaceSystemId
		WHERE ( IPS.InterfacePatientAllergyId >= @lastIndex  AND IPS.InterfacePatientAllergyId <= @maxIndex )
		 AND ( @InterfacePatientId IS NULL OR (IPS.InterfacePatientId = @InterfacePatientId))
			
						
	
		
	
	    SELECT @RecordCountBefore=COUNT(1)	FROM dbo.PatientAllergy  WITH (NOLOCK);

	
		--3) Loop throught all of the batches to improve memory efficency
		WHILE 1 = 1
		BEGIN	


				SELECT TOP (@BatchSize) PatientIdentifier, InterfaceSystemId INTO #PatientAllergyQuePatientIdentifieBATCH FROM #PatientAllergyQuePatientIdentifier;


					Create  NONCLUSTERED INDEX IDX_PatientAllergyQuePatientIdentifieBATCH_PatientIdentifier ON #PatientAllergyQuePatientIdentifieBATCH (PatientIdentifier, InterfaceSystemId);
			
    				SET @NUMROWS = ( SELECT COUNT(1) FROM #PatientAllergyQuePatientIdentifieBATCH)

	
				IF @NUMROWS <> 0 
				BEGIN



			
							SELECT InterfacePatientAllergyID
								,ISS.SourceSystemId
								,idRef.lbPatientId as PatientID
								,AllergyType
								,ISNULL(AllergyDescription,'') as AllergyDescription
								,AllergySeverity
								,AllergyReaction
								,ISNULL(AllergyComment,'') as AllergyComment
								,OnsetDate
								,IPA.CreateDateTime
								INTO #PatientAllergyQue 
									FROM dbo.vwPortal_InterfacePatientAllergy IPA with (nolock),
										dbo.vwPortal_InterfacePatient IP with (nolock),
										 dbo.vwPortal_InterfaceSystem  ISS with (nolock),
						 				 dbo.PatientIdReference idRef WITH (NOLOCK),
										 #PatientAllergyQuePatientIdentifieBATCH batch WITH (NOLOCK)
										WHERE 
										 (IPA.InterfacePatientAllergyId >= @lastIndex  AND IPA.InterfacePatientAllergyId <= @maxIndex) 
										AND IPA.InterfacePatientId = IP.InterfacePatientId
										AND IP.InterfaceSystemId = ISS.InterfaceSystemId
										AND IP.PatientIdentifier = batch.PatientIdentifier AND IP.InterfaceSystemId = batch.InterfaceSystemId
										AND idRef.SourceSystemId = ISS.SourceSystemId 
										AND idRef.ExternalId = batch.PatientIdentifier 
										AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
										AND idRef.DeleteInd =0			

					
				

							  --- add Index
							  Create  NONCLUSTERED INDEX IDX_PatientIdentifier_SourceSystemId ON #PatientAllergyQue (PatientID,SourceSystemId);
							  Create  NONCLUSTERED INDEX IDX_AllergyType_Allergy ON #PatientAllergyQue (AllergyType);
							  Create  NONCLUSTERED INDEX IDX_AllergySeverity ON #PatientAllergyQue (AllergySeverity);
							  Create  NONCLUSTERED INDEX IDX_AllergyReaction ON #PatientAllergyQue (AllergyReaction);
			
							----AllergyType
								MERGE INTO dbo.AllergyType AS target
								USING (SELECT distinct AllergyType
									   FROM #PatientAllergyQue 
									  where ISNULL(AllergyType,'')<> ''  ) AS source
								ON source.AllergyType = target.Name
								WHEN NOT MATCHED BY TARGET THEN
									INSERT (Name,DisplayValue,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
									VALUES (source.AllergyType,source.AllergyType,0,@Today,@Today,1,1);
							 
							 
							 
							 ---AllergySeverityType
								MERGE INTO dbo.AllergySeverityType AS target
								USING (SELECT distinct AllergySeverity
									   FROM #PatientAllergyQue 
									  where ISNULL(AllergySeverity,'')<> ''  ) AS source
								ON source.AllergySeverity = target.Name
								WHEN NOT MATCHED BY TARGET THEN
									INSERT (Name,DisplayValue,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
									VALUES (source.AllergySeverity,source.AllergySeverity,0,@Today,@Today,1,1);

							 ----AllergyReactionType
								MERGE INTO dbo.AllergyReactionType AS target
								USING (SELECT distinct AllergyReaction
									   FROM #PatientAllergyQue 
									  where ISNULL(AllergyReaction,'')<> ''  ) AS source
								ON source.AllergyReaction = target.Name
								WHEN NOT MATCHED BY TARGET THEN
									INSERT (Name,DisplayValue,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
									VALUES (source.AllergyReaction,source.AllergyReaction,0,@Today,@Today,1,1);

							-----   dedup data prepare for Merge
							 SELECT * 
							 INTO #PatientAllergy
							 FROM (
								SELECT  DISTINCT 
										PA.SourceSystemId
										,PA.PatientID
										,ISNULL(AT.AllergyTypeID,0) AllergyTypeID
										,PA.AllergyDescription
										,ISNULL(AST.AllergySeverityTypeID,0) AllergySeverityTypeID
										,ISNULL(ART.AllergyReactionTypeID,0) AllergyReactionTypeID
										,comment.AllergyComment
										,odate.OnsetDate
										,0 as AllergyStatusTypeID
										,0 as EncounterID,
								        PA.CreateDateTime
									FROM  
									(SELECT DISTINCT InterfacePatientAllergyId, AllergyDescription, AllergyType,
										 AllergySeverity, AllergyReaction, PatientID, CreateDateTime, SourceSystemId, 
										ROW_NUMBER() OVER ( PARTITION BY ISNULL(AllergyDescription, ''), PatientID, ISNULL(AllergyType, '') , SourceSystemId
										ORDER BY 	InterfacePatientAllergyId DESC ) RowNbr
										FROM  #PatientAllergyQue  with (nolock)	
									) PA 
									LEFT OUTER JOIN
										(SELECT DISTINCT AllergyDescription, PatientID, AllergyType, SourceSystemId,  AllergyComment,
										ROW_NUMBER() OVER ( PARTITION BY ISNULL(AllergyDescription, ''), PatientID, ISNULL(AllergyType, '') , SourceSystemId
										ORDER BY 	InterfacePatientAllergyId DESC ) RowNbr
										FROM  #PatientAllergyQue  with (nolock)	
										WHERE AllergyComment IS NOT NULL AND AllergyComment <> ''
										
									) comment ON
										PA.SourceSystemID = comment.SourceSystemID AND PA.PatientID =comment.PatientID 
										AND ISNULL(PA.AllergyType,0) = ISNULL(comment.AllergyType,0) AND ISNULL(comment.AllergyDescription,'') = ISNULL(PA.AllergyDescription, '')
									LEFT OUTER JOIN
										(SELECT DISTINCT AllergyDescription, PatientID, AllergyType, SourceSystemId,  OnsetDate,
										ROW_NUMBER() OVER ( PARTITION BY ISNULL(AllergyDescription, ''), PatientID, ISNULL(AllergyType, '') , SourceSystemId
										ORDER BY 	InterfacePatientAllergyId DESC ) RowNbr
										FROM  #PatientAllergyQue  with (nolock)	
										WHERE OnsetDate IS NOT NULL -- AND OnsetDate <> ''
										
									) odate ON
										PA.SourceSystemID = odate.SourceSystemID AND PA.PatientID =odate.PatientID 
										AND ISNULL(PA.AllergyType,0) = ISNULL(odate.AllergyType, 0) AND ISNULL(odate.AllergyDescription,'') = ISNULL(PA.AllergyDescription , '') 
									LEFT OUTER JOIN AllergyType AT WITH (NOLOCK) ON PA.AllergyType = At.Name
									LEFT OUTER JOIN AllergySeverityType AST WITH (NOLOCK) ON PA.AllergySeverity = AST.Name
									LEFT OUTER JOIN AllergyReactionType ART WITH (NOLOCK) ON PA.AllergyReaction = ART.Name
									WHERE PA.RowNbr=1
									AND (odate.RowNbr = 1 OR odate.RowNbr IS NULL)
									AND (comment.RowNbr = 1 OR comment.RowNbr IS NULL)
								) A
								




	
							  CREATE CLUSTERED INDEX CIX_#PatientID_DiagnosisCode ON #PatientAllergy(SourceSystemId, PatientId,AllergyTypeID,AllergyDescription) ;

							  --- test select * from #PatientAllergy


								MERGE DBO.PatientAllergy AS target
								USING #PatientAllergy AS source 

								ON (target.SourceSystemID = source.SourceSystemID AND target.PatientID =source.PatientID 
									AND target.AllergyTypeID = source.AllergyTypeID AND ISNULL(target.AllergyDescription,'') = ISNULL(source.AllergyDescription, '')
									)
								WHEN MATCHED THEN UPDATE SET 
		  						  target.SourceSystemID= ISNULL(source.SourceSystemID ,target.SourceSystemID),
								  target.AllergySeverityTypeID= ISNULL(source.AllergySeverityTypeID ,target.AllergySeverityTypeID), 
								  target.AllergyReactionTypeID= ISNULL(source.AllergyReactionTypeID ,target.AllergyReactionTypeID),
								  target.AllergyStatusTypeId= ISNULL(source.AllergyStatusTypeId ,target.AllergyStatusTypeId ),
								  target.AllergyComment= ISNULL(source.AllergyComment ,target.AllergyComment ),
								  target.EncounterID= ISNULL(source.EncounterID ,target.EncounterID),
								  target.ActiveInd= 1 ,
								  target.OnsetDate = ISNULL(source.OnsetDate,target.OnsetDate),
								  target.DeleteInd= 0 ,
								  target.ModifyDateTime= @Today ,
								  target.ModifyLBUserId= 1 
								WHEN NOT MATCHED THEN
								INSERT (PatientID,SourceSystemID,AllergyTypeID,AllergyDescription,AllergySeverityTypeID,AllergyReactionTypeID
									  ,AllergyStatusTypeId,AllergyComment,EncounterID,ActiveInd,OnsetDate
									  ,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId
									   )
								VALUES (source.PatientID,source.SourceSystemID,source.AllergyTypeID,source.AllergyDescription,source.AllergySeverityTypeID,source.AllergyReactionTypeID
									  ,source.AllergyStatusTypeId,source.AllergyComment,source.EncounterID,1,source.OnsetDate
									  ,0,source.CreateDateTime,@Today,1,1)
										;


	

					-- update lastdate
               		   
						--Update the max processed time and drop the temp tables
						SET @tmpmaxSourceTimeStampProcessed = 	(SELECT MAX(CreateDateTime) FROM #PatientAllergy )
						IF @tmpmaxSourceTimeStampProcessed > @maxSourceTimeStampProcessed
						BEGIN
						SET @maxSourceTimeStampProcessed = @tmpmaxSourceTimeStampProcessed
						END

					  --clean
					  drop table  #PatientAllergy,#PatientAllergyQue
	
 				
					END
			
			
				--Move onto the next batch
				DELETE FROM #PatientAllergyQuePatientIdentifier WHERE PatientIdentifier IS NULL

				DELETE FROM #PatientAllergyQuePatientIdentifier FROM 
					#PatientAllergyQuePatientIdentifier id, #PatientAllergyQuePatientIdentifieBATCH batch
				WHERE id.PatientIdentifier = batch.PatientIdentifier AND id.interfacesystemid  = batch.interfacesystemid


				DROP TABLE #PatientAllergyQuePatientIdentifieBATCH
				IF @NUMROWS = 0 BREAK;
  
			END
		
			DROP TABLE #PatientAllergyQuePatientIdentifier
	
 
 		-- update maxSourceTimeStamp with max CreateDateTime of #PatientAllergyQue
			UPDATE Maintenance..EDWTableLoadTracking
			SET maxSourceTimeStampProcessed = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex
			WHERE EDWtableName = @EDWtableName AND dataSource ='interface'  AND EDWName = @EDWName


			-- get total records from PatientAllergy after merge 
			SELECT @RecordCountAfter=COUNT(1)  FROM PatientAllergy WITH (NOLOCK);

			-- Calculate records inserted and updated counts
			SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
			SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

			-- insert log information to Maintenance.dbo.EdwProcedureRunLog
			SET  @EndTime=GETUTCDATE();
			EXEC Maintenance.dbo.EdwInsertProcedureRunLog @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	
		



	END TRY
	BEGIN CATCH
	           --- insert error log information to Maintenance.dbo.EdwProcedureRunLog
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC Maintenance.dbo.EdwInsertProcedureRunLog @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END


GO
