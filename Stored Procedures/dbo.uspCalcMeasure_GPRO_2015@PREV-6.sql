SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@PREV-6] @LookbackTimestamp DATETIME='2000-01-01 00:00:00' ,@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL DROP TABLE [#RANKEDPATIENTS];
	IF OBJECT_ID('tempdb..#PatientsVisit')  IS NOT NULL DROP TABLE [#PatientsVisit];
	IF OBJECT_ID('tempdb..#NumPerformed')   IS NOT NULL DROP TABLE [#NumPerformed];
        
	DECLARE  @yearend datetime, @measureBeginDate datetime;

	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2015';

	--Establishing the Clinical Data sourcing indicators.
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	--------------------------------------------------------------------------------------------
	--Initial Patient pooling, for patients that have ranking for the BCS measure. 
	--------------------------------------------------------------------------------------------
	SELECT r.LBPatientID,
        r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,@measureBeginDate) as Age,
		r.Birthdate,
        GM.GproMeasureTypeID,
		GPM.[PcColorectalConfirmed],
		GPM.[PcColorectalPerformed]
	INTO #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE Name = 'Prev-6' 
		AND (GPM.PcColorectalConfirmed IS NULL OR GPM.PcColorectalPerformed IS NULL)
   
   
	/*  -------------------- Denominator: Exclusion ----------- 
	c) Colorectal cancer screening not performed
	d) using cpt code with modifier in CCLF_2_PartA_RCDetail and CCLF_5_PartB_Physicians field HCPCS_1_MDFR_CD to HCPCS_5_MDFR_CD
  
	Also, checks for Age
	*/
	--Age Check
	UPDATE rp
	SET    [PcColorectalConfirmed] = '19'
	FROM   #RankedPatients rp
	WHERE [PcColorectalConfirmed] IS NULL
		AND (rp.Age > 75 OR rp.Age < 50) 
    
	
  ---Cancer or Colectomy Check
	UPDATE RP
	SET   [PcColorectalConfirmed] = 17 
	FROM  #RankedPatients RP
	WHERE [PcColorectalConfirmed] IS NULL
		AND EXISTS (SELECT TOP 1 1
					FROM vwPatientProcedure pp
						INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
							AND ModuleType = 'PREV'
							AND ModuleIndicatorGPRO = '6'
							AND VariableName = 'EX_CODE'
						INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
					WHERE pp.PatientId = RP.LbPatientId
					)
					
					--FROM DBO.PatientProcedure PP 
					--	JOIN DBO.PatientProcedureProcedureCode PPC ON RP.LbPatientID = PP.PatientID 
					--		AND PP.PatientProcedureId = PPC.PatientProcedureId
					--	JOIN DBO.ProcedureCode PC ON PPC.ProcedureCodeId = PC.[ProcedureCodeId]
					--	JOIN DBO.[GproEvaluationCode] GE ON   PC.ProcedureCode = GE.Code
					--	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
					--WHERE  [ModuleType] = 'PREV'  
					--	AND GE.[ModuleIndicatorGPRO] = '6' 
					--	AND GE.[VariableName] = 'EX_CODE'
				  --)
      
	UPDATE RP
	SET   [PcColorectalConfirmed] = 17 
	FROM  #RankedPatients RP
	WHERE EXISTS (SELECT TOP 1 1
				FROM DBO.PatientDiagnosis PD 					
					JOIN DBO.PatientDiagnosisDiagnosisCode PDDC ON  RP.LbPatientID = PD.PatientID 
						AND PD.PatientDiagnosisId = PDDC.PatientDiagnosisId
					JOIN DBO.DiagnosisCode DC ON PDDC.DiagnosisCodeId = DC.DiagnosisCodeId
					JOIN DBO.[GproEvaluationCode] GE ON  DC.DiagnosisCode = GE.Code	
					JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID]   --DIAGS 
				WHERE  [ModuleType] = 'PREV'  
					AND GE.[ModuleIndicatorGPRO] = '6' 
					AND GE.[VariableName] = 'EX_CODE'
				  ) 
    
	UPDATE rp
	SET    [PcColorectalConfirmed] = '2'
	FROM   #RankedPatients rp
	WHERE PcColorectalConfirmed IS NULL
		AND Age >= 50 AND Age <= 75
		
	--------------------------------------------------------------------------------------------------------    
		-- Encounter Requirement Check
	--------------------------------------------------------------------------------------------------------
	--
	--        SELECT RP.LBPatientID
	--        INTO #PatientsVisit
	--		    FROM #RankedPatients RP
	--        JOIN DBO.PatientProcedure PP
	--            ON  RP.LbPatientID = PP.PatientID 
	--		    JOIN DBO.PatientProcedureProcedureCode PPC		    
	--				    ON  PP.PatientProcedureId = PPC.PatientProcedureId
	--		    JOIN DBO.ProcedureCode PC 
	--				    ON  PPC.ProcedureCodeId = PC.[ProcedureCodeId]
	--        JOIN DBO.[GproEvaluationCode] GE
	--            ON   PC.ProcedureCode = GE.Code				
	--        WHERE  [ModuleType] = 'PREV'  
	--        AND GE.[ModuleIndicatorGPRO] = '6' 
	--        AND GE.[VariableName] = 'ENCOUNTER_CODE'
	--               
           

	--------------------------------------------------------------------------------------------------------    
		-- Numerator Checks  FOBT, Sigmoidoscopy, or Colonoscopy, by proc and then Lab for FOBT
	-------------------------------------------------------------------------------------------------------- 
 
	----Procedure Check
	--SELECT DISTINCT  
	--	RP.LbPatientId,
	--	MAX(pp.ProcedureDateTime) as LastVisit
	--INTO #NumPerformed
	--FROM #RankedPatients RP
	--	INNER JOIN vwPatientProcedure pp ON pp.PatientId = RP.LbPatientId
	--	INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
	--		AND ModuleType = 'PREV'
	--		AND ModuleIndicatorGPRO = '6'
	--	INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
	--WHERE RP.PcColorectalConfirmed = '2'
	--	AND DATEDIFF(YY,pp.ProcedureDateTime,@yearEnd) <= (CASE WHEN gec.[VariableName] = 'FOBT_CODE' THEN  1
	--															  WHEN gec.[VariableName] = 'FLEX_SIG_CODE' THEN  5
	--															  WHEN gec.[VariableName] = 'COLOSCOPE_CODE' THEN  10
	--															  ELSE 0
	--														 END)
	--GROUP BY RP.LbpatientId

	SELECT DISTINCT  
		RP.LbPatientId,
		MAX(pp.ProcedureDateTime) as LastVisit
	INTO #NumPerformed
	FROM #RankedPatients RP
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = RP.LbPatientId
			AND pp.ProcedureDateTime >= DATEADD(year,-1,@yearEnd)
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND ModuleType = 'PREV'
			AND ModuleIndicatorGPRO = '6'
			AND gec.[VariableName] = 'FOBT_CODE'
	WHERE RP.PcColorectalConfirmed = '2'
	GROUP BY RP.LbpatientId

	INSERT INTO #NumPerformed
	(LbPatientId,LastVisit)
	SELECT DISTINCT  
		RP.LbPatientId,
		MAX(pp.ProcedureDateTime) as LastVisit
	FROM #RankedPatients RP
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = RP.LbPatientId
			AND pp.ProcedureDateTime >= DATEADD(year,-5,@yearEnd)
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND ModuleType = 'PREV'
			AND ModuleIndicatorGPRO = '6'
			AND gec.[VariableName] = 'FLEX_SIG_CODE'
	WHERE RP.PcColorectalConfirmed = '2'
	GROUP BY RP.LbpatientId

	INSERT INTO #NumPerformed
	(LbPatientId,LastVisit)
	SELECT DISTINCT  
		RP.LbPatientId,
		MAX(pp.ProcedureDateTime) as LastVisit
	FROM #RankedPatients RP
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = RP.LbPatientId
			AND pp.ProcedureDateTime >= DATEADD(year,-10,@yearEnd)
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND ModuleType = 'PREV'
			AND ModuleIndicatorGPRO = '6'
			AND gec.[VariableName] = 'COLOSCOPE_CODE'
	WHERE RP.PcColorectalConfirmed = '2'
	GROUP BY RP.LbpatientId

  
	---Labs Check  @yearend
	INSERT INTO #NumPerformed ( LBPATIENTID,LastVisit) 
	SELECT RP.LBPATIENTID, MAX(PLR.ObservationDate) 
	FROM #RankedPatients RP
		JOIN DBO.PatientLabOrder PLO ON  RP.LbPatientID = PLO.PatientID
		JOIN DBO.PatientLabResult PLR ON  PLO.PatientLabOrderId = PLR.PatientLabOrderId
		JOIN DBO.[GproEvaluationCode] GE WITH (NOLOCK) ON  PLR.ObservationCode1 = GE.CODE
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID]    --LABS
	WHERE GE.[VariableName] = 'FOBT_CODE'
		AND   DATEDIFF(YY,PLR.ObservationDate,@yearEnd) <= 1
		AND RP.[PcColorectalConfirmed] = N'2'
	GROUP BY RP.LBPatientID
        

	--------------------------------------------------------------------------------------------------------          
   -- Updating Temp table to reflect those who had the procedure perfromed
	--------------------------------------------------------------------------------------------------------           
        
    UPDATE rp
    SET  PcColorectalPerformed = 2
    FROM #RankedPatients rp
		JOIN #NumPerformed np ON rp.LBPatientID = np.LBPATIENTID
            
	--------------------------------------------------------------------------------------------------------          
	-- Updating the required tables
	--------------------------------------------------------------------------------------------------------    
	
	UPDATE gpm
	SET GPM.PCColorectalConfirmed  = GPR.PCColorectalConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcColorectalRank IS NOT NULL
		AND gpm.[PcColorectalConfirmed] IS NULL

	UPDATE gpm
	SET   gpm.[PcColorectalPerformed] = gpr.[PcColorectalPerformed], gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcColorectalRank IS NOT NULL
		AND gpm.[PcColorectalPerformed] IS NULL


	--  --This will need to be added to the joins to check for Clinical vs Claims. Add logic that when the parameter is 'Y' only take Clinical 
	--   SELECT TOP 1 *
	--   FROM SOURCESYSTEM --CLINICALIND 
	--  
	--   
	--            
	--    
	--  Select *
	--  from #RankedPatients
	--          
	--          SET ModifyDateTime = GETUTCDATE()


END

     
GO
