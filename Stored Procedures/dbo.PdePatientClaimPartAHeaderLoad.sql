SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientClaimPartAHeaderLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT DISTINCT
		pa.ClaimId,  
		pa.PatientId,
		pa.ClaimFromDate, 
		pa.ClaimToDate, 
		pa.DiagnosisCode, 
		pa.DiagnosisShortDescription, 
		pa.DiagnosisLongDescription, 
		pa.DiagGroup1, 
		pa.DiagGroup2, 
		pa.DiagGroup3,
		pa.PaidAmt, 
		pa.DrgCode, 
		pa.FacilityNPI, 
		pa.FacilityName,
		pa.AttendingNPI, 
		pa.AttendingProviderName,
		pa.AdmissionTypeCode, 
		pa.AdmissionSourceCode 
	FROM PatientDataExportList pdel
		 INNER JOIN vwClaimPartAHeader pa ON pa.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (pa.ClaimFromDate > @dtmStartDate)
 
END
GO
