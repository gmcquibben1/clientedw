SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalMeasuresByNPI]
AS
BEGIN



SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'ACO_MM_NPI_Measures';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalMeasuresByNPI';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();





BEGIN TRY 


/* TRUNCATE TABLE */
TRUNCATE TABLE ACO_MM_NPI_Measures


/* FILL TABLE WITH INITIAL DATA */
INSERT INTO ACO_MM_NPI_Measures (
   ACO_ID
  ,ACO_Name
  ,TIN_Number
  ,TIN_Name
  ,Provider_NPI
  ,Provider_Name
  ,DOS_First
  ,MM
  ,A
  ,PRO
  ,DME
) 
SELECT
 O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.ProviderNPI, O.ProviderName, 
 DOS_First, 
 COUNT(p.LbPatientId) AS MM, 
 SUM([Part-A Cost]) AS A, 
 SUM([Professional Cost]) AS PRO, 
 SUM([DME Cost]) AS DME 
 FROM MM_Metics_Pivot p
 JOIN OrgHierarchy O
 ON p.LbPatientId = O.LbPatientId
GROUP BY 
 O.Level1Id, O.Level1Name, O.Level2Id, O.Level2Name, O.ProviderNPI, O.ProviderName, DOS_First
 
 
 /* UPDATE TOTAL */
 UPDATE ACO_MM_NPI_Measures SET
  Total = A+PRO+DME -- decimal(38, 0)
 ,PPPM = (A+PRO+DME)/MM -- decimal(38, 0)
 
 /* TRUNCATE & FILL TABLE W/ BEST-IN CLASS & AVG */
-- TRUNCATE TABLE TEMP_AVG
-- TRUNCATE TABLE TEMP_TINAVG
-- TRUNCATE TABLE TEMP_BIC
-- 
 
 
 /* FILL TEMP TABLES W/ INFO */
-- INSERT INTO #TEMP_BIC (
--   DOS_First
--  ,ACO_ID
--  ,ppbic
--) 
SELECT DOS_First, ACO_ID, MIN(PPPM) as ppbic 
 INTO #TEMP_BIC
 FROM ACO_MM_NPI_Measures 
 WHERE MM > 25
 GROUP BY DOS_First, ACO_ID


--INSERT INTO TEMP_AVG (
--   DOS_First
--  ,ACO_ID
--  ,ppavg
--) 
  SELECT DOS_First, ACO_ID, (sum(Total)/sum(MM)) as ppavg 
  INTO #TEMP_AVG
 FROM ACO_MM_NPI_Measures 
 GROUP BY DOS_First, ACO_ID
  

-- INSERT INTO TEMP_TINAVG (
--   DOS_First
--  ,ACO_ID
--  ,TIN_Number
--  ,ppavg
--) 
   SELECT DOS_First, ACO_ID, TIN_Number, (sum(Total)/sum(MM)) as ppavg 
   INTO #TEMP_TINAVG
 FROM ACO_MM_NPI_Measures 
 GROUP BY DOS_First, ACO_ID, TIN_Number
  
 
  /* UPDATE BEST IN CLASS */
 UPDATE ACO_MM_NPI_Measures SET
  PPPM_BiC = ppbic -- decimal(38, 0)
  FROM ACO_MM_NPI_Measures as a 
  JOIN #TEMP_BIC as b ON
  a.DOS_First = b.DOS_First 
  AND a.ACO_ID = b.ACO_ID 
  
  /* UPDATE ACO AVERAGE */
 UPDATE ACO_MM_NPI_Measures SET
  PPPM_ACO_Avg = ppavg -- decimal(38, 0)
  FROM ACO_MM_NPI_Measures as a 
  JOIN #TEMP_AVG as b ON
  a.DOS_First = b.DOS_First 
  AND a.ACO_ID = b.ACO_ID  

  /* UPDATE TIN AVERAGE */
 
 UPDATE ACO_MM_NPI_Measures SET
  PPPM_TIN_Avg = ppavg -- decimal(38, 0)
  FROM ACO_MM_NPI_Measures as a 
  JOIN #TEMP_TINAVG as b ON
  a.DOS_First = b.DOS_First 
  AND a.ACO_ID = b.ACO_ID
  AND a.TIN_Number = b.TIN_Number
  
  
 /* UDPATE TIN AND PROVIDER NAMES  --- OLD, NO LONGER IN USE
 UPDATE ACO_MM_NPI_Measures SET
 TIN_Name = b.TIN_Name -- nvarchar(50)
 ,Provider_Name = b.Provider_Name -- nvarchar(50)
FROM ACO_MM_NPI_Measures as a JOIN 
 dbo.ACO_Hierarchy as b ON 
  a.TIN_Number = b.TIN_Number AND
  b.Provider_NPI = a.Provider_NPI
  
  */
  
  /* UPDATE PPPM TARGETS */
  UPDATE ACO_MM_NPI_Measures SET
  PPPM_Target = b.PPPM_Target -- decimal(38, 0)
  FROM ACO_MM_NPI_Measures as a 
  JOIN ACO_PPPM_TARGETS as b
  ON a.ACO_ID = b.ACO_ID AND
  a.DOS_First = b.DOS_First
  
  /* TRUNCATE & FILL TABLE W/ BEST-IN CLASS & AVG */
DROP TABLE #TEMP_AVG
DROP TABLE #TEMP_TINAVG
DROP TABLE #TEMP_BIC



		SELECT @InsertedRecord=count(1)
		FROM [ACO_MM_NPI_Measures] ;


	-----Log Information
		
	 
          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
