SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientMedicationLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		pm.PatientId,
		pm.SourceSystemId, 
		pm.NDCCode,
		pm.RxNormCode,
		(CASE WHEN pm.MedicationNameFromSource IS NOT NULL AND pm.MedicationNameFromSource <> '' THEN pm.MedicationNameFromSource ELSE pm.BrandName END) AS MedicationName,
		(CASE WHEN pm.MedicationDoseFromSource IS NOT NULL AND pm.MedicationDoseFromSource <> '' THEN pm.MedicationDoseFromSource ELSE pm.MedicationDose END) AS MedicationDose,
		(CASE WHEN pm.MedicationFormFromSource IS NOT NULL AND pm.MedicationFormFromSource <> '' THEN pm.MedicationFormFromSource ELSE pm.MedicationForm END) AS MedicationForm,
		(CASE WHEN pm.MedicationRouteFromSource IS NOT NULL AND pm.MedicationRouteFromSource <> '' THEN pm.MedicationRouteFromSource ELSE pm.MedicationRoute END) AS MedicationRoute,
		pm.Instructions,
		pm.MedicationStartDate,
		pm.MedicationEndDate,
		pm.Comment,
		pm.Quantity,
		pm.Refills,
		pm.SampleInd,
		pm.GenericAllowedInd
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientMedication pm ON pm.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (pm.CreateDateTime > @dtmStartDate OR pm.ModifyDateTime > @dtmStartDate)
 
END
GO
