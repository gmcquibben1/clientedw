SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientImmunizationLoad] 
	@PatientId INTEGER
AS

SELECT DISTINCT       
	dbo.PatientImmunization.ServiceDate,
	dbo.SourceSystem.Description as SourceSystem,
	dbo.ImmunizationCode.ImmunizationCode,
	dbo.ImmunizationCode.ImmunizationDescription,

	dbo.PatientImmunization.ImmunizationDose,
	dbo.PatientImmunization.ImmunizationUnits,
	dbo.PatientImmunization.MaterialLotNumber,
	dbo.PatientImmunization.MaterialManufacturer,

	dbo.MedicationRouteType.DisplayValue as MedicationRoute

FROM dbo.PatientImmunization INNER JOIN
	 dbo.ImmunizationCode ON ImmunizationCode.ImmunizationCodeId = PatientImmunization.ImmunizationCodeId LEFT OUTER JOIN
	 dbo.SourceSystem ON PatientImmunization.SourceSystemId = SourceSystem.SourceSystemId LEFT OUTER JOIN
     dbo.MedicationRouteType ON MedicationRouteType.MedicationRouteTypeId = PatientImmunization.MedicationRouteTypeId

WHERE dbo.PatientImmunization.PatientId = @Patientid AND dbo.PatientImmunization.DeleteInd = 0

ORDER BY PatientImmunization.Servicedate DESC



GO
