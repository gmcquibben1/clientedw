SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalDiagnosisPriorityInsert] 
    @DiagnosisPriorityTypeId INT OUTPUT ,
	@DiagnosisPriority VARCHAR(255)   = NULL
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @DiagnosisCodeId INT

	IF @DiagnosisPriority IS NOT NULL 
	BEGIN

		BEGIN TRAN

		SELECT @DiagnosisPriorityTypeId = DiagnosisPriorityTypeId FROM DiagnosisPriorityType
		WHERE Name = @DiagnosisPriority

		IF @DiagnosisPriorityTypeId IS NULL
		BEGIN
			INSERT INTO [dbo].DiagnosisPriorityType
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
			VALUES 
			(
				@DiagnosisPriority,
				@DiagnosisPriority,
				@DeleteInd,
				GetDate(),
				GetDate(),
				@LBUserId,
				@LBUserId
			)
			SELECT @intError = @@ERROR,
				@DiagnosisPriorityTypeId = SCOPE_IDENTITY()
		END

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
		END
		ELSE
		BEGIN
			COMMIT TRAN
		END

	END
		
	RETURN @intError


GO
