SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-03-23
-- Description:	Load DiagnosisType Data into EDW
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[ETLDiagnosisType]

AS
BEGIN	
		INSERT INTO [dbo].[DiagnosisType]
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
        SELECT DISTINCT
		t1.[DiagnosisType],
		t1.[DiagnosisType],
		0,
		GETUTCDATE(),
		GETUTCDATE(),
		1,
		1
        FROM PatientDiagnosisProcessQueue t1
		LEFT JOIN [dbo].[DiagnosisType] t2
		ON LTRIM(RTRIM(t1.[DiagnosisType])) = LTRIM(RTRIM(t2.[Name]))
		WHERE t1.[DiagnosisType] IS NOT NULL AND t2.[Name] IS NULL

END
GO
