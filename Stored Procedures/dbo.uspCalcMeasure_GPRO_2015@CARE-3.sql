SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@CARE-3] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
-- declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	---------------------------------------------------------------------------------------
	-- do some cleanup and initialization
	---------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];
	IF OBJECT_ID('tempdb..#ENC') IS NOT NULL 
		DROP TABLE [#ENC];
	IF OBJECT_ID('tempdb..#VisitsConfirmed') IS NOT NULL 
		DROP TABLE [#VisitsConfirmed];
	IF OBJECT_ID('tempdb..#CurrentMeds') IS NOT NULL 
		DROP TABLE [#CurrentMeds];

	DECLARE  @yearend datetime, @measureBeginDate datetime;

	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2015';

	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	---------------------------------------------------------------------------------------
	--Initial Patient pooling, for patients that have ranking for the care-3 measure. 
	---------------------------------------------------------------------------------------
	SELECT r.LBPatientID,
		r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,@measureBeginDate) as Age,
		r.Birthdate,
		GM.GproMeasureTypeID,
		GPM.CareMedDocRank,
		GPM.CareMedDocConfirmed,
		GPM.CareMedDocComments,
		GMed.CareMedDocVisitDate,
		GMed.CareMeddocVisitConfirmed, 
		GMed.MedicationDocumented,
		GETUTCDATE() as ModifyDateTime
	INTO   #RankedPatients --Drop Table #RankedPatients
	FROM   GproPatientRanking r
		INNER JOIN   GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		INNER JOIN   GproMeasureType GM ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		INNER JOIN   GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
		INNER JOIN   GproPatientMedication GMed ON  r.GProPatientRankingId = GMed.GproPatientRankingId
			AND GPM.GProPatientMeasureId = GMed.GproPatientMeasureId
	WHERE  GM.Name = 'CARE-3' 
		AND (GPM.CareMedDocConfirmed IS NULL OR GMed.CaremeddocVisitConfirmed IS NULL)


	---------------------------------------------------------------------------------------
	---CHECK IF PATIENT HAS THE CORRECT ENCOUNTERS DURING MEASURMENT YEAR 
	---------------------------------------------------------------------------------------

	SELECT DISTINCT RP.*, PP.ProcedureDateTime
    INTO #ENC
	FROM #RANKEDPATIENTS RP
		INNER JOIN vwPatientProcedure PP ON RP.LbPatientId = PP.PatientId
			AND PP.ProcedureDateTime >= @measureBeginDate 
			AND PP.ProcedureDateTime <  @yearend
		INNER JOIN DBO.[GproEvaluationCode] HVSC ON PP.ProcedureCode = HVSC.CODE
			AND hvsc.[ModuleType]= 'care'
			AND hvsc.[ModuleIndicatorGPRO]='3'
			AND HVSC.[VariableName] ='ENCOUNTER_CODE'	
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
     
    --Patients COnfirmed for Documentation of Meds
    UPDATE #RankedPatients
    SET    CareMedDocConfirmed = '2' -- Not Confirmed - Age.
    FROM #RankedPatients RP 
	  JOIN #ENC ENC ON RP.LBPatientId = ENC.LBPatientId      
	
--  --Age Check FOR PATIENT WITH ENCOUNTER YOUNGER THAN 65
--    UPDATE #RankedPatients
--    SET    CareFallsConfirmed = '19' -- Not Confirmed - Age.
--    FROM #RankedPatients RP 
--	  JOIN #ENC ENC 
--        ON RP.LBPatientId = ENC.LBPatientId
--    Where ENC.Age_ENC < 65 

	--	select distinct age from #RankedPatients order by age desc
	-- SELECT * FROM GproPatientMeasure

/*

Visit date confirmation and verification

*/
    ---------------------------------------------------------------------------------------
	---Confirm if the patient had a visit on or near the date specified
	---------------------------------------------------------------------------------------

  	SELECT DISTINCT RP.*, 
                    PP.ProcedureDateTime
    INTO #VisitsConfirmed
	FROM #RankedPatients RP
		INNER JOIN vwPatientProcedure PP ON RP.LbPatientId = PP.PatientId
			AND PP.ProcedureDateTime > DATEADD(DAY,-3,CareMedDocVisitDate)
			AND PP.ProcedureDateTime < DATEADD(DAY, 3,CareMedDocVisitDate)
		JOIN DBO.[GproEvaluationCode] HVSC ON PP.ProcedureCode = HVSC.CODE
			AND hvsc.[ModuleType]= 'care'
			AND hvsc.[ModuleIndicatorGPRO]='3'
			AND HVSC.[VariableName] ='ENCOUNTER_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
    WHERE RP.CareMedDocConfirmed = 2      
  
	--medication visits confirmed
    UPDATE #RankedPatients
    SET    CareMedDocVisitConfirmed = '2'
    FROM #RankedPatients RP 
	  JOIN #VisitsConfirmed VC ON RP.LBPatientId = VC.LBPatientId
    WHERE RP.CareMedDocConfirmed = '2'


    --current meds confirmation
    SELECT DISTINCT RP.*
    INTO #CurrentMeds
	FROM #RankedPatients RP
		INNER JOIN #VisitsConfirmed VC ON RP.LbPatientId = VC.LbPatientId
		INNER JOIN vwPatientProcedure PP ON RP.LbPatientId = PP.PatientId
			AND CAST(pp.ProcedureDateTime AS DATE) = CAST(VC.ProcedureDateTime AS DATE)
		INNER JOIN DBO.[GproEvaluationCode] HVSC ON PP.ProcedureCode = HVSC.CODE
			AND hvsc.[ModuleType]= 'care'
			AND hvsc.[ModuleIndicatorGPRO]='3'
			AND HVSC.[VariableName] ='Current_MEDS_DOC'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId    
    WHERE RP.CareMEdDocVisitConfirmed = 2         

	-- yes 
	UPDATE rp
	SET rp.MedicationDocumented = '2' --yes
	FROM #RankedPatients rp
		INNER JOIN #CurrentMeds CM on rp.LBPatientId = CM.LBPatientId
	WHERE rp.CareMedDocVisitConfirmed = 2 

	--------------------------------------------------------------------------------------------------------          
	-- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	--select * from #RankedPatients
   
	UPDATE gpm
	SET	gpm.CareMedDocConfirmed   = GPR.CareMedDocConfirmed,           
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.CareMeddocRank IS NOT NULL
		AND gpm.CareMeddocConfirmed IS NULL


	UPDATE GMED
	SET	GMED.CareMeddocVisitConfirmed = GPR.CareMeddocVisitConfirmed,
		GMED.MedicationDocumented     = GPR.MedicationDocumented,
		GMED.ModifyDateTime = GETUTCDATE(),
		GMED.ModifyLbUserId = 1
	FROM GproPatientMedication GMed 
		INNER JOIN #RankedPatients GPR ON GPR.GproPatientRankingId = GMED.GproPatientRankingId
	WHERE gmed.CareMeddocVisitConfirmed IS NULL
		OR gmed.MedicationDocumented IS NULL




END
	
 
GO
