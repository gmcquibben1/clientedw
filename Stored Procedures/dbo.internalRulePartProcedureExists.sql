SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalRulePartProcedureExists] @timeBegin datetime = '1/1/2015', @timeEnd datetime = '12/31/2015',@procValueSet varchar(250), 
@MetricName varchar(100), @MetricType varchar(50), @RulePartId int = null, @MetricValue varchar(50) = 1
WITH EXEC AS CALLER
AS
DELETE FROM PatientMetric WHERE MetricName = @MetricName

INSERT INTO PatientMetric (
   LbPatientId
  ,MetricName
  ,MetricType
  ,MetricValue
) SELECT DISTINCT
 pp.PatientID, @MetricName, @MetricType, @MetricValue
FROM
			[dbo].[HEDISValueSetCodes] hvsc
			JOIN [dbo].[ProcedureCode] pc
				ON  hvsc.[Value Set Name] IN ( @procValueSet )
				AND pc.[ProcedureCode] = hvsc.[Code]
			JOIN [dbo].[PatientProcedureProcedureCode] pppc
				ON  pppc.[ProcedureCodeId] = pc.[ProcedureCodeId]
			JOIN [dbo].[PatientProcedure] pp
				ON  pp.[PatientProcedureId] = pppc.[PatientProcedureId]
				AND pp.[ProcedureDateTime] >= @timeBegin -- Exam occurred during the measurement year.
				AND pp.[ProcedureDateTime] <  @timeEnd
GO
