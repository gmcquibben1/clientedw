SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-05-03
-- Description:	
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[internalJHACGPatientEdcCodeLoad]

AS
BEGIN	

SELECT DISTINCT [patient_id], edc_codes
INTO #PatientEdcCode
FROM [dbo].[JHACG_DETAIL]
WHERE ISNULL(edc_codes,'')<> ''

SELECT 
patient_id AS LbpatientId, 
t2.[EDC], 
t2.[EDCDescription],
t2.[JHACG_EdcCodeId],
t2.[JHACG_MedcCodeId],
t2.[MEDC], 
t2.[MEDCDescription]
INTO #JHACG_PatientEdcCode
FROM(
SELECT p.patient_id, e.data as edc_code
FROM #PatientEdcCode p
CROSS APPLY  [EdwReferenceData].[dbo].[udf_SplitByXml](edc_codes,' ')AS e
) t1
JOIN [dbo].[vwJHACG_EdcCode] t2
ON t1.edc_code = t2.[EDC]

MERGE [dbo].[JHACG_PatientEdcCode] T
USING #JHACG_PatientEdcCode S
ON (T.LbpatientId = S.LbpatientId
    AND T.JHACG_EdcCodeId = S.JHACG_EdcCodeId )
WHEN MATCHED
     THEN UPDATE SET T.EDCDescription = S.EDCDescription,
	                 T.EDC = S.EDC,
					 T.JHACG_MedcCodeId = S.JHACG_MedcCodeId,
                     T.MEDC = S.MEDC,
					 T.MEDCDescription = S.MEDCDescription
WHEN NOT MATCHED BY TARGET
     THEN INSERT(LbpatientId, EDC,EDCDescription,JHACG_EdcCodeId,JHACG_MedcCodeId,MEDC,MEDCDescription) VALUES(S.LbpatientId, S.EDC,S.EDCDescription,S.JHACG_EdcCodeId,S.JHACG_MedcCodeId, S.MEDC,S.MEDCDescription)
WHEN NOT MATCHED BY SOURCE
    THEN DELETE
;
 
DROP TABLE  #PatientEdcCode;
DROP TABLE  #JHACG_PatientEdcCode;

END
GO
