SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ClinicalDecisionPatientProblemsLoad]
@PatientID INT
AS 


--This stored procedure will return all the anonymous medical problm information for the clinical decisions 
--Evaluation Request
--LBPP - 1463 - Introduced in version 2.01
--CJL - 02.17.2016  -- First Cut


select distinct
	pcc.PatientChronicConditionId, dc.DiagnosisDescription, dc.DiagnosisCodeRaw, pd.DiagnosisDateTime, dcst.Name as 'CodeSystem',
	 pd.ActiveInd, pd.DiagnosisConfidentialityInd, pd.DiagnosisResolvedDate, ISNULL(dst.Name, 'UNKNOWN') as 'DiagnosisStatus', 
	 ChronicInd

FROM 
	PatientChronicCondition	pcc WITH (NOLOCK)  ,
	PatientDiagnosis pd WITH (NOLOCK)
	LEFT JOIN DiagnosisStatusType dst WITH (NOLOCK) ON  pd.DiagnosisStatusTypeId = dst.DiagnosisStatusTypeId, 
	PatientDiagnosisDiagnosisCode pddc WITH (NOLOCK), 
	DiagnosisCode dc WITH (NOLOCK), 
	DiagnosisCodingSystemType dcst WITH (NOLOCK)
WHERE
	pd.DeleteInd = 0 AND pd.PatientId = @PatientId AND pcc.PatientId = pd.PatientId  AND pcc.DiagnosisCodeId = pddc.DiagnosisCodeId AND
	pd.PatientDiagnosisId = pddc.PatientDiagnosisId  AND
	pddc.DiagnosisCodeId = dc.DiagnosisCodeId AND dc.DiagnosisCodingSystemTypeId = dcst.DiagnosisCodingSystemTypeID 
	

GO
