SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ClinicalDecisionPatientVitalsLoad]
@PatientID INT
AS 


--This stored procedure will return all the anonymous vital signs  information for the clinical decisions 
--Evaluation Request
--LBPP - 1463 - Introduced in version 2.01
--CJL - 02.22.2016  -- First Cut


SELECT TOP 1
	PatientVitalsId, PatientID, ServiceDateTime, HeightCM, 
	WeightKG, TemperatureCelcius, Pulse, Respiration, 
	BloodPressureSystolic, BloodPressureDiastolic, OxygenSaturationSP02
FROM 
	PatientVitals pv WITH (NOLOCK)
WHERE
	pv.DeleteInd = 0 AND pv.PatientId = @PatientId
ORDER BY  ServiceDateTime DESC
GO
