SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[internalMakeMemberMonths] 
AS
BEGIN
/*=================================================================================================
	CREATED BY: 	Alex
	CREATED ON:		2016-01-01
	INITIAL VER:	1.6.1
	MODULE:			ETL Financials 		
	DESCRIPTION:   Each patient have a record for each month starting from the first eligible month
                   or first claim month through the last term date or last claim date
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.1.1		2016-10-18		Lan			LBAN-3157	add CCLF2, CCLF6 and CCLF7 into the calculation
  	2.2.1		2017-01-17		SG			LBETL-501	add code to check visitdate is not null. fixed in temp table sections.
====================================================================================================*/

SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'membersMonth';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalMakeMemberMonths';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @COMMENT VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @CurreffectiveDate DATE = '01/01/2012' 

BEGIN TRY

 --   IF NOT EXISTS(SELECT NAME FROM sys.indexes WHERE NAME = 'IX_membersMonth_LbPatientId')

	--BEGIN
	--	CREATE INDEX IX_membersMonth_LbPatientId ON [dbo].membersMonth([LbPatientID],[visitdate])
	--END

	TRUNCATE TABLE membersMonth 

	-- drop table #roster
	
	SELECT LbPatientId 
		   ,CASE 
				WHEN MIN(CLM_FROM_DT) < @CurreffectiveDate THEN @CurreffectiveDate
				ELSE MIN(CLM_FROM_DT)
			END AS CurreffectiveDate
		   ,CASE 
				WHEN MAX(CLM_THRU_DT) > getdate() THEN CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0), 101)
				ELSE MAX(CLM_THRU_DT)
			END AS CurrTermDate
	INTO #roster
	FROM 
	(
		SELECT LbPatientId,MIN(CLM_FROM_DT)  AS CLM_FROM_DT,MAX(CASE WHEN CLM_THRU_DT < CLM_FROM_DT THEN CLM_FROM_DT ELSE CLM_THRU_DT END) AS CLM_THRU_DT
		FROM CCLF_1_PartA_Header
		WHERE LbPatientId IS NOT NULL
		GROUP BY LbPatientId
		UNION ALL
		SELECT LbPatientId,MIN(CLM_FROM_DT) AS CLM_FROM_DT,MAX(CASE WHEN CLM_THRU_DT < CLM_FROM_DT THEN CLM_FROM_DT ELSE CLM_THRU_DT END) AS CLM_THRU_DT
		FROM CCLF_5_PartB_Physicians
		WHERE LbPatientId IS NOT NULL
		GROUP BY LbPatientId
		UNION ALL
		SELECT LbPatientId,MIN(CLM_LINE_FROM_DT) AS CLM_FROM_DT,MAX(CASE WHEN CLM_LINE_THRU_DT < CLM_LINE_FROM_DT THEN CLM_LINE_FROM_DT ELSE CLM_LINE_THRU_DT END) AS CLM_THRU_DT
		FROM CCLF_5_PartB_Physicians
		WHERE LbPatientId IS NOT NULL
		GROUP BY LbPatientId
	--add CCLF6 and CCLF7 into the calculation	
		UNION ALL
		SELECT LbPatientId,MIN(CLM_LINE_FROM_DT) AS CLM_FROM_DT,MAX(CASE WHEN CLM_LINE_THRU_DT < CLM_LINE_FROM_DT THEN CLM_LINE_FROM_DT ELSE CLM_LINE_THRU_DT END) AS CLM_THRU_DT
		FROM CCLF_2_PartA_RCDetail
		WHERE LbPatientId IS NOT NULL
		GROUP BY LbPatientId
		UNION ALL
		SELECT LbPatientId,MIN(CLM_FROM_DT) AS CLM_FROM_DT,MAX(CASE WHEN CLM_THRU_DT < CLM_FROM_DT THEN CLM_FROM_DT ELSE CLM_THRU_DT END) AS CLM_THRU_DT
		FROM CCLF_6_PartB_DME
		WHERE LbPatientId IS NOT NULL
		GROUP BY LbPatientId
		UNION ALL
		SELECT LbPatientId,MIN(CLM_LINE_FROM_DT) AS CLM_FROM_DT,MAX(CASE WHEN CLM_LINE_THRU_DT < CLM_LINE_FROM_DT THEN CLM_LINE_FROM_DT ELSE CLM_LINE_THRU_DT END) AS CLM_THRU_DT
		FROM CCLF_6_PartB_DME
		WHERE LbPatientId IS NOT NULL
		GROUP BY LbPatientId
		UNION ALL
		SELECT LbPatientId,MIN(CLM_LINE_FROM_DT) AS CLM_FROM_DT ,MAX(CLM_LINE_FROM_DT) AS CLM_THRU_DT
		FROM CCLF_7_PartD
		WHERE LbPatientId IS NOT NULL
		GROUP BY LbPatientId
	) X 
	GROUP BY LbPatientId
	                                
    INSERT INTO #roster(LbPatientId,CurreffectiveDate,CurrTermDate)
	SELECT DISTINCT m.LbPatientId
	, CASE 
		WHEN MIN(CAST([StartDate] AS DATE)) < @CurreffectiveDate THEN @CurreffectiveDate
		ELSE MIN(CAST([StartDate] AS DATE))
		END AS CurreffectiveDate
	,CASE 
		WHEN MAX(CAST([TermDate] AS DATE)) > getdate() THEN CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0), 101)
		ELSE MAX(CAST([TermDate] AS DATE))
	END AS CurrTermDate	
	FROM PatientRosterEnrollment M
	JOIN OrgHierarchy O ON M.LbPatientId = O.LbPatientId
	WHERE O.ContractName NOT IN ('Commercial','NoContract','All Other') AND M.LbPatientId IS NOT NULL
	GROUP BY m.LbPatientId

	--  drop table #members
	SELECT  LbPatientId,MIN(CurreffectiveDate) AS CurreffectiveDate,MAX(CurrTermDate) AS CurrTermDate
	INTO #members_final
	FROM #roster  t
	GROUP BY LbPatientId

	--  drop table #members_final

	--select * from #members_final order by LbPatientId
	
	 CREATE CLUSTERED INDEX CIX_##membersfinal_LbPatientID ON #members_final([LbPatientID])

	;WITH getDates AS
	(
		SELECT DATEADD(DAY, -(DAY(CurreffectiveDate) - 1), CurreffectiveDate) AS CurreffectiveDate, CurrTermDate, LbPatientId
		FROM #members_final --where LbpatientId in(11024,11025,11026,11027)
		UNION ALL
		SELECT DATEADD(MONTH, 1, CurreffectiveDate), CurrTermDate, LbPatientId
		FROM getDates
		WHERE CurreffectiveDate < DATEADD(DAY, -(DAY(CurrTermDate) - 1), CurrTermDate) AND CurreffectiveDate <= CAST( GetUTCDate() AS DATE )
	)

	SELECT LbPatientId, CurreffectiveDate
	INTO #Range1
	FROM getDates
	--order by LbPatientId,CurreffectiveDate

	--SELECT * FROM #Range1 order by LbPatientId

	INSERT INTO membersMonth(LbPatientId,visitdate)
	SELECT 
	LbPatientId,
	CurreffectiveDate
	FROM #Range1 R1 
	WHERE CurreffectiveDate IS NOT NULL

	SET @InsertedRecord=@@ROWCOUNT;



		DROP TABLE #roster
		DROP TABLE #members_final
	    DROP TABLE #Range1


	--CODE INSERTED ON 01/12/2015 BY ISMAIL
	INSERT INTO [dbo].[membersMonth] (LbPatientId, visitdate)
	SELECT DISTINCT '123456789', visitdate 
	FROM [dbo].[membersMonth]
	WHERE visitdate IS NOT NULL
	--END MAKE MEMBER MONTHS
	SET @InsertedRecord=@InsertedRecord+@@ROWCOUNT;


	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PatientRosterEnrollment')
	BEGIN
	UPDATE MM SET ContractName = ISNULL(PRE.SourceFeed, 'No Eligibility') FROM membersMonth MM
	LEFT JOIN PatientRosterEnrollment PRE ON MM.LbPatientId = PRE.LbPatientId AND MM.visitdate >= PRE.StartDate
										AND MM.visitdate <= ISNULL(PRE.TermDate, '1/1/2099')
                                   
	END



	     -----Log Information
		--SET @InsertedRecord=@@ROWCOUNT;
	 
          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @COMMENT =LEFT(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
				
	END CATCH


END
GO
