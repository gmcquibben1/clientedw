SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPatientEnrollmentProcessor] 
AS 
BEGIN
-- UPDATE ROWS ALREADY THERE 
UPDATE PE 
SET ACOIdentifier = MRA.ACO, ModifyLBUserId = 1, ModifyDateTime = GETUTCDATE(),
	ActiveInd = CASE WHEN [Deceased Beneficiary Flag2] = '1' OR TRY_CONVERT(DATETIME,MRA.[Deceased Beneficiary Flag2]) > '2000-01-01' THEN 0 -- NON ACTIVE
	ELSE ActiveInd -- ACTIVE 
	END  
FROM dbo.MemberRosterArchives MRA 
INNER JOIN vwPortal_PatientEnrollment PE ON MRA.HICNO = PE.BeneHicNum
WHERE MRA.TimeLineID = 1 AND MRA.StatusDesc <> 'Dropped'

UPDATE PE 
SET ACOIdentifier = MRA.ACO, ModifyLBUserId = 1, ModifyDateTime = GETUTCDATE(),
   DeleteInd = 
	CASE WHEN StatusDesc = 'Dropped' THEN 1 -- Deleted
	  ELSE 0 -- Not deleted 
	END
FROM dbo.MemberRosterArchives MRA 
INNER JOIN vwPortal_PatientEnrollment PE ON MRA.HICNO = PE.BeneHicNum WHERE MRA.TimeLineID = 1 

SELECT DISTINCT MRA.HICNO, P.PatientId, I.IndividualId
INTO #Deceased
FROM dbo.MemberRosterArchives MRA 
INNER JOIN vwPortal_PatientEnrollment PE ON MRA.HICNO = PE.BeneHicNum
INNER JOIN vwPortal_Patient P ON PE.PatientId = P.PatientId 
INNER JOIN vwPortal_Individual I ON P.IndividualId = I.IndividualId 
WHERE ([Deceased Beneficiary Flag2] = '1' OR TRY_CONVERT(DATETIME,MRA.[Deceased Beneficiary Flag2]) > '2000-01-01')

UPDATE I 
SET ModifyLBUserId = 1, ModifyDateTime = GETUTCDATE(), DeleteInd = 1  
FROM vwPortal_Individual I 
INNER JOIN #Deceased D ON D.IndividualId = I.IndividualId 

UPDATE PE 
SET ModifyLBUserId = 1, ModifyDateTime = GETUTCDATE(), DeleteInd = 1,	ActiveInd = 0  
FROM vwPortal_PatientEnrollment PE
INNER JOIN #Deceased D ON D.PatientId = PE.PatientId 

-- INSERT NEW ROWS NOT ALREADY THERE -- DIRECT JOINS
INSERT INTO vwPortal_PatientEnrollment (
			ACOIdentifier,
			BeneHicNum,
			PatientId,
			CreateLbUserId,ModifyLbUserId)
SELECT DISTINCT 
	ACO, 
	HICNO ,
	PR.LbPatientId, 
	1,1
FROM dbo.MemberRosterArchives MRA 
JOIN [PatientIdReference] PR ON PR.ExternalId = MRA.HICNO AND PR.DeleteInd = 0 
LEFT JOIN vwPortal_PatientEnrollment PE ON MRA.HICNO = PE.BeneHicNum
INNER JOIN vwPortal_Patient P ON PR.LbPatientId = P.PatientId
WHERE TimeLineID = 1 AND PE.BeneHicNum IS NULL  AND PR.IdTypeDesc = 'BENE_HIC_NUM' 
AND (MRA.[Deceased Beneficiary Flag2] = '0' OR TRY_CONVERT(DATETIME,MRA.[Deceased Beneficiary Flag2]) < '2000-01-01') AND MRA.StatusDesc <> 'DROPPED'

-- INSERT NEW ROWS NOT ALREADY THERE  -- MATCHED AND LOST 
INSERT INTO vwPortal_PatientEnrollment (
			ACOIdentifier,
			BeneHicNum,
			PatientId,
			CreateLbUserId,ModifyLbUserId)
SELECT DISTINCT 
	ACO, 
	HICNO ,
	PR1.LbPatientId, 
	1,1
FROM dbo.MemberRosterArchives MRA 
INNER JOIN vwMemberIdConflictsXReference CXR ON MRA.HICNO = CXR.OrigHicNo
INNER JOIN [PatientIdReference] PR1 ON PR1.ExternalId = CXR.ResolvedHicNo AND PR1.IdTypeDesc = 'BENE_HIC_NUM' AND PR1.DeleteInd = 0 
INNER JOIN vwPortal_Patient P ON PR1.LbPatientId = P.PatientId
LEFT  JOIN [PatientIdReference] PR2 ON PR2.ExternalId = MRA.HICNO AND PR2.IdTypeDesc = 'BENE_HIC_NUM' AND PR2.DeleteInd = 0 
LEFT  JOIN vwPortal_PatientEnrollment PE ON MRA.HICNO = PE.BeneHicNum
WHERE TimeLineID = 1 AND PE.BeneHicNum IS NULL  
AND (MRA.[Deceased Beneficiary Flag2] = '0' OR TRY_CONVERT(DATETIME,MRA.[Deceased Beneficiary Flag2]) < '2000-01-01') AND MRA.StatusDesc <> 'DROPPED' 
AND PR2.LbPatientId IS NULL 

----<<<<< FOR [FileProcessHistory] >>>>
-- UPDATE IF FILE RECORD IS ALREADY THERE 
UPDATE FPH 
SET ProcessDateTime = GETUTCDATE()  
FROM (SELECT DISTINCT FileInputName FROM dbo.MemberRosterArchives MRA WHERE MRA.TimeLineID = 1 ) MRA
INNER JOIN vwPortal_FileProcessHistory FPH ON MRA.FileInputName = FPH.[FileName]

-- INSERT IF FILE RECORD IS NOT ALREADY THERE 
INSERT INTO vwPortal_FileProcessHistory (FileProcessTypeId,FileName,
           ReceiveDateTime,ProcessDateTime,
           CreateLbUserId, ModifyLbUserId)
SELECT 2, MRA.FileInputName, 
    GETUTCDATE(), GETUTCDATE(),
    1,1
FROM (SELECT DISTINCT FileInputName FROM dbo.MemberRosterArchives MRA WHERE  MRA.TimeLineID = 1 AND FileInputName IS NOT NULL) AS MRA 
LEFT JOIN vwPortal_FileProcessHistory FPH ON MRA.FileInputName = FPH.[FileName]
WHERE  FPH.[FileName] IS NULL

-- BeneHics in PatientEnrollment have since changed bcos of MPI
SELECT  a.BeneHiCNum OutGoingWinner, d.MedicareNO IncomingWinner
INTO #MPILoss -- DROP TABLE #MPILoss 
FROM vwPortal_PatientEnrollment a 
INNER JOIN  MemberExtract b on a.BeneHICNum = b.medicareNo 
INNER JOIN  PatientIdsConflictsXReference c on b.family_id = c.Family_Id and c.Family_Id <> c.Head_Id 
INNER JOIN MemberExtract d on c.head_id = d.family_id 
LEFT JOIN vwPortal_PatientEnrollment e on d.medicareNo = e.BeneHiCNum
WHERE a.BeneHiCNum <> d.MedicareNo AND e.benehicnum IS NULL  AND a.deleteind = 0
AND  EXISTS (SELECT 1 FROm MemberRosterArchives MRA WHERE MRA.HICNO = d.medicareNo)

INSERT INTO [dbo].[vwPortal_PatientEnrollment]
           ([ACOIdentifier],[BeneHICNum],[PatientId]
			,[PermissionLetterMailDate],[PermissionResponseDate],[SubstanceAbuseLetterMailDate]
			,[SubstanceAbuseResponseDate],[DataSharingPreferenceInd],[DataSharingDecisionCode]
			,[SubstanceAbuseSharingPreferenceInd],[SubstanceAbuseSharingDecisionCode],[SSPDataSharingRequestInd]
			,[SSPDataSharingRequestDateTime],[PreferenceResponseCode],[SSPDataSharingResponseCode]
			,[ActiveInd],[DeleteInd],[CreateDateTime]
			,[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
 SELECT		PE.[ACOIdentifier],MPI.IncomingWinner,PE.[PatientId]
			,PE.[PermissionLetterMailDate],PE.[PermissionResponseDate],PE.[SubstanceAbuseLetterMailDate]
			,PE.[SubstanceAbuseResponseDate],PE.[DataSharingPreferenceInd],PE.[DataSharingDecisionCode]
			,PE.[SubstanceAbuseSharingPreferenceInd],PE.[SubstanceAbuseSharingDecisionCode],PE.[SSPDataSharingRequestInd]
			,PE.[SSPDataSharingRequestDateTime],PE.[PreferenceResponseCode],PE.[SSPDataSharingResponseCode]
			,PE.[ActiveInd],PE.[DeleteInd],PE.[CreateDateTime]
			,PE.[ModifyDateTime],1,1
FROM vwPortal_PatientEnrollment PE 
INNER JOIN #MPILoss MPI ON PE.BeneHicNum = MPI.OutGoingWinner
LEFT JOIN vwPortal_PatientEnrollment PE2 ON MPI.IncomingWinner = PE2.BeneHICNum WHERE PE2.BeneHICNum IS NULL 

UPDATE [dbo].[vwPortal_PatientEnrollment]
SET ModifyLBUserId = 1, ModifyDateTime = GETUTCDATE(), DeleteInd = 1 -- Deleted
FROM vwPortal_PatientEnrollment PE 
INNER JOIN #MPILoss MPI ON PE.BeneHicNum = MPI.OutGoingWinner

-- MAPPING OVER OF SETTINGS FROM OLD TO NEW ID
;WITH CTE0 AS (
	SELECT  patientId, COUNT(*) COUNTREC FROM vwPortal_PatientEnrollment GROUP BY PatientId HAVING COUNT(*) > 1 
), -- MULTIPLE BENEHICS TO A PATIENT 
CTE1 AS (
	SELECT  A.[PatientEnrollmentId],A.[ACOIdentifier],A.[BeneHICNum]
			,A.[PatientId],A.[PermissionLetterMailDate],A.[PermissionResponseDate]
			,A.[SubstanceAbuseLetterMailDate],A.[SubstanceAbuseResponseDate],A.[DataSharingPreferenceInd]
			,A.[DataSharingDecisionCode],A.[SubstanceAbuseSharingPreferenceInd],A.[SubstanceAbuseSharingDecisionCode]
			,A.[SSPDataSharingRequestInd],A.[SSPDataSharingRequestDateTime],A.[PreferenceResponseCode]
			,A.[SSPDataSharingResponseCode],A.[ActiveInd],A.[DeleteInd]
			,A.[CreateDateTime],A.[ModifyDateTime],A.[CreateLBUserId]
			,A.[ModifyLBUserId],
			ROW_NUMBER() OVER(PARTITION BY A.PatientId ORDER BY 
				CASE WHEN A.[PermissionLetterMailDate] IS NULL AND A.[PermissionResponseDate] IS NULL 
				AND A.[SubstanceAbuseLetterMailDate] IS NULL AND A.[SubstanceAbuseResponseDate] IS NULL 
				AND A.[SSPDataSharingRequestDateTime] IS NULL AND A.[PreferenceResponseCode] IS NULL AND A.[SSPDataSharingResponseCode] IS NULL -- NEW BLANK ROW IS LOWER
					 THEN '1900-01-01' ELSE A.[ModifyDateTime] END DESC -- FILLED IN NOW BLANK ROW HIGHER 
							 ) SNO
  FROM vwPortal_PatientEnrollment A INNER JOIN CTE0 B ON A.PatientId = B.PatientId) 
SELECT B.PatientEnrollmentId, A.[PermissionLetterMailDate],A.[PermissionResponseDate]
			,A.[SubstanceAbuseLetterMailDate],A.[SubstanceAbuseResponseDate],A.[DataSharingPreferenceInd]
			,A.[DataSharingDecisionCode],A.[SubstanceAbuseSharingPreferenceInd],A.[SubstanceAbuseSharingDecisionCode]
			,A.[SSPDataSharingRequestInd],A.[SSPDataSharingRequestDateTime],A.[PreferenceResponseCode]
			,A.[SSPDataSharingResponseCode]
INTO #UpdateSet 
FROM CTE1 A INNER JOIN CTE1 B ON A.PatientId = B.PatientId WHERE A.SNO = 1 AND B.SNO > 1 

UPDATE PE
SET [PermissionLetterMailDate] = US.[PermissionLetterMailDate],
	[PermissionResponseDate] =US.[PermissionResponseDate],
	[SubstanceAbuseLetterMailDate]=US.[SubstanceAbuseLetterMailDate],
	[SubstanceAbuseResponseDate]=US.[SubstanceAbuseResponseDate],
	[DataSharingPreferenceInd]=US.[DataSharingPreferenceInd],
	[DataSharingDecisionCode]=US.[DataSharingDecisionCode],
	[SubstanceAbuseSharingPreferenceInd]=US.[SubstanceAbuseSharingPreferenceInd],
	[SubstanceAbuseSharingDecisionCode]=US.[SubstanceAbuseSharingDecisionCode],
	[SSPDataSharingRequestInd]=US.[SSPDataSharingRequestInd],
	[SSPDataSharingRequestDateTime]=US.[SSPDataSharingRequestDateTime],
	[PreferenceResponseCode]=US.[PreferenceResponseCode],
	[SSPDataSharingResponseCode]=US.[SSPDataSharingResponseCode]
FROM vwPortal_PatientEnrollment PE 
INNER JOIN #UpdateSet US ON PE.PatientEnrollmentId = US.PatientEnrollmentId 


END
--SELECT * FROM vwPortal_FileProcessHistory 
--DELETE FROM vwPortal_vwFileProcessHistory WHERE fileName is NULL 
GO
