SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =====================================================================================
-- Author:		Mike Hoxter
-- Create date: 2015-12-18
-- Description:	assign patient provider based on the attibution type priority
-- Modified By: Youping
-- Modified Date: 2016-04-21  add PatientID IS NOT NULL
-- Modified Date: 2016-06-29  
-- Modified By: Lan
-- Description:	add logic to set the patientprovider with overrideInd = 1 will be the winner
-- Modified Date: 2016-07-18  
-- Modified By: Lan
-- Description:	Set the contractType to be deleted if the contract is not in PatientContract,
-- and remove the records associated with this contract from ContractAttributionPriority
-- Modified Date: 2016-07-21  
-- Modified By: Lan
-- Description:	handle the NULL value in overRideInd 
-- Modified Date: 2016-08-05  
-- Modified By: Lan
-- Description:	remove the part that delete the records from ContractAttributionPriority 
-- =========================================================================================
CREATE PROCEDURE [dbo].[internalAttributionPriorityCalc]
AS
BEGIN

	--DELETE ALL GREATER THAN 13 DAYS, NOT ON A FRIDAY
	DELETE FROM vwPortal_PatientProvider 
	WHERE DATEDIFF("day", CreateDateTime , getdate()) > 13 
	AND DatePart("WeekDay", CreateDateTime) <> 6
	and DeleteInd = 1 and (OverrideInd = 0 OR OverrideInd IS NULL)

	INSERT INTO vwPortal_ContractType (
	   [Name]
	  ,DisplayValue
	  ,DeleteInd
	  ,CreateLBUserId
	  ,CreateDateTime
	  ,ModifyLBUserId
	  ,ModifyDateTime
	) SELECT distinct pc.ContractName, pc.ContractName, 0, 1, GETDATE(), 1, GETDATE()
	FROM PatientContract pc
	left join vwPortal_ContractType ct on pc.ContractName = ct.[Name]
	where IsCurrentInd = 1 AND ct.ContractTypeID IS NULL AND pc.ContractName IS NOT NULL

	UPDATE vwPortal_ContractType
	SET DeleteInd = 1
	WHERE Name NOT IN (SELECT DISTINCT ContractName FROM PatientContract WHERE IsCurrentInd = 1)
	
	--DELETE FROM  vwPortal_ContractAttributionPriority 
	--WHERE ContractTypeID in (SELECT DISTINCT ContractTypeID FROM vwPortal_ContractType WHERE DeleteInd = 1)

	--INSERT ANY MISSING UNATTRIBUTED TYPES
	INSERT INTO vwPortal_ContractAttributionPriority (
	  ContractTypeID
	  ,AttributiontypeID
	  ,SourceSystemId
	  ,ContractPriorityOrder
	  ,CreateLBUserId
	  ,CreateDateTime
	  ,ModifyLBUserId
	  ,ModifyDateTime
	) 
	SELECT CT.ContractTypeID, 99,1,99,1,GETDATE(),1, GETDATE()   
	FROM vwPortal_ContractType CT
	LEFT JOIN vwPortal_ContractAttributionPriority CAP 
	ON CT.ContractTypeID = CAP.ContractTypeID
	AND CAP.AttributiontypeID = 99 AND CAP.ContractPriorityOrder = 99
	WHERE CAP.ContractTypeID IS NULL AND CT.DeleteInd = 0

	--Run All Attribution Types that are enabled
	declare @dynSQL nvarchar(max)
	SELECT @dynSQL = COALESCE(@dynSQL +'
	', '') + 'EXEC '+StoredProcedureName+'
	' FROM vwPortal_AttributionType WHERE EnabledInd = 1 
	--run all enabled attribution types
	select @dynSQL
	EXECUTE sys.sp_executesql @dynSQL

--move this update part down after all the attribution SPs finished, so for each attribution type only the latest loaded rows have deleteInd = 0
--UPDATE vwPortal_PatientProvider SET DeleteInd = 1 where DeleteInd = 0 
--and CAST(CreateDateTime AS DATE)  <> (SELECT MAX(CAST(CreateDateTime AS DATE)) from vwPortal_PatientProvider)

	SELECT AttributiontypeID, MAX(CreateDateTime) AS lastCraetedDateTime
	INTO #attrLastCreateTime
	FROM vwPortal_PatientProvider
	GROUP BY AttributiontypeID

	UPDATE vwPortal_PatientProvider 
	SET DeleteInd = 1 
	FROM vwPortal_PatientProvider
	JOIN #attrLastCreateTime
	ON vwPortal_PatientProvider.AttributionTypeId = #attrLastCreateTime.AttributionTypeId
	AND vwPortal_PatientProvider.DeleteInd = 0  
	AND (OverrideInd = 0 OR OverrideInd IS NULL)
	AND vwPortal_PatientProvider.CreateDateTime <> #attrLastCreateTime.lastCraetedDateTime
    

	--GET PATIENT CONTRACT/Attribution Info
	SELECT  
	PPP.PatientProviderID, 
	PPP.PatientID, 
	PPP.ProviderID, 
	CT.ContractTypeID,  
	100 AS ContractPriorityOrder, 
	PC.ContractName, 
	PPP.AttributionTypeId, 
	PPP.SourceSystemID
	into #PatientAttributionList
	FROM vwPortal_PatientProvider PPP
	JOIN PatientContract PC 
	ON PPP.PatientID = PC.LbPatientId
	AND PC.[IsCurrentInd] = 1 AND PPP.DeleteInd = 0 --added filter the IsCurrentInd = 0 by Lan
	AND (OverrideInd = 0 OR OverrideInd IS NULL)--added filter the OverrideInd = 0 or NULL by Lan to exclude these patients from provider priority calculation
	JOIN vwPortal_ContractType CT 
	ON PC.ContractName = CT.[Name] 
	 AND CT.DeleteInd = 0 -- only have the valid contract attribution into the priority calculation
	JOIN vwPortal_AttributionType AT ON PPP.AttributionTypeId = AT.AttributionTypeId
	--WHERE PPP.DeleteInd = 0 --and PPP.PatientID = 64300
	WHERE  PPP.PatientID IS NOT NULL

	UPDATE PAL 
	   SET ContractPriorityOrder = CAP.ContractPriorityOrder  
    FROM #PatientAttributionList PAL
	JOIN vwPortal_ContractAttributionPriority CAP 
	ON PAL.AttributionTypeId = CAP.AttributiontypeID 
	AND PAL.SourceSystemID = CAP.SourceSystemId 
	AND PAL.ContractTypeID = CAP.ContractTypeID

--select * FROM vwPortal_PatientProvider where PatientID = 64300 AND DeleteInd = 0 AND AttributedProviderInd = 1
	--Update all to false (no winner)
	UPDATE vwPortal_PatientProvider 
	       SET AttributedProviderInd = 0
	WHERE DeleteInd = 0

	SELECT *
	INTO #attributedList
	FROM
	(
	SELECT 
	PatientProviderID
	,PatientId 
	,ProviderId
	,AttributionTypeId
	,ContractPriorityOrder
	,ROW_NUMBER() OVER(PARTITION BY PatientID ORDER BY ContractPriorityOrder,PatientProviderID ) AS rowNbr
	FROM #PatientAttributionList
	--GROUP BY PatientId
	) t WHERE rowNbr = 1
  
  --Add logic by where if a paient has an overrideInd value = 1, 
  --all the others are set to 0 and the override is inserted & set to 1

  --these are the patients who has override provider set up
     SELECT 	 
	 PatientId 
    ,MAX(PatientProviderID) AS PatientProviderID
	INTO #overRideList
	FROM  [dbo].[vwPortal_PatientProvider]
    WHERE [OverrideInd] = 1
	GROUP BY PatientId

  --remove the patients from the calculated attibution list if they have Override provider set up 
	DELETE FROM #attributedList
	WHERE PatientID in(
	SELECT 	 
	PatientId 
	FROM  #overRideList)

  --set the attibutedProviderInd = 1 for the patients from priority calculation
	UPDATE vwPortal_PatientProvider 
	SET AttributedProviderInd = 1
	FROM #attributedList t1
	JOIN vwPortal_PatientProvider t2
	ON t1.PatientProviderID = t2.PatientProviderID

   --set the attibutedProviderInd = 1 for the patients have override providers set up
	UPDATE vwPortal_PatientProvider 
	SET AttributedProviderInd = 1
	WHERE PatientProviderID in(
	SELECT 	 
	PatientProviderID 
	FROM  #overRideList)


END

GO
