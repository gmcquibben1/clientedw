SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientSocialHistoryLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 

AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		psh.PatientId, 
		psh.SourceSystemId,
		psh.SmokingStatus,
		psh.MaritalStatus,
		psh.EmploymentStatus,
		psh.TransportationInd,
		psh.LivingArrangementCode,
		psh.AlcoholUseInd,
		psh.DrugUseInd,
		psh.CaffeineUseInd,
		psh.SexuallyActiveInd,
		psh.ExerciseHoursPerWeek
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientSocialHistory psh ON psh.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
		AND (psh.CreateDateTime > @dtmStartDate )--OR psh.ModifyDateTime > @dtmStartDate)

END
GO
