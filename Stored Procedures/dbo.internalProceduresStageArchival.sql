SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalProceduresStageArchival] 
    @LookbackDays INT = 0  ,
	@LookbackSince DateTime = NULL,
	@ClearStage BIT = 0
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

    SELECT @LookbackSince = CASE WHEN @LookbackSince IS NULL THEN GETDATE() ELSE @LookbackSince END 

	MERGE InterfacePatientProcedureArchive AS TARGET
	USING (
			SELECT 
			  [InterfacePatientProcedureId]
			  ,[InterfacePatientID]
			  ,[InterfaceSystemId]
			  ,[ProcedureFunctionTypeID]
			  ,[ProcedureCodeModifier1]
			  ,[ProcedureCodeModifier2]
			  ,[ProcedureCodeModifier3]
			  ,[ProcedureCodeModifier4]
			  ,[ProcedureDateTime]
			  ,[ProcedureComment]
			  ,[PerformedByClinician]
			  ,[OrderedByClinician]
			  ,[CreateDateTime]
		    FROM [dbo].vwPortal_InterfacePatientProcedure (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			([InterfacePatientProcedureId],[InterfacePatientID],[InterfaceSystemId],[ProcedureFunctionTypeID],[ProcedureCodeModifier1],[ProcedureCodeModifier2]
			  ,[ProcedureCodeModifier3],[ProcedureCodeModifier4],[ProcedureDateTime],[ProcedureComment],[PerformedByClinician],[OrderedByClinician],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientProcedureId] = SOURCE.[InterfacePatientProcedureId] 
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientProcedureId]			=Source.[InterfacePatientProcedureId],
			[InterfacePatientID]					=Source.[InterfacePatientID],
			[InterfaceSystemId]						=Source.[InterfaceSystemId],
			[ProcedureFunctionTypeID]				=Source.[ProcedureFunctionTypeID],
			[ProcedureCodeModifier1]				=Source.[ProcedureCodeModifier1],
			[ProcedureCodeModifier2]				=Source.[ProcedureCodeModifier2],
			[ProcedureCodeModifier3]				=Source.[ProcedureCodeModifier3],
			[ProcedureCodeModifier4]				=Source.[ProcedureCodeModifier4],
			[ProcedureDateTime]						=Source.[ProcedureDateTime],
			[ProcedureComment]						=Source.[ProcedureComment],
			[PerformedByClinician]					=Source.[PerformedByClinician],
			[OrderedByClinician]					=Source.[OrderedByClinician],
			[CreateDateTime]						=Source.[CreateDateTime]
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientProcedureId],[InterfacePatientID],[InterfaceSystemId],[ProcedureFunctionTypeID],[ProcedureCodeModifier1],[ProcedureCodeModifier2]
			  ,[ProcedureCodeModifier3],[ProcedureCodeModifier4],[ProcedureDateTime],[ProcedureComment],[PerformedByClinician],[OrderedByClinician],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientProcedureId],Source.[InterfacePatientID],Source.[InterfaceSystemId],Source.[ProcedureFunctionTypeID],Source.[ProcedureCodeModifier1],Source.[ProcedureCodeModifier2]
			  ,Source.[ProcedureCodeModifier3],Source.[ProcedureCodeModifier4],Source.[ProcedureDateTime],Source.[ProcedureComment],Source.[PerformedByClinician],Source.[OrderedByClinician],Source.[CreateDateTime]) ;

	MERGE InterfacePatientProcedureProcedureCodeArchive AS TARGET
	USING (
			SELECT 
			  [InterfacePatientProcedureProcedureCodeId]
			  ,[InterfacePatientProcedureId]
			  ,[ProcedureCode]
			  ,[ProcedureCodingSystemName]
			  ,[ProcedureDescription]
			  ,[CreateDateTime]
		    FROM [dbo].vwPortal_InterfacePatientProcedureProcedureCode (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			([InterfacePatientProcedureProcedureCodeId],[InterfacePatientProcedureId],[ProcedureCode],[ProcedureCodingSystemName],[ProcedureDescription],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientProcedureProcedureCodeId] = SOURCE.[InterfacePatientProcedureProcedureCodeId] 
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientProcedureProcedureCodeId]	=Source.[InterfacePatientProcedureProcedureCodeId],
			[InterfacePatientProcedureId]				=Source.[InterfacePatientProcedureId],
			[ProcedureCode]								=Source.[ProcedureCode],
			[ProcedureCodingSystemName]					=Source.[ProcedureCodingSystemName],
			[ProcedureDescription]						=Source.[ProcedureDescription],
			[CreateDateTime]							=Source.[CreateDateTime]
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientProcedureProcedureCodeId],[InterfacePatientProcedureId],[ProcedureCode],[ProcedureCodingSystemName],[ProcedureDescription],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientProcedureProcedureCodeId],Source.[InterfacePatientProcedureId],Source.[ProcedureCode],Source.[ProcedureCodingSystemName],Source.[ProcedureDescription],Source.[CreateDateTime]) ;

	IF @ClearStage = 1
	BEGIN
		DELETE vwPortal_InterfacePatientProcedure FROM vwPortal_InterfacePatientProcedure  A
		INNER JOIN InterfacePatientProcedureArchive B ON A.[InterfacePatientProcedureId] = B.[InterfacePatientProcedureId]
		WHERE A.[CreateDateTime] >= GetDate() - @LookbackDays OR A.[CreateDateTime] > @LookbackSince

		DELETE vwPortal_InterfacePatientProcedureProcedureCode FROM vwPortal_InterfacePatientProcedureProcedureCode  A
		INNER JOIN InterfacePatientProcedureProcedureCodeArchive B ON A.[InterfacePatientProcedureProcedureCodeId] = B.[InterfacePatientProcedureProcedureCodeId]
		WHERE  A.[CreateDateTime] >= GetDate() - @LookbackDays OR A.[CreateDateTime] > @LookbackSince
	END
		


GO
