SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientCareOpportunityLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		cg.PatientId,
		cg.TaskId,
		cg.TaskStatus,
		cg.CareGapId,
		cg.CareGapType,
		cg.CareGapRuleType,
		cg.CareGapActionType,
		cg.CreateDateTime
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientCareGap cg ON cg.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (cg.CreateDateTime > @dtmStartDate OR cg.ModifyDateTime > @dtmStartDate)
		AND cg.TaskStatus <> 'Complete'
 
END
GO
