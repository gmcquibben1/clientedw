SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ETLOrgHierarchyHistoryBuild]

AS
BEGIN	

  /*===============================================================
	CREATED BY: 	Lan Ma
	CREATED ON:		2016-08-19
	INITIAL VER:	2.1.1
	MODULE:			ETL
	Description:	this SP load the orghierarchy data into its history table, remove the history data 
				that are 2 months old, but only keep the firt record that was created in that month
	PARAMETERS:		

	
	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.0.1		2016-08-19		Lan Ma					Initial
	2.1.2		2016-12-31		CJL			LNETL-908	Fixed an issue where null patient ids are causing the history insert to break, found on Esse
					

														 


=================================================================*/




	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'OrgHierarchyHistory';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLOrgHierarchyHistoryBuild';
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

    BEGIN TRY
		--delete the records that have been loaded into history at the same day
		DELETE FROM [OrgHierarchyHistory]          
		WHERE [CreateDate]= CAST(GETDATE() AS DATE)

		-- load orghierarchy data into history table
		INSERT INTO [dbo].[OrgHierarchyHistory]
				   ([State]
				   ,[City]
				   ,[Zip]
				   ,[GeoCodeLong]
				   ,[GeoCodeLat]
				   ,[Level1Id]
				   ,[Level1Name]
				   ,[Level1TypeName]
				   ,[Level2Id]
				   ,[Level2Name]
				   ,[Level2TypeName]
				   ,[Level3Id]
				   ,[Level3Name]
				   ,[Level3TypeName]
				   ,[Level4Id]
				   ,[Level4Name]
				   ,[Level4TypeName]
				   ,[Level5Id]
				   ,[Level5Name]
				   ,[Level5TypeName]
				   ,[Level6Id]
				   ,[Level6Name]
				   ,[Level6TypeName]
				   ,[ContractName]
				   ,[MemberID]
				   ,[MemberFullName]
				   ,[Member_Provider_Relationship_Effective_Date]
				   ,[Member_Provider_Relationship_Termination_Date]
				   ,[BENE_HIC_NUM]
				   ,[Patient_Status]
				   ,[ProviderId]
				   ,[ProviderName]
				   ,[ProviderNPI]
				   ,[BirthDate]
				   ,[GenderId]
				   ,[County]
				   ,[LastUpdatedTimestamp]
				   ,[LbPatientId]
				   ,[PatientId1TypeName]
				   ,[PatientId1]
				   ,[PatientId2TypeName]
				   ,[PatientId2]
				   ,[PatientId3TypeName]
				   ,[PatientId3]
				   ,[PatientId4TypeName]
				   ,[PatientId4]
				   ,[CreateDate]
				   ,[Modifydate])
		SELECT   DISTINCT   
					[State]
				   ,[City]
				   ,[Zip]
				   ,[GeoCodeLong]
				   ,[GeoCodeLat]
				   ,[Level1Id]
				   ,[Level1Name]
				   ,[Level1TypeName]
				   ,[Level2Id]
				   ,[Level2Name]
				   ,[Level2TypeName]
				   ,[Level3Id]
				   ,[Level3Name]
				   ,[Level3TypeName]
				   ,[Level4Id]
				   ,[Level4Name]
				   ,[Level4TypeName]
				   ,[Level5Id]
				   ,[Level5Name]
				   ,[Level5TypeName]
				   ,[Level6Id]
				   ,[Level6Name]
				   ,[Level6TypeName]
				   ,[ContractName]
				   ,[MemberID]
				   ,[MemberFullName]
				   ,[Member_Provider_Relationship_Effective_Date]
				   ,[Member_Provider_Relationship_Termination_Date]
				   ,[BENE_HIC_NUM]
				   ,[Patient_Status]
				   ,[ProviderId]
				   ,[ProviderName]
				   ,[ProviderNPI]
				   ,[BirthDate]
				   ,[GenderId]
				   ,[County]
				   ,[LastUpdatedTimestamp]
				   ,[LbPatientId]
				   ,[PatientId1TypeName]
				   ,[PatientId1]
				   ,[PatientId2TypeName]
				   ,[PatientId2]
				   ,[PatientId3TypeName]
				   ,[PatientId3]
				   ,[PatientId4TypeName]
				   ,[PatientId4]
				   ,CAST(GETDATE() AS DATE)
				   ,CAST(GETDATE() AS DATE)
		FROM [dbo].[OrgHierarchy] WHERE [LbPatientId] IS NOT NULL

		SET @InsertedRecord =@@ROWCOUNT
		
		--find the first record that was created in the 2nd previouse month for a patient, this reocrd's createdate will be set
		--as the first day of that month, and will be kept for that month in the history table
		SELECT
	     [State]
		,[City]
		,[Zip]
		,[GeoCodeLong]
		,[GeoCodeLat]
		,[Level1Id]
		,[Level1Name]
		,[Level1TypeName]
		,[Level2Id]
		,[Level2Name]
		,[Level2TypeName]
		,[Level3Id]
		,[Level3Name]
		,[Level3TypeName]
		,[Level4Id]
		,[Level4Name]
		,[Level4TypeName]
		,[Level5Id]
		,[Level5Name]
		,[Level5TypeName]
		,[Level6Id]
		,[Level6Name]
		,[Level6TypeName]
		,[ContractName]
		,[MemberID]
		,[MemberFullName]
		,[Member_Provider_Relationship_Effective_Date]
		,[Member_Provider_Relationship_Termination_Date]
		,[BENE_HIC_NUM]
		,[Patient_Status]
		,[ProviderId]
		,[ProviderName]
		,[ProviderNPI]
		,[BirthDate]
		,[GenderId]
		,[County]
		,[LastUpdatedTimestamp]
		,[LbPatientId]
		,[PatientId1TypeName]
		,[PatientId1]
		,[PatientId2TypeName]
		,[PatientId2]
		,[PatientId3TypeName]
		,[PatientId3]
		,[PatientId4TypeName]
		,[PatientId4]
		,DATEADD(month, DATEDIFF(month, 0, [CreateDate]), 0) AS CreateDate --the first day of the month in which the record was created
		,ModifyDate
		INTO #orgHistory
		FROM
		(           
		SELECT 
		oh.*
		,ROW_NUMBER() OVER(PARTITION BY LbPatientId ORDER BY CreateDate ASC) AS rowNbr
		FROM [dbo].[OrgHierarchyHistory] oh
		WHERE CreateDate >= dateadd(month,-2,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) -- get first day of previous 2nd month
		AND CreateDate < dateadd(month,-1,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) --get first day of previous month
		) t WHERE rowNbr = 1

		 --Delete all of 2nd prevouse month's data
		DELETE
		FROM [dbo].[OrgHierarchyHistory]
		WHERE CreateDate >= dateadd(month,-2,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) -- get first day of previous 2nd month
		AND CreateDate < dateadd(month,-1,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) --get first day of previous month



		INSERT INTO [OrgHierarchyHistory]
		([State]
				   ,[City]
				   ,[Zip]
				   ,[GeoCodeLong]
				   ,[GeoCodeLat]
				   ,[Level1Id]
				   ,[Level1Name]
				   ,[Level1TypeName]
				   ,[Level2Id]
				   ,[Level2Name]
				   ,[Level2TypeName]
				   ,[Level3Id]
				   ,[Level3Name]
				   ,[Level3TypeName]
				   ,[Level4Id]
				   ,[Level4Name]
				   ,[Level4TypeName]
				   ,[Level5Id]
				   ,[Level5Name]
				   ,[Level5TypeName]
				   ,[Level6Id]
				   ,[Level6Name]
				   ,[Level6TypeName]
				   ,[ContractName]
				   ,[MemberID]
				   ,[MemberFullName]
				   ,[Member_Provider_Relationship_Effective_Date]
				   ,[Member_Provider_Relationship_Termination_Date]
				   ,[BENE_HIC_NUM]
				   ,[Patient_Status]
				   ,[ProviderId]
				   ,[ProviderName]
				   ,[ProviderNPI]
				   ,[BirthDate]
				   ,[GenderId]
				   ,[County]
				   ,[LastUpdatedTimestamp]
				   ,[LbPatientId]
				   ,[PatientId1TypeName]
				   ,[PatientId1]
				   ,[PatientId2TypeName]
				   ,[PatientId2]
				   ,[PatientId3TypeName]
				   ,[PatientId3]
				   ,[PatientId4TypeName]
				   ,[PatientId4]
				   ,[CreateDate]
				   ,[Modifydate]
		 )
		SELECT [State]
				   ,[City]
				   ,[Zip]
				   ,[GeoCodeLong]
				   ,[GeoCodeLat]
				   ,[Level1Id]
				   ,[Level1Name]
				   ,[Level1TypeName]
				   ,[Level2Id]
				   ,[Level2Name]
				   ,[Level2TypeName]
				   ,[Level3Id]
				   ,[Level3Name]
				   ,[Level3TypeName]
				   ,[Level4Id]
				   ,[Level4Name]
				   ,[Level4TypeName]
				   ,[Level5Id]
				   ,[Level5Name]
				   ,[Level5TypeName]
				   ,[Level6Id]
				   ,[Level6Name]
				   ,[Level6TypeName]
				   ,[ContractName]
				   ,[MemberID]
				   ,[MemberFullName]
				   ,[Member_Provider_Relationship_Effective_Date]
				   ,[Member_Provider_Relationship_Termination_Date]
				   ,[BENE_HIC_NUM]
				   ,[Patient_Status]
				   ,[ProviderId]
				   ,[ProviderName]
				   ,[ProviderNPI]
				   ,[BirthDate]
				   ,[GenderId]
				   ,[County]
				   ,[LastUpdatedTimestamp]
				   ,[LbPatientId]
				   ,[PatientId1TypeName]
				   ,[PatientId1]
				   ,[PatientId2TypeName]
				   ,[PatientId2]
				   ,[PatientId3TypeName]
				   ,[PatientId3]
				   ,[PatientId4TypeName]
				   ,[PatientId4]
				   ,[CreateDate]
				   ,[Modifydate] 
		FROM #orgHistory

		DROP TABLE #orgHistory
		
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	END TRY

	BEGIN CATCH
	    --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		SET  @ErrorNumber =ERROR_NUMBER();
		SET  @ErrorState =ERROR_STATE();
		SET  @ErrorSeverity=ERROR_SEVERITY();
		SET  @ErrorLine=ERROR_LINE();
		SET  @Comment =left(ERROR_MESSAGE(),400);
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
