SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwLabdata] 
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES FOR NULL
UPDATE PatientLabOrderProcessQueue SET OrderNumber = '' WHERE OrderNumber IS NULL
UPDATE PatientLabOrderProcessQueue SET ServiceId = '' WHERE ServiceId IS NULL
UPDATE PatientLabOrderProcessQueue SET ServiceDescription = 0 WHERE ServiceDescription IS NULL
UPDATE PatientLabOrderProcessQueue SET OrderingProviderName = 0 WHERE OrderingProviderName IS NULL

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId, LbPatientID, OrderNumber --, ServiceId , ServiceDescription,  OrderingProviderName
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				-- LEN(ISNULL(ServiceDescription,''))  -- THE BIGGER THE ServiceDescription THE BETTER IT IS 
				InterfacePatientLabOrderId
				DESC 
			) SNO
	FROM PatientLabOrderProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientLabOrderProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientLabOrderId INT, PatientID INT, SourceSystemId INT,
						   ServiceId VARCHAR(150), ServiceDescription VARCHAR(250), OrderingProviderName VARCHAR(150),
						   OrderNumber VARCHAR(100) )

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientLabOrderId,PatientID,SourceSystemId,
						  ServiceId,ServiceDescription,OrderingProviderName,
						  OrderNumber)
SELECT
						MergeAction,OrigPatientID,PatientLabOrderId,PatientID,SourceSystemId,
						[ServiceId],[ServiceDescription],OrderingProviderName,
						OrderNumber 
--INTO #ResultsTemp
FROM
(
MERGE [DBO].[PatientLabOrder] AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,[OrderNumber],[ServiceId],[ServiceDescription],
		[ServiceCodingSystemName] ,[OrderingProviderId] ,[OrderingProviderIdType],[OrderingProviderName],
		GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId,
		[EncounterIdentifier],[OrderDate]
		FROM [dbo].PatientLabOrderProcessQueue PLOQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PLOQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  ([OrigPatientId],[PatientId],[SourceSystemId],[OrderNumber],[ServiceId],[ServiceDescription]
      ,[ServiceCodingSystemName] ,[OrderingProviderId] ,[OrderingProviderIdType],[OrderingProviderName],[CreateDateTime]
      ,[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId]
	  ,[EncounterIdentifier],[OrderDate])
ON (target.SourceSystemID = source.SourceSystemId AND target.PatientID = source.PatientID AND target.OrderNumber = source.OrderNumber
	--AND target.[ServiceDescription] = source.[ServiceDescription]   AND target.[ServiceId] = source.[ServiceId] 
	--AND target.OrderingProviderName = source.OrderingProviderName  
	)
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
	--[PatientId] = source.[PatientId], 
	[SourceSystemId] = source.[SourceSystemId], 
	[OrderNumber] = source.[OrderNumber], 
	[ServiceId] = source.[ServiceId], 
	[ServiceDescription] = source.[ServiceDescription], 
	[ServiceCodingSystemName] = source.[ServiceCodingSystemName], 
	[OrderingProviderId] = source.[OrderingProviderId], 
	[OrderingProviderIdType] = source.[OrderingProviderIdType], 
	[OrderingProviderName] = source.[OrderingProviderName], 
	--[CreateDateTime] = source.[CreateDateTime], 
	[ModifyDateTime] = source.[ModifyDateTime], 
	--[CreateLBUserId] = source.[CreateLBUserId], 
	[ModifyLBUserId] = source.[ModifyLBUserId], 
	[EncounterIdentifier] = source.[EncounterIdentifier], 
	[OrderDate] = source.[OrderDate] 
WHEN NOT MATCHED THEN
INSERT ([PatientId],[SourceSystemId],[OrderNumber],[ServiceId],[ServiceDescription]
      ,[ServiceCodingSystemName] ,[OrderingProviderId] ,[OrderingProviderIdType],[OrderingProviderName],[CreateDateTime]
      ,[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId]
	  ,[EncounterIdentifier],[OrderDate])
VALUES (source.[PatientId],source.[SourceSystemId],source.[OrderNumber],source.[ServiceId],source.[ServiceDescription]
      ,source.[ServiceCodingSystemName] ,source.[OrderingProviderId] ,source.[OrderingProviderIdType],source.[OrderingProviderName],source.[CreateDateTime]
      ,source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId]
	  ,source.[EncounterIdentifier],source.[OrderDate])
OUTPUT $ACTION MergeAction,  source.OrigPatientId,ISNULL(DELETED.PatientLabOrderId,INSERTED.PatientLabOrderId) PatientLabOrderId, source.[PatientId],source.serviceid, 
							 source.servicedescription, source.sourceSystemId, source.OrderingProviderName, 
							 source.OrderNumber
) x;

CREATE INDEX IX1 ON #ResultsTemp (PatientID,SourceSystemId,ServiceId,ServiceDescription,OrderingProviderName,OrderNumber)

UPDATE PLOQ
SET [PatientLabOrderId] = RT.[PatientLabOrderId]
FROM PatientLabOrderProcessQueue PLOQ
INNER JOIN #ResultsTemp RT  ON PLOQ.InterfaceSystemId = RT.sourceSystemId  AND PLOQ.[LbPatientId] = RT.[OrigPatientId] AND PLOQ.OrderNumber = RT.OrderNumber 
							--AND PLOQ.serviceid = RT.serviceid AND PLOQ.servicedescription = RT.servicedescription AND PLOQ.OrderingProviderName = RT.OrderingProviderName 
	AND (RT.MergeAction = 'INSERT' OR RT.OrigPatientId = RT.PatientId)

IF OBJECT_ID('PatientLabOrder_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientLabOrder_PICT(PatientLabOrderId,PatientId,SwitchDirectionId)
	SELECT RT.PatientLabOrderId, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END
-- SCRUB CHILD OUTPUT ROW TO PROCESS
UPDATE PatientLabOrderProcessQueue SET ScrubbedRowNum = Null
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF CHILD ROW
				[PatientLabOrderId], [ObservationCode1], [ObservationDate]
				ORDER BY	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				InterfacePatientLabOrderId
				DESC 
			) SNO
	FROM PatientLabOrderProcessQueue WHERE InterfacePatientLabResultId IS NOT NULL  AND [PatientLabOrderId] IS NOT NULL
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;
UPDATE  PatientLabOrderProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

MERGE [DBO].PatientLabResult AS target
USING (	
		SELECT DISTINCT [PatientLabOrderId],[ObservationCode1],[ObservationDescription1],[ObservationCodingSystemName1]
      ,[ObservationCode2]  ,[ObservationDescription2],[ObservationCodingSystemName2],[Value]
      ,[Units] ,[ReferenceRange],[AbnormalFlag],[ResultStatus]
      ,[ResultComment],[ObservationDate],GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId
		FROM [dbo].PatientLabOrderProcessQueue PLOQ
		WHERE InterfacePatientLabResultId IS NOT NULL AND ScrubbedRowNum = 1  AND [PatientLabOrderId] IS NOT NULL --AND PatientLabOrderId <> 635287 
	  ) AS source 
	  ([PatientLabOrderId],[ObservationCode1],[ObservationDescription1],[ObservationCodingSystemName1]
      ,[ObservationCode2]  ,[ObservationDescription2],[ObservationCodingSystemName2],[Value]
      ,[Units] ,[ReferenceRange],[AbnormalFlag],[ResultStatus]
      ,[ResultComment],[ObservationDate],[CreateDateTime],[ModifyDateTime]
      ,[CreateLBUserId],[ModifyLBUserId])
ON (target.[PatientLabOrderId] = source.[PatientLabOrderId] AND target.ObservationCode1 = source.ObservationCode1 AND ISNULL(target.ObservationDate,'2099-12-31') = ISNULL(Source.ObservationDate,'2099-12-31')
    --AND target.ObservationCodingSystemName1 = source.ObservationCodingSystemName1 AND target.[ObservationDescription1] = source.[ObservationDescription1] 
	--AND target.Value = source.Value AND ISNULL(target.ObservationCode2,'') = ISNULL(source.ObservationCode2,'') 
	)
WHEN MATCHED THEN 
    UPDATE SET 
	[PatientLabOrderId] = source.[PatientLabOrderId], 
	[ObservationCode1] = source.[ObservationCode1], 
	[ObservationDescription1] = source.[ObservationDescription1], 
	[ObservationCodingSystemName1] = source.[ObservationCodingSystemName1], 
	[ObservationCode2] = source.[ObservationCode2], 
	[ObservationDescription2] = source.[ObservationDescription2], 
	[ObservationCodingSystemName2] = source.[ObservationCodingSystemName2], 
	[Value] = source.[Value], 
	[Units] = source.[Units], 
	[ReferenceRange] = source.[ReferenceRange], 
	[AbnormalFlag] = source.[AbnormalFlag], 
	[ResultStatus] = source.[ResultStatus], 
	[ResultComment] = source.[ResultComment], 
	[ObservationDate] = source.[ObservationDate], 
	--[CreateDateTime] = source.[CreateDateTime], 
	[ModifyDateTime] = source.[ModifyDateTime], 
	--[CreateLBUserId] = source.[CreateLBUserId], 
	[ModifyLBUserId] = source.[ModifyLBUserId] 
WHEN NOT MATCHED THEN
INSERT ([PatientLabOrderId],[ObservationCode1],[ObservationDescription1],[ObservationCodingSystemName1]
      ,[ObservationCode2]  ,[ObservationDescription2],[ObservationCodingSystemName2],[Value]
      ,[Units] ,[ReferenceRange],[AbnormalFlag],[ResultStatus]
      ,[ResultComment],[ObservationDate],[CreateDateTime],[ModifyDateTime]
      ,[CreateLBUserId],[ModifyLBUserId])
VALUES (source.[PatientLabOrderId],source.[ObservationCode1],source.[ObservationDescription1],source.[ObservationCodingSystemName1]
      ,source.[ObservationCode2]  ,source.[ObservationDescription2],source.[ObservationCodingSystemName2],source.[Value]
      ,source.[Units] ,source.[ReferenceRange],source.[AbnormalFlag],source.[ResultStatus]
      ,source.[ResultComment],source.[ObservationDate],source.[CreateDateTime],source.[ModifyDateTime]
      ,source.[CreateLBUserId],source.[ModifyLBUserId]);

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE PatientLabOrderProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'Labs',GetDate()
FROM PatientLabOrderProcessQueue
WHERE ProcessedInd = 1 

UPDATE A 
SET HoldingReleaseInd = 1 
FROM [dbo].[PatientLabOrderProcessQueue_HoldingBay] A 
INNER JOIN PatientLabOrderProcessQueue B ON A.InterfacePatientLabOrderId = B.InterfacePatientLabOrderId
WHERE B.ProcessedInd = 1 

--INSERT INTO[dbo].[PatientLabOrderProcessQueue_HoldingBay]
--([InterfacePatientLabOrderId],[LbPatientId],[InterfacePatientId]
--,[InterfaceSystemId],[OrderNumber],[ServiceId]
--,[ServiceDescription],[ServiceCodingSystemName],[OrderingProviderId]
--,[OrderingProviderIdType],[OrderingProviderName],[CreateDateTime]
--,[InterfacePatientLabResultId],[ObservationCode1],[ObservationDescription1]
--,[ObservationCodingSystemName1],[ObservationCode2],[ObservationDescription2]
--,[ObservationCodingSystemName2],[Value],[Units]
--,[ReferenceRange],[AbnormalFlag],[ResultStatus]
--,[ResultComment],[ObservationDate],[LR_CreateDateTime]
--,[PatientLabOrderId],[InterfacePatientLabOrderId_HoldingOverride],[LbPatientId_HoldingOverride]
--,[InterfacePatientId_HoldingOverride],[InterfaceSystemId_HoldingOverride],[OrderNumber_HoldingOverride]
--,[ServiceId_HoldingOverride],[ServiceDescription_HoldingOverride],[ServiceCodingSystemName_HoldingOverride]
--,[OrderingProviderId_HoldingOverride],[OrderingProviderIdType_HoldingOverride],[OrderingProviderName_HoldingOverride]
--,[CreateDateTime_HoldingOverride],[InterfacePatientLabResultId_HoldingOverride],[ObservationCode1_HoldingOverride]
--,[ObservationDescription1_HoldingOverride],[ObservationCodingSystemName1_HoldingOverride],[ObservationCode2_HoldingOverride]
--,[ObservationDescription2_HoldingOverride],[ObservationCodingSystemName2_HoldingOverride],[Value_HoldingOverride]
--,[Units_HoldingOverride],[ReferenceRange_HoldingOverride],[AbnormalFlag_HoldingOverride]
--,[ResultStatus_HoldingOverride],[ResultComment_HoldingOverride],[ObservationDate_HoldingOverride]
--,[LR_CreateDateTime_HoldingOverride],[PatientLabOrderId_HoldingOverride],[HoldingStartDate]
--,[HoldingStartReason])
-- SELECT 
-- A.[InterfacePatientLabOrderId],A.[LbPatientId],A.[InterfacePatientId]
--,A.[InterfaceSystemId],A.[OrderNumber],A.[ServiceId]
--,A.[ServiceDescription],A.[ServiceCodingSystemName],A.[OrderingProviderId]
--,A.[OrderingProviderIdType],A.[OrderingProviderName],A.[CreateDateTime]
--,A.[InterfacePatientLabResultId],A.[ObservationCode1],A.[ObservationDescription1]
--,A.[ObservationCodingSystemName1],A.[ObservationCode2],A.[ObservationDescription2]
--,A.[ObservationCodingSystemName2],A.[Value],A.[Units]
--,A.[ReferenceRange],A.[AbnormalFlag],A.[ResultStatus]
--,A.[ResultComment],A.[ObservationDate],A.[LR_CreateDateTime]
--,A.[PatientLabOrderId],A.[InterfacePatientLabOrderId],A.[LbPatientId]
--,A.[InterfacePatientId],A.[InterfaceSystemId],A.[OrderNumber]
--,A.[ServiceId],A.[ServiceDescription],A.[ServiceCodingSystemName]
--,A.[OrderingProviderId],A.[OrderingProviderIdType],A.[OrderingProviderName]
--,A.[CreateDateTime],A.[InterfacePatientLabResultId],A.[ObservationCode1]
--,A.[ObservationDescription1],A.[ObservationCodingSystemName1],A.[ObservationCode2]
--,A.[ObservationDescription2],A.[ObservationCodingSystemName2],A.[Value]
--,A.[Units],A.[ReferenceRange],A.[AbnormalFlag]
--,A.[ResultStatus],A.[ResultComment],A.[ObservationDate]
--,A.[LR_CreateDateTime],A.[PatientLabOrderId],GETDate()
--,'Duplicacy'
--FROM [dbo].[PatientLabOrderProcessQueue] A 
--LEFT JOIN PatientLabOrderProcessQueue_HoldingBay B ON A.InterfacePatientLabOrderId = B.InterfacePatientLabOrderId
--WHERE ISNULL(A.ProcessedInd,0) <> 1 AND B.InterfacePatientLabOrderId IS NULL


END




GO
