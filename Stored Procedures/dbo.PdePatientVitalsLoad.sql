SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientVitalsLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 

AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		pv.PatientId, 
		pv.SourceSystemId, 
		pv.ServiceDateTime,
		pv.HeightCM,
		pv.HeightComments,
		pv.WeightKG,
		pv.WeightComments,
		pv.TemperatureCelcius,
		pv.TemperatureComments,
		pv.Pulse,
		pv.PulseComments,
		pv.Respiration,
		pv.RespirationComments,
		pv.BloodPressureSystolic,
		pv.BloodPressureDiastolic,
		pv.BloodPressureComment,
		pv.OxygenSaturationSP02,
		pv.OxygenSaturationComment,
		pv.Clinician,
		pv.PatientVitalsComment
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientVitals pv ON pv.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
		AND (pv.CreateDateTime > @dtmStartDate OR pv.ModifyDateTime > @dtmStartDate)

END
GO
