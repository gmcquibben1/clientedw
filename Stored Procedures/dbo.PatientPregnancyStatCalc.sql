SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientPregnancyStatCalc]

AS

/*===============================================================
	CREATED BY: 	Mike Hoxter
	CREATED ON:		2016-01-16
	INITIAL VER:	2.2.1
	MODULE:			ETL 
	DESCRIPTION:	Stored Procedure to prep pregnancy stat dat into patientpregnancystat
	PARAMETERS:		
	  		
				
	RETURN VALUE(s)/OUTPUT:	Id of the newly created Assessment Questionnaire type will be returned in @ObGynDeliveryProcedureTypeId
	
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	 2.2.1       2016-01-16        MH		LBAN-3313    Initial
	 2.2.1       2016-01-16        MH		LBAN-3313    to add EDD date and make list of prenatal visists pull from type table
	 2.2.1       2016-01-16        MH		LBAN-3313,LBETL-975,LBAN-3729    Code Cleanup adding addiitonal codes
	 2.2.1       2016-03-09        CL		LBAN-3313,LBETL-975,LBAN-3729    



					 1. Error: In the section that inserts into #PregnancyTemp, there is a join: 
						INNER JOIN procedurecode PC
						ON PP.PatientProcedureId = PC.ProcedureCodeId
						
						This should be:
						
						INNER JOIN procedurecode PC
						ON PP.ProcedureCodeId = PC.ProcedureCodeId

					2. Change: In the section that inserts into #EDD, please change the DATEDIFF range in the WHERE clause from "-6 AND 6" to "-3 AND 3"

					3. Change: In the section that inserts into #PregnancyTemp, after the OR (...99213/Routine Ob Care...), this needs to be added:
						OR (PP.ProcedureCodeModifier1 = 'TH')
						Also, a set of parentheses needs to go around the three ProcedureCode/Modifier sections:
						DateDiff
						AND DateDiff
						AND (
						(ProcedureCode)
						OR 
						(ProcedureCode)
						OR
						(ProcedureCodeModifier)
						)

=================================================================*/



BEGIN
	TRUNCATE TABLE patientpregnancystat

	--DISTINCT DELIVERY VALUES FOR PATIENTS
	INSERT INTO PatientPregnancyStat (
		LbPatientId
		,DeliveryDate
		,PrenatalVisits_FirstTrimester
		,PrenatalVisits_SecondTrimester
		,PrenatalVisits_ThirdTrimester
		)
	SELECT DISTINCT [OrgHierarchy].LbPatientId
		,[PatientProcedure_Delivery].ProcedureDateTime
		,0
		,0
		,0
	FROM [dbo].[OrgHierarchy] [OrgHierarchy]
	INNER JOIN [dbo].[PatientProcedure] [PatientProcedure_Delivery]
		ON ([OrgHierarchy].[LbPatientId] = [PatientProcedure_Delivery].[PatientID])
	INNER JOIN [dbo].[ProcedureCode] [ProcedureCode_Delivery]
		ON [PatientProcedure_Delivery].ProcedureCodeId = [ProcedureCode_Delivery].ProcedureCodeId
	INNER JOIN vwPortal_AllTypeTables
		ON [ProcedureCode_Delivery].ProcedureCode = dbo.vwPortal_AllTypeTables.[Name] --join to manage list type table for deliveries
			AND dbo.vwPortal_AllTypeTables.TableName = 'ObGynDeliveryProcedureType'
	WHERE [OrgHierarchy].GenderId = 2

	--CALCULATE POSSIBLE Estimated Delivery Dates
	SELECT PatientPregnancyStat_ID
		,PPS.LbPatientId
		,PPS.DeliveryDate
		,PPH.EstimatedDateOfDelivery
		,DATEDIFF(Day, PPS.DeliveryDate, PPH.EstimatedDateOfDelivery) AS DIFF
	INTO #EDD
	FROM PatientPregnancyStat PPS
	INNER JOIN PatientPregnancyHistory PPH
		ON PPS.LbPatientId = PPH.PatientId
	WHERE DATEDIFF(Month, PPS.DeliveryDate, PPH.EstimatedDateOfDelivery) BETWEEN - 3
			AND 3  --LBAN-3729

	--Calculate Estimated date of delivery, if no value found, use the delivery date found in query 1
	UPDATE PPS
	SET PPS.EddDate = ISNULL(EDD.EstimatedDateOfDelivery, PPS.DeliveryDate)
	FROM PatientPregnancyStat PPS
	LEFT JOIN (
		SELECT PatientPregnancyStat_ID
			,EstimatedDateOfDelivery
			,ROW_NUMBER() OVER (
				PARTITION BY PatientPregnancyStat_ID ORDER BY DIFF ASC
				) AS RANK
		FROM #EDD
		) EDD
		ON PPS.PatientPregnancyStat_ID = EDD.PatientPregnancyStat_ID
			AND EDD.RANK = 1

	--BUILD TRIMESTERS FROM EDD
	SELECT DISTINCT PPS.PatientPregnancyStat_ID
		,PPS.EddDate AS DeliveryDate
		,PP.ProcedureDateTime
		,datediff("day", PP.ProcedureDateTime, PPS.EddDate) AS [DaysDifference]
	INTO #PregnancyTemp --drop table #PregnancyTemp 
	FROM PatientPregnancyStat PPS
	INNER JOIN PatientProcedure PP
		ON PPS.LbPatientId = PP.PatientID
	INNER JOIN procedurecode PC
		ON PP.ProcedureCodeId = PC.ProcedureCodeId
	WHERE datediff(day, PP.ProcedureDateTime, PPS.EddDate) > 0
		AND datediff(day, PP.ProcedureDateTime, PPS.EddDate) < 300
		AND (PC.ProcedureCode IN (
			SELECT [Name]
			FROM vwPortal_AllTypeTables
			WHERE TableName = 'PrenatalProcedureType'
			)
		OR (
			PC.ProcedureCode = '99213'
			AND PC.ProcedureDescription = 'Routine Ob Care'
			)
		OR  (PP.ProcedureCodeModifier1 = 'TH')
		)

	SELECT DISTINCT a.*
		,[1st trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 190
					AND 300
				THEN 1
			ELSE 0
			END
		,[2nd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 85
					AND 189
				THEN 1
			ELSE 0
			END
		,[3rd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 0
					AND 84
				THEN 1
			ELSE 0
			END
	INTO #count --drop table #count
	FROM #PregnancyTemp a

	SELECT DISTINCT a.PatientPregnancyStat_ID
		,sum(a.[1st trimester]) PrenatalVisits_FirstTrimester
		,sum(a.[2nd trimester]) PrenatalVisits_SecondTrimester
		,sum(a.[3rd trimester]) PrenatalVisits_ThirdTrimester
	INTO #update
	FROM #count a
	GROUP BY a.PatientPregnancyStat_ID

	UPDATE patientpregnancystat
	SET PrenatalVisits_FirstTrimester = u.PrenatalVisits_FirstTrimester
		,PrenatalVisits_SecondTrimester = u.PrenatalVisits_SecondTrimester
		,PrenatalVisits_ThirdTrimester = u.PrenatalVisits_ThirdTrimester
	FROM patientpregnancystat pat
	INNER JOIN #update u
		ON pat.PatientPregnancyStat_ID = u.PatientPregnancyStat_ID

	--/*
	--*****************************************************************************************************
	--ER
	SELECT O.LbPatientId
		,COUNT(DISTINCT (CCLF0.CUR_CLM_UNIQ_ID)) AS [ER VISITS]
		,A.CLM_FROM_DT
	INTO #ER
	FROM CCLF_0_PartA_CLM_TYPES CCLF0
	INNER JOIN CCLF_1_PartA_Header A
		ON CCLF0.CUR_CLM_UNIQ_ID = A.CUR_CLM_UNIQ_ID
	INNER JOIN OrgHierarchy O
		ON A.LbPatientId = O.LbPatientId
	WHERE CCLF0.ER = 1
	GROUP BY O.LbPatientId
		,A.CLM_FROM_DT

	--select * from #ER
	--17405
	SELECT er.*
		,pat.EddDate AS DeliveryDate
		,pat.PatientPregnancyStat_ID
		,datediff("day", er.CLM_FROM_DT, pat.EddDate) AS [DaysDifference]
	INTO #er_temp --drop table #er_temp
	FROM #ER er
	INNER JOIN patientpregnancystat pat
		ON er.LbPatientId = pat.LbPatientId

	--select * from #er_temp
	--2820
	SELECT DISTINCT a.*
		,[1st trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 190
					AND 300
				THEN 1
			ELSE 0
			END
		,[2nd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 85
					AND 189
				THEN 1
			ELSE 0
			END
		,[3rd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 0
					AND 84
				THEN 1
			ELSE 0
			END
	INTO #er_count --drop table #er_count
	FROM #er_temp a

	SELECT DISTINCT a.PatientPregnancyStat_ID
		,sum(a.[1st trimester]) ERVisits_FirstTrimester
		,sum(a.[2nd trimester]) ERVisits_SecondTrimester
		,sum(a.[3rd trimester]) ERVisits_ThirdTrimester
	INTO #er_update --drop table #er_update
	FROM #er_count a
	GROUP BY a.PatientPregnancyStat_ID

	--drop table #patientpregnancystat
	--select * from patientpregnancystat
	UPDATE patientpregnancystat
	SET ERVisits_FirstTrimester = u.ERVisits_FirstTrimester
		,ERVisits_SecondTrimester = u.ERVisits_SecondTrimester
		,ERVisits_ThirdTrimester = u.ERVisits_ThirdTrimester
	--select pat.*--, u.*
	FROM patientpregnancystat pat
	INNER JOIN #er_update u
		ON pat.PatientPregnancyStat_ID = u.PatientPregnancyStat_ID

	UPDATE patientpregnancystat
	SET ERVisits_FirstTrimester = 0
	FROM patientpregnancystat
	WHERE ERVisits_FirstTrimester IS NULL

	UPDATE patientpregnancystat
	SET ERVisits_SecondTrimester = 0
	FROM patientpregnancystat
	WHERE ERVisits_SecondTrimester IS NULL

	UPDATE patientpregnancystat
	SET ERVisits_ThirdTrimester = 0
	FROM patientpregnancystat
	WHERE ERVisits_ThirdTrimester IS NULL

	--select * from #er_update where PatientPregnancyStat_ID = '92581'
	--select * from #patientpregnancystat WHERE ERVisits_FirstTrimester = 1
	--*****************************************************************************************************
	--total part a cost
	SELECT O.LbPatientId
		,A.CLM_FROM_DT
		,sum(isnull(A.CLM_PMT_AMT, 0.0)) AS [Total Part A Cost]
	INTO #partA --drop table #partA
	FROM CCLF_1_PartA_Header A
	INNER JOIN OrgHierarchy O
		ON A.LbPatientId = O.LbPatientId
	GROUP BY O.LbPatientId
		,A.CLM_FROM_DT

	--select * from #partA where [Total Part A Cost] is null
	SELECT a.*
		,pat.EddDate AS DeliveryDate
		,pat.PatientPregnancyStat_ID
		,datediff("day", a.CLM_FROM_DT, pat.EddDate) AS [DaysDifference]
	INTO #partA_temp --drop table #partA_temp
	FROM #partA a
	INNER JOIN patientpregnancystat pat
		ON a.LbPatientId = pat.LbPatientId

	--select * from #er_temp
	--2820
	SELECT DISTINCT a.*
		,[1st trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 190
					AND 300
				THEN a.[Total Part A Cost]
			ELSE 0.0
			END
		,[2nd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 85
					AND 189
				THEN a.[Total Part A Cost]
			ELSE 0.0
			END
		,[3rd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 0
					AND 84
				THEN a.[Total Part A Cost]
			ELSE 0.0
			END
	INTO #partA_count --drop table #partA_count
	FROM #partA_temp a

	--select * from #partA_count
	SELECT DISTINCT a.PatientPregnancyStat_ID
		,sum(a.[1st trimester]) PartA_Cost_FirstTrimester
		,sum(a.[2nd trimester]) PartA_Cost_SecondTrimester
		,sum(a.[3rd trimester]) PartA_Cost_ThirdTrimester
	INTO #PartA_update --drop table #PartA_update
	FROM #PartA_count a
	GROUP BY a.PatientPregnancyStat_ID

	--select * from patientpregnancystat
	--select * from #PartA_update
	UPDATE patientpregnancystat
	SET PartA_Cost_FirstTrimester = isnull(u.PartA_Cost_FirstTrimester, 0)
		,PartA_Cost_SecondTrimester = isnull(u.PartA_Cost_SecondTrimester, 0)
		,[PartA_Cost_ThirdTrimester] = isnull(u.[PartA_Cost_ThirdTrimester], 0)
	--select pat.*--, u.*
	FROM patientpregnancystat pat
	INNER JOIN #PartA_update u
		ON pat.PatientPregnancyStat_ID = u.PatientPregnancyStat_ID

	UPDATE patientpregnancystat
	SET PartA_Cost_FirstTrimester = 0
	FROM patientpregnancystat
	WHERE PartA_Cost_FirstTrimester IS NULL

	UPDATE patientpregnancystat
	SET PartA_Cost_SecondTrimester = 0
	FROM patientpregnancystat
	WHERE PartA_Cost_SecondTrimester IS NULL

	UPDATE patientpregnancystat
	SET PartA_Cost_ThirdTrimester = 0
	FROM patientpregnancystat
	WHERE PartA_Cost_ThirdTrimester IS NULL

	--*****************************************************************************************************
	--total part b cost
	SELECT O.LbPatientId
		,B.CLM_FROM_DT
		,sum(isnull(B.CLM_LINE_CVRD_PD_AMT, 0.0)) AS [Total Part B Cost]
	INTO #partB --drop table #partB
	FROM CCLF_5_PartB_Physicians B
	INNER JOIN OrgHierarchy O
		ON B.LbPatientId = O.LbPatientId
	GROUP BY O.LbPatientId
		,B.CLM_FROM_DT

	SELECT a.*
		,pat.EddDate AS DeliveryDate
		,pat.PatientPregnancyStat_ID
		,datediff("day", a.CLM_FROM_DT, pat.EddDate) AS [DaysDifference]
	INTO #partB_temp --drop table #partB_temp
	FROM #partB a
	INNER JOIN patientpregnancystat pat
		ON a.LbPatientId = pat.LbPatientId

	--select * from #er_temp
	--2820
	SELECT DISTINCT a.*
		,[1st trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 190
					AND 300
				THEN a.[Total Part B Cost]
			ELSE 0.0
			END
		,[2nd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 85
					AND 189
				THEN a.[Total Part B Cost]
			ELSE 0.0
			END
		,[3rd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 0
					AND 84
				THEN a.[Total Part B Cost]
			ELSE 0.0
			END
	INTO #partB_count --drop table #partB_count
	FROM #partB_temp a

	--select * from #partB_count
	SELECT DISTINCT a.PatientPregnancyStat_ID
		,sum(a.[1st trimester]) partB_Cost_FirstTrimester
		,sum(a.[2nd trimester]) partB_Cost_SecondTrimester
		,sum(a.[3rd trimester]) partB_Cost_ThirdTrimester
	INTO #partB_update --drop table #partB_update
	FROM #partB_count a
	GROUP BY a.PatientPregnancyStat_ID

	--select * from patientpregnancystat
	--select * from #partB_update
	UPDATE patientpregnancystat
	SET partB_Cost_FirstTrimester = isnull(u.partB_Cost_FirstTrimester, 0)
		,partB_Cost_SecondTrimester = isnull(u.partB_Cost_SecondTrimester, 0)
		,[partB_Cost_ThirdTrimester] = isnull(u.[partB_Cost_ThirdTrimester], 0)
	--select pat.*--, u.*
	FROM patientpregnancystat pat
	INNER JOIN #partB_update u
		ON pat.PatientPregnancyStat_ID = u.PatientPregnancyStat_ID

	UPDATE patientpregnancystat
	SET partB_Cost_FirstTrimester = 0
	FROM patientpregnancystat
	WHERE partB_Cost_FirstTrimester IS NULL

	UPDATE patientpregnancystat
	SET partB_Cost_SecondTrimester = 0
	FROM patientpregnancystat
	WHERE partB_Cost_SecondTrimester IS NULL

	UPDATE patientpregnancystat
	SET partB_Cost_ThirdTrimester = 0
	FROM patientpregnancystat
	WHERE partB_Cost_ThirdTrimester IS NULL

	--select * from patientpregnancystat
	--*****************************************************************************************************
	--part a cost for set of CPTs
	SELECT O.LbPatientId
		,A.CLM_FROM_DT
		,sum(isnull(A.CLM_PMT_AMT, 0.0)) AS [Total Part A Cost]
	INTO #partA_cpt
	FROM CCLF_1_PartA_Header A
	INNER JOIN OrgHierarchy O
		ON A.LbPatientId = O.LbPatientId
	WHERE A.CUR_CLM_UNIQ_ID IN (
			SELECT CUR_CLM_UNIQ_ID
			FROM CCLF_2_PartA_RCDetail
			WHERE CLM_LINE_HCPCS_CD IN (
					--'BIG LIST OF CPTS'
					SELECT DISTINCT Code
					FROM [HEDISValueSetCodes]
					WHERE [value set name] LIKE '%dbo%'
					)
			)
	GROUP BY O.LbPatientId
		,A.CLM_FROM_DT

	SELECT a.*
		,pat.EddDate AS DeliveryDate
		,pat.PatientPregnancyStat_ID
		,datediff("day", a.CLM_FROM_DT, pat.EddDate) AS [DaysDifference]
	INTO #partA_cpt_temp --drop table #partA_cpt_temp
	FROM #partA_cpt a
	INNER JOIN patientpregnancystat pat
		ON a.LbPatientId = pat.LbPatientId

	--select * from #er_temp
	--2820
	SELECT DISTINCT a.*
		,[1st trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 190
					AND 300
				THEN a.[Total Part A Cost]
			ELSE 0.0
			END
		,[2nd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 85
					AND 189
				THEN a.[Total Part A Cost]
			ELSE 0.0
			END
		,[3rd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 0
					AND 84
				THEN a.[Total Part A Cost]
			ELSE 0.0
			END
	INTO #partA_cpt_count --drop table #partA_cpt_count
	FROM #partA_cpt_temp a

	--select * from #partA_count
	SELECT DISTINCT a.PatientPregnancyStat_ID
		,sum(a.[1st trimester]) PartA_MEOC_Cost_FirstTrimester
		,sum(a.[2nd trimester]) PartA_MEOC_Cost_SecondTrimester
		,sum(a.[3rd trimester]) PartA_MEOC_Cost_ThirdTrimester
	INTO #partA_cpt_update --drop table #PartA_update
	FROM #partA_cpt_count a
	GROUP BY a.PatientPregnancyStat_ID

	--select * from patientpregnancystat
	--select * from #PartA_update
	UPDATE patientpregnancystat
	SET PartA_MEOC_Cost_FirstTrimester = isnull(u.PartA_MEOC_Cost_FirstTrimester, 0)
		,PartA_MEOC_Cost_SecondTrimester = isnull(u.PartA_MEOC_Cost_SecondTrimester, 0)
		,[PartA_MEOC_Cost_ThirdTrimester] = isnull(u.[PartA_MEOC_Cost_ThirdTrimester], 0)
	--select pat.*--, u.*
	FROM patientpregnancystat pat
	INNER JOIN #partA_cpt_update u
		ON pat.PatientPregnancyStat_ID = u.PatientPregnancyStat_ID

	UPDATE patientpregnancystat
	SET PartA_MEOC_Cost_FirstTrimester = 0
	FROM patientpregnancystat
	WHERE PartA_MEOC_Cost_FirstTrimester IS NULL

	UPDATE patientpregnancystat
	SET PartA_MEOC_Cost_SecondTrimester = 0
	FROM patientpregnancystat
	WHERE PartA_MEOC_Cost_SecondTrimester IS NULL

	UPDATE patientpregnancystat
	SET PartA_MEOC_Cost_ThirdTrimester = 0
	FROM patientpregnancystat
	WHERE PartA_MEOC_Cost_ThirdTrimester IS NULL

	--*****************************************************************************************************
	--Part B cost for a set of CPTs
	SELECT O.LbPatientId
		,B.CLM_FROM_DT
		,sum(isnull(B.CLM_LINE_CVRD_PD_AMT, 0.0)) AS [Total Part B Cost]
	INTO #partB_cpt
	FROM CCLF_5_PartB_Physicians B
	INNER JOIN OrgHierarchy O
		ON B.LbPatientId = O.LbPatientId
	WHERE B.CLM_LINE_HCPCS_CD IN (
			--'big list of CPTs'
			SELECT DISTINCT Code
			FROM [HEDISValueSetCodes]
			WHERE [value set name] LIKE '%dbo%'
			)
	GROUP BY O.LbPatientId
		,B.CLM_FROM_DT

	SELECT a.*
		,pat.EddDate AS DeliveryDate
		,pat.PatientPregnancyStat_ID
		,datediff("day", a.CLM_FROM_DT, pat.EddDate) AS [DaysDifference]
	INTO #partB_cpt_temp --drop table #partB_cpt_temp
	FROM #partB_cpt a
	INNER JOIN patientpregnancystat pat
		ON a.LbPatientId = pat.LbPatientId

	--select * from #er_temp
	--2820
	SELECT DISTINCT a.*
		,[1st trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 190
					AND 300
				THEN a.[Total Part B Cost]
			ELSE 0.0
			END
		,[2nd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 85
					AND 189
				THEN a.[Total Part B Cost]
			ELSE 0.0
			END
		,[3rd trimester] = CASE 
			WHEN a.[DaysDifference] BETWEEN 0
					AND 84
				THEN a.[Total Part B Cost]
			ELSE 0.0
			END
	INTO #partB_cpt_count --drop table #partB_cpt_count
	FROM #partB_cpt_temp a

	--select * from #partB_count
	SELECT DISTINCT a.PatientPregnancyStat_ID
		,sum(a.[1st trimester]) PartB_MEOC_Cost_FirstTrimester
		,sum(a.[2nd trimester]) PartB_MEOC_Cost_SecondTrimester
		,sum(a.[3rd trimester]) PartB_MEOC_Cost_ThirdTrimester
	INTO #partB_cpt_update --drop table #partB_update
	FROM #partB_cpt_count a
	GROUP BY a.PatientPregnancyStat_ID

	--select * from patientpregnancystat
	--select * from #partB_update
	UPDATE patientpregnancystat
	SET PartB_MEOC_Cost_FirstTrimester = isnull(u.PartB_MEOC_Cost_FirstTrimester, 0)
		,PartB_MEOC_Cost_SecondTrimester = isnull(u.PartB_MEOC_Cost_SecondTrimester, 0)
		,[PartB_MEOC_Cost_ThirdTrimester] = isnull(u.[PartB_MEOC_Cost_ThirdTrimester], 0)
	--select pat.*--, u.*
	FROM patientpregnancystat pat
	INNER JOIN #partB_cpt_update u
		ON pat.PatientPregnancyStat_ID = u.PatientPregnancyStat_ID

	UPDATE patientpregnancystat
	SET PartB_MEOC_Cost_FirstTrimester = 0
	FROM patientpregnancystat
	WHERE PartB_MEOC_Cost_FirstTrimester IS NULL

	UPDATE patientpregnancystat
	SET PartB_MEOC_Cost_SecondTrimester = 0
	FROM patientpregnancystat
	WHERE PartB_MEOC_Cost_SecondTrimester IS NULL

	UPDATE patientpregnancystat
	SET PartB_MEOC_Cost_ThirdTrimester = 0
	FROM patientpregnancystat
	WHERE PartB_MEOC_Cost_ThirdTrimester IS NULL

	--*****************************************************************************************************
	UPDATE patientpregnancystat
	SET EpisiotomyInd = ph.EpisiotomyInd
	--select *
	FROM patientpregnancystat a
	INNER JOIN [PatientPregnancyHistory] ph
		ON a.LbPatientId = ph.PatientId

	UPDATE patientpregnancystat
	SET EpisiotomyInd = 0
	FROM patientpregnancystat
	WHERE EpisiotomyInd IS NULL

	--*****************************************************************************************************
	--*/
	DROP TABLE #update

	DROP TABLE #count

	DROP TABLE #PregnancyTemp

	DROP TABLE #ER

	DROP TABLE #er_temp

	DROP TABLE #er_count

	DROP TABLE #er_update

	DROP TABLE #partA

	DROP TABLE #partA_temp

	DROP TABLE #partA_count

	DROP TABLE #partA_update

	DROP TABLE #partB

	DROP TABLE #partB_temp

	DROP TABLE #partB_count

	DROP TABLE #partB_update

	DROP TABLE #partA_cpt

	DROP TABLE #partA_cpt_temp

	DROP TABLE #partA_cpt_count

	DROP TABLE #partA_cpt_update

	DROP TABLE #partB_cpt

	DROP TABLE #partB_cpt_temp

	DROP TABLE #partB_cpt_count

	DROP TABLE #partB_cpt_update
END
GO
