SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalGproPatientMatch]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		11.06.2015
	INITIAL VER:	1.6.1
	MODULE:			GPRO		
	DESCRIPTION:	This stored proc matches patients in GproPatientRanking with patients in the EDW database and sets the LbPatientId in GproPatientRanking accordingly
	PARAMETERS:		none
	RETURN VALUE(s)/OUTPUT:	none
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.6.1		11.06.2015		BR						Initial Version
	2.2.1		2017-01-09		BR						Add where clause to make sure the MRN from PatientIdReference isn't too long

=================================================================*/
AS
BEGIN
    SET NOCOUNT ON;
	-----------------------------------------------------------------------------------------
	--Step 1; Reset the LbPatientId for all of the GPRO patients
	-----------------------------------------------------------------------------------------
	UPDATE GproPatientRanking SET LbPatientId = NULL, ModifyDateTime = GETUTCDATE();

	-----------------------------------------------------------------------------------------
	--Step 2; match by HICN
	-----------------------------------------------------------------------------------------
	UPDATE gpro
	SET gpro.LbPatientId = idRef.LbPatientId, ModifyDateTime = GETUTCDATE()
	FROM GproPatientRanking gpro
		INNER JOIN PatientIdReference idRef ON idRef.ExternalId = gpro.MedicareHicn AND idRef.DeleteInd <> 1

	-----------------------------------------------------------------------------------------
	--Step 3; match remaining patients by exact name/dob/gender/etc
	-----------------------------------------------------------------------------------------
	UPDATE gpro
	SET gpro.LbPatientId = p.PatientId, ModifyDateTime = GETUTCDATE()
	FROM GproPatientRanking gpro
		INNER JOIN vwPatientDemographic p ON p.FirstName = gpro.FirstName AND p.LastName = gpro.LastName AND CONVERT(VARCHAR(10), p.BirthDate, 101) = gpro.BirthDate
	WHERE gpro.LbPatientId IS NULL;

	-----------------------------------------------------------------------------------------
	--Step 4; match remaining patients fuzzy match by getting rid of known issues w/ last name
	-----------------------------------------------------------------------------------------
	UPDATE gpro
	SET gpro.LbPatientId = p.PatientId, ModifyDateTime = GETUTCDATE()
	FROM GproPatientRanking gpro
		INNER JOIN vwPatientDemographic p ON p.FirstName = gpro.FirstName AND p.LastName = SUBSTRING(gpro.LastName,1,CHARINDEX(' ',gpro.LastName)-1) AND CONVERT(VARCHAR(10), p.BirthDate, 101) = gpro.BirthDate
	WHERE gpro.LbPatientId IS NULL AND (gpro.LastName LIKE '% SR' OR gpro.LastName LIKE '% JR');

	-----------------------------------------------------------------------------------------
	--Step 5; set the MRN (if available) for every matched patient
	-----------------------------------------------------------------------------------------
	UPDATE gpro
	SET gpro.[MedicalRecordNumber] = idRef.ExternalId, ModifyDateTime = GETUTCDATE()
	FROM GproPatientRanking gpro
		INNER JOIN PatientIdReference idRef ON idRef.LbPatientId = gpro.LbPatientId AND idRef.IdTypeDesc = 'InterfacePatientIdentifier' AND idRef.DeleteInd <> 1
		INNER JOIN SourceSystem ss ON ss.SourceSystemId = idRef.SourceSystemId AND ss.ClinicalInd = 1
	WHERE LEN(idRef.ExternalId) <= 25

	UPDATE gpro
	SET gpro.[OtherIdentifier] = idRef.ExternalId, ModifyDateTime = GETUTCDATE()
	FROM GproPatientRanking gpro
		INNER JOIN PatientIdReference idRef ON idRef.LbPatientId = gpro.LbPatientId AND idRef.IdTypeDesc = 'InterfacePatientOtherIdentifier' AND idRef.DeleteInd <> 1
		INNER JOIN SourceSystem ss ON ss.SourceSystemId = idRef.SourceSystemId AND ss.ClinicalInd = 1
	WHERE LEN(idRef.ExternalId) <= 20

	-----------------------------------------------------------------------------------------
	--Step 6; set the medical record found indicator if we have any clinical records for this patient
	-----------------------------------------------------------------------------------------
	UPDATE gpro
	SET gpro.[MedicalRecordFound] = 2, ModifyDateTime = GETUTCDATE()
	FROM GproPatientMeasure gpro
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpro.GproPatientRankingId
	WHERE gpro.[MedicalRecordFound] IS NULL
		AND 
		(EXISTS (SELECT TOP 1 1 FROM PatientIdReference idRef
					INNER JOIN SourceSystem ss ON ss.SourceSystemId = idRef.SourceSystemId AND idRef.IdTypeDesc = 'InterfacePatientIdentifier' AND ss.ClinicalInd = 1
					WHERE idRef.DeleteInd <> 1 AND idRef.LbPatientId = gpr.LbPatientId)
		--OR EXISTS (SELECT TOP 1 1 FROM PatientProcedure pp
		--			INNER JOIN SourceSystem ss ON ss.SourceSystemId = pp.SourceSystemId AND ss.ClinicalInd = 1
		--			WHERE pp.PatientId = gpr.LbPatientId)
		--OR EXISTS (SELECT TOP 1 1 FROM PatientDiagnosis pd
		--			INNER JOIN SourceSystem ss ON ss.SourceSystemId = pd.SourceSystemId AND ss.ClinicalInd = 1
		--			WHERE pd.PatientId = gpr.LbPatientId)
		--OR EXISTS (SELECT TOP 1 1 FROM PatientVitals pv
		--			INNER JOIN SourceSystem ss ON ss.SourceSystemId = pv.SourceSystemId AND ss.ClinicalInd = 1
		--			WHERE pv.PatientId = gpr.LbPatientId)
		--OR EXISTS (SELECT TOP 1 1 FROM PatientMedication pm
		--			INNER JOIN SourceSystem ss ON ss.SourceSystemId = pm.SourceSystemId AND ss.ClinicalInd = 1
		--			WHERE pm.PatientId = gpr.LbPatientId)
		--OR EXISTS (SELECT TOP 1 1 FROM PatientImmunization pim
		--			INNER JOIN SourceSystem ss ON ss.SourceSystemId = pim.SourceSystemId AND ss.ClinicalInd = 1
		--			WHERE pim.PatientId = gpr.LbPatientId)
		--OR EXISTS (SELECT TOP 1 1 FROM PatientLabOrder plo
		--			INNER JOIN SourceSystem ss ON ss.SourceSystemId = plo.SourceSystemId AND ss.ClinicalInd = 1
		--			WHERE plo.PatientId = gpr.LbPatientId)
		)
END
GO
