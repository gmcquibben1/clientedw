SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Youping
-- Create date: 2016-04-04
-- Description:	Load Data into EDW MedicationFormType 
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[ETLMedicationFormType]

AS
BEGIN	
		DECLARE @Today DATE;

		SET @Today=GETUTCDATE();
		
		   MERGE INTO MedicationFormType AS target
			USING (SELECT distinct MedicationForm

			    --   FROM vwPortal_InterfacePatientMedication ipm
				--   WHERE CreateDateTime > @lastDateTime
				   FROM [dbo].[PatientMedicationProcessQueue]
				   where ISNULL(MedicationForm,'')<> '' 
				  ) AS source
			ON source.MedicationForm = target.[Name]
			WHEN NOT MATCHED BY TARGET THEN
				INSERT ([Name],DisplayValue,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
				VALUES (source.MedicationForm,source.MedicationForm,0,@Today,@Today,1,1);

END
GO
