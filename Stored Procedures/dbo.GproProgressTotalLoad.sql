SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproProgressTotalLoad]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2016-10-24
	INITIAL VER:	2.2.1
	MODULE:			GPRO		
	DESCRIPTION:	For the selected business unit id and measure return the summed counts for current progress
	PARAMETERS:		BusinessUnitId, GPro measure name (optional)
	RETURN VALUE(s)/OUTPUT:	a table containing all of the available totals 
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-10-24		BR			LBPP-2026	Initial version 
	2.2.1		2016-11-09		BR			LBPP-2028	Added additional column to skipped and complete sets to comply w/ new stored proc return values
	2.2.1		11-23-2016		BR			LBPP-2033	Fixed a bug by changing indexes on temp table to use GproPatientRankingId.  This is because PatientId will be NULL if
														the patient can't be matched to an existing patient
	2.2.1		01-12-2017		BR						Added a join to the #patientList when getting the top 248 patients to calculate the remaining to target
	2.2.1		01-17-2017		BR						The previous change to join on #patientList limited the patients too much, need to calculate the remaining to target in reference to the whole ACO
=================================================================*/
(
    @BusinessUnitId INT,
	@GproMeasureName VARCHAR(20) = NULL
)
AS
BEGIN
    SET NOCOUNT ON;
	
	--------------------------------------------------------------------------------
	-- create temp table to store the totals in prior to returning them
	--------------------------------------------------------------------------------
	CREATE TABLE #tmpGPROMeasureProgress
				(
					RowNumber INT IDENTITY (1, 1)
					, TotalRanked INT
					, TotalSkipped INT
					, TotalComplete INT
					, TotalIncomplete INT
					, ConsecutiveTarget INT
					, ConsecutivelyComplete INT
					, RemainingToTarget INT
				);

	CREATE TABLE #patientList
	(
		GproPatientRankingId INT,
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #groupTinPatientList
	(
		GproPatientRankingId INT,
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #measureList
	(
		RowNumber INT IDENTITY (1, 1)
		, MeasureName VARCHAR(20)
		, RankField VARCHAR(20)
	);

	CREATE TABLE #CompletePatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #IncompletePatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #SkippedPatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	DECLARE @Sql NVARCHAR(4000);
	DECLARE @RankColumnName VARCHAR(50);
	--------------------------------------------------------------------------------
	-- build a list of GPRO patients that can be viewed at this level of the hierarchy
	--------------------------------------------------------------------------------
	INSERT INTO #patientList EXEC GproGetPatientListByBusinessUnit @businessUnitId;

	INSERT INTO #groupTinPatientList
		(GproPatientRankingId)
	SELECT gpr.GproPatientRankingId 
	FROM GproPatientRanking gpr
	WHERE gpr.GroupTin IN (SELECT DISTINCT gpr2.GroupTin FROM GproPatientRanking gpr2 INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr2.GproPatientRankingId)

	--------------------------------------------------------------------------------
	-- insert a single row with al zeroes for the counts
	--------------------------------------------------------------------------------
	INSERT INTO #tmpGPROMeasureProgress
		(TotalRanked,TotalSkipped,TotalComplete,TotalIncomplete,ConsecutiveTarget,ConsecutivelyComplete,RemainingToTarget)
	VALUES
		(0,0,0,0,0,0,0);

	--------------------------------------------------------------------------------
	-- populate a temp table with the list of measures we will include
	--------------------------------------------------------------------------------
	IF @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #measureList (MeasureName, RankField) SELECT Name, RankColumnName FROM GproMeasureType;
	END
	ELSE
	BEGIN 
		INSERT INTO #measureList (MeasureName, RankField) SELECT Name, RankColumnName FROM GproMeasureType WHERE Name = @GproMeasureName;
	END

	--------------------------------------------------------------------------------
	-- loop through the temp table and update the counts in each loop
	--------------------------------------------------------------------------------
	DECLARE @CurrentRankColumn VARCHAR(20);
	DECLARE @CurrentMeasureName VARCHAR(20);
	DECLARE @CurrentMeasureId INT = (SELECT MAX(RowNumber) FROM #measureList);
	WHILE @CurrentMeasureId > 0
	BEGIN
		SET @CurrentRankColumn = (SELECT RankField FROM #measureList WHERE RowNumber = @CurrentMeasureId)
		SET @CurrentMeasureName = (SELECT MeasureName FROM #measureList WHERE RowNumber = @CurrentMeasureId)

		--------------------------------------------------------------------------------
		-- find the set of complete and skipped patients for this measure
		--------------------------------------------------------------------------------
		TRUNCATE TABLE #CompletePatients;
		TRUNCATE TABLE #SkippedPatients;
		TRUNCATE TABLE #IncompletePatients;
		SET @Sql = 'INSERT INTO #CompletePatients EXEC GproGetCompletePatientList ''' + @CurrentMeasureName + ''''
		EXEC (@Sql);
		SET @Sql = 'INSERT INTO #SkippedPatients EXEC GproGetSkippedPatientList ''' + @CurrentMeasureName + ''''
		EXEC (@Sql);
		SET @Sql = 'INSERT INTO #IncompletePatients (LbPatientId,MeasureRank,GproPatientRankingId,GproPatientMeasureId,GproMeasureTypeId)
			SELECT gpr.LbPatientId,gpr.' + @CurrentRankColumn + ',gpr.GproPatientRankingId,gpm.GproPatientMeasureId,null
			FROM GproPatientMeasure gpm 
				INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.' + @CurrentRankColumn + ' <> 0
				AND NOT EXISTS (SELECT TOP 1 1 FROM #CompletePatients cp WHERE cp.GproPatientRankingId = gpr.GproPatientRankingId)
				AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GproPatientRankingId = gpr.GproPatientRankingId)';
		EXEC (@Sql);

		--------------------------------------------------------------------------------
		-- Get the total of all ranked patients for this measure
		--------------------------------------------------------------------------------
		SET @Sql = 'UPDATE p
			SET p.TotalRanked = p.TotalRanked + (SELECT COUNT(gpr.GproPatientRankingId) 
												 FROM GproPatientRanking gpr 
													INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId 
												WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL and gpr.' + @CurrentRankColumn + ' <> 0)
			FROM #tmpGPROMeasureProgress p'
		EXEC (@Sql);

		--------------------------------------------------------------------------------
		-- Get the total skipped for this measure
		--------------------------------------------------------------------------------
		SET @Sql = 'UPDATE p
			SET p.TotalSkipped = p.TotalSkipped + (SELECT COUNT(gpr.GproPatientRankingId) 
												 FROM GproPatientRanking gpr 
													INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId 
													INNER JOIN #SkippedPatients sp ON sp.GproPatientRankingId = gpr.GproPatientRankingId
												WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL and gpr.' + @CurrentRankColumn + ' <> 0)
			FROM #tmpGPROMeasureProgress p'
		EXEC (@Sql);

		--------------------------------------------------------------------------------
		-- Get the total complete for this measure
		--------------------------------------------------------------------------------
		SET @Sql = 'UPDATE p
			SET p.TotalComplete = p.TotalComplete + (SELECT COUNT(gpr.GproPatientRankingId) 
												 FROM GproPatientRanking gpr 
													INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId 
													INNER JOIN #CompletePatients cp ON cp.GproPatientRankingId = gpr.GproPatientRankingId
												WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL and gpr.' + @CurrentRankColumn + ' <> 0)
			FROM #tmpGPROMeasureProgress p'
		EXEC (@Sql);

		--------------------------------------------------------------------------------
		-- Get the total Incomplete for this measure
		--------------------------------------------------------------------------------
		SET @Sql = 'UPDATE p
			SET p.TotalIncomplete = p.TotalIncomplete + (SELECT COUNT(gpr.GproPatientRankingId) 
												 FROM GproPatientRanking gpr 
													INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId 
													INNER JOIN #IncompletePatients ip ON ip.GproPatientRankingId = gpr.GproPatientRankingId
												WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.' + @CurrentRankColumn + ' <> 0)
			FROM #tmpGPROMeasureProgress p'
		EXEC (@Sql);

		--------------------------------------------------------------------------------
		-- Get the consecutive target for this measure
		--------------------------------------------------------------------------------
		-- find the highest rank needed for all BUs
		-- get the count of all non-skipped patients in this BU that is below the highest rank 
		SET @Sql = 'UPDATE p
			SET p.ConsecutiveTarget = p.ConsecutiveTarget + (SELECT COUNT(gpr.GproPatientRankingId) 
												FROM GproPatientRanking gpr 
													INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId 
												WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.' + @CurrentRankColumn + ' <> 0
												AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GpropatientRankingId = gpr.GproPatientRankingId)
												AND gpr.' + @CurrentRankColumn + ' <= (SELECT MAX(s.' + @CurrentRankColumn + ') FROM 
													(SELECT TOP 248 gpr2.' + @CurrentRankColumn + ' 
													FROM GproPatientRanking gpr2
														INNER JOIN #groupTinPatientList gtpl ON gtpl.GproPatientRankingId = gpr2.GproPatientRankingId
													WHERE gpr2.' + @CurrentRankColumn + ' IS NOT NULL AND gpr2.' + @CurrentRankColumn + ' <> 0
														AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GpropatientRankingId = gpr2.GproPatientRankingId)
													ORDER BY gpr2.' + @CurrentRankColumn + ') s))
			FROM #tmpGPROMeasureProgress p'
		EXEC (@Sql);


		--------------------------------------------------------------------------------
		-- Get the consecutively complete for this measure
		--------------------------------------------------------------------------------
		-- find the first incomplete for this BU
		-- get the count of all non-skipped patients in this BU below the first incomplete rank
		SET @Sql = 'UPDATE p
			SET p.ConsecutivelyComplete = p.ConsecutivelyComplete + (SELECT COUNT(gpr.GproPatientRankingId) 
												 FROM GproPatientRanking gpr 
													INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId 
													INNER JOIN #CompletePatients cp ON cp.GproPatientRankingId = gpr.GproPatientRankingId
												WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.' + @CurrentRankColumn + ' <> 0
													AND gpr.' + @CurrentRankColumn + ' < (SELECT MIN(ip.MeasureRank) FROM #IncompletePatients ip INNER JOIN #patientList pl ON pl.GproPatientRankingId = ip.GproPatientRankingId))
			FROM #tmpGPROMeasureProgress p'
		EXEC (@Sql);

		--------------------------------------------------------------------------------
		-- Get the remaining to target for this measure
		--------------------------------------------------------------------------------
		SET @Sql = 'UPDATE p
			SET p.RemainingToTarget = p.RemainingToTarget + (SELECT COUNT(gpr.GproPatientRankingId) 
												 FROM GproPatientRanking gpr 
													INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId 
													INNER JOIN #IncompletePatients ip ON ip.GproPatientRankingId = gpr.GproPatientRankingId
												WHERE gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.' + @CurrentRankColumn + ' <> 0
													AND gpr.' + @CurrentRankColumn + ' <= (SELECT MAX(s.' + @CurrentRankColumn + ') FROM 
													(SELECT TOP 248 gpr2.' + @CurrentRankColumn + ' 
													FROM GproPatientRanking gpr2
														INNER JOIN #groupTinPatientList gtpl ON gtpl.GproPatientRankingId = gpr2.GproPatientRankingId
													WHERE gpr2.' + @CurrentRankColumn + ' IS NOT NULL AND gpr2.' + @CurrentRankColumn + ' <> 0
														AND NOT EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GpropatientRankingId = gpr2.GproPatientRankingId)
													ORDER BY gpr2.' + @CurrentRankColumn + ') s))
			FROM #tmpGPROMeasureProgress p'
		EXEC (@Sql);

		SET @CurrentMeasureId = @CurrentMeasureId - 1;
	END

	--------------------------------------------------------------------------------
	-- Return the result row with the totals
	--------------------------------------------------------------------------------
	SELECT TotalRanked,TotalSkipped,TotalComplete,TotalIncomplete,ConsecutiveTarget,ConsecutivelyComplete,RemainingToTarget FROM #tmpGPROMeasureProgress;
END

GO
