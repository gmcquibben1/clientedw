SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--pick patient contracts and status to run, then run process for each contract and patient status
--select distinct ContractName, Patient_Status FROM OrgHierarchy
--where Patient_Status not like '%Deceased%'


CREATE PROCEDURE [dbo].[JH_AGG_DATA_LOAD_BY_CONTRACT] 
(@ContractName varchar(100),
 @PatientStatus varchar(100)
)

AS
BEGIN

DECLARE @USECOST VARCHAR(10) = 'YES'

TRUNCATE TABLE JHACG_Metrics

--SELECT BENE_HIC_NUM as patient_id,[Title],[Value] 
--INTO JHACG_TEMP 
--FROM MemMonth_Metrics
----pivot (sum([Value]) for [Title] in ([Pharmacy Cost],[Total Cost w/ Pharmacy],[Inpatient Admits],[ER Visits])) as JHACG_Metric
--WHERE DOS_First > DATEADD(mm,-16,GETUTCDATE()) and DOS_First < DATEADD(mm,-4,GETUTCDATE())
-- 

INSERT INTO JHACG_Metrics (
   patient_id
  ,pharmacy_cost
  ,total_cost
  ,inpatient_hospitalization_count
  ,emergency_visit_count
)
--) 
--SELECT * FROM JHACG_TEMP
--pivot (sum([Value]) for [Title] in ([Pharmacy Cost],[Total Cost w/ Pharmacy]
--,[Inpatient Admits],[ER Visits])) as JHACG_Metric
----WHERE DOS_First > DATEADD(mm,-16,GETUTCDATE()) and DOS_First > DATEADD(mm,-4,GETUTCDATE())


SELECT 
  P.LbPatientId AS [LbPatientId], 
  [Pharmacy Cost], 
  CASE 
    WHEN @USECOST = 'YES' then [12 Mo Cost]
    else 0
  end
  ,[IP Admits]
  ,[ER Visits] 
  FROM Patient_Summary_Pivot P
  JOIN OrgHierarchy O 
  ON P.LbPatientId = O.LbPatientId 
  AND O.ContractName = @ContractName 
  AND O.Patient_Status = @PatientStatus
  AND O.Patient_Status NOT LIKE '%Deceased%'
WHERE P.LbPatientId IS NOT NULL
 

--DROP TABLE JHACG_TEMP
-- 

/* SELECT JH ACG OUTPUT FILE INFO */
TRUNCATE TABLE JHACG_PATIENT

INSERT INTO JHACG_PATIENT (
   patient_id
  ,age
  ,sex
  ,line_of_business
  ,company
  ,Product
  ,benefit_plan
  ,health_system
  ,pharmacy_cost
  ,total_cost
  ,inpatient_hospitalization_count
  ,emergency_visit_count
) 

--select * from MemberExtract where providername like '%land%'
SELECT DISTINCT
 O.LbPatientId as patient_id --patient identifier
,DateDiff(YEAR,O.birthDate,GetUTCDate()) AS age
,LTRIM(RTRIM(o.genderid))
,'Risk Profile' as line_of_business 
,'Lightbeam' as company
,'Risk Profile' as Product
,'Patient Risk Plan' as benefit_plan
,'Medicare' as health_system
,pharmacy_cost
,total_cost
,PSP.[IP Admits]
,PSP.[ER Visits]
--,inpatient_hospitalization_count
--,emergency_visit_count
FROM OrgHierarchy O
JOIN JHACG_Metrics
ON O.LbPatientId = JHACG_Metrics.patient_id
JOIN Patient_Summary_Pivot PSP 
ON O.LbPatientId = PSP.LbPatientId  
AND O.ContractName = @ContractName 
AND O.Patient_Status = @PatientStatus
  AND O.Patient_Status NOT LIKE '%Deceased%'
--  and  o.providernpi = '1609810233'
--WHERE holdback IS NULL 
WHERE O.LbPatientId IS NOT NULL


--MAKE CLAIMS BASED DATA
TRUNCATE TABLE JHACG_MEDICAL
 

--THE NEW TWO BLOCKS ARE POPULATING DATA IN TEMP TABLES, WHICH WE ARE USING TO JOIN AND INSERT DATA IN JHACG_MEDICAL TABLE
SELECT DISTINCT 
PD.PatientId,
DC.DiagnosisCode, 
CONVERT(VARCHAR(10), PD.[DiagnosisDateTime], 121) AS SERVICE_BEGIN_DATE, 
CONVERT(VARCHAR(10), PD.[DiagnosisDateTime], 121) AS SERVICE_END_DATE, 
PD.[EncounterID]
INTO #FromDiag
FROM PatientDiagnosis PD
INNER JOIN PatientDiagnosisDiagnosisCode PDDC 
ON PD.PatientDiagnosisId = PDDC.PatientDiagnosisId
INNER JOIN DiagnosisCode DC
ON PDDC.DiagnosisCodeId = DC.DiagnosisCodeId
JOIN OrgHierarchy O 
  ON PD.PatientId = O.LbPatientId 
  AND O.ContractName = @ContractName 
  AND O.Patient_Status = @PatientStatus
  AND O.Patient_Status NOT LIKE '%Deceased%'
  --AND ID.IdTypeDesc = 'BENE_HIC_NUM' 
  AND CONVERT(VARCHAR(10), PD.[DiagnosisDateTime], 121) > DATEADD(MM,-16,GETUTCDATE())
  AND CONVERT(VARCHAR(10), PD.[DiagnosisDateTime], 121) < DATEADD(MM,-4,GETUTCDATE())
WHERE O.LbPatientId IS NOT NULL

--insert from chronic conditions table if exists
IF OBJECT_ID('[PatientChronicCondition]') IS NOT NULL
   BEGIN
INSERT INTO #FromDiag  
SELECT DISTINCT 
PCC.PatientId, 
DC.DiagnosisCode, 
CONVERT(VARCHAR(10), DATEADD(MM,-5,GETUTCDATE()), 121) AS SERVICE_BEGIN_DATE, 
CONVERT(VARCHAR(10), DATEADD(MM,-5,GETUTCDATE()), 121) AS SERVICE_END_DATE, 
PCC.PatientChronicConditionId as [EncounterID]
FROM PatientChronicCondition PCC
JOIN DiagnosisCode DC 
ON PCC.DiagnosisCodeId = DC.DiagnosisCodeId
JOIN OrgHierarchy O 
  ON PCC.PatientId = O.LbPatientId 
  AND O.ContractName = @ContractName 
  AND O.Patient_Status = @PatientStatus
  AND O.Patient_Status NOT LIKE '%Deceased%'
  --AND ID.IdTypeDesc = 'BENE_HIC_NUM' 
WHERE O.LbPatientId IS NOT NULL
END

SELECT DISTINCT 
PP.PatientID, 
PC.ProcedureCode, 
CONVERT(VARCHAR(10), PP.ProcedureDateTime, 121) AS SERVICE_BEGIN_DATE, 
CONVERT(VARCHAR(10), PP.ProcedureDateTime, 121) AS SERVICE_END_DATE, 
PP.[EncounterID]
INTO #FromProc
FROM PatientProcedure PP
INNER JOIN PatientProcedureProcedureCode PPC
ON PP.PatientProcedureId = PPC.PatientProcedureId
INNER JOIN ProcedureCode PC 
ON PPC.ProcedureCodeId = PC.[ProcedureCodeId]
JOIN OrgHierarchy O 
  ON pp.PatientID = O.LbPatientId 
  AND O.ContractName = @ContractName 
  AND O.Patient_Status = @PatientStatus
  AND O.Patient_Status NOT LIKE '%Deceased%'
  --AND ID.IdTypeDesc = 'BENE_HIC_NUM' 
--WHERE ID.IdTypeDesc = 'BENE_HIC_NUM' 
AND CONVERT(VARCHAR(10), PP.ProcedureDateTime, 121) > DATEADD(MM,-16,GETUTCDATE())
AND CONVERT(VARCHAR(10), PP.ProcedureDateTime, 121) < DATEADD(MM,-4,GETUTCDATE())
WHERE O.LbPatientId IS NOT NULL

;WITH CTE1 
(PatientId, 
DX_Version_1, 
DiagnosisCode, 
SERVICE_BEGIN_DATE, 
SERVICE_END_DATE, 
SERVICE_PLACE, 
REVENUE_CODE, 
ProcedureCode, 
[EncounterID])
AS
(
SELECT DISTINCT 
P.PatientId AS PatientId, 
9 AS DX_Version_1, 
D.DiagnosisCode, 
D.SERVICE_BEGIN_DATE, 
D.SERVICE_END_DATE, 
NULL AS 'SERVICE_PLACE', 
NULL AS 'REVENUE_CODE', 
P.ProcedureCode, 
D.[EncounterID]
FROM #FromDiag D
LEFT JOIN 
(SELECT PatientID, ProcedureCode, SERVICE_BEGIN_DATE, SERVICE_END_DATE, [EncounterID] FROM #FromProc) P
ON (D.PatientId = P.PatientID AND D.SERVICE_BEGIN_DATE = P.SERVICE_BEGIN_DATE) --AND D.[EncounterID] = P.[EncounterID])
--WHERE P.ProcedureCode IS NOT NULL
)

INSERT INTO JHACG_MEDICAL (
   patient_id
  ,dx_version_1
  ,dx_cd_1
  ,service_begin_date
  ,service_end_date
  ,service_place
  ,revenue_code
  ,procedure_code
)
SELECT 
PatientId, 
DX_Version_1, 
DiagnosisCode, 
SERVICE_BEGIN_DATE, 
SERVICE_END_DATE, 
SERVICE_PLACE, 
REVENUE_CODE, 
ProcedureCode FROM CTE1;

/* GENERATE PHARMACY DATA */
TRUNCATE TABLE JHACG_PHARMACY
  

INSERT INTO JHACG_PHARMACY (
   patient_id
  ,rx_fill_date
  ,rx_code
  ,rx_code_type
  ,rx_days_supply
) 
SELECT DISTINCT 
PM.PatientID, 
PM.[MedicationStartDate], 
CASE WHEN M.NDCCode <> '' THEN M.NDCCode ELSE M.RxNormCode END, 
'N' as rx_code_type--, PM.[Quantity]
,CASE WHEN TRY_CONVERT(FLOAT,PM.[Quantity]) IS NOT NULL THEN ROUND(PM.[Quantity], 0) ELSE 0 END AS MedicationQuantity
FROM PatientMedication  PM
INNER JOIN Medication M
ON PM.MedicationID = M.MedicationID
JOIN OrgHierarchy O 
  ON PM.PatientID = O.LbPatientId 
  AND O.ContractName = @ContractName 
  AND O.Patient_Status = @PatientStatus
  AND O.Patient_Status NOT LIKE '%Deceased%'
  --AND ID.IdTypeDesc = 'BENE_HIC_NUM' 
--WHERE ID.IdTypeDesc = 'BENE_HIC_NUM' 
WHERE  PM.[Quantity] IS NOT NULL
AND PM.[MedicationStartDate]  > DATEADD(MM,-16,GETUTCDATE())
AND PM.[MedicationEndDate]    < DATEADD(MM,-4,GETUTCDATE())
AND CASE WHEN TRY_CONVERT(FLOAT,PM.[Quantity]) IS NOT NULL THEN ROUND(PM.[Quantity], 0) ELSE 0 END < 1000000
AND O.LbPatientId IS NOT NULL


--SELECT min(service_begin_date) AS BEG_OBS, MAX(service_begin_date) AS END_OBS FROM JHACG_MEDICAL

--DROPPING THE TEMP TABLES
DROP TABLE #FromDiag
DROP TABLE #FromProc

END
GO
