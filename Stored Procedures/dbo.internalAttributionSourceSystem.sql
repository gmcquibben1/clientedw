SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalAttributionSourceSystem]
AS
DECLARE @AttributionTypeId int = 12
DECLARE @SourceSystemId int = 1 --setting as Internal Calculated value


INSERT INTO vwPortal_PatientProvider (
   PatientID
  ,ProviderID
  ,SourceSystemID
  ,ExternalReferenceIdentifier
  --,OtherReferenceIdentifier
  ,DeleteInd
  ,CreateDateTime
  ,ModifyDateTime
  ,CreateLBUserId
  ,ModifyLBUserId
  ,ProviderRoleTypeId
  ,OverrideInd
  ,AttributionTypeId
  ,AttributedProviderInd
) 
SELECT distinct IDR.LbPatientId, PPA.ProviderID, @SourceSystemId AS SourceSystemId, BU.ExternalCode, --IDR.ExternalId,
         0, getdate(), getdate(), 1 CreateLBUserId,1 ModifyLBUserId,0 ProviderRoleTypeId,0 OverrideInd
         ,@AttributionTypeId AttributionTypeId,0 AttributedProviderInd
FROM PatientIdReference IDR
JOIN vwPortal_BusinessUnitSourceSystem BUSS
      ON IDR.SourceSystemId = BUSS.SourceSystemId 
JOIN vwPortal_BusinessUnit BU 
      ON BUSS.BusinessUnitId = BU.BusinessUnitId 
      AND BU.DeleteInd = 0 
JOIN vwPortal_ProviderActive PPA 
      ON BU.ExternalCode = PPA.NationalProviderIdentifier 
      AND PPA.DeleteInd = 0
JOIN vwPortal_Patient PP 
      ON IDR.LbPatientId = PP.PatientId 
      and PP.DeleteInd = 0
    WHERE IDR.DeleteInd = 0
GO
