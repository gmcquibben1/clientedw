SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FaceSheetLabResultLoad] 
	@PatientId INTEGER
AS
/*===============================================================
	CREATED BY: 	Brian Russell
	CREATED ON:		2014-09-18
	INITIAL VER:	
	MODULE:			FaceSheet 		
	DESCRIPTION:	Stored Procedure used to return labs for the POC tool facesheet
	PARAMETERS:		@PatientId - id of patient

	RETURN VALUE(s)/OUTPUT:
	PROGRAMMIN NOTES: 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	Version		Date		Author	Change
	-------		----------	------	------
	1.0 		2014-09-18  BR		Initial Version
	1.1			2014-10-12	BR		Changed the length of values to prevent truncation error
	1.2			2014-10-14	BR		Removed some search codes from HbA1C and HDL so only the valid results show up
	1.3			2014-10-15	BR		Removed 2 more search codes from HbA1c so it never shows up in g/DL
	1.4			2014-10-16	BR		Added new code for LDL 
	1.5			2014-10-29	BR		Removed some Microabl/Cr codes to prevent false negatives
	1.6			2015-05-11  BR		Adding in LOINC codes in addition to Labcorp codes
	2.1.1		2016-11-21  UC		Using local parameters for PatientId and Top counts to get around SQL optimizer picking the wrong execution plan

=================================================================*/
BEGIN
	DECLARE @PatientId2 INT = @PatientId
	DECLARE @TopCount INT = 1

	DECLARE  @PatientLabResult TABLE 
	(
		TestName		VARCHAR(100),
		ResultValue		VARCHAR(50) NULL,
		ResultUnits		VARCHAR(60) NULL,
		ResultDate		DATETIME	NULL,
		ReturnFlag		VARCHAR(10) NULL
	)	
	------------- LDL -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('012054', '011916', '012054', 'LDL', '25016900') 
			OR plr.ObservationCode1 IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'LDL-C Tests')
			OR plr.ObservationCode2 IN ('012054', '011916', '012054', 'LDL', '25016900')
			OR plr.ObservationCode2 IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'LDL-C Tests')) 
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'LDL' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50), plr.Units , plr.ObservationDate ,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('012054', '011916', '012054', 'LDL', '25016900') 
				OR plr.ObservationCode1 IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'LDL-C Tests')
				OR plr.ObservationCode2 IN ('012054', '011916', '012054', 'LDL', '25016900')
				OR plr.ObservationCode2 IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'LDL-C Tests')) 
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('LDL');
	END
	------------- HDL -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('011817', '25015900', 'HDL', '2085-9') 
			OR plr.ObservationCode2 IN ('011817', '25015900', 'HDL', '2085-9')) 
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'HDL' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50) AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('011817', '25015900', 'HDL', '2085-9','43729-3') 
				OR plr.ObservationCode2 IN ('011817', '25015900', 'HDL', '2085-9','43729-3')) 
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('HDL');
	END
	------------- Chol -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('CHOL', '001065', '25003000', '001065', '2093-3') 
			OR plr.ObservationCode2 IN ('CHOL', '001065', '25003000', '001065', '2093-3')) 
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'Chol' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50) AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('CHOL', '001065', '25003000', '001065', '2093-3') 
				OR plr.ObservationCode2 IN ('CHOL', '001065', '25003000', '001065', '2093-3')) 
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('Chol');
	END
	------------- Trig -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('001172', '25002900', '001172', 'TRIG', '884256', '804563', '884256','2571-8') 
			OR plr.ObservationCode2 IN ('001172', '25002900', '001172', 'TRIG', '884256', '804563', '884256','2571-8')) 
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'Trig' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50) AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('001172', '25002900', '001172', 'TRIG', '884256', '804563', '884256','2571-8') 
				OR plr.ObservationCode2 IN ('001172', '25002900', '001172', 'TRIG', '884256', '804563', '884256','2571-8')) 
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('Trig');
	END
	------------- Gluc -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('001032', '25000000', 'GLU', '001818', '25014900', '1558-6','1504-0','14749-6') 
			OR plr.ObservationCode2 IN ('001032', '25000000', 'GLU', '001818', '25014900', '1558-6','1504-0','14749-6')) 
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'Gluc' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50) AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('001032', '25000000', 'GLU', '001818', '25014900', '1558-6','1504-0','14749-6') 
				OR plr.ObservationCode2 IN ('001032', '25000000', 'GLU', '001818', '25014900', '1558-6','1504-0','14749-6')) 
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('Gluc');
	END
	------------- HbA1c -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('001464','50026400','001458','001463','GLYHGB','50062100','50062300','50053920','A1C3','17856-6','4548-4','4549-2','59261-8','62388-4','71875-9') 
			OR plr.ObservationCode1 IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'HbA1c Tests')
			OR plr.ObservationCode2 IN ('001464','50026400','001458','001463','GLYHGB','50062100','50062300','50053920','A1C3','17856-6','4548-4','4549-2','59261-8','62388-4','71875-9')
			OR plr.ObservationCode2 IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'HbA1c Tests'))
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'HbA1c' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50) AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('001464','50026400','001458','001463','GLYHGB','50062100','50062300','50053920','A1C3','17856-6','4548-4','4549-2','59261-8','62388-4','71875-9') 
				OR plr.ObservationCode1 IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'HbA1c Tests')
				OR plr.ObservationCode2 IN ('001464','50026400','001458','001463','GLYHGB','50062100','50062300','50053920','A1C3','17856-6','4548-4','4549-2','59261-8','62388-4','71875-9')
				OR plr.ObservationCode2 IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'HbA1c Tests'))
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('HbA1c');
	END
	------------- MicroAlb/Cr -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('001081', '140288', '50060710', '50061100', '001488', '140286','9318-7','59159-4','30001-2','30000-4','44292-1') 
			OR plr.ObservationCode2 IN  ('001081', '140288', '50060710', '50061100', '001488', '140286','9318-7','59159-4','30001-2','30000-4','44292-1'))
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'MicroAlb/Cr' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50) AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('001081', '140288', '50060710', '50061100', '001488', '140286','9318-7','59159-4','30001-2','30000-4','44292-1') 
				OR plr.ObservationCode2 IN  ('001081', '140288', '50060710', '50061100', '001488', '140286','9318-7','59159-4','30001-2','30000-4','44292-1'))
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('MicroAlb/Cr');
	END
	------------- CrClearance -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('013029', '25026000', '013032', '133540','12195-4','13441-1','13442-9','13443-7','13446-0','13447-8','13449-4','13450-2',
			'2163-4','2164-2','26752-6','33558-8','35591-7','35592-5','35593-3','35594-1','50381-3','50380-5') 
			OR plr.ObservationCode2 IN ('013029', '25026000', '013032', '133540','12195-4','13441-1','13442-9','13443-7','13446-0','13447-8','13449-4','13450-2',
			'2163-4','2164-2','26752-6','33558-8','35591-7','35592-5','35593-3','35594-1','50381-3','50380-5'))
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'CrClearance' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50) AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('013029', '25026000', '013032', '133540','12195-4','13441-1','13442-9','13443-7','13446-0','13447-8','13449-4','13450-2',
				'2163-4','2164-2','26752-6','33558-8','35591-7','35592-5','35593-3','35594-1','50381-3','50380-5') 
				OR plr.ObservationCode2 IN ('013029', '25026000', '013032', '133540','12195-4','13441-1','13442-9','13443-7','13446-0','13447-8','13449-4','13450-2',
				'2163-4','2164-2','26752-6','33558-8','35591-7','35592-5','35593-3','35594-1','50381-3','50380-5'))
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('CrClearance');
	END
	------------- PSA -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('55064920', 'PSA', '480782', '480799', '480737', '480781', '480762', '480853', '55064931', '55064910', '55064921', '010334', '010327', '010326','2857-1') 
			OR plr.ObservationCode2 IN ('55064920', 'PSA', '480782', '480799', '480737', '480781', '480762', '480853', '55064931', '55064910', '55064921', '010334', '010327', '010326','2857-1'))
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'PSA' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50) AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('55064920', 'PSA', '480782', '480799', '480737', '480781', '480762', '480853', '55064931', '55064910', '55064921', '010334', '010327', '010326','2857-1') 
				OR plr.ObservationCode2 IN ('55064920', 'PSA', '480782', '480799', '480737', '480781', '480762', '480853', '55064931', '55064910', '55064921', '010334', '010327', '010326','2857-1'))
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('PSA');
	END
	------------- TSH -------------
	IF EXISTS (
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
		FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId2
		AND (plr.ObservationCode1 IN ('RTH', '004264', '55080400', '55080400', '004259', 'TSH', 'RTH*','3016-3') 
			OR plr.ObservationCode2 IN ('RTH', '004264', '55080400', '55080400', '004259', 'TSH', 'RTH*','3016-3'))
		)
	BEGIN
		INSERT INTO @PatientLabResult (TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag)
		SELECT TOP (@TopCount)
		--SELECT TOP 1
			'TSH' AS TestName,
			SUBSTRING(RTRIM(plr.Value), 1, 50) AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate,
			plr.AbnormalFlag AS ReturnFlag
			FROM PatientLabResult plr WITH (NOLOCK) INNER JOIN PatientLabOrder plo WITH (NOLOCK) ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId2
			AND (plr.ObservationCode1 IN ('RTH', '004264', '55080400', '55080400', '004259', 'TSH', 'RTH*','3016-3') 
				OR plr.ObservationCode2 IN ('RTH', '004264', '55080400', '55080400', '004259', 'TSH', 'RTH*','3016-3'))
			ORDER BY plr.ObservationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @PatientLabResult (TestName)
		VALUES ('TSH');
	END

	SELECT TestName, ResultValue, ResultUnits, ResultDate, ReturnFlag FROM @PatientLabResult;
END
GO
