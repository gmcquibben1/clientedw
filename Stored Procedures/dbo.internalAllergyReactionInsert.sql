SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalAllergyReactionInsert] 
    @AllergyReactionTypeId INT OUTPUT ,
	@AllergyReaction VARCHAR(255)  = NULL
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @DiagnosisCodeId INT

	IF @AllergyReaction IS NOT NULL 
	BEGIN

		BEGIN TRAN

		SELECT @AllergyReactionTypeId = AllergyReactionTypeId FROM AllergyReactionType
		WHERE Name = @AllergyReaction

		IF @AllergyReactionTypeId IS NULL
		BEGIN
			INSERT INTO [dbo].AllergyReactionType
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
			VALUES 
			(
				@AllergyReaction,
				@AllergyReaction,
				@DeleteInd,
				GetDate(),
				GetDate(),
				@LBUserId,
				@LBUserId
			)
			SELECT @intError = @@ERROR,
				@AllergyReactionTypeId = SCOPE_IDENTITY()
		END

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
		END
		ELSE
		BEGIN
			COMMIT TRAN
		END

	END
		
	RETURN @intError



GO
