SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@HF-6] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID(N'tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];

	DECLARE  @yearend date, @yearBegin date;

	SET @yearBegin = N'01/01/2015';
	SET @yearend   = N'01/01/2016';

	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END


	--Initial Patient pooling, for patients that have ranking for the BCS measure. 
	SELECT r.LbPatientId,
        r.GProPatientRankingId,
		dateDiff(year,r.Birthdate,@yearBegin) as Age,
		r.Birthdate,
        GM.GproMeasureTypeID,
		r.GenderCode,
        HFConfirmed,
        HFLVSD, 
        HFBetaBlocker   
	INTO #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTypeID 
			ANd GM.Name = 'HF-6'
		JOIN GproPatientMeasure GPM ON GPM.GproPatientRankingId = r.GproPatientRankingID
	WHERE r.HfRank IS NOT NULL
		AND (GPM.HFConfirmed IS NULL OR GPM.HfLvsd IS NULL OR GPM.HfBetaBlocker IS NULL)
   
	--------------------------------------------------------------------------------------------------------    
		-- Age Exclusion: 
	--------------------------------------------------------------------------------------------------------   		    
	--Age Check
	UPDATE rp
	SET rp.[HFConfirmed] = N'19'
	FROM #RankedPatients rp
	WHERE (Age < 18)     

	--------------------------------------------------------------------------------------------------------    
		-- Denominator: Active Heart Failure, or LVSD 
	--------------------------------------------------------------------------------------------------------
      
	IF OBJECT_ID( N'tempdb..#HF' ) IS NOT NULL 
		DROP TABLE #HF;
      
    -- Active Existing conditions from before the Measurement period.
	SELECT RP.*
	INTO #HF
	FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = RP.LbPatientId
			AND pd.DiagnosisDateTime < @yearBegin
			AND (pd.ActiveInd <> 0 OR (pd.DiagnosisResolvedDate IS NULL OR pd.DiagnosisResolvedDate > @yearBegin))
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = N'HF'
			AND gec.[ModuleIndicatorGPRO] = N'HF Confirm'
			AND gec.[VariableName] = N'DX_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pd.SourceSystemId
	WHERE RP.HfConfirmed IS NULL

      --Patients with a current or Prior LVEF < 40%
	IF OBJECT_ID( 'tempdb..#Denom' ) IS NOT NULL 
		DROP TABLE #Denom;
            
	--LVEF by SNOMED
	SELECT HF.* 
	INTO #Denom
	FROM #HF HF
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = HF.LbPatientId
			AND pd.DiagnosisDateTime < @yearBegin
			AND (pd.ActiveInd <> 0 OR (pd.DiagnosisResolvedDate IS NULL OR pd.DiagnosisResolvedDate > @yearBegin))
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = N'HF'
			AND gec.[ModuleIndicatorGPRO] = N'6' 
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemid = pd.SourceSystemId

		--JOIN [PatientDiagnosis] pd ON HF.[LbPatientId] = PD.PATIENTID
		--JOIN [dbo].[PatientDiagnosisDiagnosisCode] pddc ON  pddc.[PatientDiagnosisId] = pd.[PatientDiagnosisId]
		--JOIN [dbo].[DiagnosisCode] dc ON  dc.[DiagnosisCodeID] = pddc.[DiagnosisCodeID]
		--JOIN [dbo].[GproEvaluationCode] GEC ON  GEC.[Code] = dc.[DiagnosisCodeRaw]
		--	AND GEC.[ModuleType]   = N'HF'
		--	AND GEC.[ModuleIndicatorGPRO] = N'6'
		--JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID]   --DIAGS
      
      
    --LVEF by LOINC      
	INSERT INTO #Denom
	SELECT HF.*
	FROM  #HF HF
		JOIN [dbo].[PatientLabOrder] plo ON  plo.[PatientID] = HF.[LbPatientId] 
		JOIN [dbo].[PatientLabResult] plr ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
			AND ISNULL( Try_Convert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 ) <= 40.0
			AND plr.ObservationDate < @yearEnd
		JOIN  GproEvaluationCode gec ON (gec.Code = plr.ObservationCode1 OR gec.Code = plr.ObservationCode2)
			AND gec.[ModuleType]   = N'HF'
			AND gec.[ModuleIndicatorGPRO] = N'6' 
		JOIN @SourceIdTable SS ON SS.SourceSystemId = PLO.SourceSystemID    --MEDS
       
        
      -- Setting patients with out the requisite diagnosis to 8, as per specs.   
--      UPDATE #RankedPatients
--      SET    HFConfirmed = '8'
--      FROM   #RankedPatients RP
--      LEFT JOIN   #Denom D
--          ON RP.LbPatientId = D.LbPatientId
--      WHERE D.LbPatientId IS NULL
      
    -- setting patients to confirmed if they were of age and the above criteria.
	UPDATE RP
	SET    RP.HfConfirmed = '2'
	FROM   #RankedPatients RP
	JOIN   #Denom D
		ON RP.LbPatientId = D.LbPatientId
	WHERE RP.HFConfirmed IS NULL

	 --------------------------------------------------------------------------------------------------------          
	   --DENOMINATOR Exceptions
	   --Documented (Medical, System, or Patient) reasons for not prescribing beta-blocker therapy.
	--------------------------------------------------------------------------------------------------------      
     
	IF OBJECT_ID( 'tempdb..#Exclusions' ) IS NOT NULL 
		DROP TABLE #Exclusions;
  
     
	--Checking Diagnosis SNMs first for exclusions.
	SELECT DISTINCT 
		D.*,
		ExclusionReason = (CASE WHEN GEEC.[VariableName] = 'PATIENT_REASON' THEN GEEC.[VariableName]
								WHEN GEEC.[VariableName] = 'SYSTEM_REASON'  THEN GEEC.[VariableName]
								ELSE 'Medical_Reason'
							END)
	INTO #Exclusions
	FROM #Denom D
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = D.LbPatientId
			AND pd.DiagnosisDateTime > @YearEnd
			ANd pd.DiagnosisDateTime <= @YearBegin
		INNER JOIN GproExclusionExceptionCode geec ON (geec.Code = pd.DiagnosisCode OR geec.Code = pd.DiagnosisCodeRaw OR geec.Code = pd.DiagnosisCodeDisplay)
			AND geec.[ModuleType]   = N'HF'
			AND geec.[ModuleIndicatorGPRO] = N'6'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pd.SourceSystemId

	--	JOIN [PatientDiagnosis] pd ON D.[LbPatientId] = pd.patientid
	--	JOIN [dbo].[PatientDiagnosisDiagnosisCode] pddc ON  pddc.[PatientDiagnosisId] = pd.[PatientDiagnosisId]
	--	JOIN [dbo].[DiagnosisCode] dc ON  dc.[DiagnosisCodeID] = pddc.[DiagnosisCodeID]
	--	JOIN [dbo].[GproExclusionExceptionCode] GEEC ON  GEEC.[Code] = dc.[DiagnosisCodeRaw]
	--		AND GEEC.[ModuleType]   = N'HF'
	--		AND GEEC.[ModuleIndicatorGPRO] = N'6'
	--	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID]   --DIAGS
	--WHERE PD.DiagnosisDateTime > @YearEnd
	--	AND PD.DiagnosisDateTime <= @YearBegin
      
      
	--Checking Procedure SNMs for exclusions.
	INSERT INTO #Exclusions
	SELECT DISTINCT D.*,
					ExclusionReason = (CASE WHEN GEEC.[VariableName] = 'PATIENT_REASON' THEN GEEC.[VariableName]
											WHEN GEEC.[VariableName] = 'SYSTEM_REASON'  THEN GEEC.[VariableName]
											ELSE 'Medical_Reason'
										END)
	FROM  #Denom D
		JOIN [dbo].[PatientProcedure] pp ON  pp.[PatientID] = D.[LbPatientId]
		JOIN [dbo].[PatientProcedureProcedureCode] pppc ON pppc.[PatientProcedureId] = pp.[PatientProcedureId]
		JOIN [dbo].[ProcedureCode] pc ON  pc.[ProcedureCodeID] = pppc.[ProcedureCodeID]
		JOIN [dbo].[GproExclusionExceptionCode] GEEC ON  GEEC.[Code] = PC.[ProcedureCode]
			AND GEEC.[ModuleType]   = N'HF'
			AND GEEC.[ModuleIndicatorGPRO] = N'6'
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
	WHERE PP.ProcedureDateTime > @YearEnd
		AND   PP.ProcedureDateTime <= @YearBegin
      
    --Medical Reason Heart Rate Code check, LOINC
    INSERT INTO #Exclusions
    SELECT DISTINCT D.*,
                    ExclusionReason = 'Medical_Reason'
	FROM #Denom D
		JOIN [dbo].[PatientLabOrder] plo ON  plo.[PatientID] = D.[LbPatientId] -- Correlated subquery
		JOIN [dbo].[PatientLabResult] plr ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
		JOIN [dbo].[GproExclusionExceptionCode] GEEC ON  GEEC.[Code] = PLR.[ObservationCode1]
			AND GEEC.[ModuleType]   = N'HF'
			AND GEEC.[ModuleIndicatorGPRO] = N'6'
			AND GEEC.[VariableName] = 'HEART_RATE_CODE'
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID]    --PROCS    
    WHERE PLR.ObservationDate > @YearEnd
		AND PLR.ObservationDate <= @YearBegin
      
      
    UPDATE D
    SET HFBetaBlocker = CASE WHEN EXCLUSIONREASON = 'Medical_Reason' THEN '4'
                            WHEN EXCLUSIONREASON = 'PATIENT_REASON' THEN '5'
                            WHEN EXCLUSIONREASON = 'SYSTEM_REASON'  THEN '6'
                        END
    FROM #DENOM D
		JOIN #Exclusions EX ON D.LbPatientId = EX.LbPatientId
           
	--------------------------------------------------------------------------------------------------------          
	   --NUMERATOR 
	   --Patients who were prescribed a Beta-blocker therapy within a 12month period. 
	--------------------------------------------------------------------------------------------------------      
   
	IF OBJECT_ID( 'tempdb..#Num' ) IS NOT NULL 
		DROP TABLE #Num;
   
    -- BB Drugs
	SELECT DISTINCT D.*
	INTO  #NUM
	FROM  #DENOM D
		JOIN	[dbo].[PatientMedication] pm ON  pm.[PatientID] = D.[LbPatientId]
		JOIN  [dbo].[Medication] m ON m.[MedicationID] = pm.[MedicationID]
			AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @yearEnd
			AND IsNull( pm.[MedicationEndDate], DateAdd( DAY, -1,  @yearEnd ) ) > @yearBegin
		JOIN [DBO].[GproDrugCode] GDC ON  GDC.[Code] IN ( m.[NDCCode], m.[RxNormCode] )
			AND GDC.[VariableName] IN ( 'LVSD_BB_DRUG_CODE' )
			AND GDC.[ModuleType]   = N'HF'
			AND GDC.[ModuleIndicatorGPRO] = N'6' 
		JOIN @SourceIdTable SS ON SS.SourceSystemId = PM.SourceSystemID    --MEDS


	--Updating patients who have All the required categories.    
	UPDATE RP
	SET HFLVSD =  2, 
		HFBetaBlocker = 2
	FROM  #NUM N
		LEFT JOIN  #Exclusions EX ON N.LbPatientId = EX.LbPatientId
		JOIN  #RankedPatients RP ON N.LbPatientId = RP.LbPatientId
	WHERE EX.LbPatientId IS NULL
  
	--Updating patients who were excluded, but had the other stuff.   
	UPDATE RP
	SET HFLVSD =  2, 
		HFBetaBlocker = EX.HFBetablocker
	FROM #NUM N
		JOIN #Exclusions EX ON N.LbPatientId = EX.LbPatientId
		JOIN #RankedPatients RP ON N.LbPatientId = RP.LbPatientId
      
	--------------------------------------------------------------------------------------------------------          
	   -- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	UPDATE gpm
	SET gpm.HfConfirmed = gpr.HfConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.HfRank IS NOT NULL
		AND gpm.HfConfirmed IS NULL AND gpr.HfConfirmed IS NOT NULL

	UPDATE gpm
	SET gpm.[HFLVSD] = GPR.[HFLVSD], gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.HfRank IS NOT NULL
		AND gpm.HfLvsd IS NULL AND gpr.HfLvsd IS NOT NULL

	UPDATE gpm
	SET gpm.HFBetaBlocker = GPR.HFBetaBlocker, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.HfRank IS NOT NULL
		AND gpm.HFBetaBlocker IS NULL AND gpr.HFBetaBlocker IS NOT NULL



END

     
GO
