SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientInsuranceSave]
(
	  @PatientId int
	, @SourceSystemId int = NULL
	, @InsuranceName varchar(50)
	, @InsurancePriority int = 1
	, @StartDate date
	, @EndDate date = NULL
	, @PolicyNumber varchar(50)
	, @GroupNumber varchar(40)
	, @FinancialClassTypeId int
	, @LoggedinLbUserId int
	, @PatientInsuranceId int output
	, @ExistingPatientInsuranceId int
)
AS
BEGIN
	SET NOCOUNT ON;
	

	DECLARE @TimeStamp DateTime = SYSUTCDATETIME();	
	
	IF @ExistingPatientInsuranceId = 0
	BEGIN 

	IF @SourceSystemID IS NULL
		SET @SourceSystemId = (SELECT SourceSystemId FROM SourceSystem WHERE Name = 'Internal')

	DECLARE @DeleteInd bit = 0

		INSERT INTO dbo.PatientInsurance
		(
			 PatientID
			, InsuranceName
			, InsurancePriority
			, EffectiveDate
			, ExpirationDate
			, PolicyNumber
			, GroupNumber
			, FinancialClassTypeId
			, DeleteInd
			, SourceSystemId
			, CreateLBUserId
			, ModifyLBUserId
			, CreateDateTime
			, ModifyDateTime
		)
		VALUES
		(
			  @PatientID
			, @InsuranceName
			, @InsurancePriority
			, @StartDate
			, @EndDate
			, @PolicyNumber
			, @GroupNumber
			, @FinancialClassTypeId
			, @DeleteInd
			, @SourceSystemId
			, @LoggedinLbUserId
			, @LoggedinLbUserId
			, @TimeStamp
			, @TimeStamp
		);
				
		SET @PatientInsuranceId = SCOPE_IDENTITY();

		
	END
	ELSE
	BEGIN 
		SET @PatientInsuranceId = @ExistingPatientInsuranceId;

		UPDATE dbo.PatientInsurance
		SET
			InsuranceName = @InsuranceName
		  , InsurancePriority = @InsurancePriority
		  , EffectiveDate = @StartDate
		  , ExpirationDate = @EndDate
		  , PolicyNumber = @PolicyNumber
		  , GroupNumber = @GroupNumber
		  , FinancialClassTypeId = @FinancialClassTypeId
		  , ModifyLBUserId = @LoggedinLbUserId
		  , ModifyDateTime = @TimeStamp
		WHERE PatientInsuranceId = @PatientInsuranceId

	END
END


GO
