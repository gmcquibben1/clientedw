SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SystemImportMetricBuildOne] @SystemImportLogMetricTypeId int
WITH EXEC AS CALLER
AS
--DECLARE @SystemImportLogMetricTypeId INT = 3
declare @RowsAffected int
declare @SystemImportLogId int
declare @Eval1 varchar(500)
declare @Eval2 varchar(500)
declare @Eval3 varchar(500)
declare @Name varchar(50)
declare @DisplayValue nvarchar(50)
declare @metricValue varchar(50) = '123'

select top 1 @RowsAffected = B.RowsAffected, @SystemImportLogId = B.SystemImportLogId, @Eval1 = A.EvalExpression,
@Name = A.[Name], @DisplayValue = A.DisplayValue
FROM SystemImportLog B 
JOIN SystemImportLogMetricType A ON  B.SystemImportDataTypeId = A.SystemImportDataTypeId
WHERE A.SystemImportLogMetricTypeId = @SystemImportLogMetricTypeId
ORDER BY B.SystemImportLogId DESC

set @EVAL2 = REPLACE(@Eval1, '@SystemImportLogId', @SystemImportLogId)
set @EVAL3 = REPLACE(@EVAL2, '@RowsAffected', @RowsAffected)
declare @EVAL4 nvarchar(550) = 'select @metricValue = '+@EVAL3
EXEC sp_executesql @EVAL4, N'@metricValue varchar(max) out', @metricValue out

SELECT @Eval1, @Eval2, @Eval3, @EVAL4, @RowsAffected, @SystemImportLogId, @SystemImportLogMetricTypeId, @DisplayValue, @Name, @metricValue
    
    SELECT 
     @SystemImportLogId   AS SystemImportLogId       -- SystemImportLogId - int
    ,'RowsInserted'  AS [Name]                  -- Name - varchar(50)
    ,'Records Inserted'  AS DisplayValue            -- DisplayValue - varchar(50)
    ,@metricValue  AS [Value]                 -- Value - varchar(50)
    ,user AS SqlUserName            -- SqlUserName - varchar(100)
    ,1 AS CreateLbUserId         -- CreateLbUserId - int
    ,getdate() AS CreateDateTime    -- CreateDateTime - datetime
GO
