SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientImmunizationLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 

AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		pi.PatientId, 
		pi.SourceSystemId,
		pi.ServiceDate,
		pi.ImmunizationCode,
		pi.ImmunizationCodingSystem,
		pi.ImmunizationDescription,
		pi.ImmunizationDose,
		pi.ImmunizationUnits,
		pi.MaterialLotNumber,
		pi.MaterialManufacturer,
		pi.MedicationRouteType,
		pi.PerformedByClinician
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientImmunization pi ON pi.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
		AND (pi.CreateDateTime > @dtmStartDate OR pi.ModifyDateTime > @dtmStartDate)

END
GO
