SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalFlattenPortalHierarchy] 
AS
BEGIN

IF  OBJECT_ID('PortalFlattenedHierachy') IS NOT NULL 
BEGIN
	DROP TABLE PortalFlattenedHierachy
END 

SELECT BU.BusinessUnitId, BU.ParentBusinessUnitId, BU.ExternalCode,BU.Name, ISNULL(BUT.DisplayValue,'Unknown') AS ButType 
INTO #LastUnits -- DROP TABLE #LastUnits SELECT * FROM #LastUnits 
FROM vwPortal_BusinessUnit BU
LEFT JOIN vwPortal_BusinessUnitType BUT ON BU.BusinessUnitTypeId = BUT.BusinessUnitTypeId
WHERE BU.DeleteInd = 0 
AND ISNULL((SELECT COUNT(1) FROM vwPortal_BusinessUnit BU2 
	 WHERE BU2.ParentBusinessUnitId = BU.BusinessUnitId AND BU2.DeleteInd = 0 ),0) = 0 
	 

;WITH CTE1(businessunitid, lvlid, parentbusinessunitid, externalcode, name,ButType) AS 
(
	SELECT businessunitid, 1, parentbusinessunitid, externalcode,name,ButType FROM #LastUnits
	UNION ALL  
	SELECT cte1.businessunitid, cte1.lvlid + 1, bu.parentbusinessunitid, bu.externalcode, bu.name,ISNULL(BUT.DisplayValue,'Unknown') AS ButType 
	FROM vwPortal_BusinessUnit bu inner join cte1 ON bu.businessunitid = cte1.ParentBusinessUnitId AND bu.DeleteInd = 0
	INNER JOIN vwPortal_BusinessUnitType BUT ON BU.BusinessUnitTypeId = BUT.BusinessUnitTypeId
),
CTE2 (businessunitid,parentbusinessunitid,lvlid,externalcode,name,ButType) AS 
(
	SELECT businessunitid, parentbusinessunitid,ROW_NUMBER() OVER(Partition by BusinessUnitId Order by lvlid desc) lvlid,externalcode, name,ButType FROM CTE1 
),
CTE3 (BusinessUnitId,ParentBusinessUnitId,Lvlid,externalcode,name,ButType) AS 
(
SElECT BusinessUnitId, BusinessUnitId, 1 AS Lvlid,externalcode,name,ButType FROM #LastUnits 
UNION
SElECT BusinessUnitId, BusinessUnitId, 2 AS Lvlid,externalcode,name,ButType FROM #LastUnits
UNION
SElECT BusinessUnitId, BusinessUnitId, 3 AS Lvlid,externalcode,name,ButType FROM #LastUnits
UNION
SElECT BusinessUnitId, BusinessUnitId, 4 AS Lvlid,externalcode,name,ButType FROM #LastUnits
UNION
SElECT BusinessUnitId, BusinessUnitId, 5 AS Lvlid,externalcode,name,ButType FROM #LastUnits
UNION
SElECT BusinessUnitId, BusinessUnitId, 6 AS Lvlid,externalcode,name,ButType FROM #LastUnits
),
CTE4 (BusinessUnitId,Lvlid,ParentBusinessUnitId,externalcode,name,ButType) AS 
(
SELECT CTE3.BusinessUnitId, CTE3.LvlId, ISNULL(CTE2.parentbusinessunitid,CTE3.parentbusinessunitid) parentbusinessunitid, ISNULL(CTE2.externalcode,CTE3.externalcode) externalcode
	   , ISNULL(CTE2.name,CTE3.name) name, ISNULL(CTE2.ButType,CTE3.ButType) ButType 
FROM CTE3 LEFT JOIN CTE2 ON CTE3.BusinessUnitId = CTE2.BusinessUnitId AND CTE3.lvlid = CTE2.lvlid
)  
SELECT BusinessUnitId,
	   0 As HealthcareOrgUnitId,
	   MAX(CASE WHEN  LvlId = 1 THEN ParentBusinessUnitId ELSE '' END) BusinessUnitId1, 
	   MAX(CASE WHEN  LvlId = 1 THEN externalcode ELSE '' END) BusinessUnitExternalCode1, 
	   MAX(CASE WHEN  LvlId = 1 THEN name ELSE '' END) BusinessUnitName1, 
	   MAX(CASE WHEN  LvlId = 1 THEN ButType ELSE '' END) ButType1, 
	   MAX(CASE WHEN  LvlId = 2 THEN ParentBusinessUnitId ELSE '' END) BusinessUnitId2,
	   MAX(CASE WHEN  LvlId = 2 THEN externalcode ELSE '' END) BusinessUnitExternalCode2, 
	   MAX(CASE WHEN  LvlId = 2 THEN name ELSE '' END) BusinessUnitName2,
	   MAX(CASE WHEN  LvlId = 2 THEN ButType ELSE '' END) ButType2, 
	   MAX(CASE WHEN  LvlId = 3 THEN ParentBusinessUnitId ELSE '' END) BusinessUnitId3,
	   MAX(CASE WHEN  LvlId = 3 THEN externalcode ELSE '' END) BusinessUnitExternalCode3, 
	   MAX(CASE WHEN  LvlId = 3 THEN name ELSE '' END) BusinessUnitName3,
	   MAX(CASE WHEN  LvlId = 3 THEN ButType ELSE '' END) ButType3, 
	   MAX(CASE WHEN  LvlId = 4 THEN ParentBusinessUnitId ELSE '' END) BusinessUnitId4,
	   MAX(CASE WHEN  LvlId = 4 THEN externalcode ELSE '' END) BusinessUnitExternalCode4, 
	   MAX(CASE WHEN  LvlId = 4 THEN name ELSE '' END) BusinessUnitName4,
	   MAX(CASE WHEN  LvlId = 4 THEN ButType ELSE '' END) ButType4, 
	   MAX(CASE WHEN  LvlId = 5 THEN ParentBusinessUnitId ELSE '' END) BusinessUnitId5,
	   MAX(CASE WHEN  LvlId = 5 THEN externalcode ELSE '' END) BusinessUnitExternalCode5, 
	   MAX(CASE WHEN  LvlId = 5 THEN name ELSE '' END) BusinessUnitName5,
	   MAX(CASE WHEN  LvlId = 5 THEN ButType ELSE '' END) ButType5, 
	   MAX(CASE WHEN  LvlId = 6 THEN ParentBusinessUnitId ELSE '' END) BusinessUnitId6, 
	   MAX(CASE WHEN  LvlId = 6 THEN externalcode ELSE '' END) BusinessUnitExternalCode6,
	   MAX(CASE WHEN  LvlId = 6 THEN name ELSE '' END) BusinessUnitName6,
	   MAX(CASE WHEN  LvlId = 6 THEN ButType ELSE '' END) ButType6
INTO PortalFlattenedHierachy
FROM CTE4 
GROUP BY BusinessUnitId

UPDATE PortalFlattenedHierachy 
SET HealthcareOrgUnitId = HO.HealthcareOrgId
FROM PortalFlattenedHierachy PFH INNER JOIN vwPortal_HealthcareOrg HO ON PFH.BusinessUnitId	= HO.BusinessUnitId AND HO.DeleteInd = 0 

END
GO
