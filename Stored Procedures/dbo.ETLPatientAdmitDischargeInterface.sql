SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLPatientAdmitDischargeInterface]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		06.23.2016
	INITIAL VER:	2.0.1
	MODULE:			ETL		
	DESCRIPTION:	Merge the PatientAdmitDischarge data from the interface into EDW, 
					create admit and discharge events in PatientEvent
	PARAMETERS:		full load flat - determines if it should run on all rows or just new rows
	RETURN VALUE(s)/OUTPUT:	none
	PROGRAMMING NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.0.1		06.23.2016		BR						Initial Version
	2.0.2		10.29.2016		BR			LBDM-1242	Removed the SourceSystemId from the matching criteria, checked discharge date for NULL
	2.0.2		11.03.2016		BR			LBDM-1242	Changed the merge to include an update, 
														changed the select to not use SourceSystem, 
														changed the match criteria to use FacilityName
														Changed the AcuteInd to default to NULL instead of 0
	2.1.2		12.19.2016		BR			LBDM-1370	Include PatientClass field

=================================================================*/
(
    @fullLoadFlag bit = 0
)
AS
BEGIN
    SET NOCOUNT ON;

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientAdmitDischarge';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientAdmitDischargeInterface';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY
		-- Retrieve the last time the SP ran and set the boundary variables
		IF NOT EXISTS ( SELECT top 1 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] = @EDWName)
		BEGIN
			EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed
		END

		IF @fullLoadFlag = 1
		BEGIN
			UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
			SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME)
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
		END

		DECLARE @lastDateTime DATETIME = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
										  FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
										  WHERE [EDWtableName] = @EDWtableName 
											AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	
		DECLARE @maxSourceDateTime DATETIME = (SELECT MAX([CreateDateTime]) FROM [dbo].[vwPortal_InterfacePatientAdmitDischarge]);

		SELECT @RecordCountBefore=count(1) FROM [dbo].[PatientAdmitDischarge];

		--if there are any rows in the interface newer than the last time this ran bring them in
        IF @maxSourceDateTime > @lastDateTime
		BEGIN
			--create PatientEvent for admit
			MERGE INTO PatientEvent AS target
			USING (SELECT DISTINCT 
				idRef.LbPatientId,
				ifs.SourceSystemid,
				pet.PatientEventTypeId,
				ipad.AdmitDate AS EventDateTime,
				ISNULL(ipad.AdmitType,'') + ' Admission, Reason: ' + ISNULL(ipad.AdmitReason,'') + '. ' + ISNULL(ipad.DiagnsosisDescription,'') AS EventDetail
			FROM vwPortal_InterfacePatientAdmitDischarge ipad
				INNER JOIN vwPortal_InterfacePatient ip ON ip.InterfacePatientId = ipad.InterfacePatientId
				INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ipad.InterfaceSystemId
				INNER JOIN PatientIdReference idRef ON idRef.ExternalId = ip.PatientIdentifier AND idRef.SourceSystemId = ifs.SourceSystemId 
				INNER JOIN PatientEventType pet ON pet.Name = 'Admit'
			WHERE ipad.AdmitDate IS NOT NULL
				AND ipad.CreateDateTime > @lastDateTime
				AND idRef.IdTypeDesc = 'InterfacePatientIdentifier') AS source
			ON source.LbPatientId = target.LbPatientId 
				AND source.PatientEventTypeId = target.PatientEventTypeId
				AND source.EventDateTime = target.EventDateTime
			WHEN NOT MATCHED BY target THEN
				INSERT (LBPatientId,SourceSystemId,PatientEventTypeId,EventDateTime,EventDetail,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
				VALUES (source.LBPatientId,source.SourceSystemid,source.PatientEventTypeId,source.EventDateTime,source.EventDetail,0,GETUTCDATE(),GETUTCDATE(),1,1);
	
			--create PatientEvent for discharge
			MERGE INTO PatientEvent AS target
			USING (SELECT DISTINCT
					idRef.LbPatientId,
					ifs.SourceSystemid,
					pet.PatientEventTypeId,
					ipad.DischargeDate AS EventDateTime,
					'Discharge. ' + ISNULL(ipad.DischargeDisposition,'') AS EventDetail
				   FROM vwPortal_InterfacePatientAdmitDischarge ipad
						INNER JOIN vwPortal_InterfacePatient ip ON ip.InterfacePatientId = ipad.InterfacePatientId
						INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ipad.InterfaceSystemId
						INNER JOIN PatientIdReference idRef ON idRef.ExternalId = ip.PatientIdentifier AND idRef.SourceSystemId = ifs.SourceSystemId
						INNER JOIN PatientEventType pet ON pet.Name = 'Discharge'
					WHERE ipad.DischargeDate IS NOT NULL
						AND ipad.CreateDateTime > @lastDateTime
						AND idRef.IdTypeDesc = 'InterfacePatientIdentifier') AS source
			ON source.LbPatientId = target.LbPatientId 
				AND source.PatientEventTypeId = target.PatientEventTypeId
				AND source.EventDateTime = target.EventDateTime
			WHEN NOT MATCHED BY target THEN
				INSERT (LBPatientId,SourceSystemId,PatientEventTypeId,EventDateTime,EventDetail,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
				VALUES (source.LBPatientId,source.SourceSystemid,source.PatientEventTypeId,source.EventDateTime,source.EventDetail,0,GETUTCDATE(),GETUTCDATE(),1,1);
				

			--insert into PatientAdmitDischarge ---------------------------------------------------
			MERGE INTO PatientAdmitDischarge AS target
			USING (SELECT DISTINCT
						ifs.SourceSystemId,
						idRef.LbPatientId,
						ipad.AdmitType,
						ipad.AdmitReason,
						ipad.BedType,
						ipad.NotificationDate,
						ipad.AdmitDate,
						ipad.DischargeDate,
						ipad.DischargeDisposition,
						ipad.DiagnosisCode,
						ipad.DiagnsosisDescription,
						ipad.FacilityName,
						ipad.PCPName,
						ipad.PatientClass,
						admit.PatientEventId AS AdmitPatientEventId,
						disch.PatientEventId AS DischargePatientEventId,
						ROW_NUMBER() OVER (PARTITION BY idRef.LbPatientId,ipad.AdmitDate,ipad.FacilityName ORDER BY ipad.CreateDateTime DESC) row
					FROM vwPortal_InterfacePatientAdmitDischarge ipad
						INNER JOIN vwPortal_InterfacePatient ip ON ip.InterfacePatientId = ipad.InterfacePatientId
						INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ipad.InterfaceSystemId
						INNER JOIN PatientIdReference idRef ON idRef.ExternalId = ip.PatientIdentifier AND idRef.SourceSystemId = ifs.SourceSystemId
						LEFT OUTER JOIN (SELECT DISTINCT pe1.SourceSystemId, pe1.LbPatientId, pe1.EventDateTime, pe1.PatientEventId, ROW_NUMBER() OVER (PARTITION BY pe1.LbPatientId, pe1.EventDateTime ORDER BY pe1.CreateDateTime DESC) r 
											FROM PatientEvent pe1 
											INNER JOIN PatientEventType pet ON pet.PatientEventTypeId = pe1.PatientEventTypeId
											WHERE pe1.EventDateTime IS NOT NULL 
											AND pet.Name = 'Admit') admit ON admit.LbPatientId = idRef.LbPatientId AND admit.EventDateTime = ipad.AdmitDate AND admit.r = 1
						LEFT OUTER JOIN (SELECT DISTINCT pe2.SourceSystemId, pe2.LbPatientId, pe2.EventDateTime, pe2.PatientEventId, ROW_NUMBER() OVER (PARTITION BY pe2.LbPatientId, pe2.EventDateTime ORDER BY pe2.CreateDateTime DESC) r 
											FROM PatientEvent pe2
											INNER JOIN PatientEventType pet ON pet.PatientEventTypeId = pe2.PatientEventTypeId
											WHERE pe2.EventDateTime IS NOT NULL
											AND pet.Name = 'Discharge') disch ON disch.LbPatientId = idRef.LbPatientId AND disch.EventDateTime = ipad.DischargeDate AND disch.r = 1
					WHERE (admit.PatientEventId IS NOT NULL OR disch.PatientEventId IS NOT NULL)
						AND ipad.CreateDateTime > @lastDateTime) AS source
			ON source.LbPatientId = target.LBPatientId 
				--AND source.SourceSystemId = target.SourceSystemId
				AND source.AdmitDate = target.AdmitDate 
				--AND ((source.DischargeDate = target.DischargeDate) OR (source.DischargeDate IS NULL AND target.DischargeDate IS NULL))
				AND (source.FacilityName = target.FacilityName OR (source.FacilityName IS NOT NULL AND target.FacilityName IS NULL))
			WHEN MATCHED AND source.row = 1 THEN 
				UPDATE SET 
					AdmitType = ISNULL(source.AdmitType, target.AdmitType),
					AdmitReason = ISNULL(source.AdmitReason, target.AdmitReason),
					BedType = ISNULL(source.BedType, target.BedType),
					NotificationDate = ISNULL(source.NotificationDate, target.NotificationDate),
					DischargePatientEventId = ISNULL(source.DischargePatientEventId, target.DischargePatientEventId),
					DischargeDate = ISNULL(source.DischargeDate, target.DischargeDate),
					DischargeDisposition = ISNULL(source.DischargeDisposition, target.DischargeDisposition),
					DiagnosisCode = ISNULL(source.DiagnosisCode, target.DiagnosisCode),
					DiagnsosisDescription = ISNULL(source.DiagnsosisDescription, target.DiagnsosisDescription),
					FacilityName = ISNULL(source.FacilityName, target.FacilityName),
					PCPName = ISNULL(source.PCPName, target.PCPName),
					PatientClass = ISNULL(source.PatientClass, target.PatientClass),
					ModifyDateTime = GETUTCDATE()
			WHEN NOT MATCHED BY target AND source.row = 1 AND source.AdmitDate IS NOT NULL THEN
				INSERT (LBPatientId,AdmitPatientEventId,DischargePatientEventId,AdmitType,AdmitReason,BedType,NotificationDate,AdmitDate,DischargeDate,DischargeDisposition,
						DiagnosisCode,DiagnsosisDescription,FacilityName,PCPName,OnDailyCensusInd,DischargeToOtherFacility,SNFRehab,AcuteInd,SourceSystemId,PatientClass,
						CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
				VALUES (source.LbPatientId,source.AdmitPatientEventId,source.DischargePatientEventId,source.AdmitType,source.AdmitReason,source.BedType,source.NotificationDate,source.AdmitDate,source.DischargeDate,source.DischargeDisposition,
						source.DiagnosisCode,source.DiagnsosisDescription,source.FacilityName,source.PCPName,0,NULL,NULL,NULL,source.SourceSystemId,source.PatientClass,
						GETUTCDATE(),GETUTCDATE(),1,1);
		END

		-- Record the execution stats --------------------------------------------------------
		SELECT @RecordCountAfter=count(1) FROM [dbo].PatientMedication;

        -- Calculate records inserted and updated counts
	    SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	    SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	    -- update maxSourceTimeStamp with max LR_CreateDateTime of PatientLabOrderProcessQueue
		UPDATE [Maintenance].dbo.[EDWTableLoadTracking]
			SET [maxSourceTimeStampProcessed] = (SELECT MAX(convert(DATE,[CreateDateTime])) FROM [dbo].[PatientMedicationProcessQueue] ),
					 [updateDateTime] = GETUTCDATE()
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END


GO
