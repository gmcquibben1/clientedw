SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwVitals] 
AS
BEGIN
SET NOCOUNT ON

UPDATE [dbo].PatientVitalsProcessQueue SET [Clinician] = '' WHERE [Clinician] IS NULL 

;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				[InterfaceSystemId],[LbPatientID],[ServiceDateTime],ISNULL([HeightCM],0),ISNULL([WeightKG],0),ISNULL([TemperatureCelcius],0),
				ISNULL([Pulse],0) ,ISNULL([Respiration],0),ISNULL([BloodPressureSystolic],0),ISNULL([BloodPressureDiastolic],0),ISNULL([OxygenSaturationSP02],'')
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				InterfacePatientVitalsId
				DESC 
			) SNO
	FROM PatientVitalsProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientVitalsId INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientVitalsId,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientVitalsId,PatientID
FROM 
(	
MERGE [DBO].PatientVitals AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,[ServiceDateTime],[HeightCM] ,[HeightComments]
      ,[WeightKG]  ,[WeightComments] ,[TemperatureCelcius] ,[TemperatureComments] ,[Pulse]
      ,[PulseComments] ,[Respiration],[RespirationComments] ,[BloodPressureSystolic],[BloodPressureDiastolic]
      ,[BloodPressureComment],[OxygenSaturationSP02] ,[OxygenSaturationComment]      ,[Clinician],[PatientVitalsComment], 
	  0 AS DeleteInd,GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId,
	  [EncounterIdentifier]
		FROM [dbo].PatientVitalsProcessQueue PVPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PVPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  ([OrigPatientId],[PatientID] ,[SourceSystemId] ,[ServiceDateTime],[HeightCM] ,[HeightComments]
      ,[WeightKG]  ,[WeightComments] ,[TemperatureCelcius] ,[TemperatureComments] ,[Pulse]
      ,[PulseComments] ,[Respiration],[RespirationComments] ,[BloodPressureSystolic],[BloodPressureDiastolic]
      ,[BloodPressureComment],[OxygenSaturationSP02] ,[OxygenSaturationComment]      ,[Clinician],[PatientVitalsComment]
      ,[DeleteInd] ,[CreateDateTime] ,[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId],[EncounterIdentifier])
ON (target.PatientID = source.PatientID AND target.ServiceDateTime = source.ServiceDateTime and target.[SourceSystemID] = source.[SourceSystemID] --and target.[Clinician] = source.[Clinician]
	AND ISNULL(target.[HeightCM],0)  = ISNULL(source.[HeightCM],0) AND ISNULL(target.[WeightKG],0)  = ISNULL(source.[WeightKG],0) AND ISNULL(target.[TemperatureCelcius],0) = ISNULL(source.[TemperatureCelcius],0) 
	AND ISNULL(target.[Pulse],0) = ISNULL(source.[Pulse],0) AND ISNULL(target.[Respiration],0) = ISNULL(source.[Respiration],0) AND ISNULL(target.[BloodPressureSystolic],0) = ISNULL(source.[BloodPressureSystolic],0) 
	AND ISNULL(target.[BloodPressureDiastolic],0) = ISNULL(source.[BloodPressureDiastolic],0) AND ISNULL(target.[OxygenSaturationSP02],'') = ISNULL(source.[OxygenSaturationSP02],'') 
)
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
		--[PatientID] = source.[PatientID], 
		[SourceSystemId] = source.[SourceSystemId], 
		[ServiceDateTime] = source.[ServiceDateTime], 
		[HeightCM] = source.[HeightCM], 
		[HeightComments] = source.[HeightComments], 
		[WeightKG] = source.[WeightKG], 
		[WeightComments] = source.[WeightComments], 
		[TemperatureCelcius] = source.[TemperatureCelcius], 
		[TemperatureComments] = source.[TemperatureComments], 
		[Pulse] = source.[Pulse], 
		[PulseComments] = source.[PulseComments], 
		[Respiration] = source.[Respiration], 
		[RespirationComments] = source.[RespirationComments], 
		[BloodPressureSystolic] = source.[BloodPressureSystolic], 
		[BloodPressureDiastolic] = source.[BloodPressureDiastolic], 
		[BloodPressureComment] = source.[BloodPressureComment], 
		[OxygenSaturationSP02] = source.[OxygenSaturationSP02], 
		[OxygenSaturationComment] = source.[OxygenSaturationComment], 
		[Clinician] = source.[Clinician], 
		[PatientVitalsComment] = source.[PatientVitalsComment], 
		[DeleteInd] = source.[DeleteInd], 
		--[CreateDateTime] = source.[CreateDateTime], 
		[ModifyDateTime] = source.[ModifyDateTime], 
		--[CreateLBUserId] = source.[CreateLBUserId], 
		[ModifyLBUserId] = source.[ModifyLBUserId],
		[EncounterIdentifier] = source.[EncounterIdentifier]
WHEN NOT MATCHED THEN
INSERT ([PatientID] ,[SourceSystemId] ,[ServiceDateTime],[HeightCM] ,[HeightComments]
      ,[WeightKG]  ,[WeightComments] ,[TemperatureCelcius] ,[TemperatureComments] ,[Pulse]
      ,[PulseComments] ,[Respiration],[RespirationComments] ,[BloodPressureSystolic],[BloodPressureDiastolic]
      ,[BloodPressureComment],[OxygenSaturationSP02] ,[OxygenSaturationComment]      ,[Clinician],[PatientVitalsComment]
      ,[DeleteInd] ,[CreateDateTime] ,[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId],[EncounterIdentifier]
	   )
VALUES (source.[PatientID] ,source.[SourceSystemId] ,source.[ServiceDateTime],source.[HeightCM] ,source.[HeightComments]
      ,source.[WeightKG]  ,source.[WeightComments] ,source.[TemperatureCelcius] ,source.[TemperatureComments] ,source.[Pulse]
      ,source.[PulseComments] ,source.[Respiration],source.[RespirationComments] ,source.[BloodPressureSystolic],source.[BloodPressureDiastolic]
      ,source.[BloodPressureComment],source.[OxygenSaturationSP02] ,source.[OxygenSaturationComment]      ,source.[Clinician],[PatientVitalsComment]
      ,source.[DeleteInd] ,source.[CreateDateTime] ,source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId],source.[EncounterIdentifier])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientVitalsId,INSERTED.PatientVitalsId) PatientVitalsId, source.PatientID
) x ;

IF OBJECT_ID('PatientVitals_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientVitals_PICT(PatientVitalsId,PatientId,SwitchDirectionId)
	SELECT RT.PatientVitalsId, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientVitalsProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1  

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'Vitals',GetDate()
FROM PatientVitalsProcessQueue
WHERE ProcessedInd = 1 

UPDATE A 
SET HoldingReleaseInd = 1 
FROM [dbo].[PatientVitalsProcessQueue_HoldingBay] A 
INNER JOIN PatientVitalsProcessQueue B ON A.[InterfacePatientVitalsId] = B.[InterfacePatientVitalsId]
WHERE B.ProcessedInd = 1 

--INSERT INTO[dbo].[PatientVitalsProcessQueue_HoldingBay]
--([InterfacePatientVitalsId],[InterfacePatientID],[LbPatientId]
--,[InterfaceSystemId],[ServiceDateTime],[HeightCM]
--,[HeightComments],[WeightKG],[WeightComments]
--,[TemperatureCelcius],[TemperatureComments],[Pulse]
--,[PulseComments],[Respiration],[RespirationComments]
--,[BloodPressureSystolic],[BloodPressureDiastolic],[BloodPressureComment]
--,[OxygenSaturationSP02],[OxygenSaturationComment],[Clinician]
--,[PatientVitalsComment],[CreateDateTime],[InterfacePatientVitalsId_HoldingOverride]
--,[InterfacePatientID_HoldingOverride],[LbPatientId_HoldingOverride],[InterfaceSystemId_HoldingOverride]
--,[ServiceDateTime_HoldingOverride],[HeightCM_HoldingOverride],[HeightComments_HoldingOverride]
--,[WeightKG_HoldingOverride],[WeightComments_HoldingOverride],[TemperatureCelcius_HoldingOverride]
--,[TemperatureComments_HoldingOverride],[Pulse_HoldingOverride],[PulseComments_HoldingOverride]
--,[Respiration_HoldingOverride],[RespirationComments_HoldingOverride],[BloodPressureSystolic_HoldingOverride]
--,[BloodPressureDiastolic_HoldingOverride],[BloodPressureComment_HoldingOverride],[OxygenSaturationSP02_HoldingOverride]
--,[OxygenSaturationComment_HoldingOverride],[Clinician_HoldingOverride],[PatientVitalsComment_HoldingOverride]
--,[CreateDateTime_HoldingOverride],[HoldingStartDate],[HoldingStartReason]
--)
--SELECT 
--A.[InterfacePatientVitalsId],A.[InterfacePatientID],A.[LbPatientId]
--,A.[InterfaceSystemId],A.[ServiceDateTime],A.[HeightCM]
--,A.[HeightComments],A.[WeightKG],A.[WeightComments]
--,A.[TemperatureCelcius],A.[TemperatureComments],A.[Pulse]
--,A.[PulseComments],A.[Respiration],A.[RespirationComments]
--,A.[BloodPressureSystolic],A.[BloodPressureDiastolic],A.[BloodPressureComment]
--,A.[OxygenSaturationSP02],A.[OxygenSaturationComment],A.[Clinician]
--,A.[PatientVitalsComment],A.[CreateDateTime],A.[InterfacePatientVitalsId]
--,A.[InterfacePatientID],A.[LbPatientId],A.[InterfaceSystemId]
--,A.[ServiceDateTime],A.[HeightCM],A.[HeightComments]
--,A.[WeightKG],A.[WeightComments],A.[TemperatureCelcius]
--,A.[TemperatureComments],A.[Pulse],A.[PulseComments]
--,A.[Respiration],A.[RespirationComments],A.[BloodPressureSystolic]
--,A.[BloodPressureDiastolic],A.[BloodPressureComment],A.[OxygenSaturationSP02]
--,A.[OxygenSaturationComment],A.[Clinician],A.[PatientVitalsComment]
--,A.[CreateDateTime],GetDate(),'Duplicacy'
--FROM [dbo].[PatientVitalsProcessQueue] A 
--LEFT JOIN PatientVitalsProcessQueue_HoldingBay B ON A.[InterfacePatientVitalsId] = B.[InterfacePatientVitalsId]
--WHERE ISNULL(A.ProcessedInd,0) <> 1 AND B.[InterfacePatientVitalsId] IS NULL 

END

GO
