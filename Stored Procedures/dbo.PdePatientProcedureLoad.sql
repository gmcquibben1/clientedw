SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientProcedureLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 

AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		pp.PatientId, 
		pp.SourceSystemId, 
		pp.ProcedureCodingSystem,
		pp.ProcedureCode,
		pp.ProcedureDescription ,
		--pp.ProcedureFunctionTypeID,
		pp.ProcedureCodeModifier1,
		pp.ProcedureCodeModifier2,
		pp.ProcedureCodeModifier3,
		pp.ProcedureCodeModifier4,
		pp.ProcedureDateTime,
		pp.ProcedureComment,
		pp.EncounterID,
		pp.PerformedByProviderID,
		pp.PerformedByClinician,
		pp.OrderedByProviderID,
		pp.OrderedByClinician
		--pp.ProcedureDescriptionAlternate
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientProcedure pp ON pp.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
		AND (pp.CreateDateTime > @dtmStartDate OR pp.ModifyDateTime > @dtmStartDate)

END
GO
