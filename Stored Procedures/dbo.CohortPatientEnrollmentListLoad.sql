SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CohortPatientEnrollmentListLoad]
(
    @CohortId int,
    @PreferredContacts xml
)
AS
BEGIN
    SET NOCOUNT ON;
	DECLARE @LocalCohortId int = @CohortId
 
 	SELECT pe.PatientId, pe.EventName INTO #tempLastEvent 
	FROM [dbo].[fnGetLastPatientEvent]() pe
	JOIN [dbo].[vwPortal_CohortPatient] cp ON cp.PatientId = pe.PatientId
	WHERE cp.CohortId = @LocalCohortId

    SELECT cp.PatientId,
           ISNULL(ps.CCMStatusInd, CONVERT(BIT,0)) [CCMStatusInd],
           pe.EventName [LastEvent],
           px.ExternalId [MemberId],
           pd.AdmitDate,
           pd.DischargeDate,
           pd.FacilityName
      FROM [dbo].[vwPortal_CohortPatient] cp
		 LEFT JOIN [dbo].[fnGetPatientCCMStatus]() ps ON cp.PatientId = ps.PatientId
		 LEFT JOIN #tempLastEvent pe ON cp.PatientId = pe.PatientId
		 LEFT JOIN [dbo].[fnGetPatientExternalId](@PreferredContacts) px ON cp.PatientId = px.PatientId
		 LEFT JOIN [dbo].[fnGetPatientAdmitDischarge]() pd ON cp.PatientId = pd.PatientId
     WHERE cp.[CohortId] = @LocalCohortId;

END
GO
