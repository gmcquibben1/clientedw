SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalRulePartDiagExist] @yearend datetime = '12/31/2015', 
@timeBegin datetime = '1/1/2015', @timeEnd datetime = '12/31/2015', @diagValueSet varchar(250)
, @MetricName varchar(100), @MetricType varchar(50), @RulePartId int = null, @MetricValue varchar(50) = 1
WITH EXEC AS CALLER
AS
DELETE FROM PatientMetric where MetricName = @MetricName

INSERT INTO PatientMetric (
   LbPatientId
  ,MetricName
  ,MetricType
  ,MetricValue
) SELECT DISTINCT
   pd.PatientId  AS LbPatientId            -- LbPatientId - int
  ,@MetricName AS MetricName             -- MetricName - varchar(100)
  ,@MetricType AS MetricType             -- MetricType - varchar(50)
  ,@MetricValue AS MetricValue            -- MetricValue - varchar(50)
FROM [dbo].[PatientDiagnosis] pd -- COPD Diagnosis
					JOIN [dbo].[PatientDiagnosisDiagnosisCode] pddc
								ON  pddc.[PatientDiagnosisId] = pd.[PatientDiagnosisId]
					JOIN [dbo].[DiagnosisCode] dc
								ON  dc.[DiagnosisCodeID] = pddc.[DiagnosisCodeID]
					JOIN [dbo].[HEDISValueSetCodes] hvsc_diag
								ON  hvsc_diag.[Code] = dc.[DiagnosisCode]
								AND hvsc_diag.[Value Set Name] IN (@diagValueSet)
          WHERE (CONVERT(VARCHAR(10), pd.DiagnosisDateTime, 121) BETWEEN CAST(@timeBegin AS DATE) AND CAST(@timeEnd AS DATE))
GO
