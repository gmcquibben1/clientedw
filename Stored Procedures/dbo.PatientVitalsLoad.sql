SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientVitalsLoad] 
	@PatientId INTEGER
AS

SELECT DISTINCT       
	dbo.PatientVitals.ServiceDateTime,
	dbo.SourceSystem.Description as SourceSystem,
	dbo.PatientVitals.HeightCM,
	dbo.PatientVitals.WeightKG,
	dbo.PatientVitals.TemperatureCelcius,
	dbo.PatientVitals.Pulse,
	dbo.PatientVitals.Respiration,
	dbo.PatientVitals.BloodPressureSystolic,
	dbo.PatientVitals.BloodPressureDiastolic,
	dbo.PatientVitals.OxygenSaturationSP02,
	dbo.PatientVitals.PatientVitalsComment,

	BMI = CASE WHEN ( dbo.PatientVitals.HeightCM IS NOT NULL AND  dbo.PatientVitals.HeightCM <> 0 AND 
				dbo.PatientVitals.WeightKG IS NOT NULL AND  dbo.PatientVitals.WeightKG <> 0 )
		 THEN (CAST(CAST(ROUND((((dbo.PatientVitals.WeightKg * 2.20462) / ((dbo.PatientVitals.HeightCM * 0.393701) * (dbo.PatientVitals.HeightCM * 0.393701))) * 703), 2) AS numeric(7,2)) AS varchar(10))) 
		 ELSE NULL
	END,

	WeightLbs = CASE WHEN ( dbo.PatientVitals.WeightKG IS NOT NULL AND  dbo.PatientVitals.WeightKG <> 0)
		 THEN ( CAST (CAST( ROUND( (dbo.PatientVitals.WeightKG * 2.2046),2) AS numeric(7,2)) AS varchar(7) ) ) 
		 ELSE NULL
	END,

	HeightIn = CASE WHEN ( dbo.PatientVitals.HeightCM IS NOT NULL AND  dbo.PatientVitals.HeightCM <> 0)
		 THEN ( CAST (CAST( ROUND( (dbo.PatientVitals.HeightCM * 0.39370),2) AS numeric(7,2)) AS varchar(7) ) ) 
		 ELSE NULL
	END,

	TemperatureFahrenheit = CASE WHEN ( dbo.PatientVitals.TemperatureCelcius IS NOT NULL AND  dbo.PatientVitals.TemperatureCelcius <> 0)
		 THEN ( CAST( CAST(ROUND( (((dbo.PatientVitals.TemperatureCelcius * 9.0) / 5) + 32),1) AS numeric(5,1)) AS varchar(7) ) ) 
		 ELSE NULL
	END

FROM dbo.PatientVitals LEFT OUTER JOIN
	 dbo.SourceSystem ON PatientVitals.SourceSystemId = SourceSystem.SourceSystemId 

WHERE dbo.PatientVitals.PatientId = @Patientid AND dbo.PatientVitals.DeleteInd = 0 
ORDER BY PatientVitals.ServiceDateTime DESC





GO
