SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLPatientAttributePrimaryInsurance](@fullLoadFlag bit = 0)
/*===============================================================
	CREATED BY: 	Lan
	CREATED ON:		2016-12-04
	INITIAL VER:	2.2
	MODULE:			(Optional) Module it applies to 		
	DESCRIPTION:	This SP put patients' primary insurance into PatientAttibute table so that this data can be used 
	                to overwrite a customized orghierarchy level data.
	PARAMETERS:		(Optional) All non-obvious parameters must be described
	RETURN VALUE(s)/OUTPUT:	(Optional) Especially when returning specific values or error codes.
	PROGRAMMIN NOTES: (Optional) This is sometimes helpful to explain some implementation details that may not be obvious.
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.0		2016-12-04		LM			LBAN-3234	This SP put patients' primary insurance into PatientAttibute table

=================================================================*/
AS
BEGIN	

    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'vwPortal_PatientAttribute';
	DECLARE @dataSource VARCHAR(20) = 'PatientInsurance';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientAttributePrimaryInsurance';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

	IF NOT EXISTS (SELECT 1 FROM [dbo].[vwPortal_PatientAttributeType] WHERE [Name] = 'Primary Insurance')
	BEGIN
	  INSERT INTO [dbo].[vwPortal_PatientAttributeType]
			   ([Name]
			   ,[DisplayValue]
			   ,[DeleteInd]
			   ,[CreateDateTime]
			   ,[ModifyDateTime]
			   ,[CreateLBUserId]
			   ,[ModifyLBUserId])
		 VALUES( 'Primary Insurance'
				,'Primary Insurance'
				,0
				,GETUTCDATE()
				,GETUTCDATE()
				,1
				,1
				)
	END

	IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] = @dataSource AND [EDWName] = DB_NAME())
	  BEGIN
			 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed
	  END

	IF @fullLoadFlag = 1
	  BEGIN
		UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
		   SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME)
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] = @dataSource AND [EDWName] = DB_NAME()
	  END

	--set parameter values
	DECLARE @lastDateTime DATETIME = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] = @dataSource AND [EDWName] = DB_NAME());
	
	DECLARE @maxSourceDateTime DATETIME = (SELECT MAX([ModifyDateTime]) FROM [dbo].[PatientInsurance]);

	IF @maxSourceDateTime > @lastDateTime
	BEGIN  
	  BEGIN TRY

		SELECT
		[PatientId]
		,[InsuranceName]
		,(SELECT [PatientAttributeTypeId] FROM [dbo].[vwPortal_PatientAttributeType] WHERE [Name] = 'Primary Insurance') AS [PatientAttributeTypeId]
		INTO #patientInsurance
		FROM(
		SELECT
		[PatientId]
		,[InsuranceName]
		,ROW_NUMBER() OVER(PARTITION BY [PatientId] ORDER BY [ModifyDateTime]) AS rowNmr
		FROM [dbo].[PatientInsurance]
		where InsurancePriority = 1
		AND [ModifyDateTime] > @lastDateTime
		) t1 
		WHERE rowNmr = 1

		SET @RecordCountSource=@@ROWCOUNT;
		SELECT @RecordCountBefore=count(1)
		FROM [dbo].[vwPortal_PatientAttribute]
		WHERE [PatientAttributeTypeId] = (SELECT [PatientAttributeTypeId] FROM [dbo].[vwPortal_PatientAttributeType] WHERE [Name] = 'Primary Insurance');

		CREATE INDEX IDX_#patientInsurancePatientIdAttributionTypeId ON #patientInsurance (PatientId,PatientAttributeTypeId);

		MERGE [dbo].[vwPortal_PatientAttribute] T
		USING #patientInsurance S
		ON T.[PatientId] = S.[PatientId] AND T.[PatientAttributeTypeId] = S.[PatientAttributeTypeId]
		WHEN MATCHED 
		THEN UPDATE SET   T.[Value] = S.[InsuranceName],
						T.[ModifyDateTime] = GETUTCDATE()		                    
		WHEN NOT MATCHED BY TARGET
		THEN INSERT ([PatientId],[PatientAttributeTypeId],[Value],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
			VALUES(S.[PatientId],S.[PatientAttributeTypeId] ,S.[InsuranceName],0,GETUTCDATE(),GETUTCDATE(),1,1);

		-- get total records of 'Primary Insurance' after merge 
		SELECT @RecordCountAfter=count(1)
		FROM [dbo].[vwPortal_PatientAttribute]
		WHERE [PatientAttributeTypeId] = (SELECT [PatientAttributeTypeId] FROM [dbo].[vwPortal_PatientAttributeType] WHERE [Name] = 'Primary Insurance');

        -- Calculate records inserted and updated counts
	    SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	    SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

        -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				   
    -- update maxSourceTimeStamp  from CreateDateTime of interfacePatientDiagnosis
         UPDATE [Maintenance]..[EDWTableLoadTracking]
         SET [maxSourceTimeStampProcessed] = (SELECT MAX([ModifyDateTime]) FROM [dbo].[PatientInsurance] ),
		     [updateDateTime] =GETUTCDATE()
        WHERE [EDWtableName] = @EDWtableName AND [dataSource] = @dataSource  AND [EDWName] = DB_NAME()

	  END TRY
	
	  BEGIN CATCH
	    --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		SET  @ErrorNumber =ERROR_NUMBER();
		SET  @ErrorState =ERROR_STATE();
		SET  @ErrorSeverity=ERROR_SEVERITY();
		SET  @ErrorLine=ERROR_LINE();
		SET  @Comment =left(ERROR_MESSAGE(),400);
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	  END CATCH

    END

END
GO
