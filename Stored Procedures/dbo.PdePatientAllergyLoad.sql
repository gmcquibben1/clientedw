SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientAllergyLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		pa.PatientId, 
		pa.SourceSystemId, 
		pa.AllergyType, 
		pa.AllergyDescription,
		pa.AllergySeverity, 
		pa.AllergyReaction, 
		pa.AllergyStatus, 
		pa.AllergyComment,
		pa.EncounterID,
		pa.ActiveInd
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientAllergy pa ON pa.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (pa.CreateDateTime > @dtmStartDate OR pa.ModifyDateTime > @dtmStartDate)
 
END
GO
