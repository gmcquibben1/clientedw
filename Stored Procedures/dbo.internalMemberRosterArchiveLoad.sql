SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ================================================================
-- Author:		Lan Ma
-- Create date: 2015-12-18
-- Description:	Load Member Roster Data Into MemberRosterArchives
-- Modified By: Lan 
-- Modified Date: 2016-04-27
-- Description: (1) add the part to populate TimeLine 
--              (2) use TimeLine get the latest Roster data
--              (3) add the deleted members if table15 no data
--              (4) archive history data for each table
-- Modified By: Lan 
-- Modified Date: 2016-08-09
-- ===============================================================


CREATE PROCEDURE [dbo].[internalMemberRosterArchiveLoad]

AS
BEGIN	

--remove reloaded roster file from history to avoid dup
DELETE FROM [dbo].[MemberRosterTable11History]
WHERE [FileInputName] IN (SELECT DISTINCT [FileInputName] FROM [dbo].[MemberRosterArchives_Table11])

DELETE FROM [dbo].[MemberRosterTable12History]
WHERE [FileInputName] IN (SELECT DISTINCT [FileInputName] FROM [dbo].[MemberRosterArchives_Table11])

DELETE FROM [dbo].[MemberRosterTable14History]
WHERE [FileInputName] IN (SELECT DISTINCT [FileInputName] FROM [dbo].[MemberRosterArchives_Table11])

DELETE FROM [dbo].[MemberRosterTable15History]
WHERE [FileInputName] IN (SELECT DISTINCT [FileInputName] FROM [dbo].[MemberRosterArchives_Table11])

DELETE FROM [dbo].[MemberRosterTable16History]
WHERE [FileInputName] IN (SELECT DISTINCT [FileInputName] FROM [dbo].[MemberRosterArchives_Table11])

--move new loaded roster data into history table
INSERT INTO [dbo].[MemberRosterTable11History]
SELECT [HICNO],[First Name],[Last Name],[Sex1],[Birth date],[DateofDeath],[AssignmentFlag],[PrevAssignmentFlag],[ACO],[FileDate],[FileTimeline],[FileInputName],[LbPatientId]
      ,[MonthlyEligibilityFlag1],[MonthlyEligibilityFlag2],[MonthlyEligibilityFlag3],[MonthlyEligibilityFlag4],[MonthlyEligibilityFlag5],[MonthlyEligibilityFlag6]
      ,[MonthlyEligibilityFlag7] ,[MonthlyEligibilityFlag8],[MonthlyEligibilityFlag9],[MonthlyEligibilityFlag10],[MonthlyEligibilityFlag11],[MonthlyEligibilityFlag12]
      ,[HCC1],[HCC2],[HCC5],[HCC7],[HCC8],[HCC9],[HCC10],[HCC15],[HCC16],[HCC17],[HCC18],[HCC19],[HCC21],[HCC25],[HCC26],[HCC27],[HCC31],[HCC32],[HCC33],[HCC37],[HCC38],[HCC44]
      ,[HCC45],[HCC51],[HCC52],[HCC54],[HCC55],[HCC67],[HCC68],[HCC69],[HCC70],[HCC71],[HCC72],[HCC73],[HCC74],[HCC75],[HCC77],[HCC78],[HCC79],[HCC80],[HCC81],[HCC82],[HCC83]
      ,[HCC92],[HCC95],[HCC96],[HCC100],[HCC101],[HCC104],[HCC105],[HCC107],[HCC108],[HCC111],[HCC112],[HCC119],[HCC130],[HCC131],[HCC132],[HCC148],[HCC149],[HCC150],[HCC154]
      ,[HCC155],[HCC157],[HCC158],[HCC161],[HCC164],[HCC174],[HCC176],[HCC177],GETDATE(),[HCCRiskScoreESRD],[HCCRiskScoreDisabled],[HCCRiskScoreAgedDual],[HCCRiskScoreAgedNonDual]
FROM [dbo].[MemberRosterArchives_Table11]

INSERT INTO [dbo].[MemberRosterTable12History]
SELECT *, GETDATE() FROM [dbo].[MemberRosterArchives_Table12]

INSERT INTO [dbo].[MemberRosterTable14History]
SELECT *, GETDATE() FROM [dbo].[MemberRosterArchives_Table14]

INSERT INTO [dbo].[MemberRosterTable15History]
SELECT *, GETDATE() FROM [dbo].[MemberRosterArchives_Table15]

INSERT INTO [dbo].[MemberRosterTable16History]
SELECT *, GETDATE() FROM [dbo].[MemberRosterArchives_Table16]

--remove the exiting roster file loaded to avoid dup
DELETE FROM [dbo].[MemberRosterArchives]
WHERE [FileInputName] IN (SELECT DISTINCT [FileInputName] FROM [dbo].[MemberRosterArchives_Table11])

--get roster member last status in memberRosterArchive in that ACO
SELECT 
[HICNO],
[StatusDesc],
ACO
INTO #tmp
FROM
(
SELECT 
[HICNO],
[StatusDesc],
ACO,
ROW_NUMBER() OVER(PARTITION BY [HICNO] ORDER BY TimeLine DESC) AS rowNbr
FROM [dbo].[MemberRosterArchives]
WHERE ACO =  (SELECT DISTINCT ACO FROM [dbo].[MemberRosterArchives_Table11])
) t
WHERE rowNbr = 1

SELECT
COALESCE(t1.[HICNO],t6.[HICNO],t5.[HICNO]) AS [HICNO] 
,COALESCE(t1.[First Name],t6.[FirstName],t5.[FirstName]) AS [First Name]
,COALESCE(t1.[Last Name],t6.[LastName],t5.[LastName]) AS [Last Name] 
,COALESCE(t1.[Sex1],t6.[Sex],t5.[Sex]) AS [Sex]
,CAST(COALESCE(t1.[Birth Date],t6.[BirthDate],t5.[BirthDate]) as DATE) AS [Birth Date]
,CASE WHEN RTRIM(LTRIM(COALESCE(t1.[DateofDeath],t6.[DateofDeath],t5.[DateofDeath]))) IS NOT NULL AND RTRIM(LTRIM(COALESCE(t1.[DateofDeath],t6.[DateofDeath],t5.[DateofDeath]))) <> ''
      THEN CAST(RTRIM(LTRIM(COALESCE(t1.[DateofDeath],t6.[DateofDeath],t5.[DateofDeath]))) AS DATE) 
 END AS [DateofDeath]
,CASE WHEN RTRIM(LTRIM(COALESCE(t1.[DateofDeath],t6.[DateofDeath],t5.[DateofDeath]))) IS NOT NULL AND RTRIM(LTRIM(COALESCE(t1.[DateofDeath],t6.[DateofDeath],t5.[DateofDeath]))) <> ''
      THEN 1
	  ELSE 0 
 END AS [Deceased Beneficiary Flag2]
,COALESCE(t1.[ACO],t6.[ACO],t5.[ACO]) AS [ACO]
,COALESCE(t1.[FileDate],t6.[FileDate],t5.[FileDate]) AS [FileDate]
,COALESCE(t1.[FileTimeline],t6.[FileTimeline],t5.[FileTimeline]) [FileTimeline]
,COALESCE(t1.[FileInputName],t6.[FileInputName],t5.[FileInputName]) [FileInputName]
,CASE WHEN t5.[HICNO] IS NOT NULL 
      THEN 'Dropped'
	  WHEN (t1.[HICNO] IS NOT NULL and tp.[HICNO] IS NULL) OR (t1.[HICNO] IS NOT NULL AND  tp.[HICNO] IS NOT NULL AND tp.[StatusDesc] = 'Assignable')
	  THEN 'New'
	  WHEN t1.[HICNO] IS NOT NULL AND  tp.[HICNO] IS NOT NULL AND tp.[StatusDesc] = 'Dropped'
	  THEN 'Returning'
	  WHEN t1.[HICNO] IS NOT NULL AND tp.[HICNO] IS NOT NULL AND tp.[StatusDesc] NOT IN( 'Dropped', 'Assignable')
	  THEN 'Continuing'
	  WHEN t1.[HICNO] IS NULL AND t6.[HICNO] IS NOT NULL
      THEN 'Assignable'
END AS [StatusDesc]
,t1.[AssignmentFlag]
,t1.[PrevAssignmentFlag]
,t5.[PluralityPrimaryCare]
,t5.[PartAPartB]
,t5.[GroupHealthPlan]
,t5.[NotResideInUS]
,t5.[IncludedSharedSavingsInitiative]
,t5.[NoPhysicianVisitACO]
,[MonthlyEligibilityFlag1]
,[MonthlyEligibilityFlag2]
,[MonthlyEligibilityFlag3]
,[MonthlyEligibilityFlag4]
,[MonthlyEligibilityFlag5]
,[MonthlyEligibilityFlag6]
,[MonthlyEligibilityFlag7]
,[MonthlyEligibilityFlag8]
,[MonthlyEligibilityFlag9]
,[MonthlyEligibilityFlag10]
,[MonthlyEligibilityFlag11]
,[MonthlyEligibilityFlag12]
,[HCC1]
,[HCC2]
,[HCC5]
,[HCC7]
,[HCC8]
,[HCC9]
,[HCC10]
,[HCC15]
,[HCC16]
,[HCC17]
,[HCC18]
,[HCC19]
,[HCC21]
,[HCC25]
,[HCC26]
,[HCC27]
,[HCC31]
,[HCC32]
,[HCC33]
,[HCC37]
,[HCC38]
,[HCC44]
,[HCC45]
,[HCC51]
,[HCC52]
,[HCC54]
,[HCC55]
,[HCC67]
,[HCC68]
,[HCC69]
,[HCC70]
,[HCC71]
,[HCC72]
,[HCC73]
,[HCC74]
,[HCC75]
,[HCC77]
,[HCC78]
,[HCC79]
,[HCC80]
,[HCC81]
,[HCC82]
,[HCC83]
,[HCC92]
,[HCC95]
,[HCC96]
,[HCC100]
,[HCC101]
,[HCC104]
,[HCC105]
,[HCC107]
,[HCC108]
,[HCC111]
,[HCC112]
,[HCC119]
,[HCC130]
,[HCC131]
,[HCC132]
,[HCC148]
,[HCC149]
,[HCC150]
,[HCC154]
,[HCC155]
,[HCC157]
,[HCC158]
,[HCC161]
,[HCC164]
,[HCC174]
,[HCC176]
,[HCC177]
,TRY_CONVERT(DECIMAL(10,3),[HCCRiskScoreESRD] ) AS [HCCRiskScoreESRD]
,TRY_CONVERT(DECIMAL(10,3),[HCCRiskScoreDisabled] ) AS [HCCRiskScoreDisabled]
,TRY_CONVERT(DECIMAL(10,3),[HCCRiskScoreAgedDual] ) AS [HCCRiskScoreAgedDual]
,TRY_CONVERT(DECIMAL(10,3),[HCCRiskScoreAgedNonDual] ) AS [HCCRiskScoreAgedNonDual]
INTO #fullMemberList
FROM [MemberRosterArchives_Table11] t1
FULL OUTER JOIN [dbo].[MemberRosterArchives_Table16] t6
ON t1.[HICNO] = t6.[HICNO]
AND t1.[FileDate] = t6.[FileDate]
AND t1.[ACO] = t6.[ACO]
FULL OUTER JOIN [dbo].[MemberRosterArchives_Table15] t5
ON t1.[HICNO] = t5.[HICNO]
AND t1.[FileDate] = t5.[FileDate]
AND t1.[ACO] = t5.[ACO]
LEFT JOIN #tmp  tp 
ON t1.[HICNO] = tp.[HICNO]
AND t1.[ACO] = tp.[ACO]


INSERT INTO MemberRosterArchives
([HICNO]
,[First Name]
,[Last Name]
,[Sex1]
,[Birth Date]
,[DateofDeath]
,[Deceased Beneficiary Flag2]
,[ACO]
,[AssignmentFlag]
,[PrevAssignmentFlag]
,[ACO Participant TIN Number]
,[Count of Primary Care Services3]
,[ACO Participant Type]
,[IndividualNPI]
,[PluralityPrimaryCare]
,[PartAPartB]
,[GroupHealthPlan]
,[NotResideInUS]
,[IncludedSharedSavingsInitiative]
,[NoPhysicianVisitACO]
,[FileDate]
,[FileTimeline]
,[FileInputName]
,[StatusDesc]
,CCN
,[MonthlyEligibilityFlag1]
,[MonthlyEligibilityFlag2]
,[MonthlyEligibilityFlag3]
,[MonthlyEligibilityFlag4]
,[MonthlyEligibilityFlag5]
,[MonthlyEligibilityFlag6]
,[MonthlyEligibilityFlag7]
,[MonthlyEligibilityFlag8]
,[MonthlyEligibilityFlag9]
,[MonthlyEligibilityFlag10]
,[MonthlyEligibilityFlag11]
,[MonthlyEligibilityFlag12]
,[HCC1]
,[HCC2]
,[HCC5]
,[HCC7]
,[HCC8]
,[HCC9]
,[HCC10]
,[HCC15]
,[HCC16]
,[HCC17]
,[HCC18]
,[HCC19]
,[HCC21]
,[HCC25]
,[HCC26]
,[HCC27]
,[HCC31]
,[HCC32]
,[HCC33]
,[HCC37]
,[HCC38]
,[HCC44]
,[HCC45]
,[HCC51]
,[HCC52]
,[HCC54]
,[HCC55]
,[HCC67]
,[HCC68]
,[HCC69]
,[HCC70]
,[HCC71]
,[HCC72]
,[HCC73]
,[HCC74]
,[HCC75]
,[HCC77]
,[HCC78]
,[HCC79]
,[HCC80]
,[HCC81]
,[HCC82]
,[HCC83]
,[HCC92]
,[HCC95]
,[HCC96]
,[HCC100]
,[HCC101]
,[HCC104]
,[HCC105]
,[HCC107]
,[HCC108]
,[HCC111]
,[HCC112]
,[HCC119]
,[HCC130]
,[HCC131]
,[HCC132]
,[HCC148]
,[HCC149]
,[HCC150]
,[HCC154]
,[HCC155]
,[HCC157]
,[HCC158]
,[HCC161]
,[HCC164]
,[HCC174]
,[HCC176]
,[HCC177]
,[HCCRiskScoreESRD]
,[HCCRiskScoreDisabled]
,[HCCRiskScoreAgedDual]
,[HCCRiskScoreAgedNonDual]
)
SELECT
 t1.[HICNO]
,t1.[First Name]
,t1.[Last Name]
,t1.[Sex]
,t1.[Birth Date]
,t1.[DateofDeath]
,t1.[Deceased Beneficiary Flag2]
,t1.[ACO]
,[AssignmentFlag]
,[PrevAssignmentFlag]
,t2.[ACO Participant TIN Number]
,[Count of Primary Care Services3]
,[ACO Participant Type]
,[Individual NPI]
,[PluralityPrimaryCare]
,[PartAPartB]
,[GroupHealthPlan]
,[NotResideInUS]
,[IncludedSharedSavingsInitiative]
,[NoPhysicianVisitACO]
,t1.[FileDate]
,t1.[FileTimeline]
,t1.[FileInputName]
,[StatusDesc]
--,CCN
,CASE WHEN [ACO Participant Type] = 'CCN'
      THEN T2.[ACO Participant TIN Number]
 END AS CCN
,[MonthlyEligibilityFlag1]
,[MonthlyEligibilityFlag2]
,[MonthlyEligibilityFlag3]
,[MonthlyEligibilityFlag4]
,[MonthlyEligibilityFlag5]
,[MonthlyEligibilityFlag6]
,[MonthlyEligibilityFlag7]
,[MonthlyEligibilityFlag8]
,[MonthlyEligibilityFlag9]
,[MonthlyEligibilityFlag10]
,[MonthlyEligibilityFlag11]
,[MonthlyEligibilityFlag12]
,[HCC1]
,[HCC2]
,[HCC5]
,[HCC7]
,[HCC8]
,[HCC9]
,[HCC10]
,[HCC15]
,[HCC16]
,[HCC17]
,[HCC18]
,[HCC19]
,[HCC21]
,[HCC25]
,[HCC26]
,[HCC27]
,[HCC31]
,[HCC32]
,[HCC33]
,[HCC37]
,[HCC38]
,[HCC44]
,[HCC45]
,[HCC51]
,[HCC52]
,[HCC54]
,[HCC55]
,[HCC67]
,[HCC68]
,[HCC69]
,[HCC70]
,[HCC71]
,[HCC72]
,[HCC73]
,[HCC74]
,[HCC75]
,[HCC77]
,[HCC78]
,[HCC79]
,[HCC80]
,[HCC81]
,[HCC82]
,[HCC83]
,[HCC92]
,[HCC95]
,[HCC96]
,[HCC100]
,[HCC101]
,[HCC104]
,[HCC105]
,[HCC107]
,[HCC108]
,[HCC111]
,[HCC112]
,[HCC119]
,[HCC130]
,[HCC131]
,[HCC132]
,[HCC148]
,[HCC149]
,[HCC150]
,[HCC154]
,[HCC155]
,[HCC157]
,[HCC158]
,[HCC161]
,[HCC164]
,[HCC174]
,[HCC176]
,[HCC177]
,[HCCRiskScoreESRD]
,[HCCRiskScoreDisabled]
,[HCCRiskScoreAgedDual]
,[HCCRiskScoreAgedNonDual]
FROM #fullMemberList t1
LEFT JOIN [dbo].[MemberRosterArchives_Table12] t2
ON t1.[HICNO] = t2.[HICNO]
AND t1.[FileDate] = t2.[FileDate]
AND t1.[ACO] = t2.[ACO]
LEFT JOIN [dbo].[MemberRosterArchives_table14] t4
ON t2.[HICNO] = t4.[HICNO]
AND t2.[ACO Participant TIN Number] = t4.[ACO Participant TIN Number]
AND t2.[FileDate] = t4.[FileDate]
AND t2.[ACO] = t4.[ACO]

INSERT INTO [MemberRosterArchives]
(
[HICNO]
,[First Name]
,[Last Name]
,[Sex1]
,[Birth Date]
,[DateofDeath]
,[Deceased Beneficiary Flag2]
,[ACO]
,[AssignmentFlag]
,[PrevAssignmentFlag]
,[ACO Participant TIN Number]
,[Count of Primary Care Services3]
,[ACO Participant Type]
,[IndividualNPI]
,[FileDate]
,[FileTimeline]
,[FileInputName]
,[StatusDesc]
,CCN
,[HCCRiskScoreESRD]
,[HCCRiskScoreDisabled]
,[HCCRiskScoreAgedDual]
,[HCCRiskScoreAgedNonDual]
)
SELECT 
 t1.[HICNO]
,t1.[First Name]
,t1.[Last Name]
,t1.[Sex1]
,t1.[Birth Date]
,t1.[DateofDeath]
,t1.[Deceased Beneficiary Flag2]
,t1.[ACO]
,t1.[AssignmentFlag]
,t1.[PrevAssignmentFlag]
,t1.[ACO Participant TIN Number]
,t1.[Count of Primary Care Services3]
,t1.[ACO Participant Type]
,t1.[IndividualNPI]
,t6.[FileDate]
,t6.[FileTimeline]
,t6.[FileInputName]
,'Dropped'
,t1.CCN
,t1.[HCCRiskScoreESRD]
,t1.[HCCRiskScoreDisabled]
,t1.[HCCRiskScoreAgedDual]
,t1.[HCCRiskScoreAgedNonDual]
FROM [MemberRosterArchives] t1
INNER JOIN #tmp t2
ON t1.[HICNO] = t2.[HICNO]
AND t1.[ACO] = t2.[ACO]
LEFT JOIN [dbo].[MemberRosterArchives_Table11] t3
ON t2.[HICNO] = t3.[HICNO]
AND t2.[ACO] = t3.[ACO]
LEFT JOIN [dbo].[MemberRosterArchives_Table15] t5
ON t2.[HICNO] = t5.[HICNO]
AND t2.[ACO] = t5.[ACO]
JOIN 
(
SELECT DISTINCT [FileDate],[FileTimeline],[FileInputName] FROM [dbo].[MemberRosterArchives_Table11]) t6
ON 1= 1
WHERE t3.[HICNO] IS NULL AND t5.[HICNO] IS NULL AND t2.StatusDesc <> 'Dropped'

UPDATE [dbo].[MemberRosterArchives]
SET [Deceased Beneficiary Flag2] = CASE WHEN [DateofDeath] IS NULL OR RTRIM(LTRIM([DateofDeath])) = '' THEN 0 ELSE 1 END
WHERE [Deceased Beneficiary Flag2] IS NULL

UPDATE MemberRosterArchives
SET	TimeLine = Substring(FileTimeLine,6,4) + '-' +
	CASE CASE WHEN Substring(FileTimeLine,10,1) = ',' THEN Substring(FileTimeLine,20,1) ELSE Substring(FileTimeLine,19,1) END 
		 WHEN 1 THEN '01' WHEN 2 THEN '04' WHEN 3 THEN '07' WHEN 4 THEN '10' END + '-01'
WHERE TimeLine IS NULL AND FileTimeLine LIKE 'Year%Quarter%'

UPDATE MemberRosterArchives
SET	TimeLine =
	CAST(CAST(Substring(FileTimeLine,6,4) AS INT)-1 AS CHAR(4)) + '-12-31'	
FROM MemberRosterArchives
WHERE TimeLine IS NULL AND FileTimeLine LIKE 'Year%Preliminary Prospective'

DROP TABLE #fullMemberList
DROP TABLE #tmp


END


GO
