SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproGetPatientListByBusinessUnit]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2016-11-11
	INITIAL VER:	2.2.1
	MODULE:			GPRO		
	DESCRIPTION:	Returns the list of patients that are in the GPRO ranking table and assigned to the selected business unit or below
	PARAMETERS:		Business Unit id
	RETURN VALUE(s)/OUTPUT:	a table containing the list of GproPatientRankingIds
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		11.11.2016		BR						Initial Version

=================================================================*/
(
	@businessUnitId INT
)
AS
BEGIN
    SET NOCOUNT ON;

	CREATE TABLE #patientList
	(
		GproPatientRankingId INT,
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	DECLARE @EdwDbName VARCHAR(200) = DB_NAME();
	DECLARE @TranDbName VARCHAR(200) = REPLACE(@edwDbName,'Edw','Lbportal') 
	DECLARE @Sql NVARCHAR(4000);
	DECLARE @Where VARCHAR(4000) = '';
	DECLARE @useGproAttribution SMALLINT = 0;
	DECLARE @params nvarchar(500);

	SET @Sql = N'SELECT @attributionPref = (CASE WHEN SetValue = ''true'' THEN 1 ELSE 0 END) 
		FROM '+ @tranDbName +'..SystemSettings WHERE SettingType = ''GPro'' AND SettingParameter = ''UseGproAttribution'''
	SET @params='@attributionPref smallint OUTPUT'
	EXEC sp_executesql @Sql, @params, @attributionPref=@useGproAttribution OUTPUT

	--if they are not viewing the top-most level of the hierarchy limit patients to the current level and below
	IF NOT EXISTS (SELECT 1 FROM vwPortal_BusinessUnit pbu WHERE pbu.BusinessUnitId = @businessUnitId AND ParentBusinessUnitId IS NULL)
	BEGIN
		IF (@useGproAttribution = 1)
		BEGIN
			SET @Where = ' WHERE (gpr.GroupTin IN (SELECT bu.GroupTin FROM vwPortal_BusinessUnit bu WHERE bu.DeleteInd <> 1 AND bu.BusinessUnitId IN (SELECT BusinessUnitId FROM ' + @TranDbName + '..[fnGetHierarchy] (' + CAST(@businessUnitId AS VARCHAR(20)) + '))))
			OR (gpr.ClinicIdentifier IN
			(SELECT bu.ExternalCode FROM vwPortal_BusinessUnit bu WHERE bu.DeleteInd <> 1 AND bu.BusinessUnitId IN (SELECT BusinessUnitId FROM ' + @TranDbName + '..[fnGetHierarchy] (' + CAST(@businessUnitId AS VARCHAR(20)) + '))))';			
		END
		ELSE
		BEGIN
			SET @Where = ' WHERE EXISTS (SELECT 1 FROM vwPortal_PatientHealthcareOrg phorg 
			INNER JOIN vwPortal_HealthcareOrg horg ON horg.HealthcareOrgId = phorg.HealthcareOrgId 
				AND horg.BusinessUnitId IN  (SELECT BusinessUnitId FROM ' + @TranDbName + '..[fnGetHierarchy] (' + CAST(@businessUnitId AS VARCHAR(20)) + '))
			WHERE phorg.PatientId = gpr.LbPatientId)';
		END
	END

	SET @Sql = 'INSERT INTO #patientList
		(GproPatientRankingId)
	SELECT
		gpr.GproPatientRankingId
	FROM ' + @EdwDbName + '..GproPatientRanking gpr' + @Where;
	EXEC (@Sql);


	SELECT * FROM #patientList;
END
GO
