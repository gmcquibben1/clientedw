SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[ETLPatientDedupeFuzzy]  
AS
BEGIN


/*===============================================================

	CREATED BY: 	MH
	CREATED ON:		01.14.2016
	INITIAL VER:	2.1.1
	MODULE:			EDW ETL		
	DESCRIPTION: 	Levenshtahl distance patient de-depulication
	PARAMETERS:		
				

	RETURN VALUE(s)/OUTPUT:	NONE
	
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.6.1		01.14.2017		MH			LBAN-3490,  Creating Fuzzy Matching type based on External ID from a specific source		


														
=================================================================*/

		------------------------------------------------------------------------------
	-- Step 2: Select Winning Patient Id 
	-- inPid is always smaller than matchedPid. 
	-- We will use the smallest PatientId as the winnder 
	-- for all matched patient dupe pairs. 
	------------------------------------------------------------------------------
	

	SELECT DISTINCT 		matchedFirstName AS FirstName
           ,MatchedLastName AS [LastName]
           ,matchedDOB AS [BirthDate]
           ,Gender
		,FM.inPid AS newPatientId , FM.matchedPid AS PatientID
	INTO #TMP2
	FROM [dbo].[FuzzyMatchResult] FM
  JOIN FuzzyMatchType FMT ON FM.FuzzyMatchTypeId = FMT.FuzzyMatchTypeId
	WHERE FM.similarityScore >= FMT.AutoMatchThreshold
	AND FM.isMerged <> 1 and FM.inPid < FM.matchedPid;


		-- find out smaller PatientId
	WITH PID_CTE AS (
		SELECT FirstName, LastName, BirthDate, Gender, newPatientId, PatientId , null as smallerPid , 1 as plevel
		FROM #TMP2 T
		WHERE newPatientId < PatientId

		UNION ALL

		-- picker larger pid as winnder
		--SELECT R.FirstName, R.LastName, R.BirthDate, R.Gender, R.newPatientId, R.PatientId , C1.PatientId as biggerPid 
		--FROM #TMP2 R
		--INNER JOIN PID_CTE C1 ON C1.newPatientId = R.PatientId

		-- pick smaller Pid as winner
		SELECT R.FirstName, R.LastName, R.BirthDate, R.Gender, R.newPatientId, R.PatientId , C1.newPatientId as smallerPid , C1.plevel+1 as plevel
		FROM #TMP2 R
		INNER JOIN PID_CTE C1 ON R.newPatientId = C1.PatientId
	)
	SELECT distinct C.* , 'compared-to-origin' as comments1 , T2.newPatientId as oNewPid, T2.PatientId as oPid
		into #TMP4
	FROM PID_CTE C
		join #TMP2 T2
		on C.smallerPid = T2.newPatientId
		where C.smallerPid is not NULL

		select distinct FirstName, LastName, BirthDate, Gender, oNewPid, oPid, mpid 
		into #TMP5
		from (
		select T.*, min(smallerPid) over(partition by newPatientId, PatientId) mpid
		 from #TMP4 T
		 ) A 

		 update T2
		set T2.newPatientId = T5.mpid 
		 from #TMP2 T2 join #TMP5 T5
		 on T2.PatientId = T5.oPid
 

	------------------------------------------------------------------------------
	-- Step 3: Loop thru Target tables to be updated
	------------------------------------------------------------------------------

	DECLARE @DbLocation VARCHAR(100), @DependencyTable VARCHAR(100), @TablePk VARCHAR(100),  
			@TablePKDataType VARCHAR(100),
			@UpdatingColumn VARCHAR(100),@UpdatingColumnDataType VARCHAR(100), @ResolvingForId  VARCHAR(100),
			@DynSql NVARCHAR(MAX) = '', @TOBJECT_ID BIGINT, @COBJECT_ID BIGINT ,
			@ErrorMsg VARCHAR(4000)


	DECLARE MyCur CURSOR FAST_FORWARD FOR 
	SELECT DbLocation, 
		TargetTable, 
		PrimaryKeyName,
		UpdatingColumn  
	FROM [dbo].[DedupeTables]
	WHERE   PrimaryKeyName IS NOT NULL
	ORDER BY DbLocation ASC , TargetTable ASC 

	SET @DynSql = ''
	OPEN MyCur FETCH NEXT FROM MyCur INTO @DbLocation, @DependencyTable, @TablePk, @UpdatingColumn 	

	WHILE @@FETCH_STATUS = 0
	BEGIN

		-- Update Audit table
		-- sample 
		/*
	 		INSERT INTO [dbo].[DedupePatientIdAudit]
			   ([DedupeTableName]
			   ,[DedupeTablePKType]
			   ,[DedupeTablePKValue]
			   ,[OldPatientId]
			   ,[NewPatientId]
			   ,[Description]
			   ,[EventDateTime])
		select 'PatientEnrollment' , 
				'Int' , 
				TGT.PatientEnrollmentId, 
				SS.PatientId , 
				SS.NewPatientId , 
				null, 
				GetUTCDate()  
		from VfpLbPortalTest.dbo.PatientEnrollment TGT 
		inner join #TMP2 SS  
		ON TGT.PatientId = SS.PatientId
		*/

		----------------------------------------------------------------------------------
		-- Step 4: Keep an audit record of each statement being issued
		----------------------------------------------------------------------------------

		SET @DynSql = '	INSERT INTO [dbo].[DedupePatientIdAudit]
			   ([DedupeTableName]
			   ,[DedupeTablePKType]
			   ,[DedupeTablePKValue]
			   ,[OldPatientId]
			   ,[NewPatientId]
			   ,[Description]
			   ,[EventDateTime])
		SELECT ''' + @DependencyTable + ''' , ''' + 'Int' + ''' ,' +
		' TGT.' + @TablePk + ', SS.PatientId , SS.NewPatientId , null, GetUTCDate() '  + 
		' from ' + @DbLocation + '.dbo.' + @DependencyTable + ' TGT INNER JOIN #TMP2 SS ' + 
		' ON TGT.' + @UpdatingColumn + ' = SS.PatientId'

		--SELECT (@DynSql)
		INSERT INTO DedupeExecuteLog ( errorMsg, createTime ) SELECT @DynSql, GetUTCDate();

		BEGIN TRY
			EXEC   (@DynSql) --run it
		END TRY
		BEGIN CATCH
				SET @ErrorMsg =  'Failed : ' + @DynSql + CAST(ERROR_LINE() AS VARCHAR(100))  + ' Message=' + 
			CAST(ERROR_MESSAGE() AS VARCHAR(1000))  + ' ErrorNumber=' + 
			CAST(ERROR_NUMBER() AS VARCHAR(100)) + ' Severity=' + 
			CAST(ERROR_SEVERITY() AS VARCHAR(100)) + ' ErrState=' + 
			CAST(ERROR_STATE() AS VARCHAR(100)) 

			INSERT INTO DedupeExecuteLog ( errorMsg, createTime ) SELECT @ErrorMsg, GetUTCDate();
		END CATCH

		------------------------------------------------------------------------------
		-- Step 5:  Issue the Update against each target table with winning Patient Id
		-- use smaller Pid as winner
		------------------------------------------------------------------------------
		
		/* sample 
		 UPDATE VfpLbPortalTest.dbo.PatientEnrollment SET PatientId = SS.newPatientId   
		FROM VfpLbPortalTest.dbo.PatientEnrollment PP INNER JOIN #TMP2 SS   ON PP.PatientId = SS.PatientId
		*/

		SET @DynSql = ' UPDATE ' + @DbLocation + '.dbo.' + @DependencyTable +  
					  ' SET ' + @UpdatingColumn + ' = SS.newPatientId  '  + --, ModifyDateTime = Getdate()
					  ' FROM ' + @DbLocation + '.dbo.' + @DependencyTable + ' PP INNER JOIN #TMP2 SS  ' + 
					  ' ON PP.' + @UpdatingColumn + ' = SS.PatientId'  
		--SELECT (@DynSql)  
		INSERT INTO DedupeExecuteLog ( errorMsg, createTime ) SELECT @DynSql, GetUTCDate();

		BEGIN TRY
			EXEC   (@DynSql) --run it
		END TRY
		BEGIN CATCH
				SET @ErrorMsg =  'Failed : ' + @DynSql + CAST(ERROR_LINE() AS VARCHAR(100))  + ' Message=' + 
			CAST(ERROR_MESSAGE() AS VARCHAR(1000))  + ' ErrorNumber=' + 
			CAST(ERROR_NUMBER() AS VARCHAR(100)) + ' Severity=' + 
			CAST(ERROR_SEVERITY() AS VARCHAR(100)) + ' ErrState=' + 
			CAST(ERROR_STATE() AS VARCHAR(100)) 

			INSERT INTO DedupeExecuteLog ( errorMsg, createTime ) SELECT @ErrorMsg, GetUTCDate();
		END CATCH

		FETCH NEXT FROM MyCur INTO @DbLocation, @DependencyTable, @TablePk, @UpdatingColumn 
	
	END  --WHILE
	CLOSE MyCur 
	DEALLOCATE MyCur 

	------------------------------------------------------------------------------
	-- Step 6:  Update those tables with DeleteInd flag
	------------------------------------------------------------------------------
	BEGIN TRY
			

	/*
	 * Mark DeleteInd = 1 for all 
	 * Individual's FK related demographics tables
	 */
	UPDATE vwPortal_IndividualName
		SET DeleteInd = 1
	FROM vwPortal_IndividualName I 
		JOIN vwPortal_Patient P ON I.IndividualID = P.IndividualID
		JOIN #TMP2 T ON P.PatientId = T.PatientId

	UPDATE vwPortal_IndividualEmail
		SET DeleteInd = 1
	FROM vwPortal_IndividualEmail I 
		JOIN vwPortal_Patient P ON I.IndividualID = P.IndividualID
		JOIN #TMP2 T ON P.PatientId = T.PatientId

	UPDATE vwPortal_IndividualStreetAddress
		SET DeleteInd = 1
	FROM vwPortal_IndividualStreetAddress I 
		JOIN vwPortal_Patient P ON I.IndividualID = P.IndividualID
		JOIN #TMP2 T ON P.PatientId = T.PatientId

	UPDATE vwPortal_IndividualTelephoneNumber
		SET DeleteInd = 1
	FROM vwPortal_IndividualTelephoneNumber I 
		JOIN vwPortal_Patient P ON I.IndividualID = P.IndividualID
		JOIN #TMP2 T ON P.PatientId = T.PatientId

	UPDATE vwPortal_Individual
		SET DeleteInd = 1
	FROM vwPortal_Individual I 
		JOIN vwPortal_Patient P ON I.IndividualID = P.IndividualID
		JOIN #TMP2 T ON P.PatientId = T.PatientId

	UPDATE vwPortal_Patient
		SET DeleteInd = 1
	FROM vwPortal_Patient P 
		JOIN #TMP2 T ON P.PatientId = T.PatientId  -- changed to delete smaller pid

	UPDATE vwPortal_Party
		SET DeleteInd = 1
	FROM vwPortal_Party  PTY
		JOIN vwPortal_Individual I ON PTY.PartyId = I.PartyId
		JOIN vwPortal_Patient P ON I.IndividualID = P.IndividualID
		JOIN #TMP2 T ON P.PatientId = T.PatientId

	------------------------------------------------------------------------------
	-- Step 7:  Log all PatientIds de-duped into history table
	------------------------------------------------------------------------------
	INSERT INTO [dbo].[DedupedPatientIdHistory]
           ([FirstName]
           ,[LastName]
           ,[BirthDate]
           ,[Gender]
           ,[PatientId]
           ,[newPatientId]
           ,[createTime])	--** should change to CreateDateTime
	SELECT [FirstName]
           ,[LastName]
           ,[BirthDate]
           ,[Gender]
           ,[PatientId]
           ,[newPatientId]
		   ,GetUTCDate() 
	FROM #TMP2

	-- update the FuzzyMatchResult to mark it merged.
	UPDATE FR
	SET FR.isMerged = 1 
	FROM [dbo].[FuzzyMatchResult] FR
	JOIN #TMP2 T
	ON FR.inPid = T.newPatientId


	DROP TABLE #TMP2

	END TRY
		BEGIN CATCH
				SET @ErrorMsg =  'Failed : ' + @DynSql + CAST(ERROR_LINE() AS VARCHAR(100))  + ' Message=' + 
			CAST(ERROR_MESSAGE() AS VARCHAR(1000))  + ' ErrorNumber=' + 
			CAST(ERROR_NUMBER() AS VARCHAR(100)) + ' Severity=' + 
			CAST(ERROR_SEVERITY() AS VARCHAR(100)) + ' ErrState=' + 
			CAST(ERROR_STATE() AS VARCHAR(100)) 

			INSERT INTO DedupeExecuteLog ( errorMsg, createTime ) SELECT @ErrorMsg, GetUTCDate();
		END CATCH
END 

GO
