SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@IVD-2] @LookbackTimestamp DATETIME='2000-01-01 00:00:00', @dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];

	DECLARE  @yearend datetime, @yearBegin datetime;
	SET @yearBegin = '01/01/2015' 
	SET @yearEnd   = '01/01/2016';

	--Establishing the Clinical Data sourcing indicators.
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	--Initial Patient pooling, for patients that have ranking for the BCS measure. 
	SELECT r.LBPatientID,
        r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,@yearBegin) as Age,
		r.Birthdate,
        GM.GproMeasureTypeID,
		r.GenderCode,
		GPM.[IVDConfirmed],
		GPM.[IVDAntithrombotic]
	INTO #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GproPatientRankingId = r.GproPatientRankingID
	WHERE  Name = 'IVD-2' 
		AND (GPM.IvdConfirmed IS NULL OR GPM.IvdAntithrombotic IS NULL )
   
	--------------------------------------------------------------------------------------------------------    
	-- Denominator Checks - Patient has either CABG, AMI, or PCI 12months prior to the measuremnet period,
	-- Or, had an active diagnosis of IVD during the measurement period.
	--------------------------------------------------------------------------------------------------------    
      
	IF OBJECT_ID( 'tempdb..#DIAG' ) IS NOT NULL 
		DROP TABLE #DIAG;

    SELECT DISTINCT RP.*
    INTO #DIAG
    FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON RP.LbPatientId = pd.PatientId
			AND pd.DiagnosisDateTime < @yearBegin
			AND (pd.ActiveInd <> 0 OR (pd.DiagnosisResolvedDate IS NULL OR pd.DiagnosisResolvedDate > @yearBegin))
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = 'IVD'
			AND gec.[VariableName] in ('AMI_CODE','CABG_CODE','PCI_CODE')  
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pd.SourceSystemId    
	UNION
  --or who had an active diagnosis of ischemic vascular disease (IVD) during the measurement period
    SELECT DISTINCT RP.*
    FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON RP.LbPatientId = pd.PatientId
			AND pd.DiagnosisDateTime < @yearBegin
			AND (pd.ActiveInd <> 0 OR (pd.DiagnosisResolvedDate IS NULL OR pd.DiagnosisResolvedDate > @yearBegin))
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = 'IVD'
			AND gec.[VariableName] = 'IVD_DX_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pd.SourceSystemId    

                                                
	--  --updating patients without a necessary denominator hit.
	--	UPDATE #RankedPatients
	--  SET    [IVDConfirmed] = '8'
	--  FROM   #RankedPatients RP
	--  LEFT JOIN 
	--         #DIAG D
	--            ON RP.LBPATIENTID = D.LBPATIENTID
	--  Where RP.LBPATIENTID IS NULL 
  
	--setting the patients who are in the denom to confirmed.
	UPDATE RP
	SET    RP.[IVDConfirmed] = '2'
	FROM   #RankedPatients RP
		JOIN   #DIAG D ON RP.LbPatientId = D.LbPatientId
  
	--------------------------------------------------------------------------------------------------------          
	--NUMERATOR 
	--Patients who had a complete lipid profile performed during the measurement period
	--------------------------------------------------------------------------------------------------------      
  
	IF OBJECT_ID( 'tempdb..#Num' ) IS NOT NULL 
		DROP TABLE #Num;

    SELECT DISTINCT RP.*
    INTO #Num
    FROM #RankedPatients RP
		INNER JOIN vwPatientMedication pm ON pm.PatientId = RP.LbPatientId
			AND ISNULL(pm.MedicationStartDate, '1990-01-01') < @yearEnd
			AND ISNULL(pm.MedicationEndDate, DATEADD(day, -1,  @yearEnd ) ) > @yearBegin
		INNER JOIN GproDrugCode gdc ON (gdc.Code = pm.NDCCode OR gdc.Code = pm.RxNormCode)
			AND gdc.[ModuleType] = 'IVD' 
			AND gdc.[ModuleIndicatorGPRO] = '2' 
			AND gdc.[VariableName] ='ASA_DRUG_CODE'
		INNER JOIN @SourceIdTable SS ON  SS.SourceSystemId = pm.SourceSystemID    --MEDS
    WHERE RP.[IvdConfirmed] = '2'
    

    UPDATE RP
    SET    RP.[IVDAntithrombotic] = '2'
    FROM   #RANKEDPATIENTS RP
    JOIN   #NUM NUM
           on RP.LbPatientId = NUM.lbpatientid
    
	--------------------------------------------------------------------------------------------------------          
	   -- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	UPDATE gpm
	SET gpm.IvdConfirmed = gpr.IvdConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.IvdConfirmed IS NULL

	UPDATE gpm
	SET gpm.IvdAntithrombotic = gpr.IvdAntithrombotic, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.IvdAntithrombotic IS NULL


END

     
GO
