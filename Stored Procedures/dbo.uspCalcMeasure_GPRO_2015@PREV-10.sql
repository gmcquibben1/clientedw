SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@PREV-10] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];
	IF OBJECT_ID('tempdb..#SCREENING') IS NOT NULL 
		DROP TABLE [#SCREENING];
	IF OBJECT_ID('tempdb..#Screen_results') IS NOT NULL 
		DROP TABLE [#Screen_results];
	IF OBJECT_ID('tempdb..#plan') IS NOT NULL 
		DROP TABLE [#plan];

	DECLARE  @yearend datetime, @measureBeginDate datetime;
	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2015';

	--Establishing the Clinical Data sourcing indicators.
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	--Initial Patient pooling, for patients that have ranking for the PREV-10 measure. 
	SELECT r.LBPatientId,
        r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,@measureBeginDate) as Age,
		r.Birthdate,
        GM.GproMeasureTypeID,
		GPM.PcTobaccoRank,
		GPM.PcTobaccoScreen,
        GPM.PcTobaccoConfirmed,
        GPM.PcTobaccoCounsel,
		GETUTCDATE() as ModifyDateTime
	INTO   #RankedPatients --	Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE GM.Name = 'PREV-10' 
		AND (GPM.PcTobaccoConfirmed IS NULL OR GPM.PcTobaccoScreen IS NULL OR GPM.PcTobaccoCounsel IS NULL)
    
	
	--tobacco screening once within 24 month
	SELECT DISTINCT sub.*
	INTO  #SCREENING -- DROP TABLE #SCREENING
	FROM (SELECT DISTINCT 
			stage.*	, 
			plr.[ObservationDate] screen_dt,
			ROW_NUMBER () OVER (PARTITION BY stage.LBPatientId ORDER BY plr.[ObservationDate] DESC) latest_SCREEN_DT
		FROM #RankedPatients stage
			JOIN [dbo].[PatientLabOrder] plo ON  plo.[PatientId] = stage.[LbPatientId]
			JOIN [dbo].[PatientLabResult] plr ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
				AND plr.[ObservationDate] >= DATEADD(year,-1,@measureBeginDate) --dateadd(year,-1,'1-1-2015')-- 
				AND plr.[ObservationDate] <  @yearend --'1-1-2016'--
			JOIN [dbo].[GproEvaluationCode] hvsc ON  hvsc.[Code] = plr.[ObservationCode1]
				AND hvsc.[ModuleType]= 'PREV'
				AND hvsc.[ModuleIndicatorGPRO]='10'
				AND HVSC.[VariableName] IN   ('TOBACCO_SCREEN_CODE')
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID]    --LABS
		WHERE Stage.AGE >= 18
		) AS sub
	WHERE latest_SCREEN_DT = 1 -- take most recent screen date
	

	--check if non-user or user RESULT on screen date
	SELECT DISTINCT 
		RP.*,
		HVSC.[VariableName]
	INTO #Screen_results
	FROM #SCREENING RP
		JOIN dbo.patientProcedure PP ON RP.LBPatientId = PP.PatientId
		JOIN dbo.patientProcedureProcedureCode PPC ON pp.[PatientProcedureId] = ppc.[PatientProcedureId]
			AND PP.PROCEDUREDATETIME = RP.screen_dt -- on same date IF CODE AS TOBACCO USER OR NOT
		JOIN dbo.ProcedureCode PC ON PPC.PROCEDURECODEID = PC.PROCEDURECODEID
		JOIN DBO.[GproEvaluationCode] HVSC ON PC.ProcedureCode = HVSC.CODE
			AND hvsc.[ModuleType]= 'PREV'
			AND hvsc.[ModuleIndicatorGPRO]='10'
			AND HVSC.[VariableName] IN   ('TOBACCO_NON_USER_CODE','TOBACCO_USER_CODE')
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
			

	--check if TOBACCO USER was code for CESSATION COUNSELING
	SELECT DISTINCT sr.*
	INTO #plan
	FROM #Screen_results sr
		JOIN dbo.patientProcedure PP ON sr.LBPatientId = PP.PatientId
		JOIN dbo.patientProcedureProcedureCode PPC ON pp.[PatientProcedureId] = ppc.[PatientProcedureId]
			AND PP.ProcedureDateTime = sr.screen_dt -- on same date AS CODE FOR TOBACCO USER
		JOIN dbo.ProcedureCode PC ON PPC.ProcedureCodeId = PC.ProcedureCodeId
		JOIN DBO.[GproEvaluationCode] HVSC ON PC.ProcedureCode = HVSC.CODE
			AND hvsc.[ModuleType]= 'PREV'
			AND hvsc.[ModuleIndicatorGPRO]='10'
			AND HVSC.[VariableName] IN   ('CESSA_COUNSEL_CODE')
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
	WHERE sr.[VariableName] = 'TOBACCO_USER_CODE'

	--patient age comfirmed
	UPDATE rp
	SET rp.PcTobaccoConfirmed = '2' -- yes
	FROM #RankedPatients rp
	WHERE rp.Age >= 18
		AND rp.PcTobaccoConfirmed IS NULL

	-- not confirmed
	--update #RankedPatients
	--set PcTobaccoConfirmed = '19' -- not confirmed
	----	SELECT DISTINCT rp.*
	--FROM #RankedPatients rp
	--left join #ENC enc on rp.lbPatientId = enc.lbPatientId
	--WHERE enc.lbPatientId is NULL
	--	and rp.PcTobaccoConfirmed is NULL

	--tobacco screen and tobacco user
	UPDATE rp
	SET rp.PcTobaccoScreen = '2'
	FROM #RankedPatients rp
		JOIN #SCREENING enc on rp.lbPatientId = enc.lbPatientId
		JOIN #Screen_results sr on rp.LBPatientId = sr.LBPatientId
			AND sr.[VariableName] = 'TOBACCO_USER_CODE'
	WHERE rp.PcTobaccoScreen IS NULL

	--update #RankedPatients
	--set pcTobaccoscreen = NULL

	--tobacco screen and non tobacco user
	UPDATE rp
	SET rp.PcTobaccoScreen = '1'
	FROM #RankedPatients rp
		JOIN #SCREENING enc on rp.lbPatientId = enc.lbPatientId
		JOIN #Screen_results sr on rp.LBPatientId = sr.LBPatientId
			AND sr.[VariableName] = 'TOBACCO_NON_USER_CODE'
	WHERE rp.PcTobaccoScreen is NULL

	--------------------------------------------------------------------------------
	-- smoking status in social history implies they were screened...
	--------------------------------------------------------------------------------
	--UPDATE rp
	--SET PcTobaccoScreen = '14'
	--FROM #RankedPatients rp
	--	INNER JOIN PatientSocialHistory psh ON psh.PatientId = rp.LbPatientId AND psh.SmokingStatus = '266927001'
	--WHERE rp.PcTobaccoScreen IS NULL

	UPDATE rp
	SET PcTobaccoScreen = '2'
	FROM #RankedPatients rp
		INNER JOIN (SELECT PatientId, SmokingStatus, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY CreateDateTime DESC) r FROM PatientSocialHistory WHERE SmokingStatus <> '266927001') psh ON psh.PatientId = rp.LbPatientId AND psh.r = 1
		INNER JOIN HEDISValueSetCodes hvsc ON hvsc.Code = psh.SmokingStatus AND hvsc.[Value Set Name] = 'Current Smoker' 
	WHERE rp.PcTobaccoScreen IS NULL

	UPDATE rp
	SET PcTobaccoScreen = '1'
	FROM #RankedPatients rp
		INNER JOIN (SELECT PatientId, SmokingStatus, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY CreateDateTime DESC) r FROM PatientSocialHistory WHERE SmokingStatus <> '266927001') psh ON psh.PatientId = rp.LbPatientId AND psh.r = 1
		INNER JOIN HEDISValueSetCodes hvsc ON hvsc.Code = psh.SmokingStatus AND hvsc.[Value Set Name] like 'Non-Smoker' 
	WHERE rp.PcTobaccoScreen IS NULL

	


	----not screen for tobacco use
	--update #RankedPatients
	--set pcTobaccoscreen = '14' -- no
	----	SELECT DISTINCT rp.*
	--FROM #RankedPatients rp
	--left join #SCREENING enc on rp.lbPatientId = enc.lbPatientId
	--WHERE enc.lbPatientId is NULL
	--and rp.pcTobaccoscreen is NULL

	---tobacco conuseel
	UPDATE rp
	SET rp.PcTobaccoCounsel = '2' -- yes
	FROM #RankedPatients rp
		JOIN #plan enc on rp.lbPatientId = enc.lbPatientId
	WHERE rp.PcTobaccoCounsel is NULL
		AND rp.PcTobaccoScreen = '2'

	--
	---- non tobacco conuseel
	--update #RankedPatients
	--set PcTobaccoCounsel = '1' -- no
	----	SELECT DISTINCT rp.*
	--FROM #RankedPatients rp
	--left join #plan enc on rp.lbPatientId = enc.lbPatientId
	--WHERE enc.lbPatientId is NULL
	--	and rp.PcTobaccoCounsel is NULL

	--------------------------------------------------------------------------------------------------------          
	-- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	UPDATE gpm
	SET   gpm.PcTobaccoConfirmed = gpr.PcTobaccoConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcTobaccoRank IS NOT NULL
		AND gpm.PcTobaccoConfirmed IS NULL
		
	UPDATE gpm
	SET   gpm.PcTobaccoScreen = gpr.PcTobaccoScreen, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcTobaccoRank IS NOT NULL
		AND gpm.PcTobaccoScreen IS NULL

	UPDATE gpm
	SET   gpm.PcTobaccoCounsel = gpr.PcTobaccoCounsel, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcTobaccoRank IS NOT NULL
		AND gpm.PcTobaccoCounsel IS NULL
		
		
END
GO
