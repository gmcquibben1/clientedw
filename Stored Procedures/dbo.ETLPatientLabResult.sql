SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[ETLPatientLabResult] 
WITH RECOMPILE

AS
/* ================================================================
 Author:		Youping Liu
 Create date:	2016-04-01
 Description:	Load PatientLabResult Data into EDW
 Notes: 
 

 Main Steps: 
        1. Get PatientLabOrderId from dbo.PatientLabOrder
        2. Dedup based on PatientLabOrderId, ObservationCode1, ObservationDate
        3. Merge data to PatientLabResult


 Log Tracking:
        1. track table the last run date: dbo.EDWTableLoadTracking
        2. log the procedure running status


	Version		Date		Author	Change
	-------		----------	------	------	
	2.0.1		2016-04-01	YL		Initial
									Fix LBDM 754  Dedup key 
	2.2.1		2016-09-29	YL	    LBETL-143 Fix Missing lab results due to incremental load based on laborder created date only
											It needs to use both laborder created date and lab result created date.
	2.2.1		2017-02-02	YL	    LBETL-631 maxSourceTimeStamp update only when Que has data
	2.2.1		2017-02-02	CL	    LBETL-788 Rollup multiple records to the latest non-null value
	2.2.1		2017-05-30	CL	    LBETL-788 Reworked rollup to use temp tables to improve performance on caravan systems
	
===============================================================*/



BEGIN	

	SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientLabResult';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientLabResult';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	
	DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY





			SELECT @RecordCountBefore=count(1)
			FROM dbo.PatientLabResult;





			--create the core lab results table.
			SELECT p.PatientLabOrderId, lbPatientId, q.OrderNumber, InterfaceSystemId As SourceSystemId,
					q.ObservationCode1, 
					ISNULL(q.ObservationDescription1, '') as ObservationDescription1, 
					ISNULL(q.ObservationCodingSystemName1,'') as ObservationCodingSystemName1,
					ISNULL( q.ObservationCode2, '') as ObservationCode2, 
					ISNULL( q.ObservationDescription2, '') as ObservationDescription2,
					ISNULL( q.ObservationCodingSystemName2, '') as ObservationCodingSystemName2,
					LR_CreateDateTime, q.ObservationDate,
					ROW_NUMBER() OVER (PARTITION BY p.PatientLabOrderId, q.ObservationCode1,	
					ISNULL(q.ObservationDescription1, '') , 
					ISNULL(q.ObservationCodingSystemName1,'') ,
					ISNULL( q.ObservationCode2, '') , 
					ISNULL( q.ObservationDescription2, '') ,
					ISNULL( q.ObservationCodingSystemName2, ''), 
					ISNULL(q.ObservationDate,'2099-12-31')
					ORDER BY [InterfacePatientLabOrderId] DESC) rowNumber 
			INTO #T1
					FROM
						 PatientLabOrderProcessQueue q WITH (NOLOCK)
					left outer join dbo.PatientLabOrder p  WITH (NOLOCK) 
						ON p.SourceSystemID = q.InterfaceSystemId AND p.PatientID = q.lbPatientId AND p.OrderNumber = q.OrderNumber
					WHERE q.InterfacePatientLabResultId IS NOT NULL  AND p.PatientLabOrderId IS NOT NULL
			CREATE CLUSTERED INDEX IX_#T1_Rollup
				 ON #T1 (lbPatientId, OrderNumber, SourceSystemId, 
				ObservationCode1,ObservationDescription1,ObservationCodingSystemName1, 
				ObservationCode2,ObservationDescription2, ObservationCodingSystemName2, ObservationDate);




			--Create a temp table with all of the values for rolling up
			SELECT  OrderNumber, InterfaceSystemId As SourceSystemId, Value, lbPatientId,
				ObservationCode1, ObservationDescription1, ObservationCodingSystemName1, 
				ObservationCode2, ObservationDescription2, ObservationCodingSystemName2, 
				ObservationDate,
				ROW_NUMBER() OVER 
				(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId 	,ISNULL(ObservationCode1, ''), ISNULL( ObservationDescription1, ''), ISNULL( ObservationCodingSystemName1, ''),
					ISNULL( ObservationCode2, ''), ISNULL( ObservationDescription2,''), ISNULL( ObservationCodingSystemName2, ''), ISNULL(ObservationDate,'2099-12-31')
					 ORDER BY InterfacePatientLabResultId DESC) rowNumber 
			INTO #val
			FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE Value IS NOT NULL
			CREATE CLUSTERED INDEX IX_#val_Rollup
				 ON #val (lbPatientId, OrderNumber, SourceSystemId, 
				ObservationCode1,ObservationDescription1,ObservationCodingSystemName1, 
				ObservationCode2,ObservationDescription2, ObservationCodingSystemName2, ObservationDate);



			--create a temp table for rolling up the unit of measures 
			SELECT  OrderNumber, InterfaceSystemId As SourceSystemId, Units, lbPatientId, 
				ObservationCode1, ObservationDescription1, ObservationCodingSystemName1, ObservationCode2, ObservationDescription2, ObservationCodingSystemName2, 
				ObservationDate,
			 ROW_NUMBER() OVER 
				(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId 		,ISNULL(ObservationCode1, ''), ISNULL( ObservationDescription1, ''), ISNULL( ObservationCodingSystemName1, ''),
					ISNULL( ObservationCode2, ''), ISNULL( ObservationDescription2,''), ISNULL( ObservationCodingSystemName2, ''), ISNULL(ObservationDate,'2099-12-31')
					 ORDER BY InterfacePatientLabResultId DESC) rowNumber 
			INTO #units
			FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE Units IS NOT NULL
			  
			CREATE CLUSTERED INDEX IX_#units_Rollup
				 ON #units (lbPatientId, OrderNumber, SourceSystemId, 
				ObservationCode1,ObservationDescription1,ObservationCodingSystemName1, 
				ObservationCode2,ObservationDescription2, ObservationCodingSystemName2, ObservationDate);





			--create a temp table for rolling up the reference units 
			SELECT  OrderNumber, InterfaceSystemId As SourceSystemId, ReferenceRange,lbPatientId,
				ObservationCode1, ObservationDescription1, ObservationCodingSystemName1, ObservationCode2, ObservationDescription2, ObservationCodingSystemName2, 
				ObservationDate,
				ROW_NUMBER() OVER 
			(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId , lbPatientId
				,ISNULL(ObservationCode1, ''), ISNULL( ObservationDescription1, ''), ISNULL( ObservationCodingSystemName1, ''),
				ISNULL( ObservationCode2, ''), ISNULL( ObservationDescription2,''), ISNULL( ObservationCodingSystemName2, ''), ISNULL(ObservationDate,'2099-12-31')
			ORDER BY InterfacePatientLabResultId DESC) rowNumber 
			INTO #ref
			FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE ReferenceRange IS NOT NULL
			  
			CREATE CLUSTERED INDEX IX_#ref_Rollup
				ON #ref (lbPatientId, OrderNumber, SourceSystemId, 
			ObservationCode1,ObservationDescription1,ObservationCodingSystemName1, 
			ObservationCode2,ObservationDescription2, ObservationCodingSystemName2, ObservationDate);


			--create a temp table to roll up the abnormal reference flags rollup
			SELECT  OrderNumber, InterfaceSystemId As SourceSystemId, AbnormalFlag,lbPatientId,
			ObservationCode1, ObservationDescription1, ObservationCodingSystemName1, ObservationCode2, ObservationDescription2, ObservationCodingSystemName2, 
			ObservationDate,
				ROW_NUMBER() OVER 
			(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId, lbPatientId
				,ISNULL(ObservationCode1, ''), ISNULL( ObservationDescription1, ''), ISNULL( ObservationCodingSystemName1, ''),
				ISNULL( ObservationCode2, ''), ISNULL( ObservationDescription2,''), ISNULL( ObservationCodingSystemName2, ''), ISNULL(ObservationDate,'2099-12-31')
				ORDER BY InterfacePatientLabResultId DESC) rowNumber 
			INTO #af
			FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE AbnormalFlag IS NOT NULL
			CREATE CLUSTERED INDEX IX_#af_Rollup
				ON #af (lbPatientId, OrderNumber, SourceSystemId, 
			ObservationCode1,ObservationDescription1,ObservationCodingSystemName1, 
			ObservationCode2,ObservationDescription2, ObservationCodingSystemName2, ObservationDate);


			
			--Create a temp table to roll up the result status values.
			SELECT  OrderNumber, InterfaceSystemId As SourceSystemId, ResultStatus,lbPatientId,
			ObservationCode1, ObservationDescription1, ObservationCodingSystemName1, ObservationCode2, ObservationDescription2, ObservationCodingSystemName2, 
			ObservationDate,
			ROW_NUMBER() OVER 
			(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId
			,ISNULL(ObservationCode1, ''), ISNULL( ObservationDescription1, ''), ISNULL( ObservationCodingSystemName1, ''),
			ISNULL( ObservationCode2, ''), ISNULL( ObservationDescription2,''), ISNULL( ObservationCodingSystemName2, ''), ISNULL(ObservationDate,'2099-12-31')
			ORDER BY InterfacePatientLabResultId DESC) rowNumber 
			INTO #rs
			FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE ResultStatus IS NOT NULL

			CREATE CLUSTERED INDEX IX_#rs_Rollup
				ON #rs (lbPatientId, OrderNumber, SourceSystemId, 
			ObservationCode1,ObservationDescription1,ObservationCodingSystemName1, 
			ObservationCode2,ObservationDescription2, ObservationCodingSystemName2, ObservationDate);




				--Create a temp table to roll up the result comments values.
			SELECT  OrderNumber, InterfaceSystemId As SourceSystemId, ResultComment,lbPatientId,
			ObservationCode1, ObservationDescription1, ObservationCodingSystemName1, ObservationCode2, ObservationDescription2, ObservationCodingSystemName2, 
			ObservationDate,
				ROW_NUMBER() OVER 
			(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId
				,ISNULL(ObservationCode1, ''), ISNULL( ObservationDescription1, ''), ISNULL( ObservationCodingSystemName1, ''),
				ISNULL( ObservationCode2, ''), ISNULL( ObservationDescription2,''), ISNULL( ObservationCodingSystemName2, ''), ISNULL(ObservationDate,'2099-12-31')
				ORDER BY InterfacePatientLabResultId DESC) rowNumber 
			INTO #rc
			FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE ResultComment IS NOT NULL
			
			CREATE CLUSTERED INDEX IX_#rc_Rollup
				ON #rc (lbPatientId, OrderNumber, SourceSystemId, 
			ObservationCode1,ObservationDescription1,ObservationCodingSystemName1, 
			ObservationCode2,ObservationDescription2, ObservationCodingSystemName2, ObservationDate);


			-- test select @maxSourceDateTime, @lastDateTime 

			-- 1. Use the data from PatientLabOrderProcessQueue, Get LBPatient ID from PatientIdReference 
	
		   SELECT   t1.PatientLabOrderId
			       ,t1.ObservationCode1
				   ,t1.ObservationDescription1
				   ,t1.ObservationCodingSystemName1
				   ,t1.ObservationCode2
				   ,t1.ObservationDescription2
				   ,t1.ObservationCodingSystemName2
				   ,val.Value
				   ,units.Units
				   ,ref.ReferenceRange
				   ,af.AbnormalFlag
				   ,rs.ResultStatus
				   ,rc.ResultComment
				   ,t1.ObservationDate
				   ,LR_CreateDateTime AS CreateDateTime
				   ,1 AS CreateLBUserId
				   ,1 AS ModifyLBUserId
					-----PatientLabOrderId, ObservationCode1, ObservationDate
					--  , ROW_NUMBER() OVER(PARTITION BY p.PatientLabOrderId, t1.ObservationCode1, t1.ObservationDate
					--	ORDER BY t1.InterfacePatientLabOrderId	DESC ) row_nbr
            INTO #tmpPatientLabResult
			FROM #T1 t1
			LEFT OUTER JOIN #val val
			ON val.lbPatientId = t1.lbPatientId 	
					AND val.OrderNumber = t1.OrderNumber 
					AND val.SourceSystemId = t1.SourceSystemId  
					AND val.ObservationCode1 = t1.ObservationCode1 	
					AND ISNULL(val.ObservationDescription1, '')  = ISNULL(t1.ObservationDescription1, '')   
					AND ISNULL(val.ObservationCodingSystemName1, '')  = ISNULL(t1.ObservationCodingSystemName1, '')   
					AND ISNULL(val.ObservationCode2, '')  = ISNULL(t1.ObservationCode2, '')   
					AND ISNULL(val.ObservationDescription2, '')  = ISNULL(t1.ObservationDescription2, '')   
					AND ISNULL(val.ObservationCodingSystemName2, '')  = ISNULL(t1.ObservationCodingSystemName2, '')   
					AND ISNULL(val.ObservationDate,'2099-12-31') = ISNULL(t1.ObservationDate,'2099-12-31')
			LEFT OUTER JOIN #units units
					ON units.lbPatientId = t1.lbPatientId 	
					AND units.OrderNumber = t1.OrderNumber 
					AND units.SourceSystemId = t1.SourceSystemId  
					AND units.ObservationCode1 = t1.ObservationCode1 	
					AND ISNULL(units.ObservationDescription1, '')  = ISNULL(t1.ObservationDescription1, '')   
					AND ISNULL(units.ObservationCodingSystemName1, '')  = ISNULL(t1.ObservationCodingSystemName1, '')   
					AND ISNULL(units.ObservationCode2, '')  = ISNULL(t1.ObservationCode2, '')   
					AND ISNULL(units.ObservationDescription2, '')  = ISNULL(t1.ObservationDescription2, '')   
					AND ISNULL(units.ObservationCodingSystemName2, '')  = ISNULL(t1.ObservationCodingSystemName2, '') 
					AND ISNULL(units.ObservationDate,'2099-12-31') = ISNULL(t1.ObservationDate,'2099-12-31')
			LEFT OUTER JOIN #ref ref 
				ON ref.lbPatientId = t1.lbPatientId 	
					AND ref.OrderNumber = t1.OrderNumber 
					AND ref.SourceSystemId = t1.SourceSystemId  
					AND ref.ObservationCode1 = t1.ObservationCode1 	
					AND ISNULL(ref.ObservationDescription1, '')  = ISNULL(t1.ObservationDescription1, '')   
					AND ISNULL(ref.ObservationCodingSystemName1, '')  = ISNULL(t1.ObservationCodingSystemName1, '')   
					AND ISNULL(ref.ObservationCode2, '')  = ISNULL(t1.ObservationCode2, '')   
					AND ISNULL(ref.ObservationDescription2, '')  = ISNULL(t1.ObservationDescription2, '')   
					AND ISNULL(ref.ObservationCodingSystemName2, '')  = ISNULL(t1.ObservationCodingSystemName2, '') 
					AND ISNULL(ref.ObservationDate,'2099-12-31') = ISNULL(t1.ObservationDate,'2099-12-31')
			LEFT OUTER JOIN	#af af
				ON af.lbPatientId = t1.lbPatientId 	
					AND af.OrderNumber = t1.OrderNumber 
					AND af.SourceSystemId = t1.SourceSystemId  
					AND af.ObservationCode1 = t1.ObservationCode1 	
					AND ISNULL(af.ObservationDescription1, '')  = ISNULL(t1.ObservationDescription1, '')   
					AND ISNULL(af.ObservationCodingSystemName1, '')  = ISNULL(t1.ObservationCodingSystemName1, '')   
					AND ISNULL(af.ObservationCode2, '')  = ISNULL(t1.ObservationCode2, '')   
					AND ISNULL(af.ObservationDescription2, '')  = ISNULL(t1.ObservationDescription2, '')   
					AND ISNULL(af.ObservationCodingSystemName2, '')  = ISNULL(t1.ObservationCodingSystemName2, '') 
					AND ISNULL(af.ObservationDate,'2099-12-31') = ISNULL(t1.ObservationDate,'2099-12-31')
			LEFT OUTER JOIN	#rs rs 
				ON rs.lbPatientId = t1.lbPatientId 	
					AND rs.OrderNumber = t1.OrderNumber 
					AND rs.SourceSystemId = t1.SourceSystemId  
					AND rs.ObservationCode1 = t1.ObservationCode1 	
					AND ISNULL(rs.ObservationDescription1, '')  = ISNULL(t1.ObservationDescription1, '')   
					AND ISNULL(rs.ObservationCodingSystemName1, '')  = ISNULL(t1.ObservationCodingSystemName1, '')   
					AND ISNULL(rs.ObservationCode2, '')  = ISNULL(t1.ObservationCode2, '')   
					AND ISNULL(rs.ObservationDescription2, '')  = ISNULL(t1.ObservationDescription2, '')   
					AND ISNULL(rs.ObservationCodingSystemName2, '')  = ISNULL(t1.ObservationCodingSystemName2, '') 
					AND ISNULL(rs.ObservationDate,'2099-12-31') = ISNULL(t1.ObservationDate,'2099-12-31')
		LEFT OUTER JOIN #rc rc 
				ON rc.lbPatientId = t1.lbPatientId 	
					AND rc.OrderNumber = t1.OrderNumber 
					AND rc.SourceSystemId = t1.SourceSystemId  
					AND rc.ObservationCode1 = t1.ObservationCode1 	
					AND ISNULL(rc.ObservationDescription1, '')  = ISNULL(t1.ObservationDescription1, '')   
					AND ISNULL(rc.ObservationCodingSystemName1, '')  = ISNULL(t1.ObservationCodingSystemName1, '')   
					AND ISNULL(rc.ObservationCode2, '')  = ISNULL(t1.ObservationCode2, '')   
					AND ISNULL(rc.ObservationDescription2, '')  = ISNULL(t1.ObservationDescription2, '')   
					AND ISNULL(rc.ObservationCodingSystemName2, '')  = ISNULL(t1.ObservationCodingSystemName2, '') 
						 AND ISNULL(rc.ObservationDate,'2099-12-31') = ISNULL(t1.ObservationDate,'2099-12-31')
			WHERE t1.rowNumber = 1
						AND (val.rowNumber = 1 OR val.rowNumber IS NULL)
						AND (ref.rowNumber = 1 OR ref.rowNumber IS NULL)
						AND (units.rowNumber = 1 OR units.rowNumber IS NULL)
						AND (af.rowNumber = 1 OR af.rowNumber IS NULL)
						AND (rs.rowNumber = 1 OR rs.rowNumber IS NULL)
						AND (rc.rowNumber = 1 OR rc.rowNumber IS NULL)
						

						
			DROP TABLE #rc, #rs, #af, #units, #val, #T1, #ref
				
		

		    --  2. load deduped patient lab result data into temp table fisrt, data deduped by PatientLabOrderId, ObservationCode1, ObservationDate
			--SELECT  t.PatientLabOrderId
			--       ,t.ObservationCode1
			--	   ,t.ObservationDescription1
			--	   ,t.ObservationCodingSystemName1
			--	   ,t.ObservationCode2
			--	   ,t.ObservationDescription2
			--	   ,t.ObservationCodingSystemName2
			--	   ,t.Value
			--	   ,t.Units
			--	   ,t.ReferenceRange
			--	   ,t.AbnormalFlag
			--	   ,t.ResultStatus
			--	   ,t.ResultComment
			--	   ,t.ObservationDate
			--	   ,t.CreateDateTime
			--	   ,1 AS CreateLBUserId
			--	   ,1 AS ModifyLBUserId
			--		, GETUTCDATE() as ModifyDateTime
					
			--INTO #PatientLabResult
			--FROM  #tmpPatientLabResult t
			--WHERE row_nbr=1;

			-- test select Count(1) from #PatientLabResult

			-- get source records counts
			SET @RecordCountSource=@@ROWCOUNT;

			
			---for merge key
		    CREATE CLUSTERED INDEX CIX_#patientLabResult_PatientID ON #tmpPatientLabResult (PatientLabOrderId,ObservationCode1,ObservationDescription1, ObservationCodingSystemName1, ObservationCode2,ObservationDescription2, ObservationCodingSystemName2, ObservationDate);


			-- 3. Merge to dbo.PatientLabResult
			
			MERGE dbo.PatientLabResult AS target
		    USING #tmpPatientLabResult AS source 
	        ON (target.PatientLabOrderId = source.PatientLabOrderId AND target.ObservationCode1 = source.ObservationCode1 
				AND ISNULL(target.ObservationDescription1,'') = source.ObservationDescription1 
				AND ISNULL(target.ObservationCodingSystemName1, '') = source.ObservationCodingSystemName1 
				AND ISNULL(target.ObservationCode2, '') = source.ObservationCode2 
				AND ISNULL(target.ObservationDescription2,'') = source.ObservationDescription2 
				AND ISNULL(target.ObservationCodingSystemName2,'') = source.ObservationCodingSystemName2 
			     AND ISNULL(target.ObservationDate,'2099-12-31') = ISNULL(source.ObservationDate,'2099-12-31'))
			WHEN MATCHED THEN 
				UPDATE SET 
				PatientLabOrderId = ISNULL(source.PatientLabOrderId, target.PatientLabOrderId),
				ObservationCode1 =  ISNULL(source.ObservationCode1, target.ObservationCode1 ),
				ObservationDescription1 = ISNULL(source.ObservationDescription1, target.ObservationDescription1),
				ObservationCodingSystemName1 = ISNULL(source.ObservationCodingSystemName1, target.ObservationCodingSystemName1), 
				ObservationCode2 = ISNULL( source.ObservationCode2, target.ObservationCode2 ),
				ObservationDescription2 = ISNULL( source.ObservationDescription2, target.ObservationDescription2 ) ,
				ObservationCodingSystemName2 = ISNULL(source.ObservationCodingSystemName2, target.ObservationCodingSystemName2),
				Value = ISNULL(source.Value, target.Value),
				Units = ISNULL(source.Units, target.Units),
				ReferenceRange =ISNULL( source.ReferenceRange, target.ReferenceRange),
				AbnormalFlag = ISNULL(source.AbnormalFlag, target.AbnormalFlag), 
				ResultStatus =  ISNULL(source.ResultStatus, target.ResultStatus),
				ResultComment = ISNULL(source.ResultComment, target.ResultComment),
				ObservationDate =  ISNULL(source.ObservationDate, target.ObservationDate), 
				--CreateDateTime = source.CreateDateTime, 
				ModifyDateTime = GetutcDate(),
				CreateLBUserId =  ISNULL(source.CreateLBUserId,  target.CreateLBUserId),
				ModifyLBUserId = ISNULL(source.ModifyLBUserId, target.ModifyLBUserId)
			WHEN NOT MATCHED THEN
			INSERT (PatientLabOrderId,ObservationCode1,ObservationDescription1,ObservationCodingSystemName1
				  ,ObservationCode2  ,ObservationDescription2,ObservationCodingSystemName2,Value
				  ,Units ,ReferenceRange,AbnormalFlag,ResultStatus
				  ,ResultComment,ObservationDate,CreateDateTime,ModifyDateTime
				  ,CreateLBUserId,ModifyLBUserId)
			VALUES (source.PatientLabOrderId,source.ObservationCode1,source.ObservationDescription1,source.ObservationCodingSystemName1
				  ,source.ObservationCode2  ,source.ObservationDescription2,source.ObservationCodingSystemName2,source.Value
				  ,source.Units ,source.ReferenceRange,source.AbnormalFlag,source.ResultStatus
				  ,source.ResultComment,source.ObservationDate,source.CreateDateTime,GetutcDate()
				  ,source.CreateLBUserId,source.ModifyLBUserId);


          ;

		      -- get total records from dbo.PatientLabResult after merge 
		       		SELECT @RecordCountAfter=COUNT(1)
			      FROM dbo.PatientLabResult;

              -- Calculate records inserted and updated counts
	            SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	           SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to Maintenance.dbo.EdwProcedureRunLog
			   SET  @EndTime=GETUTCDATE();
			   EXEC Maintenance.dbo.EdwInsertProcedureRunLog @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	               -- update maxSourceTimeStamp with max LR_CreateDateTime of PatientLabOrderProcessQueue
				 UPDATE Maintenance.dbo.EDWTableLoadTracking
				 SET maxSourceTimeStampProcessed = (SELECT MAX(convert(date,CreateDateTime)) FROM dbo.PatientLabOrderProcessQueue ),
					 updateDateTime =GETUTCDATE()
				WHERE EDWtableName = @EDWtableName AND dataSource ='interface'  AND EDWName = @EDWName

            --clean tables
			DROP TABLE  #tmpPatientLabResult;

	END TRY

	BEGIN CATCH
	           --- insert error log information to Maintenance.dbo.EdwProcedureRunLog
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC Maintenance.dbo.EdwInsertProcedureRunLog @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
	

END



GO
