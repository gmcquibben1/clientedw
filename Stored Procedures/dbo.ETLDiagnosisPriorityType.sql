SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-03-23
-- Description:	Load DiagnosisPriorityType Data into EDW
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[ETLDiagnosisPriorityType]

AS
BEGIN	
		INSERT INTO [dbo].[DiagnosisPriorityType]
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
        SELECT DISTINCT
		t1.[DiagnosisPriority],
		t1.[DiagnosisPriority],
		0,
		GETUTCDATE(),
		GETUTCDATE(),
		1,
		1
        FROM PatientDiagnosisProcessQueue t1
		LEFT JOIN [dbo].[DiagnosisPriorityType] t2
		ON LTRIM(RTRIM(t1.[DiagnosisPriority])) = LTRIM(RTRIM(t2.[Name]))
		WHERE t1.[DiagnosisPriority] IS NOT NULL AND  t2.[Name] IS NULL
END
GO
