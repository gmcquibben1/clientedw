SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FaceSheetDiagnosisLoad] 
	@PatientId INTEGER
AS

SELECT DISTINCT 
	dc.DiagnosisCodeDisplay AS DiagnosisCode,
	--dc.DiagnosisDescription AS DiagnosisDescription,
	i9.ShortDescription AS DiagnosisDescription,
	ptcc.DisplayOrder
FROM PatientChronicCondition ptcc 
	INNER JOIN DiagnosisCode dc ON dc.DiagnosisCodeId = ptcc.DiagnosisCodeId
	LEFT OUTER JOIN ICD9Mstr i9 ON dc.DiagnosisCode = i9.ICD9
WHERE ptcc.PatientId = @PatientId
	AND ptcc.DeleteInd <> 1
	AND dc.DeleteInd <> 1
ORDER BY ptcc.DisplayOrder;
GO
