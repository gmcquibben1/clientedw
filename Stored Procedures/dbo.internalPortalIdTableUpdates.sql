SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPortalIdTableUpdates]
AS
--IH 09/28/2015 Adding the lbpatientid in table Patient_Summary_Pivot
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'LbPatientId')
BEGIN
     ALTER TABLE Patient_Summary_Pivot ADD LbPatientId int NULL;
END

--IH 09/28/2015 Adding the lbpatientid in table MM_Metics_Pivot
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MM_Metics_Pivot' AND COLUMN_NAME = 'LbPatientId')
BEGIN
     ALTER TABLE MM_Metics_Pivot ADD LbPatientId int NULL;
END

UPDATE B SET LbPatientId = IDR.LbPatientId FROM CCLF_2_PartA_RCDetail B 
JOIN PatientIdReference IDR ON B.BENE_HIC_NUM = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= b.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(b.LbPatientId,0) <> idr.LbPatientId)

UPDATE B SET LbPatientId = IDR.LbPatientId FROM CCLF_3_PartA_ProcCd B 
JOIN PatientIdReference IDR ON B.BENE_HIC_NUM = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= b.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(b.LbPatientId,0) <> idr.LbPatientId)

UPDATE B SET LbPatientId = IDR.LbPatientId FROM CCLF_4_PartA_Diagnosis B 
JOIN PatientIdReference IDR ON B.BENE_HIC_NUM = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= b.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE ISNULL(b.LbPatientId,0) <> idr.LbPatientId

UPDATE B SET LbPatientId = IDR.LbPatientId FROM CCLF_9_BeneXref B 
JOIN PatientIdReference IDR ON B.CRNT_HIC_NUM = IDR.ExternalId OR b.PRVS_HIC_NUM = IDR.ExternalID
WHERE (ISNULL(b.LbPatientId,0) <> idr.LbPatientId)

--IH 10/01/2015 Populating the lbpatientid values in table CCLF_1_PartA_Header
UPDATE C SET LbPatientId = IDR.LbPatientId FROM CCLF_1_PartA_Header C 
INNER JOIN PatientIdReference IDR ON C.BENE_HIC_NUM  = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= c.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(c.LbPatientId,0) <> idr.LbPatientId)


--IH 10/02/2015 Populating the lbpatientid values in table CCLF_6_PartB_DME
UPDATE C SET LbPatientId = IDR.LbPatientId FROM CCLF_6_PartB_DME C 
INNER JOIN PatientIdReference IDR ON C.BENE_HIC_NUM  = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= c.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(c.LbPatientId,0) <> idr.LbPatientId)

--IH 10/02/2015 Populating the lbpatientid values in table CCLF_5_PartB_Physicians
UPDATE C SET LbPatientId = IDR.LbPatientId FROM CCLF_5_PartB_Physicians C 
INNER JOIN PatientIdReference IDR ON C.BENE_HIC_NUM = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= c.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(c.LbPatientId,0) <> idr.LbPatientId)

--IH 10/02/2015 Populating the lbpatientid values in table CCLF_7_PartD
UPDATE C SET LbPatientId = IDR.LbPatientId FROM CCLF_7_PartD C 
INNER JOIN PatientIdReference IDR ON C.BENE_HIC_NUM = IDR.ExternalId 
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= c.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(c.LbPatientId,0) <> idr.LbPatientId)

UPDATE C SET LbPatientId = IDR.LbPatientId FROM CCLF_8_BeneDemo C 
INNER JOIN PatientIdReference IDR ON C.BENE_HIC_NUM  = IDR.ExternalId 
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= C.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(c.LbPatientId,0) <> idr.LbPatientId)

--MH 1/6/2016 - adding member roster archives tables
UPDATE MRA SET LbPatientId = IDR.LbPatientId  FROM MemberRosterArchives MRA
JOIN PatientIdReference IDR ON MRA.HICNO = IDR.ExternalId
WHERE (ISNULL(MRA.LbPatientId,0) <> idr.LbPatientId)

   
UPDATE MRA SET LbPatientId = IDR.LbPatientId  FROM dbo.MemberRosterArchives_Table11 MRA
JOIN PatientIdReference IDR ON MRA.HICNO = IDR.ExternalId
WHERE (ISNULL(MRA.LbPatientId,0) <> idr.LbPatientId)
   
UPDATE MRA SET LbPatientId = IDR.LbPatientId  FROM dbo.MemberRosterArchives_ProviderTab MRA
JOIN PatientIdReference IDR ON MRA.HICNO = IDR.ExternalId
WHERE (ISNULL(MRA.LbPatientId,0) <> idr.LbPatientId)
         
UPDATE B SET LbPatientId = IDR.LbPatientId 
FROM PatientRosterEnrollment B 
JOIN PatientIdReference IDR ON B.patientExternalID = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= b.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(b.LbPatientId,0) <> idr.LbPatientId)

UPDATE B SET LbPatientId = IDR.LbPatientId 
FROM PatientRosterEnrollmentRaw B 
JOIN PatientIdReference IDR ON B.patientExternalID = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= b.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(b.LbPatientId,0) <> idr.LbPatientId)

UPDATE B SET LbPatientId = IDR.LbPatientId 
FROM PatientRosterMonths B 
JOIN PatientIdReference IDR ON B.patientExternalID = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= b.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(b.LbPatientId,0) <> idr.LbPatientId)

UPDATE B SET LbPatientId = IDR.LbPatientId 
FROM PatientRosterMonthsRaw B 
JOIN PatientIdReference IDR ON B.patientExternalID = IDR.ExternalId
JOIN [SourceSystemSetting] sss on sss.[SettingValue]= b.sourcefeed and sss.[SettingName]='ResolvingCCLFName'
WHERE (ISNULL(b.LbPatientId,0) <> idr.LbPatientId)

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RISK SCORES') 
BEGIN
UPDATE RS SET LbPatientId = IDR.LbPatientId FROM [Risk Scores] RS
JOIN PatientIdReference IDR ON IDR.ExternalId = RS.HICN and IDR.DeleteInd = 0
WHERE (ISNULL(RS.LbPatientId,0) <> idr.LbPatientId)
END
GO
