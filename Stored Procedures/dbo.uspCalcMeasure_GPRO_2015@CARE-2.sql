SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@CARE-2] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];

	DECLARE  @yearend datetime, @measureBeginDate datetime;
	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2015';

	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	
	/*
	1) make certain the RANK column for the measure is not null for the patient pool 
		OR - 
	-- GPRO measures:			       select * from GproMeasureType
				    
	   Patients Table:           select * from GproPatientRanking

	Union of Pat and Measures:   select * from GproPatientRankingGproMeasureType where GproMeasureTypeID = 10

	 File sent to CMS with patient INFORMATION --- SELECT * FROM GPROPATIENTMEASURE where 


	select r.LBaPatientID
	FROM   GPROPATIENTRANKING r
	JOIN   GPROPATIENTRANKINGGPROMEASURETYPE xref on xref.GproPatientRankingID = r.GproPatientRankingID
	JOIN   

	*/

	--Initial Patient pooling, for patients that have ranking for the care-2 measure. 
	SELECT r.LBPatientID,
		r.GProPatientRankingId,
		dateDiff(year,r.Birthdate,@measureBeginDate) as Age,
		r.Birthdate,
		GM.GproMeasureTypeID,
		GPM.CareFallsRank,
		GPM.CareFallsConfirmed,
		GPM.FallsScreening, 
		GPM.CareFallsComments, 		
		GETUTCDATE() as ModifyDateTime
	INTO #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE GM.Name = 'CARE-2' 
		AND (GPM.CareFallsConfirmed IS NULL OR GPM.FallsScreening IS NULL )

	
	---CHECK IF PATIENT HAS ENCOUNTER DURING MEASURMENT YEAR AND CALC AGE AT THE TIME OF EARLIEST ENCOUNTER
	SELECT DISTINCT SUB.*
	, dateDiff(year,SUB.BirthDate,SUB.PROCEDUREDATETIME) as Age_ENC
	INTO #ENC -- drop table #enc
	FROM 
	(
		SELECT DISTINCT RP.*, PP.PROCEDUREDATETIME
			, ROW_NUMBER () OVER (PARTITION BY RP.LBPATIENTID ORDER BY PP.PROCEDUREDATETIME ASC) EARLIEST_ENC
		FROM #RANKEDPATIENTS RP
			JOIN dbo.patientProcedure PP ON RP.LBPATIENTID = PP.PATIENTID
			JOIN dbo.patientProcedureProcedureCode PPC ON pp.[PatientProcedureId] = ppc.[PatientProcedureId]
			JOIN dbo.ProcedureCode PC ON PPC.PROCEDURECODEID = PC.PROCEDURECODEID
			JOIN DBO.[GproEvaluationCode] HVSC ON PC.ProcedureCode = HVSC.CODE
				AND hvsc.[ModuleType]= 'care'
				AND hvsc.[ModuleIndicatorGPRO]='2'
				AND HVSC.[VariableName] IN   ('ENCOUNTER_CODE')
				AND PP.ProcedureDateTime >= @measureBeginDate 
				AND PP.ProcedureDateTime <  @yearend
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
	) SUB
	WHERE SUB.EARLIEST_ENC = 1 -- TAKE EARLIEST ENCOUNTER TO CALCULATE AGE
	
	--carefalls-confirmed
	UPDATE #RankedPatients
	SET CareFallsConfirmed = '2' --Yes
	FROM #RankedPatients RP 
		JOIN #ENC ENC ON RP.LBPatientId = ENC.LBPatientId
	Where ENC.Age_ENC >= 65
		AND RP.CareFallsConfirmed IS NULL

	--SET REMAINDER PATIENT UNDER 65 TO NOT CONFIRMED
	UPDATE #RankedPatients
	SET CareFallsConfirmed = '19' -- Not Confirmed - Age.
	FROM #RankedPatients RP 
		JOIN #ENC ENC ON RP.LBPatientId = ENC.LBPatientId
	WHERE ENC.AGE < 65
		AND RP.CareFallsConfirmed IS NULL

	----carefalls-confirmed
	--UPDATE #RankedPatients
	--SET CareFallsConfirmed = '2' --Yes
	--FROM #RankedPatients RP 
	--WHERE RP.Age >= 65 
	--	AND RP.CareFallsConfirmed IS NULL

	----SET REMAINDER PATIENT UNDER 65 TO NOT CONFIRMED
	--UPDATE #RankedPatients
	--SET CareFallsConfirmed = '19' -- Not Confirmed - Age.
	--FROM #RankedPatients RP 
	--WHERE RP.Age < 65
	--	AND RP.CareFallsConfirmed IS NULL


	---CHECK IF PATIENT HAS fall screening DURING MEASURMENT YEAR
	SELECT DISTINCT SUB.*
	INTO #fall -- drop table #fall
	FROM 
	(
		SELECT DISTINCT stage.*	
			, ROW_NUMBER () OVER (PARTITION BY stage.LbPatientId ORDER BY pp.[ProcedureDateTime] ASC) EARLIEST_FS
		FROM #ENC stage
			INNER JOIN vwPatientProcedure pp ON pp.PatientId = stage.LbPatientId
				AND pp.ProcedureDateTime >= @measureBeginDate
				AND pp.ProcedureDateTime < @yearend
			INNER JOIN [dbo].[GproEvaluationCode] hvsc ON hvsc.[Code] = pp.[ProcedureCode]
				AND hvsc.[ModuleType]= 'care'
				AND hvsc.[ModuleIndicatorGPRO]='2'
				AND hvsc.[VariableName] IN   ('FALLS_RISK_CODE')
			JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = pp.[SourceSystemID]    --PROCS
	) SUB
	WHERE SUB.EARLIEST_FS = 1 

	-- yes screening
	UPDATE #RankedPatients
	SET FallsScreening = '2' --yes
	FROM #RankedPatients rp
		JOIN #fall f ON rp.LBPatientId = f.LBPatientId
	WHERE rp.FallsScreening IS NULL

	--	--no screening
	--	update #RankedPatients
	--	set FallsScreening = '1' -- no
	--	-- select distinct rp.*,  f.LBPatientId 
	--	from #RankedPatients rp
	--	left join #fall f on rp.LBPatientId = f.LBPatientId
	--	where f.LBPatientId is null
	--	and rp.FallsScreening is null
	
	--------------------------------------------------------------------------------------------------------          
	-- Updating the required tables
	--------------------------------------------------------------------------------------------------------    
	--INSERT INTO GproAuditHistory
	--(GproPatientRankingId,GProPatientMeasureId,GproMeasureTypeId,LbUserId,Comment,CreateDateTime,ModifyDateTime)
	--SELECT gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gmt.GproMeasureTypeId, 1, 'Updated by CARE-2 Measure Calc', GETUTCDATE(), GETUTCDATE()
	--FROM GproPatientMeasure gpm
	--	INNER JOIN #RankedPatients rpat ON rpat.GproPatientRankingId = gpm.GproPatientRankingId
	--	INNER JOIN GproMeasureType gmt ON gmt.Name = 'CARE-2'
	--WHERE (gpm.CareFallsConfirmed IS NULL AND rpat.CareFallsConfirmed IS NOT NULL)
	--	OR (gpm.FallsScreening IS NULL AND rpat.FallsScreening IS NOT NULL)

	UPDATE gpm
	SET gpm.CareFallsConfirmed = gpr.CareFallsConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.CareFallsConfirmed IS NULL;

	UPDATE gpm
	SET gpm.FallsScreening = gpr.FallsScreening, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.FallsScreening IS NULL;

	
		
END
	

GO
