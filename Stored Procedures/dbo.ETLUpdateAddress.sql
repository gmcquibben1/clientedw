SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLUpdateAddress] 
AS

BEGIN



/* ================================================================
	Author:         Youping Liu
	Create date:	2016-05-07	
	Description:    Extract patient addresses from the member extract table


              
     Version       Date			 Author	  Jira				Change
     -------       ----------    ------	 ------				------
				 2016-05-07		 YL							Re-write the address update process
				 2016-08-04		 YL							correction on mistype AddressLine2 with AddressLine1
				 2016-08-05		 YL							WHERE ip.Address1 <> '' changed to WHERE ISNULL(ip.Address1,'') <> ''
				 2016-08-08		 YL							Comment update process when merge key matched, no need to do update
				 2016-09-13		 YL		 LBDM-1206			Missing address due to existing logic using PatientIdsRegistry (mismatched family ID) Modification to use [vwPortal_Patient] usinfg lbpatientID instead of family ID
	 2.1.1       2016-10-24      YL      LBETL-152			correction on patientidentifier + InterfaceSystemId
	 2.1.2       2016-12-15      SG      LBETL-261/303		rewrite code.
	 2.2.1       2017-02-24      CL      LBETL-909			Address line doubled due to null address line 1 and a value in Address line 2
	 2.2.1       2017-04-13      CL      LBETL-756			Handled cases where the NULL territory id causes duplicates
      
================================================================*/


     SET NOCOUNT ON

          DECLARE @EDWName VARCHAR(128) = DB_NAME(); 
          DECLARE @EDWtableName VARCHAR(128) = 'vwPortal_IndividualStreetAddress';
          DECLARE @dataSource VARCHAR(20) = 'IndividualStreetAddress';
          DECLARE @ProcedureName VARCHAR(128) = 'ETLUpdateAddress';
          DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
          DECLARE @maxSourceIdProcessed INT = NULL;
          DECLARE @StartTime DATETIME = GETUTCDATE();
          DECLARE @EndTime DATETIME = NULL;
          DECLARE @InsertedRecord INT=0;
          DECLARE @UpdatedRecord INT=0;
          DECLARE @COMMENT VARCHAR(400) = '';
          DECLARE @ErrorNumber INT =0;
          DECLARE @ErrorState INT =0;
          DECLARE @ErrorSeverity INT=0;
          DECLARE @ErrorLine INT=0;
          DECLARE @RecordCountBefore INT=0;
          DECLARE @RecordCountAfter  INT=0;
          DECLARE @RecordCountSource INT=0;

          DECLARE @LBUSERID            INT=1
          DECLARE @CurrentDateTime     DATETIME=GETUTCDATE()
          DECLARE @DeleteInd           BIT=0


          SELECT @RecordCountBefore=COUNT(1)
          FROM vwPortal_IndividualStreetAddress;




              BEGIN TRY

			  SELECT ISNULL(ISNULL(Address1, Address2), '') as Address1, CASE  WHEN  ISNULL(Address1,'') = '' THEN '' ELSE Address2 END AS Address2 , City, State, Zip , Phone1, PatientIdentifier , InterfaceSystemId, CreateDateTime,
				ROW_NUMBER() OVER(PARTITION BY  PatientIdentifier, InterfaceSystemId ORDER BY CreateDateTime DESC) r
				INTO #IPT0 
				FROM vwPortal_InterfacePatient IP
				WHERE ISNULL(ip.City, '') <> '' AND ISNULL(ip.state, '') <> '' AND ISNULL(ip.zip, '') <> '' AND 
				(ISNULL(ip.Address1,'') <> ''   OR ISNULL(ip.Address2, '') <> '')

          -- update address from vwPortal_InterfacePatientMatched
              UPDATE Me SET -- DIRECT 
              mailAddrLine1 = LTRIM(RTRIM(ip.Address1)),
              mailAddrLine2 =  LTRIM(RTRIM(ip.Address2)),
              mailCity =  LTRIM(RTRIM(ip.City)),
              mailState =  LTRIM(RTRIM(ip.State)),
              mailZip =  LTRIM(RTRIM(ip.Zip)),
              homePhone =  LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(ip.Phone1,'(',''),')',''),'-',''),'+1','')))
              FROM MemberExtract Me 
              INNER JOIN #IPT0 IP ON Me.medicareNo =  IP.PatientIdentifier
              INNER JOIN [dbo].[vwPortal_InterfaceSystem] VSS ON IP.InterfaceSystemId = VSS.InterfaceSystemId
              AND ME.SourceSystemId = VSS.SourceSystemId
             

			  UPDATE memberextract SET mailaddrline2 = '' where mailaddrline1 = mailaddrline2 --LBETL-909

              UPDATE MemberExtract SET mailZip = homeZip WHERE mailZIp IS NULL AND homeZip IS NOT NULL  



          ---Step 1. select Individual Address from MemberExtract
                   -- only address with non-null/empty mailAddrLine1 will be considered valid
                   SELECT   DISTINCT  ME.SourceSystemId AS SourceSystemId, ME.LbPatientId , ME.CreateDateTime , PIR.IndividualID AS IndividualID, MedicareNo AS ExternalReferenceIdentifier, 
                      1 AS StreetAddressTypeId,  mailAddrLine1 AS AddressLine1,mailAddrLine2 AS AddressLine2,
                     ''  AS AddressLine3 , 0 AS DwellingTypeId,  mailCity AS City , mailState , TT.TerritoryTypeID AS TerritoryTypeID,
                     LEFT(LTRIM(mailZip),5) AS  PostalCode, CASE WHEN len(LTRIM(mailZip))>5 THEN RIGHT(LTRIM(mailZip),len(LTRIM(mailZip))-5) END
                     AS PostalCodePlus4, 1 AS CountryTypeId, ISNULL(mailAddrLine1,'')+' '+ISNULL(mailAddrLine2,'')  AS CombinedAddress 
                    INTO #MemberAddressT1
                   FROM  MemberExtract ME
                   INNER JOIN dbo.vwportal_Patient P
                   ON ME.LbPatientId = P.PatientId
                   INNER JOIN dbo.vwportal_individual PIR 
                   ON P.IndividualID = PIR.IndividualID
                   LEFT JOIN vwPortal_TerritoryType TT ON ME.mailState = TT.NAME
                   WHERE ISNULL(mailCity, '') <> '' AND ISNULL(mailzip, '') <> '' AND ISNULL(mailState, '') <> '' AND
                        ( ISNULL(ME.mailAddrLine2 , '') <> ''  OR  ISNULL(ME.mailAddrLine1 , '') <> '')

                   -- We'll only capture the latest non-null address for a patient.
                   SELECT * INTO #MemberAddress
                   FROM(
                        SELECT A.*, ROW_NUMBER() OVER(PARTITION BY LbPatientId, IndividualId ORDER BY CreateDateTime DESC) r 
                   FROM #MemberAddressT1 A
                   ) B
                   WHERE B.r = 1

                   CREATE NONCLUSTERED INDEX  IDX_Address_Individual ON #MemberAddress (IndividualID);

                   CREATE NONCLUSTERED INDEX  IDX_Address ON #MemberAddress (PostalCode,City) INCLUDE (CombinedAddress);


                   ---Step 2.  select distinct Address from Step 1
                   SELECT   DISTINCT s.StreetAddressID,  1 AS StreetAddressTypeId, a.SourceSystemId AS SourceSystemId,  
                             a.AddressLine1, ISNULL(a.AddressLine2,'') AddressLine2,
                               ISNULL(a.AddressLine3,'') AddressLine3, 0 AS DwellingTypeId,  a.City , ISNULL(a.TerritoryTypeID, 0) as TerritoryTypeID,
                               a.PostalCode, ISNULL(a.PostalCodePlus4,'') PostalCodePlus4, 1 AS CountryTypeId
                                , 0 AS DeleteInd, @CurrentDateTime AS CreateDatetime, @CurrentDateTime AS ModifyDatetime, @LBUSERID AS CreateLbUserId, @LBUSERID AS ModifyLbUserid
                   INTO  #Address
                   FROM  #MemberAddress  a
                   LEFT OUTER JOIN vwPortal_StreetAddress s ON a.SourceSystemId=s.SourceSystemId AND isnull(a.TerritoryTypeID,0)=isnull(s.TerritoryTypeID,0)
                   AND s.StreetAddressTypeId=1 
                   AND ISNULL(s.City,'')=ISNULL(a.City,'')
                   AND LEFT(ISNULL(s.PostalCode,''),5)=ISNULL(a.PostalCode,'')
                   AND ISNULL(s.AddressLine1,'')+' '+ISNULL(s.AddressLine2,'')+' '+ISNULL(s.AddressLine3,'')=a.CombinedAddress

                   CREATE CLUSTERED INDEX  IDX_Address ON #Address (StreetAddressID);
                   ;


                   ---Step 3. merge new  Address to vwPortal_StreetAddress  
                    MERGE vwPortal_StreetAddress AS TARGET
                        USING #Address AS SOURCE
                                  ON TARGET.StreetAddressId = SOURCE.StreetAddressId 
                         WHEN MATCHED THEN UPDATE SET
                                    StreetAddressTypeId = SOURCE.StreetAddressTypeId, 
                                    SourceSystemId = SOURCE.SourceSystemId,
                                    AddressLine1 = SOURCE.AddressLine1,
                                    AddressLine2  = SOURCE.AddressLine2,
                                    AddressLine3 = SOURCE.AddressLine3,
                                    DwellingTypeId = SOURCE.DwellingTypeId,
                                    City = SOURCE.City, 
                                    TerritoryTypeID = SOURCE.TerritoryTypeID,
                                    PostalCode = SOURCE.PostalCode,
                                    PostalCodePlus4  = SOURCE.PostalCodePlus4,
                                    CountryTypeId = SOURCE.CountryTypeId,
                                    DeleteInd = SOURCE.DeleteInd,
                                    ModifyDateTime = SOURCE.ModifyDateTime,
                                    ModifyLBUserId = SOURCE.ModifyLBUserId
                        WHEN NOT MATCHED THEN INSERT 
                                   (StreetAddressTypeId, SourceSystemId, AddressLine1, AddressLine2, AddressLine3,DwellingTypeId,City,TerritoryTypeID,
                                     PostalCode,PostalCodePlus4,CountryTypeId,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId) 
                         VALUES (SOURCE.StreetAddressTypeId, SOURCE.SourceSystemId, SOURCE.AddressLine1, SOURCE.AddressLine2, SOURCE.AddressLine3,SOURCE.DwellingTypeId,SOURCE.City,SOURCE.TerritoryTypeID,
                                     SOURCE.PostalCode,SOURCE.PostalCodePlus4,SOURCE.CountryTypeId,SOURCE.DeleteInd,SOURCE.CreateDateTime,SOURCE.ModifyDateTime,SOURCE.CreateLBUserId,SOURCE.ModifyLBUserId) 

                   ;


                   ----
                   SELECT   DISTINCT s.StreetAddressID, a.IndividualID, 1 AS StreetAddressTypeId, 
                               0 AS DeleteInd, @CurrentDateTime AS CreateDatetime, 
                               @CurrentDateTime AS ModifyDatetime, @LBUSERID AS CreateLbUserId, @LBUSERID AS ModifyLbUserid
                   INTO  #MemberAddress2
                   FROM  #MemberAddress  a
                   INNER JOIN vwPortal_StreetAddress s ON a.SourceSystemId=s.SourceSystemId
                   AND s.StreetAddressTypeId=1  AND isnull(a.TerritoryTypeID,0)=isnull(s.TerritoryTypeID,0)
                   AND ISNULL(s.City,'')=ISNULL(a.City,'')
                   AND LEFT(ISNULL(s.PostalCode,''),5)=ISNULL(a.PostalCode,'')
                   AND ISNULL(s.AddressLine1,'')+' '+ISNULL(s.AddressLine2,'')+' '+ISNULL(s.AddressLine3,'')=a.CombinedAddress

                   ;

                   SELECT @RecordCountSource=COUNT(1)
                   FROM #MemberAddress2;


                   CREATE CLUSTERED INDEX  IDX_Address ON #MemberAddress2 (IndividualID);

                   SELECT StreetAddressID , IndividualID , StreetAddressTypeId , DeleteInd , 
                        CreateDatetime , ModifyDatetime , CreateLbUserId , ModifyLbUserid
                   INTO #MemberAddress3
                   FROM (
                        SELECT StreetAddressID , IndividualID , StreetAddressTypeId , DeleteInd , 
                        CreateDatetime , ModifyDatetime , CreateLbUserId , ModifyLbUserid
                        , ROW_NUMBER() OVER (PARTITION BY IndividualID ORDER BY StreetAddressID DESC) r 
                        FROM #MemberAddress2 
                   ) A WHERE A.r = 1

                   CREATE CLUSTERED INDEX  IDX_Address3 ON #MemberAddress3 (IndividualID);

                   ---Step 4. merge new  Member Address to vwPortal_IndividualStreetAddress
                   MERGE vwPortal_IndividualStreetAddress AS TARGET
                        USING #MemberAddress3 AS SOURCE
                                  ON TARGET.IndividualID = SOURCE.IndividualID 
                             --   AND TARGET.StreetAddressID = SOURCE.StreetAddressId 
                         WHEN MATCHED THEN UPDATE SET
                                  StreetAddressID = SOURCE.StreetAddressId
                             --     StreetAddressTypeId = Source.StreetAddressTypeId, 
                             --     DeleteInd = Source.DeleteInd,
                             --     ModifyDateTime = Source.ModifyDateTime,
                             --     ModifyLBUserId = Source.ModifyLBUserId
                        WHEN NOT MATCHED THEN INSERT 
                                   (IndividualID,StreetAddressId,StreetAddressTypeID,DeleteInd,CreateDateTime, ModifyDateTime,  CreateLbUserId,ModifyLBUserId) 
                         VALUES (SOURCE.IndividualID, SOURCE.StreetAddressId, SOURCE.StreetAddressTypeID, SOURCE.DeleteInd
                        , SOURCE.CreateDateTime,SOURCE.ModifyDateTime,SOURCE.CreateLBUserId,SOURCE.ModifyLBUserId) 

                   ;
                        -- get total records from vwPortal_IndividualStreetAddress after merge 
                        SELECT @RecordCountAfter=COUNT(1)
                        FROM vwPortal_IndividualStreetAddress;


              
                             
                                    -- Calculate records inserted and updated counts
                                      SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
                                     -- SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

                                    -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
                                     SET  @EndTime=GETUTCDATE();
                                     EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
     


                   ---last to clean temp tables
                    DROP TABLE  #MemberAddress,  #MemberAddress2,  #Address, #IPT0, #MemberAddressT1


              END TRY
              BEGIN CATCH
                           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
                             SET  @EndTime=GETUTCDATE();
                             SET  @ErrorNumber =ERROR_NUMBER();
                             SET  @ErrorState =ERROR_STATE();
                             SET  @ErrorSeverity=ERROR_SEVERITY();
                             SET  @ErrorLine=ERROR_LINE();
                             SET  @COMMENT =LEFT(ERROR_MESSAGE(),400);
                             EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
                   
              END CATCH

END
GO
