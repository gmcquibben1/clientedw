SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MemberExtractSave]
	  @FirstName nchar(50)
	, @LastName nchar(50)
	, @GenderTypeID int = NULL
	, @BirthDate date  = NULL
	, @HomeAddressLine1 nchar(50) = NULL
	, @HomeAddressLine2 nchar(50) = NULL
	, @HomeCity nchar(50) = NULL
	, @HomeState nchar(50) = NULL
	, @HomeZip nchar(50) = NULL
	, @HomePhone nchar(50) = NULL
	, @HomeEmail nchar(50) = NULL
	, @IsDeceased nchar(50) = NULL
	, @ProviderId nchar(50) = NULL
	, @DeathDate date = NULL
	, @medicareNo varchar(50) 
	, @LbPatientId int
	, @SourceSystemId int = NULL
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @ExistingPatientId int;

	IF @SourceSystemID IS NULL
		SET @SourceSystemId = (SELECT SourceSystemId FROM SourceSystem WHERE Name = 'Internal')

	SET @ExistingPatientId=(SELECT LbPatientId FROM MemberExtract Where LbPatientId = @LbPatientId AND SourceSystemId = @SourceSystemId)
	
	IF ISNULL(@ExistingPatientId,'') = ''
	BEGIN
		INSERT INTO dbo.MemberExtract
		(
			 firstName
		   , lastName
		   , sex
		   , birthDate
		   , homeAddrLine1
		   , homeAddrLine2
		   , homeCity
		   , homeState
		   , homeZip
		   , homePhone
		   , homeEMail
		   , IsDeceased
		   , ProviderId
		   , CreateDatetime
		   , ModifyDatetime
		   , DeathDate
		   , medicareNo
		   , LbPatientId
		   , SourceSystemId
		)
		VALUES
		(
			 @FirstName
		   , @LastName
		   , @GenderTypeID
		   , @BirthDate
		   , @HomeAddressLine1
		   , @HomeAddressLine2
		   , @HomeCity
		   , @HomeState
		   , @HomeZip
		   , @HomePhone
		   , @HomeEmail
		   , @IsDeceased
		   , @ProviderId
		   , GETUTCDATE()
		   , GETUTCDATE()
		   , @DeathDate
		   , @medicareNo
		   , @LbPatientId
		   , @SourceSystemId
		)
	END
	ELSE
	BEGIN
		UPDATE dbo.MemberExtract
		SET
		  firstName=@FirstName
		, lastName=@LastName
		, sex=@GenderTypeID
		, birthDate=@BirthDate
		, homeAddrLine1=@HomeAddressLine1
		, homeAddrLine2=@HomeAddressLine2
		, homeCity=@HomeCity
		, homeState=@HomeState
		, homeZip=@HomeZip
		, homePhone=@HomePhone
		, homeEMail=@HomeEmail
		, IsDeceased=@IsDeceased
		, ProviderId=@ProviderId
		, ModifyDatetime=GETUTCDATE()
		, DeathDate=@DeathDate
		, medicareNo=@medicareNo
		Where LbPatientId=@ExistingPatientId AND SourceSystemId = @SourceSystemId
	END
END
GO
