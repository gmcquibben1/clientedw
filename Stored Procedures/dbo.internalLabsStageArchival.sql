SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalLabsStageArchival] 
    @LookbackDays INT = 0  ,
	@LookbackSince DateTime = NULL,
	@ClearStage BIT = 0
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

    SELECT @LookbackSince = CASE WHEN @LookbackSince IS NULL THEN GETDATE() ELSE @LookbackSince END 

	MERGE InterfacePatientLabOrderArchive AS TARGET
	USING (
			SELECT 
			  [InterfacePatientLabOrderId]
			  ,[InterfacePatientId]
			  ,[InterfaceSystemId]
			  ,[OrderNumber]
			  ,[ServiceId]
			  ,[ServiceDescription]
			  ,[ServiceCodingSystemName]
			  ,[OrderingProviderId]
			  ,[OrderingProviderIdType]
			  ,[OrderingProviderName]
			  ,[CreateDateTime]
		    FROM [dbo].vwPortal_InterfacePatientLabOrder (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			([InterfacePatientLabOrderId],[InterfacePatientId],[InterfaceSystemId],[OrderNumber],[ServiceId],[ServiceDescription]
			,[ServiceCodingSystemName],[OrderingProviderId],[OrderingProviderIdType],[OrderingProviderName],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientLabOrderId] = SOURCE.[InterfacePatientLabOrderId] 
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientLabOrderId]	=Source.[InterfacePatientLabOrderId],
			[InterfacePatientId]			=Source.[InterfacePatientId],
			[InterfaceSystemId]				=Source.[InterfaceSystemId],
			[OrderNumber]					=Source.[OrderNumber],
			[ServiceId]						=Source.[ServiceId],
			[ServiceDescription]			=Source.[ServiceDescription],
			[ServiceCodingSystemName]		=Source.[ServiceCodingSystemName],
			[OrderingProviderId]			=Source.[OrderingProviderId],
			[OrderingProviderIdType]		=Source.[OrderingProviderIdType],
			[OrderingProviderName]			=Source.[OrderingProviderName],
			[CreateDateTime]				=Source.[CreateDateTime]
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientLabOrderId],[InterfacePatientId],[InterfaceSystemId],[OrderNumber],[ServiceId],[ServiceDescription]
			,[ServiceCodingSystemName],[OrderingProviderId],[OrderingProviderIdType],[OrderingProviderName],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientLabOrderId],Source.[InterfacePatientId],Source.[InterfaceSystemId],Source.[OrderNumber],Source.[ServiceId],Source.[ServiceDescription]
			,Source.[ServiceCodingSystemName],Source.[OrderingProviderId],Source.[OrderingProviderIdType],Source.[OrderingProviderName],Source.[CreateDateTime]) ;

	MERGE InterfacePatientLabResultArchive AS TARGET
	USING (
			SELECT 
			  [InterfacePatientLabResultId]
			  ,[InterfacePatientLabOrderId]
			  ,[ObservationCode1]
			  ,[ObservationDescription1]
			  ,[ObservationCodingSystemName1]
			  ,[ObservationCode2]
			  ,[ObservationDescription2]
			  ,[ObservationCodingSystemName2]
			  ,[Value]
			  ,[Units]
			  ,[ReferenceRange]
			  ,[AbnormalFlag]
			  ,[ResultStatus]
			  ,[ResultComment]
			  ,[ObservationDate]
			  ,[CreateDateTime]
		    FROM [dbo].vwPortal_InterfacePatientLabResult  (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			( [InterfacePatientLabResultId],[InterfacePatientLabOrderId],[ObservationCode1],[ObservationDescription1],[ObservationCodingSystemName1],[ObservationCode2]
			 ,[ObservationDescription2],[ObservationCodingSystemName2],[Value],[Units],[ReferenceRange],[AbnormalFlag],[ResultStatus],[ResultComment]
			 ,[ObservationDate],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientLabOrderId] = SOURCE.[InterfacePatientLabOrderId] AND TARGET.[InterfacePatientLabResultId] = SOURCE.[InterfacePatientLabResultId]
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientLabResultId]	=Source.[InterfacePatientLabResultId],
			--[InterfacePatientLabOrderId]	=Source.[InterfacePatientLabOrderId],
			[ObservationCode1]				=Source.[ObservationCode1],
			[ObservationDescription1]		=Source.[ObservationDescription1],
			[ObservationCodingSystemName1]	=Source.[ObservationCodingSystemName1],
			[ObservationCode2]				=Source.[ObservationCode2],
			[ObservationDescription2]		=Source.[ObservationDescription2],
			[ObservationCodingSystemName2]	=Source.[ObservationCodingSystemName2],
			[Value]							=Source.[Value],
			[Units]							=Source.[Units],
			[ReferenceRange]				=Source.[ReferenceRange],
			[AbnormalFlag]					=Source.[AbnormalFlag],
			[ResultStatus]					=Source.[ResultStatus],
			[ResultComment]					=Source.[ResultComment],
			[ObservationDate]				=Source.[ObservationDate],
			[CreateDateTime]				=Source.[CreateDateTime]
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientLabResultId],[InterfacePatientLabOrderId],[ObservationCode1],[ObservationDescription1],[ObservationCodingSystemName1],[ObservationCode2]
			 ,[ObservationDescription2],[ObservationCodingSystemName2],[Value],[Units],[ReferenceRange],[AbnormalFlag],[ResultStatus],[ResultComment]
			 ,[ObservationDate],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientLabResultId],Source.[InterfacePatientLabOrderId],Source.[ObservationCode1],Source.[ObservationDescription1],Source.[ObservationCodingSystemName1],Source.[ObservationCode2]
			 ,Source.[ObservationDescription2],Source.[ObservationCodingSystemName2],Source.[Value],Source.[Units],Source.[ReferenceRange],Source.[AbnormalFlag],Source.[ResultStatus],Source.[ResultComment]
			 ,Source.[ObservationDate],Source.[CreateDateTime]) ;

	IF @ClearStage = 1
	BEGIN
		DELETE vwPortal_InterfacePatientLabOrder FROM vwPortal_InterfacePatientLabOrder  A
		INNER JOIN InterfacePatientLabOrderArchive B ON A.[InterfacePatientLabOrderId] = B.[InterfacePatientLabOrderId] 
		WHERE A.[CreateDateTime] >= GetDate() - @LookbackDays OR A.[CreateDateTime] > @LookbackSince

		DELETE vwPortal_InterfacePatientLabResult FROM vwPortal_InterfacePatientLabResult A
		INNER JOIN InterfacePatientLabResultArchive B ON A.[InterfacePatientLabOrderId] = B.[InterfacePatientLabOrderId] AND A.[InterfacePatientLabResultId] = B.[InterfacePatientLabResultId]
		WHERE A.[CreateDateTime] >= GetDate() - @LookbackDays OR A.[CreateDateTime] > @LookbackSince
	END
		


GO
