SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ======================================================================================
-- Author:		Lan Ma
-- Create date: 2015-11
-- Description:	Build the PatientHealthcareOrg and patientProviderOrgHierarchy Tables
-- Modified By: Lan Ma
-- Modified Date: 2016-05-16
-- Description:: 1. add extra function to populate PatientHealthcareorg table with an option,
--               When @AllHealthCareOrgs bit = 0, PatientHealthcareorg is populated with 
--               the default healthcareorg of a patient's winning provider, a patient has 
--               1 and only 1 healthcareorg
--               When @AllHealthCareOrgs bit = 1, PatientHealthcareorg is populated with 
--               all the healthcareorgs of a patient's providers, a patient has 
--               multiple providers and multiple healthcareorgs
--               2. add index on temp tables
--
-- Modified By: Chris lutz
-- Modified Date: 2016-06-27
-- Description::  Corrected issue with the where clause on the MERGE statement when multiple 
--				   Business Unit attribution is enabled. 
--
-- Modified By: Chris lutz
-- Modified Date: 2016-06-29
-- Description::  Reverted change based upon feedback from LAN 
-- =======================================================================================

/*
	Version		Date		Author	Change
	-------		----------	------	------	
	2.2.1		2017-01-03	YL		LBETL-393 add procedure run log to OrgHierarchy related procedures

*/
CREATE PROCEDURE [dbo].[ETLPatientProviderOrgHierarchy] @AllHealthCareOrgs bit = 0
AS
BEGIN


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientProviderOrgHierarchy';
	DECLARE @dataSource VARCHAR(20) = 'internal';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientProviderOrgHierarchy';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT =0;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

--Step 1: get the winner provider for a patient

SELECT 
PatientID, 
ProviderID,
[OtherReferenceIdentifier] AS NationalProviderIdentifier,
SourceSystemID,
[ExternalReferenceIdentifier]  AS medicareNo
INTO #patientProvider
FROM [dbo].[vwPortal_PatientProvider] t1
WHERE [DeleteInd] = 0 AND --Coalesce(OverrideInd,ISNULL(AttributedProviderInd,1)) = 1
      AttributedProviderInd = 1

CREATE NONCLUSTERED INDEX NCIX_#patientProvider_ProviderID ON #patientProvider(	[ProviderID] ASC)

--Step 2: build patient-healthcareorg relationship

declare @businessUnitID int = (SELECT distinct businessUnitID FROM [dbo].[vwPortal_BusinessUnit] WHERE [ParentBusinessUnitId] IS NULL AND DeleteInd =0)

--Undelete if found in vwPortal_HealthcareOrg where deleteInd = 1
IF EXISTS (SELECT  HealthcareOrgId 
               FROM [dbo].[vwPortal_HealthcareOrg]
               WHERE [BusinessUnitId] IN (@businessUnitID)
			         AND DeleteInd = 1 )
begin
update [vwPortal_HealthcareOrg] set DeleteInd = 0
where HealthcareOrgId in
 (SELECT distinct HealthcareOrgId 
               FROM [dbo].[vwPortal_HealthcareOrg]
               WHERE [BusinessUnitId] IN (@businessUnitID)
			         AND DeleteInd = 1)
end

-- add the highest business unit in the Healthcareorg, so the unattributed patients will be assigned to the highest level healthcareorg
IF Not EXISTS (SELECT  HealthcareOrgId 
               FROM [dbo].[vwPortal_HealthcareOrg]
               WHERE [BusinessUnitId] IN (@businessUnitID)
			         AND DeleteInd =0 )
BEGIN
INSERT INTO [dbo].[vwportal_HealthcareOrg]
           ([BusinessUnitId] 
           ,[BusinessUnitTypeId]
           ,[Name]
           ,[Description]
           ,[HealthcareOrgTypeId]
           ,[TIN]
           ,[OtherIdentifier]
           --,[StreetAddressId]
           ,[DeleteInd]
           ,[CreateDateTime]
           ,[ModifyDateTime]
           ,[CreateLBUserId]
           ,[ModifyLBUserId])
SELECT
			businessUnitID 
			,[BusinessUnitTypeId]
			,[Name]
			,[Description]
			,0
			,[ExternalCode]
			,[ExternalCode]
			,0
			,GETDATE() 
			,GETDATE()
			,1
			,1
FROM [dbo].[vwPortal_BusinessUnit] 
WHERE [ParentBusinessUnitId] IS NULL  AND DeleteInd =0
END

DECLARE @defaultbusinessUnitId INT = (SELECT businessUnitID FROM [dbo].[vwPortal_BusinessUnit] WHERE [ParentBusinessUnitId] IS NULL AND DeleteInd =0)
DECLARE @defaultOrgHealthcareId INT = (SELECT  HealthcareOrgId 
                                       FROM [dbo].[vwPortal_HealthcareOrg] 
                                       WHERE [BusinessUnitId] IN (SELECT businessUnitID FROM [dbo].[vwPortal_BusinessUnit] WHERE [ParentBusinessUnitId] IS NULL  AND DeleteInd =0)
									    AND DeleteInd =0)

--BEGIN Tran
 BEGIN TRY

DELETE FROM vwPortal_PatientHealthcareOrg

IF @AllHealthCareOrgs = 0 
BEGIN
INSERT INTO vwPortal_PatientHealthcareOrg
(PatientID
,HealthcareOrgID
,SourceSystemID
,ExternalReferenceIdentifier
,OtherReferenceIdentifier
,DeleteInd
,CreateDateTime
,ModifyDateTime
,ModifyLBUserId
,CreateLBUserId)
SELECT 
 t4.PatientID
,COALESCE(t5.HealthcareOrgID,@defaultOrgHealthcareId)
,t4.SourceSystemID
,NULL
,NULL
,0
,GETUTCDATE()
,GETUTCDATE()
,1
,1	
FROM #PatientProvider t4
LEFT JOIN [dbo].[vwPortal_ProviderHealthcareOrg] t5
ON t4.ProviderID = t5.ProviderID
AND t5.DeleteInd = 0 AND t5.DefaultInd = 1
END

IF @AllHealthCareOrgs = 1
BEGIN

INSERT INTO vwPortal_PatientHealthcareOrg
(PatientID
,HealthcareOrgID
,SourceSystemID
,ExternalReferenceIdentifier
,OtherReferenceIdentifier
,DeleteInd
,CreateDateTime
,ModifyDateTime
,ModifyLBUserId
,CreateLBUserId)
SELECT DISTINCT
 t4.PatientID
,COALESCE(t5.HealthcareOrgID,@defaultOrgHealthcareId)
,t4.SourceSystemID
,NULL
,NULL
,0
,GETUTCDATE()
,GETUTCDATE()
,1
,1	
FROM  vwPortal_PatientProvider t4
LEFT JOIN [dbo].[vwPortal_ProviderHealthcareOrg] t5
ON t4.ProviderID = t5.ProviderID
AND t5.DeleteInd = 0 AND t4.DeleteInd = 0
END
--COMMIT Tran


--Step 3: Build the patient-provider-OrgHierarchy relationship
IF  OBJECT_ID('patientProviderOrgHierarchy') IS NULL 

BEGIN

CREATE TABLE patientProviderOrgHierarchy
(PatientID  INT, 
 ProviderID INT,
 ProviderNPI VARCHAR(50),
 medicareNo VARCHAR(100),
 HealthcareOrgID INT,
 BusinessUnitId INT,
 SourceSystemID INT
)

END 



SELECT
[PatientID],
t1.ProviderID,
t1.NationalProviderIdentifier AS ProviderNPI,
medicareNo,
COALESCE(t2.[HealthcareOrgID],@defaultOrgHealthcareId) AS HealthcareOrgID,
COALESCE(t3.[BusinessUnitId],@defaultbusinessUnitId) AS BusinessUnitId,
t1.SourceSystemID
INTO #patientproviderOrg
FROM #PatientProvider t1
LEFT JOIN [dbo].[vwPortal_ProviderHealthcareOrg] t2
ON t1.ProviderID = t2.ProviderID
AND  t2.[DeleteInd] =0 AND t2.DefaultInd = 1
LEFT JOIN [dbo].[vwPortal_HealthcareOrg] t3
ON t2.HealthcareOrgID = t3.HealthcareOrgId

CREATE CLUSTERED INDEX CIX_#patientproviderOrg_PatientID ON #patientproviderOrg([PatientID])

--BEGIN Tran

MERGE patientProviderOrgHierarchy T
USING #patientproviderOrg S
ON  (T.PatientID = S.PatientID)
WHEN MATCHED
     THEN UPDATE SET T.ProviderID = S.ProviderID,
	                 T.ProviderNPI = S.ProviderNPI,
					 T.medicareNo = S.medicareNo,
                     T.HealthcareOrgId = S.HealthcareOrgId,
					 T.BusinessUnitId = S.BusinessUnitId,
					 T.SourceSystemID = S.SourceSystemID
WHEN NOT MATCHED BY TARGET
     THEN INSERT(PatientID, ProviderID,ProviderNPI,medicareNo,HealthcareOrgID,BusinessUnitId,SourceSystemID) VALUES(S.PatientID, S.ProviderID,S.ProviderNPI,S.medicareNo, S.HealthcareOrgID,S.BusinessUnitId, S.SourceSystemID)
WHEN NOT MATCHED BY SOURCE
    THEN DELETE
;
--COMMIT Tran




 select @InsertedRecord=count(1) from  #patientproviderOrg;
	 
          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	

 END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

  END



GO
