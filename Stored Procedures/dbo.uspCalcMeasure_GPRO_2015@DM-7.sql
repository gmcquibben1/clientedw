SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@DM-7] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL DROP TABLE [#RANKEDPATIENTS];
	IF OBJECT_ID('tempdb..#DIABETIC_PATIENTS') IS NOT NULL DROP TABLE [#DIABETIC_PATIENTS];
	IF OBJECT_ID('tempdb..#DiabVisit') IS NOT NULL DROP TABLE [#DiabVisit];

	DECLARE  @yearend datetime, @measureBeginDate datetime;

	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2014';

	--Establishing the Clinical Data sourcing indicators.
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END


	--Initial Patient pooling, for patients that have ranking for the BCS measure. 
	SELECT r.LBPatientID,
        r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,dateAdd(year,-1,@yearend)) as Age,
		r.Birthdate,
        GM.GproMeasureTypeID,
		GPM.DMEyeExam,
        GPM.DMConfirmed
	INTO #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GproPatientRankingId = r.GproPatientRankingID
	WHERE Name = 'DM-7' 
		AND gpm.DmRank IS NOT NULL
		AND (GPM.DmConfirmed IS NULL OR GPM.DmEyeExam IS NULL)
   
	--Denominator Exceptions
  
    --Age Check
    UPDATE rp
    SET rp.[DmConfirmed] = '19'
    FROM #RankedPatients rp
    WHERE rp.DmConfirmed IS NULL
		AND (Age > 75 OR Age < 18) 

	--------------------------------------------------------------------------------------------------------    
    -- Diabetes Diagnosis Requirement
	-------------------------------------------------------------------------------------------------------- 
    SELECT DISTINCT 
		STAGE.LbPatientId,
        pd.[DiagnosisDateTime] AS DiabDate
    INTO #DIABETIC_PATIENTS       --Drop Table  #DIABETIC_PATIENTS        
    FROM #RankedPatients STAGE
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = STAGE.LbPatientId
			AND pd.DiagnosisDateTime >= DATEADD(year, -2, @yearend)
			AND pd.DiagnosisDateTime < @yearend
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND ModuleType = 'DM'
			AND VariableName = 'DX_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pd.SourceSystemId
      
	--------------------------------------------------------------------------------------------------------    
    ---Updating Patients whom did not have Diabetes.t
	--------------------------------------------------------------------------------------------------------
     
	--     UPDATE #RankedPatients
	--     SET    DmConfirmed = '8'
	--     FROM   #RankedPatients RP
	--     LEFT JOIN #DIABETIC_PATIENTS DP
	--        ON RP.LBPATIENTID = DP.LBPATIENTID
	--     WHERE DP.LBPATIENTID IS NULL
	--        --AND RP.DmConfirmed is null
	--        --RP.GProPatientRankingId = DP.GProPatientRankingId
	--           
	--------------------------------------------------------------------------------------------------------    
		-- Annual Encounter Requirement
	--------------------------------------------------------------------------------------------------------
    
	--    SELECT DISTINCT LBPATIENTID,
	--                    MAX(PP.ProcedureDateTime) as LastVisit
	--    INTO   #DiabVisit --Drop Table #diabVisit
	--    FROM   #DIABETIC_PATIENTS DP
	--    JOIN   DBO.PATIENTPROCEDURE PP
	--        ON DP.LBPATIENTID = PP.PatientID
	--    JOIN   DBO.PatientProcedureProcedureCode PPPC
	--        ON PP.PatientProcedureId = PPPC.PatientProcedureId
	--    JOIN   DBO.ProcedureCode PC
	--        ON PPPC.ProcedureCodeId = PC.ProcedureCodeId
	--    JOIN   dbo.[GproEvaluationCode] GEC
	--        ON  PC.ProcedureCode = GEC.Code
	--    where [ModuleType] = 'DM'                                    AND 
	--          [VariableName] = N'ENCOUNTER_CODE'                     AND
	--          PP.PROCEDUREDATETIME >= dateAdd(year,-1,@yearend)       AND
	--          PP.PROCEDUREDATETIME <  @yearend      
	--          --PP.PROCEDUREDATETIME = DP.DiabDate      --Not Certain if it has to be on the same day as a Diabetic Visit.
	--    GROUP BY LBPATIENTID
    

	--------------------------------------------------------------------------------------------------------    
		-- Annual Encounter Requirement Update.
	--------------------------------------------------------------------------------------------------------
     
	--     UPDATE #RankedPatients
	--     SET    DmConfirmed = '8'
	--     FROM   #RankedPatients RP
	--     LEFT JOIN #DiabVisit DV
	--        ON RP.LBPATIENTID = DV.LBPATIENTID
	--     WHERE DV.LBPATIENTID IS NULL 

    -- Setting the patients verified with Diabetes as Confirmed.
     UPDATE RP
     SET RP.DmConfirmed = '2'
     FROM #RankedPatients RP
		JOIN #DIABETIC_PATIENTS DP ON RP.LbPatientId = DP.LbPatientId

      
	--------------------------------------------------------------------------------------------------------    
    -- Exceptions from Measure Spec
	--------------------------------------------------------------------------------------------------------

	--------------------------------------------------------------------------------------------------------    
    -- Numerator Checks: Eye Exam within the measure period.
	--------------------------------------------------------------------------------------------------------    
    
    
    SELECT DISTINCT 
		DP.LbPatientId,
        MAX(PP.ProcedureDateTime) AS LastVisit
    INTO   #EyeExam --Drop Table #diabVisit
    FROM   #DIABETIC_PATIENTS DP
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = DP.LbPatientId
			AND PP.ProcedureDateTime >= DATEADD(year,-1,@yearend)
			AND PP.ProcedureDateTime < @yearend
		INNER JOIN DBO.[GproEvaluationCode] GEC ON pp.ProcedureCode = GEC.Code
			AND GEC.ModuleType = 'DM'
			AND GEC.ModuleIndicatorGPRO = '7'
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS      
    GROUP BY LbPatientId
                               

    UPDATE RP
    SET   RP.DMEyeExam = 2
    FROM #RankedPatients RP
    JOIN #EyeExam    EE
        ON RP.LBPatientID = EE.LBPatientID

	--------------------------------------------------------------------------------------------------------          
	   -- Updating the required tables
	--------------------------------------------------------------------------------------------------------    
	--
	--select distinct *
	--from #rankedPatients
   
     --update if the DM Was confirmed.   
    UPDATE gpm
    SET gpm.DmConfirmed = gpr.DmConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
    FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
    WHERE gpr.DmConfirmed IS NOT NULL
		ANd gpm.DmConfirmed IS NULL


    --updating measure table with patients who had been tested
    UPDATE gpm
    SET gpm.DMEyeExam = GPR.DMEyeExam, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
    FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpr.DmEyeExam IS NOT NULL
		AND gpm.DmEyeExam IS NULL

END

     
GO
