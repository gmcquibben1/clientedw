SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FaceSheetMedicationLoad] 
	@PatientId INTEGER
AS
BEGIN
	DECLARE @MedicationOutput TABLE 
	(
		PatientMedicationId		int,
		BrandName				varchar(250),
		GenericName				varchar(250),
		MedicationDose			varchar(75),
		SIGCode					varchar(30),
		MedicationStartDate		datetime,
		ndc_code				varchar(20) NULL
	)	

	--insert rows from the PatientMedication table to give precedence to the "MedicationNameFromSource" value that was sent from the client system
	INSERT INTO @MedicationOutput
		(PatientMedicationId, BrandName, GenericName, MedicationDose, SIGCode, MedicationStartDate, ndc_code)
	SELECT DISTINCT     
		dbo.PatientMedication.PatientMedicationId,
		dbo.PatientMedication.MedicationNameFromSource, 
		dbo.PatientMedication.MedicationNameFromSource, 
		dbo.PatientMedication.MedicationDoseFromSource, 
		dbo.PatientMedication.SIGCode,
		dbo.PatientMedication.MedicationStartDate,
		HedisMedsList.ndc_code													-- If this value is NOT NULL then this med IS high risk
	FROM dbo.PatientMedication 
		INNER JOIN dbo.Medication ON PatientMedication.MedicationId = Medication.MedicationId 
		LEFT OUTER JOIN dbo.HedisMedsList ON HedisMedsList.ndc_code = PatientMedication.NDCCode AND HedisMedsList.list IN ( 'DAE-A','DAE-B','DAE-C' ) 
	WHERE dbo.PatientMedication.PatientId = @PatientId 
		AND dbo.PatientMedication.DeleteInd = 0 
		AND (dbo.PatientMedication.MedicationEndDate > GETDATE() OR dbo.PatientMedication.MedicationEndDate IS NULL)
		AND (dbo.PatientMedication.StatusDescription IS NULL OR (dbo.PatientMedication.StatusDescription IS NOT NULL AND dbo.PatientMedication.StatusDescription <> 'inactive'))
		AND dbo.PatientMedication.MedicationNameFromSource IS NOT NULL
		AND len(dbo.PatientMedication.MedicationNameFromSource) > 0

	--insert any remaining rows that had blank descriptions in the MedicationNameFromSource by using the name from the Medication table
	INSERT INTO @MedicationOutput
		(PatientMedicationId, BrandName, GenericName, MedicationDose, SIGCode, MedicationStartDate, ndc_code)
	SELECT DISTINCT     
		dbo.PatientMedication.PatientMedicationId,
		dbo.Medication.BrandName, 
		dbo.Medication.GenericName,
		dbo.Medication.MedicationDose,
		dbo.PatientMedication.SIGCode,
		dbo.PatientMedication.MedicationStartDate,
		HedisMedsList.ndc_code													-- If this value is NOT NULL then this med IS high risk
	FROM dbo.PatientMedication 
		INNER JOIN dbo.Medication ON PatientMedication.MedicationId = Medication.MedicationId 
		LEFT OUTER JOIN dbo.HedisMedsList ON HedisMedsList.ndc_code = PatientMedication.NDCCode AND HedisMedsList.list IN ( 'DAE-A','DAE-B','DAE-C' ) 
	WHERE dbo.PatientMedication.PatientId = @PatientId 
		AND dbo.PatientMedication.DeleteInd = 0 
		AND (dbo.PatientMedication.MedicationEndDate > GETDATE() OR dbo.PatientMedication.MedicationEndDate IS NULL)
		AND (dbo.PatientMedication.StatusDescription IS NULL OR (dbo.PatientMedication.StatusDescription IS NOT NULL AND dbo.PatientMedication.StatusDescription <> 'inactive'))
		AND NOT EXISTS (SELECT 1 FROM @MedicationOutput mo WHERE mo.PatientMedicationId = dbo.PatientMedication.PatientMedicationId) 
		AND (len(Medication.BrandName) > 0 and len(Medication.GenericName) > 0)


	--spit out the output
	SELECT
		BrandName, 
		GenericName,
		MedicationDose,
		SIGCode,
		MAX(MedicationStartDate) AS MedicationStartDate,
		ndc_code
	FROM @MedicationOutput
	GROUP BY GenericName, BrandName, MedicationDose, SIGCode, ndc_code
	ORDER BY GenericName, BrandName, MedicationDose, SIGCode ASC
END


GO
