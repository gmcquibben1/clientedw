SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientLabResultLoad] 
/*===============================================================
	CREATED BY: 	RJN
	CREATED ON:		2015-04-15
	INITIAL VER:	
	MODULE:			Patient Clinical data 		
	DESCRIPTION:	Returns lab results to be displayed in the patient clinical data page
	PARAMETERS:		@PatientId
	RETURN VALUE(s)/OUTPUT:	(Optional) Especially when returning specific values or error codes.
	PROGRAMMIN NOTES: (Optional) This is sometimes helpful to explain some implementation details that may not be obvious.
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.5.2       2015-04-15		RJN						New Procedure
	2.2.1		2017-01-19		UC			LBETL-527	Include orders with no results
=================================================================*/
	@PatientId INTEGER
AS

SELECT
	ISNULL(dbo.PatientLabResult.ObservationDate, dbo.PatientLabOrder.OrderDate) AS ObservationDate,
	dbo.PatientLabOrder.ServiceDescription,
	dbo.PatientLabResult.ObservationCode1,
	dbo.PatientLabResult.ObservationDescription1,
	dbo.PatientLabResult.ObservationCodingSystemName1,
	dbo.PatientLabResult.ObservationCode2,
	dbo.PatientLabResult.ObservationDescription2,
	dbo.PatientLabResult.ObservationCodingSystemName2,	
	dbo.PatientLabResult.Value,
	dbo.PatientLabResult.Units,
	dbo.PatientLabResult.ReferenceRange,
	dbo.PatientLabResult.AbnormalFlag,
	dbo.PatientLabResult.ResultStatus,
	dbo.PatientLabResult.ResultComment,
	dbo.SourceSystem.Description as SourceSystem

FROM dbo.PatientLabOrder
	 LEFT OUTER JOIN dbo.PatientLabResult ON PatientLabResult.PatientLabOrderId = PatientLabOrder.PatientLabOrderId  LEFT OUTER JOIN
 	 dbo.SourceSystem ON PatientLabOrder.SourceSystemId = SourceSystem.SourceSystemId 
WHERE dbo.PatientLabOrder.PatientId = @Patientid  

ORDER BY PatientLabOrder.OrderDate DESC, PatientLabResult.ObservationDate DESC

GO
