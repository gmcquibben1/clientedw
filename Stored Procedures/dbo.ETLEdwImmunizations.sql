SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwImmunizations] 
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES 
UPDATE PatientImmunizationProcessQueue SET PerformingClinician = '' WHERE PerformingClinician IS NULL
UPDATE PatientImmunizationProcessQueue SET ImmunizationDose = '' WHERE ImmunizationDose IS NULL
UPDATE PatientImmunizationProcessQueue SET MedicationRouteTypeID = 0 WHERE MedicationRouteTypeID IS NULL

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId,LbPatientID, ServiceDate, ImmunizationCodeId --,PerformingClinician,   ImmunizationDose,MedicationRouteTypeID
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				--LEN(ISNULL([MaterialManufacturer],''))  +  -- THE BIGGER THE [MaterialManufacturer] Text THE BETTER IT IS 
				--LEN(ISNULL([MaterialLotNumber],''))   + -- THE BIGGER THE [MaterialLotNumber] Text THE BETTER IT IS  
				--LEN(ISNULL([ImmunizationUnits],''))    -- THE BIGGER THE [ImmunizationUnits] Text THE BETTER IT IS   
				InterfacePatientImmunizationId
				DESC 
			) SNO
	FROM PatientImmunizationProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientImmunizationProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientImmunizationId INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientImmunizationId,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientImmunizationId,PatientID
FROM 
(	
MERGE [DBO].PatientImmunization AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,[ServiceDate],[ImmunizationCodeId] ,[ImmunizationDose]
      ,[ImmunizationUnits],[MaterialLotNumber],[MaterialManufacturer] ,[MedicationRouteTypeID],PerformingClinician AS [PerformedByClinician], 
	    0 AS DeleteInd,GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId, [EncounterIdentifier]
		FROM [dbo].PatientImmunizationProcessQueue PIPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PIPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  ([OrigPatientId],[PatientID] ,[SourceSystemId],[ServiceDate],[ImmunizationCodeId] ,[ImmunizationDose]
      ,[ImmunizationUnits],[MaterialLotNumber],[MaterialManufacturer] ,[MedicationRouteTypeID],[PerformedByClinician]
	  ,[DeleteInd],[CreateDateTime],[ModifyDateTime] ,[CreateLBUserId],[ModifyLBUserId],[EncounterIdentifier])
ON (target.[SourceSystemID] = source.[SourceSystemID] AND target.PatientID = source.PatientID 
	AND ISNULL(target.ServiceDate,'2099-12-31') = ISNULL(source.ServiceDate,'2099-12-31') AND target.ImmunizationCodeId = source.ImmunizationCodeId 
	--AND  target.[PerformedByClinician] = source.[PerformedByClinician] AND target.ImmunizationDose = source.ImmunizationDose AND target.MedicationRouteTypeID = source.MedicationRouteTypeID
	)
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
		--[PatientID] =  source.[PatientID], 
		[SourceSystemId] =  source.[SourceSystemId], 
		[ServiceDate] =  source.[ServiceDate], 
		[ImmunizationCodeId] =  source.[ImmunizationCodeId], 
		[ImmunizationDose] =  source.[ImmunizationDose], 
		[ImmunizationUnits] =  source.[ImmunizationUnits], 
		[MaterialLotNumber] =  source.[MaterialLotNumber], 
		[MaterialManufacturer] =  source.[MaterialManufacturer], 
		[MedicationRouteTypeID] =  source.[MedicationRouteTypeID], 
		[PerformedByClinician] =  source.[PerformedByClinician], 
		[DeleteInd] =  source.[DeleteInd], 
		--[CreateDateTime] =  source.[CreateDateTime], 
		[ModifyDateTime] =  source.[ModifyDateTime], 
		--[CreateLBUserId] =  source.[CreateLBUserId], 
		[ModifyLBUserId] =  source.[ModifyLBUserId],
		[EncounterIdentifier] = source.[EncounterIdentifier]
WHEN NOT MATCHED THEN
INSERT ([PatientID] ,[SourceSystemId],[ServiceDate],[ImmunizationCodeId] ,[ImmunizationDose]
      ,[ImmunizationUnits],[MaterialLotNumber],[MaterialManufacturer] ,[MedicationRouteTypeID],[PerformedByClinician]
	  ,[DeleteInd],[CreateDateTime],[ModifyDateTime] ,[CreateLBUserId],[ModifyLBUserId],[EncounterIdentifier]
	   )
VALUES (source.[PatientID] ,source.[SourceSystemId],source.[ServiceDate],source.[ImmunizationCodeId] ,source.[ImmunizationDose]
      ,source.[ImmunizationUnits],source.[MaterialLotNumber],source.[MaterialManufacturer] ,source.[MedicationRouteTypeID],[PerformedByClinician]
	  ,source.[DeleteInd],source.[CreateDateTime],source.[ModifyDateTime] ,source.[CreateLBUserId],source.[ModifyLBUserId],source.[EncounterIdentifier])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientImmunizationId,INSERTED.PatientImmunizationId) PatientImmunizationId, source.PatientID
) x ;

IF OBJECT_ID('PatientImmunization_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientImmunization_PICT(PatientImmunizationId,PatientId,SwitchDirectionId)
	SELECT RT.PatientImmunizationId, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientImmunizationProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'Immunization',GetDate()
FROM PatientImmunizationProcessQueue
WHERE ProcessedInd = 1 

UPDATE A 
SET HoldingReleaseInd = 1 
FROM [dbo].[PatientImmunizationProcessQueue_HoldingBay] A 
INNER JOIN PatientImmunizationProcessQueue B ON A.InterfacePatientImmunizationId = B.InterfacePatientImmunizationId
WHERE B.ProcessedInd = 1 

--INSERT INTO[dbo].[PatientImmunizationProcessQueue_HoldingBay]
--([InterfacePatientImmunizationId],[InterfacePatientID],[LbPatientId]
--,[InterfaceSystemId],[ServiceDate],[ImmunizationCode]
--,[ImmunizationCodeId],[ImmunizationCodeSystemName],[ImmunizationDescription]
--,[ImmunizationDose],[ImmunizationUnits],[MaterialLotNumber]
--,[MaterialManufacturer],[RouteCode],[RouteCodeSystemName]
--,[RouteDescription],[MedicationRouteTypeID],[PerformingClinician]
--,[CreateDateTime],[InterfacePatientImmunizationId_HoldingOverride],[InterfacePatientID_HoldingOverride]
--,[LbPatientId_HoldingOverride],[InterfaceSystemId_HoldingOverride],[ServiceDate_HoldingOverride]
--,[ImmunizationCode_HoldingOverride],[ImmunizationCodeId_HoldingOverride],[ImmunizationCodeSystemName_HoldingOverride]
--,[ImmunizationDescription_HoldingOverride],[ImmunizationDose_HoldingOverride],[ImmunizationUnits_HoldingOverride]
--,[MaterialLotNumber_HoldingOverride],[MaterialManufacturer_HoldingOverride],[RouteCode_HoldingOverride]
--,[RouteCodeSystemName_HoldingOverride],[RouteDescription_HoldingOverride],[MedicationRouteTypeID_HoldingOverride]
--,[PerformingClinician_HoldingOverride],[CreateDateTime_HoldingOverride],[HoldingStartDate]
--,[HoldingStartReason])
--SELECT 
--A.[InterfacePatientImmunizationId],A.[InterfacePatientID],A.[LbPatientId]
--,A.[InterfaceSystemId],A.[ServiceDate],A.[ImmunizationCode]
--,A.[ImmunizationCodeId],A.[ImmunizationCodeSystemName],A.[ImmunizationDescription]
--,A.[ImmunizationDose],A.[ImmunizationUnits],A.[MaterialLotNumber]
--,A.[MaterialManufacturer],A.[RouteCode],A.[RouteCodeSystemName]
--,A.[RouteDescription],A.[MedicationRouteTypeID],A.[PerformingClinician]
--,A.[CreateDateTime],A.[InterfacePatientImmunizationId],A.[InterfacePatientID]
--,A.[LbPatientId],A.[InterfaceSystemId],A.[ServiceDate]
--,A.[ImmunizationCode],A.[ImmunizationCodeId],A.[ImmunizationCodeSystemName]
--,A.[ImmunizationDescription],A.[ImmunizationDose],A.[ImmunizationUnits]
--,A.[MaterialLotNumber],A.[MaterialManufacturer],A.[RouteCode]
--,A.[RouteCodeSystemName],A.[RouteDescription],A.[MedicationRouteTypeID]
--,A.[PerformingClinician],A.[CreateDateTime],GetDate()
--,'Duplicacy'
--FROM PatientImmunizationProcessQueue A 
--LEFT JOIN [dbo].[PatientImmunizationProcessQueue_HoldingBay]  B ON A.InterfacePatientImmunizationId = B.InterfacePatientImmunizationId
--WHERE ISNULL(A.ProcessedInd,0) <> 1 AND B.InterfacePatientImmunizationId IS NULL 

END

GO
