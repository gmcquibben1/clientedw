SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@PREV-9] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];
	IF OBJECT_ID('tempdb..#ENC') IS NOT NULL 
		DROP TABLE [#ENC];
	IF OBJECT_ID('tempdb..#BMI_VALUE') IS NOT NULL 
		DROP TABLE [#BMI_VALUE];
	IF OBJECT_ID('tempdb..#NORMAL') IS NOT NULL 
		DROP TABLE [#NORMAL];
	IF OBJECT_ID('tempdb..#OUTSIDE') IS NOT NULL 
		DROP TABLE [#OUTSIDE];
	IF OBJECT_ID('tempdb..#FOLLOW_UP') IS NOT NULL 
		DROP TABLE [#FOLLOW_UP];
		

	DECLARE  @yearend datetime, @measureBeginDate datetime;
	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2015';

	-- get the list of source system ids limited to clinical-only or clinical-and-claims
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END


	---------------------------------------------------------------------------------------
	--Initial Patient pooling, for patients that have ranking for the PREV-9 measure. 
	---------------------------------------------------------------------------------------
	select r.LBPatientID,
        r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,@measureBeginDate) as Age,
		r.Birthdate,
        GM.GproMeasureTypeID,
		GPM.PcBmiScreenRank,
		GPM.PcBmiScreenConfirmed,
		GPM.PcBmiCalculated,
        GPM.PcBmiNormal,
		gpm.PcBmiPlan,
		gpm.PcBmiComments,
		GETUTCDATE() as ModifyDateTime
	INTO #RankedPatients --	Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE GM.Name = 'PREV-9' 
		AND gpm.PcBmiScreenRank IS NOT NULL
		AND (gpm.PcBmiScreenConfirmed IS NULL OR gpm.PcBmiCalculated IS NULL OR gpm.PcBmiNormal IS NULL OR gpm.PcBmiPlan IS NULL)

	----------------------------CHECK IF PATIENT HAS ENCOUNTER DURING MEASURMENT YEAR------------------------------------------------
	SELECT DISTINCT RP.LbPatientId, 
					rp.age,  
					PP.ProcedureDateTime AS ENC_DT, 
					ROW_NUMBER () OVER (PARTITION BY rp.LbPatientId ORDER BY pp.ProcedureDateTime DESC)LATEST_enc
	INTO #ENC -- drop table #enc
	FROM #RankedPatients RP
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = RP.LbPatientId
			AND pp.ProcedureDateTime >= @measureBeginDate
			AND pp.ProcedureDateTime < @yearEnd
		INNER JOIN GproEvaluationCode hvsc ON hvsc.Code = pp.ProcedureCode
			AND hvsc.[ModuleType]= 'PREV'
			AND hvsc.[ModuleIndicatorGPRO]='9'
			AND HVSC.[VariableName] IN   ('ENCOUNTER_CODE')
		INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]
	GROUP BY RP.LbPatientId, rp.Age, pp.ProcedureDateTime



	-------------------------------check if encounter has bmi documented DURING ENCOUNTER OR 6 MONTHS PRIOR-----------------------------------------------------
	SELECT DISTINCT 
		SUB.*, 
		ROW_NUMBER () OVER (PARTITION BY LbPatientId ORDER BY ServiceDateTime DESC) LATEST_BMI_DT
	INTO #BMI_VALUE -- DROP TABLE #BMI_VALUE
	FROM 
	(
		SELECT DISTINCT 
			enc.*
			, gec.[VariableName]
			, LTRIM(RTRIM(gec.CodeDescription)) AS CodeDescription
			, NULL AS BMI_VALUE
			, PP.ProcedureDateTime AS ServiceDateTime
		FROM #enc enc
			INNER JOIN vwPatientProcedure pp ON pp.PatientId = enc.LbPatientId
				AND pp.ProcedureDateTime >= DATEADD(month,-6,ENC.ENC_DT)
				AND pp.ProcedureDateTime <= ENC.ENC_DT
			INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
				AND gec.ModuleType = 'PREV'
				AND gec.ModuleIndicatorGPRO = '9'
				ANd gec.VariableName IN ('BMI_ABNORM_CODE','DX_CODE')
			INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
		UNION
		SELECT DISTINCT enc.*
			, gec.[VariableName]
			, LTRIM(RTRIM(gec.CodeDescription)) AS CodeDescription
			, NULL AS BMI_VALUE
			, pd.[DiagnosisDateTime] AS ServiceDateTime
		FROM #enc enc
			INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = enc.LbPatientId
				AND pd.DiagnosisDateTime >= DATEADD(month,-6,ENC.ENC_DT)
				AND pd.DiagnosisDateTime <= ENC.ENC_DT
			INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
				AND gec.[ModuleType]= 'PREV'
				AND gec.[ModuleIndicatorGPRO]='9'
				AND gec.[VariableName] IN   ('DX_CODE')
			INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pd.[SourceSystemID]
		UNION
		SELECT DISTINCT enc.*
			, HVSC.[VariableName]
			, LTRIM(RTRIM(HVSC.CodeDescription)) AS CodeDescription
			, NULL AS BMI_VALUE
			, plr.[ObservationDate] AS ServiceDateTime
		FROM #enc  enc
			INNER JOIN [dbo].[PatientLabOrder] plo ON plo.[PatientID] = enc.[LbPatientID]
			INNER JOIN [dbo].[PatientLabResult] plr ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]               
				AND plr.[ObservationDate] >= DATEADD(month, -6, ENC.ENC_DT )  
				AND plr.[ObservationDate] <= ENC.ENC_DT
			INNER JOIN [DBO].[GproEvaluationCode] HVSC ON (HVSC.[Code] = plr.[ObservationCode1] OR HVSC.[Code] = plr.[ObservationCode2])
						AND HVSC.[ModuleType] = 'PREV'  
						AND HVSC.[VariableName] = 'BMI_CODE'
			INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID]
		UNION
		SELECT DISTINCT ENC.*
			, 'BMI_VALUE' AS [VariableName]
			, 'Body Mass Index (BMI)' AS CodeDescription
			, (PV.[WeightKG]/PV.[HeightCM]/PV.[HeightCM])*10000 AS BMI_VALUE
			, PV.[ServiceDateTime] AS ServiceDateTime
		FROM #enc  enc
			INNER JOIN PatientVitals PV ON ENC.LbPatientId = PV.PatientId
				AND PV.ServiceDateTime >= DATEADD(month, -6, ENC.ENC_DT)
				AND TRY_CONVERT(FLOAT, PV.HeightCM) IS NOT NULL 
				AND TRY_CONVERT(FLOAT, PV.HeightCM) <> 0
				AND TRY_CONVERT(FLOAT, PV.WeightKG) IS NOT NULL 
				AND TRY_CONVERT(FLOAT, PV.WeightKG) <> 0
			INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PV.[SourceSystemID]   
	) AS SUB

	-------------------------NORMAL PARAMETERS AND PATIENT WITH FOLLOW UP PLAN------------------------------------------------------------
	--Age 65 years and older BMI ≥ 23 and < 30 kg/m2 
	SELECT *
	INTO #NORMAL-- DROP TABLE #NORMAL
	FROM #BMI_VALUE
	WHERE AGE >64
		AND LATEST_BMI_DT = 1 -- LATEST BMI DT
		AND (BMI_VALUE >= 23 OR BMI_VALUE < 30)
	UNION
	--NORMAL PARAMETERS
	-- Age 18 – 64 years BMI ≥ 18.5 and < 25 kg/m2
	SELECT *
	FROM #BMI_VALUE
	WHERE AGE >= 18 AND AGE <= 64
		AND LATEST_BMI_DT = 1 -- LATEST BMI DT
		AND (BMI_VALUE >= 18.5 OR BMI_VALUE < 25)
	UNION
	--PATIENT BMI OUTSIDE NORMAL PARAMETERS BUT HAS A FOLLOW-UP PLAN DOCUMENTED
	SELECT *
	FROM #BMI_VALUE
	WHERE (CodeDescription LIKE '%overweight%'
		OR CodeDescription LIKE '%underweight%'
		OR CodeDescription LIKE '%above normal%'
		OR CodeDescription LIKE '%below normal%')
		AND LATEST_BMI_DT = 1 -- LATEST BMI DT



	-----------------------------------------------GET PATIENT OUTSIDE PARAMETERS --------------------------------------------------
	SELECT *
	INTO #OUTSIDE -- DROP TABLE #OUTSIDE
	FROM #BMI_VALUE
	EXCEPT 
	SELECT *
	FROM #NORMAL

	
	---------------------------------------OUTSIDE PARAMETERS WITH follow-up plan is documented during the encounter or during the previous six months of the current encounter.------------------------------------------------
	SELECT DISTINCT enc.*
	INTO #FOLLOW_UP -- DROP TABLE #FOLLOW_UP 
	FROM #OUTSIDE enc
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = enc.LbPatientId
			AND pp.ProcedureDateTime >= DATEADD(month,-6,ENC.ENC_DT)
			AND pp.ProcedureDateTime <= ENC.ENC_DT
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND gec.[ModuleType]= 'PREV'
			AND gec.[ModuleIndicatorGPRO]='9'
			AND gec.[VariableName] IN   ('BMI_FOLLOW_UP_CODE')
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
	UNION
	SELECT DISTINCT enc.*
	FROM #OUTSIDE enc
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = enc.LbPatientId
			AND pd.DiagnosisDateTime >= DATEADD(month, -6, ENC.ENC_DT)
			AND pd.DiagnosisDateTime <= ENC.ENC_DT
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]= 'PREV'
			AND gec.[ModuleIndicatorGPRO]='9'
			AND gec.[VariableName] IN   ('BMI_FOLLOW_UP_CODE')
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pd.SourceSystemId

	-----------------------patient age comfirmed 18 AND OLDER----------------------------------
	UPDATE rp
	SET rp.PcBmiScreenConfirmed = '2' -- yes
	--	select distinct rp.*
	FROM #RankedPatients rp
	WHERE RP.Age >= 18
		AND rp.PcBmiScreenConfirmed IS NULL

	-------------------------Determine if a BMI was Calculated.--------------------------------
	UPDATE rp
	SET rp.PcBmiCalculated = '2' -- yes
	--	select distinct rp.*
	FROM #RankedPatients rp
		INNER JOIN #BMI_VALUE BV ON RP.LBPatientId = BV.LBPatientId
	WHERE RP.PcBmiCalculated IS NULL

	------------------Determine if BMI is Normal EXCLUDE PATIENT WITH CODE OUTSIDE PARAMETERS WITH FOLLOW UP PLAN DOCUEMENTED--------------------------
	UPDATE rp
	SET rp.PcBmiNormal = '2' -- yes
	--	select distinct rp.*
	FROM #RankedPatients rp
		INNER JOIN #NORMAL BV ON RP.LBPatientId = BV.LBPatientId
	WHERE RP.PcBmiNormal IS NULL
		AND (BV.CodeDescription LIKE '%overweight%'
			OR BV.CodeDescription LIKE '%underweight%'
			OR BV.CodeDescription LIKE '%above normal%'
			OR BV.CodeDescription LIKE '%below normal%')

	UPDATE rp
	SET rp.PcBmiNormal = '1' -- yes
	--	select distinct rp.*
	FROM #RankedPatients rp
		INNER JOIN #NORMAL BV ON RP.LBPatientId = BV.LBPatientId
	WHERE RP.PcBmiNormal IS NULL
		AND (BV.CodeDescription LIKE '%overweight%'
			OR BV.CodeDescription LIKE '%underweight%'
			OR BV.CodeDescription LIKE '%above normal%'
			OR BV.CodeDescription LIKE '%below normal%')

	UPDATE rp
	SET rp.PcBmiNormal = 1
	FROM #RankedPatients rp
		INNER JOIN #BMI_VALUE bmi ON bmi.LbPatientId = rp.LbPatientId
			AND bmi.LATEST_BMI_DT = 1
	WHERE (rp.AGE >64
			AND (bmi.BMI_VALUE >= 23.0 OR bmi.BMI_VALUE < 30)
		)
		OR
		(rp.AGE >= 18 
			AND rp.AGE <= 64
			AND (bmi.BMI_VALUE >= 18.5 OR bmi.BMI_VALUE < 25)
		)

	UPDATE rp
	SET rp.PcBmiNormal = 0
	FROM #RankedPatients rp
		INNER JOIN #BMI_VALUE bmi ON bmi.LbPatientId = rp.LbPatientId
			AND bmi.LATEST_BMI_DT = 1
	WHERE (rp.AGE > 64
		AND (bmi.BMI_VALUE < 23 OR bmi.BMI_VALUE >= 30)
		)
		OR
		(rp.AGE >= 18 
		AND rp.AGE <= 64
		AND (bmi.BMI_VALUE < 18.5 OR bmi.BMI_VALUE >= 25)
		)
		OR
		(bmi.CodeDescription LIKE '%overweight%'
		OR bmi.CodeDescription LIKE '%underweight%'
		OR bmi.CodeDescription LIKE '%above normal%'
		OR bmi.CodeDescription LIKE '%below normal%')

	------------------------------Determine if Follow-Up Plan is Documented--------------------------------------------------------
	UPDATE rp
	SET PcBmiPlan = '1' -- yes
	--	select distinct rp.*
	FROM #RankedPatients rp
		INNER JOIN #FOLLOW_UP BV ON RP.LBPatientId = BV.LBPatientId
	WHERE RP.PcBmiPlan IS NULL 
		AND RP.PcBmiNormal = '0'

	------------------------PATIENT THAT WAS CODED OUTSIDE PARAMETERS AND FOLLOW UP PLAN DOCUMENTED-----------------------------------------
	UPDATE rp
	SET rp.PcBmiPlan = '1' -- yes
	--	select distinct rp.*
	FROM #RankedPatients rp
		INNER JOIN #NORMAL BV ON RP.LBPatientId = BV.LBPatientId
	WHERE RP.PcBmiPlan IS NULL
		AND (BV.CodeDescription LIKE '%overweight%'
			OR BV.CodeDescription LIKE '%underweight%'
			OR BV.CodeDescription LIKE '%above normal%'
			OR BV.CodeDescription LIKE '%below normal%')

	--------------------------------------------------------------------------------------------------------          
	-- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	UPDATE gpm
	SET gpm.PcBmiScreenConfirmed = gpr.PcBmiScreenConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcBmiScreenRank IS NOT NULL
		AND gpm.PcBmiScreenConfirmed IS NULL

	UPDATE gpm
	SET gpm.PcBmiCalculated  = gpr.PcBmiCalculated, gpm.PcBmiNormal = gpr.PcBmiNormal, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcBmiScreenRank IS NOT NULL
		AND gpm.PcBmiCalculated IS NULL

	UPDATE gpm
	SET gpm.PcBmiPlan = gpr.PcBmiPlan, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcBmiScreenRank IS NOT NULL
		AND gpm.PcBmiPlan IS NULL


END
GO
