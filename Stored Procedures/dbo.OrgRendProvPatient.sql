SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[OrgRendProvPatient] @ForceProcedure int = 0
WITH EXEC AS CALLER
AS
--SELECT DISTINCT PatientId, ProviderID, Clinician, DiagnosisDateTime AS [EncounterDate] 
--INTO #DIAGENCOUNTERS
--FROM PatientDiagnosis

--First, calculate based on encounters table if encounters exist
IF (SELECT COUNT(*) FROM PatientEncounter) > 0 and @ForceProcedure = 0 --Checking to see if there are any encounters, if so calculate based on encounters
BEGIN
     --Clear tables
     TRUNCATE TABLE OrgRenderingEncounter
     
     INSERT INTO OrgRenderingEncounter 
        ( LbPatientId
          ,RenderingProviderName
          ,Location
          ,EncounterDate
          ,CPT
        ) SELECT distinct PE.PatientId, PE.Clinician, PE.Location, PE.EncounterDate, PC.ProcedureCode
        FROM PatientEncounter PE
        JOIN PatientProcedure PP ON PE.SourceEncounterId = PP.EncounterIdentifier
  JOIN dbo.PatientProcedureProcedureCode PPPC ON PPPC.PatientProcedureId = PP.PatientProcedureId
  JOIN ProcedureCode PC ON PPPC.ProcedureCodeId = PC.ProcedureCodeId
 WHERE PC.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Outpatient')
    END 
    IF (SELECT COUNT(*) FROM PatientEncounter) = 0 OR @ForceProcedure = 1 
    BEGIN--if no encounters, calculate based on Outpatient procedures
    truncate table OrgRenderingEncounter
SELECT DISTINCT PP.PatientID, PerformedByProviderID as ProviderID, PerformedByClinician as Clinician, PE.Location , ProcedureDateTime AS [EncounterDate], PC.ProcedureCode
INTO #PROCENCOUNTERS
FROM PatientProcedure PP 
  JOIN dbo.PatientProcedureProcedureCode PPPC ON PPPC.PatientProcedureId = PP.PatientProcedureId
  JOIN ProcedureCode PC ON PPPC.ProcedureCodeId = PC.ProcedureCodeId
  LEFT JOIN PatientEncounter PE ON PP.PatientID = PE.PatientId and convert(date,PP.ProcedureDateTime) = PE.EncounterDate
WHERE PC.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Outpatient')

TRUNCATE TABLE OrgRenderingEncounter

INSERT INTO OrgRenderingEncounter (
   LbPatientId
  ,RenderingProviderNPI
  ,RenderingProviderName
  ,Location
  ,EncounterDate, CPT
) --SELECT PatientId, ProviderID, Clinician, [EncounterDate] FROM #DIAGENCOUNTERS
--UNION ALL ( 
SELECT PatientId, ProviderID, Clinician, Location, [EncounterDate], ProcedureCode FROM #PROCENCOUNTERS 
--)
    END --NOW MOVE ON TO MAKING THE ORG-PATIENT HIERARCHY

TRUNCATE TABLE OrgRenderingProvPatient

INSERT INTO OrgRenderingProvPatient (
   LbPatientId
  ,RenderingProviderNPI
  ,RenderingProviderName
  ,Location
  ,EncounterDate, CPT
  ,EncounterCount
) SELECT LbPatientId, RenderingProviderNPI, RenderingProviderName, Location, EncounterDate AS [EncounterDate], CPT, COUNT(*) 
FROM OrgRenderingEncounter 
GROUP BY LbPatientId, RenderingProviderNPI, RenderingProviderName, Location, EncounterDate, CPT

UPDATE OrgRenderingProvPatient SET RenderingProviderNPI = RenderingProviderName 
WHERE ISNUMERIC(RenderingProviderName) = 1 AND LEN(RenderingProviderName) = 10

UPDATE OrgRenderingProvPatient SET RenderingProviderName = NPI.[NAME] 
FROM OrgRenderingProvPatient ORPP
JOIN NPI_Lookup NPI ON ORPP.RenderingProviderNPI = NPI.NPI
WHERE (ISNUMERIC(RenderingProviderName) = 1 AND LEN(RenderingProviderName) = 10) OR ORPP.RenderingProviderName IS NULL
GO
