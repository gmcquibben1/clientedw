SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalAllergySeverityInsert] 
    @AllergySeverityTypeId INT OUTPUT ,
	@AllergySeverity VARCHAR(255)  = NULL
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @DiagnosisCodeId INT

	IF @AllergySeverity IS NOT NULL 
	BEGIN

		BEGIN TRAN

		SELECT @AllergySeverityTypeId = AllergySeverityTypeId FROM AllergySeverityType
		WHERE Name = @AllergySeverity

		IF @AllergySeverityTypeId IS NULL
		BEGIN
			INSERT INTO [dbo].AllergySeverityType
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
			VALUES 
			(
				@AllergySeverity,
				@AllergySeverity,
				@DeleteInd,
				GetDate(),
				GetDate(),
				@LBUserId,
				@LBUserId
			)
			SELECT @intError = @@ERROR,
				@AllergySeverityTypeId = SCOPE_IDENTITY()
		END

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
		END
		ELSE
		BEGIN
			COMMIT TRAN
		END

	END
		
	RETURN @intError



GO
