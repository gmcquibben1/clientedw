SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwMedications] 
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES FOR NULL COLUMNS
UPDATE PatientMedicationProcessQueue SET Clinician = '' WHERE Clinician IS NULL
UPDATE PatientMedicationProcessQueue SET EncounterId = 0 WHERE EncounterId IS NULL

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId,LbPatientID, [MedicationID],MedicationStartDate --,    EncounterId 
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				--LEN(ISNULL(MedicationInstructions,''))  +  -- THE BIGGER THE MedicationInstructions Text THE BETTER IT IS 
				--ISNULL(TRY_CONVERT(INT,MedicationQuantity),0)  -- THE RECORD WITH HIGHER QUANTITY WINS  
				InterfacePatientMedicationId
				DESC 
			) SNO
	FROM PatientMedicationProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientMedicationProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientMedicationId INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientMedicationId,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientMedicationId,PatientID
FROM 
(	
MERGE [DBO].PatientMedication AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,[MedicationID] ,InterfaceSystemId as SourceSystemID,EncounterID,NULL aS ProviderId ,MedicationStartDate,
		[MedicationEndDate],MedicationQuantity AS Quantity,MedicationRefills As Refills,NULL AS SignCode,MedicationInstructions As Instructions,SampleInd,
		NULL As GenericAllowedInd, 0 AS DeleteInd,MedicationComment as Comment,GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId,
		MedicationName, MedicationForm, MedicationRoute, MedicationDose, CASE WHEN MedicationCodeName = 'NDC' THEN MedicationCode END AS [NDCCode], 
		CASE WHEN MedicationCodeName = 'RxNorm' THEN MedicationCode END AS [RxNormCode], StatusDescription, EncounterIdentifier
		FROM [dbo].PatientMedicationProcessQueue PPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1
	  ) AS source 
	  ([OrigPatientId],[PatientID] ,[MedicationID] ,[SourceSystemID],[EncounterID],[ProviderID],[MedicationStartDate]
      ,[MedicationEndDate] ,[Quantity] ,[Refills] ,[SIGCode] ,[Instructions]   ,[SampleInd]
      ,[GenericAllowedInd] ,[DeleteInd] ,[Comment] ,[CreateDateTime]  ,[ModifyDateTime] ,[CreateLBUserId],[ModifyLBUserId]
	  ,[MedicationName], [MedicationForm], [MedicationRoute], [MedicationDose], [NDCCode], [RxNormCode],[StatusDescription], [EncounterIdentifier]
	  )
ON ( target.[SourceSystemID] = source.[SourceSystemID] AND target.PatientID = source.PatientID 
     AND target.[MedicationID] = source.[MedicationID] AND ISNULL(target.MedicationStartDate,'2099-12-31') = ISNULL(source.MedicationStartDate,'2099-12-31')   
	 --and target.EncounterId = source.EncounterId 
    )
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
--	[PatientID] = source.[PatientID],
	[MedicationID] = source.[MedicationID],
	[SourceSystemID] = source.[SourceSystemID],
	[EncounterID] = source.[EncounterID],
	[ProviderID] = source.[ProviderID],
	[MedicationStartDate] = source.[MedicationStartDate],
	[MedicationEndDate] = source.[MedicationEndDate],
	[Quantity] = source.[Quantity],
	[Refills] = source.[Refills],
	[SIGCode] = source.[SIGCode],
	[Instructions] = source.[Instructions],
	[SampleInd] = source.[SampleInd],
	[GenericAllowedInd] = source.[GenericAllowedInd],
	[DeleteInd] = source.[DeleteInd],
	[Comment] = source.[Comment],
	[MedicationNameFromSource] = source.MedicationName,
	[MedicationFormFromSource] = source.MedicationForm,
	[MedicationRouteFromSource] = source.MedicationRoute,
	[MedicationDoseFromSource] = source.MedicationDose,
	[NDCCode] = source.[NDCCode],
	[RxNormCode] = source.[RxNormCode],
	[StatusDescription] = source.[StatusDescription],
	[EncounterIdentifier] = source.[EncounterIdentifier],
	--[CreateDateTime] = source.[CreateDateTime],
	[ModifyDateTime] = source.[ModifyDateTime],
	--[CreateLBUserId] = source.[CreateLBUserId],
	[ModifyLBUserId] = source.[ModifyLBUserId]
WHEN NOT MATCHED THEN
INSERT ([PatientID] ,[MedicationID] ,[SourceSystemID],[EncounterID],[ProviderID],[MedicationStartDate]
      ,[MedicationEndDate] ,[Quantity] ,[Refills] ,[SIGCode] ,[Instructions]   ,[SampleInd]
      ,[GenericAllowedInd] ,[DeleteInd] ,[Comment] ,[CreateDateTime]  ,[ModifyDateTime] ,[CreateLBUserId],[ModifyLBUserId]
	  ,[MedicationNameFromSource] ,[MedicationFormFromSource] ,[MedicationRouteFromSource] ,[MedicationDoseFromSource] ,[NDCCode] ,[RxNormCode]
	  ,[StatusDescription], [EncounterIdentifier]
	   )
VALUES (source.[PatientID] ,source.[MedicationID] ,source.[SourceSystemID],source.[EncounterID],source.[ProviderID],source.[MedicationStartDate]
      ,source.[MedicationEndDate] ,source.[Quantity] ,source.[Refills] ,source.[SIGCode] ,source.[Instructions]   ,source.[SampleInd]
      ,source.[GenericAllowedInd] ,source.[DeleteInd] ,source.[Comment] ,source.[CreateDateTime]  ,source.[ModifyDateTime] ,source.[CreateLBUserId],source.[ModifyLBUserId]
	  ,source.[MedicationName] ,source.[MedicationForm] ,source.[MedicationRoute] ,source.[MedicationDose] ,source.[NDCCode] ,source.[RxNormCode]
	  ,source.[StatusDescription], source.[EncounterIdentifier])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientMedicationId,INSERTED.PatientMedicationId) PatientMedicationId, source.PatientID
) x ;

IF OBJECT_ID('PatientMedication_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientMedication_PICT(PatientMedicationID,PatientId,SwitchDirectionId)
	SELECT RT.PatientMedicationID, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientMedicationProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'Diagnosis',GetDate()
FROM PatientMedicationProcessQueue
WHERE ProcessedInd = 1 

UPDATE A 
SET HoldingReleaseInd = 1 
FROM [dbo].[PatientMedicationProcessQueue_HoldingBay] A 
INNER JOIN PatientMedicationProcessQueue B ON A.[InterfacePatientMedicationId] = B.[InterfacePatientMedicationId]
WHERE B.ProcessedInd = 1 

--INSERT INTO [dbo].[PatientMedicationProcessQueue_HoldingBay]
--([EncounterId],[InterfacePatientMedicationId],[InterfacePatientID]
--,[LbPatientId],[InterfaceSystemId],[MedicationCode]
--,[MedicationCodeName],[MedicationName],[MedicationDose]
--,[MedicationForm],[MedicationFormTypeID],[MedicationRoute]
--,[MedicationRouteTypeID],[MedicationStartDate],[MedicationEndDate]
--,[MedicationInstructions],[MedicationQuantity],[MedicationRefills]
--,[SampleInd],[PrescribedBy],[Clinician]
--,[MedicationComment],[CreateDateTime],[ProcessedInd]
--,[MedicationId],[EncounterId_HoldingOverride],[InterfacePatientMedicationId_HoldingOverride]
--,[InterfacePatientID_HoldingOverride],[LbPatientId_HoldingOverride],[InterfaceSystemId_HoldingOverride]
--,[MedicationCode_HoldingOverride],[MedicationCodeName_HoldingOverride],[MedicationName_HoldingOverride]
--,[MedicationDose_HoldingOverride],[MedicationForm_HoldingOverride],[MedicationFormTypeID_HoldingOverride]
--,[MedicationRoute_HoldingOverride],[MedicationRouteTypeID_HoldingOverride],[MedicationStartDate_HoldingOverride]
--,[MedicationEndDate_HoldingOverride],[MedicationInstructions_HoldingOverride],[MedicationQuantity_HoldingOverride]
--,[MedicationRefills_HoldingOverride],[SampleInd_HoldingOverride],[PrescribedBy_HoldingOverride]
--,[Clinician_HoldingOverride],[MedicationComment_HoldingOverride],[CreateDateTime_HoldingOverride]
--,[ProcessedInd_HoldingOverride],[MedicationId_HoldingOverride],[HoldingStartDate]
--,[HoldingStartReason])
--SELECT
--A.[EncounterId],A.[InterfacePatientMedicationId],A.[InterfacePatientID]
--,A.[LbPatientId],A.[InterfaceSystemId],A.[MedicationCode]
--,A.[MedicationCodeName],A.[MedicationName],A.[MedicationDose]
--,A.[MedicationForm],A.[MedicationFormTypeID],A.[MedicationRoute]
--,A.[MedicationRouteTypeID],A.[MedicationStartDate],A.[MedicationEndDate]
--,A.[MedicationInstructions],A.[MedicationQuantity],A.[MedicationRefills]
--,A.[SampleInd],A.[PrescribedBy],A.[Clinician]
--,A.[MedicationComment],A.[CreateDateTime],A.[ProcessedInd]
--,A.[MedicationId],A.[EncounterId],A.[InterfacePatientMedicationId]
--,A.[InterfacePatientID],A.[LbPatientId],A.[InterfaceSystemId]
--,A.[MedicationCode],A.[MedicationCodeName],A.[MedicationName]
--,A.[MedicationDose],A.[MedicationForm],A.[MedicationFormTypeID]
--,A.[MedicationRoute],A.[MedicationRouteTypeID],A.[MedicationStartDate]
--,A.[MedicationEndDate],A.[MedicationInstructions],A.[MedicationQuantity]
--,A.[MedicationRefills],A.[SampleInd],A.[PrescribedBy]
--,A.[Clinician],A.[MedicationComment],A.[CreateDateTime]
--,A.[ProcessedInd],A.[MedicationId],GetDate()
--,'Duplicacy'
--FROM [dbo].[PatientMedicationProcessQueue] A 
--LEFT JOIN PatientMedicationProcessQueue_HoldingBay B ON A.[InterfacePatientMedicationId] = B.[InterfacePatientMedicationId]
--WHERE ISNULL(A.ProcessedInd,0) <> 1 AND  B.[InterfacePatientMedicationId] IS NULL 

END


GO
