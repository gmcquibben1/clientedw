SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create PROCEDURE [dbo].[internalTargetBuild]
AS

SELECT O.LbPatientId, visitdate, O.Level1Id, O.Level2Id, O.Level3Id, O.Level4Id, O.Level5Id, O.Level6Id, MTT.MeasureTargetTypeId, O.ContractName, TRY_CONVERT(DECIMAL(20,4),000000.000) as [Value] 
INTO #MEASURETARGETTEMP
FROM membersMonth JOIN OrgHierarchy O ON dbo.membersMonth.LbPatientId = O.LbPatientId
  JOIN MeasureTargetType MTT ON MTT.DeleteInd = 0
  --JOIN MeasureTarget MT ON MTT.MeasureTargetTypeId = MT.MeasureTargetTypeId and MT.DeleteInd = 0

  
--UPDATE DEFAULT MEASURE TARGET VALUES BASED ON DEFAULT DATE SPANS
  UPDATE #MEASURETARGETTEMP SET [Value] = MT.[Value]
  FROM #MEASURETARGETTEMP T 
    JOIN MeasureTarget MT 
    ON T.MeasureTargetTypeId = MT.MeasureTargetTypeId
      and T.visitdate >= MT.StartDate and T.visitdate <= MT.EndDate
      AND MT.LevelId = 'Default' AND MT.DeleteInd = 0
    JOIN MeasureTargetContract MTC ON MTC.MeasureTargetId = MT.MeasureTargetId AND MTC.DeleteInd = 0
    JOIN CMeasure_ContractNames CCN ON MTC.ContractId = CCN.ContractId and T.ContractName = CCN.ContractName
  WHERE MT.DeleteInd = 0
 
 --UPDATE targets based on level1 ID
  UPDATE #MEASURETARGETTEMP SET [Value] = MT.[Value]
  FROM #MEASURETARGETTEMP T 
    JOIN MeasureTarget MT 
    ON T.MeasureTargetTypeId = MT.MeasureTargetTypeId
    and T.visitdate >= MT.StartDate and T.visitdate <= MT.EndDate
    AND MT.LevelId = T.Level1Id AND MT.DeleteInd = 0
    JOIN MeasureTargetContract MTC ON MTC.MeasureTargetId = MT.MeasureTargetId AND MTC.DeleteInd = 0
    JOIN CMeasure_ContractNames CCN ON MTC.ContractId = CCN.ContractId and T.ContractName = CCN.ContractName
 WHERE MT.DeleteInd = 0
 
 --UPDATE targets based on level2 ID
  UPDATE #MEASURETARGETTEMP SET [Value] = MT.[Value]
  FROM #MEASURETARGETTEMP T 
    JOIN MeasureTarget MT 
    ON T.MeasureTargetTypeId = MT.MeasureTargetTypeId
    and T.visitdate >= MT.StartDate and T.visitdate <= MT.EndDate
    AND MT.LevelId = T.Level2Id AND MT.DeleteInd = 0
    JOIN MeasureTargetContract MTC ON MTC.MeasureTargetId = MT.MeasureTargetId AND MTC.DeleteInd = 0
    JOIN CMeasure_ContractNames CCN ON MTC.ContractId = CCN.ContractId and T.ContractName = CCN.ContractName
 
 --UPDATE targets based on level3 ID
  UPDATE #MEASURETARGETTEMP SET [Value] = MT.[Value]
  FROM #MEASURETARGETTEMP T 
    JOIN MeasureTarget MT 
    ON T.MeasureTargetTypeId = MT.MeasureTargetTypeId
    and T.visitdate >= MT.StartDate and T.visitdate <= MT.EndDate
    AND MT.LevelId = T.Level3Id AND MT.DeleteInd = 0
    JOIN MeasureTargetContract MTC ON MTC.MeasureTargetId = MT.MeasureTargetId AND MTC.DeleteInd = 0
    JOIN CMeasure_ContractNames CCN ON MTC.ContractId = CCN.ContractId and T.ContractName = CCN.ContractName
    WHERE MT.DeleteInd = 0

 --UPDATE targets based on level4 ID
  UPDATE #MEASURETARGETTEMP SET [Value] = MT.[Value]
  FROM #MEASURETARGETTEMP T 
    JOIN MeasureTarget MT 
    ON T.MeasureTargetTypeId = MT.MeasureTargetTypeId
    and T.visitdate >= MT.StartDate and T.visitdate <= MT.EndDate
    AND MT.LevelId = T.Level4Id AND MT.DeleteInd = 0
    JOIN MeasureTargetContract MTC ON MTC.MeasureTargetId = MT.MeasureTargetId AND MTC.DeleteInd = 0
    JOIN CMeasure_ContractNames CCN ON MTC.ContractId = CCN.ContractId and T.ContractName = CCN.ContractName
    WHERE MT.DeleteInd = 0
 
 --UPDATE targets based on level5 ID
  UPDATE #MEASURETARGETTEMP SET [Value] = MT.[Value]
  FROM #MEASURETARGETTEMP T 
    JOIN MeasureTarget MT 
    ON T.MeasureTargetTypeId = MT.MeasureTargetTypeId
    and T.visitdate >= MT.StartDate and T.visitdate <= MT.EndDate
    AND MT.LevelId = T.Level5Id AND MT.DeleteInd = 0
    JOIN MeasureTargetContract MTC ON MTC.MeasureTargetId = MT.MeasureTargetId AND MTC.DeleteInd = 0
    JOIN CMeasure_ContractNames CCN ON MTC.ContractId = CCN.ContractId and T.ContractName = CCN.ContractName
    WHERE MT.DeleteInd = 0
 
 --UPDATE targets based on level6 ID
  UPDATE #MEASURETARGETTEMP SET [Value] = MT.[Value]
  FROM #MEASURETARGETTEMP T 
    JOIN MeasureTarget MT 
    ON T.MeasureTargetTypeId = MT.MeasureTargetTypeId
    and T.visitdate >= MT.StartDate and T.visitdate <= MT.EndDate
    AND MT.LevelId = T.Level6Id AND MT.DeleteInd = 0   
    JOIN MeasureTargetContract MTC ON MTC.MeasureTargetId = MT.MeasureTargetId AND MTC.DeleteInd = 0
    JOIN CMeasure_ContractNames CCN ON MTC.ContractId = CCN.ContractId and T.ContractName = CCN.ContractName
    WHERE MT.DeleteInd = 0
  
    --insert into memmonth metrics
    INSERT INTO MemMonth_Metrics (
        LbPatientId
      ,DOS_First
      ,Title
      ,[Value]
    ) select distinct LbPatientId, visitdate,MT.[Description], MTT.[Value] 
    FROM #MEASURETARGETTEMP MTT
    JOIN MeasureTargetType MT on MTT.MeasureTargetTypeId = MT.MeasureTargetTypeId
 --drop temp table
 drop table #MEASURETARGETTEMP
GO
