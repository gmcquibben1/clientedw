SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ClinicalDataTypeSave]
	@ClinicalDataTypeId INT OUTPUT, 
	@Name VARCHAR(250), 
	@DisplayValue VARCHAR(1000) ,   --Stored procedure or REST service end point for the drop down list values
	@DeleteInd BIT,
	@UserId INT, 
	@SkipUpdate BIT = 1 --Option to skip running the update statement IF the record already exists
AS
/*===============================================================
	CREATED BY: 	Christopher Lutz
	CREATED ON:		2017-02-15
	INITIAL VER:	2.2.1
	MODULE:			ETL Data Load
	DESCRIPTION:	This stored procedure will save a Clinical Data type to the database
	PARAMETERS:		
	  		
			@ClinicalDataTypeId				Id of the event type, if this is a new event type it will create a brand new one and return it in this fields
			@Name								Internal Name of this event type
			@DisplayValue						Client visible name of this type
			@DeleteInd							Is this record to be flagged as deleted
			@UserId								Id of the user making the change
			@SkipUpdate							Option to skip running the update statement IF the record already exists
		
	RETURN VALUE(s)/OUTPUT:	Id of the newly created record will be returned in @ClinicalDataTypeId
	
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2017-02-15		CJL			LBETL-865	 Initial
	 
=================================================================*/


	DECLARE @id INT
	DECLARE @tmpName VARCHAR(250) = RTRIM(LTRIM(REPLACE(@Name, CHAR(160),' ')))	--remove the leading, trailing and non-breaking space characters
	DECLARE @tmpDisplayValue VARCHAR(1000) = RTRIM(LTRIM(REPLACE(@DisplayValue, CHAR(160),' ')))




	SET @id = ( SELECT ClinicalDataTypeId FROM ClinicalDataType WITH (NOLOCK) WHERE  Name = @tmpName ) 

	IF  @id IS NULL
	BEGIN


		
		-- type of this table's column must match the type of the
		-- identity column of the table you'll be inserting into
		DECLARE @IdentityOutput table ( ID INT )

			
		INSERT INTO ClinicalDataType
			( Name, DisplayValue, 
			DeleteInd, CreateDateTime, ModifyDateTime, CreateLBUserId, ModifyLBUserId)
			OUTPUT inserted.ClinicalDataTypeId INTO @IdentityOutput
		values
			( @tmpName, @tmpDisplayValue, 
			@DeleteInd, SYSUTCDATETIME(), SYSUTCDATETIME(), @UserId, @UserId )


		SELECT @ClinicalDataTypeId = (SELECT ID FROM @IdentityOutput)
	END
	ELSE
	BEGIN

		IF @SkipUpdate <> 1
		BEGIN
		
			UPDATE ClinicalDataType
			SET
				DeleteInd = @DeleteInd, ModifyDateTime = SYSUTCDATETIME(), ModifyLBUserId = @UserId, 
			  DisplayValue = @tmpDisplayValue,
				Name =@tmpName
			WHERE
				ClinicalDataTypeId = @id
		END
	END


GO
