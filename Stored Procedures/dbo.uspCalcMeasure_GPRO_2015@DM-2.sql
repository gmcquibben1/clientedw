SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@DM-2] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS')    IS NOT NULL DROP TABLE [#RANKEDPATIENTS];
	IF OBJECT_ID('tempdb..#DIABETIC_PATIENTS') IS NOT NULL DROP TABLE [#DIABETIC_PATIENTS];
	IF OBJECT_ID('tempdb..#DiabVisit')         IS NOT NULL DROP TABLE [#DiabVisit];
	IF OBJECT_ID('tempdb..#TestResults')       IS NOT NULL DROP TABLE [#TestResults];

   
	DECLARE  @yearend datetime, @measureBeginDate datetime;

	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2014';

	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	--Initial Patient pooling, for patients that have ranking for the BCS measure. 
	SELECT r.LBPatientID,
        r.GProPatientRankingId,
		dateDiff(year,r.Birthdate,dateAdd(year,-1,@yearend)) as Age,
		r.Birthdate,
        GM.GproMeasureTypeID,
		GPM.DMHBA1CDATE,
        GPM.DmHba1cTest,
        GPM.DmHba1cValue, 
        GPM.DmConfirmed 
	INTO   #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE Name = 'DM-2' 
		ANd gpm.DmRank IS NOT NULL
		AND (GPM.DmConfirmed IS NULL OR GPM.DmHba1cTest IS NULL OR GPM.DmHba1cDate IS NULL OR GPM.DmHba1cValue IS NULL)
   
	--Denominator Exceptions
		
  
    --Age Check
    UPDATE rp
    SET    rp.[DmConfirmed] = '19'
    FROM   #RankedPatients rp
    WHERE rp.DmConfirmed IS NULL
		AND (Age > 75 OR Age < 18) 
   
    
	--------------------------------------------------------------------------------------------------------    
    -- Diabetes Diagnosis Requirement
	-------------------------------------------------------------------------------------------------------- 
    SELECT DISTINCT 
		STAGE.LbPatientId,
        pd.[DiagnosisDateTime] AS DiabDate
    INTO #DIABETIC_PATIENTS       --Drop Table  #DIABETIC_PATIENTS        
    FROM #RankedPatients STAGE
		INNER JOIN vwPatientDiagnosis pd ON pd.PatientId = STAGE.LbPatientId
			AND pd.DiagnosisDateTime >= DATEADD(year, -2, @yearend)
			AND pd.DiagnosisDateTime < @yearend
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND ModuleType = 'DM'
			AND VariableName = 'DX_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pd.SourceSystemId
 
	--------------------------------------------------------------------------------------------------------    
    ---Updating Patients whom did not have Diabetes.
	--------------------------------------------------------------------------------------------------------
    
	--     UPDATE #RankedPatients
	--     SET    DmConfirmed = '8'
	--     FROM   #RankedPatients RP
	--     LEFT JOIN #DIABETIC_PATIENTS DP
	--        ON RP.LBPATIENTID = DP.LBPATIENTID
	--     WHERE DP.LBPATIENTID IS NULL
	--        --AND RP.DmConfirmed is null
	--        --RP.GProPatientRankingId = DP.GProPatientRankingId
           
	--------------------------------------------------------------------------------------------------------    
		-- Annual Encounter Requirement
	--------------------------------------------------------------------------------------------------------
    
	--    SELECT DISTINCT LBPATIENTID,
	--                    MAX(PP.ProcedureDateTime) as LastVisit
	--    INTO   #DiabVisit --Drop Table #diabVisit
	--    FROM   #DIABETIC_PATIENTS DP
	--    JOIN   DBO.PATIENTPROCEDURE PP
	--        ON DP.LBPATIENTID = PP.PatientID
	--    JOIN   DBO.PatientProcedureProcedureCode PPPC
	--        ON PP.PatientProcedureId = PPPC.PatientProcedureId
	--    JOIN   DBO.ProcedureCode PC
	--        ON PPPC.ProcedureCodeId = PC.ProcedureCodeId
	--    JOIN   [DBO].[GproEvaluationCode] GEC
	--        ON  PC.ProcedureCode = GEC.Code
	--    where [ModuleType] = 'DM'                      AND 
	--          [VariableName] = N'ENCOUNTER_CODE'       AND
	--          PP.PROCEDUREDATETIME >= DateAdd( YEAR, -1, @yearend )      AND
	--          PP.PROCEDUREDATETIME <  @yearend      
	--    GROUP BY LBPATIENTID
    
	--------------------------------------------------------------------------------------------------------    
		-- Annual Encounter Requirement Update.
	--------------------------------------------------------------------------------------------------------
     
	--     UPDATE #RankedPatients
	--     SET    DmConfirmed = '8'
	--     FROM   #RankedPatients RP
	--     LEFT JOIN #DiabVisit DV
	--        ON RP.LBPATIENTID = DV.LBPATIENTID
	--     WHERE DV.LBPATIENTID IS NULL 

    -- Setting the patients verified with Diabetes as Confirmed.
     UPDATE RP
     SET RP.DmConfirmed = '2'
     FROM #RankedPatients RP
		JOIN #DIABETIC_PATIENTS DP ON RP.LbPatientId = DP.LbPatientId
   
	--------------------------------------------------------------------------------------------------------    
    -- Numerator Checks
	--------------------------------------------------------------------------------------------------------    

	SELECT DISTINCT  
		plo.[PatientID] AS [PatientID],
        CONVERT(VARCHAR(10), CAST(plr.[ObservationDate] AS DATE), 101) AS DMHBA1cDate,
        2 AS DMHBA1cTest,
        IsNull( Try_Convert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 )  AS DMHBa1cValue
	INTO #TestResults
	FROM #DIABETIC_PATIENTS  stage
		JOIN [dbo].[PatientLabOrder] plo ON  plo.[PatientID] = stage.[LbPatientID]
		JOIN [dbo].[PatientLabResult] plr ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
		JOIN (SELECT MAX(ObservationDate) as LatestTest, 
				PatientId
			  FROM #DIABETIC_PATIENTS  stage
				JOIN [dbo].[PatientLabOrder] plo ON plo.[PatientID] = stage.[LbPatientID]
				JOIN [dbo].[PatientLabResult] plr ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]               
					AND plr.[ObservationDate] >= DateAdd( YEAR, -1, @yearend )  --@MeasurePeriodBegin
		            AND plr.[ObservationDate] < @yearend --@MeasurePeriodEnd
					AND (plr.[Units] IS NULL OR plr.[Units] LIKE '%[%]%')
				JOIN [DBO].[GproEvaluationCode] GEC ON (GEC.[Code] = plr.[ObservationCode1] OR GEC.[Code] = plr.[ObservationCode2])
		            AND [ModuleType] = 'DM'  
					AND [VariableName] = 'A1C_CODE'
				JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID]    --PROCS
			  GROUP BY PATIENTID
			) MaxDate ON  plo.PatientId = Maxdate.PatientID
			AND plr.ObservationDate = MaxDate.LatestTest
		JOIN [DBO].[GproEvaluationCode] GEC ON  GEC.[Code] = plr.[ObservationCode1]
		    AND [ModuleType] = 'DM'  
            AND [VariableName] = 'A1C_CODE'
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID]    --PROCS
                

	UPDATE RP
	SET RP.DmHba1cDate = TR.DMHBA1CDATE, 
		RP.DmHba1cTest = TR.DmHba1cTest , 
		RP.DmHba1cValue = TR.DmHba1cValue
	FROM #RankedPatients RP
		JOIN #TestResults TR ON RP.LBPatientID = TR.PatientID
	WHERE TR.DmHba1cValue < 100;

	--------------------------------------------------------------------------------------------------------    
	--
	--select distinct *
	--------------------------------------------------------------------------------------------------------          
	   -- Updating the required tables
	--from #rankedPatients

	--updating measure table with patients who had been tested
	--UPDATE gpm
	--SET    GPM.DmConfirmed = GPR.DmConfirmed, gpm.DmHba1cTest = GPR.DmHba1cTest, gpm.DmHba1cDate = gpr.DmHba1cDate , GPM.DmHba1cValue = GPR.DmHba1cValue , gpm.ModifyDateTime = GETUTCDATE()
	--FROM   GproPatientMeasure gpm
	--	   JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	--WHERE GPR.DmConfirmed = 2

	UPDATE gpm
	SET gpm.DmConfirmed = gpr.DmConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.DmRank IS NOT NULL
		AND gpm.DmConfirmed IS NULL

	UPDATE gpm
	SET gpm.DmHba1cTest = GPR.DmHba1cTest, gpm.DmHba1cDate = gpr.DmHba1cDate , GPM.DmHba1cValue = GPR.DmHba1cValue, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.DmRank IS NOT NULL
		AND (gpm.DmHba1cTest IS NULL 
		OR gpm.DmHba1cDate IS NULL
		OR gpm.DmHba1cValue IS NULL)


END

     
GO
