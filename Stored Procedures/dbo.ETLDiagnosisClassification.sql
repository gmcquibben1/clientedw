SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-03-23
-- Description:	Load DiagnosisClassification Data into EDW
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[ETLDiagnosisClassification]

AS
BEGIN	
		INSERT INTO [dbo].DiagnosisClassificationType
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
        SELECT DISTINCT
		t1.[DiagnosisClassification],
		t1.[DiagnosisClassification],
		0,
		GETUTCDATE(),
		GETUTCDATE(),
		1,
		1
        FROM PatientDiagnosisProcessQueue t1
		LEFT JOIN [dbo].[DiagnosisClassificationType] t2
		ON LTRIM(RTRIM(t1.[DiagnosisClassification])) = LTRIM(RTRIM(t2.[Name]))
		WHERE t1.[DiagnosisClassification] IS NOT NULL AND t2.[Name] IS NULL
END
GO
