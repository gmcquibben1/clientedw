SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalMkCclfClmSummaryACO]
AS
BEGIN

SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'CCLF_CLM_SUMMARY_ACO';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalMkCclfClmSummaryACO';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();





BEGIN TRY 

TRUNCATE TABLE CCLF_CLM_SUMMARY_ACO

INSERT INTO CCLF_CLM_SUMMARY_ACO (
   ACO_ID
  ,CLAIM_FEED
  ,DOS_First
  ,PAID
  ,MEMB_MONTH
)
SELECT O.Level1Id AS [ACO_ID] , CLAIM_FEED, DOS_First, SUM(PAID) AS PAID, COUNT(DISTINCT(C.LbPatientId)) AS MM
FROM CCLF_CLM_SUMMARY C
--JOIN ACO_Hierarchy ON 
-- dbo.CCLF_CLM_SUMMARY.LbPatientId = 
-- dbo.ACO_Hierarchy.LbPatientId
-- GROUP BY ACO_ID, CLAIM_FEED, DOS_First
JOIN OrgHierarchy O ON
C.LbPatientId = O.LbPatientId
GROUP BY O.Level1Id, CLAIM_FEED, DOS_First

set @InsertedRecord=@@rowcount;
		
 UPDATE CCLF_CLM_SUMMARY_ACO SET
  PAID_PERMM = PAID/MEMB_MONTH -- decimal(8, 0)


	-----Log Information
		
	 
          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
