SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproMeasurePatientDistributionLoad]
/*===============================================================
	CREATED BY: 	William Keen
	CREATED ON:		2016-12-19
	INITIAL VER:	2.2.1
	MODULE:			GPro	
	DESCRIPTION:	Returns data used by the Patient Distribution report
	PARAMETERS: 
		@BusinessUnitId - The parent business unit id
		@MeasureTypeName - The name of the measure to filter by
		@MeasureYear - The year for which to filter data
	PROGRAMMING NOTES: 	

	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-12-19		WK						Initial Version
	2.2.1		2017-02-09		WK						Fixing a potential divide by zero error
	2.2.2		2017-03-23		WK						Filtering out BUs with no patients

=================================================================*/
(
	  @BusinessUnitId INT = NULL
	, @MeasureTypeName VARCHAR(50) = NULL
	, @MeasureYear INT = NULL
)
AS 
BEGIN
	CREATE TABLE #TempTable (
		RowNumber		INT IDENTITY PRIMARY KEY	NOT NULL, 
		Name			VARCHAR(255)				NULL, 
		BusinessUnitId	INT							NULL
	)

	INSERT INTO #TempTable (Name, BusinessUnitId) 
		SELECT Name, BusinessUnitId 
		FROM vwPortal_BusinessUnit 
		WHERE 
			ParentBusinessUnitId = @BusinessUnitId 
			AND DeleteInd = 0 
		GROUP BY Name, BusinessUnitId ORDER BY [Name]

	DECLARE @TotalPatients FLOAT 
	DECLARE @MeasureYearHolder INT = @MeasureYear
	IF @MeasureTypeName IS NULL
	BEGIN
		SET @TotalPatients = (SELECT SUM(TotalPatients) FROM GProMeasureProgressHistory WHERE BusinessUnitId = @BusinessUnitId)
		SELECT gpmph.BusinessUnitName, 'ALL' AS MeasureTypeName, 'ALL' AS MeasureTypeDescription, 
		CASE WHEN @TotalPatients = 0 THEN 0 ELSE (SUM(gpmph.TotalPatients) / @TotalPatients * 100.0) END AS PatientPercentage
		FROM GProMeasureProgressHistory gpmph
		JOIN #TempTable t
		ON t.BusinessUnitId = gpmph.BusinessUnitId
		WHERE gpmph.MeasureYear = @MeasureYearHolder AND gpmph.TotalPatients > 0
		GROUP BY gpmph.BusinessUnitName
	END
	ELSE
	BEGIN
		SET @TotalPatients = (SELECT TotalPatients FROM GProMeasureProgressHistory WHERE BusinessUnitId = @BusinessUnitId AND MeasureTypeName = @MeasureTypeName)
		SELECT gpmph.BusinessUnitName, gpmph.MeasureTypeName, gpmph.MeasureTypeDescription, 
		CASE WHEN @TotalPatients = 0 THEN 0 ELSE (SUM(gpmph.TotalPatients) / @TotalPatients * 100.0) END AS PatientPercentage
		FROM GProMeasureProgressHistory gpmph
		JOIN #TempTable t
		ON t.BusinessUnitId = gpmph.BusinessUnitId 
		WHERE 
			gpmph.MeasureYear = @MeasureYearHolder 
			AND gpmph.MeasureTypeName = @MeasureTypeName
			AND gpmph.TotalPatients > 0
		GROUP BY gpmph.BusinessUnitName , gpmph.MeasureTypeName, gpmph.MeasureTypeDescription
	END
END
GO
