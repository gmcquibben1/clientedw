SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalOrgHierarchyMaintenance] 
AS
BEGIN

DECLARE @OHTVP1 AS OrgHierarchyUnitProcessType;
INSERT @OHTVP1
EXEC internalComposeCustomResultset @RequestSubject = 'OrgHierarhy', @Param1 = 1

/********************************************
	-- EXISTING PATIENTS -- 
********************************************/
--UPDATE OrgHierarchy  SET 
--State = OP.State, City = OP.City , Zip = OP.Zip,
--Level1Id = OP.Level1Id, Level1Name = OP.Level1Name, Level1TypeName = OP.Level1TypeName,
--Level2Id = OP.Level2Id, Level2Name = OP.Level2Name, Level2TypeName = OP.Level2TypeName,
--Level3Id = OP.Level3Id, Level3Name = OP.Level3Name, Level3TypeName = OP.Level3TypeName,
--Level4Id = OP.Level4Id, Level4Name = OP.Level4Name, Level4TypeName = OP.Level4TypeName,
--Level5Id = OP.Level5Id, Level5Name = OP.Level5Name, Level5TypeName = OP.Level5TypeName,
--Level6Id = OP.Level6Id, Level6Name = OP.Level6Name, Level6TypeName = OP.Level6TypeName,
--ContractName = Me.Source, MemberID = Me.Id,MemberFullName = LTRIM(RTRIM(ME.lastName)) + ', ' + LTRIM(RTRIM(me.firstName)),
--ProviderId = Me.ProviderId,ProviderName = ISNULL(LTRIM(RTRIM(pe.ProviderLastName)) + ', ' + LTRIM(RTRIM(pe.ProviderFirstName)),'Unattributed'),ProviderNPI= Me.ProviderId
--FROM OrgHierarchy OH 
--INNER JOIN MemberExtract ME ON ME.medicareNo = OH.BENE_HIC_NUM 
--LEFT JOIN ProviderExtract PE ON Me.ProviderId = PE.ProviderNPI
--INNER JOIN @OHTVP1 OP ON ME.medicareNo = Op.BENE_HIC_NUM
--WHERE me.Holdback IS NULL 

SELECT BENE_HIC_NUM,LastUpdatedTimestamp -- CACHE THE DATES BEFORE TRUNCATE
INTO #CacheDates
FROM OrgHierarchy

TRUNCATE TABLE OrgHierarchy

/********************************************
	-- NEW PATIENTS -- 
********************************************/

INSERT INTO OrgHierarchy (
State, City, Zip, 
Level1Id,Level1Name,Level1TypeName,Level2Id,Level2Name,Level2TypeName,
Level3Id,Level3Name,Level3TypeName,Level4Id,Level4Name,Level4TypeName,
Level5Id,Level5Name,Level5TypeName,Level6Id,Level6Name,Level6TypeName,
ContractName,MemberID,MemberFullName,Member_Provider_Relationship_Effective_Date,Member_Provider_Relationship_Termination_Date,BENE_HIC_NUM,
Patient_Status,ProviderId,ProviderName,ProviderNPI,BirthDate,GenderId,LastUpdatedTimestamp 
)
SELECT DISTINCT OP.State, OP.City , OP.Zip,
	   OP.Level1Id,OP.Level1Name,OP.Level1TypeName,OP.Level2Id,OP.Level2Name,OP.Level2TypeName,
	   OP.Level3Id,OP.Level3Name,OP.Level3TypeName,OP.Level4Id,OP.Level4Name,OP.Level4TypeName,
	   OP.Level5Id,OP.Level5Name,OP.Level5TypeName,OP.Level6Id,OP.Level6Name,OP.Level6TypeName,
	   Me.Source,Me.Id,LTRIM(RTRIM(ME.lastName)) + ', ' + LTRIM(RTRIM(me.firstName)),NULL,NULL,Me.medicareNo,
'Active (WithClaims)',Me.ProviderId,'Unattributed',Me.ProviderId, Me.BirthDate, Me.Sex, GetDate()
FROM MemberExtract ME 
INNER JOIN @OHTVP1 OP ON ME.medicareNo = Op.BENE_HIC_NUM
LEFT JOIN OrgHierarchy OH ON ME.medicareNo = OH.BENE_HIC_NUM 
WHERE OH.BENE_HIC_NUM IS NULL  and ISNULL(me.Holdback,0) = 0 

UPDATE OrgHierarchy 
SET ProviderName = ISNULL(LTRIM(RTRIM(pe.ProviderLastName)) + ', ' + LTRIM(RTRIM(pe.ProviderFirstName)),'Unattributed')
FROM OrgHierarchy ME LEFT JOIN ProviderExtract PE ON Me.ProviderNPI = PE.ProviderNPI

UPDATE OrgHierarchy -- REAPPLY PREVIOUS DATES IF APPLICABLE, I.E EXISTING RECORDS
SET LastUpdatedTimestamp = CD.LastUpdatedTimestamp 
FROM OrgHierarchy ME 
INNER JOIN  #CacheDates CD ON ME.BENE_HIC_NUM = CD.BENE_HIC_NUM

UPDATE OrgHierarchy 
SET LbPatientId = PIR.LbPatientid 
FROM OrgHierarchy OH INNER JOIN PatientIdReference PIR ON OH.BENE_HIC_NUM = PIR.ExternalId

/********************************************
	-- PATIENT STATUS UPDATES -- 
	-- FOR MSSP PATIENTS - WE GET TO KNOW THIER DECEASED STATUS AND IF WE HAVE THIER MRA - THIER MRA STATUS
	-- PATIENT STATUS USUALLY DEFAULTS TO ACTIVE ON FIRST ENTRY AND ADJUSTS AS BELOW TO SOMETHING OTHER THAN ACTIVE IF DATA RECORDS THUS
********************************************/

CREATE TABLE #TEMP (BENE_HIC_NUM VARCHAR(100), StatusDesc VARCHAR(40), DateSince DATETIME)

CREATE TABLE #TCLAIMS (BENE_HIC_NUM VARCHAR(100), CLAIMS INT)

INSERT INTO #TCLAIMS (BENE_HIC_NUM, CLAIMS)
SELECT BENE_HIC_NUM , SUM(CLAIMS) FROM
(	SELECT BENE_HIC_NUM, COUNT(1) CLAIMS FROM CCLF_1_PartA_Header GROUP BY BENE_HIC_NUM 
	UNION 
	SELECT BENE_HIC_NUM, COUNT(1) CLAIMS FROM CCLF_5_PartB_Physicians GROUP BY BENE_HIC_NUM 
) X
GROUP BY BENE_HIC_NUM

INSERT INTO #TEMP (BENE_HIC_NUM,StatusDesc,DateSince)
SELECT BENE_HIC_NUM, 'Deceased' StatusDesc, MIN(BENE_DEATH_DT) DateSince
FROM CCLF_8_BeneDemo
WHERE TRY_CONVERT(DATETIME,[BENE_DEATH_DT]) >= '2000-01-01'  
GROUP BY BENE_HIC_NUM

INSERT INTO #TEMP (BENE_HIC_NUM,StatusDesc,DateSince)
SELECT Me.medicareNo, 'Deceased' StatusDesc, MIN(DeathDate) DateSince
FROM MemberExtract ME
LEFT JOIN #TEMP T ON Me.medicareNo = T.BENE_HIC_NUM 
WHERE TRY_CONVERT(DATETIME,[DeathDate]) >= '2000-01-01' AND T.BENE_HIC_NUM IS NULL
GROUP BY Me.medicareNo

INSERT INTO #TEMP (BENE_HIC_NUM,StatusDesc,DateSince)
SELECT HICNO , 'Deceased' StatusDesc, MIN(TimeLine) DateSince
FROM MemberRosterArchives MRA 
LEFT JOIN #TEMP T ON MRA.HICNO = T.BENE_HIC_NUM 
WHERE ( [Deceased Beneficiary Flag2] = '1' OR TRY_CONVERT(DATETIME,[Deceased Beneficiary Flag2]) >= '2000-01-01') AND T.BENE_HIC_NUM IS NULL 
GROUP BY HICNO

INSERT INTO #TEMP (BENE_HIC_NUM,StatusDesc)
SELECT HICNO , 
	   SUBSTRING(X.StatusDesc,CHARINDEX('_',X.StatusDesc)+1,100) StatusDesc 
FROM (
	SELECT HICNO ,
		   MIN(REPLICATE('a',CAST(timelineid AS INT)) + '_' + 
			   CASE WHEN StatusDesc in ('NEW','CONTINUING','RETURNING') THEN 'Active' ELSE 'Inactive' END) 
		   StatusDesc 
	FROM MemberRosterArchives
	GROUP BY HICNO 
) X 
LEFT JOIN #TEMP T ON X.HICNO = T.BENE_HIC_NUM 
WHERE T.BENE_HIC_NUM IS NULL 

UPDATE #TEMP
SET StatusDesc = CASE WHEN OH.StatusDesc = 'Deceased' AND T.CLAIMS > 0 THEN 'Deceased (Active w/Claims)'
					  WHEN OH.StatusDesc = 'Deceased' AND ISNULL(T.CLAIMS,0) = 0 THEN 'Deceased (Inactive)'
					  WHEN OH.StatusDesc = 'Active' AND ISNULL(T.CLAIMS,0)> 0 THEN 'Active (WithClaims)'
					  WHEN OH.StatusDesc = 'Active' AND ISNULL(T.CLAIMS,0) = 0 THEN 'Active (NoClaims)'
					  WHEN OH.StatusDesc = 'Inactive' AND ISNULL(T.CLAIMS,0) > 0 THEN 'Inactive (WithClaims)'
					  WHEN OH.StatusDesc = 'Inactive' AND ISNULL(T.CLAIMS,0) = 0 THEN 'Inactive (NoClaims)'
				 END 
FROM #TEMP OH 
LEFT JOIN #TCLAIMS T ON OH.BENE_HIC_NUM = T.BENE_HIC_NUM

UPDATE #TEMP
SET StatusDesc = 'Deceased (Inactive)'
FROM #TEMP OH 
LEFT JOIN MemberRosterArchives T ON OH.BENE_HIC_NUM = T.HICNO  AND T.TimelineID = 1
WHERE OH.StatusDesc = 'Deceased (Active w/Claims)' AND T.HICNO IS NULL 

UPDATE OrgHierarchy
SET Patient_Status = T.StatusDesc 
FROM OrgHierarchy OH 
INNER JOIN #TEMP T ON OH.BENE_HIC_NUM = T.BENE_HIC_NUM

-- THE INACTIVE CATCHALL RULE FOR MSSP PATIENTS 
UPDATE OrgHierarchy
SET Patient_Status = 'Inactive (WithClaims)'
FROM OrgHierarchy OH 
LEFT JOIN #TEMP T ON OH.BENE_HIC_NUM = T.BENE_HIC_NUM 
INNER JOIN #TCLAIMS TC ON OH.BENE_HIC_NUM = TC.BENE_HIC_NUM
WHERE T.BENE_HIC_NUM IS NULL AND OH.ContractName = 'MSSP'

UPDATE OrgHierarchy
SET Patient_Status = 'Inactive (NoClaims)'
FROM OrgHierarchy OH 
LEFT JOIN #TEMP T ON OH.BENE_HIC_NUM = T.BENE_HIC_NUM 
LEFT JOIN #TCLAIMS TC ON OH.BENE_HIC_NUM = TC.BENE_HIC_NUM
WHERE T.BENE_HIC_NUM IS NULL AND TC.BENE_HIC_NUM IS NULL AND OH.ContractName = 'MSSP'

/********************************************
	-- MEMBEREXTRACT UPDATE FOR TermDate -- 
********************************************/

-- EARLIEST OF WHEN DECEASED UPDATE BACK MEMBEREXTRACT
UPDATE MemberExtract 
Set CurrTermDate = T.DateSince
FROM MemberExtract Me
INNER JOIN #TEMP T On Me.medicareNo = T.BENE_HIC_NUM
WHERE T.StatusDesc IN ('Deceased (Active w/Claims)','Deceased (Inactive)')

UPDATE OrgHierarchy 
SET LbPatientId = PIR.LbPatientid 
FROM OrgHierarchy OH INNER JOIN PatientIdReference PIR ON OH.BENE_HIC_NUM = PIR.ExternalId  AND Pir.PrimaryId = 1 WHERE  OH.LbPatientId IS NULL 

UPDATE OrgHierarchy 
SET LbPatientId = PIR.LbPatientid 
FROM OrgHierarchy OH INNER JOIN PatientIdReference PIR ON OH.BENE_HIC_NUM = PIR.ExternalId  AND PIR.IdTypeDesc = 'Bene_Hic_Num' WHERE OH.LbPatientId IS NULL 

-- CUSTOM PATIENT STATUS IF ANY 
CREATE TABLE #CustomPatientStatus (MedicareNO VARCHAR(100),Patient_Status VARCHAR(100))
INSERT INTO #CustomPatientStatus (MedicareNO,Patient_Status)
EXEC internalComposeCustomResultset @RequestSubject = 'CustomPatientStatus', @Param1 = 2

UPDATE OrgHierarchy 
SET Patient_Status = CPS.Patient_Status
FROM OrgHierarchy OH INNER JOIN #CustomPatientStatus CPS ON OH.BENE_HIC_NUM = CPS.MedicareNO  

END
GO
