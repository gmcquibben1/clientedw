SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproMeasureSummaryColumnNameLoad]
/*===============================================================
	CREATED BY: 	William Keen
	CREATED ON:		2016-12-19
	INITIAL VER:	2.2.1
	MODULE:			GPro	
	DESCRIPTION:	Returns data used by the Benchmark Summary report
	PARAMETERS: 
		@BusinessUnitId - The parent business unit id.
	PROGRAMMING NOTES: 	

	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-12-19		WK						Initial Version
	2.2.2		2017-03-23		WK						Filter out business units with no patients
	2.2.2		2017-04-01		WK			LBPP-2467	Adding all parent business units to results
	2.2.2		2017-05-04		WK			LBPP-2467	Updating the join/pivot to use BusinessUnitId instead of BusinessUnitName
	2.2.2		2017-05-10		WK			LBPP-2467	Updating the proc to return the header color

=================================================================*/
	@BusinessUnitId INT = NULL
AS 
BEGIN
	CREATE TABLE #TempTable (RowNumber INT IDENTITY PRIMARY KEY NOT NULL, Name VARCHAR(255) NULL, HeaderColor VARCHAR(255) NULL)

	--Inserting the business units that have ParentBusinessUnitId = @BusinessUnitId
	INSERT INTO #TempTable (Name, HeaderColor) 
	SELECT bu.Name, '#9bbfd1'  FROM vwPortal_BusinessUnit bu
	JOIN GProMeasureProgressHistory gmph ON bu.BusinessUnitId = gmph.BusinessUnitId
	WHERE ParentBusinessUnitId = @BusinessUnitId AND DeleteInd = 0  AND gmph.TotalPatients > 0 
	GROUP BY [Name] ORDER BY [Name]

	--Inserting the Business Unit selected and all parents
	;WITH CTE (ParentBusinessUnitId, BusinessUnitId, Name, HeaderColor)
	AS
	(
		SELECT ParentBusinessUnitId, BusinessUnitId, Name, '#66ccff' AS HeaderColor
		FROM vwPortal_BusinessUnit
		WHERE BusinessUnitId = @BusinessUnitId AND DeleteInd = 0
		UNION ALL
		SELECT v.ParentBusinessUnitId,v.BusinessUnitId, v.Name, '#66ccff' AS HeaderColor
		FROM vwPortal_BusinessUnit v
		JOIN CTE ON cte.ParentBusinessUnitId = v.BusinessUnitId
		AND v.DeleteInd = 0
	)
	INSERT INTO #TempTable SELECT Name, HeaderColor FROM CTE
	OPTION (MAXRECURSION 20);

	UPDATE #TempTable
	SET HeaderColor = '#34afcc'
	WHERE RowNumber=(SELECT MAX(RowNumber) FROM #TempTable)

	--Padding the rest of the temp table to make sure 25 rows are returned
	DECLARE @NullPadding INT = 25 - (SELECT Count(RowNumber) FROM #TempTable)
	DECLARE @x INT = 0
	WHILE @x < @NullPadding
	BEGIN
		INSERT INTO #TempTable (Name, HeaderColor) VALUES(NULL, 'Transparent') 
		SET @X += 1
	END
	SELECT * FROM #TempTable
	DROP TABLE #TempTable
END

GO
