SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalInterfacePatientAttribution]
AS

SELECT IPA.ProviderIdentifier, IPA.ProviderNPI, IPA.ProgramName, IPM.LbPatientId, I.SourceSystemId, PP.ProviderID  
INTO #IPA
FROM vwPortal_InterfacePatientAttribution IPA
JOIN vwPortal_InterfacePatientMatched IPM ON IPA.PatientIdentifier = IPM.PatientIdentifier
JOIN vwPortal_InterfaceSystem I ON IPM.InterfaceSystemId = I.InterfaceSystemId
JOIN vwPortal_Provider PP ON IPA.ProviderNPI = PP.NationalProviderIdentifier

SELECT DISTINCT IPA.LbPatientID, IPA.ProviderID, IPA.SourceSystemId, IPA.ProviderIdentifier, 
IPA.ProviderNPI
into #IPA2
FROM #IPA IPA
left JOIN vwPortal_PatientProvider PP ON IPA.LbPatientId = PP.PatientID
where (PP.ProviderID = 0) or PP.PatientProviderID is null

DROP TABLE #IPA

UPDATE PP SET DeleteInd = 1 FROM vwPortal_PatientProvider PP 
JOIN #IPA2 IPA2 ON PP.PatientID = IPA2.LbPatientID

INSERT INTO vwPortal_PatientProvider (
   PatientID
  ,ProviderID
  ,SourceSystemID
  ,ExternalReferenceIdentifier
  ,OtherReferenceIdentifier
  ,ProviderRoleTypeID
  ,DeleteInd
  ,CreateDateTime
  ,ModifyDateTime
  ,CreateLBUserId
  ,ModifyLBUserId
) SELECT LbPatientID, ProviderID, SourceSystemId, ProviderIdentifier, ProviderNPI, 1, 0, GETDATE(), GETDATE(), 1,1
    FROM #IPA2
    
    DROP TABLE #IPA2
GO
