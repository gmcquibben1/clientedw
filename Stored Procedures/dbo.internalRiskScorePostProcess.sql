SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalRiskScorePostProcess]
/*-- ===================================================================================
-- Author:		Lan Ma
-- Create date: 2012-12-18
-- Description:	Update ATI & JHACG Risk Score after JHACG and ATI Risk Calculation
-- Modified By: Mike
-- Modified Date:
-- Description: Added Hcc Risk Process
-- Modified By: Lan
-- Modified Date: 2016-09-01
-- Description: switched to Jhacg11 table JhacgPatientDetail to get Jhacg risk score  
   MODIFICATIONS
   Version     Date            Author		JIRA		Change
   =======     ==========      ======      =========	=========	
	2.2.2	   2017-02-01		LM			LBETL-517	Replace the full delete & re-insert 
	                                                    with merge statement to update records 
														with changed riskValue and insert new
														records only
	2.2.2	   2017-02-16		YL			LBETL-831	overwrite Risk Score if DefaultRisk = 'JHACG'
	                                                    in Patient_Summary_Pivot
	2.2.2	   2017-02-17		YL			LBETL-831	[Risk Scores] section can be removed as it was used of UAM only
	2.3.1      2017-05-17       YL          LBETL-1381  Prevent DELETE other risktype in the Merge process                                                   
-- ===================================================================================*/
AS
BEGIN
	

	

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'vwportal_PatientRisk';
	DECLARE @ProcedureName VARCHAR(128) = 'internalRiskScorePostProcess';
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	BEGIN TRY
		--run ATI
		SET @EDWtableName  = 'ATI_PatFactor';
		SET @ProcedureName = 'internalATIScriptBuild';	
	
		EXEC internalATIScriptBuild 

		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
		--REFRESH PATIENT SUMMARY PIVOT W/ ATI
		UPDATE PSP SET ATI = ATI.ATIScore FROM Patient_Summary_Pivot PSP
		JOIN ATI_PatFactor ATI ON PSP.LbPatientId = ATI.LBPatientID


		--get default risk type setting
        DECLARE @DefaultRisk varchar(100) = ISNULL((select SetValue FROM vwPortal_SystemSettings where SettingType = 'AnalyticsSetting' and SettingParameter = 'DefaultRisk'), 'JHACG')
		
		--add if JHACG is risk type REFRESH PATIENT SUMMARY PIVOT W/ Risk Score
		if @DefaultRisk = 'JHACG'
		BEGIN
			--Add Risk Scores
		--	IF OBJECT_ID('[Risk Scores]') IS NOT NULL
		--	BEGIN
		--		if (select count(1) from [Risk Scores]) > 1 
		--		BEGIN
		--			-- [Risk Scores] exists, [JhacgPatientDetail] does not exist 
		--			UPDATE a
		--			SET [Risk Score]=b.[Risk Score]
		--			FROM Patient_Summary_Pivot a
		--			INNER JOIN [Risk Scores]  b on a.LbPatientId=b.LbPatientId
		--		END
		--	END
		--	ELSE
		--	BEGIN
				IF OBJECT_ID('[JhacgPatientDetail]') IS NOT NULL --changed to use JHACG11 table JhacgPatientDetail
					UPDATE a
					SET [Risk Score]=b.rescaled_total_cost_pred_risk
					FROM Patient_Summary_Pivot a
					INNER JOIN JhacgPatientDetail  b on a.LbPatientId=b.[patient_id]
		--	END
		 END




		SET @EDWtableName = 'vwportal_PatientRisk';
		SET @ProcedureName  = 'internalRiskScorePostProcess';
		SET @StartTime  = GETUTCDATE();

		DECLARE @JhacgRiskTypeId INT = (Select PatientRiskTypeID FROM vwPortal_PatientRiskType WHERE Name = 'LbRisk');
		DECLARE @ATIRiskTypeId INT = (Select PatientRiskTypeID FROM vwPortal_PatientRiskType WHERE Name = 'LBATI');
		
		IF NOT EXISTS (SELECT 1 FROM SYSINDEXES WHERE NAME = 'NCIX_PatientSummaryPivot_LbPatientId' )
		BEGIN
		CREATE NONCLUSTERED INDEX NCIX_PatientSummaryPivot_LbPatientId
		ON Patient_Summary_Pivot (LbPatientId) INCLUDE ([ATI])
		END

		IF NOT EXISTS (SELECT 1 FROM SYSINDEXES WHERE NAME = 'NCIX_HccPatHistory_YearLbPatientId' )
		BEGIN
		CREATE NONCLUSTERED INDEX NCIX_HccPatHistory_YearLbPatientId
		ON HccPatHistory ([Year],LbPatientId) INCLUDE (RiskAdjustmentFactor)
		END

		SELECT
		PatientId, 
		RiskValue,
		PatientRiskTypeID 
		INTO #patientRisk
		FROM
		(
		SELECT  
		Patient_Id AS PatientId, 
		CAST(rescaled_total_cost_pred_risk AS DECIMAL(8,3)) AS RiskValue,
		@JhacgRiskTypeId AS PatientRiskTypeID 
		FROM [dbo].[JhacgPatientDetail]

		UNION ALL
		SELECT  
		LbPatientId AS PatientId, 
		CAST(ATI AS DECIMAL(8,3)) AS RiskValue, 
		@ATIRiskTypeId AS PatientRiskTypeID 
		FROM [dbo].Patient_Summary_Pivot
		where LbPatientId is not null

		UNION ALL
		SELECT  
		LbPatientId, 
		CAST(RiskAdjustmentFactor AS DECIMAL(8,3)) AS RiskValue,
		PRT.PatientRiskTypeID
		FROM HccPatHistory HCC
		JOIN [vwPortal_PatientRiskType] PRT
		ON HCC.Year = RIGHT(PRT.Name,4) AND PRT.Name LIKE 'HCC%'
		where LbPatientId is not null 
		) t

		CREATE UNIQUE INDEX IX_#patientRiskPatientID ON #patientRisk(PatientId,PatientRiskTypeID) INCLUDE(RiskValue)
		
		SET @RecordCountBefore = (SELECT COUNT(1) from  vwportal_PatientRisk) 

		MERGE vwportal_PatientRisk T
		USING #patientRisk S
		ON T.PatientId = S.PatientId AND T.PatientRiskTypeID = S.PatientRiskTypeID
		WHEN MATCHED AND CAST(T.RiskValue AS DECIMAL(8,3)) <> S.RiskValue
		   THEN UPDATE SET T.RiskValue = S.RiskValue,
						   T.ModifyDateTime = GETUTCDATE()
		WHEN NOT MATCHED BY TARGET 
		THEN  INSERT(PatientId,RiskValue,PatientRiskTypeId,StartDateTime,EndDateTime,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUserId)
		VALUES(S.PatientId,S.RiskValue,S.PatientRiskTypeId,GetUtcDate(),GetUtcDate() + 100,GetUtcDate(),GetUtcDate(),1,1)
		WHEN NOT MATCHED BY SOURCE AND T.PatientRiskTypeID IN (select distinct PatientRiskTypeID  FROM  #patientRisk)
		    THEN DELETE
		;

		SET @InsertedRecord = (SELECT COUNT(1) from  vwportal_PatientRisk WHERE CAST(CreateDateTime AS DATE) = CAST(GETDATE() AS DATE))
		SET @UpdatedRecord = (SELECT COUNT(1) from  vwportal_PatientRisk WHERE CAST(ModifyDateTime AS DATE) = CAST(GETDATE() AS DATE)) - @InsertedRecord
		
		DROP TABLE #patientRisk

		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
