SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create PROCEDURE [dbo].[ETLMemberExtractMergePortalTables] 
AS


BEGIN

SET NOCOUNT ON

	set ANSI_WARNINGS OFF

	DECLARE @PartyDescription    VARCHAR(50)='Patient'
	DECLARE @PartyTypeID         INT=1--Patient
	DECLARE @SourceSystemID      INT=2--UA Claims Feed
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientIdsRegistry';
	DECLARE @dataSource VARCHAR(20) = 'MemberExtract';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLMemberExtractMergePortalTables';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @LBUSERID            INT=1
	DECLARE @CurrentDateTime     DATETIME=GETUTCDATE()
	DECLARE @DeleteInd           BIT=0
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	BEGIN TRY

				CREATE TABLE #ResultsTemp1 (family_id uniqueidentifier,externalreferenceidentifier varchar(100),partyid int, individualid int, PatientID int)
				CREATE TABLE #ResultsTemp2 (family_id uniqueidentifier,externalreferenceidentifier varchar(100),partyid int, individualid int, PatientID int)
				CREATE TABLE #CMI (family_id UNIQUEIDENTIFIER, IdType VARCHAR(100), IdValue VARCHAR(100), IdMetaType VARCHAR(50), Source VARCHAR(50), SourceFeed VARCHAR(50)) -- DROP TABLE #CMI


				INSERT INTO  #CMI (family_id,IdType,IdValue,IdMetaType,Source,SourceFeed)
				SELECT -- ContractMemberIdentifier
						Family_id,         'ContractMemberIdentifier' AS IdType,
						CASE WHEN SourceFeed = 'Interface' THEN 
							(SELECT LTRIM(RTRIM(PatientOtherIdentifier)) FROM vwPortal_InterfacePatientMatched  IPM WHERE UToken = ME.medicareNo)
							ELSE MedicareNO END AS IdValue ,
						'' As IdMetaType, Me.Source, Me.SourceFeed
				FROM MemberExtract ME

				/*
				INSERT INTO  #CMI (family_id,IdType,IdValue,IdMetaType,Source,SourceFeed )
				EXEC internalComposeCustomResultset @RequestSubject = 'DisplayFriendlyIds', @Param1 = 2
				*/

				SELECT Family_id, MIN(IdValue) IdValue, MIN(IDType) IdType
				INTO #PreferredIds 
				FROM #CMI WHERE IDType = 'Display Nbr' 
				GROUP BY Family_Id

				;WITH Lids AS (SELECT MedicareNo , SourceSystemId FROM MemberExtract GROUP BY  MedicareNO , SourceSystemId HAVING COUNT(1) > 1)
				SELECT Me.MedicareNo, Me.Family_Id , ME.SourceSystemId
				INTO #LoosingManyNamesIds -- DROP TABLE #LoosingManyNamesIds
				FROM MemberExtract Me INNER JOIN Lids ON Me.MedicareNo = Lids.medicareNo and ME.SourceSystemId = Lids.SourceSystemId

				--set @ERRMSG = 'Merge into vwPortal_PARTY'
				--INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
				--     VALUES( @DBNAME, 'EtlMemberExtract', @ERRMSG, GETUTCDATE(), GETUTCDATE() );


		---Trace 1:  log information for vwPortal_PARTY
				SET @EDWtableName ='vwPortal_PARTY';
					SELECT @RecordCountBefore=COUNT(1)
					FROM vwPortal_PARTY;


				INSERT INTO #ResultsTemp1 (family_id,externalreferenceidentifier,partyid,individualid,PatientID)
				SELECT
						family_id, externalreferenceidentifier, partyid, NULL, NULL
				FROM
				(
						MERGE vwPortal_PARTY AS TARGET
						USING (
							select ExternalReferenceIdentifier,matched_ExternalReferenceIdentifier,OtherReferenceIdentifier,PartyDescription,PartyEndDateTime,PartyStartDateTime,
								PartyTypeID,SourceSystemID,     DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,family_id 
							FROM (
							select ExternalReferenceIdentifier,matched_ExternalReferenceIdentifier,OtherReferenceIdentifier,PartyDescription,PartyEndDateTime,PartyStartDateTime,
								PartyTypeID,SourceSystemID,     DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,family_id,
							row_number() over(partition by externalreferenceidentifier , SourceSystemId , PartyDescription order by PartyDescription ASC) r
							FROM (
								SELECT 
									me.MedicareNo as externalreferenceidentifier, me.MedicareNo as matched_ExternalReferenceIdentifier, me.MedicareNo AS OtherReferenceIdentifier,  @PartyDescription AS PartyDescription, NULL as PartyEndDateTime, @CurrentDateTime as PartyStartDateTime, 
									@PartyTypeID As PartyTypeId, me.SourceSystemID AS SourceSystemID, ISNULL(ME.holdback,0) As DeleteInd, 
									@CurrentDateTime as CreateDatetime, @CurrentDateTime as ModifyDatetime, @LBUSERID as CreateLbUserId, @LBUSERID AS ModifyLbUserid, 
									me.family_id 
								FROM MemberExtract ME 
								where ME.LbPatientId is NULL and ME.SEX is not null
								) A 
							) B where B.r = 1	
								) AS SOURCE
								(ExternalReferenceIdentifier,matched_ExternalReferenceIdentifier,OtherReferenceIdentifier,PartyDescription,PartyEndDateTime,PartyStartDateTime,
								PartyTypeID,SourceSystemID,     DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,family_id) 
							ON TARGET.ExternalReferenceIdentifier = SOURCE.matched_ExternalReferenceIdentifier 
								AND TARGET.PartyDescription = SOURCE.PartyDescription 
								-- added SourceSystemId check for new patients by Sam 4/25/2016
								AND TARGET.SourceSystemID = SOURCE.SourceSystemID 
						WHEN MATCHED THEN UPDATE SET
								ExternalReferenceIdentifier = Source.ExternalReferenceIdentifier,
								OtherReferenceIdentifier = Source.OtherReferenceIdentifier,
								PartyDescription = Source.PartyDescription,
								PartyEndDateTime  = Source.PartyEndDateTime,
								--PartyStartDateTime = Source.PartyStartDateTime,
								PartyTypeID = Source.PartyTypeID,
								SourceSystemID = Source.SourceSystemID,
								DeleteInd = Source.DeleteInd,
								ModifyDateTime = Source.ModifyDateTime,
								ModifyLBUserId = Source.ModifyLBUserId
						WHEN NOT MATCHED THEN INSERT 
								(ExternalReferenceIdentifier,OtherReferenceIdentifier,PartyDescription,PartyEndDateTime,PartyStartDateTime,
								PartyTypeID,SourceSystemID,     DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId) 
						VALUES (source.ExternalReferenceIdentifier,source.OtherReferenceIdentifier,source.PartyDescription,source.PartyEndDateTime,source.PartyStartDateTime,
								source.PartyTypeID,source.SourceSystemID,source.DeleteInd,source.CreateDateTime,source.ModifyDateTime,source.CreateLBUserId,source.ModifyLBUserId) 
				OUTPUT $ACTION MergeAction,  ISNULL(INSERTED.PartyId,DELETED.PartyID) PartyID, source.family_id,source.ExternalReferenceIdentifier 
				) x;

					SELECT @RecordCountAfter=COUNT(1)
					FROM [DBO].vwPortal_PARTY;

						-- Calculate records inserted and updated counts
					SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
					--  SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

					-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
					SET  @EndTime=GETUTCDATE();
					EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	


					--SELECT * FROM #ResultsTemp1;
					CREATE INDEX IX1 ON #ResultsTemp1 (   Family_id, ExternalReferenceIdentifier) INCLUDE (PartyID)


					--set @ERRMSG = 'Merge into vwPortal_Individual'
					--INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
					--     VALUES( @DBNAME, 'EtlMemberExtract', @ERRMSG, GETUTCDATE(), GETUTCDATE() );


		---Trace 2:  log information for vwPortal_Individual
					SET @StartTime=@EndTime;

					SET @EDWtableName ='vwPortal_Individual';
						SELECT @RecordCountBefore=COUNT(1)
						FROM vwPortal_Individual;


					INSERT INTO #ResultsTemp2 (family_id,externalreferenceidentifier,partyid,individualid,PatientID)
					SELECT
						 family_id, externalreferenceidentifier, partyid, individualid, NULL
					FROM
					(
						 MERGE vwPortal_Individual AS TARGET
						 USING (
								  SELECT 
									   Me.BirthDate,me.MedicareNo as ExternalReferenceIdentifier, me.MedicareNo  AS OtherReferenceIdentifier,  CASE WHEN Me.SEX = 'M' THEN 1 WHEN Me.SEX = 'F' THEN 2 ELSE Me.Sex END  AS GenderTypeId, NULL as MaritalStatusTypeID, 
									   P.PartyId, @PartyTypeID As PartyTypeId, NULL AS ReligionTypeID,@SourceSystemID AS SourceSystemID, NULL AS TobaccoTypeId, 
									   ISNULL(ME.holdback,0) As DeleteInd, @CurrentDateTime as CreateDatetime, @CurrentDateTime as ModifyDatetime, @LBUSERID as CreateLbUserId, @LBUSERID AS ModifyLbUserid, 
									   me.family_id, ISNULL(me.MedicareNo,me.MedicareNo) as matched_ExternalReferenceIdentifier, PI.IdValue As PreferredId,
									   CASE WHEN Me.DeathDate IS NOT NULL THEN 1 ELSE 0 END AS DeceasedInd
								  FROM MemberExtract ME 
								  LEFT JOIN #PreferredIds PI ON ME.family_id = PI.family_id 
								  INNER JOIN ( select distinct P1.* from #ResultsTemp1 P1) P ON ME.Family_id = P.Family_ID
								  -- LEFT JOIN #LoosingManyNamesIds Lids ON ME.family_id = Lids.family_id 
								  -- WHERE Lids.family_id IS NULL 
								 ) AS SOURCE
								   (BirthDate,ExternalReferenceIdentifier,OtherReferenceIdentifier,GenderTypeID,MaritalStatusTypeID,
									  PartyID,PartyTypeID,ReligionTypeID,SourceSystemID,TobaccoTypeID,
									   DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId,family_id,matched_ExternalReferenceIdentifier,PreferredId,DeceasedInd) 
								ON TARGET.PartyId = SOURCE.PartyId AND TARGET.PartyTypeId = SOURCE.PartyTypeId 
						 WHEN MATCHED THEN UPDATE SET
								  BirthDate = source.BirthDate,
								  ExternalReferenceIdentifier = source.PreferredId,
								  OtherReferenceIdentifier = source.OtherReferenceIdentifier,
								  GenderTypeID = source.GenderTypeID,
								  MaritalStatusTypeID = source.MaritalStatusTypeID,
								  PartyID = source.PartyID,
								  PartyTypeID = source.PartyTypeID,
								  ReligionTypeID = source.ReligionTypeID,
								  SourceSystemID = source.SourceSystemID,
								  TobaccoTypeID = source.TobaccoTypeID,
								  DeleteInd = source.DeleteInd,
								  ModifyDateTime = source.ModifyDateTime,
								  ModifyLBUserId = source.ModifyLBUserId,
								  DeceasedInd = source.DeceasedInd
						 WHEN NOT MATCHED THEN INSERT 
									  (BirthDate,ExternalReferenceIdentifier,OtherReferenceIdentifier,GenderTypeID,MaritalStatusTypeID,
									  PartyID,PartyTypeID,ReligionTypeID,SourceSystemID,TobaccoTypeID,
									   DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId,DeceasedInd)
						 VALUES         (source.BirthDate,source.PreferredId,source.OtherReferenceIdentifier,source.GenderTypeID,source.MaritalStatusTypeID,
									  source.PartyID,source.PartyTypeID,source.ReligionTypeID,source.SourceSystemID,source.TobaccoTypeID,
									   source.DeleteInd,source.CreateDateTime,source.ModifyDateTime,source.ModifyLBUserId,source.CreateLBUserId,source.DeceasedInd) 
					OUTPUT $ACTION MergeAction,  ISNULL(INSERTED.IndividualId,DELETED.IndividualID) IndividualId, source.family_id,source.ExternalReferenceIdentifier, source.partyId
					) x;

					CREATE INDEX IX2 ON #ResultsTemp2 (   Family_id) INCLUDE (PartyID, ExternalReferenceIdentifier, IndividualId)


						SELECT @RecordCountAfter=COUNT(1)
						FROM [DBO].vwPortal_Individual;

							-- Calculate records inserted and updated counts
						SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
						--  SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

						-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						SET  @EndTime=GETUTCDATE();
						EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	


					--set @ERRMSG = 'update vwPortal_Patient'
					--INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
					--     VALUES( @DBNAME, 'EtlMemberExtract', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

					UPDATE vwPortal_Patient
					SET DeleteInd  = I.DeleteInd
					FROM #ResultsTemp2 RT2 
					INNER JOIN vwPortal_Individual I ON RT2.IndividualId = I.IndividualID
					INNER JOIN vwPortal_Patient P ON P.IndividualId = I.IndividualID

					INSERT INTO vwPortal_Patient
							  (PatientGuid,IndividualID,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId)
					SELECT newid(), RT2.individualid, @DeleteInd,@CurrentDateTime,@CurrentDateTime,@LBUSERID ,@LBUSERID
					FROM #ResultsTemp2 RT2 
					LEFT JOIN vwPortal_Patient P ON RT2.IndividualId = P.IndividualID
					WHERE P.IndividualID IS NULL 

					UPDATE #ResultsTemp2
					SET PatientID = P.PatientID 
					FROM #ResultsTemp2 RT2 
					INNER JOIN vwPortal_Patient P ON RT2.IndividualId = P.IndividualID


		---Trace 3:  log information for vwPortal_IndividualName
					SET @StartTime=GETUTCDATE();

					SET @EDWtableName ='vwPortal_IndividualName';
						SELECT @RecordCountBefore=COUNT(1)
						FROM vwPortal_IndividualName;


					MERGE vwPortal_IndividualName AS TARGET
					USING (
							  SELECT 
								  P.IndividualID,@SourceSystemID AS SourceSystemID, LTRIM(RTRIM(Me.FirstName)) FirstName,  LTRIM(RTRIM(Me.midInit)) as MiddleName, LTRIM(RTRIM(Me.LastName)) LastName,
								  NULL As NamePrefix, NULL As NameSuffix,P.ExternalReferenceIdentifier, NULL AS OtherReferenceIdentifier,ISNULL(ME.holdback,0)  As DeleteInd, 
								  @CurrentDateTime as CreateDatetime, @CurrentDateTime as ModifyDatetime, @LBUSERID as ModifyLbUserid, @LBUSERID AS CreateLBUserId
							  FROM MemberExtract ME 
							  INNER JOIN #ResultsTemp2 P ON ME.Family_id = P.Family_ID 
							  ) AS SOURCE
							  (IndividualID,SourceSystemID,FirstName,MiddleName,LastName,
								   NamePrefix,NameSuffix,ExternalReferenceIdentifier,OtherReferenceIdentifier,DeleteInd,
								   CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId) 
							  ON TARGET.IndividualId = SOURCE.IndividualId 
					WHEN MATCHED THEN UPDATE SET
							  IndividualID = source.IndividualID,
							  SourceSystemID = source.SourceSystemID,
							  FirstName = source.FirstName,
							  MiddleName = source.MiddleName,
							  LastName = source.LastName,
							  NamePrefix = source.NamePrefix,
							  NameSuffix = source.NameSuffix,
							  ExternalReferenceIdentifier = source.ExternalReferenceIdentifier,
							  OtherReferenceIdentifier = source.OtherReferenceIdentifier,
							  DeleteInd = source.DeleteInd,
							  ModifyDateTime = source.ModifyDateTime,
							  ModifyLBUserId = source.ModifyLBUserId
					WHEN NOT MATCHED THEN INSERT 
								  (IndividualID,SourceSystemID,FirstName,MiddleName,LastName,
								   NamePrefix,NameSuffix,ExternalReferenceIdentifier,OtherReferenceIdentifier,DeleteInd,
								   CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId)
					VALUES         (source.IndividualID,source.SourceSystemID,source.FirstName,source.MiddleName,source.LastName,
								   source.NamePrefix,source.NameSuffix,source.ExternalReferenceIdentifier,source.OtherReferenceIdentifier,source.DeleteInd,
								   source.CreateDateTime,source.ModifyDateTime,source.ModifyLBUserId,source.CreateLBUserId) ;

					/*
					 * TBD. Not performing. 
					 * need to Debug.
					 *
					 * by : Sam 4/26/2016
					 *
					set @ERRMSG = 'Update vwPortal_IndividualName'
					INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
						 VALUES( @DBNAME, 'EtlMemberExtract', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

					--update names with stuff from the interface if available
							SELECT InterfaceSystemId,PatientIdentifier, MiddleName, PrefixName, SuffixName ,
							  ROW_NUMBER() OVER(PARTITION BY InterfaceSystemId,PatientIdentifier ORDER BY CreateDateTime DESC) r 
							  into #ITMP1
							 FROM vwPortal_InterfacePatient

					UPDATE pin
					SET pin.MiddleName = ip.MiddleName, pin.NamePrefix = ip.PrefixName, pin.NameSuffix = ip.SuffixName
					FROM  vwPortal_IndividualName pin
						 INNER JOIN vwPortal_Patient p ON pin.IndividualId = p.IndividualId
						 INNER JOIN PatientIdReference idRef ON p.PatientId = idRef.LbPatientId
						 INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.SourceSystemId = idRef.SourceSystemId
						 INNER JOIN #ITMP1 ip ON ip.InterfaceSystemId = ifs.SourceSystemId AND ip.PatientIdentifier = idRef.ExternalId ANd ip.r = 1
					*/


					SELECT InterfaceSystemId,PatientIdentifier, MiddleName, PrefixName, SuffixName ,
							  ROW_NUMBER() OVER(PARTITION BY InterfaceSystemId,PatientIdentifier ORDER BY CreateDateTime DESC) r 
							  into #ITMP1
							 FROM vwPortal_InterfacePatient

					CREATE NONCLUSTERED INDEX [IDX_ITMP1_R1]
					ON [dbo].[#ITMP1] ([r])
					INCLUDE ([InterfaceSystemId],[PatientIdentifier],[MiddleName],[PrefixName],[SuffixName])

					CREATE NONCLUSTERED INDEX [IDX_ITMP1_R2]
					ON [dbo].[#ITMP1] ([InterfaceSystemId],[PatientIdentifier],[r])
					INCLUDE ([MiddleName],[PrefixName],[SuffixName])


					UPDATE pin
					SET pin.MiddleName = ip.MiddleName, pin.NamePrefix = ip.PrefixName, pin.NameSuffix = ip.SuffixName
					FROM  vwPortal_IndividualName pin
						 INNER JOIN vwPortal_Patient p ON pin.IndividualId = p.IndividualId
						 INNER JOIN PatientIdReference idRef ON p.PatientId = idRef.LbPatientId
						 INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.SourceSystemId = idRef.SourceSystemId
						 INNER JOIN #ITMP1 ip ON ip.InterfaceSystemId = ifs.InterfaceSystemId AND ip.PatientIdentifier = idRef.ExternalId ANd ip.r = 1

                    
						SELECT @RecordCountAfter=COUNT(1)
						FROM vwPortal_IndividualName;

							-- Calculate records inserted and updated counts
						SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
						--  SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

						-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						SET  @EndTime=GETUTCDATE();
						EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	


					-- ID MAINTENANCE SECTION 
					
		---Trace 4:  log information for PatientIdsRegistry
					SET @StartTime=GETUTCDATE();

					SET @EDWtableName ='PatientIdsRegistry';
					
                    DECLARE @rowcounts TABLE(mergeAction nvarchar(10));
					
					MERGE [DBO].PatientIdsRegistry AS target
					USING (   
								SELECT Family_id , IdType,IdValue AS IdValue, '' AS IdComments, IdMetaType AS IdMetaType, Source As IdSource,SourceFeed AS IdFeed
								FROM #CMI SS --WHERE IdType = 'ContractMemberIdentifier'
						   ) AS source 
						   (Family_id,IdType,IdValue,IdComments,IdMetaType,IdSource,IdFeed)
					ON (target.Family_id = source.Family_id AND target.IdType = source.IdType)
					WHEN MATCHED THEN 
						 UPDATE SET IdSource = source.IdSource, IdFeed = SOurce.IdFeed , IdMetaType = Source.IdMetaType
					WHEN NOT MATCHED THEN
						 INSERT (Family_id,IdType, IdValue, IdComments,IdMetaType, IdFeed, IdSource )
						 VALUES (source.Family_id,source.IdType, source.IdValue, source.IdComments,source.IdMetaType, source.IdFeed, source.IdSource)
					WHEN NOT MATCHED By SOURCE AND target.Idtype IN ('ContractMemberIdentifier','Display Nbr') THEN DELETE
					OUTPUT $action into @rowcounts;
					;
					
					SELECT @InsertedRecord=  SUM(CASE WHEN mergeAction = 'INSERT' THEN 1 ELSE 0 END),
						   @UpdatedRecord = SUM(CASE WHEN mergeAction = 'UPDATE' THEN 1 ELSE 0 END)
					  FROM @rowcounts 
                    -- test
					--Print @InsertedRecord;
					--Print @UpdatedRecord;
					

					INSERT INTO PatientIdsRegistry (Family_Id, IdType, IdValue) 
					SELECT RT2.family_id, 'PartyId', RT2.partyid 
					FROM #ResultsTemp2 RT2
					LEFT JOIN PatientIdsRegistry PIR ON RT2.family_id = PIR.Family_Id AND RT2.partyid = PIR.IdValue AND PIR.IDType = 'PartyId'
					WHERE PIR.IdValue IS NULL ;
					SET  @InsertedRecord=@InsertedRecord+@@ROWCOUNT;


					INSERT INTO PatientIdsRegistry (Family_Id, IdType, IdValue) 
					SELECT RT2.family_id, 'IndividualId', RT2.individualid 
					FROM #ResultsTemp2 RT2
					LEFT JOIN PatientIdsRegistry PIR ON RT2.family_id = PIR.Family_Id AND RT2.individualid = PIR.IdValue AND PIR.IDType = 'IndividualId'
					WHERE PIR.IdValue IS NULL ;
					SET  @InsertedRecord=@InsertedRecord+@@ROWCOUNT;


					INSERT INTO PatientIdsRegistry (Family_Id, IdType, IdValue) 
					SELECT RT2.family_id, 'LbPatientId', RT2.PatientId 
					FROM #ResultsTemp2 RT2
					LEFT JOIN PatientIdsRegistry PIR ON RT2.family_id = PIR.Family_Id AND RT2.PatientID = PIR.IdValue AND PIR.IDType = 'LbPatientId'
					WHERE PIR.IdValue IS NULL 
					SET  @InsertedRecord=@InsertedRecord+@@ROWCOUNT;

						-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						SET  @EndTime=GETUTCDATE();
						EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	

					-- update InterfacePatientMatched
					UPDATE vwPortal_InterfacePatientMatched
					SET LbPatientId = PIRS2.IdValue --SELECT PIRS2.IdValue 
					FROM vwPortal_InterfacePatientMatched IPM 
					INNER JOIN PatientIdsRegistry PIRS1 ON IPM.UToken = PIRS1.IdValue --AND PIRS1.IDType = 'Utoken'
					INNER JOIN PatientIdsRegistry PIRS2 ON PIRS1.Family_Id = PIRS2.Family_Id AND PIRS2.IDType = 'Lbpatientid'
					WHERE IPM.LbPatientId IS NULL 

					UPDATE vwPortal_InterfacePatientMatched
					SET LbPatientId = PIRS2.IdValue --SELECT PIRS2.IdValue 
					FROM vwPortal_InterfacePatientMatched IPM 
					INNER JOIN MemberExtract PIRS1 ON IPM.UToken = PIRS1.medicareNo --AND PIRS1.IDType = 'Utoken'
					INNER JOIN PatientIdsRegistry PIRS2 ON PIRS1.Family_Id = PIRS2.Family_Id AND PIRS2.IDType = 'Lbpatientid'
					WHERE IPM.LbPatientId IS NULL 




					--UPDATE vwPortal_InterfacePatientIdReference 
					--SET LbPatientID = IPM.LbPatientId 
					--FROM vwPortal_InterfacePatientIdReference IPIR INNER JOIN vwPortal_InterfacePatientMatched IPM 
					--ON IPIR.InterfacePatientMatchedId = IPM.InterfacePatientMatchedId
					--WHERE IPIR.LbPatientId IS NULL 

	    
		END TRY
		BEGIN CATCH
				   --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
					SET  @EndTime=GETUTCDATE();
					SET  @ErrorNumber =ERROR_NUMBER();
					SET  @ErrorState =ERROR_STATE();
					SET  @ErrorSeverity=ERROR_SEVERITY();
					SET  @ErrorLine=ERROR_LINE();
					SET  @Comment =left(ERROR_MESSAGE(),400);
					EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
		END CATCH

END



GO
