SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GProMeasureStatus]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2014-12-09
	INITIAL VER:	1.4.2
	MODULE:			GPRO		
	DESCRIPTION:	Stored Procedure used to return the list of ranked GPro measures for a specific 
					patient and calculate the status of each ranked GPro measure
	PARAMETERS:		Business Unit id
	RETURN VALUE(s)/OUTPUT:	a table containing the progress totals for all gpro measure
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.0 		2014-12-09		BR						Initial Version
	1.1			2014-12-17		BR						Added row ids to uniquely identify which items in the ranking and measure tables are returned, altered patient status calculation
	1.2			2015-01-05		BR						Updated the CARE-1 status calculation to incorporate the new GProPatientDischarge table 
	1.3			2015-01-19		BR						Adding the rank to each measure
	1.4			2015-01-30		BR						Fixed an issue calculating CARE-1 status if no discharge dates were loaded
	1.5			2015-02-05		BR						Modified the CARE-1 status again, added checks for NULL in many measures
	1.6.1		2015-11-17		BR						Updated to use 2015 measures
	1.6.1		2015-12-10		BR						Updated logic for MH-1, DM-7, and CARE-3
	1.6.1		2016-02-19		BR						Added additional denominator exclusion values to some measures
	1.6.1		2016-02-28		BR						updated the way some measures are counted as skipped for CAD, HF, and MH
	2.2.1		2016-11-03		BR						Refactored to dynamically calculate status, includes PREV-13 and all measures in GproMeasureType
	2.2.1		2016-11-09		BR			LBPP-2028	Added additional column to skipped and complete sets to comply w/ new stored proc return values
	2.2.1		2016-11-23		BR			LBPP-2033	Fixed a bug by changing temp table index to GproPatientRankingId.  Using PatientId would fail if the patient wasn't matched
														to a pre-existing LB patient
=================================================================*/
(
	@Hicn VARCHAR(15),
	@ReturnResult bit = 1
)
AS
BEGIN
    SET NOCOUNT ON;

	--Create a temp table to hold the measure information, this will be returned to the caller
	DECLARE  @GProMeasureStatusResult TABLE 
	(
		GProPatientMeasureId	INT,	--used to find the exact row in the GProPatientMeasure table this result refers to
		GProPatientRankingId	INT,	--used to find the exact row in the GProPatientRanking table this result refers to
		MeasureId				VARCHAR(8),
		MeasureName				VARCHAR(250),
		MeasureStatus			VARCHAR(10),
		MeasureRank				INT
	)

	CREATE TABLE #CompletePatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #SkippedPatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #tmpMeasureList
	(
		RowNumber INT IDENTITY (1, 1)
		, MeasureId VARCHAR(25)
	)

	------------------------------------------------------------------
	-- Create the initial set for the selected patient
	------------------------------------------------------------------
	INSERT INTO @GproMeasureStatusResult
		(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank)
	SELECT gpm.GpropatientMeasureId,gpm.GproPatientRankingId,gmt.Name,gmt.DisplayValue,'Incomplete',gprgmt.MeasureRank
	FROM GproPatientMeasure gpm
		INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
		INNER JOIN [GproPatientRankingGproMeasureType] gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId
		INNER JOIN [GproMeasureType] gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId
	WHERE gpr.MedicareHicn = @Hicn

	INSERT INTO #tmpMeasureList (MeasureId) SELECT DISTINCT MeasureId FROM @GproMeasureStatusResult;

	DECLARE @Sql VARCHAR(4000);
	DECLARE @MeasureTypeCount INT = (SELECT COUNT(1) FROM #tmpMeasureList);
	DECLARE @CurrentMeasureName VARCHAR(20) = NULL;
	WHILE @MeasureTypeCount > 0
	BEGIN
		TRUNCATE TABLE #CompletePatients;
		TRUNCATE TABLE #SkippedPatients;

		SET @CurrentMeasureName = (SELECT MeasureId FROM #tmpMeasureList WHERE RowNumber = @MeasureTypeCount)
		IF (@CurrentMeasureName IS NOT NULL)
		BEGIN
			--------------------------------------------------------------------------------
			-- find the set of all complete patients and all skipped patients for this measure
			--------------------------------------------------------------------------------
			SET @Sql = 'INSERT INTO #CompletePatients EXEC GproGetCompletePatientList ''' + @CurrentMeasureName + ''''
			EXEC (@Sql);
			SET @Sql = 'INSERT INTO #SkippedPatients EXEC GproGetSkippedPatientList ''' + @CurrentMeasureName + ''''
			EXEC (@Sql);

			--------------------------------------------------------------------------------
			-- update the status of any skipped measure
			--------------------------------------------------------------------------------
			UPDATE r
				SET r.MeasureStatus = 'Skipped'
			FROM @GproMeasureStatusResult r
				INNER JOIN #SkippedPatients s ON s.GproPatientRankingId = r.GproPatientRankingId
			WHERE r.MeasureId = @CurrentMeasureName

			--------------------------------------------------------------------------------
			-- update the status of any complete measure
			--------------------------------------------------------------------------------
			UPDATE r
				SET r.MeasureStatus = 'Complete'
			FROM @GproMeasureStatusResult r
				INNER JOIN #CompletePatients c ON c.GproPatientRankingId = r.GproPatientRankingId
			WHERE r.MeasureId = @CurrentMeasureName
		END

		SET @MeasureTypeCount = @MeasureTypeCount - 1;
	END
	

	--------------------------------------------------------------------------------
	-- Update the patient level status
	--------------------------------------------------------------------------------
	DECLARE @RankingId INT = (SELECT TOP 1 GproPatientRankingId FROM @GproMeasureStatusResult);
	EXEC GproPatientStatusSet @RankingId;


	--now spit out whatever is in the temp table
	IF @ReturnResult = 1
		SELECT GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank FROM @GProMeasureStatusResult;
END
GO
