SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-03-25
-- Description:	Load Procedure Code Data into EDW
-- Modified By:
-- Modified Date:
-- ===============================================================
-- Modified By: Youping
-- Modified Date:6/27/2016
-- Modification: duplication due to left join, need to add a dedup process before that
------------------------------------------------------------------


CREATE PROCEDURE [dbo].[ETLProcedureCode]

AS
BEGIN	

        --get all ProcedureCodingSystemName and ID into temp table
		SELECT
		ProcedureCodingSystemTypeID,
		RefCodingSystemName
		INTO #ProcedureCodingSystemType
		FROM(
			  SELECT
			  PCST.ProcedureCodingSystemTypeID,
			  PCST.NAME As RefCodingSystemName
			  FROM ProcedureCodingSystemType PCST 
			  UNION
			  SELECT 
			  PCST.ProcedureCodingSystemTypeID,
			  PCSTS.SynonymName As RefCodingSystemName
			  FROM ProcedureCodingSystemType PCST 
			  INNER JOIN ProcedureCodingSystemTypeSynonyms PCSTS ON PCST.ProcedureCodingSystemTypeID = PCSTS.ProcedureCodingSystemTypeID
		    ) t


         ;
		 --- dedup 6/27/2016
		 SELECT * 
		 INTO #PatientProcedure
		 FROM (

			SELECT t2.ProcedureCodingSystemTypeID,
					t1.[ProcedureCode],
					ISNULL(t1.[ProcedureDescription],'') [ProcedureDescription],
					ROW_NUMBER() OVER(PARTITION BY t2.ProcedureCodingSystemTypeID,t1.[ProcedureCode] ORDER BY  InterfacePatientProcedureId DESC ) AS rowNbr
		FROM PatientProcedureProcessQueue t1
		INNER JOIN #ProcedureCodingSystemType t2
		ON LTRIM(RTRIM(t1.ProcedureCodingSystemName)) = LTRIM(RTRIM(t2.RefCodingSystemName))

        ) t WHERE rowNbr = 1 



      --load new procedure codes into ProcedureCode table
		INSERT INTO [dbo].[ProcedureCode]
			   ([ProcedureCodingSystemTypeId]
			   ,[ProcedureCode]
			   ,[ProcedureDescription]
			   ,[ProcedureDescriptionAlternate]
			   ,[DeleteInd]
			   ,[CreateDateTime]
			   ,[ModifyDateTime]
			   ,[CreateLBUserId]
			   ,[ModifyLBUserId])
		SELECT DISTINCT
		t1.ProcedureCodingSystemTypeID,
		t1.[ProcedureCode],
		ISNULL(t1.[ProcedureDescription],''),
		ISNULL(t1.[ProcedureDescription],''),
		0,
		GETUTCDATE(),
		GETUTCDATE(),
		1,
		1
		FROM #PatientProcedure T1
		LEFT JOIN [dbo].[ProcedureCode] t3
		ON t1.[ProcedureCodingSystemTypeId] = t3.[ProcedureCodingSystemTypeId] 
		AND LTRIM(RTRIM(t1.[ProcedureCode])) = LTRIM(RTRIM(t3.[ProcedureCode]))
		WHERE t1.[ProcedureCodingSystemTypeId] IS NOT NULL AND t3.ProcedureCodingSystemTypeId IS NULL

		UPDATE ProcedureCode 
		SET ProcedureDescription = CPT4.description, 
			ProcedureDescriptionAlternate = CPT4.description
		FROM ProcedureCode PC INNER JOIN CPT4Mstr CPT4 ON PC.ProcedureCode = CPT4.cpt4_code_id
		WHERE PC.ProcedureDescription LIKE 'DUMMY%' OR PC.ProcedureDescription = ''

END
GO
