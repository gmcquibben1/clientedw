SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ======================================================================================
-- Author:		Mike Hoxter
-- Create date: 2015-12-18
-- Description:	set patient provider attribution from Patient Claims PCP data
-- Modified By: Lan Ma
-- Modified Date: 2016-04-05
-- Modified By: Youping
-- Modified Date: 2016-04-21  add LbPatientId IS NOT NULL
-- ======================================================================================
/*
	Version		Date		Author	Change
	-------		----------	------	------	
	2.2.1       2017-01-19  YL      LBETL-525 Add index to speed process
*/

CREATE PROCEDURE [dbo].[internalAttributionPluralityClaimPCP]  @LookBackMonths int = 18
AS
BEGIN

	DECLARE @AttributionTypeId int = 5;

	--EXEC internalGetExternalNPIData 

--using the vwportal_ProviderActive to exclude the provides without healthcare org assigned, updated on 04/05/2016
	SELECT B.LbPatientId, B.BENE_HIC_NUM, B.RNDRG_PRVDR_NPI_NUM, PP.ProviderID, ISNULL(count(1),0) AS VisitCount, SUM(isnull(B.CLM_LINE_CVRD_PD_AMT,0)) AS SumPaid,
	MAX(B.CLM_FROM_DT) AS ClmDate, 0 AS TopProvider 
	INTO #tmp1
	FROM CCLF_5_PartB_Physicians B
	JOIN HEDISValueSetCodes H ON B.CLM_LINE_HCPCS_CD = H.Code AND H.[Value Set Name] = 'Outpatient'
	JOIN vwPortal_ProviderActive PP ON B.RNDRG_PRVDR_NPI_NUM = PP.NationalProviderIdentifier
	JOIN NPI_Lookup NPI ON B.RNDRG_PRVDR_NPI_NUM = NPI.NPI AND NPI.Specialty 
    IN ('1','01','8','08','11','31', 'FP','GP','IM', '37', 'OBG','OBS','GYN','16','38','FPG','IMG','MDM', '50')
	WHERE DATEDIFF(Month, B.CLM_FROM_DT, getdate()) <= 18 
		and ISNULL(TRY_CONVERT(INT,B.CLM_ADJSMT_TYPE_CD),0) = 0   AND B.LbPatientId IS NOT NULL 
	GROUP BY B.LbPatientId, B.BENE_HIC_NUM, B.RNDRG_PRVDR_NPI_NUM, PP.ProviderID

	
	CREATE INDEX IX_PatientID_tmp1 on #tmp1 (LbPatientId) include (ProviderID, SumPaid ,ClmDate , VisitCount);



--determin the winner provider by the claims amount first, then claimDate, then visit count, providerID
	INSERT INTO [dbo].[vwPortal_PatientProvider]
        ([PatientID]
        ,[ProviderID]
        ,[SourceSystemID]
        ,[ExternalReferenceIdentifier]
        ,[OtherReferenceIdentifier]
        ,[ProviderRoleTypeID]
        ,[DeleteInd]
        ,[CreateDateTime]
        ,[ModifyDateTime]
        ,[CreateLBUserId]
        ,[ModifyLBUserId]
        ,[AttributionTypeId]
		)
	SELECT 
		LbPatientId AS [PatientID]
		,ProviderID
		,1 AS [SourceSystemID]
		,BENE_HIC_NUM AS [ExternalReferenceIdentifier]			
		,RNDRG_PRVDR_NPI_NUM  AS [OtherReferenceIdentifier]
		,1 AS [ProviderRoleTypeID]
		,0 AS [DeleteInd]
		,GETDATE() AS [CreateDateTime]
		,GETDATE() AS [ModifyDateTime]
		,1 AS [CreateLBUserId]
		,1 AS [ModifyLBUserId]
		,5 AS [AttributionTypeId]
	FROM(
			SELECT
			LbPatientId
			,ProviderID
			,BENE_HIC_NUM			
			,RNDRG_PRVDR_NPI_NUM
			,ROW_NUMBER() OVER(PARTITION BY LbPatientId ORDER BY SumPaid DESC,ClmDate DESC, VisitCount DESC,ProviderID ) AS rowNbr
	FROM #tmp1
	) t WHERE rowNbr = 1

DROP TABLE #tmp1

/* --Commented the original code, keep it for reference

--determine winner by visit count
SELECT LbPatientId, max(VisitCount) as MaxVisits
INTO #tpm2
FROM #tmp1
GROUP BY LbPatientId

SELECT distinct T1.*
INTO #tmp3
FROM #tmp1 T1
JOIN #tpm2 T2 ON T1.LbPatientId = T2.LbPatientId 
      and T1.VisitCount = T2.MaxVisits

--determine winner by paid amount
SELECT LbPatientId, max(SumPaid) as SumPaid
INTO #tpm2b
FROM #tmp1
GROUP BY LbPatientId

SELECT distinct T1.LbPatientId, T1.BENE_HIC_NUM, T1.RNDRG_PRVDR_NPI_NUM, T1.VisitCount, T1.ClmDate, T1.TopProvider
INTO #tmp3b
FROM #tmp1 T1
JOIN #tpm2b T2 ON T1.LbPatientId = T2.LbPatientId 
      and T1.SumPaid = T2.SumPaid


--SELECT PATIENTS W/ 1 PROVIDER W/ COUNT OF VISITS INTO T4, THOSE WITH A TIE GO INTO T5
SELECT LbPatientId, COUNT(*) AS PRVCOUNT
INTO #tmp4
FROM #tmp3b T3
GROUP BY LbPatientId
HAVING COUNT(*) = 1



SELECT LbPatientId, COUNT(*) AS PRVCOUNT, MAX(ClmDate) AS [ClmDate]
INTO #tmp5
FROM #tmp3b T3
GROUP BY LbPatientId
HAVING COUNT(*) > 1

--set Winners based on plurality
UPDATE T1 SET TopProvider = 1 FROM #tmp1 T1
JOIN #tmp3b T3 ON T1.LbPatientId = T3.LbPatientId AND T1.RNDRG_PRVDR_NPI_NUM = T3.RNDRG_PRVDR_NPI_NUM
JOIN #tmp4 T4 ON T4.LbPatientId = T3.LbPatientId

--FOR THOSE WITH A TIE
UPDATE T1 SET TopProvider = 1 FROM #tmp1 T1
JOIN #tmp3b T3 ON T1.LbPatientId = T3.LbPatientId AND T1.RNDRG_PRVDR_NPI_NUM = T3.RNDRG_PRVDR_NPI_NUM
JOIN #tmp5 T5 ON T5.LbPatientId = T3.LbPatientId AND T3.ClmDate = T5.ClmDate

SELECT LbPatientId, sum(TopProvider) as ProvCount
into #StillDups
FROM #tmp1
where TopProvider = 1
group by LbPatientId
HAVING SUM(TopProvider) > 1

--write PatientProvider where currently unattributed
UPDATE PP SET DeleteInd = 1, ModifyDateTime = GETDATE()
FROM vwPortal_PatientProvider PP
JOIN #tmp1 T ON PP.PatientID = T.LbPatientId
where PP.ProviderID = 0 and PP.DeleteInd = 0 and PP.AttributionTypeId = @AttributionTypeId

INSERT INTO vwPortal_PatientProvider
(PatientID, ProviderID, SourceSystemID, ExternalReferenceIdentifier, OtherReferenceIdentifier, DeleteInd, 
CreateDateTime, ModifyDateTime, CreateLBUserId, ModifyLBUserId, ProviderRoleTypeId, AttributionTypeId ) 
SELECT LbPatientId, MIN(ProviderId) as ProviderId, 1, BENE_HIC_NUM, RNDRG_PRVDR_NPI_NUM, 0,
getdate(), getdate(), 1, 1, 1, @AttributionTypeId
FROM #tmp1 where TopProvider = 1
group by LbPatientId,BENE_HIC_NUM, RNDRG_PRVDR_NPI_NUM

drop table #StillDups
DROP TABLE #tmp1
DROP TABLE #tpm2
DROP TABLE #tmp3
DROP TABLE #tmp4
DROP TABLE #tmp5
DROP TABLE #tmp3b
DROP TABLE #tpm2b
*/
END

GO
