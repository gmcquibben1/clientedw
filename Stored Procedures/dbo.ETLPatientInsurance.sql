SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ================================================================
-- Author:		Youping
-- Create date: 2016-06-29
-- Description:	Load Interface PatientInsurance Data into EDW
-- @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
-- @fullLoadFlag =0 Merge date to the existing table
-- ===============================================================
-- Modified Youping on 2016-07-20
-- Fix Invalid values for InsurancePriority 'P'=1, 'S'=2, 'T'=3 Blank=9
-- ===============================================================
-- Modified Youping on 2016-07-27
-- Add tracing log to [Maintenance].[dbo].[EdwProcedureRunLog]



Create PROCEDURE [dbo].[ETLPatientInsurance](@fullLoadFlag bit = 0)

AS
BEGIN	
 
    SET NOCOUNT ON;
	
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientInsurance';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientInsurance';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY
  		
			--set @fullLoadFlag =1
	
			IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] =@EDWName)
			  BEGIN
			  
					 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed

			  END

			IF @fullLoadFlag = 1
			  BEGIN
				UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				   SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME)
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
			  END

			DECLARE @lastDateTime DATETIME = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
											FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
											WHERE [EDWtableName] = @EDWtableName 
											AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	


			SELECT	  [InterfacePatientInsuranceId]
					  ,ISS.SourceSystemId
				      ,IP.PatientIdentifier
					  ,ISNULL([InsuranceName],'') [InsuranceName]
					  ,[FinancialClass]
					   ,ISNULL( try_convert(int,CASE WHEN ISNUMERIC([InsurancePriority])=1 then [InsurancePriority]
					   when [InsurancePriority]='P' then '1' when [InsurancePriority]='S' then '2' 
					   when [InsurancePriority]='T' then '3' 
					   when [InsurancePriority]='' then '9' else [InsurancePriority] end) ,'9')  [InsurancePriority]
					  ,[EffectiveDate]
					  ,[ExpirationDate]
					  ,[InsuranceType]
					  ,[PolicyNumber]
					  ,[Comment]
					  ,IPM.[CreateDateTime]
				
				INTO #PatientInsuranceQue 
	
					FROM [dbo].[vwPortal_InterfacePatientInsurance] IPM with (nolock)
					INNER JOIN [dbo].[vwPortal_InterfacePatient] IP with (nolock) ON IPM.InterfacePatientId = IP.InterfacePatientId
					INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) ON IPM.InterfaceSystemId = ISS.[InterfaceSystemId]
					WHERE IPM.CreateDateTime >= @lastDateTime 
					
				


              --- add Index
			  Create  NONCLUSTERED INDEX IDX_PatientIdentifier_SourceSystemId ON #PatientInsuranceQue (PatientIdentifier,SourceSystemId);
    
		

            -----   dedup data prepare for Merge
			 SELECT * 
			 INTO #PatientInsurance
			 FROM (
				SELECT  [InterfacePatientInsuranceId]
					  ,PA.SourceSystemId
				      ,idRef.LbPatientId as PatientId
					  ,[InsuranceName]
					  ,[FinancialClass]
					  ,[InsurancePriority]
					  ,[EffectiveDate]
					  ,[ExpirationDate]
					  ,[InsuranceType]
					  ,[PolicyNumber]
					  ,[Comment]
					  ,[CreateDateTime]
					, ROW_NUMBER() OVER ( PARTITION BY 	PA.SourceSystemId, idRef.LbPatientId, InsuranceName 
					ORDER BY 	InterfacePatientInsuranceId DESC ) RowNbr
					FROM  #PatientInsuranceQue PA with (nolock)
					 INNER JOIN dbo.PatientIdReference idRef with (nolock) ON idRef.SourceSystemId = PA.SourceSystemId 
					  AND idRef.ExternalId = PA.PatientIdentifier 
					  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
					  AND idRef.DeleteInd =0
					
				) A
				WHERE A.RowNbr=1

			--- Truncate data
			 IF (@fullLoadFlag=1)
			   BEGIN
				   Truncate table [dbo].[PatientInsurance];
			   END
	
	          			
			SELECT @RecordCountBefore=COUNT(1)
			FROM [dbo].[PatientInsurance];


			SELECT @RecordCountSource=COUNT(1)
			FROM #PatientInsurance;
			


			  CREATE CLUSTERED INDEX CIX_PatientID_Insurance ON #PatientInsurance(SourceSystemId, [PatientId],[InsuranceName]) ;

			  --- test select * from #PatientInsurance


				MERGE [DBO].PatientInsurance AS [target]
				USING #PatientInsurance AS [source] 

				ON (target.[SourceSystemID] = [source].[SourceSystemID] AND target.PatientID =[source].PatientID 
					 AND ISNULl(target.[InsuranceName],'') = [source].[InsuranceName] 
					)
         		WHEN MATCHED THEN UPDATE SET 
				[target].[FinancialClass] = source.[FinancialClass],
				[target].[InsurancePriority] = source.[InsurancePriority],
				[target].[EffectiveDate] = source.[EffectiveDate],
				[target].[ExpirationDate] = source.[ExpirationDate],
				[PolicyNumber] = source.[PolicyNumber],
				[target].[Comment] = source.[Comment],
				[target].[ModifyDateTime] = @Today ,
				[target].[ModifyLBUserId] = 1,
				[target].[CreateDateTime]=source.[CreateDateTime]
				WHEN NOT MATCHED THEN
				INSERT ([PatientId],[SourceSystemID],
					   [InsuranceName],[FinancialClass],[InsurancePriority],[EffectiveDate],[ExpirationDate],[PolicyNumber],[Comment],
					   [DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
				VALUES (source.[PatientId],source.[SourceSystemID],
					   source.[InsuranceName],source.[FinancialClass],source.[InsurancePriority],source.[EffectiveDate],source.[ExpirationDate],source.[PolicyNumber],source.[Comment],
					   0,source.[CreateDateTime],@Today ,1,1)
			;


	        -- update lastdate
               		   
			 -- update maxSourceTimeStamp with max CreateDateTime of #PatientInsuranceQue
			 UPDATE [Maintenance]..[EDWTableLoadTracking]
			 SET [maxSourceTimeStampProcessed] = (SELECT MAX(CreateDateTime) FROM #PatientInsuranceQue ),
				 [updateDateTime] =GETUTCDATE()
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


	
 
 		      -- get total records from PatientInsurance after merge 
		       			SELECT @RecordCountAfter=COUNT(1)
			            FROM PatientInsurance;

              -- Calculate records inserted and updated counts
	             SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	             SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			   SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	          

		
				 --clean
			  drop table #PatientInsurance,#PatientInsuranceQue
			

	END TRY

		BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH


END


GO
