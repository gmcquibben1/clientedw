SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLEdwAllergy] 
AS
BEGIN
	SET NOCOUNT ON

	-- TRIVIAL UPDATES FOR NULL COLUMNS
	UPDATE PatientAllergyProcessQueue SET EncounterId = 0 WHERE EncounterId IS NULL
	UPDATE PatientAllergyProcessQueue SET [AllergyDescription] = '' WHERE [AllergyDescription] IS NULL
	UPDATE PatientAllergyProcessQueue SET AllergyComment = '' WHERE AllergyComment IS NULL

	-- SCRUB OUTPUT ROW TO PROCESS
	;With CTE AS 
	(
		SELECT 
			ScrubbedRowNum,
			ROW_NUMBER() 
			OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
					InterfaceSystemId,LbPatientID, ISNULL(AllergyType,''), ISNULL(AllergyDescription,'')  --, AllergyReaction, ,AllergySeverity,   
					ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
					--LEN(ISNULL(AllergyComment,''))  +  -- THE BIGGER THE AllergyComment Text THE BETTER IT IS 
					--LEN(ISNULL(AllergyReaction,''))  + -- THE RECORD THE AllergyReaction Text THE BETTER IT IS   
					--LEN(ISNULL(AllergyType,''))  + -- THE RECORD THE AllergyType Text THE BETTER IT IS   
					--LEN(ISNULL(AllergySeverity,''))   -- THE RECORD THE AllergySeverity Text THE BETTER IT IS   
					InterfacePatientAllergyID
					DESC 
				) SNO
		FROM PatientAllergyProcessQueue
	)
	UPDATE CTE 
	SET ScrubbedRowNum = SNO;

	UPDATE  PatientAllergyProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

	CREATE TABLE #ResultsTemp 
	(
		MergeAction Varchar(20),
		OrigPatientID INT,
		PatientAllergyID INT, 
		PatientID INT
	)	

	INSERT INTO #ResultsTemp 
		(MergeAction,OrigPatientID,PatientAllergyID,PatientID)
	SELECT
		MergeAction,OrigPatientID,PatientAllergyID,PatientID
	FROM 
	(	
	MERGE [DBO].PatientAllergy AS target
	USING (	
			SELECT DISTINCT 
				LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,[AllergyTypeID],[AllergyDescription],[AllergySeverityTypeID],[AllergyReactionTypeID],
				[AllergyStatusTypeId],[AllergyComment],[EncounterID],1 As ActiveInd,[OnsetDate], 0 AS DeleteInd,
				GETUTCDATE() AS CreateDateTime,GETUTCDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId
			FROM [dbo].PatientAllergyProcessQueue PPQ
			LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
			WHERE ScrubbedRowNum = 1 
		  ) AS source 
		  ([OrigPatientId],[PatientID],[SourceSystemID],[AllergyTypeID],[AllergyDescription],[AllergySeverityTypeID],[AllergyReactionTypeID]
		  ,[AllergyStatusTypeId],[AllergyComment],[EncounterID],[ActiveInd],[OnsetDate]
		  ,[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
	ON ( target.[SourceSystemID] = source.[SourceSystemID] AND target.PatientID = source.PatientID 
		 AND ISNULL(target.AllergyTypeID,0) = ISNULL(source.AllergyTypeID,0) AND ISNULL(target.[AllergyDescription],'') = ISNULL(source.[AllergyDescription],'') 
		 --AND ISNULL(target.AllergyReactionTypeID,0) = ISNULL(source.AllergyReactionTypeID,0)  AND target.AllergyComment = source.AllergyComment
		 --AND ISNULL(target.AllergySeverityTypeID,0) = ISNULL(source.AllergySeverityTypeID,0)
		 )
	WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
		  --[PatientID]= source.[PatientID] ,
		  [SourceSystemID]= source.[SourceSystemID] ,
		  [AllergyTypeID]= source.[AllergyTypeID] ,
		  [AllergyDescription]= source.[AllergyDescription] ,
		  [AllergySeverityTypeID]= source.[AllergySeverityTypeID] ,
		  [AllergyReactionTypeID]= source.[AllergyReactionTypeID] ,
		  [AllergyStatusTypeId]= source.[AllergyStatusTypeId] ,
		  [AllergyComment]= source.[AllergyComment] ,
		  [EncounterID]= source.[EncounterID] ,
		  [ActiveInd]= source.[ActiveInd] ,
		  [OnsetDate] = source.[OnsetDate],
		  [DeleteInd]= source.[DeleteInd] ,
		  --[CreateDateTime]= source.[CreateDateTime] ,
		  [ModifyDateTime]= source.[ModifyDateTime] ,
		  --[CreateLBUserId]= source.[CreateLBUserId] ,
		  [ModifyLBUserId]= source.[ModifyLBUserId] 
	WHEN NOT MATCHED THEN
	INSERT ([PatientID],[SourceSystemID],[AllergyTypeID],[AllergyDescription],[AllergySeverityTypeID],[AllergyReactionTypeID]
		  ,[AllergyStatusTypeId],[AllergyComment],[EncounterID],[ActiveInd],[OnsetDate]
		  ,[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId]
		   )
	VALUES (source.[PatientID],source.[SourceSystemID],source.[AllergyTypeID],source.[AllergyDescription],source.[AllergySeverityTypeID],source.[AllergyReactionTypeID]
		  ,source.[AllergyStatusTypeId],source.[AllergyComment],source.[EncounterID],source.[ActiveInd],source.[OnsetDate]
		  ,source.[DeleteInd],source.[CreateDateTime],source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId])
	OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientAllergyID,INSERTED.PatientAllergyID) PatientAllergyID, source.PatientID
	) x ;

	IF OBJECT_ID('PatientAllergy_PICT') IS NOT NULL
	BEGIN
		INSERT INTO PatientAllergy_PICT(PatientAllergyID,PatientId,SwitchDirectionId)
		SELECT RT.PatientAllergyID, OrigPatientID, 0 
		FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
	END

	-- END ZONE UPDATES AND TIMESTAMPING 
	UPDATE  PatientAllergyProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

	INSERT INTO PatientFeedTimestamps 
		(PatientId,FeedType,FeedSubType,FeedTimestamp)
	SELECT DISTINCT 
		LbPatientID, NULL, 'Allergy',GetDate()
	FROM PatientAllergyProcessQueue
	WHERE ProcessedInd = 1 

	UPDATE A 
	SET HoldingReleaseInd = 1 
	FROM [dbo].[PatientAllergyProcessQueue_HoldingBay] A 
		INNER JOIN PatientAllergyProcessQueue B ON A.InterfacePatientAllergyID = B.InterfacePatientAllergyID
	WHERE B.ProcessedInd = 1 

	INSERT INTO[dbo].[PatientAllergyProcessQueue_HoldingBay]
		([InterfacePatientAllergyID],[InterfacePatientID],[InterfaceSystemId]
		,[AllergyType],[AllergyTypeId],[AllergyDescription]
		,[AllergySeverity],[AllergySeverityTypeId],[AllergyReaction]
		,[AllergyReactionTypeId],[AllergyStatusTypeId],[AllergyComment]
		,[OnsetDate],[CreateDateTime],[EncounterId]
		,[LbPatientId],[InterfacePatientAllergyID_HoldingOverride],[InterfacePatientID_HoldingOverride]
		,[InterfaceSystemId_HoldingOverride],[AllergyType_HoldingOverride],[AllergyTypeId_HoldingOverride]
		,[AllergyDescription_HoldingOverride],[AllergySeverity_HoldingOverride],[AllergySeverityTypeId_HoldingOverride]
		,[AllergyReaction_HoldingOverride],[AllergyReactionTypeId_HoldingOverride],[AllergyStatusTypeId_HoldingOverride]
		,[AllergyComment_HoldingOverride],[OnsetDate_HoldingOverride],[CreateDateTime_HoldingOverride]
		,[EncounterId_HoldingOverride],[LbPatientId_HoldingOverride],[HoldingStartDate]
		,[HoldingStartReason])
	 SELECT 
		 A.[InterfacePatientAllergyID],A.[InterfacePatientID],A.[InterfaceSystemId]
		,A.[AllergyType],A.[AllergyTypeId],A.[AllergyDescription]
		,A.[AllergySeverity],A.[AllergySeverityTypeId],A.[AllergyReaction]
		,A.[AllergyReactionTypeId],A.[AllergyStatusTypeId],A.[AllergyComment]
		,A.[OnsetDate],A.[CreateDateTime],A.[EncounterId]
		,A.[LbPatientId],A.[InterfacePatientAllergyID],A.[InterfacePatientID]
		,A.[InterfaceSystemId],A.[AllergyType],A.[AllergyTypeId]
		,A.[AllergyDescription],A.[AllergySeverity],A.[AllergySeverityTypeId]
		,A.[AllergyReaction],A.[AllergyReactionTypeId],A.[AllergyStatusTypeId]
		,A.[AllergyComment],A.[OnsetDate],A.[CreateDateTime]
		,A.[EncounterId],A.[LbPatientId],GetDate()
	,'Duplicacy'
	FROM PatientAllergyProcessQueue  A 
		LEFT JOIN [dbo].[PatientAllergyProcessQueue_HoldingBay] B ON A.InterfacePatientAllergyID = B.InterfacePatientAllergyID
	WHERE ISNULL(A.ProcessedInd,0) <> 1 AND B.InterfacePatientAllergyID IS NULL 

END


GO
