SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FacesheetPastERAdmissionLoad]
	@PatientId int
AS
BEGIN
	/*IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '#TempPastAdmin')
	BEGIN
		CREATE TABLE #TempPastAdmin (Facility varchar(50), ReasonforVisit varchar(50), DateofVisit datetime)

		INSERT INTO #TempPastAdmin (Facility, ReasonforVisit, DateofVisit)
		VALUES ('Test Location 1', 'Test Reason 1', getdate())

		INSERT INTO #TempPastAdmin (Facility, ReasonforVisit, DateofVisit)
		VALUES ('Test Location 2', 'Test Reason 1', getdate())

		INSERT INTO #TempPastAdmin (Facility, ReasonforVisit, DateofVisit)
		VALUES ('Test Location 3', 'Test Reason 1', getdate())

	END
	*/

	--Need to pull most recent records in the past 12 Months in descending order.
	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '#TempPastAdmin')
	BEGIN
		CREATE TABLE #TempPastAdmin (LbPatientId int, PatientFirstName varchar(100), PatientLastName varchar(100), Facility varchar(250), ER_Full_Ind varchar(5), 
		ReasonforVisit varchar(250), DateofVisit date)

		INSERT INTO #TempPastAdmin (LbPatientId, PatientFirstName, PatientLastName, Facility, ER_Full_Ind, ReasonforVisit, DateofVisit)
			SELECT 
			A.LbPatientId, vwpi.FirstName, vwpi.LastName, ISNULL(NPI.NAME, 'Unknown Facility'), 
			(CASE WHEN R.ER_INP = 1 THEN 'Both'
				WHEN R.ER = 1 AND R.INP = 0 THEN 'FA'
				WHEN R.ER = 0 AND R.INP = 1 THEN 'ER'
			END), DC.DiagnosisDescription, A.CLM_FROM_DT
			FROM CCLF_0_PartA_CLM_TYPES R
			JOIN CCLF_1_PartA_Header A ON R.CUR_CLM_UNIQ_ID = A.CUR_CLM_UNIQ_ID
			LEFT JOIN NPI_Lookup NPI ON A.FAC_PRVDR_NPI_NUM = NPI.NPI
			JOIN DiagnosisCode DC ON A.PRNCPL_DGNS_CD = DC.DiagnosisCodE
			JOIN vwPortal_Patient vwp on A.lbPatientId = vwp.PatientId
			JOIN vwPortal_IndividualName vwpi on vwp.IndividualId = vwpi.IndividualId
			WHERE A.LbPatientId = @PatientId AND (R.ER = 1 OR R.INP = 1 OR R.ER_INP = 1) --AND NPI.Name <> '' AND NPI.Name IS NOT NULL

	END
	
	
		SELECT DISTINCT Facility AS Location, ReasonforVisit AS ReasonforVisit, ER_Full_ind AS ERFAInd, DateofVisit AS Date
		FROM #TempPastAdmin
		ORDER BY Date DESC

END

GO
