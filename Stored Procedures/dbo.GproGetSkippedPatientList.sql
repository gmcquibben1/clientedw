SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproGetSkippedPatientList]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2016-10-24
	INITIAL VER:	2.2.1
	MODULE:			GPRO		
	DESCRIPTION:	For the measure return the list of patients that are skipped
	PARAMETERS:		GPro measure name 
	RETURN VALUE(s)/OUTPUT:	a table containing the patient id, rank, and gpro ids of patients ranked for this measure that are skipped
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-10-24		BR			LBPP-2026	Initial version 
	2.2.1		2016-11-09		BR			LBPP-2026	Added additional column to enable the patient-level status checking
														If the parameter is NULL it includes all measures
	2.2.1		2016-12-01		BR			LBPP-2028	Updated the PREV-13 calculation to include the additional fields added
	2.2.1		2016-12-19		BR			LBPP-2143	Fixed a bug in DM-2 skip calculation
	2.2.1		2017-01-13		BR						Removed the patient-level status check for colorectal 
	2.2.1		2017-01-16		BR			LBPP-2213	Fixed an issue with HF-6 confirmed, switched to skip if <> 2.  Don't check LVSD for skips
	2.2.1		2017-01-24		BR						Fixed the check for patients who are skipped at the patient level (CAD-7)
	2.2.1		2017-02-07		BR			LBPP-2213	Undoing LBPP-2213, conflicting documentation, these should be skipped
	2.2.1		2017-04-20		BR			LBPP-2467	Changed the way PREV-13 is counted to accurately match the CMS reports
=================================================================*/
(
	@GproMeasureName VARCHAR(20) = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

	-----------------------------------------------------------------------
	-- define variables
	-----------------------------------------------------------------------
	CREATE TABLE #skippedList
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		--UNIQUE CLUSTERED (LbPatientId)
	);

	-----------------------------------------------------------------------
	-- CARE-2
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'CARE-2' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.CareFallsRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'CARE-2'
		WHERE gpr.CareFallsRank IS NOT NULL 
			AND ((gpm.[MedicalRecordFound] IS NOT NULL AND gpm.[MedicalRecordFound] <> 2)
				OR (gpm.[MedicalRecordFound] = 2 AND gpm.CareFallsConfirmed IS NOT NULL AND gpm.CareFallsConfirmed <> 2))
	END

	-----------------------------------------------------------------------
	-- CARE-3
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'CARE-3' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.CareMeddocRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'CARE-3'
		WHERE gpr.CareMeddocRank IS NOT NULL 
			AND ((gpm.[MedicalRecordFound] IS NOT NULL AND gpm.[MedicalRecordFound] <> 2)
				OR (gpm.[MedicalRecordFound] = 2 AND gpm.CareMeddocConfirmed IS NOT NULL AND gpm.CareMeddocConfirmed <> 2)
				OR (gpm.[MedicalRecordFound] = 2 AND gpm.CareMeddocConfirmed = 2
					AND EXISTS (SELECT TOP 1 1 FROM GproPatientMedication med WHERE med.GproPatientMeasureId = gpm.GproPatientMeasureId)
					AND NOT EXISTS (SELECT TOP 1 1 FROM GproPatientMedication med WHERE med.GproPatientMeasureId = gpm.GproPatientMeasureId AND med.CareMeddocVisitConfirmed IS NULL)
					AND NOT EXISTS (SELECT TOP 1 1 FROM GproPatientMedication med WHERE med.GproPatientMeasureId = gpm.GproPatientMeasureId AND med.CareMeddocVisitConfirmed = 2))
				)
	END

	-----------------------------------------------------------------------
	-- CAD-7
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'CAD-7' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.CadRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'CAD-7'
		WHERE gpr.CadRank IS NOT NULL 
			AND ((gpm.MedicalRecordFound IS NOT NULL AND gpm.MedicalRecordFound <> 2)
				OR (gpm.MedicalRecordFound = 2 AND gpm.CadConfirmed IS NOT NULL AND gpm.CadConfirmed <> 2)
				OR (gpm.[MedicalRecordFound] = 2 AND gpm.[CadConfirmed] = 2 AND gpm.[CadDiabetesLvsd] = 1))
	END

	-----------------------------------------------------------------------
	-- DM-2
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'DM-2' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.DmRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'DM-2'
		WHERE gpr.DmRank IS NOT NULL 
			AND ((gpm.MedicalRecordFound IS NOT NULL AND gpm.MedicalRecordFound <> 2)
				OR (gpm.MedicalRecordFound = 2 AND gpm.DmConfirmed IS NOT NULL AND gpm.DmConfirmed <> 2))
	END

	-----------------------------------------------------------------------
	-- DM-7
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'DM-7' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.DmRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'DM-7'
		WHERE gpr.DmRank IS NOT NULL 
			AND ((gpm.MedicalRecordFound IS NOT NULL AND gpm.MedicalRecordFound <> 2)
				OR (gpm.MedicalRecordFound = 2 AND gpm.DmConfirmed IS NOT NULL AND gpm.DmConfirmed <> 2))
	END

	-----------------------------------------------------------------------
	-- HF-6
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'HF-6' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.HfRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'HF-6'
		WHERE gpr.HfRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2)
				OR ([MedicalRecordFound] = 2 AND [HfConfirmed] IS NOT NULL AND [HfConfirmed] <> 2)
				OR ([MedicalRecordFound] = 2 AND [HfConfirmed] IS NOT NULL AND [HfConfirmed] = 2 AND [HfLvsd] IS NOT NULL AND [HfLvsd] = 1))
	END

	-----------------------------------------------------------------------
	-- HTN-2
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'HTN-2' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.HtnRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'HTN-2'
		WHERE gpr.HtnRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2)
				OR ([MedicalRecordFound] = 2 AND [HtnConfirmed] IN (8, 15, 17, 19)) )
	END

	-----------------------------------------------------------------------
	-- IVD-2
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'IVD-2' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.IvdRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'IVD-2'
		WHERE gpr.IvdRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [IvdConfirmed] IN (8, 15, 19)) 
				)
	END

	-----------------------------------------------------------------------
	-- MH-1
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'MH-1' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.MhRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'MH-1'
		WHERE gpr.MhRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [MhConfirmed] <> 2)
				OR ([MedicalRecordFound] = 2 AND [MhConfirmed] = 2 AND [MhIndexPerformed] = 1 )
				OR ([MedicalRecordFound] = 2 AND [MhConfirmed] = 2 AND [MhIndexPerformed] = 2 AND [MhIndexTest] = 1 ) 
				)
	END

	-----------------------------------------------------------------------
	-- PREV-5
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-5' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcMammogramRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-5'
		WHERE gpr.PcMammogramRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [PcMammogramConfirmed] IS NOT NULL AND [PcMammogramConfirmed] <> 2) )
	END

	-----------------------------------------------------------------------
	-- PREV-6
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-6' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcColorectalRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-6'
			INNER JOIN GproStatusType gst ON gst.GproStatusTypeId = gpr.GproStatusTypeId
		WHERE gpr.PcColorectalRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2) 
				OR ([MedicalRecordFound] = 2 AND [PcColorectalConfirmed] IS NOT NULL AND [PcColorectalConfirmed] IN (15,17,19)) )
	END

	-----------------------------------------------------------------------
	-- PREV-7
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-7' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcFluShotRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-7'
		WHERE gpr.PcFluShotRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [PcFluShotConfirmed] IS NOT NULL AND [PcFluShotConfirmed] IN (15,17,19)) )
	END

	-----------------------------------------------------------------------
	-- PREV-8
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-8' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcPneumoshotRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-8'
		WHERE gpr.PcPneumoShotRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [PcPneumoShotConfirmed] IS NOT NULL AND [PcPneumoShotConfirmed] <> 2) )
	END

	-----------------------------------------------------------------------
	-- PREV-9
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-9' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcBmiscreenRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-9'
		WHERE gpr.PcBmiScreenRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [PcBmiScreenConfirmed] IS NOT NULL AND [PcBmiScreenConfirmed] <> 2) )
	END

	-----------------------------------------------------------------------
	-- PREV-10
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-10' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcTobaccouseRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-10'
		WHERE gpr.PcTobaccoUseRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [PcTobaccoConfirmed] IS NOT NULL AND [PcTobaccoConfirmed] <> 2) )
	END

	-----------------------------------------------------------------------
	-- PREV-11
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-11' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcBloodPressureRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-11'
		WHERE gpr.PcBloodPressureRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [PcBloodPressureConfirmed] IS NOT NULL AND [PcBloodPressureConfirmed] <> 2) )
	END

	-----------------------------------------------------------------------
	-- PREV-12
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-12' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcDepressionRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-12'
		WHERE gpr.PcDepressionRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [PcDepressionConfirmed] IS NOT NULL AND [PcDepressionConfirmed] <> 2) )
	END

	-----------------------------------------------------------------------
	-- PREV-13
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-13' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #skippedList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcStatinRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-13'
		WHERE gpr.PcStatinRank IS NOT NULL 
			AND (([MedicalRecordFound] IS NOT NULL AND [MedicalRecordFound] <> 2 )
				OR ([MedicalRecordFound] = 2 AND [PcStatinAscvd] IS NOT NULL AND [PcStatinAscvd] NOT IN (2,23)) 
				OR ([MedicalRecordFound] = 2 AND [PcStatinAscvd] = 23 AND [PcStatinLdlc] IS NOT NULL AND [PcStatinLdlc] NOT IN (1,2,19))
				OR ([MedicalRecordFound] = 2 AND [PcStatinAscvd] = 23 AND [PcStatinLdlc] = 1 AND [PcStatinDiabetes] IS NOT NULL AND [PcStatinDiabetes] <> 2)
				OR ([MedicalRecordFound] = 2 AND [PcStatinAscvd] = 23 AND [PcStatinLdlc] = 1 AND [PcStatinDiabetes] = 2 AND [PcStatinLdlcReading] IS NOT NULL AND [PcStatinLdlcReading] <> 2)
				)
	END

	-----------------------------------------------------------------------
	-- Return the list of patients
	-----------------------------------------------------------------------
	SELECT LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId FROM #skippedList
END

GO
