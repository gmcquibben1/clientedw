SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FaceSheetNotesLoad] 
	@PatientId INTEGER
AS
BEGIN
	--This temp table will hold all of the rows to be displayed.
	DECLARE  @FacesheetNotes TABLE 
	(
		NoteName			VARCHAR(100)
		--,NoteDate			DATE NULL
	)	
	-- get last comprehensive exam
	IF EXISTS (SELECT 1 FROM dbo.vwPatientDiagnosis 
		WHERE (dbo.vwPatientDiagnosis.DiagnosisCode IN ('V70.0','V72.31','Z00.00','Z00.01','Z01.419') 
				OR dbo.vwPatientDiagnosis.DiagnosisCodeRaw IN ('V70.0','V72.31','Z00.00','Z00.01','Z01.419')
				OR dbo.vwPatientDiagnosis.DiagnosisCodeDisplay IN ('V70.0','V72.31','Z00.00','Z00.01','Z01.419')
				)
			AND dbo.vwPatientDiagnosis.PatientID = @PatientId )
	BEGIN
		INSERT INTO @FacesheetNotes (NoteName)
		SELECT TOP 1
			'Last Complete Exam - ' + CONVERT(VARCHAR(20),vwPatientDiagnosis.DiagnosisDateTime , 101) AS NoteName
			--vwPatientDiagnosis.DiagnosisDateTime AS NoteDate
		FROM dbo.vwPatientDiagnosis 
		WHERE (dbo.vwPatientDiagnosis.DiagnosisCode IN ('V70.0','V72.31','Z00.00','Z00.01','Z01.419') 
				OR dbo.vwPatientDiagnosis.DiagnosisCodeRaw IN ('V70.0','V72.31','Z00.00','Z00.01','Z01.419')
				OR dbo.vwPatientDiagnosis.DiagnosisCodeDisplay IN ('V70.0','V72.31','Z00.00','Z00.01','Z01.419')
				)
			AND dbo.vwPatientDiagnosis.PatientID = @PatientId
		ORDER BY dbo.vwPatientDiagnosis.DiagnosisDateTime DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetNotes (NoteName)
		VALUES ('Last Complete Exam');
	END
	--now spit out whatever is in the temp table
	SELECT NoteName FROM @FacesheetNotes;
END
GO
