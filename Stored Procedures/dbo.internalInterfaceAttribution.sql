SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =====================================================================================
-- Author:		Mike Hoxter
-- Create date: 2015-12-18
-- Description:	set patient provider attribution from interfacePatientAttribution data
-- Modified By: Lan Ma
-- Modified Date: 2016-04-05
-- Modified By: Youping
-- Modified Date: 2016-04-21  add LbPatientId IS NOT NULL
-- ======================================================================================
/*
	Version		Date		Author	Change
	-------		----------	------	------	
	2.2.1       2017-01-19  YL      LBETL-525 Add index to speed process
*/

CREATE PROCEDURE [dbo].[internalInterfaceAttribution] 
AS
BEGIN

	DECLARE @AttributionTypeId INT = 3;

	SELECT [InterfaceSystemId],[PatientIdentifier], MAX(CreateDateTime) AS [MaxCreateDateTime] 
	INTO #KeepList
	FROM vwPortal_InterfacePatientAttribution
	GROUP BY [InterfaceSystemId],[PatientIdentifier] 

--add index YL 2017-01-19
	CREATE NONCLUSTERED INDEX [IX_PatientIDentifierID_InterfaceSystem_CreateDate] ON #KeepList
(
	[InterfaceSystemId] ASC,
	[PatientIdentifier] ASC
)
INCLUDE ( 	[MaxCreateDateTime]);



	INSERT INTO [dbo].[vwPortal_PatientProvider]
        ([PatientID]
        ,[ProviderID]
        ,[SourceSystemID]
        ,[ExternalReferenceIdentifier]
        ,[OtherReferenceIdentifier]
        ,[ProviderRoleTypeID]
        ,[DeleteInd]
        ,[CreateDateTime]
        ,[ModifyDateTime]
        ,[CreateLBUserId]
        ,[ModifyLBUserId]
        ,[AttributionTypeId]
		)
	SELECT DISTINCT 
		[LbPatientId] AS PatientID 
		,ProviderID  
		,t3.SourceSystemID 
		,'|' + cast(t1.[InterfaceSystemId] AS varchar(5)) + '|' +  t1.PatientIdentifier + '|' AS [ExternalReferenceIdentifier]
		,[ProviderNPI]
		,1 [ProviderRoleTypeID]
		,0 [DeleteInd]
		,getdate() [CreateDateTime]
		,getdate() [ModifyDateTime]
		,1 [CreateLBUserId]
		,1 [ModifyLBUserId]
		,3 [AttributionTypeId]
	FROM vwPortal_InterfacePatientAttribution t1
	JOIN #KeepList t2 
	ON t1.[PatientIdentifier] = t2.[PatientIdentifier]
	AND t1.[InterfaceSystemId] = t2.[InterfaceSystemId]
	AND t1.CreateDateTime = t2.MaxCreateDateTime
	JOIN [vwPortal_InterfaceSystem] t3 
	on t1.InterfaceSystemId = t3.InterfaceSystemId
	JOIN PatientIdReference t4 
	ON t1.PatientIdentifier = t4.ExternalId 
	AND t3.SourceSystemID = t4.SourceSystemID
	AND t4.IdTypeDesc = 'InterfacePatientIdentifier'
	AND t4.DeleteInd = 0
	JOIN vwPortal_ProviderActive t5 
	ON t1.ProviderNPI = t5.NationalProviderIdentifier
	WHERE [LbPatientId] IS NOT NULL


/*--original code for reference
SELECT DISTINCT InterfaceSystemId, InterfacePatientId, MAX(CreateDateTime) AS [MaxCreateDateTime] 
INTO #KeepList
FROM vwPortal_InterfacePatientAttribution
GROUP BY InterfaceSystemId, InterfacePatientId

SELECT DISTINCT 
t1.[InterfaceSystemId]
,t1.[PatientIdentifier]
,t1.[InterfacePatientId], 
[ProviderNPI] as PrimaryCareProviderNPI
, intsys.SourceSystemId
, IDR.LbPatientId AS PatientId, 
 PP.ProviderID
--INTO #patientList
INTO #patientProvider
from [dbo].[vwPortal_InterfacePatient] t1
LEFT JOIN [vwPortal_InterfaceSystem] intsys on t1.InterfaceSystemId = intsys.InterfaceSystemId
join vwPortal_InterfacePatientAttribution IPA ON t1.InterfacePatientId = IPA.InterfacePatientId 
                                              and t1.InterfaceSystemId = IPA.InterfaceSystemId
JOIN vwPortal_Provider PP ON IPA.ProviderNPI = PP.NationalProviderIdentifier and PP.DeleteInd = 0
join #KeepList KL ON ISNULL(IPA.InterfaceSystemId,0) = ISNULL(KL.InterfaceSystemId,0) 
                  AND IPA.InterfacePatientId = KL.InterfacePatientId
                  AND IPA.CreateDateTime = KL.MaxCreateDateTime
JOIN vwPortal_InterfacePatientIdReference IDR ON IPA.InterfacePatientId = IDR.InterfacePatientId
join vwPortal_Patient pat on IDR.LbPatientId = pat.PatientID and pat.DeleteInd = 0


--remove this update statement, all the rows will be insert into patientProvider table in every run
/*
UPDATE vwPortal_PatientProvider
SET DeleteInd = 1, ModifyDateTime = GETDATE()
FROM vwPortal_PatientProvider PP
JOIN #patientProvider PPR ON PP.PatientID = PPR.PatientID and ISNULL(PPR.SourceSystemID,0) = ISNULL(PP.SourceSystemID,0)
WHERE PPR.ProviderID <> PP.ProviderID AND PP.AttributionTypeId = @AttributionTypeId
      and PP.DeleteInd = 0
*/

INSERT INTO vwPortal_PatientProvider (
  PatientID
  ,ProviderID
  ,SourceSystemID
  ,ExternalReferenceIdentifier
  ,OtherReferenceIdentifier
  ,ProviderRoleTypeID
  ,DeleteInd
  ,CreateDateTime
  ,ModifyDateTime
  ,CreateLBUserId
  ,ModifyLBUserId
  ,OverrideInd
  ,AttributionTypeId
  ,AttributedProviderInd
)
select distinct PPR.PatientID, PPR.ProviderID, PPR.SourceSystemID, PPR.[PatientIdentifier], PPR.PrimaryCareProviderNPI,1,0, GETDATE(), GETDATE(), 1,1, 0, 3, 0 
FROM #patientProvider PPR
--LEFT JOIN vwPortal_PatientProvider PP ON PPR.PatientID = PP.PatientID and ISNULL(PPR.SourceSystemID,0) = ISNULL(PP.SourceSystemID,0)
--      and PP.AttributionTypeId = 3 --@AttributionTypeId
--      and PP.DeleteInd = 0
--where PP.PatientProviderID IS NULL

drop table #patientProvider
*/
drop table #KeepList

END
GO
