SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalMakeCCLFWellness]
AS

/*
	Version		Date		Author	Change
	-------		----------	------	------	
        2.2.1           2017-02-07  YL      LBETL-426 Not to use table PatientProcedureProcedureCode
*/

BEGIN


SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'CCLF_WELLNESS_VISITS';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalMakeCCLFWellness';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;
  DECLARE @ForceInNetwork bit = 0;
	DECLARE @Today DATE=GETUTCDATE();




BEGIN TRY 

--RETRIEVE IN NETWORK SETTINGS VALUE
IF EXISTS (SELECT 1 FROM vwPortal_SystemSettings WHERE SettingType = 'AnalyticsOptions' AND SettingParameter = 'WellnessInNetwork' AND SetValue = '1')
BEGIN
SET @ForceInNetwork = 1
END
	--CLEAR WELLNESS VISITS TABLE
	TRUNCATE TABLE CCLF_WELLNESS_VISITS


	--ENTER PART-A WELLNESS CLAIMS
	INSERT INTO CCLF_WELLNESS_VISITS (
	   CLAIM_FEED
	  ,CLM_LINE_REV_CTR_CD
	  ,CLM_LINE_HCPCS_CD
	  ,CLM_DGNS_CD
	  ,LbPatientId
	  ,CLM_FROM_DT
	--  ,CUR_CLM_UNIQ_ID
	  ,FACILITY_NPI
	  ,PROV_NPI
	  ,Specialty_CD, IN_NETWORK
	)
	SELECT 
	DISTINCT 
	--TOP 100
	'Part-A Claims' as CLAIM_FEED,
	CLM_LINE_REV_CTR_CD, 
	CLM_LINE_HCPCS_CD,
	 CCLF1.PRNCPL_DGNS_CD,
	CCLF1.LbPatientId, 
	CCLF1.CLM_FROM_DT, 
	--CCLF1.CUR_CLM_UNIQ_ID, 
	 CCLF1.FAC_PRVDR_NPI_NUM AS FACILITY_NPI, 
	 CCLF1.ATNDG_PRVDR_NPI_NUM AS PROV_NPI,
	 npi.Specialty,0								--LBAN-2284
	--count((CCLF2.CUR_CLM_UNIQ_ID))
	FROM CCLF_2_PartA_RCDetail CCLF2
	JOIN CCLF_1_PartA_Header CCLF1 ON CCLF2.CLM_FROM_DT = CCLF1.CLM_FROM_DT 
		AND CCLF2.CUR_CLM_UNIQ_ID = CCLF1.CUR_CLM_UNIQ_ID 
		and TRY_Convert(INT,ISNULL(CCLF1.CLM_ADJSMT_TYPE_CD,0)) = 0
	left join NPI_Lookup npi on CCLF1.ATNDG_PRVDR_NPI_NUM = npi.npi
	WHERE CLM_LINE_REV_CTR_CD IN (
	SELECT code FROM RefTable_Wellness WHERE field = 'revcode') 
	OR
	CLM_LINE_HCPCS_CD IN (
	SELECT code FROM RefTable_Wellness WHERE field = 'hcpc') 


	--INSERT PART-B CLAIMS
	INSERT INTO CCLF_WELLNESS_VISITS (
	   CLAIM_FEED
	  ,CLM_LINE_HCPCS_CD
	  ,CLM_DGNS_CD
	  ,LbPatientId
	  ,CLM_FROM_DT
	--  ,CUR_CLM_UNIQ_ID
	  ,FACILITY_TIN
	  ,PROV_NPI
	  ,Specialty_CD, IN_NETWORK
	)
	SELECT DISTINCT -- group by natural key & exclude where charge amount <=0
--TOP 100
	'Part-B Claims' as CLAIM_FEED, 
	CLM_LINE_HCPCS_CD, 
	CLM_LINE_DGNS_CD,
	LbPatientId, 
	CLM_FROM_DT, 
-- CUR_CLM_UNIQ_ID, 
	CLM_RNDRG_PRVDR_TAX_NUM, 
	RNDRG_PRVDR_NPI_NUM
	,npi.Specialty, 0	
	FROM CCLF_5_PartB_Physicians B
	JOIN RefTable_Wellness W ON B.CLM_LINE_HCPCS_CD = W.code and field = 'hcpc'
	left join NPI_Lookup npi on B.RNDRG_PRVDR_NPI_NUM = npi.npi
	GROUP BY CLM_LINE_HCPCS_CD, CLM_LINE_DGNS_CD, LbPatientId, CLM_FROM_DT, CLM_RNDRG_PRVDR_TAX_NUM, RNDRG_PRVDR_NPI_NUM,npi.Specialty	
	  HAVING sum(CLM_LINE_ALOWD_CHRG_AMT) > 0


  UPDATE CCLF_WELLNESS_VISITS SET 
	 IN_NETWORK = 1 -- int
	 WHERE PROV_NPI IN (
	 select NationalProviderIdentifier FROM vwPortal_ProviderActive)
	 OR FACILITY_NPI IN (
	 select NationalProviderIdentifier FROM vwPortal_ProviderActive)
	 OR FACILITY_TIN IN (
	 SELECT TIN FROM ProviderExtract)


	INSERT INTO CCLF_WELLNESS_VISITS (
	   CLAIM_FEED
	  ,CLM_LINE_HCPCS_CD
	  ,CLM_DGNS_CD
	  ,LbPatientId
	  ,CLM_FROM_DT
	--  ,CUR_CLM_UNIQ_ID
	  ,FACILITY_TIN
	  ,PROV_NPI, IN_NETWORK
	  ,Specialty_CD
	)
	SELECT DISTINCT
	ISNULL(SS.[Name],'EMR Data'), PC.ProcedureCode, 'V70', pp.PatientId, PP.ProcedureDateTime, '9999999999',  
	 CASE 
	  WHEN (ISNUMERIC(PerformedByProviderID) = 1 AND LEN(PerformedByProviderID) = 10) THEN PerformedByProviderID
	  WHEN (ISNUMERIC(PerformedByClinician) = 1 AND LEN(PerformedByClinician) = 10) THEN PerformedByClinician 
	  ELSE 9999999999
	 END
	, 1
	,w.Specialty_CD								--LBAN-2284
	FROM PatientProcedure PP
	--JOIN PatientProcedureProcedureCode PPPC ON PPPC.PatientProcedureId = PP.PatientProcedureId
	JOIN ProcedureCode PC ON PP.ProcedureCodeId = PC.ProcedureCodeId
	JOIN SourceSystem SS ON PP.SourceSystemID = SS.SourceSystemId and SS.ClinicalInd = 1
	LEFT JOIN CCLF_WELLNESS_VISITS W ON PP.PatientID = W.LbPatientId AND W.IN_NETWORK = 1 
                                    AND PP.ProcedureDateTime = W.CLM_FROM_DT
                                    AND PC.ProcedureCode = W.CLM_LINE_HCPCS_CD
	WHERE W.id IS NULL AND PC.ProcedureCode IN (
	SELECT code FROM RefTable_Wellness) 

	--SELECT COUNT(*) AS TOTAL, SUM(IN_NETWORK) AS In_TIN FROM CCLF_WELLNESS_VISITS

--*******************************************************************************
--TRUNCATE WELLNESS AGING TABLE
TRUNCATE TABLE CCLF_WELLNESS_AGING



IF (@ForceInNetwork = 1) 
BEGIN

INSERT INTO CCLF_WELLNESS_AGING
 (
   LbPatientId
  --,DAYS_SINCE_VISIT
  ,LAST_DOS
)
SELECT 
--TOP 100
LbPatientId, 
MAX(CLM_FROM_DT) AS LAST_DOS
FROM CCLF_WELLNESS_VISITS
WHERE IN_NETWORK = 1
GROUP BY LbPatientId


--UPDATE TABLE W/ WELLNESS VISIT INSTEAD OF PCS VISIT
UPDATE CCLF_WELLNESS_AGING SET
  LAST_WELLNESS_DOS = (SELECT MAX(CCLF_WELLNESS_VISITS.CLM_FROM_DT) 
  FROM CCLF_WELLNESS_VISITS 
WHERE IN_NETWORK = 1 
AND CCLF_WELLNESS_VISITS.CLM_LINE_HCPCS_CD in ('G0438','G0439') --and CCLF_WELLNESS_VISITS.CLM_DGNS_CD LIKE 'V7%'
AND CCLF_WELLNESS_VISITS.LbPatientId = CCLF_WELLNESS_AGING.LbPatientId)

END
IF (@ForceInNetwork = 0) 
BEGIN

INSERT INTO CCLF_WELLNESS_AGING
 (
   LbPatientId
  --,DAYS_SINCE_VISIT
  ,LAST_DOS
)
SELECT 
LbPatientId, 
MAX(CLM_FROM_DT) AS LAST_DOS
FROM CCLF_WELLNESS_VISITS
GROUP BY LbPatientId


--UPDATE TABLE W/ WELLNESS VISIT INSTEAD OF PCS VISIT
UPDATE CCLF_WELLNESS_AGING SET
  LAST_WELLNESS_DOS = (SELECT MAX(CCLF_WELLNESS_VISITS.CLM_FROM_DT) 
  FROM CCLF_WELLNESS_VISITS 
WHERE CCLF_WELLNESS_VISITS.CLM_LINE_HCPCS_CD in ('G0438','G0439') --and CCLF_WELLNESS_VISITS.CLM_DGNS_CD LIKE 'V7%'
AND CCLF_WELLNESS_VISITS.LbPatientId = CCLF_WELLNESS_AGING.LbPatientId)


END
  
  --UPDATE THE 'DAYS SINCE VISIT' VALUE
UPDATE CCLF_WELLNESS_AGING SET
  DAYS_SINCE_VISIT = DATEDIFF(day, LAST_DOS, getdate()), -- int
  DAYS_SINCE_WELLNESS = DATEDIFF(day, LAST_WELLNESS_DOS, getdate())
  
--INSERT ALL MISSING PATIENTS W/ NULL DATES
INSERT INTO CCLF_WELLNESS_AGING (
   LbPatientId
  ,DAYS_SINCE_VISIT
  ,LAST_DOS
  ,LAST_WELLNESS_DOS
  ,DAYS_SINCE_WELLNESS
) SELECT H.LbPatientId, 999, NULL, NULL, 999 
FROM OrgHierarchy H 
LEFT JOIN CCLF_WELLNESS_AGING A ON A.LbPatientId = H.LbPatientId
WHERE A.LbPatientId IS NULL


UPDATE CCLF_WELLNESS_AGING SET DAYS_SINCE_WELLNESS = 999
WHERE DAYS_SINCE_WELLNESS IS NULL




	-----Log Information
		
	 
		SELECT @InsertedRecord=count(1)
		FROM [CCLF_WELLNESS_VISITS] ;

          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
