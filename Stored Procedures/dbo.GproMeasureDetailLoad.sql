SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproMeasureDetailLoad]
/*===============================================================
	CREATED BY: 	William Keen
	CREATED ON:		2016-12-19
	INITIAL VER:	2.2.1
	MODULE:			GPro	
	DESCRIPTION:	Returns data used by the Measure Rates report
	PARAMETERS: 
		@BusinessUnitId - The parent business unit id.
		@MeasureYear - The year for which to filter data
	PROGRAMMING NOTES: 	

	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-12-19		WK						Initial Version
	2.2.1		2017-02-09		WK						Updating the select statement with new column names
	2.2.2		2017-04-13		WK			LBPP-2467	Updating the report to include Quality Points
=================================================================*/
(
	@BusinessUnitId INT = NULL,
	@MeasureYear INT = NULL
)
AS 
BEGIN
	DECLARE @MeasureYearHolder INT = @MeasureYear
	DECLARE @BusinessUnitName VARCHAR(255) = (SELECT Name FROM vwPortal_BusinessUnit WHERE BusinessUnitId = @BusinessUnitId AND DeleteInd = 0)
	CREATE TABLE #TempTable (
		RowNumber INT IDENTITY PRIMARY KEY NOT NULL, 
		Name VARCHAR(255) NULL, 
		BusinessUnitId INT NULL
	)

	INSERT INTO #TempTable (Name, BusinessUnitId) VALUES(@businessUnitName, @businessUnitId)
	INSERT INTO #TempTable (Name, BusinessUnitId) 
		SELECT Name, BusinessUnitId 
		FROM vwPortal_BusinessUnit 
		WHERE ParentBusinessUnitId = @BusinessUnitId AND DeleteInd = 0 
		GROUP BY Name, BusinessUnitId 
		ORDER BY [Name]	

	SELECT	t.RowNumber
			, gpmph.BusinessUnitId
			, gpmph.BusinessUnitName
			, gpmph.MeasureTypeId
			, gpmph.MeasureTypeName
			, gpmph.MeasureTypeDescription
			, gpmph.RankingTableColumnName
			, gpmph.TotalPatients
			, gpmph.TotalComplete
			, gpmph.ConsecutiveCount AS ConsecutiveComplete
			, gpmph.TotalSkipped
			, gpmph.PercentConsecutive AS PercentComplete
			, gpmph.MeasureStatus
			, gpmph.CMSTotalEligible
			, gpmph.CMSDenominator
			, gpmph.CMSDenominatorException
			, gpmph.CMSMeasureMet
			, gpmph.CMSMeasureNotMet
			, gpmph.CMSPercentMeasureMet
			, gpmph.CompletedTotalEligible
			, gpmph.CompletedDenominator
			, gpmph.CompletedDenominatorException
			, gpmph.CompletedMeasureMet
			, gpmph.CompletedMeasureNotMet
			, gpmph.CompletedPercentMeasureMet
			, gpmph.GetMeasureRankResultsTotalSkipped
			, gpmph.CompletedBenchmark
			, gpmph.CMSBenchmark
			, CASE  WHEN gpmph.CMSBenchmark IS NULL THEN NULL
					WHEN gpmph.CMSBenchmark = 0 THEN 0
					ELSE gpmb.QualityPoints
			  END AS QualityPoints
	FROM GProMeasureProgressHistory gpmph	
	JOIN #TempTable t ON t.BusinessUnitId = gpmph.BusinessUnitId
	LEFT JOIN GProMeasureBenchmark gpmb 
		ON gpmph.MeasureTypeId = gpmb.GProMeasureTypeId 
		AND gpmph.MeasureYear = gpmb.MeasureYear
		AND gpmph.CMSBenchmark = gpmb.Benchmark
	WHERE gpmph.MeasureYear = @MeasureYearHolder
	AND gpmph.TotalPatients > 0
	ORDER BY t.RowNumber, gpmph.CMSBenchmark DESC, gpmph.MeasureTypeName

	DROP TABLE #TempTable
END
GO
