SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwPatientInsurance] 
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES FOR NULL COLUMNS

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				SourceSystemId,LbPatientID, InsuranceName 
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				PatientInsuranceId
				DESC 
			) SNO
	FROM PatientInsuranceProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientInsuranceProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientInsuranceId INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientInsuranceId,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientInsuranceId,PatientID
FROM 
(	
MERGE [DBO].PatientInsurance AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,SourceSystemID as SourceSystemID,
			[InsuranceName],[FinancialClass],[InsurancePriority],[EffectiveDate],[ExpirationDate],[PolicyNumber],[Comment],
			0 AS DeleteInd, GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId
		FROM [dbo].PatientInsuranceProcessQueue PPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  ([OrigPatientId],[PatientId],[SourceSystemID],
	   [InsuranceName],[FinancialClass],[InsurancePriority],[EffectiveDate],[ExpirationDate],[PolicyNumber],[Comment],
	   [DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
ON ( target.PatientID = source.PatientID AND  target.SourceSystemId = source.SourceSystemId AND  target.InsuranceName = source.InsuranceName)
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
[InsuranceName] = source.[InsuranceName],
[FinancialClass] = source.[FinancialClass],
[InsurancePriority] = source.[InsurancePriority],
[EffectiveDate] = source.[EffectiveDate],
[ExpirationDate] = source.[ExpirationDate],
[PolicyNumber] = source.[PolicyNumber],
[Comment] = source.[Comment],
[ModifyDateTime] = source.[ModifyDateTime],
[ModifyLBUserId] = source.[ModifyLBUserId]
WHEN NOT MATCHED THEN
INSERT ([PatientId],[SourceSystemID],
	   [InsuranceName],[FinancialClass],[InsurancePriority],[EffectiveDate],[ExpirationDate],[PolicyNumber],[Comment],
	   [DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
VALUES (source.[PatientId],source.[SourceSystemID],
	   source.[InsuranceName],source.[FinancialClass],source.[InsurancePriority],source.[EffectiveDate],source.[ExpirationDate],source.[PolicyNumber],source.[Comment],
	   source.[DeleteInd],source.[CreateDateTime],source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientInsuranceId,INSERTED.PatientInsuranceId) PatientInsuranceId, source.PatientID
) x ;

IF OBJECT_ID('PatientInsurance_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientInsurance_PICT(PatientInsuranceId,PatientId,SwitchDirectionId)
	SELECT RT.PatientInsuranceId, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientInsuranceProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'PatientInsurance',GetDate()
FROM PatientInsuranceProcessQueue
WHERE ProcessedInd = 1 


END


GO
