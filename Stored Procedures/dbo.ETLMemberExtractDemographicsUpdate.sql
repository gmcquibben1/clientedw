SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ETLMemberExtractDemographicsUpdate] @FullLoad CHAR(1) = 'N' 

AS
BEGIN



/*===============================================================
	CREATED BY: 	SG
	CREATED ON:		2014
	INITIAL VER:	1.6.1
	MODULE:			ETL Demographic Extraction
	DESCRIPTION:	This stored procedure will move the staged data located in the member extract tables into the core demographic tables
	PARAMETERS:		NA, 
	RETURN VALUE(s)/OUTPUT:	Exception information will be captured in the exception log table
	PROGRAMMIN NOTES: 
	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.1.1       2016-09-20		LM			LBETL-119	updated the step 4 to exclude the deleted party, individual, and Patient in update set
	2.1.1		2016-10-24		YL			LBETL-186	Fix email 
	2.1.1		2016-11-01		SG			LBAN-2785	corrected issues with death indicator in Individual table
	2.1.1		2016-11-02		SG			LBETL-204	Fix SourceSystemId
	2.1.1		2016-11-18		CJL			LBETL-243	Made modification to the update routine to prevent patients with a death indicator already set to revert back to null
	2.1.2		2016-11-29		YL			LBETL-248	Exclude records in IndividualEmail with blank email address	
	2.1.2		2016-12-15		SG			LBETL-207   Added code for Race Code, Marital Code and Ethnicity.
	2.1.2		2016-12-15		CJL			LBETL-207   Added Adjustments for taxonomy 
	2.1.2		2016-01-25		CJL			LBETL-207   Hardened Stored procedure against bad marital, race and enthic status codes. We will now attempt to match on the Name on the null taxonomy in order to try to resolve a code
	2.2.1		2017-02-04		CJL			LBETL-635   Millenium had patients with a null deceased ind but a deceased date.  Attempt to interpolate the deathind based on the death date
	2.2.1		2017-02-27		YL		    LBETL-948   add @FullLoad flag and for default process two weeks InterfacePatient data to speeding the process;
                                                                                    add procedure log to each steps 

=================================================================*/

	SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'vwPortal_IndividualEmail';
	DECLARE @dataSource VARCHAR(20) = 'MemberExtract';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLMemberExtractDemographicsUpdate';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @COMMENT VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @LBUSERID            INT=1
	DECLARE @CurrentDateTime     DATETIME=GETUTCDATE()
	DECLARE @DeleteInd           BIT=0
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;

	
     --YL 2017/02/27
    DECLARE @StartDatetime  DateTime ;
    SET  @StartDatetime  =CASE WHEN @FullLoad='Y' THEN '2000-01-01' ELSE Dateadd(day,-14,GETUTCDATE()) END;

	 
BEGIN TRY
		
		
      

        SELECT IP.*
		INTO #tempLastestDemo
		FROM
		(  
		 SELECT InterfaceSystemId,PatientIdentifier, SUBSTRING(PrimaryEmail,1,50) AS PrimaryEmail,PreferredCommunicationMethod,
		        OptOutEmailInd,OptOutVoiceInd,OptOutTextInd,LanguageCode,PrimaryCareProviderNPI,
				  ROW_NUMBER() OVER(PARTITION BY InterfaceSystemId,PatientIdentifier ORDER BY CreateDateTime DESC) r 
		 FROM vwPortal_InterfacePatient
		 --WHERE ISNULL(PrimaryEmail,'') <> ''   --YL 2016/11/29
	    WHERE [CreateDateTime]>=@StartDatetime       --YL 2017/02/27
		 ) IP
		 WHERE r=1

        CREATE INDEX IDX_IP_DEMO ON  #tempLastestDemo (InterfaceSystemId,PatientIdentifier) INCLUDE (PrimaryEmail,PreferredCommunicationMethod, OptOutEmailInd,OptOutVoiceInd,OptOutTextInd,LanguageCode);


       --- 1. Email
	   SELECT @RecordCountBefore=COUNT(1)
		FROM vwPortal_IndividualEmail;


		MERGE INTO vwPortal_IndividualEmail AS TARGET
		USING (SELECT DISTINCT p.IndividualId,ip.PrimaryEmail,ifs.SourceSystemId
			 FROM --vwPortal_Patient p 
		--	 INNER JOIN vwPortal_InterfacePatientMatched ipm ON ipm.LbPatientId = p.PatientId
		--	 INNER JOIN (SELECT InterfaceSystemId,PatientIdentifier, SUBSTRING(PrimaryEmail,1,50) AS PrimaryEmail,
		--		  ROW_NUMBER() OVER(PARTITION BY InterfaceSystemId,PatientIdentifier ORDER BY CreateDateTime DESC) r FROM vwPortal_InterfacePatient) ip ON ip.InterfaceSystemId = ipm.InterfaceSystemId AND ip.PatientIdentifier = ipm.PatientIdentifier AND r = 1
			 #tempLastestDemo ip 
			 INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ip.InterfaceSystemId
			 INNER JOIN dbo.PatientIdReference idRef ON idRef.SourceSystemId = ifs.SourceSystemId 
						  AND idRef.ExternalId = IP.PatientIdentifier 
						  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
						  AND idRef.DeleteInd =0
			 INNER JOIN vwPortal_Patient p on p.patientid=idref.lbpatientID
			 WHERE ip.PrimaryEmail IS NOT NULL) AS SOURCE
			ON TARGET.IndividualId = SOURCE.IndividualId
				  AND ISNULL(TARGET.Email,'') = ISNULL(SOURCE.PrimaryEmail,'')
				  AND SOURCE.SourceSystemId = TARGET.SourceSystemId
		WHEN MATCHED THEN 
			UPDATE SET TARGET.ModifyDateTime = @CurrentDateTime, TARGET.ModifyLbUserId = 1, TARGET.DeleteInd = 0
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (IndividualID,EmailTypeId,Email,SourceSystemID,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
			VALUES (SOURCE.IndividualId,1,SOURCE.PrimaryEmail,SOURCE.SourceSystemId,0,@CurrentDateTime,@CurrentDateTime,1,1);

				   SELECT @RecordCountAfter=COUNT(1)
		           FROM vwPortal_IndividualEmail;

                SET @InsertedRecord= @RecordCountAfter-@RecordCountBefore;

							-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
							SET @StartTime  =@EndTime
					
	-- 2. update communication method
	             
					UPDATE p
					SET p.PreferredCommunicationMethodTypeId = (CASE WHEN pcm.CommunicationMethodTypeId IS NOT NULL THEN pcm.CommunicationMethodTypeId ELSE 0 END),
						 p.OptOutEmailInd = (CASE WHEN ip.OptOutEmailInd IS NOT NULL THEN ip.OptOutEmailInd ELSE 0 END),
						 p.OptOutPhoneInd = (CASE WHEN ip.OptOutVoiceInd IS NOT NULL THEN ip.OptOutVoiceInd ELSE 0 END),
						 p.OptOutSmsInd = (CASE WHEN ip.OptOutTextInd IS NOT NULL THEN ip.OptOutTextInd ELSE 0 END),
						 ModifyDateTime = GETUTCDATE(),ModifyLBUserId = 1, p.PcpNationalProviderIdentifier=ip.PrimaryCareProviderNPI
					FROM #tempLastestDemo ip
					 INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ip.InterfaceSystemId
			         INNER JOIN dbo.PatientIdReference idRef ON idRef.SourceSystemId = ifs.SourceSystemId 
						  AND idRef.ExternalId = IP.PatientIdentifier 
						  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
						  AND idRef.DeleteInd =0	
					 INNER JOIN vwPortal_Patient p  on p.patientid=idref.lbpatientID
					 LEFT OUTER JOIN vwPortal_CommunicationMethodType pcm ON ip.PreferredCommunicationMethod LIKE '%' + pcm.NAME + '%'		
					
                 --YL 2017/02/27 add log
                SET @UpdatedRecord=@@ROWCOUNT;
				SET @InsertedRecord=0;
				SET @EDWtableName ='step 2: vwPortal_Patient'
							-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
						 
							SET @StartTime  =@EndTime
	-- 3.  update language
				UPDATE i
					 SET i.PreferredLanguageCode = ip.LanguageCode
				FROM vwPortal_Individual i
					 INNER JOIN vwPortal_Patient p ON p.IndividualId = i.IndividualId
					 INNER JOIN dbo.PatientIdReference idRef ON idRef.LbPatientId=p.patientid
						  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
						  AND idRef.DeleteInd =0	
					  INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.SourceSystemId = i.SourceSystemId
			         INNER JOIN #tempLastestDemo ip ON ip.InterfaceSystemId = ifs.InterfaceSystemId AND ip.PatientIdentifier = idRef.ExternalId
					   AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
						  AND idRef.DeleteInd =0	
					 WHERE LEN(ip.LanguageCode) <= 3					 
			--YL 2017/02/27 add log		
			    SET @UpdatedRecord=@@ROWCOUNT;
				SET @InsertedRecord=0;
				SET @EDWtableName ='step 3: vwPortal_Individual'
							-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
					
						
							SET @StartTime  =@EndTime
     -- 4. Update LbPatientId
					UPDATE [dbo].[MemberExtract]
					SET LbPatientId = P.patientId
					FROM [dbo].[MemberExtract] M INNER JOIN vwPortal_Party PTY
					ON M.medicareNo = PTY.ExternalReferenceIdentifier
					AND M.SourceSystemId = PTY.SourceSystemID
					AND PTY.[DeleteInd] = 0
					INNER JOIN vwPortal_Individual I
					ON PTY.PartyID = I.PartyID
					AND I.DeleteInd = 0
					INNER JOIN vwPortal_Patient P
					ON I.IndividualID = P.IndividualID
					AND P.DeleteInd = 0
					

					UPDATE [dbo].[MemberExtract]
					SET LbPatientId = P.patientId
					FROM [dbo].[MemberExtract] M INNER JOIN dbo.vwPatientDemographic P 
					ON 	LTRIM(RTRIM(P.FirstName)) = LTRIM(RTRIM(M.FirstName))
						AND LTRIM(RTRIM(P.LastName)) = LTRIM(RTRIM(M.LastName))
						AND P.BirthDate = M.[birthDate]
						AND CAST( (CASE WHEN P.Gender = 'Female' THEN 2
								WHEN P.Gender = 'Male' THEN 1
								WHEN P.Gender = 'Unknown' THEN 3
								WHEN P.Gender = 'Undifferentiated' THEN 4
								ELSE 0 end) AS INT) = M.sex;
                            --YL 2017/02/27 add log
  			    SET @UpdatedRecord=@@ROWCOUNT;
				SET @InsertedRecord=0;
				SET @EDWtableName ='step 4: MemberExtract'
							-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
					
					
							SET @StartTime  =@EndTime

--- 5.  Update DeathDate
   						-- Fixed by Sam : 9/26/2016 10:47PM
						; WITH CTE AS 
						( SELECT  IFP.PatientIdentifier , IFP.DeathDate , IFP.DeathInd, IPS.SourceSystemId ,
						  ROW_NUMBER() OVER(PARTITION BY IPS.SourceSystemId, IFP.PatientIdentifier ORDER BY IFP.CreateDateTime DESC) SNO
						  FROM vwPortal_InterfacePatient IFP JOIN vwportal_InterfaceSystem IPS
						  ON IFP.InterfaceSystemId = IPS.InterfaceSystemId
						   WHERE IPS.[CreateDateTime]>=@StartDatetime --YL 2017/02/27
						  )
						UPDATE MemberExtract 
						SET DeathDate = ISNULL(CTE.DeathDate,'1900-01-01')
						FROM MemberExtract ME INNER JOIN CTE ON ME.medicareNo = CTE.PatientIdentifier AND CTE.SourceSystemId = ME.SourceSystemId
							AND CTE.SNO = 1 AND (CTE.DeathDate IS NOT NULL OR DeathInd = 1 )

                           SET @UpdatedRecord=@@ROWCOUNT;

						; WITH CTE AS 
						( SELECT BENE_HIC_NUM Utoken, TRY_CONVERT(DATETIME,BENE_DEATH_DT) DeathDate FROM CCLF_8_BeneDemo )
						UPDATE MemberExtract 
						SET DeathDate = CTE.DeathDate 
						FROM MemberExtract ME INNER JOIN CTE ON ME.medicareNo = CTE.Utoken AND CTE.DeathDate  >= '2000-01-01'   

						SELECT  DISTINCT IP.PatientIdentifier , ME.Medicareno, ME.sourcesystemId, me.lbpatientid , IP.DeathInd, IP.DeathDate
						INTO #T1
						FROM dbo.vwportal_interfacePatient IP
						JOIN dbo.vwportal_interfacesystem IFS
						ON IP.InterfaceSystemId = IFS.InterfaceSystemId
						JOIN dbo.MemberExtract ME
						ON IP.PatientIdentifier = ME.medicareNo
						AND  ME.sourcesystemId = IFS.SourceSystemId
						WHERE IP.deathInd = 1 OR IP.DeathDate IS NOT NULL
						

						UPDATE ID
						SET ID.DeathDate = T1.DeathDate , ID.DeceasedInd = ISNULL(T1.DeathInd, CASE  WHEN T1.DeathDate  >= '2000-01-01' THEN 1 ELSE 0 END)
						FROM dbo.vwportal_individual ID
						JOIN dbo.vwportal_patient IP
						ON ID.IndividualID = IP.IndividualId
						JOIN ( SELECT DISTINCT lbpatientId, DeathInd , DeathDate FROM #T1 T WHERE T.DeathDate IS NOT NULL ) T1
						ON T1.LbpatientId = IP.PatientId 
                         SET @UpdatedRecord=@UpdatedRecord+@@ROWCOUNT;
						DROP TABLE #T1

						--; WITH CTE AS 
						--( SELECT  '|' + CAST(InterfaceSystemId AS VARCHAR(4)) + '|' + PatientIdentifier + '|' Utoken, DeathDate , DeathInd,
						--  ROW_NUMBER() OVER(PARTITION BY InterfaceSystemId, PatientIdentifier ORDER BY CreateDateTime Desc) SNO
						--  FROM vwPortal_InterfacePatient )
						--UPDATE MemberExtract 
						--SET DeathDate = ISNULL(CTE.DeathDate,'1900-01-01')
						--FROM MemberExtract ME INNER JOIN CTE ON ME.medicareNo = CTE.Utoken AND CTE.SNO = 1 AND (CTE.DeathDate IS NOT NULL OR DeathInd = 1 )

						--; WITH CTE AS 
						--( SELECT BENE_HIC_NUM Utoken, TRY_CONVERT(DATETIME,BENE_DEATH_DT) DeathDate FROM CCLF_8_BeneDemo )
						--UPDATE MemberExtract 
						--SET DeathDate = CTE.DeathDate 
						--FROM MemberExtract ME INNER JOIN CTE ON ME.medicareNo = CTE.Utoken AND CTE.DeathDate  >= '2000-01-01'   

				--YL 2017/02/27 add log		  			 
				SET @InsertedRecord=0;
				SET @EDWtableName ='step 5: vwPortal_Individual'
							-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;

							SET @StartTime  =@EndTime

		-- 6 : Update RaceCode, Marital Code, Ethnicity
		SELECT DISTINCT IP.PatientIdentifier , IP.PatientOtherIdentifier, IP.CreateDateTime,
			 LTRIM(RTRIM(IP.FirstName)) AS IPFirstName, 
			 LTRIM(RTRIM(IP.LastName)) AS IPLastName,
			 LTRIM(RTRIM(ISNULL(IP.MaritalCode, ''))) As IMaritalCode, LTRIM(RTRIM(IP.MaritalDescription)) AS IMaritalDescription, LTRIM(RTRIM(ISNULL(IP.MaritalSystemId, ''))) AS IMaritalSystemId,
			 LTRIM(RTRIM(IP.RaceDescription)) AS IPRaceDescription , LTRIM(RTRIM(ISNULL(IP.RaceCode, ''))) AS IRaceCode,   LTRIM(RTRIM(ISNULL(IP.RaceSystemId, ''))) AS IRaceSystemId, 
			 LTRIM(RTRIM(ISNULL(IP.EthnicCode, ''))) As IEthnicCode , LTRIM(RTRIM(ISNULL(IP.EthnicSystemId, ''))) As IEthnicSystemId, LTRIM(RTRIM(IP.EthnicDescription)) as IEthnicDescription,
			 ME.LbPatientId , ME.Firstname, ME.lastName , ME.SourceSystemId
		INTO #IM1
	FROM dbo.vwportal_interfacepatient IP 
	JOIN dbo.vwportal_interfaceSystem ISS WITH (NOLOCK) ON IP.InterfaceSystemId = ISS.InterfaceSystemId
	JOIN dbo.MemberExtract ME ON ME.medicareNo = IP.PatientIdentifier AND ME.SourceSystemId = ISS.SourceSystemId
	JOIN dbo.vwportal_Patient P	ON ME.LbPatientId = P.PatientId 
	WHERE P.DeleteInd = 0   -- guard against old Pid
	AND  IP.[CreateDateTime]>=@StartDatetime --YL 2017/02/27

	--Handle MaritalCode
	SELECT A.*, MS.MaritalStatusTypeId, MSNameMatch.MaritalStatusTypeId As NameMatchId INTO #IM2
	FROM (
		 SELECT  I.*,
		 ROW_NUMBER() OVER(PARTITION BY I.lbpatientId  ORDER BY CreateDateTime DESC) r
		 FROM #IM1 I 
		 WHERE I.IMaritalCode <> ''
	) A 
	LEFT  JOIN dbo.vwportal_MaritalStatusType MS ON   (A.IMaritalCode = MS.Code OR A.IMaritalCode = MS.Name)  AND ISNULL(A.IMaritalSystemId,'') = MS.Oid
	LEFT  JOIN dbo.vwportal_MaritalStatusType MSNameMatch ON   (A.IMaritalDescription = MSNameMatch.NAME  AND MSNameMatch.Oid = '')		--If we can not match by the code, attempt to match on the name using the null oid value
	WHERE A.r = 1 

		--Get the default value if we can not resolve the cod
	DECLARE @DefaultMaritalStatusTypeId INT = (SELECT MaritalStatusTypeId FROM vwportal_MaritalStatusType WHERE Name = 'Unknown' AND Oid = '')	

	
	
	CREATE NONCLUSTERED INDEX IDX_NONCLXTR_IM2_LbPid
	ON [dbo].[#IM2] ([LbPatientId])
	INCLUDE ([MaritalStatusTypeId])

	UPDATE I
	SET I.MaritalStatusTypeID = ISNULL(ISNULL( M.MaritalStatusTypeId, M.NameMatchId), @DefaultMaritalStatusTypeId)
	FROM dbo.vwportal_Individual I
	JOIN dbo.vwportal_Patient P ON I.IndividualID = P.IndividualID
	JOIN #IM2 M ON P.PatientId = M.LbPatientId

             --YL 2017/02/27 add log
	  		 SET @UpdatedRecord=@@ROWCOUNT;
				SET @InsertedRecord=0;
				SET @EDWtableName ='step 6-1: vwPortal_Individual'
							-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
	
							SET @StartTime  =@EndTime
	DROP TABLE #IM2


	
		--Get the default value if we can not resolve the cod
	DECLARE @DefaultRaceTypeId INT = (SELECT RaceTypeId FROM vwPortal_RaceType WHERE Name = 'Unknown' AND Oid = '')	

	-- Handle RaceCode
	SELECT A.lbpatientId, A.PatientIdentifier, A.PatientOtherIdentifier,  A.SourceSystemID, RT.RaceTypeId, MIN( ISNULL(RTNameMatch.RaceTypeId,@DefaultRaceTypeId)) as RaceTypeIdNameMatch INTO #IM3
	FROM (
		 SELECT  I.*,
		 ROW_NUMBER() OVER(PARTITION BY I.lbpatientId  ORDER BY CreateDateTime DESC) r
		 FROM #IM1 I 
		  WHERE I.IRaceCode <> ''
	) A 
	LEFT  JOIN dbo.vwPortal_RaceType RT ON   (A.IRaceCode = RT.NAME OR A.IRaceCode  = RT.Code)   AND ISNULL(A.IRaceSystemId,'') = RT.Oid
	LEFT  JOIN dbo.vwPortal_RaceType RTNameMatch ON   (A.IPRaceDescription = RTNameMatch.NAME  AND RTNameMatch.Oid = '')		--If we can not match by the code, attempt to match on the name using the null oid value
	WHERE A.r = 1 
	GROUP BY A.lbpatientId, A.PatientIdentifier, A.PatientOtherIdentifier, RT.RaceTypeId,  A.SourceSystemID





	
	
	

	MERGE vwPortal_IndividualRace AS TARGET
	USING
	(
		SELECT DISTINCT
			I.IndividualID, IM.RaceTypeId, IM.RaceTypeIdNameMatch, 
			IM.SourceSystemID AS SourceSystemID,
			0  AS DeleteInd,
			getUTCDate() AS CreateDatetime,
			getUTCDate() AS ModifyDatetime,
			1 AS ModifyLbUserid,1 AS CreateLBUserId ,
			IM.PatientIdentifier AS ExternalReferenceIdentifier,
			IM.PatientOtherIdentifier AS OtherReferenceIdentifier
		FROM #IM3 IM
		JOIN dbo.vwPortal_Patient PT ON IM.LbpatientId = PT.PatientId
		JOIN dbo.vwportal_Individual I ON I.IndividualId = PT.IndividualId
	) AS SOURCE
	(
	IndividualID, RaceTypeId, RaceTypeIdNameMatch,SourceSystemID,
	DeleteInd, CreateDateTime,ModifyDateTime,
	ModifyLBUserId,CreateLBUserId ,ExternalReferenceIdentifier,OtherReferenceIdentifier)
	ON TARGET.IndividualId = SOURCE.IndividualId
	WHEN MATCHED THEN UPDATE SET
		IndividualID = SOURCE.IndividualID,
		SourceSystemID = SOURCE.SourceSystemID,
		RaceTypeId = ISNULL(ISNULL( SOURCE.RaceTypeId, SOURCE.RaceTypeIdNameMatch), @DefaultRaceTypeId),
		ExternalReferenceIdentifier = SOURCE.ExternalReferenceIdentifier,
		OtherReferenceIdentifier = SOURCE.OtherReferenceIdentifier,
		DeleteInd = SOURCE.DeleteInd,
		ModifyDateTime = SOURCE.ModifyDateTime,
		ModifyLBUserId = SOURCE.ModifyLBUserId
	WHEN NOT MATCHED THEN INSERT
	(
		IndividualID,SourceSystemID,RaceTypeId,
		ExternalReferenceIdentifier,OtherReferenceIdentifier,DeleteInd,
		CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId)
	VALUES 
	(
		SOURCE.IndividualID,SOURCE.SourceSystemID,  ISNULL(ISNULL( SOURCE.RaceTypeId, SOURCE.RaceTypeIdNameMatch), @DefaultRaceTypeId),
		SOURCE.ExternalReferenceIdentifier,SOURCE.OtherReferenceIdentifier,SOURCE.DeleteInd,
		SOURCE.CreateDateTime,SOURCE.ModifyDateTime,SOURCE.ModifyLBUserId,SOURCE.CreateLBUserId
	) ;

     --YL 2017/02/27 add log
     SET @UpdatedRecord=(select count(1) from #IM3);
  		
				SET @InsertedRecord=0;
				SET @EDWtableName ='step 6-2: vwPortal_IndividualRace'
							-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
	
							SET @StartTime  =@EndTime

	DROP TABLE #IM3
	DECLARE @DefaultEnthnicityTypeId INT = (SELECT TOP 1 EthnicityTypeID FROM [vwPortal_EthnicityType] WHERE Name = 'Unknown' AND Oid = '')	

	
	
	


	--Handle EthnicCode
	SELECT A.lbpatientId, A.PatientIdentifier, A.PatientOtherIdentifier, ET.EthnicityTypeID,  A.SourceSystemID, MIN(ISNULL(ETNameMatch.EthnicityTypeID,@DefaultEnthnicityTypeId)) as ETNameMatchId INTO #IM4
	FROM (
		 SELECT  I.*,
		 ROW_NUMBER() OVER(PARTITION BY I.lbpatientId  ORDER BY CreateDateTime DESC) r
		 FROM #IM1 I 
		  WHERE I.IEthnicCode <> ''
	) A 
	LEFT  JOIN [dbo].[vwPortal_EthnicityType] ET	ON (A.IEthnicCode = ET.NAME OR A.IEthnicCode = ET.Code ) AND (ISNULL(A.IEthnicSystemId,'') = ET.Oid)
	LEFT  JOIN dbo.[vwPortal_EthnicityType] ETNameMatch ON   (A.IEthnicDescription = ETNameMatch.NAME  AND ETNameMatch.Oid = '')		--If we can not match by the code, attempt to match on the name using the null oid value
	WHERE A.r = 1 
	GROUP BY A.lbpatientId, A.PatientIdentifier, A.PatientOtherIdentifier,  A.SourceSystemID, ET.EthnicityTypeID


	
	MERGE dbo.vwportal_IndividualEthnicity AS TARGET
	USING 
	(
		SELECT DISTINCT
			I.IndividualID, 
			IM.EthnicityTypeID, IM.ETNameMatchId,
			IM.SourceSystemID AS SourceSystemID,
			0  AS DeleteInd,
			getUTCDate() AS CreateDatetime,
			getUTCDate() AS ModifyDatetime,
			1 AS ModifyLbUserid,1 AS CreateLBUserId ,
			IM.PatientIdentifier AS ExternalReferenceIdentifier,
			IM.PatientOtherIdentifier AS OtherReferenceIdentifier
		FROM #IM4 IM
		JOIN dbo.vwPortal_Patient PT ON IM.LbpatientId = PT.PatientId
		JOIN dbo.vwportal_Individual I 	ON I.IndividualId = PT.IndividualId
	)
	AS SOURCE
	(
		IndividualID, EthnicityTypeID, ETNameMatchId,  SourceSystemID,
		DeleteInd,	CreateDateTime,ModifyDateTime,
		ModifyLBUserId,CreateLBUserId ,	ExternalReferenceIdentifier,OtherReferenceIdentifier
	)
	ON TARGET.IndividualId = SOURCE.IndividualId
	WHEN MATCHED THEN UPDATE SET
		IndividualID = SOURCE.IndividualID,
		SourceSystemID = SOURCE.SourceSystemID,
		EthnicityTypeID = ISNULL(ISNULL(SOURCE.EthnicityTypeID, SOURCE.ETNameMatchId), @DefaultEnthnicityTypeId),
		ExternalReferenceIdentifier = SOURCE.ExternalReferenceIdentifier,
		OtherReferenceIdentifier = SOURCE.OtherReferenceIdentifier,
		DeleteInd = SOURCE.DeleteInd,
		ModifyDateTime = SOURCE.ModifyDateTime,
		ModifyLBUserId = SOURCE.ModifyLBUserId
	WHEN NOT MATCHED THEN INSERT
	(
		IndividualID,SourceSystemID,EthnicityTypeID,
		ExternalReferenceIdentifier,OtherReferenceIdentifier,DeleteInd,
		CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId
	)
	VALUES
	(
		SOURCE.IndividualID,SOURCE.SourceSystemID,ISNULL(ISNULL(SOURCE.EthnicityTypeID, SOURCE.ETNameMatchId), @DefaultEnthnicityTypeId),
		SOURCE.ExternalReferenceIdentifier,SOURCE.OtherReferenceIdentifier,SOURCE.DeleteInd,
		SOURCE.CreateDateTime,SOURCE.ModifyDateTime,SOURCE.ModifyLBUserId,SOURCE.CreateLBUserId
	);
            --YL 2017/02/27 add log
	     SET @UpdatedRecord=(select count(1) from #IM4);
  		
				SET @InsertedRecord=0;
				SET @EDWtableName ='step 6-3: vwportal_IndividualEthnicity'
							-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	

	DROP TABLE #IM4
	DROP TABLE #tempLastestDemo
	DROP TABLE #IM1



		END TRY
		BEGIN CATCH
				   -- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
					SET  @EndTime=GETUTCDATE();
					SET  @ErrorNumber =ERROR_NUMBER();
					SET  @ErrorState =ERROR_STATE();
					SET  @ErrorSeverity=ERROR_SEVERITY();
					SET  @ErrorLine=ERROR_LINE();
					SET  @COMMENT =LEFT(ERROR_MESSAGE(),400);
					EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
				
		END CATCH

	
	
END





GO
