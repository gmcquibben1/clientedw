SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientSocialHistoryLoad] 
	 @PatientId INT = NULL, 
	 @RollupToSingleRow BIT = 1

AS


/*
	Procedure:		PatientSocialHistoryLoad

	Description:	Stored Procedure used to return a list of social history elements for a specific patient

	@PatientId -			id of patient 
	@RollupToSingleRow		Flag indicating if we should roll the all of the values int ht

	Version		Date		Author	JIRA	 Change	
	-------		----------	------	------    ------
	1.5.2		2015-04-15	RJN					New Procedure
	1.5.2		2015-04-15	MT					Instead SmokingStatus code Smoking description will be shown.
	2.2.1		2017-02-10	CJL		LBETL-830	Handle cases in which smokeing status is NULL, changed the join to a LEFT JOIN
	2.2.1		2017-02-22	CJL		LBDM-1988	Roll up to a single value regardless of the sercive 
	
*/



IF @RollupToSingleRow = 1 
BEGIN


	--Roll up to the newest non null values accross all of the
	SELECT DISTINCT       
		ss.SourceSystem,
		sm.SmokingStatus,
		m.MaritalStatus,
		e.EmploymentStatus,
		t.TransportationInd,
		l.LivingArrangementCode,
		a.AlcoholUseInd,
		d.DrugUseInd,
		c.CaffeineUseInd,
		s.SexuallyActiveInd,
		ex.ExerciseHoursPerWeek,
		psh.CreateDateTime
	FROM  
	  ( SELECT patientid, CreateDateTime, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE PatientId = @PatientId    ) psh 
	  LEFT OUTER JOIN ( SELECT patientid, MaritalStatus, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
			 FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE MaritalStatus IS NOT NULL AND MaritalStatus <> '' AND PatientId = @PatientId   ) m ON psh.PatientId = m.PatientId
	  LEFT OUTER JOIN ( SELECT patientid, EmploymentStatus, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE EmploymentStatus IS NOT NULL AND EmploymentStatus <> '' AND PatientId = @PatientId    ) e ON psh.PatientId = e.PatientId
	  LEFT OUTER JOIN ( SELECT patientid, TransportationInd, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE TransportationInd IS NOT NULL  AND PatientId = @PatientId   ) t ON psh.PatientId = t.PatientId
	  LEFT OUTER JOIN ( SELECT patientid, LivingArrangementCode, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE LivingArrangementCode IS NOT NULL AND LivingArrangementCode <> ''  AND PatientId = @PatientId   ) l ON psh.PatientId = l.PatientId
	  LEFT OUTER JOIN ( SELECT patientid, AlcoholUseInd, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE AlcoholUseInd IS NOT NULL  AND PatientId = @PatientId   ) a ON psh.PatientId = a.PatientId
	  LEFT OUTER JOIN ( SELECT patientid, DrugUseInd, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE DrugUseInd IS NOT NULL  AND PatientId = @PatientId   ) d ON psh.PatientId = d.PatientId
	  LEFT OUTER JOIN ( SELECT patientid, CaffeineUseInd, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE CaffeineUseInd IS NOT NULL   AND PatientId = @PatientId   ) c ON psh.PatientId = c.PatientId
	 LEFT OUTER JOIN ( SELECT patientid, SexuallyActiveInd, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE SexuallyActiveInd IS NOT NULL  AND PatientId = @PatientId   ) s ON psh.PatientId = s.PatientId
	 LEFT OUTER JOIN ( SELECT patientid, ExerciseHoursPerWeek, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK) WHERE ExerciseHoursPerWeek IS NOT NULL   AND PatientId = @PatientId   ) ex ON psh.PatientId = ex.PatientId
	 LEFT OUTER JOIN ( SELECT patientid, dbo.Codeset.DisplayValue as SmokingStatus, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK)
		 LEFT JOIN dbo.Codeset WITH (NOLOCK) ON PatientSocialHistory.SmokingStatus= Codeset.CodeValue AND   dbo.Codeset.CodeSetTypeId = (SELECT CodeSetTypeId FROM CodeSetType WHERE Name = 'SmokingStatus' AND DeleteInd = 0)
		 WHERE dbo.Codeset.DisplayValue  IS NOT NULL AND dbo.Codeset.DisplayValue  <> ''  AND PatientId = @PatientId   ) sm ON psh.PatientId = sm.PatientId

	 LEFT OUTER JOIN ( SELECT patientid, dbo.SourceSystem.Description as SourceSystem, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PatientSocialHistoryId DESC) rowNumber
		 FROM  dbo.PatientSocialHistory WITH (NOLOCK)
		 LEFT OUTER JOIN  dbo.SourceSystem WITH (NOLOCK) ON PatientSocialHistory.SourceSystemId = SourceSystem.SourceSystemId 
		 WHERE dbo.SourceSystem.Description  IS NOT NULL AND dbo.SourceSystem.Description  <> ''  AND PatientId = @PatientId   ) ss ON psh.PatientId = ss.PatientId
	WHERE
		psh.PatientId = @PatientId  AND 
		psh.rowNumber = 1 
		AND (m.rowNumber = 1 OR m.rowNumber IS NULL)
		AND (e.rowNumber = 1 OR e.rowNumber IS NULL)
		AND (t.rowNumber = 1 OR t.rowNumber IS NULL)
		AND (l.rowNumber = 1 OR l.rowNumber IS NULL)
		AND (a.rowNumber = 1 OR a.rowNumber IS NULL)
		AND (d.rowNumber = 1 OR d.rowNumber IS NULL)
		AND (c.rowNumber = 1 OR c.rowNumber IS NULL)
		AND (s.rowNumber = 1 OR s.rowNumber IS NULL)
		AND (ex.rowNumber = 1 OR ex.rowNumber IS NULL)
		AND (sm.rowNumber = 1 OR sm.rowNumber IS NULL)
		AND (ss.rowNumber = 1 OR ss.rowNumber IS NULL)
							
END
ELSE
BEGIN
	SELECT DISTINCT       
		dbo.SourceSystem.Description as SourceSystem,
		dbo.Codeset.DisplayValue as SmokingStatus,
		dbo.PatientSocialHistory.MaritalStatus,
		dbo.PatientSocialHistory.EmploymentStatus,
		dbo.PatientSocialHistory.TransportationInd,
		dbo.PatientSocialHistory.LivingArrangementCode,
		dbo.PatientSocialHistory.AlcoholUseInd,
		dbo.PatientSocialHistory.DrugUseInd,
		dbo.PatientSocialHistory.CaffeineUseInd,
		dbo.PatientSocialHistory.SexuallyActiveInd,
		dbo.PatientSocialHistory.ExerciseHoursPerWeek,
		dbo.PatientSocialHistory.CreateDateTime
	FROM  
		  dbo.PatientSocialHistory WITH (NOLOCK)
		  LEFT OUTER JOIN dbo.Codeset WITH (NOLOCK) ON PatientSocialHistory.SmokingStatus= Codeset.CodeValue AND   dbo.Codeset.CodeSetTypeId = (SELECT CodeSetTypeId FROM CodeSetType WHERE Name = 'SmokingStatus' AND DeleteInd = 0)
		  LEFT OUTER JOIN  dbo.SourceSystem WITH (NOLOCK) ON PatientSocialHistory.SourceSystemId = SourceSystem.SourceSystemId 
	WHERE dbo.PatientSocialHistory.PatientId = @PatientId 

END	

GO
