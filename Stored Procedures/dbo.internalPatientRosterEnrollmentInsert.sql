SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPatientRosterEnrollmentInsert] 
AS

BEGIN
-- update LbPatientID
UPDATE [PatientRosterEnrollmentRaw] SET LbPatientId = IDR.LbPatientId 
FROM [PatientRosterEnrollmentRaw] B 
JOIN PatientIdReference IDR ON B.[PatientExternalID] = IDR.ExternalId --and b.sourcesystemid = idr.sourcesystemid


--Set default Term Date if it is NULL/Missing/False dates
UPDATE [PatientRosterEnrollmentRaw] SET termdate = '01/01/2099'
--select *
from [PatientRosterEnrollmentRaw]
where termdate is null or termdate = '01/01/1900'

--Get Most Recent 
-- DROP TABLE #dedupe_temp
select DISTINCT [PatientExternalID],[StartDate], max(FileDate) as filedate
into #dedupe_temp
FROM [PatientRosterEnrollmentRaw]
group by [PatientExternalID],[StartDate]
--37091

--  drop table #1
select DISTINCT AMC.* 
into #dedupe_temp2
from [PatientRosterEnrollmentRaw] AMC
JOIN #dedupe_temp DT ON AMC.[PatientExternalID] = DT.[PatientExternalID] and AMC.FileDate = DT.filedate
		and AMC.[StartDate] = DT.[StartDate]

truncate table [PatientRosterEnrollment]

-- Insert all records from #dedupe_temp2
INSERT INTO [dbo].[PatientRosterEnrollment]
           ([SourceSystemID]
           ,[PatientExternalID]
           ,[FirstName]
           ,[LastName]
           ,[Gender]
           ,[Birthdate]
           ,[StartDate]
           ,[TermDate]
           ,[DeathDate]
           ,[ProviderNPI]
           ,[FileDate]
           ,[SourceFeed]
           ,[LbPatientId]
		   )
select distinct a.[SourceSystemID]
           ,a.[PatientExternalID]
           ,a.[FirstName]
           ,a.[LastName]
           ,a.[Gender]
           ,a.[Birthdate]
           ,a.[StartDate]
           ,a.[TermDate]
           ,a.[DeathDate]
           ,a.[ProviderNPI]
           ,a.[FileDate]
           ,a.[SourceFeed]
           ,a.[LbPatientId]
from #dedupe_temp2 a
--left join [PatientRosterEnrollment] pre on pre.[LbPatientId] = a.[LbPatientId]
--	and ISNULL(pre.[StartDate],'01/01/1900')  = ISNULL(a.[StartDate],'01/01/1900')
--	and pre.[SourceSystemID] = a.[SourceSystemID] and pre.[FileDate] = a.[FileDate]
--WHERE pre.[LbPatientId] is null 

END
GO
