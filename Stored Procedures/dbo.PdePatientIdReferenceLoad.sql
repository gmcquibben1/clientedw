SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientIdReferenceLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT
		pir.LbPatientId AS PatientId,
		pir.IdTypeDesc AS IdType,
		pir.ExternalId AS ExternalId,
		pir.Name AS SourceSystem
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientIdReference pir ON pir.LbPatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 
END
GO
