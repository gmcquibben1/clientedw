SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Youping
-- Create date: 2016-08-12
-- Description:	Load Interface PatientHistory Data into EDW
-- @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
-- @fullLoadFlag =0 Merge date to the existing table
-- Add procedure running log to [Maintenance].[dbo].[EdwProcedureRunLog]
-- ===============================================================

Create PROCEDURE [dbo].[ETLPatientHistory](@fullLoadFlag bit = 0)

AS
BEGIN	
 
    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientHistory';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientHistory';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY

  		
			--set @fullLoadFlag =1
	
			IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] =@EDWName)
			  BEGIN
			  
					 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed

			  END

			IF @fullLoadFlag = 1
			  BEGIN
				UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				   SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME)
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
			  END

			DECLARE @lastDateTime DATETIME = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
											FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
											WHERE [EDWtableName] = @EDWtableName 
											AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	



			
			SELECT  [InterfacePatientHistoryId]
			     	,ISS.SourceSystemId
				   ,IP.PatientIdentifier
				  ,[DateRecorded]
				  ,[HistoryItemType]
				  ,[NPI]
				  ,LEFT([Comment],250) as [Comment]
				  ,IPH.[CreateDateTime]
				INTO #PatientHistoryQue	
					  
					FROM [dbo].[vwPortal_InterfacePatientHistory] IPH with (nolock)
					INNER JOIN [dbo].[vwPortal_InterfacePatient] IP with (nolock) ON IPH.InterfacePatientId = IP.InterfacePatientId
					INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) ON IPH.InterfaceSystemId = ISS.[InterfaceSystemId]
					WHERE IPH.CreateDateTime >= @lastDateTime 
				
				;




              --- add Index
			  Create  NONCLUSTERED INDEX IDX_PatientIdentifier_SourceSystemId ON #PatientHistoryQue (PatientIdentifier,SourceSystemId) INCLUDE (DateRecorded  );
            			
				
	-----   dedup data prepare for Merge
			 SELECT * 
			 INTO #PatientHistory
			 FROM (
				SELECT  [InterfacePatientHistoryId]
						,PH.SourceSystemId
						, idRef.LbPatientId AS PatientID
				  ,[DateRecorded]
				  ,[HistoryItemType]
				  ,[NPI]
				  ,[Comment]
				  ,PH.[CreateDateTime]
					, ROW_NUMBER() OVER ( PARTITION BY 	 idRef.LbPatientId, DateRecorded 
					ORDER BY 	InterfacePatientHistoryId DESC ) RowNbr
					FROM  #PatientHistoryQue PH with (nolock)
					 INNER JOIN dbo.PatientIdReference idRef with (nolock) ON idRef.SourceSystemId = PH.SourceSystemId 
					  AND idRef.ExternalId = PH.PatientIdentifier 
					  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
					  AND idRef.DeleteInd =0
				
				) A
				WHERE A.RowNbr=1

			--- Truncate data
			 IF (@fullLoadFlag=1)
			   BEGIN
				   Truncate table [dbo].[PatientHistory];
			   END
	
			  CREATE CLUSTERED INDEX CIX_#PatientID_DiagnosisCode ON #PatientHistory([PatientId],DateRecorded) ;

			SELECT @RecordCountBefore=COUNT(1)
			FROM [dbo].[PatientHistory];


			SELECT @RecordCountSource=COUNT(1)
			FROM #PatientHistory;	    

			  --- test select * from #PatientHistory


				MERGE [DBO].PatientHistory AS [target]
				USING #PatientHistory AS [source] 

				ON ( target.PatientID =[source].PatientID  AND ISNULL(target.DateRecorded,'1900-01-01') = ISNULL(source.DateRecorded,'1900-01-01')
					)
                WHEN MATCHED  THEN UPDATE SET 
				      target.[HistoryTypeCode]			= source.[HistoryItemType]
					,target.[NPI]	= source.[NPI]
					,target.[Comment]	= source.[Comment]
					,target.[CreateDateTime]=source.[CreateDateTime]
					,target.[ModifyDateTime]		= @Today
					,target.[CreateLBUserId]=1
					,target.[ModifyLBUserId]=1
					WHEN NOT MATCHED THEN
					INSERT ([PatientId]
						  ,[DateRecorded]
						  ,[HistoryTypeCode]
						  ,[NPI]
						  ,[Comment]
						  ,[DeleteInd]
						  ,[CreateDateTime]
						  ,[ModifyDateTime]
						  ,[CreateLBUserId]
						  ,[ModifyLBUserId])
					VALUES (source.[PatientId],source.[DateRecorded],
							source.[HistoryItemType],source.[NPI],
							source.[Comment],0,source.[CreateDateTime],@Today,1,1);

	    
		

		    -- update lastdate
               		   
			 -- update maxSourceTimeStamp with max CreateDateTime of #PatientPatientHistoryQue
			 UPDATE [Maintenance]..[EDWTableLoadTracking]
			 SET [maxSourceTimeStampProcessed] = (SELECT MAX(CreateDateTime) FROM #PatientHistoryQue ),
				 [updateDateTime] =GETUTCDATE()
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


					
		   -- get total records from PatientPatientHistory after merge 
		    			SELECT @RecordCountAfter=COUNT(1)
			            FROM PatientHistory;

              -- Calculate records inserted and updated counts
	             SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	             SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			   SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
			  --clean
			  drop table  #PatientHistory,#PatientHistoryQue
	
 	END TRY

		BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END


GO
