SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[internalPatientMatchingResearch] 
@SummaryStats bit = 0 --summary stats
, @IDnotinIDR bit = 0 --Patient IDs from Claims not in ID Reference
, @IDRnotOrg bit = 0 --Patients in ID Reference, not in OrgHierarchy
, @PortalPatientNotOrg bit = 0 --Patients in PortalSide Patient Table, not in OrgHierarchy
, @DemoNoOrg bit = 0 --Patients in Demographics but no in OrgHierarchy
, @DeletedIndividual bit = 0 --Patients witha deleted individual record
, @InterfacePatientNoOrg bit =0 --InterfacePatient Records not in OrgHierarchy (or at least not found via LbPatientId)
, @InterfacePatientNoIDR bit = 0 --InterfacePatient Records w/ External Id not found in ID Reference
, @UnmatchedClaims bit = 0 
AS
SET NOCOUNT ON

EXEC InternalDropTempTables

--PULL ALL EXTERNAL IDS FROM ALL SOURCES
SELECT DISTINCT BENE_HIC_NUM AS ExternalId, BENE_1ST_NAME, BENE_LAST_NAME, BENE_DOB, BENE_SEX_CD, SourceFeed
INTO #TMP_IDs
FROM CCLF_8_BeneDemo

INSERT INTO #TMP_IDs (ExternalId, BENE_1ST_NAME, BENE_LAST_NAME, BENE_DOB, BENE_SEX_CD, SourceFeed)
SELECT DISTINCT HICNO AS ExternalId, mra.[FIRST NAME], mra.[LAST NAME], mra.[Birth DATE], mra.Sex1, SourceFeed
FROM MemberRosterArchives mra
LEFT JOIN #TMP_IDs t1 ON t1.ExternalId = mra.HICNO
WHERE t1.ExternalId IS NULL

INSERT INTO #TMP_IDs   (ExternalId, BENE_1ST_NAME, BENE_LAST_NAME, BENE_DOB, BENE_SEX_CD, SourceFeed)
SELECT DISTINCT PatientExternalID, mra.FirstName, mra.LastName, mra.Birthdate, mra.Gender, 'MSSP' 
FROM PatientRosterEnrollmentRaw mra
LEFT JOIN #TMP_IDs t1 ON t1.ExternalId = mra.PatientExternalID
WHERE t1.ExternalId IS NULL

IF @IDnotinIDR = 1
BEGIN
SET NOCOUNT OFF
--***question - are all these patients in ID Reference w/ an LbPatient Id
--select 'All_Patients_In_PidReference?' 
SELECT distinct  T1.*, IDR.ExternalId as IDR_ExternalID, IDR.LbPatientId FROM #TMP_IDs T1
LEFT JOIN PatientIdReference IDR ON T1.ExternalId = IDR.ExternalId
WHERE IDR.LbPatientId IS NULL
SET NOCOUNT ON
end

IF @IDRnotOrg = 1
BEGIN
SET NOCOUNT OFF
--Patient in ID Reference, not OrgHierarchy
--select 'Patient in ID Reference, not OrgHierarchy?'  
SELECT distinct  idr.SourceSystemId, idr.IdTypeDesc, SS.[Description], COUNT(DISTINCT idr.LbPatientId) AS [DistinctPatientIds]
FROM PatientIdReference idr
LEFT JOIN OrgHierarchy o ON idr.LbPatientId = o.LbPatientId
LEFT JOIN SourceSystem SS ON idr.SourceSystemId = SS.SourceSystemId
WHERE o.ID IS NULL
GROUP BY idr.SourceSystemId, idr.IdTypeDesc, SS.[Description]
SET NOCOUNT ON
END

IF @PortalPatientNotOrg = 1
BEGIN
SET NOCOUNT OFF
--same thing joined w/ PatientTable - 100% matchd
--select 'Patient in IdRef, portal Patient table, not in Org?' 
SELECT distinct  idr.SourceSystemId, idr.IdTypeDesc, SS.[Description], COUNT(DISTINCT idr.LbPatientId) AS [DistinctPatientIds]
FROM PatientIdReference idr
JOIN vwPortal_Patient pp ON idr.LbPatientId = pp.PatientId
LEFT JOIN OrgHierarchy o ON idr.LbPatientId = o.LbPatientId
LEFT JOIN SourceSystem SS ON idr.SourceSystemId = SS.SourceSystemId
WHERE o.ID IS NULL
GROUP BY idr.SourceSystemId, idr.IdTypeDesc, SS.[Description]
SET NOCOUNT ON
END


--pick up patinets w/ ID Reference LbPatientIds who are in Demographics but not OrgHierarchy
IF @DemoNoOrg = 1
BEGIN
SET NOCOUNT OFF
--select 'Patient in Demographic,not in Org?' 
SELECT DISTINCT  pd.FirstName, pd.MiddleName, pd.LastName, pd.BirthDate, pd.Gender
FROM vwPatientDemographic pd 
JOIN PatientIdReference idr ON pd.PatientId = idr.LbPatientId
LEFT JOIN OrgHierarchy o ON idr.LbPatientId = o.LbPatientId
WHERE o.ID IS NULL
--yields 0 results, so all patinets not in OrgHierarchy are deleted?
SET NOCOUNT ON
END

--
--select idr.SourceSystemId, idr.IdTypeDesc, ss.[Description], count(distinct idr.LbPatientId) AS [DistinctPatientIds]
--FROM PatientIdReference idr
--join vwPortal_Patient pp on idr.LbPatientId = pp.PatientId
--left join OrgHierarchy o on idr.LbPatientId = o.LbPatientId
--left join SourceSystem ss on idr.SourceSystemId = ss.SourceSystemId
--where o.ID is null
--group by idr.SourceSystemId, idr.IdTypeDesc, ss.[Description]
--
--
--select distinct pp.*
--FROM PatientIdReference idr
--join vwPatientDemographic pp on idr.LbPatientId = pp.PatientId
--left join OrgHierarchy o on idr.LbPatientId = o.LbPatientId
--where o.ID is null

--Interface side
--interface patient info - fname,lname,dob,gen,patientId (PatientIdentifier), Should all be in OrgHierarchy & PatientIdReference
--merge based on demographics if a field is currently null
--don't allow patient to be added if a patient is missing any 1 of 4 required elements
SELECT DISTINCT  Utoken , PatientIdentifier, PatientOtherIdentifier, FirstName, LastName, DOBDateTime, GenderId, 0 as MissingFromOrg
INTO #IntPats
FROM vwPortal_InterfacePatientMatched
WHERE FirstName IS NOT NULL AND LastName IS NOT NULL AND DOBDateTime IS NOT NULL AND GenderId IS NOT NULL

--add LbPatientId
ALTER TABLE #IntPats ADD LbPatientId INT NULL

--find patient IDs based on PatientOtherIdentifier
UPDATE ip SET LbPatientId = idr.LbPatientId FROM #IntPats ip 
JOIN PatientIdReference idr ON idr.ExternalId = ip.PatientOtherIdentifier

--find leftovers based on PatientIdentifier
UPDATE ip SET LbPatientId = idr.LbPatientId FROM #IntPats ip 
JOIN PatientIdReference idr ON idr.ExternalId = ip.PatientIdentifier

-- find using Utoken
UPDATE ip SET LbPatientId = idr.LbPatientId FROM #IntPats ip 
JOIN PatientIdReference idr ON idr.ExternalId = ip.Utoken

--find patients missing from OrgHierarchy
UPDATE #IntPats SET MissingFromOrg = 1 FROM #IntPats IP
LEFT JOIN OrgHierarchy O ON O.LbPatientId = IP.LbPatientId
WHERE O.ID IS NULL

IF @InterfacePatientNoIDR = 1
BEGIN
SET NOCOUNT OFF
--Interface Patients missing from ID Reference
--select 'Interface Patients missing from ID Reference?'
SELECT distinct  IP.PatientIdentifier, IP.PatientOtherIdentifier, IP.FirstName, IP.LastName, IP.DOBDateTime, IP.GenderId, IP.LbPatientId, IP.MissingFromOrg
FROM #IntPats IP
LEFT JOIN PatientIdReference IDR ON IP.PatientOtherIdentifier = IDR.ExternalId OR IP.PatientIdentifier = IDR.ExternalId OR IP.Utoken = IDR.ExternalId
WHERE IDR.PatientIdReferenceId IS NULL
SET NOCOUNT ON 
END

IF @InterfacePatientNoOrg = 1
BEGIN
SET nocount OFF
--FIND MISSING PATIENTS
--select '--FIND MISSING PATIENTS'
SELECT distinct PatientIdentifier, PatientOtherIdentifier, FirstName, LastName, DOBDateTime, GenderId, LbPatientId, MissingFromOrg 
FROM #IntPats WHERE MissingFromOrg = 1
SET nocount ON
end


IF @DeletedIndividual = 1
BEGIN
SET NOCOUNT OFF
--count w/ a deleted individual record
SELECT PP.PatientId, INN.FirstName, INN.LastName, PI.BirthDate, PP.DeleteInd AS [PatientDelete], 
PI.DeleteInd AS [IndividualDelete], INN.DeleteInd AS [IndividualNameDelete]
FROM #IntPats IP 
JOIN vwPortal_Patient PP ON IP.LbPatientId = PP.PatientId
JOIN vwPortal_Individual PI ON PP.IndividualID = PI.IndividualID
JOIN vwPortal_IndividualName INN ON PI.IndividualID = INN.IndividualID
WHERE MissingFromOrg = 1
SET nocount ON
end


IF @SummaryStats = 1
BEGIN 
CREATE TABLE #SummaryStats (
TITLE VARCHAR(100),
MetricValue INT)

INSERT INTO #SummaryStats (TITLE,MetricValue)
SELECT 'External IDs from Claims Missing from ID Reference', COUNT(distinct T1.ExternalId) AS [MetricValue] FROM #TMP_IDs T1
LEFT JOIN PatientIdReference IDR ON T1.ExternalId = IDR.ExternalId
WHERE IDR.LbPatientId IS NULL

INSERT INTO #SummaryStats (TITLE,MetricValue)
--check lbpatient IDs in ID Reference, but in the 'from' ID List in dedupe
SELECT 'ID Reference - Wrong Duplicate ID', COUNT(DISTINCT idr.LbPatientId)
FROM DedupedPatientIdHistory idh
JOIN PatientIdReference idr ON idr.LbPatientId = idh.PatientId

INSERT INTO #SummaryStats (TITLE,MetricValue)
SELECT 'InterfacePatient Records Missing from OrgHierarchy', COUNT(*) FROM #IntPats WHERE MissingFromOrg = 1

INSERT INTO #SummaryStats (TITLE,MetricValue)
SELECT 'Patients in ID Reference from '+ISNULL(SS.[Description],'Unknown')+', SS ID:'+ISNULL(try_convert(VARCHAR(100),idr.SourceSystemId),'N/A')+' not in OrgHierarchy', COUNT(DISTINCT idr.LbPatientId) AS [DistinctPatientIds]
FROM PatientIdReference idr
LEFT JOIN OrgHierarchy o ON idr.LbPatientId = o.LbPatientId
LEFT JOIN SourceSystem SS ON idr.SourceSystemId = SS.SourceSystemId
WHERE o.ID IS NULL
GROUP BY 'Patients in ID Reference from '+ISNULL(SS.[Description],'Unknown')+', SS ID:'+ISNULL(try_convert(VARCHAR(100),idr.SourceSystemId),'N/A')+' not in OrgHierarchy'


INSERT INTO #SummaryStats (TITLE,MetricValue)
--Interface Patients missing from ID Reference
SELECT 'InterfacePatient Records Missing from ID Reference (Not Distinct)', COUNT(*)
FROM #IntPats IP
LEFT JOIN PatientIdReference IDR ON IP.PatientOtherIdentifier = IDR.ExternalId OR IP.PatientIdentifier = IDR.ExternalId Or IP.Utoken =  IDR.ExternalId
WHERE IDR.PatientIdReferenceId IS NULL


SET nocount OFF
SELECT * FROM #SummaryStats
SET nocount ON
end

IF @UnmatchedClaims = 1
BEGIN 
CREATE TABLE #MissingTmp (
Status varchar(50) NOT NULL,
ClmTable varchar(100) NOT NULL, 
SourceFeed varchar(100) NOT NULL,
[RowCount] INT NULL,
SumPaid DECIMAL(18,5))

INSERT INTO #MissingTmp (Status, ClmTable, SourceFeed, [RowCount], SumPaid)
SELECT
CASE 
  WHEN O.ID IS NOT NULL then 'Matched to Patient'
  else 'Unmatched Claim'
END as [Status],
'PartB' as [ClmTable], ISNULL(B.SourceFeed, 'MSSP') AS SourceFeed, ISNULL(COUNT(*),0) AS [RowCount], sum(ISNULL(B.CLM_LINE_CVRD_PD_AMT,0)) as [SumPaid]  
FROM CCLF_5_PartB_Physicians B
LEFT JOIN OrgHierarchy O ON B.LbPatientId = O.LbPatientId
WHERE O.ID IS NULL
group by CASE 
  WHEN O.ID IS NOT NULL THEN 'Matched to Patient'
  else 'Unmatched Claim'
END, ISNULL(B.SourceFeed, 'MSSP')

INSERT INTO #MissingTmp (Status, ClmTable, SourceFeed, [RowCount], SumPaid)
SELECT
CASE 
  WHEN O.ID IS NOT NULL THEN 'Matched to Patient'
  else 'Unmatched Claim'
END as [Status], 'PartA' as [ClmTable], ISNULL(A.SourceFeed, 'MSSP'), ISNULL(COUNT(*),0) AS [RowCount], sum(ISNULL(A.CLM_PMT_AMT,0))
FROM CCLF_1_PartA_Header A
LEFT JOIN OrgHierarchy O ON A.LbPatientId = O.LbPatientId
group by CASE 
  WHEN O.ID IS NOT NULL THEN 'Matched to Patient'
  else 'Unmatched Claim'
END, ISNULL(A.SourceFeed, 'MSSP')

INSERT INTO #MissingTmp (Status, ClmTable, SourceFeed, [RowCount], SumPaid)
SELECT CASE 
  WHEN O.ID IS NOT NULL THEN 'Matched to Patient'
  else 'Unmatched Claim'
END,'PARTB DME' AS [ClmTable], ISNULL(SourceFeed, 'MSSP'), ISNULL(COUNT(*),0) AS [RowCount], sum(ISNULL(CLM_LINE_CVRD_PD_AMT,0))
FROM CCLF_6_PartB_DME DME
LEFT JOIN OrgHierarchy O ON DME.LbPatientId = O.LbPatientId
GROUP BY CASE 
  WHEN O.ID IS NOT NULL THEN 'Matched to Patient'
  else 'Unmatched Claim'
END,ISNULL(SourceFeed, 'MSSP')

INSERT INTO #MissingTmp (Status, ClmTable, SourceFeed, [RowCount], SumPaid)
SELECT
CASE 
  WHEN O.ID IS NOT NULL THEN 'Matched to Patient'
  else 'Unmatched Claim'
END as [Status], 'PARTD RX', ISNULL(D.SourceFeed, 'MSSP') as SourceFeed, ISNULL(COUNT(*),0) AS [RowCount], sum(ISNULL(D.CLM_LINE_BENE_PMT_AMT,0)+ISNULL(D.CLM_LINE_PAYER_PAID_AMT,0))
FROM CCLF_7_PartD D
LEFT JOIN OrgHierarchy O ON D.LbPatientId = O.LbPatientId
group by CASE 
  WHEN O.ID IS NOT NULL THEN 'Matched to Patient'
  else 'Unmatched Claim'
END, ISNULL(D.SourceFeed, 'MSSP')

SET NOCOUNT OFF
SELECT * FROM #MissingTmp
SET NOCOUNT ON
END
GO
