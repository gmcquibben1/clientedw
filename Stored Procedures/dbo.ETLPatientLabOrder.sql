SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ETLPatientLabOrder] 
	@fullLoadFlag bit = 0, 
	@BatchSize INT = 250, 
	@InterfacePatientId VARCHAR(50) = NULL,
	@overrideMinIndex INT = NULL,
	@overrideMaxIndex INT = NULL
AS
BEGIN	



/* ================================================================
 Author:		Youping Liu
 Create date: 2016-03-29
 Description:	Load PatientLabOrder Data into EDW
 Notes: 
 		 @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
		 @fullLoadFlag =0 Merge date to the existing table
		 @BatchSize = Batch size to run for each iteration
		 @InterfacePatientId = Scope the run for a specific patient
		 @overrideMinIndex INT = NULL - When pulling data what is the starting index we want to pull data from 


 Main Steps: 
        1. truncate table PatientLabOrderProcessQueue
        2. Populated data to PatientLabOrderProcessQueue from source
        3. Get LBPatient ID from PatientIdReference
        4. Dedup based on SourceSystemId, LbPatientID, OrderNumber 
        5. Merge data to PatientLabOrder
        6. Call ETLPatientLabResult



 Log Tracking:
        1. track table the last run date: dbo.EDWTableLoadTracking
        2. log the procedure running status


	Version		Date		Author	Change
	-------		----------	------	------	
	2.2.1		2016-09-29	YL	    LBETL-143 Fix Missing lab results due to incremental load based on laborder created date only
											It needs to use both laborder created date and lab result created date.
	2.2.1		2017-02-02	YL	    LBETL-631 maxSourceTimeStamp update only when Que has data
	2.2.1		2017-02-02	CL	    LBETL-788 Rollup multiple records to the latest non-null value
	
===============================================================*/


	SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientLabOrder';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientLabOrder';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @RowCount INT =0;
	DECLARE @NumRows INT = 0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);

	 BEGIN TRY


	




		--1) Retrieve the last run time id values.  We are going to pull both the date and the last run time id. If the last max id 
		--is null, calculate a best guese based on the 
		--We are changing away from using date values for query purposes as it is more efficient to query by the Id value.  Since
		--the id is a seeded, integer greater id values are implicitly newer. It also allows us to pick up records that come in while the 
		--procedure is running the next time it runs
		IF @fullLoadFlag = 1
		BEGIN
			UPDATE Maintenance.dbo.EDWTableLoadTracking 
				SET maxSourceTimeStampProcessed = CAST('1900-01-01' AS DATETIME), maxSourceIdProcessed= 0
				WHERE EDWtableName = @EDWtableName AND dataSource ='interface' AND EDWName =@EDWName
		END

		 SET @lastDateTime = (SELECT COALESCE(maxSourceTimeStampProcessed,CAST('1900-01-01' AS DATETIME)) 
									FROM Maintenance.dbo.EDWTableLoadTracking 
									WHERE EDWtableName = @EDWtableName 
									AND dataSource ='interface' AND EDWName =@EDWName);
	
	
		
		SET  @lastIndex = (SELECT maxSourceIdProcessed
									FROM Maintenance.dbo.EDWTableLoadTracking 
									WHERE EDWtableName = @EDWtableName 
									AND dataSource ='interface' AND EDWName =@EDWName);

		IF @lastIndex IS NULL OR @lastIndex = 0 
		BEGIN
			SET @lastIndex= (SELECT MIN(InterfacePatientLabOrderId) FROM  vwPortal_InterfacePatientLabOrder IPV WITH (NOLOCK) WHERE IPV.CreateDateTime >= @lastDateTime ) 
		END
		IF @overrideMinIndex IS NOT NULL
		BEGIN
			SET @lastIndex= @overrideMinIndex
		END


		SET @maxIndex = (SELECT MAX(InterfacePatientLabOrderId) FROM  vwPortal_InterfacePatientLabOrder IPV WITH (NOLOCK));
		IF @overrideMaxIndex IS NOT NULL
		BEGIN
			SET @maxIndex = @overrideMaxIndex
		END
	
		--2) Get the full list of patient identifiers that we are going to be dealing with
		--We are going to breaking this up into batches along patient id lines.
		SELECT DISTINCT IP.PatientIdentifier, IPV.InterfaceSystemId INTO #PatientLabOrderQuePatientIdentifier 
			FROM dbo.vwPortal_InterfacePatientLabOrder IPV with (nolock)
				INNER JOIN dbo.vwPortal_InterfacePatient IP with (nolock) ON IPV.InterfacePatientId = IP.InterfacePatientId AND  IPV.InterfaceSystemId = IP.InterfaceSystemId
				INNER JOIN dbo.vwPortal_InterfaceSystem  ISS with (nolock) ON IP.InterfaceSystemId = ISS.InterfaceSystemId
				WHERE ( IPV.InterfacePatientLabOrderId >= @lastIndex  AND IPV.InterfacePatientLabOrderId <= @maxIndex )
				AND ( ( @InterfacePatientId IS NULL )  OR ( @InterfacePatientId = IP.InterfacePatientId))
				--AND ServiceDateTime IS NOT NULL 
		





			SET @RecordCountBefore= (SELECT SUM(row_count)
			FROM sys.dm_db_partition_stats
			WHERE object_id=OBJECT_ID('PatientLabOrder')   
			AND (index_id=0 or index_id=1))


		
		--3) Loop throught all of the batches to improve memory efficency
		WHILE 1 = 1
		BEGIN
    			SELECT TOP (@BatchSize) PatientIdentifier, InterfaceSystemId 
					INTO #PatientLabOrderQuePatientIdentifieBATCH FROM #PatientLabOrderQuePatientIdentifier;
				Create  NONCLUSTERED INDEX IDX_PatientLabOrderQuePatientIdentifieBATCH_PatientIdentifier ON #PatientLabOrderQuePatientIdentifieBATCH (PatientIdentifier);
			
    			SET @NUMROWS = ( SELECT COUNT(1) FROM #PatientLabOrderQuePatientIdentifieBATCH)

			IF @NUMROWS <> 0 
			BEGIN
			
				-- 1.remove records from PatientLabOrderProcessQueue
				TRUNCATE TABLE PatientLabOrderProcessQueue


	

							-- test select @maxSourceDateTime, @lastDateTime 

							-- 2. populate data into PatientLabOrderProcessQueue
							INSERT INTO dbo.PatientLabOrderProcessQueue
									   (InterfacePatientLabOrderId
									   ,LbPatientId
									   ,InterfacePatientId
									   ,InterfaceSystemId
									   ,OrderNumber
									   ,ServiceId
									   ,ServiceDescription
									   ,ServiceCodingSystemName
									   ,OrderingProviderId
									   ,OrderingProviderIdType
									   ,OrderingProviderName
									   ,CreateDateTime
									   ,InterfacePatientLabResultId
									   ,ObservationCode1
									   ,ObservationDescription1
									   ,ObservationCodingSystemName1
									   ,ObservationCode2
									   ,ObservationDescription2
									   ,ObservationCodingSystemName2
									   ,Value
									   ,Units
									   ,ReferenceRange
									   ,AbnormalFlag
									   ,ResultStatus
									   ,ResultComment
									   ,ObservationDate
									   ,LR_CreateDateTime
									   --,PatientLabOrderId
									   --,ProcessedInd
									   ,EncounterIdentifier
									   ,OrderDate)

							SELECT  LO.InterfacePatientLabOrderId
							, idRef.LbPatientId  --  LO.InterfacePatientId
							,idRef.LbPatientId
							,ISS.SourceSystemId			--LO.InterfaceSystemId
							,ISNULL(LO.OrderNumber,0) OrderNumber
							,ISNULL(LO.ServiceId,0) ServiceId
							,ISNULL(LO.ServiceDescription,'') ServiceDescription
							,LO.ServiceCodingSystemName
							,LO.OrderingProviderId
							,LO.OrderingProviderIdType
							,ISNULL(LO.OrderingProviderName,'') OrderingProviderName
							,LO.CreateDateTime
							,LR.InterfacePatientLabResultId
							,LR.ObservationCode1
							,LR.ObservationDescription1
							,LR.ObservationCodingSystemName1
							,LR.ObservationCode2
							,LR.ObservationDescription2
							,LR.ObservationCodingSystemName2
							,LR.Value
							,LR.Units
							,LR.ReferenceRange
							,LR.AbnormalFlag
							,LR.ResultStatus
							,LR.ResultComment
							,LR.ObservationDate
							,LR.CreateDateTime AS LR_CreateDateTime
							,LO.EncounterIdentifier
							,LO.OrderDate
							-- test into #test1
							FROM 
									dbo.vwPortal_InterfacePatient IP with (nolock), 
									dbo.vwPortal_InterfaceSystem  ISS with (nolock), 
									 #PatientLabOrderQuePatientIdentifieBATCH batch, 
									 PatientIdReference idRef with (nolock),
									 dbo.vwPortal_InterfacePatientLabOrder LO 
								LEFT OUTER JOIN  dbo.vwPortal_InterfacePatientLabResult LR ON LO.InterfacePatientLabOrderId = LR.InterfacePatientLabOrderId
								WHERE (LO.InterfacePatientLabOrderId >= @lastIndex  AND LO.InterfacePatientLabOrderId <= @maxIndex)
								  AND  LO.InterfacePatientId = IP.InterfacePatientId AND  IP.InterfaceSystemId = ISS.InterfaceSystemId 
								AND IP.PatientIdentifier = batch.PatientIdentifier AND IP.InterfaceSystemId = batch.InterfaceSystemId 	
								AND  idRef.SourceSystemId = ISS.SourceSystemId 
									  AND idRef.ExternalId = batch.PatientIdentifier 
									  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
									  AND idRef.DeleteInd =0	



							ALTER INDEX IX_PatientLabOrderProcessQueue_PatIdOrderNumSource ON PatientLabOrderProcessQueue
							REBUILD;
			
						   -- 3. Get LBPatient ID from PatientIdReference 
						   -- select count(1) from #test1
						   -- create nonclustered index CIX_#patientLabOrder_PatientIdentifier on #test1 (InterfacePatientId) include (InterfaceSystemId);

		   					   					SELECT  DISTINCT --t1.InterfacePatientLabOrderId
									t1.LbPatientId AS PatientID
									--,t1.InterfacePatientId
									,t1.SourceSystemId		--We are storing the source system id of the 
									,t1.OrderNumber
									,SI.ServiceId
									,SD.ServiceDescription 
									,SC.ServiceCodingSystemName
									,OPI.OrderingProviderId
									,OPIT.OrderingProviderIdType
									,OPN.OrderingProviderName 
									,EI.EncounterIdentifier
									,OD.OrderDate
									,t1.CreateDateTime
							 	   , t1.rowNumber
							INTO #tmpPatientLabOrder
							FROM 
								(SELECT DISTINCT lbPatientId,  InterfaceSystemId As SourceSystemId,  OrderNumber,  OrderDate, EncounterIdentifier,CreateDateTime,
								ROW_NUMBER() OVER (PARTITION BY lbPatientId,  OrderNumber,  InterfaceSystemId ORDER BY InterfacePatientLabOrderId DESC) rowNumber 
								FROM PatientLabOrderProcessQueue WITH (NOLOCK)) t1 
  
						LEFT OUTER JOIN
								(SELECT DISTINCT lbPatientId, OrderNumber, InterfaceSystemId As SourceSystemId, ServiceId, ROW_NUMBER() OVER 
								(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId  ORDER BY InterfacePatientLabOrderId DESC) rowNumber 
								FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE ServiceId IS NOT NULL) SI 
								ON SI.lbPatientId = t1.lbPatientId 	AND SI.OrderNumber = t1.OrderNumber  AND SI.SourceSystemId = t1.SourceSystemId  
						LEFT OUTER JOIN
								(SELECT DISTINCT lbPatientId, OrderNumber, InterfaceSystemId As SourceSystemId, ServiceDescription, ROW_NUMBER() OVER 
								(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId  ORDER BY InterfacePatientLabOrderId DESC) rowNumber 
								FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE ServiceDescription IS NOT NULL) SD 
								ON SD.lbPatientId = t1.lbPatientId 	AND SD.OrderNumber = t1.OrderNumber  AND SD.SourceSystemId = t1.SourceSystemId
						LEFT OUTER JOIN
								(SELECT DISTINCT lbPatientId, OrderNumber, InterfaceSystemId As SourceSystemId, ServiceCodingSystemName, ROW_NUMBER() OVER 
								(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId  ORDER BY InterfacePatientLabOrderId DESC) rowNumber 
								FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE ServiceCodingSystemName IS NOT NULL) SC 
								ON SC.lbPatientId = t1.lbPatientId 	AND SC.OrderNumber = t1.OrderNumber  AND SC.SourceSystemId = t1.SourceSystemId
						LEFT OUTER JOIN
								(SELECT DISTINCT lbPatientId, OrderNumber, InterfaceSystemId As SourceSystemId, OrderingProviderId, ROW_NUMBER() OVER 
								(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId  ORDER BY InterfacePatientLabOrderId DESC) rowNumber 
								FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE OrderingProviderId IS NOT NULL) OPI 
								ON OPI.lbPatientId = t1.lbPatientId 	AND OPI.OrderNumber = t1.OrderNumber  AND OPI.SourceSystemId = t1.SourceSystemId
						LEFT OUTER JOIN
								(SELECT DISTINCT lbPatientId, OrderNumber, InterfaceSystemId As SourceSystemId, OrderingProviderIdType, ROW_NUMBER() OVER 
								(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId  ORDER BY InterfacePatientLabOrderId DESC) rowNumber 
								FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE OrderingProviderIdType IS NOT NULL) OPIT 
								ON OPIT.lbPatientId = t1.lbPatientId 	AND OPIT.OrderNumber = t1.OrderNumber  AND OPIT.SourceSystemId = t1.SourceSystemId
						LEFT OUTER JOIN
								(SELECT DISTINCT lbPatientId, OrderNumber, InterfaceSystemId As SourceSystemId, OrderingProviderName, ROW_NUMBER() OVER 
								(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId  ORDER BY InterfacePatientLabOrderId DESC) rowNumber 
								FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE OrderingProviderName IS NOT NULL) OPN 
								ON OPN.lbPatientId = t1.lbPatientId 	AND OPN.OrderNumber = t1.OrderNumber  AND OPN.SourceSystemId = t1.SourceSystemId
						LEFT OUTER JOIN
								(SELECT lbPatientId, OrderNumber, InterfaceSystemId As SourceSystemId, EncounterIdentifier, ROW_NUMBER() OVER 
								(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId  ORDER BY InterfacePatientLabOrderId DESC) rowNumber 
								FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE EncounterIdentifier IS NOT NULL) EI 
								ON EI.lbPatientId = t1.lbPatientId 	AND EI.OrderNumber = t1.OrderNumber  AND EI.SourceSystemId = t1.SourceSystemId
					    LEFT OUTER JOIN
								(SELECT lbPatientId, OrderNumber, InterfaceSystemId As SourceSystemId, OrderDate, ROW_NUMBER() OVER 
								(PARTITION BY lbPatientId, OrderNumber, InterfaceSystemId  ORDER BY InterfacePatientLabOrderId DESC) rowNumber 
								FROM PatientLabOrderProcessQueue  v1 WITH (NOLOCK) WHERE OrderDate IS NOT NULL AND EncounterIdentifier <> '') OD 
								ON OD.lbPatientId = t1.lbPatientId 	AND OD.OrderNumber = t1.OrderNumber  AND OD.SourceSystemId = t1.SourceSystemId
						WHERE t1.rowNumber = 1
							AND (SI.rowNumber = 1 OR SI.rowNumber IS NULL)
							AND (SD.rowNumber = 1 OR SD.rowNumber IS NULL)
							AND (SC.rowNumber = 1 OR SC.rowNumber IS NULL)
							AND (OPI.rowNumber = 1 OR OPI.rowNumber IS NULL)
							AND (OPIT.rowNumber = 1 OR OPIT.rowNumber IS NULL)
							AND (OPN.rowNumber = 1 OR OPN.rowNumber IS NULL)
							AND (EI.rowNumber = 1 OR EI.rowNumber IS NULL)
							AND (OD.rowNumber = 1 OR OD.rowNumber IS NULL)

							----  4. load deduped patient lab order data into temp table fisrt, data deduped by SourceSystemId, LbPatientID, OrderNumber 

							-- get source records counts
							SET @RecordCountSource=@@ROWCOUNT;

			
							---for merge key
							CREATE CLUSTERED INDEX CIX_#patientLabOrder_PatientID ON #tmpPatientLabOrder(PatientID,SourceSystemID,OrderNumber);


							-- 5. Merge to dbo.PatientLabOrder
			
							MERGE DBO.PatientLabOrder AS target
							USING #tmpPatientLabOrder AS source 
							ON (target.SourceSystemID = source.SourceSystemId AND target.PatientID = source.PatientID AND target.OrderNumber = source.OrderNumber)
							WHEN MATCHED  THEN UPDATE 
							SET target.SourceSystemId = ISNULL(source.SourceSystemId,  target.SourceSystemId ),
								target.OrderNumber = ISNULL(source.OrderNumber, target.OrderNumber), 
								target.ServiceId = ISNULL(source.ServiceId, target.ServiceId), 
								target.ServiceDescription = ISNULL(source.ServiceDescription, target.ServiceDescription),
								target.ServiceCodingSystemName = ISNULL(source.ServiceCodingSystemName, target.ServiceCodingSystemName),
								target.OrderingProviderId = ISNULL(source.OrderingProviderId, target.OrderingProviderId ),
								target.OrderingProviderIdType = ISNULL(source.OrderingProviderIdType, target.OrderingProviderIdType ),
								target.OrderingProviderName = ISNULL(source.OrderingProviderName, target.OrderingProviderName),
								--CreateDateTime = source.CreateDateTime,
								target.ModifyDateTime = GETUTCDATE() , 
								target.ModifyLBUserId = 1,
								target.EncounterIdentifier = ISNULL( source.EncounterIdentifier, target.EncounterIdentifier),
								target.OrderDate = ISNULL(source.OrderDate , target.OrderDate )

							WHEN NOT MATCHED THEN
							INSERT (PatientId,SourceSystemId,OrderNumber,ServiceId,ServiceDescription
								  ,ServiceCodingSystemName ,OrderingProviderId ,OrderingProviderIdType,OrderingProviderName,CreateDateTime
								  ,ModifyDateTime,CreateLBUserId,ModifyLBUserId
								  ,EncounterIdentifier,OrderDate)
							VALUES (source.PatientId,source.SourceSystemId,source.OrderNumber,source.ServiceId,source.ServiceDescription
								  ,source.ServiceCodingSystemName ,source.OrderingProviderId ,source.OrderingProviderIdType,source.OrderingProviderName,source.CreateDateTime
								  ,GETUTCDATE(), 1, 1
								  ,source.EncounterIdentifier,source.OrderDate)

									;


		  
							  --clean tables
							DROP TABLE #tmpPatientLabOrder;
			
							-- 6. Call procedure ETLPatientLabResult to populate table PatientLabResult
							 EXEC  dbo.ETLPatientLabResult
   							--Update the max processed time and drop the temp tables
							
							--SET @tmpmaxSourceTimeStampProcessed = 	(SELECT MAX(CreateDateTime) FROM #PatientLabOrderQue )
							--IF @tmpmaxSourceTimeStampProcessed > @maxSourceTimeStampProcessed
							--BEGIN
							--SET @maxSourceTimeStampProcessed = @tmpmaxSourceTimeStampProcessed
							--END


					

				END
			
			
			--Move onto the next batch
			DELETE FROM #PatientLabOrderQuePatientIdentifier FROM 
				#PatientLabOrderQuePatientIdentifier id, #PatientLabOrderQuePatientIdentifieBATCH batch
			WHERE id.PatientIdentifier = batch.PatientIdentifier AND id.interfacesystemid  = batch.interfacesystemid
			DROP TABLE #PatientLabOrderQuePatientIdentifieBATCH
	
	
			--DECLARE @tmpcount INT
			--SET @tmpcount = (SELECT COUNT(1) FROM #PatientLabOrderQuePatientIdentifier)
			--Print @tmpcount
			--INSERT INTO PatientLabOrderQuePatientIdentifierCount ( count, dt) VALUES ( @tmpcount, getdate())
	
			IF @NUMROWS = 0 BREAK;
  
		END
		




		DROP table #PatientLabOrderQuePatientIdentifier
		-- update lastdate




	-- get total records from dbo.PatientLabOrder after merge 

			
			SET @RecordCountAfter= (SELECT SUM(row_count)
			FROM sys.dm_db_partition_stats
			WHERE object_id=OBJECT_ID('PatientLabOrder')   
			AND (index_id=0 or index_id=1))


    -- Calculate records inserted and updated counts
	    SET @InsertedRecord= ABS( @RecordCountAfter -@RecordCountBefore)
	    SET @UpdatedRecord = ABS(@RecordCountSource - @InsertedRecord)
         		   
		-- update maxSourceTimeStamp with max CreateDateTime of #PatientLabOrderQue
		UPDATE Maintenance..EDWTableLoadTracking
		SET maxSourceTimeStampProcessed = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex, updatedatetime = GETUTCDATE()
		WHERE EDWtableName = @EDWtableName AND dataSource ='interface'  AND EDWName = @EDWName


		-- get total records from PatientLabOrder after merge 
		SELECT @RecordCountAfter=COUNT(1)  FROM PatientLabOrder WITH (NOLOCK);

		-- Calculate records inserted and updated counts
		SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
		SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

		-- insert log information to Maintenance.dbo.EdwProcedureRunLog
		SET  @EndTime=GETUTCDATE();
		EXEC Maintenance.dbo.EdwInsertProcedureRunLog @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;

        
 

					

	END TRY

		BEGIN CATCH
	           --- insert error log information to Maintenance.dbo.EdwProcedureRunLog
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC Maintenance.dbo.EdwInsertProcedureRunLog @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END



GO
