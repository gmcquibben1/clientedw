SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GProMeasureHistoryPopulate]
AS 
/*===============================================================
	CREATED BY: 	William Keen
	CREATED ON:		2016-12-19
	INITIAL VER:	2.2.1
	MODULE:			GPro		
	DESCRIPTION:	Populates the GProMeasureHistory table for a given year. This year is hardcoded on purpose.

	RETURN VALUE(s)/OUTPUT:	No return values. 
	PROGRAMMING NOTES: 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.1.1		12/19/2016		WK						Initial Version
	2.2.1		2/9/2017		WK						Updating the SP to add ConsecutiveTarget and RemainingToTarget. Renaming 2 columns in #TempMeasureHistory
	2.2.2		4/19/2017		WK						Fixing MH-1, PREV-11 and PREV-13

=================================================================*/
BEGIN
	DECLARE @MeasureYear INT = 2016
	
	--This table holds the results of EXEC GProMeasureProgressListLoad @BusinessUnitId
	CREATE TABLE #TempMeasureHistory(
		RowNumber						INT				NULL,
		MeasureTypeId					INT				NULL,	
		MeasureTypeAbbr					VARCHAR(50)		NULL,	
		MeasureTypeDesc					VARCHAR(500)	NULL,
		RankingTableColumnName			VARCHAR(50)		NULL,
		TotalPatients					INT				NULL,
		TotalComplete					INT				NULL,
		ConsecutiveCount				INT				NULL,
		TotalSkipped					INT				NULL,
		PercentConsecutive				FLOAT			NULL,
		StatusDesc						VARCHAR(20)		NULL,
		PercentTotal					INT				NULL,
		ConsecutiveTarget				INT				NULL,
		RemainingToTarget				INT				NULL,
	)
	--DELETE FROM GProMeasureProgressHistory WHERE MeasureYear = @MeasureYear
	DECLARE @BusinessUnitId INT
	DECLARE @BusinessUnitName VARCHAR(50)

	CREATE TABLE #BusinessUnits(
		BusinessUnitId INT 
	)
	IF ((SELECT DB_NAME()) LIKE 'NracoEdw%')
		BEGIN
			--INSERT INTO #BusinessUnits (BusinessUnitId) 
			--	(SELECT v1.BusinessUnitId FROM vwPortal_BusinessUnit v1 
			--	 WHERE v1.GroupTin IS NOT NULL AND v1.DeleteInd = 0)
			--INSERT INTO #BusinessUnits (BusinessUnitId) 
			--	(SELECT v2.BusinessUnitId FROM vwPortal_BusinessUnit v1 
			--	 JOIN vwPortal_BusinessUnit v2 ON v1.BusinessUnitId = v2.ParentBusinessUnitId
			--	 WHERE v1.GroupTin IS NOT NULL AND v1.DeleteInd = 0 AND v2.DeleteInd = 0)
			WITH BizUnit (ParentBusinessUnitId,BusinessUnitId, Name, BusinessUnitTypeId, ExternalCode, DeleteInd)
			AS (SELECT e.ParentBusinessUnitId,e.BusinessUnitId, e.Name, e.BusinessUnitTypeId, e.ExternalCode, e.DeleteInd
				FROM vwPortal_BusinessUnit e WHERE e.GroupTin IS NOT NULL AND e.DeleteInd = 0),
			BizUnitTree (ParentBusinessUnitId,BusinessUnitId, BizUnitName, BizUnitType, BizUnitExtCode, DeleteInd)
			AS
			(
				SELECT e.ParentBusinessUnitId,e.BusinessUnitId, e.Name AS BizUnitName, e.BusinessUnitTypeId, e.ExternalCode, e.DeleteInd
				FROM BizUnit AS e  
				UNION ALL
				SELECT e.ParentBusinessUnitId,e.BusinessUnitId, e.Name AS BizUnitName, e.BusinessUnitTypeId, e.ExternalCode, e.DeleteInd
				FROM vwPortal_BusinessUnit AS e
				INNER JOIN BizUnitTree AS d ON d.BusinessUnitId = e.ParentBusinessUnitId								
			)
			INSERT INTO #BusinessUnits (BusinessUnitId)
				SELECT BusinessUnitId FROM BizUnitTree WHERE DeleteInd = 0

			--DELETE FROM #BusinessUnits
			--INSERT INTO #BusinessUnits (BusinessUnitId) VALUES (536)
			--INSERT INTO #BusinessUnits (BusinessUnitId) VALUES (550)
			--INSERT INTO #BusinessUnits (BusinessUnitId) VALUES (738)
			--INSERT INTO #BusinessUnits (BusinessUnitId) VALUES (743)
			--INSERT INTO #BusinessUnits (BusinessUnitId) VALUES (747)
			--INSERT INTO #BusinessUnits (BusinessUnitId) VALUES (737)
			--INSERT INTO #BusinessUnits (BusinessUnitId) VALUES (748)
		END
	ELSE
		BEGIN
			INSERT INTO #BusinessUnits (BusinessUnitId) 
				(SELECT DISTINCT BusinessUnitId FROM vwPortal_BusinessUnit WHERE DeleteInd = 0)
		END	

	DECLARE BusinessUnitCursor CURSOR FOR SELECT DISTINCT BusinessUnitId FROM #BusinessUnits
	OPEN BusinessUnitCursor
	FETCH NEXT FROM BusinessUnitCursor INTO @BusinessUnitId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @BusinessUnitName = (SELECT Name FROM vwPortal_BusinessUnit WHERE BusinessUnitId = @BusinessUnitId)
		TRUNCATE TABLE #TempMeasureHistory

		--Updated the SP to take in a bit flag. If set to true, the SP will load into #TempMeasureHistory instead of returning results.
		EXEC GProMeasureProgressListLoad @BusinessUnitId, NULL, 1

		INSERT INTO GProMeasureProgressHistory (MeasureYear, BusinessUnitId, BusinessUnitName, MeasureTypeId, MeasureTypeName, MeasureTypeDescription, RankingTableColumnName, TotalPatients, TotalComplete, ConsecutiveCount, TotalSkipped, PercentConsecutive, MeasureStatus, ConsecutiveTarget, RemainingToTarget)
			SELECT @MeasureYear, @BusinessUnitId, @BusinessUnitName, MeasureTypeId, MeasureTypeAbbr, MeasureTypeDesc, RankingTableColumnName, TotalPatients, TotalComplete, ConsecutiveCount, TotalSkipped, PercentConsecutive, StatusDesc, ConsecutiveTarget, RemainingToTarget
			FROM #TempMeasureHistory

		FETCH NEXT FROM BusinessUnitCursor INTO @BusinessUnitId
	END
	CLOSE BusinessUnitCursor
	DEALLOCATE BusinessUnitCursor
	DROP TABLE #TempMeasureHistory

	CREATE TABLE #TempGProPatientRanking (
		GProPatientRankingId	INT, 
		FirstName				VARCHAR(25), 
		LastName				VARCHAR(20), 
		BirthDate				DATE, 
		GenderCode				INT, 
		MedicareHicn			VARCHAR(15), 
		GProStatusTypeId		VARCHAR(50),
		MeasureRank				INT,
		BusinessUnitName		VARCHAR(250)
		)

	DECLARE @BeginDate						DATE = CAST(CAST(@MeasureYear AS VARCHAR) +'-01-01' AS DATE)
	DECLARE @EndDate						DATE = CAST(CAST(@MeasureYear AS VARCHAR) +'-12-31' AS DATE)
	DECLARE @75YearsAgo						DATE = DATEADD(YEAR, -75, @BeginDate)
	DECLARE @40YearsAgo						DATE = DATEADD(YEAR, -40, @BeginDate)
	DECLARE @21YearsAgo						DATE = DATEADD(YEAR, -21, @BeginDate)
	DECLARE @MeasureTypeName				VARCHAR(50)
	DECLARE @MeasureTypeId					INT
	DECLARE @FirstIncomplete				INT

	DECLARE @CMSMeasureMet					INT
	DECLARE @CMSDenominatorException		INT
	DECLARE @CMSDenominator					INT

	DECLARE @CompletedMeasureMet			INT
	DECLARE @CompletedDenominatorException	INT
	DECLARE @CompletedDenominator			INT
	DECLARE @TotalSkipped					INT

	DECLARE MeasureProgressHistoryCursor CURSOR FOR SELECT BusinessUnitId, MeasureTypeId, MeasureTypeName FROM GProMeasureProgressHistory WHERE MeasureYear = @MeasureYear AND BusinessUnitId = 536
	OPEN MeasureProgressHistoryCursor
	FETCH NEXT FROM MeasureProgressHistoryCursor INTO @BusinessUnitId, @MeasureTypeId, @MeasureTypeName
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM #TempGProPatientRanking

		EXEC GproMeasureRankListLoad @BusinessUnitId, @MeasureTypeId, 2, NULL, 1
		SET @FirstIncomplete = ISNULL((SELECT TOP 1 MeasureRank FROM #TempGProPatientRanking WHERE GProStatusTypeId = 'Incomplete' ORDER BY MeasureRank), 99999999)
		SET @TotalSkipped = (SELECT COUNT(1) FROM #TempGProPatientRanking WHERE GProStatusTypeId = 'Skipped' )

		SET @CMSMeasureMet = NULL
		SET @CMSDenominatorException = 0
		SET @CMSDenominator = (SELECT COUNT(1) FROM #TempGProPatientRanking WHERE GProStatusTypeId = 'Complete' AND MeasureRank < @FirstIncomplete)

		SET @CompletedMeasureMet = NULL
		SET @CompletedDenominatorException = 0
		SET @CompletedDenominator =  (SELECT COUNT(1) FROM #TempGProPatientRanking WHERE GProStatusTypeId = 'Complete' )

		IF(@MeasureTypeName = 'CARE-2')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.CareFallsConfirmed, gpm.FallsScreening
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.CareFallsConfirmed = 2 
				AND gpm.FallsScreening = 2
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) --gpm.CareFallsConfirmed, gpm.FallsScreening
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.CareFallsConfirmed = 2 
				AND gpm.FallsScreening = 4			
			)

			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.CareFallsConfirmed, gpm.FallsScreening
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.CareFallsConfirmed = 2 
				AND gpm.FallsScreening = 2
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.CareFallsConfirmed, gpm.FallsScreening
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.CareFallsConfirmed = 2 
				AND gpm.FallsScreening = 4			
			)
		END
		ELSE IF(@MeasureTypeName = 'CARE-3')
		BEGIN
			SET @CMSDenominator =
			(
				SELECT COUNT(1) --gpm.MedicalRecordFound, gpm.CareMedDocConfirmed --, gpm.CareMedDocVisitDate, gpm.CareMedDocVisitConfirmation, gpm.MedicationsDocumented, gpm.CareMedDocComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				JOIN GProPatientMedication pm ON gpm.GProPatientRankingId = pm.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete 
				AND gpm.MedicalRecordFound = 2 
				AND (gpm.CareMeddocConfirmed IS NULL OR gpm.CareMedDocConfirmed = 2)
				AND CAST(pm.CareMeddocVisitDate AS DATE) BETWEEN @BeginDate AND @EndDate
				AND pm.CareMeddocVisitConfirmed = 2
			)
			SET @CMSMeasureMet = 
			(
				SELECT COUNT(1) --gpm.MedicalRecordFound, gpm.CareMedDocConfirmed --, gpm.CareMedDocVisitDate, gpm.CareMedDocVisitConfirmation, gpm.MedicationsDocumented, gpm.CareMedDocComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				JOIN GProPatientMedication pm ON gpm.GProPatientRankingId = pm.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete 
				AND gpm.MedicalRecordFound = 2 
				AND (gpm.CareMeddocConfirmed IS NULL OR gpm.CareMedDocConfirmed = 2)
				AND CAST(pm.CareMeddocVisitDate AS DATE) BETWEEN @BeginDate AND @EndDate
				AND pm.CareMeddocVisitConfirmed = 2
				AND pm.MedicationDocumented = 2
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) --gpm.MedicalRecordFound, gpm.CareMedDocConfirmed --, gpm.CareMedDocVisitDate, gpm.CareMedDocVisitConfirmation, gpm.MedicationsDocumented, gpm.CareMedDocComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				JOIN GProPatientMedication pm ON gpm.GProPatientRankingId = pm.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete 
				AND gpm.MedicalRecordFound = 2 
				AND gpm.CareMedDocConfirmed = 2
				AND CAST(pm.CareMeddocVisitDate AS DATE) BETWEEN @BeginDate AND @EndDate
				AND pm.CareMeddocVisitConfirmed = 2
				AND pm.MedicationDocumented = 4
			)
			SET @CompletedDenominator =
			(
				SELECT COUNT(1) --gpm.MedicalRecordFound, gpm.CareMedDocConfirmed --, gpm.CareMedDocVisitDate, gpm.CareMedDocVisitConfirmation, gpm.MedicationsDocumented, gpm.CareMedDocComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				JOIN GProPatientMedication pm ON gpm.GProPatientRankingId = pm.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.MedicalRecordFound = 2 
				AND gpm.CareMedDocConfirmed = 2
				AND CAST(pm.CareMeddocVisitDate AS DATE) BETWEEN @BeginDate AND @EndDate
				AND pm.CareMeddocVisitConfirmed = 2
			)
			SET @CompletedMeasureMet = 
			(
				SELECT COUNT(1) --gpm.MedicalRecordFound, gpm.CareMedDocConfirmed --, gpm.CareMedDocVisitDate, gpm.CareMedDocVisitConfirmation, gpm.MedicationsDocumented, gpm.CareMedDocComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				JOIN GProPatientMedication pm ON gpm.GProPatientRankingId = pm.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.MedicalRecordFound = 2 
				AND gpm.CareMedDocConfirmed = 2
				AND CAST(pm.CareMeddocVisitDate AS DATE) BETWEEN @BeginDate AND @EndDate
				AND pm.CareMeddocVisitConfirmed = 2
				AND pm.MedicationDocumented = 2
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.MedicalRecordFound, gpm.CareMedDocConfirmed --, gpm.CareMedDocVisitDate, gpm.CareMedDocVisitConfirmation, gpm.MedicationsDocumented, gpm.CareMedDocComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				JOIN GProPatientMedication pm ON gpm.GProPatientRankingId = pm.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.MedicalRecordFound = 2 
				AND gpm.CareMedDocConfirmed = 2
				AND CAST(pm.CareMeddocVisitDate AS DATE) BETWEEN @BeginDate AND @EndDate
				AND pm.CareMeddocVisitConfirmed = 2
				AND pm.MedicationDocumented = 4
			)
		END
		ELSE IF(@MeasureTypeName = 'CAD-7')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.CadConfirmed, gpm.CadDiabetesLvsd, gpm.CadAceArb
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.CadConfirmed = 2 
				AND gpm.CadDiabetesLvsd = 2 
				AND gpm.CadAceArb = 2
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) --gpm.CadConfirmed, gpm.CadDiabetesLvsd, gpm.CadAceArb
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.CadAceArb IN (4,5,6)
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.CadConfirmed, gpm.CadDiabetesLvsd, gpm.CadAceArb
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.CadConfirmed = 2 
				AND gpm.CadDiabetesLvsd = 2 
				AND gpm.CadAceArb = 2
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.CadConfirmed, gpm.CadDiabetesLvsd, gpm.CadAceArb
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.CadAceArb IN (4,5,6)
			)
		END
		ELSE IF(@MeasureTypeName = 'DM-2')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.DmConfirmed, gpm.DmHba1cTest, CAST(gpm.DmHba1cDate AS DATE) AS DmHba1cDate, gpm.DmHba1cValue
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.DmConfirmed = 2
				AND 
				(
					gpm.DmHba1cTest = 1
					OR
					(
						gpm.DmHba1cTest = 2
						AND CAST(gpm.DmHba1cDate AS DATE) BETWEEN @BeginDate AND @EndDate
						AND gpm.DmHba1cValue > 9.0 OR gpm.DmHba1cValue = 0
					)
				)
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.DmConfirmed, gpm.DmHba1cTest, CAST(gpm.DmHba1cDate AS DATE) AS DmHba1cDate, gpm.DmHba1cValue
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.DmConfirmed = 2
				AND 
				(
					gpm.DmHba1cTest = 1
					OR
					(
						gpm.DmHba1cTest = 2
						AND CAST(gpm.DmHba1cDate AS DATE) BETWEEN @BeginDate AND @EndDate
						AND gpm.DmHba1cValue > 9.0
					)
				)
			)
		END
		ELSE IF(@MeasureTypeName = 'DM-7')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.DmConfirmed, gpm.DmEyeExam
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.DmConfirmed = 2
				AND gpm.DmEyeExam = 2 
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.DmConfirmed, gpm.DmEyeExam
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.DmConfirmed = 2
				AND gpm.DmEyeExam = 2 
			)
		END	
		ELSE IF(@MeasureTypeName = 'HF-6')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.HfConfirmed, gpm.HfLvsd, gpm.HfBetaBlocker
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.HfConfirmed = 2
				AND gpm.HfLvsd = 2
				AND gpm.HfBetaBlocker = 2
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) --gpm.HfConfirmed, gpm.HfLvsd, gpm.HfBetaBlocker
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.HfConfirmed = 2
				AND gpm.HfLvsd = 2
				AND gpm.HfBetaBlocker IN (4,5,6)
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.HfConfirmed, gpm.HfLvsd, gpm.HfBetaBlocker
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.HfConfirmed = 2
				AND gpm.HfLvsd = 2
				AND gpm.HfBetaBlocker = 2
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.HfConfirmed, gpm.HfLvsd, gpm.HfBetaBlocker
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.HfConfirmed = 2
				AND gpm.HfLvsd = 2
				AND gpm.HfBetaBlocker IN (4,5,6)
			)
		END	
		ELSE IF(@MeasureTypeName = 'HTN-2')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.HtnConfirmed, gpm.HtnRecentBp, CAST(gpm.HtnBpDate AS DATE) as HtnBpDate, gpm.HtnBpDiastolic, gpm.HtnBpSystolic
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.HtnConfirmed= 2
				AND gpm.HtnRecentBp = 1
				AND CAST(gpm.HtnBpDate AS DATE) BETWEEN @BeginDate AND @EndDate
				AND gpm.HtnBpDiastolic < 90
				AND gpm.HtnBpSystolic < 140
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.HtnConfirmed, gpm.HtnRecentBp, CAST(gpm.HtnBpDate AS DATE) as HtnBpDate, gpm.HtnBpDiastolic, gpm.HtnBpSystolic
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId 
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.HtnConfirmed= 2
				AND gpm.HtnRecentBp = 1
				AND CAST(gpm.HtnBpDate AS DATE) BETWEEN @BeginDate AND @EndDate
				AND gpm.HtnBpDiastolic < 90
				AND gpm.HtnBpSystolic < 140
			)
		END	
		ELSE IF(@MeasureTypeName = 'IVD-2')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.IvdConfirmed, gpm.IvdAntithrombotic
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.IvdConfirmed = 2 
				AND gpm.IvdAntithrombotic = 1
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.IvdConfirmed, gpm.IvdAntithrombotic
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.IvdConfirmed = 2 
				AND gpm.IvdAntithrombotic = 1
			)
		END	
		ELSE IF(@MeasureTypeName = 'MH-1')
		BEGIN
			SET @CMSDenominator = 
			(
				SELECT	COUNT(1) 
				--gpm.MhConfirmed, gpm.MhIndexPerformed, gpm.MhIndexTest, gpm.MhIndexDate, gpm.MhIndexScore, gpm.MhFollowupPerformed, gpm.MhFollowupTest, gpm.MhFollowupDate, gpm.MhFollowupScore, gpm.MhComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId AND t.MeasureRank < @FirstIncomplete
				WHERE t.GProStatusTypeId <> 'Incomplete'
				AND gpm.MhConfirmed = 2
				AND gpm.MhIndexPerformed = 2
				AND gpm.MhIndexTest = 2
			) 
			SET @CMSMeasureMet = 	
			(
				SELECT	COUNT(1) --gpm.MhConfirmed, gpm.MhIndexPerformed, gpm.MhIndexTest, gpm.MhIndexDate, gpm.MhIndexScore, gpm.MhFollowupPerformed, gpm.MhFollowupTest, gpm.MhFollowupDate, gpm.MhFollowupScore, gpm.MhComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId AND t.MeasureRank < @FirstIncomplete
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.MhConfirmed = 2
				AND gpm.MhIndexPerformed = 2
				AND gpm.MhIndexTest = 2 
				AND gpm.MhIndexDate IS NOT NULL 
				AND gpm.MhIndexScore IS NOT NULL
				AND gpm.MhIndexScore >= 5 
				AND gpm.MhFollowupPerformed = 2 
				AND gpm.MhFollowupTest = 2 
				AND gpm.MhFollowupDate IS NOT NULL
				AND gpm.MhFollowupDate BETWEEN DateAdd(month, 11,gpm.MhIndexDate) AND DateAdd(month, 13,gpm.MhIndexDate)
				AND gpm.MhFollowupScore < 5
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT	COUNT(1) --gpm.MhConfirmed, gpm.MhIndexPerformed, gpm.MhIndexTest, gpm.MhIndexDate, gpm.MhIndexScore, gpm.MhFollowupPerformed, gpm.MhFollowupTest, gpm.MhFollowupDate, gpm.MhFollowupScore, gpm.MhComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.MhConfirmed = 2
				AND gpm.MhIndexPerformed = 2
				AND gpm.MhIndexTest = 2 
				AND gpm.MhIndexDate IS NOT NULL 
				AND gpm.MhIndexScore IS NOT NULL
				AND gpm.MhIndexScore >= 5 
				AND gpm.MhFollowupPerformed = 2 
				AND gpm.MhFollowupTest = 2 
				AND gpm.MhFollowupDate IS NOT NULL
				AND gpm.MhFollowupDate BETWEEN DateAdd(month, 11,gpm.MhIndexDate) AND DateAdd(month, 13,gpm.MhIndexDate)
				AND gpm.MhFollowupScore < 5
			)
		END	
		ELSE IF(@MeasureTypeName = 'PREV-5')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcMammogramConfirmed, gpm.PcMammogramPerformed, gpm.PcMammogramComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcMammogramPerformed = 1
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcMammogramConfirmed, gpm.PcMammogramPerformed, gpm.PcMammogramComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcMammogramPerformed = 1
			)
		END	
		ELSE IF(@MeasureTypeName = 'PREV-6')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcColorectalConfirmed, gpm.PcColorectalPerformed, gpm.PcColorectalComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcColorectalConfirmed = 2
				AND gpm.PcColorectalPerformed = 1
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcColorectalConfirmed, gpm.PcColorectalPerformed, gpm.PcColorectalComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcColorectalConfirmed = 2
				AND gpm.PcColorectalPerformed = 1
			)
		END	
		ELSE IF(@MeasureTypeName = 'PREV-7')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcFluShotConfirmed, gpm.PcFluShotReceived, gpm.PcFluShotComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcFluShotConfirmed = 2
				AND gpm.PcFluShotReceived = 2
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcFluShotConfirmed, gpm.PcFluShotReceived, gpm.PcFluShotComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcFluShotConfirmed = 2
				AND gpm.PcFluShotReceived IN (4,5,6)
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcFluShotConfirmed, gpm.PcFluShotReceived, gpm.PcFluShotComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcFluShotConfirmed = 2
				AND gpm.PcFluShotReceived = 2
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcFluShotConfirmed, gpm.PcFluShotReceived, gpm.PcFluShotComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcFluShotConfirmed = 2
				AND gpm.PcFluShotReceived IN (4,5,6)
			)
		END	
		ELSE IF(@MeasureTypeName = 'PREV-8')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcPneumoShotConfirmed, gpm.PcPneumoShotReceived, gpm.PcPneumoShotComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcPneumoShotConfirmed = 2
				AND gpm.PcPneumoShotReceived = 1
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcPneumoShotConfirmed, gpm.PcPneumoShotReceived, gpm.PcPneumoShotComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.PcPneumoShotConfirmed = 2
				AND gpm.PcPneumoShotReceived = 1
			)
		END	
		ELSE IF(@MeasureTypeName = 'PREV-9')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcBmiScreenConfirmed, gpm.PcBmiCalculated, gpm.PcBmiNormal, gpm.PcBmiPlan, gpm.PcBmiComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcBmiScreenConfirmed = 2
				AND gpm.PcBmiCalculated = 2
				AND 
				(
					gpm.PcBmiNormal = 1 
					OR 
					(gpm.PcBmiNormal = 0 AND gpm.PcBmiPlan = 1)
				)
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcBmiScreenConfirmed, gpm.PcBmiCalculated, gpm.PcBmiNormal, gpm.PcBmiPlan, gpm.PcBmiComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcBmiScreenConfirmed = 2
				AND gpm.PcBmiCalculated IN (4,5)
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcBmiScreenConfirmed, gpm.PcBmiCalculated, gpm.PcBmiNormal, gpm.PcBmiPlan, gpm.PcBmiComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcBmiScreenConfirmed = 2
				AND gpm.PcBmiCalculated = 2
				AND 
				(
					gpm.PcBmiNormal = 1 
					OR 
					(gpm.PcBmiNormal = 0 AND gpm.PcBmiPlan = 1)
				)
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcBmiScreenConfirmed, gpm.PcBmiCalculated, gpm.PcBmiNormal, gpm.PcBmiPlan, gpm.PcBmiComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcBmiScreenConfirmed = 2
				AND gpm.PcBmiCalculated IN (4,5)
			)
		END	
		ELSE IF(@MeasureTypeName = 'PREV-10')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcTobaccoConfirmed, gpm.PcTobaccoScreen, gpm.PcTobaccoCounsel, gpm.PcTobaccoComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcTobaccoConfirmed = 2
				AND 
				(
					gpm.PcTobaccoScreen = 1
					OR
					(gpm.PcTobaccoScreen = 2 AND gpm.PcTobaccoCounsel = 1)
				)
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcTobaccoConfirmed, gpm.PcTobaccoScreen, gpm.PcTobaccoCounsel, gpm.PcTobaccoComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcTobaccoConfirmed = 2
				AND gpm.PcTobaccoScreen = 4
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcTobaccoConfirmed, gpm.PcTobaccoScreen, gpm.PcTobaccoCounsel, gpm.PcTobaccoComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcTobaccoConfirmed = 2
				AND 
				(
					gpm.PcTobaccoScreen = 1
					OR
					(gpm.PcTobaccoScreen = 2 AND gpm.PcTobaccoCounsel = 1)
				)
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcTobaccoConfirmed, gpm.PcTobaccoScreen, gpm.PcTobaccoCounsel, gpm.PcTobaccoComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcTobaccoConfirmed = 2
				AND gpm.PcTobaccoScreen = 4
			)
		END	
		ELSE IF(@MeasureTypeName = 'PREV-11')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcBloodPressureConfirmed, gpm.PcBloodPressureScreen, gpm.PcBloodPressureNormal, gpm.PcBloodPressureFollowUp, gpm.PcBloodPressureComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcBloodPressureConfirmed = 2
				AND gpm.PcBloodPressureScreen = 2
				AND 
				(
					gpm.PcBloodPressureNormal = 1
					OR 
					(gpm.PcBloodPressureNormal = 0 AND gpm.PcBloodPressureFollowUp = 2)
				)
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) AS DenominatorException 
				--gpm.PcBloodPressureConfirmed, gpm.PcBloodPressureScreen, gpm.PcBloodPressureNormal, gpm.PcBloodPressureFollowUp, gpm.PcBloodPressureComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId AND t.MeasureRank < @FirstIncomplete
				WHERE t.GProStatusTypeId = 'Complete'
				AND gpm.PcBloodPressureConfirmed = 2
				AND
				( 
					(gpm.PcBloodPressureScreen = 2 AND PcBloodPressureNormal = 0 AND PcBloodPressureFollowUp = 5)
					OR
					gpm.PcBloodPressureScreen IN (4,5)
				)
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcBloodPressureConfirmed, gpm.PcBloodPressureScreen, gpm.PcBloodPressureNormal, gpm.PcBloodPressureFollowUp, gpm.PcBloodPressureComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcBloodPressureConfirmed = 2
				AND gpm.PcBloodPressureScreen = 2
				AND 
				(
					gpm.PcBloodPressureNormal = 1
					OR 
					(gpm.PcBloodPressureNormal = 0 AND gpm.PcBloodPressureFollowUp = 2)
				)
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcBloodPressureConfirmed, gpm.PcBloodPressureScreen, gpm.PcBloodPressureNormal, gpm.PcBloodPressureFollowUp, gpm.PcBloodPressureComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcBloodPressureConfirmed = 2
				AND gpm.PcBloodPressureScreen IN (4,5)
			)
		END		
		ELSE IF(@MeasureTypeName = 'PREV-12')
		BEGIN
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcDepressionConfirmed, gpm.PcDepressionScreen, gpm.PcDepressionPositive, gpm.PcDepressionPlan, gpm.PcDepressionComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcDepressionConfirmed = 2
				AND gpm.PcDepressionScreen = 2
				AND 
				(
					gpm.PcDepressionPositive = 1
					OR
					(gpm.PcDepressionPositive = 2 AND gpm.PcDepressionPlan = 2)
				)
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcDepressionConfirmed, gpm.PcDepressionScreen, gpm.PcDepressionPositive, gpm.PcDepressionPlan, gpm.PcDepressionComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND gpm.PcDepressionConfirmed = 2
				AND gpm.PcDepressionScreen IN (4,5)
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcDepressionConfirmed, gpm.PcDepressionScreen, gpm.PcDepressionPositive, gpm.PcDepressionPlan, gpm.PcDepressionComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcDepressionConfirmed = 2
				AND gpm.PcDepressionScreen = 2
				AND 
				(
					gpm.PcDepressionPositive = 1
					OR
					(gpm.PcDepressionPositive = 2 AND gpm.PcDepressionPlan = 2)
				)
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcDepressionConfirmed, gpm.PcDepressionScreen, gpm.PcDepressionPositive, gpm.PcDepressionPlan, gpm.PcDepressionComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' 
				AND gpm.PcDepressionConfirmed = 2
				AND gpm.PcDepressionScreen IN (4,5)
			)
		END
		ELSE IF(@MeasureTypeName = 'PREV-13')
		BEGIN
			SET @CMSDenominator =
			(
				SELECT COUNT(1) --gpm.PcDepressionConfirmed, gpm.PcDepressionScreen, gpm.PcDepressionPositive, gpm.PcDepressionPlan, gpm.PcDepressionComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND t.BirthDate <= @21YearsAgo											--Over 21
				AND 
				(
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 2)			--ASCVD Diagnosed = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 2)			--LDL-C > or = 190 mg/dL = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 1		AND	--LDL-C > or = 190 mg/dL = No
						t.BirthDate >= @75YearsAgo AND t.BirthDate <= @40YearsAgo	AND --Patient is between ages of 40 and 75
						gpm.PcStatinDiabetes = 2									AND --Patient diagnosed with Diabetes
						gpm.PcStatinLdlcReading = 2)									--Highest LDL-C reading in last three years is between 70-189 mg/dL.
				)
			)
			SET @CMSMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcDepressionConfirmed, gpm.PcDepressionScreen, gpm.PcDepressionPositive, gpm.PcDepressionPlan, gpm.PcDepressionComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND t.BirthDate <= @21YearsAgo											--Over 21
				AND 
				(
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 2)			--ASCVD Diagnosed = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 2)			--LDL-C > or = 190 mg/dL = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 1		AND	--LDL-C > or = 190 mg/dL = No
						t.BirthDate >= @75YearsAgo AND t.BirthDate <= @40YearsAgo	AND --Patient is between ages of 40 and 75
						gpm.PcStatinDiabetes = 2									AND --Patient diagnosed with Diabetes
						gpm.PcStatinLdlcReading = 2)									--Highest LDL-C reading in last three years is between 70-189 mg/dL.
				)
				AND gpm.PcStatinPrescribed = 2											--Statin prescribed
			)
			SET @CMSDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcDepressionConfirmed, gpm.PcDepressionScreen, gpm.PcDepressionPositive, gpm.PcDepressionPlan, gpm.PcDepressionComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete' AND t.MeasureRank < @FirstIncomplete
				AND t.BirthDate <= @21YearsAgo											--Over 21
				AND 
				(
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 2)			--ASCVD Diagnosed = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 2)			--LDL-C > or = 190 mg/dL = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 1		AND	--LDL-C > or = 190 mg/dL = No
						t.BirthDate >= @75YearsAgo AND t.BirthDate <= @40YearsAgo	AND --Patient is between ages of 40 and 75
						gpm.PcStatinDiabetes = 2									AND --Patient diagnosed with Diabetes
						gpm.PcStatinLdlcReading = 2)									--Highest LDL-C reading in last three years is between 70-189 mg/dL.
				)
				AND gpm.PcStatinPrescribed = 4											--No prescribed, medical exclusion
			)
			SET @CompletedMeasureMet = 	
			(
				SELECT COUNT(1) --gpm.PcDepressionConfirmed, gpm.PcDepressionScreen, gpm.PcDepressionPositive, gpm.PcDepressionPlan, gpm.PcDepressionComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND t.BirthDate <= @21YearsAgo											--Over 21
				AND 
				(
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 2)			--ASCVD Diagnosed = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 2)			--LDL-C > or = 190 mg/dL = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 1		AND	--LDL-C > or = 190 mg/dL = No
						t.BirthDate >= @75YearsAgo AND t.BirthDate <= @40YearsAgo	AND --Patient is between ages of 40 and 75
						gpm.PcStatinDiabetes = 2									AND --Patient diagnosed with Diabetes
						gpm.PcStatinLdlcReading = 2)									--Highest LDL-C reading in last three years is between 70-189 mg/dL.
				)
				AND gpm.PcStatinPrescribed = 2											--Statin prescribed
			)
			SET @CompletedDenominatorException = 
			(
				SELECT COUNT(1) --gpm.PcDepressionConfirmed, gpm.PcDepressionScreen, gpm.PcDepressionPositive, gpm.PcDepressionPlan, gpm.PcDepressionComments
				FROM GProPatientMeasure gpm 
				JOIN #TempGProPatientRanking t ON gpm.GProPatientRankingId = t.GProPatientRankingId
				WHERE t.GProStatusTypeId = 'Complete'
				AND t.BirthDate <= @21YearsAgo											--Over 21
				AND 
				(
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 2)			--ASCVD Diagnosed = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 2)			--LDL-C > or = 190 mg/dL = Yes
					OR
					(gpm.PcStatinAscvd IS NOT NULL AND gpm.PcStatinAscvd = 23		AND	--ASCVD Diagnosed = No-Diagnosis					
						gpm.PcStatinLdlc IS NOT NULL AND gpm.PcStatinLdlc = 1		AND	--LDL-C > or = 190 mg/dL = No
						t.BirthDate >= @75YearsAgo AND t.BirthDate <= @40YearsAgo	AND --Patient is between ages of 40 and 75
						gpm.PcStatinDiabetes = 2									AND --Patient diagnosed with Diabetes
						gpm.PcStatinLdlcReading = 2)									--Highest LDL-C reading in last three years is between 70-189 mg/dL.
				)
				AND gpm.PcStatinPrescribed = 4											--No prescribed, medical exclusion
			)
		END	

		SET @FirstIncomplete = (SELECT TOP 1 MeasureRank FROM #TempGProPatientRanking WHERE GProStatusTypeId = 'Incomplete' ORDER BY MeasureRank)
		UPDATE GProMeasureProgressHistory SET 
		CMSTotalEligible = ISNULL(@CMSDenominator, 0),
		CMSDenominator = ISNULL(@CMSDenominator - @CMSDenominatorException, 0),
		CMSDenominatorException = ISNULL(@CMSDenominatorException, 0), 
		CMSMeasureNotMet = ISNULL(@CMSDenominator - @CMSDenominatorException - @CMSMeasureMet, 0),
		CMSMeasureMet = ISNULL(@CMSMeasureMet, 0), 
		CMSPercentMeasureMet = FORMAT(ISNULL(@CMSMeasureMet * 100.0/NULLIF(@CMSDenominator - @CMSDenominatorException, 0) , 0), 'N2'),
		CompletedTotalEligible = ISNULL(@CompletedDenominator, 0),
		CompletedDenominator = ISNULL(@CompletedDenominator - @CompletedDenominatorException, 0),
		CompletedMeasureNotMet = ISNULL(@CompletedDenominator - @CompletedDenominatorException - @CompletedMeasureMet, 0),
		CompletedDenominatorException = ISNULL(@CompletedDenominatorException, 0), 
		CompletedMeasureMet = ISNULL(@CompletedMeasureMet, 0), 
		CompletedPercentMeasureMet = FORMAT(ISNULL(@CompletedMeasureMet * 100.0/NULLIF(@CompletedDenominator - @CompletedDenominatorException, 0) , 0), 'N2'),
		GetMeasureRankResultsTotalSkipped = @TotalSkipped,
		GetMeasureRankResultsFirstIncomplete = @FirstIncomplete
		WHERE BusinessUnitId = @BusinessUnitId AND MeasureTypeId = @MeasureTypeId AND MeasureYear = @MeasureYear
		FETCH NEXT FROM MeasureProgressHistoryCursor INTO @BusinessUnitId, @MeasureTypeId, @MeasureTypeName
	END
	CLOSE MeasureProgressHistoryCursor
	DEALLOCATE MeasureProgressHistoryCursor
	DROP TABLE #TempGProPatientRanking

	UPDATE g
	SET g.CMSBenchmark = (
		CASE 
		WHEN gmb.MeasureRateLowBound IS NULL AND gmb.Benchmark = 30 THEN NULL
		WHEN gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark = 30 AND g.CMSPercentMeasureMet < gmb.MeasureRateLowBound THEN 0
		WHEN gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark = 90 AND g.CMSPercentMeasureMet >= gmb.MeasureRateLowBound THEN gmb.Benchmark
		WHEN gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark <> 90 AND g.CMSPercentMeasureMet >= gmb.MeasureRateLowBound AND g.CMSPercentMeasureMet < gmb.MeasureRateHighBound THEN gmb.Benchmark
		ELSE g.CMSBenchmark END
	)
	FROM GProMeasureProgressHistory g
	JOIN GProMeasureBenchmark gmb 
	ON g.MeasureTypeId = gmb.GProMeasureTypeId AND g.MeasureYear = gmb.MeasureYear
	WHERE 
	   (gmb.MeasureRateLowBound IS NULL AND gmb.Benchmark = 30)
	OR (gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark = 30 AND g.CMSPercentMeasureMet < gmb.MeasureRateLowBound)
	OR (gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark = 90 AND g.CMSPercentMeasureMet >= gmb.MeasureRateLowBound)
	OR (gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark <> 90 AND g.CMSPercentMeasureMet >= gmb.MeasureRateLowBound AND g.CMSPercentMeasureMet < gmb.MeasureRateHighBound )

	UPDATE g
	SET g.CompletedBenchmark = (
		CASE 
		WHEN gmb.MeasureRateLowBound IS NULL THEN NULL
		WHEN gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark = 30 AND g.CompletedPercentMeasureMet < gmb.MeasureRateLowBound THEN 0
		WHEN gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark = 90 AND g.CompletedPercentMeasureMet >= gmb.MeasureRateLowBound THEN gmb.Benchmark
		WHEN gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark != 90 AND g.CompletedPercentMeasureMet >= gmb.MeasureRateLowBound AND g.CompletedPercentMeasureMet < gmb.MeasureRateHighBound THEN gmb.Benchmark
		ELSE g.CompletedBenchmark END
	)
	FROM GProMeasureProgressHistory g
	JOIN GProMeasureBenchmark gmb 
	ON g.MeasureTypeId = gmb.GProMeasureTypeId 	AND g.MeasureYear = gmb.MeasureYear
	WHERE 
	   (gmb.MeasureRateLowBound IS NULL AND gmb.Benchmark = 30)
	OR (gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark = 30 AND g.CompletedPercentMeasureMet < gmb.MeasureRateLowBound)
	OR (gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark = 90 AND g.CompletedPercentMeasureMet >= gmb.MeasureRateLowBound)
	OR (gmb.MeasureRateLowBound IS NOT NULL AND gmb.Benchmark <> 90 AND g.CompletedPercentMeasureMet >= gmb.MeasureRateLowBound AND g.CompletedPercentMeasureMet < gmb.MeasureRateHighBound )

	IF ((SELECT DB_NAME()) = 'NracoEdwProd')
	BEGIN
		EXEC GproMeasureDetailLoadALLACO
	END
END
GO
