SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE  [dbo].[MeasureConfigurationLoad]
@ContractId INT = NULL
AS


--This stored procedure is used to load the measure reference data from the EDW reference master database
--CJL 07.14.16
--LBPP--1669
--
DECLARE @TSQL  NVARCHAR(MAX)

IF EXISTS(SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CMeasure_DefinitionContractType'  )
BEGIN

	PRINT '1'
	SET @TSQL = N'SELECT DISTINCT  ContractTypeId as ContractId, cd.measureid, 
	measureprogram, measurecategory,measuresubcategory, 
	MeasureName, MeasureAbbr, CAST(cdt.highlow as bit) as ''highlow'', CAST(0 as bit) as ImportantFlag,
	cd.StoredProcedure, cd.MeasureContractID, '''' as ValueSetMeasureAbbr, 
	CAST( ISNULL(cd.EnforceEligibilty, 0) as bit) as EnforceEligibilty,CAST( 0 as bit) as UseLBSupplementCodesInd, CAST( 0 as bit) as UseClinicalDataOnlyInd, 
	CAST( CASE cd.TaskingNeeded  WHEN ''Y'' THEN 1 ELSE 0 END as bit) as TaskingNeeded, '''' as MeasureYear,
	CAST( CASE cdt.enableflag  WHEN ''Y'' THEN 1 ELSE 0 END as bit) as enableflag, avg, nat30, nat90
	FROM [CMeasure_DefinitionsContractType] cdt WITH (NOLOCK), 
	[CMeasure_Definitions] cd  WITH (NOLOCK)
	WHERE ContractTypeId = 1 AND  cd.MeasureContractId = contracttypeid
	order by  measureprogram, measurecategory,measuresubcategory, MeasureName'



END
ELSE
BEGIN


	IF NOT EXISTS(SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CMeasure_Definitions' AND COLUMN_NAME = 'MeasureYear' )
	BEGIN


	PRINT '2'
		SET @TSQL = N'SELECT DISTINCT @ContractId as ContractId, cd.measureid, measureprogram, measurecategory,measuresubcategory, MeasureName, MeasureAbbr, CAST( highlow as bit) as ''highlow'', CAST(0 as bit) as ImportantFlag,
	cd.StoredProcedure, cd.MeasureContractID, '''' as ValueSetMeasureAbbr, 
	CAST( ISNULL(cd.EnforceEligibilty, 0) as bit) as EnforceEligibilty,CAST( 0 as bit) as UseLBSupplementCodesInd, CAST( 0 as bit) as UseClinicalDataOnlyInd, 
	CAST( CASE cd.TaskingNeeded  WHEN ''Y'' THEN 1 ELSE 0 END as bit) as TaskingNeeded, '''' as MeasureYear,
	CAST( CASE enableflag  WHEN ''Y'' THEN 1 ELSE 0 END as bit) as enableflag, mt.avg, mt.nat30, mt.nat90
	FROM CMeasure_Definitions cd WITH (NOLOCK) LEFT JOIN 
	ACO_Measure_NatAvgTargets mt WITH (NOLOCK) ON  mt.MeasureID =  cd.MeasureID
	order by  measureprogram, measurecategory,measuresubcategory, MeasureName'

	END
	ELSE
	BEGIN


	PRINT '3'
		SET @TSQL = N'SELECT DISTINCT @ContractId as ContractId, cd.measureid, measureprogram, measurecategory,measuresubcategory, MeasureName, MeasureAbbr, CAST( highlow as bit) as ''highlow'', CAST(0 as bit) as ImportantFlag,
		cd.StoredProcedure, cd.MeasureContractID, ValueSetMeasureAbbr,  
		CAST( ISNULL(cd.EnforceEligibilty, 0) as bit) as EnforceEligibilty, CAST( cd.UseLBSupplementCodesInd as bit) AS UseLBSupplementCodesInd , 
		CAST( cd.UseClinicalDataOnlyInd as bit) as UseClinicalDataOnlyInd, CAST( CASE cd.TaskingNeeded  WHEN ''Y'' THEN 1 ELSE 0 END as bit) as TaskingNeeded, MeasureYear,
		CAST( CASE enableflag  WHEN ''Y'' THEN 1 ELSE 0 END as bit) as enableflag, mt.avg, mt.nat30, mt.nat90
		FROM CMeasure_Definitions cd WITH (NOLOCK) LEFT JOIN 
		ACO_Measure_NatAvgTargets mt WITH (NOLOCK) ON  mt.MeasureID =  cd.MeasureID
		order by  measureprogram, measurecategory,measuresubcategory, MeasureName'
	END
END

DECLARE @ParmDefinition nvarchar(500); 
SET @ParmDefinition = N'@ContractId int';  
exec sp_executesql  @TSQL ,@ParmDefinition,  @ContractId
GO
