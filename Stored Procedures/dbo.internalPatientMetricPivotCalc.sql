SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalPatientMetricPivotCalc]
AS
--declaring varialbes
DECLARE @colBottom NVARCHAR(MAX), @SQL NVARCHAR(MAX), @createTable NVARCHAR(MAX), @colTable NVARCHAR(MAX)

--dropping table if exist

set @SQL = 
'IF OBJECT_ID(''dbo.PatientMetricPivot'', ''U'') IS NOT NULL
  DROP TABLE dbo.PatientMetricPivot'
  
  EXECUTE sp_executesql @SQL



--concatenating the distinct items of column title in one row using FOR XML PATH
SET @colTable = STUFF((SELECT DISTINCT ', ['+CAST(MetricName AS VARCHAR(100))+']'+ ' INT' FROM PatientMetric FOR XML PATH('')),1,1, N'');
SET @colBottom = STUFF((SELECT DISTINCT ', ['+CAST(MetricName AS VARCHAR(100))+']' FROM PatientMetric FOR XML PATH('')),1,1, N'');

--creating a table
SET @createTable = N'Create Table PatientMetricPivot (Id int IDENTITY(1,1) PRIMARY KEY, LbPatientId INT, MetricType varchar(50), '+@colTable+')'

--populating the records in a variable
SET @SQL = '(SELECT LbPatientId, MetricType, '+@colBottom+' INTO PatientMetricPivot FROM 
                     (SELECT * FROM (
                     SELECT LbPatientId, MetricType, MetricName, CAST(MetricValue AS INT) AS Metric_Value
                     FROM PatientMetric ) T
                     ) derivedTable
                     PIVOT
                     (SUM(Metric_Value) FOR MetricName IN ('+@colBottom+')) pivotTable)'
      
      SELECT @SQL

EXECUTE sp_executesql @SQL
GO
