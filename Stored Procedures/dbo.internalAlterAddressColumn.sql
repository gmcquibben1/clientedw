SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalAlterAddressColumn]  
AS
BEGIN
	

	------------------------------------------------------------------------------
	-- Step 1: Get a list of tables with address length < 50
	------------------------------------------------------------------------------

	EXEC InternalDropTempTables

	DECLARE @TABLENAME VARCHAR(100), @ColumnName VARCHAR(100), @DynSql VARCHAR(1000) , @ErrorMsg VARCHAR(2000)
	
	DECLARE MyCur CURSOR FAST_FORWARD FOR 
	SELECT DISTINCT TABLE_NAME, COLUMN_NAME  FROM INFORMATION_SCHEMA.COLUMNS
	WHERE (COLUMN_NAME LIKE '%address%' OR COLUMN_NAME LIKE '%addr%' )
	AND TABLE_NAME NOT LIKE 'vw%' 
	AND character_maximum_length < 100
	AND TABLE_NAME <> 'NPI_Registry'
	ORDER BY TABLE_NAME ASC , COLUMN_NAME ASC 

	SET @DynSql = ''
	OPEN MyCur FETCH NEXT FROM MyCur INTO @TABLENAME , @ColumnName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		------------------------------------------------------------------------------
		-- Generate dynamic sql to alter table.
		------------------------------------------------------------------------------

		SET @DynSql = '	Alter table [dbo].' + @TABLENAME + ' alter column ' + @ColumnName + ' varchar(100) ' 
			   
		

		BEGIN TRY
			SELECT @DynSql
			EXEC   (@DynSql) --run it
		END TRY
		BEGIN CATCH
				SET @ErrorMsg =  'Failed : ' + @DynSql + CAST(ERROR_LINE() AS VARCHAR(100))  + ' Message=' + 
			CAST(ERROR_MESSAGE() AS VARCHAR(1000))  + ' ErrorNumber=' + 
			CAST(ERROR_NUMBER() AS VARCHAR(100)) + ' Severity=' + 
			CAST(ERROR_SEVERITY() AS VARCHAR(100)) + ' ErrState=' + 
			CAST(ERROR_STATE() AS VARCHAR(100)) 

			
		END CATCH

		FETCH NEXT FROM MyCur  INTO @TABLENAME , @ColumnName 
	
	END  --WHILE
	CLOSE MyCur 
	DEALLOCATE MyCur 

END
GO
