SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@PREV-12] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];
	IF OBJECT_ID('tempdb..#ENC') IS NOT NULL 
		DROP TABLE [#ENC];
	IF OBJECT_ID('tempdb..#SCREENING') IS NOT NULL 
		DROP TABLE [#SCREENING];
	IF OBJECT_ID('tempdb..#screen_results') IS NOT NULL 
		DROP TABLE [#screen_results];

	DECLARE  @yearend datetime, @measureBeginDate datetime;

	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2015';

	-- determine if the data sources allowed are clinical-only or clinical-and-claims
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END


	--Initial Patient pooling, for patients that have ranking for the care-8 measure. 
	SELECT r.LBPatientID,
        r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,@measureBeginDate) as Age,
		r.Birthdate,
        GM.GproMeasureTypeID,
		GPM.PcDepressionRank,
		GPM.PcDepressionScreen,
        GPM.PcDepressionConfirmed,
        GPM.PcDepressionPositive, 
		GPM.PcDepressionPlan, 
		GPM.PcDepressionComments, 
		GETUTCDATE() as ModifyDateTime
	INTO #RankedPatients --	Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE GM.Name = 'PREV-12'
		AND (GPM.PcDepressionConfirmed IS NULL OR GPM.pcdepressionscreen IS NULL)

	
	---CHECK IF PATIENT HAS ENCOUNTER DURING MEASURMENT YEAR AND CALC AGE AT THE TIME OF EARLIEST ENCOUNTER

    SELECT DISTINCT 
		RP.LbPatientId, 
        RP.Age,  
        PP.ProcedureDateTime, 
        ROW_NUMBER () OVER (PARTITION BY rp.LbPatientId ORDER BY pp.ProcedureDateTime ASC)earliest_enc
    INTO #ENC -- drop table #enc
    FROM #RANKEDPATIENTS RP
		INNER JOIN vwPatientProcedure pp ON RP.LbPatientid = pp.PatientId
			AND pp.ProcedureDateTime >= @measureBeginDate
			AND pp.ProcedureDateTime < @yearEnd
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND gec.[ModuleType]= 'PREV'
    		AND gec.[ModuleIndicatorGPRO]='12'
    		AND gec.[VariableName] IN   ('ENCOUNTER_CODE')
	GROUP BY RP.LbPatientId, RP.Age, pp.ProcedureDateTime

		--JOIN dbo.patientProcedure PP ON RP.LbPatientId = PP.PatientId
		--JOIN dbo.patientProcedureProcedureCode PPC ON pp.[PatientProcedureId] = ppc.[PatientProcedureId]
		--JOIN dbo.ProcedureCode PC ON PPC.ProcedureCodeId = PC.ProcedureCodeId
		--JOIN DBO.[GproEvaluationCode] HVSC ON PC.ProcedureCode = HVSC.Code
  --  		AND hvsc.[ModuleType]= 'PREV'
  --  		AND hvsc.[ModuleIndicatorGPRO]='12'
  --  		AND HVSC.[VariableName] IN   ('ENCOUNTER_CODE')
  --  		AND PP.ProcedureDateTime >= @measureBeginDate
  --  		AND PP.ProcedureDateTime < @yearend
		--JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS       
  --  GROUP BY RP.LbPatientId, rp.age,  PP.ProcedureDateTime

	
    --depression screening
	SELECT DISTINCT stage.*
	INTO #SCREENING
	FROM #ENC stage
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = stage.LbPatientId
			AND CAST(pp.ProcedureDateTime AS DATE) = CAST(stage.ProcedureDateTime AS DATE)
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND gec.[ModuleType]= 'PREV'
    		AND gec.[ModuleIndicatorGPRO]='12'
    		AND gec.[VariableName] IN   ('SCREENING_CODE')
		INNER JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pp.[SourceSystemID]

  --  SELECT distinct stage.*	panthers
  --  INTO #SCREENING
  --  FROM #ENC stage
		--JOIN [dbo].[PatientLabOrder] plo ON  plo.[PatientID] = stage.[LbPatientID]
		--JOIN [dbo].[PatientLabResult] plr ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
  --  		AND PLR.ObservationDate = STAGE.ProcedureDateTime -- on same date
		--JOIN [dbo].[GproEvaluationCode] hvsc ON hvsc.[Code] = plr.[ObservationCode1]
  --  		AND hvsc.[ModuleType]= 'PREV'
  --  		AND hvsc.[ModuleIndicatorGPRO]='12'
  --  		AND HVSC.[VariableName] IN   ('SCREENING_CODE')
		--JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID]    --LABS


    --NEG AND POS RESULT
    SELECT DISTINCT 
		RP.*,
        gec.[VariableName]
    INTO #Screen_results
    FROM #SCREENING RP
		INNER JOIN vwPatientProcedure pp ON RP.LbPatientId = pp.PatientId
			AND CAST(pp.ProcedureDateTime AS Date) = CAST(RP.ProcedureDateTime AS Date)
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND gec.[ModuleType]= 'PREV'
    		AND gec.[ModuleIndicatorGPRO]='12'
    		AND gec.[VariableName] IN   ('NEG_SCREENING_CODE','POS_SCREENING_CODE')
		JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PP.[SourceSystemID] 

		--JOIN dbo.patientProcedure PP ON RP.LBPATIENTID = PP.PATIENTID
		--JOIN dbo.patientProcedureProcedureCode PPC ON pp.[PatientProcedureId] = ppc.[PatientProcedureId]
		--	AND PP.PROCEDUREDATETIME = RP.PROCEDUREDATETIME -- on same date
		--JOIN dbo.ProcedureCode PC ON PPC.PROCEDURECODEID = PC.PROCEDURECODEID
		--JOIN DBO.[GproEvaluationCode] HVSC ON PC.ProcedureCode = HVSC.CODE
  --  		AND hvsc.[ModuleType]= 'PREV'
  --  		AND hvsc.[ModuleIndicatorGPRO]='12'
  --  		AND HVSC.[VariableName] IN   ('NEG_SCREENING_CODE','POS_SCREENING_CODE')
		--JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
    			

    --positive screening check for follow up plan
    SELECT DISTINCT SR.*
    INTO #PLAN
    FROM #Screen_results sr
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = sr.LbPatientId
			AND CAST(pp.ProcedureDateTime AS Date) = CAST(sr.ProcedureDateTime AS Date)
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND gec.[ModuleType]    = 'PREV'
    		AND gec.[ModuleIndicatorGPRO]='12'
    		AND gec.[VariableName] IN   ('INTERVENTION_CODE')
		JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PP.[SourceSystemID] 
	WHERE sr.VariableName = 'POS_SCREENING_CODE'

		--JOIN dbo.patientProcedure PP ON sr.LbPatientId = PP.PatientId
		--JOIN dbo.patientProcedureProcedureCode PPC ON pp.[PatientProcedureId] = ppc.[PatientProcedureId]
		--	AND PP.ProcedureDateTime = sr.ProcedureDateTime -- on same date
		--JOIN dbo.ProcedureCode PC ON PPC.ProcedureCodeId = PC.ProcedureCodeId
		--JOIN DBO.[GproEvaluationCode] HVSC ON PC.ProcedureCode = HVSC.CODE
  --  		AND hvsc.[ModuleType]    = 'PREV'
  --  		AND hvsc.[ModuleIndicatorGPRO]='12'
  --  		AND HVSC.[VariableName] IN   ('INTERVENTION_CODE')
		--JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS       
  --  WHERE sr.[VariableName] = 'POS_SCREENING_CODE'

    --patient age confirmed
    UPDATE RP
    SET RP.PcDepressionConfirmed = '2' -- YES
    FROM #RANKEDPATIENTS RP
		JOIN #ENC ENC ON RP.LBPATIENTID = ENC.LBPATIENTID
    WHERE ENC.AGE >= 12 -- AGE 12 AND OLDER
    	AND RP.PcDepressionConfirmed IS NULL


	--    -- not confirmed
	--    update #RankedPatients
	--    set pcdepressionconfirmed = '19' -- not confirmed
	--    --	select distinct rp.*
	--    from #RankedPatients rp
	--    left join #ENC enc on rp.lbpatientid = enc.lbpatientid
	--    where enc.lbpatientid is null
	--    	and rp.pcdepressionconfirmed is null

	--depression screen
	UPDATE RP
	SET RP.PcDepressionScreen = '2' -- yes
	FROM #RankedPatients rp
		JOIN #SCREENING enc on rp.lbpatientid = enc.lbpatientid
	WHERE rp.PcDepressionScreen IS NULL

	----depression not screen
	--update #RankedPatients
	--set pcdepressionscreen = '1' -- no
	----	select distinct rp.*
	--from #RankedPatients rp
	--left join #SCREENING enc on rp.lbpatientid = enc.lbpatientid
	--where enc.lbpatientid is null
	--and rp.pcdepressionscreen is null

	---positive screen
	UPDATE rp
	SET rp.PcDepressionPositive = '2' -- yes
	FROM #RankedPatients rp
		JOIN #Screen_results enc ON rp.lbpatientid = enc.lbpatientid
	WHERE enc.[VariableName] = 'POS_SCREENING_CODE'
		AND rp.pcdepressionPositive IS NULL

	--not positive screen
	--update #RankedPatients
	--set pcdepressionPositive = '1' -- no
	----	select distinct rp.*
	--from #RankedPatients rp
	--left join #Screen_results enc on rp.lbpatientid = enc.lbpatientid
	--where enc.lbpatientid is null
	--	and rp.pcdepressionPositive is null

	--follow up plan
	UPDATE rp
	SET rp.pcdepressionPlan = '2' -- yes
	FROM #RankedPatients rp
		JOIN #plan enc ON rp.lbpatientid = enc.lbpatientid
	WHERE rp.pcdepressionPlan IS NULL

	--no follow up plan
	--update #RankedPatients
	--set pcdepressionPlan = '1' -- no
	----	select distinct rp.*
	--from #RankedPatients rp
	--left join #plan enc on rp.lbpatientid = enc.lbpatientid
	--where enc.lbpatientid is null
	--	and rp.pcdepressionPlan is null


	-- select * from #RankedPatients 

	--	SELECT * FROM [GproEvaluationCode] WHERE [ModuleType]= 'PREV' AND [ModuleIndicatorGPRO]='12' and [VariableName] = 'INTERVENTION_CODE' order by [VariableName]

	--	pcdepressionconfirmed, pcdepressionscreen, pcdepressionPositive, pcdepressionPlan

	--------------------------------------------------------------------------------------------------------          
	-- Updating the required tables
	--------------------------------------------------------------------------------------------------------    
  
	--UPDATE gpm
	--SET   gpm.pcdepressionconfirmed = gpr.pcdepressionconfirmed, gpm.pcdepressionscreen = gpr.pcdepressionscreen, gpm.pcdepressionPositive = gpr.pcdepressionPositive, gpm.pcdepressionPlan = gpr.pcdepressionPlan, gpm.ModifyDateTime = GETUTCDATE()
	----select gpm.*,gpr.PcPneumoShotConfirmed
	--FROM GproPatientMeasure gpm
	--	   JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId

	UPDATE gpm
	SET gpm.PcDepressionConfirmed = gpr.PcDepressionConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcDepressionRank IS NOT NULL
		AND gpm.PcDepressionConfirmed IS NULL

	UPDATE gpm
	SET gpm.PcDepressionScreen = gpr.PcDepressionScreen, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcDepressionRank IS NOT NULL
		AND gpm.PcDepressionScreen IS NULL

	UPDATE gpm
	SET gpm.PcDepressionPositive = gpr.PcDepressionPositive, gpm.PcDepressionPlan = gpr.PcDepressionPlan, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcDepressionRank IS NOT NULL
		AND gpm.PcDepressionPositive IS NULL 
		AND gpm.PcDepressionPlan IS NULL
		
END
GO
