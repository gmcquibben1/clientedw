SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FaceSheetPatientDiagnosisLoad] 
	@PatientId INTEGER
AS

SELECT DISTINCT
	dc.DiagnosisCodeDisplay AS DiagnosisCode,
	dc.DiagnosisDescription AS DiagnosisDescription,
	MAX(pd.DiagnosisDateTime) AS DiagnosisDateTime
FROM PatientDiagnosis pd 
INNER JOIN PatientDiagnosisDiagnosisCode pddc ON pd.PatientDiagnosisId = pddc.PatientDiagnosisId 
INNER JOIN DiagnosisCode dc ON  dc.DiagnosisCodeId = pddc.DiagnosisCodeId 
WHERE pd.PatientId = @PatientId	AND pd.DeleteInd <> 1
GROUP BY dc.DiagnosisCodeDisplay, dc.DiagnosisDescription
ORDER BY DiagnosisDateTime desc;
GO
