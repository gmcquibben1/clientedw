SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproBenchmarkSummaryQualityPointsLoad]
/*===============================================================
	CREATED BY: 	William Keen
	CREATED ON:		2017-04-13
	INITIAL VER:	2.2.2
	MODULE:			GPro	
	DESCRIPTION:	Returns the Quality Points totals for Benchmark Summary Report
	PARAMETERS: 
		@BusinessUnitId - The parent business unit id.
		@MeasureYear - The year for which to filter data
	PROGRAMMING NOTES: 	

	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2017-04-13		WK			LBPP-2467	Initial Version
=================================================================*/
(
	@BusinessUnitId INT = NULL,
	@MeasureYear INT = NULL
)
AS 
BEGIN
	DECLARE 
	 @BusinessUnitName VARCHAR(255) = (SELECT Name FROM vwPortal_BusinessUnit WHERE BusinessUnitId = @BusinessUnitId AND DeleteInd = 0)
	,@businessUnitNameList NVARCHAR(MAX) 
	,@businessUnitSelectList NVARCHAR(MAX)
	,@buinessUnitIdList NVARCHAR(MAX)
	,@SQL NVARCHAR(MAX)

	CREATE TABLE #TempBusinessUnitList (RowNumber INT IDENTITY PRIMARY KEY NOT NULL, Name VARCHAR(255) NULL, BusinessUnitId INT NULL)
	
	--Inserting children
	INSERT INTO #TempBusinessUnitList (Name, BusinessUnitId) 
	SELECT bu.Name, bu.BusinessUnitId FROM vwPortal_BusinessUnit bu
	JOIN GProMeasureProgressHistory gmph ON bu.BusinessUnitId = gmph.BusinessUnitId
	WHERE ParentBusinessUnitId = @BusinessUnitId AND DeleteInd = 0 AND gmph.TotalPatients > 0 group by bu.Name, bu.BusinessUnitId order by [Name]

	--Inserting the Business Unit selected and all parents
	;WITH CTE (ParentBusinessUnitId, BusinessUnitId, Name)
	AS
	(
		SELECT ParentBusinessUnitId, BusinessUnitId, Name
		FROM vwPortal_BusinessUnit
		WHERE BusinessUnitId = @BusinessUnitId AND DeleteInd = 0
		UNION ALL
		SELECT v.ParentBusinessUnitId,v.BusinessUnitId, v.Name
		FROM vwPortal_BusinessUnit v
		JOIN CTE ON cte.ParentBusinessUnitId = v.BusinessUnitId
		AND v.DeleteInd = 0
	)
	INSERT INTO #TempBusinessUnitList (Name, BusinessUnitId) 
	SELECT Name, BusinessUnitId FROM CTE
	OPTION (MAXRECURSION 20);

	SET @businessUnitSelectList = 'MeasureTypeName, MeasureTypeDescription,' + STUFF((SELECT  ', ['+ CAST(BusinessUnitId AS VARCHAR(50)) +'] AS Facility' + CAST(RowNumber AS VARCHAR) FROM #TempBusinessUnitList FOR XML PATH('')),1,1, N'')
	DECLARE @NullPadding INT = (SELECT COUNT(RowNumber) FROM #TempBusinessUnitList) + 1
	DECLARE @x INT = 0
	WHILE @NullPadding < 26
	BEGIN
		SET @businessUnitSelectList = @businessUnitSelectList + ', NULL AS Facility' + CAST(@NullPadding AS VARCHAR)
		SET @NullPadding += 1
	END

	SET @businessUnitNameList = STUFF((SELECT  ', ['+ CAST(BusinessUnitId AS VARCHAR(50)) +']' FROM #TempBusinessUnitList FOR XML PATH('')),1,1, N'')
	SET @buinessUnitIdList =  STUFF((SELECT  ', ' + CAST(BusinessUnitId AS VARCHAR(50)) FROM #TempBusinessUnitList group by BusinessUnitId, RowNumber order by RowNumber FOR XML PATH('')),1,1, N'')
	SET @SQL = 
	'SELECT ' + @businessUnitSelectList + ' FROM (
		SELECT ''Quality Points'' AS MeasureTypeName, g.BusinessUnitId, ''Total Quality Points (out of a possible 26.00 points)'' AS MeasureTypeDescription, 
			(SELECT dbo.fnGProBusinessUnitTotalQualityPointsGet(g.BusinessUnitId,' + CAST(@MeasureYear AS VARCHAR) + ')) AS CMSBenchmark
		FROM GProMeasureProgressHistory g 
		WHERE BusinessUnitId IN  (' + @buinessUnitIdList + ')
		AND g.MeasureYear = ' + CAST(@MeasureYear AS VARCHAR) + ' 
		) src
	PIVOT(
		MAX(CMSBenchmark) FOR BusinessUnitId IN (' + @businessUnitNameList + ')
	 ) piv;'

	DROP TABLE #TempBusinessUnitList
	--PRINT @SQL
	EXECUTE sp_executesql @SQL
END
GO
