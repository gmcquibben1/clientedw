SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPatientIdsConflictsChangeDataCapture] @RunMode INT = 1
-- @RunMode = 1 FOR Demerge and Merge , @RunMode = 2 FOR Demerge Only , @RunMode = 3 FOR Merge Only
-- How to run :- EXEC [dbo].[internalPatientIdsConflictsChangeDataCapture] @RunMode = 1 OR EXEC [dbo].[internalPatientIdsConflictsChangeDataCapture] @RunMode = 2 etc
AS
BEGIN

-- CREATE SWITCHSETS 
CREATE TABLE #PatientIdsConflictsSwitchSets -- DROP TABLE #PatientIdsConflictsSwitchSets
(
	IdType Varchar(100),
	IdDataType Varchar(100),
	SwitchoutId Varchar(100),
	SwitchInId Varchar(100),
	SwitchDirectionId Bit Default 0,
	ActiveSet Bit Default 0,
	CreateDateTime Datetime Default GetDate(),
	ModifyDateTime Datetime Default GetDate()
)

INSERT INTO #PatientIdsConflictsSwitchSets (IdType,IdDataType,SwitchoutId,SwitchInId,SwitchDirectionId,ActiveSet,CreateDateTime, ModifyDateTime)
SELECT IdType, IdDataType,SwitchOutPatientId,SwitchInPatientid,SwitchDirectionId,ActiveSet,CreateDateTime,ModifyDateTime  FROM (
SELECT DISTINCT 
	'LbPatientId' As IdType, 'INT' AS IdDataType,
	PIR.IdValue AS SwitchOutPatientId,
	(SELECT (PIR2.Idvalue)
	FROM PatientIdsRegistry PIR2  WHERE PIR2.family_id = PIXR1.Head_Id AND PIR2.IDType = 'LbPatientId') SwitchInPatientid ,
	0 AS SwitchDirectionId,
	0 As ActiveSet, GetDate() as CreateDateTime, GetDate() as ModifyDateTime
FROM MemberExtract ME1 
INNER JOIN PatientIdsRegistry PIR On ME1.family_id = PIR.Family_Id AND PIR.IDType = 'LbPatientId'
INNER JOIN PatientIdsConflictsXReference PIXR1 ON ME1.family_id = PIXR1.Family_Id AND ME1.family_id <> PIXR1.Head_Id
LEFT JOIN  PatientIdsConflictsDemergeMandate DM ON ME1.family_id = DM.KeepOut_Family_Id WHERE DM.rooted_id IS NULL ) X WHERE SwitchOutPatientId <> SwitchInPatientid

INSERT INTO #PatientIdsConflictsSwitchSets (IdType,IdDataType,SwitchoutId,SwitchInId,SwitchDirectionId,ActiveSet,CreateDateTime, ModifyDateTime)
SELECT IdType, IdDataType,SwitchOutPatientId,SwitchInPatientid,SwitchDirectionId,ActiveSet,CreateDateTime,ModifyDateTime  FROM (
SELECT  DISTINCT 
	'HICNO' As IdType, 'VARCHAR(100)' AS IdDataType,
	ME1.medicareNo AS SwitchOutPatientId,
	(SELECT (ME2.medicareNo)
	FROM MemberExtract ME2 WHERE ME2.family_id = PIXR1.Head_Id) SwitchInPatientid ,
	0 AS SwitchDirectionId,
	0 As ActiveSet, GetDate() as CreateDateTime, GetDate() as ModifyDateTime
FROM MemberExtract ME1 
INNER JOIN PatientIdsConflictsXReference PIXR1 ON ME1.family_id = PIXR1.Family_Id AND ME1.family_id <> PIXR1.Head_Id
LEFT JOIN  PatientIdsConflictsDemergeMandate DM ON ME1.family_id = DM.KeepOut_Family_Id WHERE DM.rooted_id IS NULL ) X WHERE SwitchOutPatientId <> SwitchInPatientid

INSERT INTO #PatientIdsConflictsSwitchSets (IdType,IdDataType,SwitchoutId,SwitchInId,SwitchDirectionId,ActiveSet,CreateDateTime, ModifyDateTime)
SELECT  DISTINCT 
	'LbPatientId' As IdType,'INT' AS IdDataType,
	(SELECT (PIR2.Idvalue)
	FROM PatientIdsRegistry PIR2 WHERE PIR2.family_id = PIXR1.Head_Id AND PIR2.IDType = 'LbPatientId') SwitchOutPatientid,
	PIR.IdValue AS SwitchInPatientId,
	1 AS SwitchDirectionId,
	0 As ActiveSet, GetDate() as CreateDateTime, GetDate() as ModifyDateTime
FROM MemberExtract ME1 
INNER JOIN PatientIdsRegistry PIR On ME1.family_id = PIR.Family_Id AND PIR.IDType = 'LbPatientId'
INNER JOIN [PatientIdsConflictsDemergeMandate] PIXR1 ON ME1.family_id = PIXR1.KeepOut_Family_Id

INSERT INTO #PatientIdsConflictsSwitchSets (IdType,IdDataType,SwitchoutId,SwitchInId,SwitchDirectionId,ActiveSet,CreateDateTime, ModifyDateTime)
SELECT  DISTINCT 
	'HICNO' As IdType,'VARCHAR(100)' AS IdDataType,
	(SELECT (ME2.medicareNo)
	FROM MemberExtract ME2 WHERE ME2.family_id = PIXR1.Head_Id) SwitchOutPatientid,
	ME1.medicareNo AS SwitchInPatientId,
	1 AS SwitchDirectionId,
	0 As ActiveSet, GetDate() as CreateDateTime, GetDate() as ModifyDateTime
FROM MemberExtract ME1 
INNER JOIN [PatientIdsConflictsDemergeMandate] PIXR1 ON ME1.family_id = PIXR1.KeepOut_Family_Id

IF @RunMode  IN (1,3)
BEGIN

UPDATE target 
SET ActiveSet = 1 ,
	ModifyDateTime = GetDate()
FROM [DBO].PatientIdsConflictsSwitchSets  target
LEFT JOIN #PatientIdsConflictsSwitchSets source ON target.SwitchoutId = source.SwitchoutId AND target.SwitchInId = source.SwitchInId and target.IdType = source.IdType 
WHERE source.SwitchoutId IS NULL 

MERGE [DBO].PatientIdsConflictsSwitchSets AS target
USING (	
		  SELECT IdType , IdDataType,SwitchoutId , SwitchInId , SwitchDirectionId , ActiveSet, CreateDateTime, ModifyDateTime
		  FROM #PatientIdsConflictsSwitchSets SS WHERE SwitchDirectionId = 0 
	  ) AS source 
	  (IdType , IdDataType,SwitchoutId ,	SwitchInId , 	SwitchDirectionId , ActiveSet , CreateDateTime , ModifyDateTime)
ON (target.SwitchoutId = source.SwitchoutId AND target.SwitchInId = source.SwitchInId and target.IdType = source.IdType)
WHEN MATCHED THEN 
UPDATE SET 
	IdType = source.IdType, 
	IdDataType = source.IdDataType, 
	SwitchoutId = source.SwitchoutId, 
	SwitchInId = source.SwitchInId, 
	SwitchDirectionId = source.SwitchDirectionId, 
	ActiveSet = source.ActiveSet, 
	ModifyDateTime = source.ModifyDateTime
WHEN NOT MATCHED THEN
INSERT (IdType , IdDataType,SwitchoutId ,	SwitchInId , 	SwitchDirectionId , ActiveSet ,  CreateDateTime, ModifyDateTime )
VALUES (source.IdType , source.IdDataType,source.SwitchoutId ,	source.SwitchInId , source.SwitchDirectionId , source.ActiveSet , source.CreateDateTime , source.ModifyDateTime);

END

-- ACT ON  SWITCHSETS 
DECLARE @DbLocation VARCHAR(100), @DependencyTable VARCHAR(100), @TableView VARCHAR(100), @TablePk VARCHAR(100),  @TablePKDataType VARCHAR(100),
	    @UpdatingColumn VARCHAR(100),@UpdatingColumnDataType VARCHAR(100), @ResolvingForId  VARCHAR(100),
	    @DynSql NVARCHAR(MAX) = '', @TOBJECT_ID BIGINT, @COBJECT_ID BIGINT

-- MERGE CASE 1 : Merge Case :- Merge Case :- Update the column of the source row to merged id if not the same as merged Id; Demerge
DECLARE MyCur CURSOR FAST_FORWARD FOR 
SELECT DbLocation, DependencyTable, TableView, TablePk, TablePKDataType, UpdatingColumn, UpdatingColumnDataType, ResolvingForId
FROM PatientIdsConflictsResolveDependencies
WHERE ResolutionTypeId = 1 

OPEN MyCur FETCH NEXT FROM MyCur INTO @DbLocation, @DependencyTable, @TableView, @TablePk,  @TablePKDataType , @UpdatingColumn, @UpdatingColumnDataType, @ResolvingForId

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @DbLocation, @DependencyTable, @TableView, @TablePk,  @TablePKDataType , @UpdatingColumn, @UpdatingColumnDataType, @ResolvingForId
	SELECT @TOBJECT_ID = NULL, @COBJECT_ID = NULL 
	SET @DynSql='SELECT @TOBJECT_ID=OBJECT_ID FROM '+ @DbLocation +'.sys.Tables  WHERE name = ''' + @DependencyTable + '_PICT' + ''''
	EXEC SP_EXECUTESQL @DynSql,N'@TOBJECT_ID BIGINT OUTPUT',@TOBJECT_ID OUTPUT
	SET @DynSql='SELECT @COBJECT_ID=OBJECT_ID FROM '+ @DbLocation +'.sys.Columns WHERE name = ''' + @UpdatingColumn +  ''' AND OBJECT_ID = ' + CAST(@TOBJECT_ID AS VARCHAR(20))
	EXEC SP_EXECUTESQL @DynSql,N'@COBJECT_ID BIGINT OUTPUT',@COBJECT_ID OUTPUT

	IF @TOBJECT_ID IS NULL 
	BEGIN 
		SET @DynSql = ' CREATE TABLE ' + @DbLocation + '.dbo.' + @DependencyTable + '_PICT (' 
			          + @TablePk + ' '  + @TablePKDataType + ' , SwitchDirectionId INT, CreateTimestamp DateTime Default GetDate() , '  
					  + @UpdatingColumn  + ' ' + @UpdatingColumnDataType + '  )'-- , ModifyTimestamp DateTime Default GetDate()
		EXEC (@DynSql)
	END 
	ELSE 
	BEGIN
		IF @COBJECT_ID IS NULL --COL_LENGTH(@DependencyTable + '_PICT',@UpdatingColumn) IS NULL
		BEGIN
			SET @DynSql = ' ALTER TABLE ' + @DbLocation + '.dbo.' + @DependencyTable + '_PICT ADD ' 
			          + @UpdatingColumn  + ' ' + @UpdatingColumnDataType 
			EXEC (@DynSql)
		END
	END 
	CREATE TABLE #Updated (LocalId Int Identity(1,1),SwitchDirectionId INT) --CREATE TABLE #Updated (PatientProcedureId Int, LbPatientId Int)
	SET @DynSql = ' ALTER TABLE #Updated ADD ' + @TablePk + ' ' + @TablePKDataType + ' , ' + @UpdatingColumn  + ' ' + @UpdatingColumnDataType + ' '
	EXEC (@DynSql) 

	SET @DynSql=' INSERT INTO #PatientIdsConflictsSwitchSets (IdType,IdDataType,SwitchoutId,SwitchInId,SwitchDirectionId) '  
	            + ' SELECT ''' + @ResolvingForId + ''',''' + @UpdatingColumnDataType + ''', X.SwitchInId, X.SwitchOutId, 1 FROM ( ' 
				+ ' SELECT CAST(A.' + @UpdatingColumn + ' AS VARCHAR(100)) AS SwitchInId, CAST(B.SwitchOutId AS VARCHAR(100))  SwitchOutId '
				+ ' FROM '+ @DbLocation + '.dbo.' + @DependencyTable + ' A '
				+ ' INNER JOIN ( ' 
				+ ' SELECT PICT.' + @TablePk + ', SUBSTRING(MIN(CONVERT(VARCHAR(19),PICT.CreateTimestamp,120) + ''|'' + '
				+ ' CAST(PICT.' + @UpdatingColumn + ' AS VARCHAR(100))),21,1000) SwitchOutId '
				+ ' FROM '+ @DbLocation + '.dbo.' + @DependencyTable + '_PICT PICT WHERE SwitchDirectionId = 0  GROUP BY  PICT.' + @TablePk 
				+ ' ) B ON A.' + @TablePk + ' = B.' + @TablePk + ' AND A.' + @UpdatingColumn + ' <> B.SwitchOutId GROUP BY A.' + @UpdatingColumn + ' , B.SwitchOutId '
				+ ' ) X LEFT JOIN PatientIdsConflictsSwitchSets SS ON SS.SwitchInId = X.SwitchInId AND SS.SwitchOutId = X.SwitchOutId  AND SS.SwitchDirectionId = 0 '
				+ ' AND SS.IdType = '''  + @ResolvingForId + ''''
				+ ' LEFT JOIN #PatientIdsConflictsSwitchSets SS2 ON SS2.SwitchInId = X.SwitchoutId AND SS2.SwitchOutId = X.SwitchInId  AND SS2.SwitchDirectionId = 1 '
				+ ' AND SS2.IdType = ''' + @ResolvingForId + ''''
				+ ' WHERE SS.SwitchInId IS NULL AND SS2.SwitchInId IS NULL'
	EXEC (@DynSql)

	IF @RunMode  IN (1,2)
	BEGIN
	SET @DynSql = ' UPDATE ' + @DbLocation + '.dbo.' + @DependencyTable +  
				  ' SET ' + @UpdatingColumn + ' = SwitchInId  '  + --, ModifyDateTime = Getdate()
				  ' OUTPUT Deleted.' + @TablePk + ', Deleted.' + @UpdatingColumn  + ' , SS.SwitchDirectionId INTO #Updated ( ' + @TablePk + ', '  + @UpdatingColumn  + ',SwitchDirectionId ) ' +  
				  ' FROM ' + @DbLocation + '.dbo.' + @DependencyTable + ' PP INNER JOIN #PatientIdsConflictsSwitchSets SS  ' + 
				  ' ON PP.' + @UpdatingColumn + ' = TRY_CONVERT(' + @UpdatingColumnDataType + ', SS.SwitchOutId) AND SS.IdType =  ''' + @ResolvingForId + ''' AND SS.SwitchDirectionId = 1 ' + 
				  ' WHERE EXISTS (SELECT 1 FROM ' + @DbLocation + '.dbo.' + @DependencyTable + '_PICT PICT WHERE PICT.' + @TablePk + ' = PP.' + @TablePk +  
				  ' AND PICT.SwitchDirectionId = 0 AND PICT.' + @UpdatingColumn + '= TRY_CONVERT(' + @UpdatingColumnDataType + ', SS.SwitchInId) ) ' 
	EXEC (@DynSql) --CREATE TABLE #Updated (PatientProcedureId Int, LbPatientId Int)
	END
	IF @RunMode  IN (1,3)
	BEGIN
	SET @DynSql = ' UPDATE ' + @DbLocation + '.dbo.' + @DependencyTable +  
				  ' SET ' + @UpdatingColumn + ' = SwitchInId  '  + --, ModifyDateTime = Getdate()
				  ' OUTPUT Deleted.' + @TablePk + ', Deleted.' + @UpdatingColumn  + ' , SS.SwitchDirectionId INTO #Updated ( ' + @TablePk + ', '  + @UpdatingColumn  + ',SwitchDirectionId ) ' +  
				  ' FROM ' + @DbLocation + '.dbo.' + @DependencyTable + ' PP INNER JOIN #PatientIdsConflictsSwitchSets SS  ' + 
				  ' ON PP.' + @UpdatingColumn + ' = TRY_CONVERT(' + @UpdatingColumnDataType + ', SS.SwitchOutId) AND SS.IdType =  ''' + @ResolvingForId + ''' AND SS.SwitchDirectionId = 0 '
	EXEC (@DynSql) --CREATE TABLE #Updated (PatientProcedureId Int, LbPatientId Int)
	END
	--SELECT * FROM #Updated
	--UPDATE PatientProcedure 
	--SET PatientId = SwitchInId
	--OUTPUT Deleted.PatientProcedureId, Deleted.PatientId INTO #Updated 
	--FROM PatientProcedure PP INNER JOIN PatientIdsConflictsSwitchSets SS 
	--ON PP.PatientId = TRY_CONVERT(INT,SS.SwitchOutId) AND SS.IdType =  @ResolvingForId AND SS.ActiveSet = 0 AND SS.SwitchDirectionId = 1 

	SET @DynSql = ' INSERT INTO ' + @DbLocation + '.dbo.' + @DependencyTable +  '_PICT (' + @TablePk + ', ' + @UpdatingColumn + ' , SwitchDirectionId) ' + 
				  ' SELECT  ' + @TablePk + ' , ' + @UpdatingColumn  + ' , SwitchDirectionId '  + 
				  ' FROM #Updated ' 
	EXEC (@DynSql) --CREATE TABLE #Updated (PatientProcedureId Int, LbPatientId Int)
		
	--INSERT INTO PatientProcedure_PICT  (PatientProcedureId, PatientId, SwitchDirection) SELECT PatientProcedureId, LbPatientId, 1 FROM #Updated
	DROP TABLE  #Updated
FETCH NEXT FROM MyCur INTO @DbLocation, @DependencyTable, @TableView, @TablePk,  @TablePKDataType , @UpdatingColumn, @UpdatingColumnDataType, @ResolvingForId
END
CLOSE MyCur 
DEALLOCATE MyCur 

-- MERGE CASE 3 : Merge Case :- Retire the source row if not credited to merged Id; Demerge Case :- Activate the sourc
SELECT Pat.PatientId , Ind.IndividualID, Party.PartyID, Party.ExternalReferenceIdentifier , Party.OtherReferenceIdentifier , 0 As DeleteInd 
INTO #PA
From vwPortal_Patient  Pat 
INNER JOIN vwPortal_Individual IND ON Pat.IndividualID = Ind.IndividualID 
INNER JOIN vwPortal_Party Party ON Ind.PartyID = Party.PartyID 

SELECT ME2.medicareNo INTO #Exc FROM MemberExtract ME2 GROUP BY ME2.medicareNo HAVING COUNT(ME2.medicareNo) > 1 

UPDATE #PA 
SET DeleteInd = CASE WHEN ME.Holdback IS NOT NULL THEN 1 ELSE 0 END 
FROM #PA PA INNER JOIN MemberExtract ME ON  PA.ExternalReferenceIdentifier = ME.medicareNo 
WHERE PA.DeleteInd <>  CASE WHEN ME.Holdback IS NOT NULL THEN 1 ELSE 0 END 
AND NOT EXISTS (SELECT ME2.medicareNo FROM #Exc ME2 WHERE PA.ExternalReferenceIdentifier = ME2.medicareNo )
 
DECLARE MyCur CURSOR FAST_FORWARD FOR 
SELECT DbLocation, DependencyTable, TableView, TablePk, TablePKDataType, UpdatingColumn, UpdatingColumnDataType, ResolvingForId
FROM PatientIdsConflictsResolveDependencies
WHERE ResolutionTypeId = 3

SET @DynSql = ''
OPEN MyCur FETCH NEXT FROM MyCur INTO @DbLocation, @DependencyTable, @TableView, @TablePk,  @TablePKDataType , @UpdatingColumn, @UpdatingColumnDataType, @ResolvingForId

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @DynSql = ' UPDATE ' + @DbLocation + '.dbo.' + @DependencyTable +  
				  ' SET ' + @UpdatingColumn + ' = SS.DeleteInd  '  + --, ModifyDateTime = Getdate()
				  ' FROM ' + @DbLocation + '.dbo.' + @DependencyTable + ' PP INNER JOIN #PA SS  ' + 
				  ' ON PP.' + @TablePk + ' = SS.' +  @TablePk  + ' WHERE PP.' + @UpdatingColumn + ' <> SS.DeleteInd  '
	SELECT (@DynSql)  
	EXEC   (@DynSql)

FETCH NEXT FROM MyCur INTO @DbLocation, @DependencyTable, @TableView, @TablePk,  @TablePKDataType , @UpdatingColumn, @UpdatingColumnDataType, @ResolvingForId
END
CLOSE MyCur 
DEALLOCATE MyCur 

END 
GO
