SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FaceSheetDiagnosticsLoad] 
	@PatientId INTEGER,
	@PatientGender VARCHAR(10) = NULL
AS
	DECLARE @bEKGInd BIT = 0
	DECLARE @bMammogramInd BIT = 0
	DECLARE @bPAPInd BIT = 0
	DECLARE @bBoneDensityInd BIT = 0
	DECLARE @bColonoscopyInd BIT = 0
	DECLARE @bSpirometryInd BIT = 0
	DECLARE @bFlochecInd BIT = 0
	DECLARE @bDiabeticEyeExamInd BIT = 0

	DECLARE  @PatientDiagnostics TABLE 
	(
		DiagnosticDescription	VARCHAR(100),
		DiagnosticDate			DATE	NULL
	)	

	IF (@PatientGender = 'Female') 
		BEGIN
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'EKG' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Mammogram' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'PAP' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Bone Density' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Colonoscopy' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Spirometry' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Flochec' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Diabetic Eye Exam' )
			SET @bEKGInd = 1
			SET @bMammogramInd = 1
			SET @bPAPInd = 1
			SET @bBoneDensityInd = 1
			SET @bColonoscopyInd = 1
			SET @bSpirometryInd = 1
			SET @bFlochecInd = 1
			SET @bDiabeticEyeExamInd = 1
		END
	ELSE IF (@PatientGender = 'Male') 
		BEGIN
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'EKG' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Bone Density' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Colonoscopy' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Spirometry' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Flochec' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Diabetic Eye Exam' )
			SET @bEKGInd = 1
			SET @bBoneDensityInd = 1
			SET @bColonoscopyInd = 1
			SET @bSpirometryInd = 1
			SET @bFlochecInd = 1
			SET @bDiabeticEyeExamInd = 1
		END
	ELSE  -- gender not specific...we default to check for ALL
		BEGIN
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'EKG' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Mammogram' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'PAP' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Bone Density' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Colonoscopy' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Spirometry' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Flochec' )
			INSERT INTO @PatientDiagnostics (DiagnosticDescription) VALUES ( 'Diabetic Eye Exam' )
			SET @bEKGInd = 1
			SET @bMammogramInd = 1
			SET @bPAPInd = 1
			SET @bBoneDensityInd = 1
			SET @bColonoscopyInd = 1
			SET @bSpirometryInd = 1
			SET @bFlochecInd = 1
			SET @bDiabeticEyeExamInd = 1
		END
	
	
	-- EKG
	IF @bEKGInd = 1 
	BEGIN 	
		UPDATE @PatientDiagnostics SET DiagnosticDate = 
			(
			SELECT TOP 1
				dbo.PatientProcedure.ProcedureDateTime
			FROM dbo.PatientProcedure INNER JOIN
				 dbo.PatientProcedureProcedureCode ON PatientProcedure.PatientProcedureId = PatientProcedureProcedureCode.PatientProcedureId INNER JOIN
				 dbo.ProcedureCode ON PatientProcedureProcedureCode.ProcedureCodeId = ProcedureCode.ProcedureCodeId 
			WHERE dbo.PatientProcedure.PatientId = @Patientid AND dbo.PatientProcedure.DeleteInd = 0 AND 
					ProcedureCode.ProcedureCode IN 
					( '930WS', 'G0366', 'G0367', 'G0368', 'G0403', 'G0404', 'G0405', '93000TC','93000', '93005', '93010')
			ORDER BY PatientProcedure.ProcedureDateTime DESC
			)
		WHERE DiagnosticDescription = 'EKG'
	END

	-- MAMMOGRAM	
	IF @bMammogramInd = 1 
	BEGIN
		UPDATE @PatientDiagnostics SET DiagnosticDate = 
			(
			SELECT TOP 1        
				dbo.PatientProcedure.ProcedureDateTime
			FROM dbo.PatientProcedure INNER JOIN
				 dbo.PatientProcedureProcedureCode ON PatientProcedure.PatientProcedureId = PatientProcedureProcedureCode.PatientProcedureId INNER JOIN
				 dbo.ProcedureCode ON PatientProcedureProcedureCode.ProcedureCodeId = ProcedureCode.ProcedureCodeId 
			WHERE dbo.PatientProcedure.PatientId = @Patientid AND dbo.PatientProcedure.DeleteInd = 0 
				AND (ProcedureCode.ProcedureCode IN 
					( '77055', '77056', '77057', 'G0202', 'G0204', 'G0206')
					OR ProcedureCode.ProcedureCode IN
					(SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Mammography'))
			ORDER BY PatientProcedure.ProcedureDateTime DESC
			)
		WHERE DiagnosticDescription = 'Mammogram'
	END
		
	-- PAP
	IF @bPAPInd = 1 
	BEGIN
		UPDATE @PatientDiagnostics SET DiagnosticDate = 
			(
			SELECT TOP 1
				dbo.PatientProcedure.ProcedureDateTime
			FROM dbo.PatientProcedure INNER JOIN
				 dbo.PatientProcedureProcedureCode ON PatientProcedure.PatientProcedureId = PatientProcedureProcedureCode.PatientProcedureId INNER JOIN
				 dbo.ProcedureCode ON PatientProcedureProcedureCode.ProcedureCodeId = ProcedureCode.ProcedureCodeId 
			WHERE dbo.PatientProcedure.PatientId = @Patientid AND dbo.PatientProcedure.DeleteInd = 0 AND 
					(ProcedureCode.ProcedureCode IN 
					( '8150T', 'Q0091')
					OR ProcedureCode.ProcedureCode IN 
					(SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Cervical Cytology'))
						ORDER BY PatientProcedure.ProcedureDateTime DESC
			)
		WHERE DiagnosticDescription = 'PAP'
	END

	-- BONE DENSITY
	IF @bBoneDensityInd = 1 
	BEGIN
		UPDATE @PatientDiagnostics SET DiagnosticDate = 
			(
			SELECT  TOP 1      
				dbo.PatientProcedure.ProcedureDateTime
			FROM dbo.PatientProcedure INNER JOIN
				 dbo.PatientProcedureProcedureCode ON PatientProcedure.PatientProcedureId = PatientProcedureProcedureCode.PatientProcedureId INNER JOIN
				 dbo.ProcedureCode ON PatientProcedureProcedureCode.ProcedureCodeId = ProcedureCode.ProcedureCodeId 
			WHERE dbo.PatientProcedure.PatientId = @Patientid AND dbo.PatientProcedure.DeleteInd = 0 
				AND (ProcedureCode.ProcedureCode IN 
					('77080', '76499', '76977', '77081', '77082', '78350','78351','G0062','G8106','G8107','G0130')
					OR ProcedureCode.ProcedureCode IN
					(SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Bone Mineral Density Tests'))
			ORDER BY PatientProcedure.ProcedureDateTime DESC
			)
		WHERE DiagnosticDescription = 'Bone Density'
	END

	-- COLONOSCOPY
	IF @bColonoscopyInd = 1
	BEGIN
		UPDATE @PatientDiagnostics SET DiagnosticDate = 
			(
			SELECT  TOP 1      
				dbo.PatientProcedure.ProcedureDateTime
			FROM dbo.PatientProcedure INNER JOIN
				 dbo.PatientProcedureProcedureCode ON PatientProcedure.PatientProcedureId = PatientProcedureProcedureCode.PatientProcedureId INNER JOIN
				 dbo.ProcedureCode ON PatientProcedureProcedureCode.ProcedureCodeId = ProcedureCode.ProcedureCodeId 
			WHERE dbo.PatientProcedure.PatientId = @Patientid AND dbo.PatientProcedure.DeleteInd = 0 
				AND (ProcedureCode.ProcedureCode IN 
					( '45337', '45338', '45339', '45340', '45341', '45342', '45345', 'G0121', '45385')
					OR ProcedureCode.ProcedureCode IN 
					(SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Colonoscopy'))
			ORDER BY PatientProcedure.ProcedureDateTime DESC
			)
		WHERE DiagnosticDescription = 'COLONOSCOPY'
	END

	-- SPIROMETRY
	IF @bSpirometryInd = 1 
	BEGIN
		UPDATE @PatientDiagnostics SET DiagnosticDate = 
			(
			SELECT  TOP 1      
				dbo.PatientProcedure.ProcedureDateTime
			FROM dbo.PatientProcedure INNER JOIN
				 dbo.PatientProcedureProcedureCode ON PatientProcedure.PatientProcedureId = PatientProcedureProcedureCode.PatientProcedureId INNER JOIN
				 dbo.ProcedureCode ON PatientProcedureProcedureCode.ProcedureCodeId = ProcedureCode.ProcedureCodeId 
			WHERE dbo.PatientProcedure.PatientId = @Patientid AND dbo.PatientProcedure.DeleteInd = 0 
				AND (ProcedureCode.ProcedureCode IN 
					(  '010WS','3023F','3025F','3027F','94010','9401026','94010TC','94620')
					OR ProcedureCode.ProcedureCode IN
					(SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Spirometry'))
			ORDER BY PatientProcedure.ProcedureDateTime DESC
			)
		WHERE DiagnosticDescription = 'Spirometry'
	END

	-- DIABETIC EYE EXAM
	IF @bBoneDensityInd = 1 
	BEGIN
		UPDATE @PatientDiagnostics SET DiagnosticDate = 
			(
			SELECT  TOP 1      
				dbo.PatientProcedure.ProcedureDateTime
			FROM dbo.PatientProcedure INNER JOIN
				 dbo.PatientProcedureProcedureCode ON PatientProcedure.PatientProcedureId = PatientProcedureProcedureCode.PatientProcedureId INNER JOIN
				 dbo.ProcedureCode ON PatientProcedureProcedureCode.ProcedureCodeId = ProcedureCode.ProcedureCodeId 
			WHERE dbo.PatientProcedure.PatientId = @Patientid AND dbo.PatientProcedure.DeleteInd = 0 
				AND (ProcedureCode.ProcedureCode IN 
					('2024F')
					OR ProcedureCode.ProcedureCode IN
					(SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Diabetes Eye Exam'))
			ORDER BY PatientProcedure.ProcedureDateTime DESC
			)
		WHERE DiagnosticDescription = 'Diabetic Eye Exam'
	END

	SELECT 	DiagnosticDescription,	DiagnosticDate
	FROM @PatientDiagnostics 
	ORDER BY DiagnosticDescription



GO
