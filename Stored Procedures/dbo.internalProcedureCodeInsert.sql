SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalProcedureCodeInsert] 
    @ProcedureCodeId INT OUTPUT ,
	@ProcedureCodingSystemTypeId INT ,
	@ProcedureCode VARCHAR(255)  ,
	@ProcedureDescription VARCHAR(255) --,
	--@ProcedureDescriptionAlternate VARCHAR(255) = ''
AS

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @ProcedureCodeId INT

	BEGIN TRAN

	SELECT @ProcedureCodeId = ProcedureCodeId FROM [ProcedureCode]
	WHERE ProcedureCodingSystemTypeId  = @ProcedureCodingSystemTypeId
	AND ProcedureCode = @ProcedureCode

	IF @ProcedureCodeId IS NULL
	BEGIN
		INSERT INTO [dbo].[ProcedureCode]
			   ([ProcedureCodingSystemTypeId]
			   ,[ProcedureCode]
			   ,[ProcedureDescription]
			   ,[ProcedureDescriptionAlternate]
			   ,[DeleteInd]
			   ,[CreateDateTime]
			   ,[ModifyDateTime]
			   ,[CreateLBUserId]
			   ,[ModifyLBUserId])
		VALUES 
		(
			@ProcedureCodingSystemTypeId,
			@ProcedureCode,
			@ProcedureDescription,
			@ProcedureDescription,
			@DeleteInd,
			GetDate(),
			GetDate(),
			@LBUserId,
			@LBUserId
		)
		SELECT @intError = @@ERROR,
			@ProcedureCodeId = SCOPE_IDENTITY()
	END

	IF @@ERROR <> 0 
	BEGIN
		ROLLBACK TRAN
	END
	ELSE
	BEGIN
		COMMIT TRAN
	END
		
	RETURN @intError




GO
