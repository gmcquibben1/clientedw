SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[ETLRunAllFuzzyMatchMethods]
AS
BEGIN


/*=================================================================================================
	CREATED BY: 	Mike H
	CREATED ON:		2017-01-18
	INITIAL VER:	2.2.2
	MODULE:			Fuzzy Match Type for External ID/Source System ID	
	DESCRIPTION:   Each patient record where we have created a duplicate patient with the same External ID from the same 
                   Source System, we will create a fuzzy match result record to merge the patient information together
                    we will keep the largest (latest) version of the patient in this case.


	MODIFICATIONS
	Version     Date            Author		  JIRA  		Change
	=======     ==========      ======      =========	=========	
  2.2.2       1/24/2017         MH        LBAN-3490   Initial Version
  2.2.2       03.07.2017		CJL		  LBAN-3490   Added logging, removed hardcoded value type of 3, handled issues in which duplicate records would be inserted


====================================================================================================*/


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'ETLRunAllFuzzyMatchMethods';
	DECLARE @dataSource VARCHAR(20) = 'EDW';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLRunAllFuzzyMatchMethods';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @NUMROWS INT

	
	BEGIN TRY



		--Run All Fuzzy Match Stored Procedures that are enabled
		DECLARE @dynSQL nvarchar(MAX)

		SELECT @dynSQL = COALESCE(@dynSQL +'
		', '') + 'EXEC '+ SPName +'  @MatchScoreMin=' + CAST(MatchThreshHold AS VARCHAR(10)) FROM dbo.FuzzyMatchType WHERE EnableInd = 1 
		--run all enabled matchin  types
		SELECT @dynSQL
		EXECUTE sys.sp_executesql @dynSQL



	
		-- update maxSourceTimeStamp with max CreateDateTime of
		UPDATE [Maintenance]..[EDWTableLoadTracking]
		SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex, updatedatetime = GETUTCDATE()
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] =@dataSource  AND [EDWName] = @EDWName


		-- get total records from PatientMedication after merge 
		SELECT @RecordCountAfter= (SELECT COUNT(1)  FROM FuzzyMatchResult WITH (NOLOCK));

		-- Calculate records inserted and updated counts
		SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
		SET @UpdatedRecord = 0

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;



	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
END

GO
