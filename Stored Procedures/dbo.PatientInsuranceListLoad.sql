SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientInsuranceListLoad]
	@PatientID int
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT pins.PatientInsuranceId,pins.InsuranceName, pins.GroupNumber, pins.PolicyNumber, pins.FinancialClassTypeId,pins.EffectiveDate,
	       pins.ExpirationDate, pins.InsurancePriority
	FROM PatientInsurance pins  
	WHERE pins.PatientId = @PatientID AND pins.DeleteInd = 0

END
GO
