SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientLabResultLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 

AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		plo.PatientId,
		plr.PatientLabOrderId,
		plr.ObservationCode1,
		plr.ObservationDescription1,
		plr.ObservationCodingSystemName1,
		plr.ObservationCode2,
		plr.ObservationDescription2,
		plr.ObservationCodingSystemName2,
		plr.Value,
		plr.Units,
		plr.ReferenceRange,
		plr.AbnormalFlag,
		plr.ResultStatus,
		plr.ResultComment,
		plr.ObservationDate,
		plr.CreateDateTime,
		plr.ModifyDateTime,
		plr.CreateLBUserId,
		plr.ModifyLBUserId
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientLabOrder plo ON plo.PatientId = pdel.LbPatientId 
		 INNER JOIN vwPatientLabResult plr ON plr.PatientLabOrderId = plo.PatientLabOrderId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
		AND (plr.CreateDateTime > @dtmStartDate OR plr.ModifyDateTime > @dtmStartDate)

END
GO
