SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalATIScriptBuild]
AS
BEGIN
--=============================================================
--                 ATI Score Builder V1.0
--                Original Author: Mike Hoxter
--                Origin Date: 9/14/2014 
--=============================================================

exec InternalDropTempTables

--===========================================
--STEP 1: Create Data for Factors & Responses
--           By: Mike Hoxter
--           Origin Date: 9/2/2014
--===========================================
--Modified 10/24/2014 to take additional factors into account (ER Visits, 
--Clear out FR Raw Table
	TRUNCATE TABLE ATI_Factor_Raw

	TRUNCATE TABLE ATI_Response_Raw


	--ATI_Factor_Raw (LBPatientId, FName, FValue)
	--MAKE TEMP MOST RECENT VITALS TABLE
	SELECT --top 10 
	Max(ServiceDateTime) as lastDOS, PatientID 
	INTO #VITALTEMP
	FROM PatientVitals
	group by PatientID

	--INSERT MOST RECENT BMI, 0 IF < 30, 1 IF > 30, 2 IF > 40
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	)
	SELECT B.PatientID, 'F_HIGHBMI',
	(CASE 
	   WHEN (PV.[WeightKG]/((PV.[HeightCM]/100)*(PV.[HeightCM]/100))) > 40 THEN 2 
	   WHEN (PV.[WeightKG]/((PV.[HeightCM]/100)*(PV.[HeightCM]/100))) > 30 THEN 1
	   ELSE 0
	   END) AS HIGHBMI
	FROM PatientVitals PV
	JOIN #VITALTEMP B on PV.PatientID = B.PatientID AND PV.ServiceDateTime = B.lastDOS
	WHERE PV.HeightCM > 0


	--INSERT MOST RECENT BLOOD PRESSURE, ABNORMAL = 1
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	)SELECT B.PatientID, 'F_HIGHBP', 
	(CASE 
		WHEN PV.BloodPressureSystolic > 120 OR PV.BloodPressureDiastolic > 80 THEN 1
		ELSE 0
		END) AS HIGHBP
	FROM PatientVitals PV
	JOIN #VITALTEMP B on PV.PatientID = B.PatientID AND PV.ServiceDateTime = B.lastDOS


	--INSERT MOST RECENT O2 SAT, IF < 96, 1
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	)SELECT B.PatientID, 'F_O2SAT', 
	(CASE 
		WHEN TRY_CONVERT(DECIMAL (5,2), PV.OxygenSaturationSP02) < 94  THEN 1
		WHEN TRY_CONVERT(DECIMAL (5,2), PV.OxygenSaturationSP02) < 97  THEN 2
		ELSE 0
		END) AS HIGHBP
	FROM PatientVitals PV
	JOIN #VITALTEMP B on PV.PatientID = B.PatientID AND PV.ServiceDateTime = B.lastDOS


	DROP TABLE #VITALTEMP


	--insert most recent HbA1C, if <8=0, >8=1, >9=2
	SELECT-- TOP 10
	 MAX(LR.ObservationDate) AS [LABDATE], LO.PatientId as PatientId
	INTO #HBA1CTEMP
	  FROM PatientLabResult LR
	  JOIN PatientLabOrder LO ON LR.PatientLabOrderId = LO.PatientLabOrderId
	  JOIN HEDISValueSetCodes H ON LR.ObservationCode1 = H.Code
	  WHERE H.[Value Set Name] = 'HBA1C Tests' 
	  GROUP BY LO.PatientId
  
  
	  INSERT INTO ATI_Factor_Raw (LBPatientId, FName, FValue)
	 SELECT LO.PatientId, 'F_HBA1C', 
	  MAX(CASE
	  WHEN TRY_CONVERT(DECIMAL (5,2), L.[Value]) > 9.0 THEN 2
	  WHEN TRY_CONVERT(DECIMAL (5,2), L.[Value]) > 8.0 THEN 1
	  ELSE 0
	  END) AS HBA1CLATEST
	  FROM PatientLabResult L 
	  join dbo.PatientLabOrder LO ON LO.PatientLabOrderId = L.PatientLabOrderId
	  JOIN #HBA1CTEMP B ON LO.PatientId = B.PatientId AND L.ObservationDate = B.LABDATE
	  JOIN HEDISValueSetCodes H ON L.ObservationCode1 = H.Code
	  WHERE H.[Value Set Name] = 'HBA1C Tests'
	  GROUP BY LO.PatientId
  

	  DROP TABLE #HBA1CTEMP
  
  
  
	--insert most recent LDL-C, if <100=0, >100=1, >120=2
	SELECT-- TOP 10
	 MAX(LR.ObservationDate) AS [LABDATE], LO.PatientId as PatientId
	INTO #LDLCTEMP
	  FROM PatientLabResult LR
	  JOIN PatientLabOrder LO ON LR.PatientLabOrderId = LO.PatientLabOrderId
	  JOIN HEDISValueSetCodes H ON LR.ObservationCode1 = H.Code
	  WHERE H.[Value Set Name] = 'LDL-C Tests' 
	  GROUP BY LO.PatientId
  
  
	  INSERT INTO ATI_Factor_Raw (LBPatientId, FName, FValue)
	 SELECT LO.PatientId, 'F_LDL-C', 
	  MAX(CASE
	  WHEN TRY_CONVERT(DECIMAL (6,2), L.[Value]) > 120.0 THEN 2
	  WHEN TRY_CONVERT(DECIMAL (6,2), L.[Value]) > 100.0 THEN 1
	  ELSE 0
	  END) AS HBA1CLATEST
	  FROM PatientLabResult L 
	  join dbo.PatientLabOrder LO ON LO.PatientLabOrderId = L.PatientLabOrderId
	  JOIN #LDLCTEMP B ON LO.PatientId = B.PatientId AND L.ObservationDate = B.LABDATE
	  JOIN HEDISValueSetCodes H ON L.ObservationCode1 = H.Code
	  WHERE H.[Value Set Name] = 'LDL-C Tests'
	  GROUP BY LO.PatientId
  

	  DROP TABLE #LDLCTEMP
  
  
	  --INSERT ER VISIT FACTORS
	  INSERT INTO ATI_Factor_Raw (LBPatientId, FName, FValue)
	  SELECT p.LbPatientId, 'F_ER_VISITS' AS FACTORNAME, 
	  (CASE
	  WHEN SUM(P.[ER Visits]) > 3 THEN 2
	  WHEN SUM(P.[ER Visits]) > 1 THEN 1
	  ELSE 0
	  END) AS ERVISITS
	  FROM MM_Metics_Pivot P 
	  --JOIN PatientIdReference R ON R.ExternalId = P.BENE_HIC_NUM
	  WHERE DateDiff("Month", P.DOS_First, GETDATE()) <8
	  GROUP BY p.LbPatientId

	--Add Chronic Disease Factors
	--CHF
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'F_CHF' AS FName                 -- FName - varchar(50)
	  ,V.[CHF] AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V 
	where LbPatientId is not null

	--COPD
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'F_COPD' AS FName                 -- FName - varchar(50)
	  ,V.[COPD] AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V 
	where LbPatientId is not null

	--Oncology
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'F_ONCOLOGY' AS FName                 -- FName - varchar(50)
	  ,V.Oncology AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V 
	where LbPatientId is not null

	--DIABETES
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'F_DIABETES' AS FName                 -- FName - varchar(50)
	  ,V.[Diabetes] AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V 
	where LbPatientId is not null

	--ASTHMA
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'F_ASTHMA' AS FName                 -- FName - varchar(50)
	  ,V.[Asthma] AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V 
	where LbPatientId is not null

	--HYPERTENSION
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'F_HYPERTENSION' AS FName                 -- FName - varchar(50)
	  ,V.[Hypertension] AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V 
	where LbPatientId is not null

	--ESRD
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'F_ESRD' AS FName                 -- FName - varchar(50)
	  ,V.ESRD AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V
	where LbPatientId is not null

	--Mental Health
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'F_MentalHealth' AS FName                 -- FName - varchar(50)
	  ,V.[Mental Health] AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V 
	where LbPatientId is not null

	--Chronic Pain
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'F_ChronicPain' AS FName                 -- FName - varchar(50)
	  ,V.[Chronic Pain] AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V 
	where LbPatientId is not null

	------RISK SCORE
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	   v.LbPatientId AS LBPatientId             -- LBPatientId - int
	  ,'F_RISK' AS FName                 -- FName - varchar(50)
	  ,CASE 
		  WHEN V.[Risk Score]<10 THEN CAST(V.[Risk Score] as INT)            -- FValue - tinyint
		  ELSE 11 
		  END
	FROM Patient_Summary_Pivot as V 
	where LbPatientId is not null

	/*** --- Marital Status, where not null ***/
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	PatientID, 'F_MaritalStatus',
	(CASE
	WHEN MaritalStatus = 'M' THEN 1
	WHEN MaritalStatus = 'S' THEN 2
	WHEN MaritalStatus = 'D' THEN 3
	WHEN MaritalStatus = 'W' THEN 4
	end) as MStatus
	FROM PatientSocialHistory

	/*** --- Smoking Status, where not null ***/
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) SELECT 
	PatientID, 'F_SmokingStatus', try_convert(bigint,SmokingStatus) 
	FROM PatientSocialHistory

	/*BMI > 25
	INSERT INTO ATI_Factor_Raw (
	   LBPatientId
	  ,FName
	  ,[FValue]
	) 
	SELECT LbPatientId 
	FROM PatientIdReference AS P
	JOIN PatientVitalsProcessQueue
	*/

	--RESPONSE VARIABLES
	INSERT INTO ATI_Response_Raw (
	   LBPatientId
	  ,RName
	  ,[RValue]
	) SELECT 
	   v.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'R_12MO_COST' AS FName                 -- FName - varchar(50)
	  ,V.[12 Mo Cost] AS [FValue]                -- FValue - tinyint
	FROM Patient_Summary_Pivot as V 
	where (V.[12 Mo Cost] <> '' or V.[12 Mo Cost] is not null)
	and  LbPatientId is not null
	/*
	--ER VISITS
	INSERT INTO ATI_Response_Raw (
	   LBPatientId
	  ,RName
	  ,[RValue]
	) SELECT 
	   P.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'R_ER_VISITS' AS FName                 -- FName - varchar(50)
	  ,SUM(MM.[ER Visits]) AS [FValue]                -- FValue - tinyint
	FROM MM_Metics_Pivot AS MM
	JOIN PatientIdReference AS P
	ON MM.BENE_HIC_NUM = P.ExternalId
	WHERE DATEDIFF("m",MM.DOS_First, GETDATE()) < 12
	GROUP BY P.LbPatientId
	 */


	--INPATIENT ACUTE COST
	INSERT INTO ATI_Response_Raw (
	   LBPatientId
	  ,RName
	  ,[RValue]
	) SELECT 
	   mm.LbPatientId   AS LBPatientId             -- LBPatientId - int
	  ,'R_IPCOST' AS FName                 -- FName - varchar(50)
	  ,SUM(MM.[Inpatient Acute Cost]) AS [FValue]                -- FValue - tinyint
	FROM MM_Metics_Pivot AS MM
	WHERE DATEDIFF("m",MM.DOS_First, GETDATE()) < 12
	GROUP BY mm.LbPatientId


	UPDATE ATI_Factor_Raw SET
	  FValue = 0 -- int
	WHERE FValue IS NULL -- int


	UPDATE ATI_Response_Raw SET
	 RValue = 0 
	 WHERE RValue IS NULL
 

	--DELETE FROM ATI_Factor_Raw 
	--WHERE LBPatientId IN (
	--SELECT R.LbPatientId FROM Patient_Summary_Pivot P
	--JOIN PatientIdReference R ON R.ExternalId = P.BENE_HIC_NUM
	--WHERE 
	----P.[12 Mo Cost] = '' OR 
	--TRY_CONVERT(DECIMAL (6,2), P.[12 Mo Cost]) = 0 OR P.[12 Mo Cost] IS NULL)
	--

	--===========================================================
	--     Step 2: Calculate Response-Factor Relationships
	--           By: Mike Hoxter
	--           Origin Date: 9/8/2014
	--===========================================================

	--CALCUALTE FACTORIAL RESPONSES
	TRUNCATE TABLE ATI_FactorResponse


	INSERT INTO ATI_FactorResponse (
	   Factor
	  ,Response
	  ,FactorValue
	  ,ResponseMean
	  ,ResponseStDev
	  ,SampleSize
	) SELECT DISTINCT F.FName, R.RName, F.FValue, CAST(AVG(R.RValue) AS DECIMAL), CAST(STDEV(R.RValue) AS DECIMAL), COUNT(R.ID)
	FROM ATI_Factor_Raw F
	JOIN ATI_Response_Raw R ON F.LBPatientId = R.LBPatientId
	WHERE F.LBPatientId IN (
	SELECT p.LbPatientId FROM Patient_Summary_Pivot P
	WHERE 
	--P.[12 Mo Cost] = '' OR 
	TRY_CONVERT(DECIMAL (6,2), P.[12 Mo Cost]) > 0 OR P.[12 Mo Cost] IS not NULL)
	GROUP BY F.FName, R.RName, F.FValue

	--
	----PURGE LOW SAMPLE SIZE
	DELETE FROM ATI_FactorResponse 
	WHERE SampleSize < 50 --OR (Factor = 'F_RISK' AND FactorValue = 0)


	--Update overall mean 
	UPDATE ATI_FactorResponse SET
	  OverallMean = (SELECT AVG(R.RValue) as mean
	FROM ATI_Response_Raw R WHERE R.RName = FR.Response)
	FROM ATI_FactorResponse FR
   
  
	--Update overall STDEV
	UPDATE ATI_FactorResponse SET
	   OverallStDev = (SELECT STDEV(R.RValue) as mean
	FROM ATI_Response_Raw R WHERE R.RName = FR.Response)
	FROM ATI_FactorResponse FR
   
  
	--Calcualte Sb 
	UPDATE ATI_FactorResponse SET
	 Sb = (
	 SELECT  
	 SUM([SampleSize]*(([ResponseMean]-[OverallMean])*([ResponseMean]-[OverallMean])))
	 FROM ATI_FactorResponse rf WHERE  Factor = FR.Factor AND Response = FR.Response AND FactorValue = FR.FactorValue
	 )
	 FROM ATI_FactorResponse as FR
   
  
	--Calcualte fb
	UPDATE ATI_FactorResponse SET
	fb = (SELECT COUNT(DISTINCT(Factor+cast(FactorValue as varchar)))-1 FROM ATI_FactorResponse 
	WHERE Response = FR.Response)
	from ATI_FactorResponse as FR
   
  
	--Calculate MSb
	UPDATE ATI_FactorResponse SET
	MSb = Sb/fb
  
	  --Calcualte Sw 
	UPDATE ATI_FactorResponse SET
	 Sw = (
	 SELECT  
	 SUM([SampleSize]*(([ResponseMean]-[OverallMean])*([ResponseMean]-[OverallMean])))
	 FROM ATI_FactorResponse rf WHERE Response = FR.Response
	 )
	 FROM ATI_FactorResponse as FR
  
	--Calcualte fw
	UPDATE ATI_FactorResponse SET
	 fw = ((SELECT sum([SampleSize])-1 FROM ATI_FactorResponse
	 WHERE Response = FR.Response))
	 from ATI_FactorResponse FR
 
	 UPDATE ATI_FactorResponse SET
	 fw = (SELECT AVG(cast(([fb]+1)*([fw]) as bigint)) FROM ATI_FactorResponse
	   WHERE Response = FR.Response)
	 from ATI_FactorResponse FR
  
  
	--calculate MSw
	UPDATE ATI_FactorResponse SET
	  MSw = Sw/fw -- decimal(18, 0)


	--calcualte FResponse
	UPDATE ATI_FactorResponse SET
	FResponse = MSb/MSw -- decimal(18, 0)
	  WHERE MSw <> 0


	  --calcualte Fgroup
	UPDATE ATI_FactorResponse SET
	FGroup = (SELECT AVG(MSb)/AVG(MSw) FROM ATI_FactorResponse 
	WHERE Response = FR.Response)-- decimal(18, 0)
	 from ATI_FactorResponse FR
	  WHERE MSw <> 0


	--===========================================================
	--     Step 3: Use Factor-Response to Calculate ATI/VAR Weights
	--           By: Mike Hoxter
	--           Origin Date: 9/9/2014
	--===========================================================

	--CALCULATE ATI WEIGHT
	UPDATE ATI_FactorResponse SET
	   FWeight = FResponse/FGroup -- decimal(18, 3)
	WHERE FGroup <> 0

  
	--CALCULATE VAR WEIGHT
	UPDATE ATI_FactorResponse SET
	VARWeight = ResponseStDev/CASE WHEN OverallStDev = 0 THEN 1 ELSE OverallStDev END   -- decimal(18, 3)


	--===========================================================
	--     Step4: Use Weights to Calculate Values per Patient
	--           By: Mike Hoxter
	--           Origin Date: 9/16/2014
	--===========================================================

	--Write ATI_PatFactor to ATI_PatFactorHistory before Truncating
	INSERT INTO ATI_PatFactorHistory (
	   LBPatientID
	  ,MemberID
	  ,FactorCount
	  ,ATIWeightSum
	  ,VARWeightSum
	  ,ATIWeightFactor
	  ,ATIScore
	  ,WriteDate
	  ,ArchiveDate
	) SELECT LBPatientID, MemberID, FactorCount, ATIWeightSum, VARWeightSum, ATIWeightFactor, ATIScore, WriteDate, GetDate() FROM ATI_PatFactor
  
  
	--Truncate ATI_PatFactor
	TRUNCATE TABLE ATI_PatFactor


	--Insert Weight Sums & Factor Counts
	INSERT INTO ATI_PatFactor (
	   LBPatientID
	  --,MemberID
	  ,FactorCount
	  ,ATIWeightSum
	  ,VARWeightSum
	) 
	SELECT LBPatientId, COUNT(DISTINCT(R.ID)) AS FactorCount, SUM(R.FWeight) AS ATIWeightFactor, SUM(R.VARWeight) AS VARWeightSum  
	FROM ATI_Factor_Raw F
	JOIN ATI_FactorResponse R ON F.FName = R.Factor AND F.FValue = R.FactorValue
	--WHERE R.FactorValue <> 0
	GROUP BY LBPatientId


	--Update MemberID
	UPDATE ATI_PatFactor SET
	  MemberID = R.ExternalId -- varchar(20)
	  FROM ATI_PatFactor PF
	  join PatientIdReference R ON PF.LBPatientID = R.LbPatientId
	  where R.IdTypeDesc = 'BENE_HIC_NUM'
	--temporary fix
	  UPDATE ATI_PatFactor SET
		ATIScore = ATIScore+PSP.[Risk Score] 
		  FROM ATI_PatFactor A
		  JOIN Patient_Summary_Pivot PSP 
		  ON A.LBPatientID = PSP.LBPatientID
  
	  INSERT INTO ATI_PatFactor (
		 LBPatientID
		--,MemberID
		,FactorCount
		,ATIWeightSum
		,VARWeightSum
		,ATIWeightFactor
		,ATIScore
		,WriteDate
	  )   SELECT dbo.OrgHierarchy.LbPatientId--, PSP.BENE_HIC_NUM
	  , 1, 0, PSP.[Risk Score], 0, 0, GETDATE()  
			FROM Patient_Summary_Pivot PSP 
		  JOIN OrgHierarchy ON PSP.LBPatientID = dbo.OrgHierarchy.LBPatientID
		  LEFT JOIN ATI_PatFactor ATI ON PSP.LBPatientID = ATI.LBPatientID
		  WHERE ATI.ATI_PatFactors_ID IS NULL
      
      
	--Make ATIScore TempTable
	declare @ATITEMP table
	(
	  LBPatientID int,
	  ATIScore decimal(10,3)
	)
	INSERT INTO @ATITEMP (
	   LBPatientID
	  ,ATIScore
	) SELECT LBPatientID, NTILE(1000) OVER (ORDER BY VARWeightSum) ATIScore
	  FROM ATI_PatFactor
  
	--UPDATE ATI_PatFactor
	UPDATE ATI_PatFactor SET ATIScore = T.ATIScore/100
	FROM ATI_PatFactor A JOIN @ATITEMP T ON A.LBPatientID = T.LBPatientID


--===================================================================
--Reserved for ATI-Factor-Adjustment portion
--===================================================================

END

GO
