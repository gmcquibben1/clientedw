SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[PdePatientDemographicLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT DISTINCT 
		pd.PatientId, 
		pd.FirstName,
		pd.MiddleName,
		pd.LastName,
		pd.NamePrefix,
		pd.NameSuffix,
		pd.BirthDate,
		pd.ExternalReferenceIdentifier,
		pd.OtherReferenceIdentifier,
		pd.Gender,
		pd.Race,
		pd.Ethnicity,
		pd.PrimaryPhoneNumber,
		pd.PrimaryPhoneExtension,
		pd.PrimaryPhoneType,
		pd.SecondaryPhoneNumber,
		pd.SecondaryPhoneExtension,
		pd.SecondaryPhoneType,
		pd.PrimaryEmail,
		pd.SecondaryEmail,
		pd.DeceasedInd,
		pd.DeathDate,
		pd.AddressType,
		pd.AddressLine1,
		pd.AddressLine2,
		pd.City,
		pd.State,
		pd.PostalCode,
		pd.Patient_Status AS PatientStatus,
		pd.ProviderName AS AttributedProvider,
		pd.ProviderNPI AS AttributedProviderNPI,
		pd.CareGapCount,
		pd.RiskScore,
		pd.BusinessUnitExternalCode,
		pd.ATIScore
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientDemographic pd ON pd.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (pd.CreateDateTime > @dtmStartDate OR pd.ModifyDateTime > @dtmStartDate)
 
END

GO
