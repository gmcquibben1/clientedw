SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GPROPatientMeasureLoad]
AS
BEGIN
	
	--------------------------------------------------------------------------------
	--determine if he system will use clinical only, or clinical plus claims
	--------------------------------------------------------------------------------
	DECLARE @useClinical SMALLINT = 1;
	DECLARE @edwDbName VARCHAR(200) = DB_NAME();
	DECLARE @tranDbName VARCHAR(200);
	DECLARE @query nvarchar(4000)
	DECLARE @params nvarchar(500);

	SET @tranDbName = LTRIM(RTRIM(REPLACE(@edwDbName,'Edw','Lbportal') ));

	SET @query = N'SELECT @clinicalPref = (CASE WHEN SetValue = ''true'' THEN 1 ELSE 0 END) 
		FROM '+ @tranDbName +'..SystemSettings WHERE SettingType = ''GPro'' AND SettingParameter = ''ClinicalOnlyEnabled'''
	SET @params='@clinicalPref smallint OUTPUT'
	EXEC sp_executesql @query, @params, @clinicalPref=@useClinical OUTPUT

	--------------------------------------------------------------------------------
	--call all of the GPRO measure calc stored procedures
	--------------------------------------------------------------------------------
	EXEC [uspCalcMeasure_GPRO_2015@CARE-2] @dataPreference = @useClinical
	--EXEC [uspCalcMeasure_GPRO_2015@CARE-3] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@CAD-7] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@DM-2] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@DM-7] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@HF-6] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@HTN-2] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@IVD-2] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@MH-1] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@PREV-5] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@PREV-6] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@PREV-7] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@PREV-8] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@PREV-9] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@PREV-10] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@PREV-11] @dataPreference = @useClinical
	EXEC [uspCalcMeasure_GPRO_2015@PREV-12] @dataPreference = @useClinical


END

GO
