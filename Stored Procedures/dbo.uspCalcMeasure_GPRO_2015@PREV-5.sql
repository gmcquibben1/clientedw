SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@PREV-5] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1 
AS
BEGIN
--declare @dataPreference smallint = 1 
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];

	DECLARE  @yearend datetime, @measureBeginDate datetime;

	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2014';

	--Establishing the Clinical Data sourcing indicators.
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	--Initial Patient pooling, for patients that have ranking for the BCS measure. 
	SELECT r.LBPatientID,
        r.GProPatientRankingId,
		dateDiff(year,r.Birthdate,@measureBeginDate) as Age,
		r.Birthdate,
        GM.GproMeasureTypeID,
		r.GenderCode,
		GPM.[PcMammogramConfirmed],
		GPM.[PcMammogramPerformed]
	INTO #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
		JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE  Name = 'Prev-5'
		AND (GPM.[PcMammogramConfirmed] IS NULL OR gpm.PcMammogramPerformed IS NULL)
   
	--Denominator Exceptions
		
    -- Gender Based Exception
    UPDATE #RankedPatients
    SET    [PcMammogramConfirmed] = '18'
    FROM   #RankedPatients
    WHERE  [GenderCode] <> 2
    
    --Age Check
    UPDATE #RankedPatients
    SET    [PcMammogramConfirmed] = '19'
    FROM   #RankedPatients
    WHERE  ([Age] > 74 or [Age] < 50) --and [PcMammogramConfirmed] is null
    
	--------------------------------------------------------------------------------------------------------    
    -- Exceptions from Measure Spec
	--------------------------------------------------------------------------------------------------------
    --Mastectomy or Other Exclusion
	UPDATE STAGE
	SET	   [PcMammogramConfirmed] = '17'
	FROM   #RankedPatients STAGE
	WHERE  EXISTS(SELECT TOP 1 1 
				  FROM vwPatientProcedure pp
					INNER JOIN #RankedPatients rp ON rp.LbPatientId = pp.PatientId
					INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
					INNER JOIN GproExclusionExceptionCode geec ON geec.Code = pp.ProcedureCode 
						AND geec.ModuleType = 'PREV'
						AND geec.ModuleIndicatorGPRO = '5'
				  WHERE pp.PatientId = STAGE.LbPatientId)
		AND STAGE.PcMammogramConfirmed IS NULL
    
    UPDATE STAGE
	SET	   [PcMammogramConfirmed] = '2'
	FROM   #RankedPatients STAGE
	WHERE  [PcMammogramConfirmed] IS NULL  
		AND [Age] >= 52 AND [Age] <= 74

--------------------------------------------------------------------------------------------------------    
    -- Numerator Checks
--------------------------------------------------------------------------------------------------------    
    
    --Checking the 52-74 Patients first 
  	UPDATE Stage
	SET	   [PcMammogramPerformed] = N'2'
	FROM   #RankedPatients Stage
	WHERE  [PcMammogramConfirmed] = N'2'
		AND PcMammogramPerformed IS NULL
		AND [Age] >= 52 AND [Age] <= 74  
		AND EXISTS(SELECT TOP 1 1
				   FROM vwPatientProcedure pp
						INNER JOIN #RankedPatients rp ON rp.LbPatientId = pp.PatientId
						INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
						INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
							AND ModuleType = 'PREV'
							AND VariableName = 'MAMMO_CODE'
				   WHERE pp.ProcedureDateTime > DATEADD(MONTH,-27, @yearend)
				   )

	--LOINC CHECK
	UPDATE Stage
	SET	   [PcMammogramPerformed] = N'2'
	FROM   #RankedPatients Stage
	WHERE  [PcMammogramConfirmed] = N'2'
		AND PcMammogramPerformed IS NULL
		AND [Age] >= 52 AND [Age] <= 74  
		AND EXISTS(SELECT TOP 1 1
				   FROM PatientLabResult plr
						INNER JOIN PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
						INNER JOIN #RankedPatients rp ON rp.LbPatientId = plo.PatientId
						INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = plo.SourceSystemId
						INNER JOIN GproEvaluationCode gec ON (gec.Code = plr.ObservationCode1 OR gec.Code = plr.ObservationCode2)
							AND ModuleType = 'PREV'
							AND VariableName = 'MAMMO_CODE'
				   WHERE plr.ObservationDate > DATEADD(MONTH,-27, @yearend)
				   )
        
--------------------------------------------------------------------------------------------------------          
   -- Patient is 50 or 51 years old, only look back to their 50th birthday.
--------------------------------------------------------------------------------------------------------      
    
    UPDATE Stage
	SET	   [PcMammogramPerformed] = '2' 
	FROM   #RankedPatients Stage
	WHERE  [PcMammogramPerformed] IS NULL
		AND [PcMammogramConfirmed] = '2'    
		AND [Age] < 52 AND [Age] >= 50
		AND EXISTS (SELECT TOP 1 1
				    FROM vwPatientProcedure pp
						INNER JOIN #RankedPatients rp ON rp.LbPatientId = pp.PatientId
						INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
						INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
							AND ModuleType = 'PREV'
							AND VariableName = 'MAMMO_CODE'
					 WHERE pp.ProcedureDateTime >= DATEADD(year,50,BirthDate)
					)
      
    -- LABs for Younger Pool
    UPDATE Stage
	SET	   [PcMammogramPerformed] = '2'
	FROM   #RankedPatients Stage
	WHERE  PcMammogramPerformed IS NULL
		AND [PcMammogramConfirmed] = '2'
		AND [Age] < 52 AND [Age] >= 50
		AND EXISTS (SELECT TOP 1 1
				   FROM PatientLabResult plr
						INNER JOIN PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
						INNER JOIN #RankedPatients rp ON rp.LbPatientId = plo.PatientId
						INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = plo.SourceSystemId
						INNER JOIN GproEvaluationCode gec ON (gec.Code = plr.ObservationCode1 OR gec.Code = plr.ObservationCode2)
							AND ModuleType = 'PREV'
							AND VariableName = 'MAMMO_CODE'
				   WHERE plr.ObservationDate > DATEADD(year,50,BirthDate)
				   )
          
        
         
	--------------------------------------------------------------------------------------------------------          
	   -- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	--UPDATE gpm
	--SET  gpm.PcMammogramConfirmed = gpr.PcMammogramConfirmed,gpm.PcMammogramPerformed = GPR.[PcMammogramPerformed], gpm.ModifyDateTime = GETUTCDATE()
	--FROM GproPatientMeasure gpm
	--	   JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	--WHERE gpm.PcMammogramConfirmed IS NOT NULL

	UPDATE gpm
	SET  gpm.PcMammogramConfirmed = gpr.PcMammogramConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		   JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcMammogramRank IS NOT NULL
		AND gpm.PcMammogramConfirmed IS NULL

	UPDATE gpm
	SET  gpm.PcMammogramPerformed = gpr.PcMammogramPerformed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		   JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcMammogramRank IS NOT NULL
		AND gpm.PcMammogramPerformed IS NULL

END

     
GO
