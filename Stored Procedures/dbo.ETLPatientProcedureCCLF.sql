SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*===============================================================
	CREATED BY: 	Lan Ma
	CREATED ON:		2016-03-25
	INITIAL VER:	2.0.1
	MODULE:			ETL Interface Process	
	DESCRIPTION:	Load CCLF Patient Procedure Data into EDW
	PARAMETERS:		
				 @fullLoadFlag have values of 1 or 0. Setting this value will recalculate all of the values in the Patient Proceedures table

	RETURN VALUE(s)/OUTPUT:	
		Error and logging information will be written to the [EDWTableLoadTrackingInsert] table

	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
				2016-03-25		LM						Initial version
				2016-04-07 		YL						Add the procedure running status to [dbo].[EdwProcedureRunLog]
				2016-06-03		YL						Add Revenue, place of service, CCLF2 HCPCS
	2.1.2		2016-12-29		CJL			LBETL-315	Greenway Procedure Updates with NULL place of service
			
			
=================================================================*/

CREATE PROCEDURE [dbo].[ETLPatientProcedureCCLF](@fullLoadFlag bit = 0)

AS
BEGIN	

    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientProcedure';
	DECLARE @dataSource VARCHAR(20) = NULL;
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientProcedureCCLF';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();




IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
                WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'CCLF3' AND [EDWName] = DB_NAME())
  BEGIN

		 SET @dataSource = 'CCLF3';

		 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed;
  END

IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
                WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'CCLF2' AND [EDWName] = DB_NAME())
  BEGIN

		 SET @dataSource = 'CCLF2';

		 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed;
  END


IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
                WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'CCLF5' AND [EDWName] = DB_NAME())
  BEGIN

		 SET @dataSource = 'CCLF5';

		 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed;
  END

IF @fullLoadFlag = 1 
  BEGIN
    UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
	   SET [maxSourceIdProcessed] = 0
    WHERE [EDWtableName] = @EDWtableName AND ([dataSource] = 'CCLF3' OR [dataSource] = 'CCLF5' OR [dataSource] = 'CCLF2') AND [EDWName] = DB_NAME()

  END

--clean staging table
TRUNCATE TABLE [dbo].[PatientProcedureProcessQueue]
	
DECLARE @maxEDWIDCCLF2 INT = (SELECT [maxSourceIdProcessed] 
								FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
								WHERE [EDWtableName] = @EDWtableName --'PatientProcedure' 
								AND [dataSource] ='CCLF2'AND [EDWName] = DB_NAME());
	
DECLARE @maxSourceIDCCLF2 INT = (SELECT COALESCE(MAX([id]),0) FROM [dbo].[CCLF_2_PartA_RCDetail]);

IF @maxSourceIDCCLF2 > @maxEDWIDCCLF2
	BEGIN

		---Load new patient Diagnosies into staging table incrementally based on the ID in table CCLF3
		---Exclude the records that don't have vaules for LbPatientID, ProcedureCodingSystemName, ProcedureCode, ProcedureDateTime	
			
			SELECT 
				S.[id]
				,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID) AS  [InterfacePatientProcedureId]
				,S.LbPatientID AS InterfacePatientID 
				,ISNULL(SSS.SourceSystemId,0) AS InterfaceSystemId
				,S.[CLM_FROM_DT] AS [ProcedureDateTime]
				,LEFT(H.FAC_PRVDR_NPI_NUM,100) AS [PerformedByClinician]
				,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID) AS  InterfacePatientProcedureProcedureCodeId
				,LTRIM(RTRIM([CLM_LINE_HCPCS_CD])) AS ProcedureCode
				,'CPT4' AS ProcedureCodingSystemName
				,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID) As EncounterID
				,S.LbPatientID
				,S.CUR_CLM_UNIQ_ID AS EncounterIdentifier
				,S.[CLM_LINE_REV_CTR_CD]
				--,[HCPCS_1_MDFR_CD] [ProcedureCodeModifier1]
			    --,[HCPCS_2_MDFR_CD] [ProcedureCodeModifier2]
			    --,[HCPCS_3_MDFR_CD] [ProcedureCodeModifier3]
			    --,[HCPCS_4_MDFR_CD] [ProcedureCodeModifier4]
				,LTRIM(RTRIM(case when HCPCS_1_MDFR_CD='NULL' then '' else HCPCS_1_MDFR_CD end)) AS [ProcedureCodeModifier1]
				,LTRIM(RTRIM(case when HCPCS_2_MDFR_CD='NULL' then '' else HCPCS_2_MDFR_CD end)) AS [ProcedureCodeModifier2]
				,LTRIM(RTRIM(case when HCPCS_3_MDFR_CD='NULL' then '' else HCPCS_3_MDFR_CD end)) AS [ProcedureCodeModifier3]
				,LTRIM(RTRIM(case when HCPCS_4_MDFR_CD='NULL' then '' else HCPCS_4_MDFR_CD end)) as [ProcedureCodeModifier4]
					,ROW_NUMBER() OVER(PARTITION BY S.CUR_CLM_UNIQ_ID,S.LbPatientID, [CLM_LINE_HCPCS_CD], S.[CLM_FROM_DT]
					ORDER BY S.[id] ) AS rowNbr
				INTO #CCLF_2_PartA_RCDetail 
				FROM [dbo].[CCLF_2_PartA_RCDetail] S (NOLOCK)
				--INNER JOIN [dbo].[PatientIdReference] PIR (NOLOCK) ON S.BENE_HIC_NUM = PIR.ExternalId AND PIR.IdTypeDesc = 'BENE_HIC_NUM'
				INNER JOIN CCLF_1_PartA_Header H (NOLOCK)ON S.CUR_CLM_UNIQ_ID = H.CUR_CLM_UNIQ_ID 
				LEFT JOIN SourceSystemSetting SSS (NOLOCK) ON ISNULL(S.SourceFeed,'Default') = SSS.SettingValue AND SSS.SettingName = 'ResolvingCCLFName'
			WHERE ISNULL([CLM_LINE_HCPCS_CD] ,'') <> ''
			      AND S.[CLM_FROM_DT] IS NOT NULL
			      AND S.LbPatientID IS NOT NULL 
			      AND S.[id] > @maxEDWIDCCLF2	





			INSERT INTO dbo.PatientProcedureProcessQueue
			 (
				sourceIdCCLF3
			   ,[InterfacePatientProcedureId]
			   ,[InterfacePatientID]
			   ,[InterfaceSystemId]
			   ,[ProcedureDateTime]
			   ,[PerformedByClinician]
			   ,[InterfacePatientProcedureProcedureCodeId]
			   ,[ProcedureCode]
			   ,[ProcedureCodingSystemName]
			   ,[EncounterId]
			   ,[LbPatientID]
			   ,[EncounterIdentifier]
			    ,RevenueCode
			    ,[ProcedureCodeModifier1]
			    ,[ProcedureCodeModifier2]
			    ,[ProcedureCodeModifier3]
			    ,[ProcedureCodeModifier4]
			   )
			 SELECT 
				[id]
				,[InterfacePatientProcedureId]
				,InterfacePatientID 
				,InterfaceSystemId
				,[ProcedureDateTime]
				,[PerformedByClinician]
				,InterfacePatientProcedureProcedureCodeId
				,ProcedureCode
				,ProcedureCodingSystemName
				,EncounterID
				,LbPatientID
				, EncounterIdentifier
				,[CLM_LINE_REV_CTR_CD]
				,[ProcedureCodeModifier1]
			    ,[ProcedureCodeModifier2]
			    ,[ProcedureCodeModifier3]
			    ,[ProcedureCodeModifier4]

				FROM #CCLF_2_PartA_RCDetail 
			WHERE rowNbr=1				 
	END


--set parameter values
DECLARE @maxEDWIDCCLF3 INT = (SELECT [maxSourceIdProcessed] 
								FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
								WHERE [EDWtableName] = @EDWtableName --'PatientProcedure' 
								AND [dataSource] ='CCLF3'AND [EDWName] = DB_NAME());
	
DECLARE @maxSourceIDCCLF3 INT = (SELECT COALESCE(MAX([id]),0) FROM [dbo].[CCLF_3_PartA_ProcCd]);

IF @maxSourceIDCCLF3 > @maxEDWIDCCLF3
	BEGIN

		---Load new patient Diagnosies into staging table incrementally based on the ID in table CCLF3
		---Exclude the records that don't have vaules for LbPatientID, ProcedureCodingSystemName, ProcedureCode, ProcedureDateTime	
			INSERT INTO dbo.PatientProcedureProcessQueue
			 (
				sourceIdCCLF3
			   ,[InterfacePatientProcedureId]
			   ,[InterfacePatientID]
			   ,[InterfaceSystemId]
			   ,[ProcedureDateTime]
			   ,[PerformedByClinician]
			   ,[InterfacePatientProcedureProcedureCodeId]
			   ,[ProcedureCode]
			   ,[ProcedureCodingSystemName]
			   ,[EncounterId]
			   ,[LbPatientID]
			   ,[EncounterIdentifier]
			   )
			SELECT
				S.[id]
				,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID) AS  [InterfacePatientProcedureId]
				,S.LbPatientID AS InterfacePatientID 
				,ISNULL(SSS.SourceSystemId,0) AS InterfaceSystemId
				,S.CLM_FROM_DT AS [ProcedureDateTime]
				,LEFT(H.FAC_PRVDR_NPI_NUM,100) AS [PerformedByClinician]
				,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID) AS  InterfacePatientProcedureProcedureCodeId
				,LTRIM(RTRIM(CLM_PRCDR_CD)) AS ProcedureCode
				,'CPT4' AS ProcedureCodingSystemName
				,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID) As EncounterID
				,S.LbPatientID
				,S.CUR_CLM_UNIQ_ID AS EncounterIdentifier
				FROM  CCLF_3_PartA_ProcCd S (NOLOCK)
				--INNER JOIN [dbo].[PatientIdReference] PIR (NOLOCK) ON S.BENE_HIC_NUM = PIR.ExternalId AND PIR.IdTypeDesc = 'BENE_HIC_NUM'
				INNER JOIN CCLF_1_PartA_Header H (NOLOCK)ON S.CUR_CLM_UNIQ_ID = H.CUR_CLM_UNIQ_ID 
				LEFT JOIN SourceSystemSetting SSS (NOLOCK) ON ISNULL(S.SourceFeed,'Default') = SSS.SettingValue AND SSS.SettingName = 'ResolvingCCLFName'
			WHERE CLM_PRCDR_CD IS NOT NULL AND LTRIM(RTRIM(CLM_PRCDR_CD)) <> ''
			      AND S.CLM_FROM_DT IS NOT NULL
			      AND S.LbPatientID IS NOT NULL
			      AND S.[id] > @maxEDWIDCCLF3				 
	END

DECLARE @maxEDWIDCCLF5 INT = (SELECT [maxSourceIdProcessed] 
	                            FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
								WHERE [EDWtableName] = @EDWtableName --'PatientProcedure'  
								AND [dataSource] ='CCLF5'AND [EDWName] = DB_NAME());
	
DECLARE @maxSourceIDCCLF5 INT = (SELECT COALESCE(MAX([id]),0) FROM [dbo].[CCLF_5_PartB_Physicians]);

IF @maxSourceIDCCLF5 > @maxEDWIDCCLF5
	BEGIN	

	
      ---Load new patient Diagnosis into staging table incrementally based on the ID intable CCLF5
	  ---Exclude the records that don't have vaules for DiagnosisCodingSystemName, DiagnosisCode, DiagnosisDateTime
		INSERT INTO dbo.PatientProcedureProcessQueue
			(
			sourceIdCCLF5
			,[InterfacePatientProcedureId]
			,[InterfacePatientID]
			,[InterfaceSystemId]
			,[ProcedureCodeModifier1]
			,[ProcedureCodeModifier2]
			,[ProcedureCodeModifier3]
			,[ProcedureCodeModifier4]
			,[ProcedureDateTime]
			,[PerformedByClinician]
			,[InterfacePatientProcedureProcedureCodeId]
			,[ProcedureCode]
			,[ProcedureCodingSystemName]
			,[EncounterId]
			,[LbPatientID]
			,[EncounterIdentifier]
			,[PlaceOfService]
			)
		SELECT
			[id]
			,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID) AS  [InterfacePatientProcedureId]
			,S.LbPatientID AS InterfacePatientID
			,ISNULL(SSS.SourceSystemId,0) AS InterfaceSystemId
			,LTRIM(RTRIM(case when HCPCS_1_MDFR_CD='NULL' then '' else HCPCS_1_MDFR_CD end)) AS [ProcedureCodeModifier1]
			,LTRIM(RTRIM(case when HCPCS_2_MDFR_CD='NULL' then '' else HCPCS_2_MDFR_CD end)) AS [ProcedureCodeModifier2]
			,LTRIM(RTRIM(case when HCPCS_3_MDFR_CD='NULL' then '' else HCPCS_3_MDFR_CD end)) AS [ProcedureCodeModifier3]
			,LTRIM(RTRIM(case when HCPCS_4_MDFR_CD='NULL' then '' else HCPCS_4_MDFR_CD end)) as [ProcedureCodeModifier4]
			, CLM_FROM_DT AS [ProcedureDateTime]
			, LEFT(RNDRG_PRVDR_NPI_NUM,100) AS [PerformedByClinician]
			,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID) AS  InterfacePatientProcedureProcedureCodeId
			,LTRIM(RTRIM(CLM_LINE_HCPCS_CD)) AS ProcedureCode
			,'CPT4' AS ProcedureCodingSystemName
			,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID) As EncounterID
			,S.LbPatientID,
			S.CUR_CLM_UNIQ_ID AS EncounterIdentifier
			,S.CLM_POS_CD
			FROM  CCLF_5_PartB_Physicians S  (NOLOCK)
			--INNER JOIN [dbo].[PatientIdReference] PIR (NOLOCK) ON S.BENE_HIC_NUM = PIR.ExternalId AND PIR.IdTypeDesc = 'BENE_HIC_NUM'
			LEFT JOIN SourceSystemSetting SSS (NOLOCK) ON ISNULL(S.SourceFeed,'Default') = SSS.SettingValue AND SSS.SettingName = 'ResolvingCCLFName'
			WHERE CLM_LINE_HCPCS_CD IS NOT NULL AND LTRIM(RTRIM(CLM_LINE_HCPCS_CD)) <> '' 
			      AND S.CLM_FROM_DT IS NOT NULL
			      AND S.LbPatientID IS NOT NULL
			      AND [id] > @maxEDWIDCCLF5
				  		 
	END

--only process if there are data in processQueue table
IF EXISTS (SELECT 1 FROM dbo.PatientProcedureProcessQueue)
    BEGIN

	BEGIN TRY
    --get all codingSystemName and ID into temp table
	    SELECT 
		ProcedureCodingSystemTypeID,
		RefCodingSystemName
		INTO #ProcedureCodingSystemType
		FROM
	  	(
		  SELECT
		  PCST.ProcedureCodingSystemTypeID,
		  PCST.NAME As RefCodingSystemName
		  FROM ProcedureCodingSystemType PCST 
		  UNION
		  SELECT 
		  PCST.ProcedureCodingSystemTypeID,
		  PCSTS.SynonymName As RefCodingSystemName
		  FROM ProcedureCodingSystemType PCST 
		  INNER JOIN ProcedureCodingSystemTypeSynonyms PCSTS ON PCST.ProcedureCodingSystemTypeID = PCSTS.ProcedureCodingSystemTypeID
		) t2
			

		
		--loading Procedure dimension table

		EXEC [dbo].[ETLProcedureCode]	
						
		--load deduped patient Procedure data into temp table fisrt, data deduped by sourceSystemID, LbpatientId, procedureCodeID, and procedureDateTime			     	  
	    SELECT *
		INTO #patientProcedureCCLF
		FROM
		(
			SELECT
			 LbPatientId AS PatientID
			,[InterfaceSystemId] AS sourceSystemID
			,t2.ProcedureCodingSystemTypeID
			,t3.ProcedureCodeID
			, ProcedureFunctionTypeID
			,[ProcedureCodeModifier1]
			,[ProcedureCodeModifier2]
			,[ProcedureCodeModifier3]
			,[ProcedureCodeModifier4]
			,[ProcedureDateTime]
			,[ProcedureComment]
			,[EncounterID]
			,[PerformedByClinician]
			,[OrderedByClinician]
			,EncounterIdentifier
			,TotalCharges
			,InternalChargeCode
			,ProcedureResult
			,CASE WHEN(LEN(LTRIM(RTRIM(PerformedByClinician))) = 10 AND ISNUMERIC (LTRIM(RTRIM(PerformedByClinician))) = 1)
			      THEN LTRIM(RTRIM(PerformedByClinician))
			 END AS RenderingProviderNPI
			--,[PerformedByClinician] AS [RenderingProviderNPI]
			,[PlaceOfService]
			,t1.RevenueCode
			,ROW_NUMBER() OVER(PARTITION BY [InterfaceSystemId],LbPatientID, t3.ProcedureCodeId, Proceduredatetime
					ORDER BY [InterfacePatientProcedureId] DESC ) AS rowNbr
			FROM PatientProcedureProcessQueue t1
			INNER JOIN  #ProcedureCodingSystemType t2
			ON LTRIM(RTRIM(t1.ProcedureCodingSystemName)) = LTRIM(RTRIM(t2.RefCodingSystemName))
			INNER JOIN [dbo].[ProcedureCode] t3
			ON t2.[ProcedureCodingSystemTypeId] = t3.[ProcedureCodingSystemTypeId] 
			AND LTRIM(RTRIM(t1.[ProcedureCode])) = LTRIM(RTRIM(t3.[ProcedureCode]))
         ) AS tmp
		 WHERE rowNbr = 1


		  	SET @RecordCountSource=@@ROWCOUNT;

			SELECT @RecordCountBefore=COUNT(1)
			FROM [dbo].PatientProcedure;

    CREATE CLUSTERED INDEX CIX_#patientProcedureCCLF_PatientIDPPID ON #patientProcedureCCLF([PatientID],[SourceSystemID],ProcedureCodeID,ProcedureDateTime)

		--Load patientProcedure and [dbo].[PatientProcedureProcedureCode]
		BEGIN TRAN
		 MERGE PatientProcedure T		 
		 USING #patientProcedureCCLF S
		 ON T.[PatientID] = S.[PatientID]
		 AND T.[SourceSystemID] = S.[SourceSystemID]
		 AND T.ProcedureCodeID = S.ProcedureCodeID
		 AND T.ProcedureDateTime = S.ProcedureDateTime
		 WHEN MATCHED 
		  THEN UPDATE SET   T.[ProcedureFunctionTypeID] = S.ProcedureFunctionTypeID,
							T.[ProcedureCodeModifier1] = S.[ProcedureCodeModifier1],
							T.[ProcedureCodeModifier2] = S.[ProcedureCodeModifier2],
							T.[ProcedureCodeModifier3] = S.[ProcedureCodeModifier3],
							T.[ProcedureCodeModifier4] = S.[ProcedureCodeModifier4],
							T.[ProcedureDateTime] = S.[ProcedureDateTime],
							T.[ProcedureComment] = S.[ProcedureComment],
							T.[EncounterID] = S.[EncounterID],
							T.[PerformedByClinician] = S.[PerformedByClinician],
							T.[OrderedByClinician] = S.[OrderedByClinician],
							T.EncounterIdentifier = S.EncounterIdentifier,
							T.TotalCharges = S.TotalCharges,
							T.InternalChargeCode = S.InternalChargeCode,
							T.ProcedureResult = S.ProcedureResult,
							T.[RenderingProviderNPI] = S.[RenderingProviderNPI],
							T.[PlaceOfService] = ISNULL(S.[PlaceOfService], T.[PlaceOfService]),		--LBETL-315
							T.RevenueCode=S.RevenueCode,
							T.[DeleteInd] = 0,
							T.[ModifyDateTime] = GETUTCDATE()
         WHEN NOT MATCHED BY TARGET
           THEN INSERT ( PatientID,SourceSystemId,ProcedureCodeID,ProcedureCodingSystemTypeID, ProcedureFunctionTypeID,[ProcedureCodeModifier1],[ProcedureCodeModifier2],[ProcedureCodeModifier3]
			           ,[ProcedureCodeModifier4],[ProcedureDateTime],[ProcedureComment],[EncounterID],[PerformedByClinician],[OrderedByClinician],EncounterIdentifier,TotalCharges,InternalChargeCode
			           ,ProcedureResult,[RenderingProviderNPI],[PlaceOfService],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId],RevenueCode)
                Values(	S.PatientID,S.SourceSystemId,S.ProcedureCodeID,S.ProcedureCodingSystemTypeID, S.ProcedureFunctionTypeID,S.[ProcedureCodeModifier1],S.[ProcedureCodeModifier2],S.[ProcedureCodeModifier3]
			           ,S.[ProcedureCodeModifier4],S.[ProcedureDateTime],S.[ProcedureComment],S.[EncounterID],S.[PerformedByClinician],S.[OrderedByClinician],S.EncounterIdentifier,S.TotalCharges,S.InternalChargeCode
			           ,S.ProcedureResult,S.[RenderingProviderNPI],S.[PlaceOfService],0,GETUTCDATE(),GETUTCDATE(),1,1,RevenueCode);

		 MERGE [dbo].[PatientProcedureProcedureCode] T
		 USING (SELECT [PatientProcedureId], [ProcedureCodeId], [ProcedureCodingSystemTypeId] 
		        FROM patientProcedure 
				WHERE [ProcedureCodeId] IS NOT NULL AND [ProcedureCodingSystemTypeId] IS NOT NULL) S
		 ON T.[PatientProcedureId] = S.[PatientProcedureId]
         WHEN NOT MATCHED BY TARGET
         THEN INSERT ([PatientProcedureId],[ProcedureCodingSystemTypeId] ,[ProcedureCodeId])
		      VALUES(S.[PatientProcedureId],S.[ProcedureCodingSystemTypeId] ,S.[ProcedureCodeId]);
		
				
 -- get total records from [dbo].PatientProcedure after merge 
		       			SELECT @RecordCountAfter=count(1)
			            FROM [dbo].PatientProcedure;

              -- Calculate records inserted and updated counts
	            SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	            SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			   SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	 
		-- update maxSourceID in [Maintenance]..[EDWTableLoadTracking]
        UPDATE [Maintenance]..[EDWTableLoadTracking]
         SET [maxSourceIdProcessed] = (SELECT COALESCE(MAX(sourceIdCCLF3),@maxEDWIDCCLF3) FROM [dbo].PatientProcedureProcessQueue),
		     [updateDateTime] =GETUTCDATE()
        WHERE [EDWtableName] = 'PatientProcedure' AND [dataSource] ='CCLF3'  AND [EDWName] = DB_NAME()

		 UPDATE [Maintenance]..[EDWTableLoadTracking]
         SET [maxSourceIdProcessed] = (SELECT COALESCE(MAX(sourceIdCCLF5),@maxEDWIDCCLF5) FROM [dbo].PatientProcedureProcessQueue),
		     [updateDateTime] =GETUTCDATE()
        WHERE [EDWtableName] = 'PatientProcedure' AND [dataSource] ='CCLF5'  AND [EDWName] = DB_NAME()

		 UPDATE [Maintenance]..[EDWTableLoadTracking]
         SET [maxSourceIdProcessed] = @maxSourceIDCCLF2,
		     [updateDateTime] =GETUTCDATE()
        WHERE [EDWtableName] = 'PatientProcedure' AND [dataSource] ='CCLF2'  AND [EDWName] = DB_NAME()

		COMMIT
		
		DROP TABLE #patientProcedureCCLF, #ProcedureCodingSystemType
	END TRY
	
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
  END

END
GO
