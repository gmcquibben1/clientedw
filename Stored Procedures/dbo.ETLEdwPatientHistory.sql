SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwPatientHistory] 
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES FOR NULL COLUMNS

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId,LbPatientID, DateRecorded 
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				InterfacePatientHistoryId
				DESC 
			) SNO
	FROM PatientHistoryProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientHistoryProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientHistoryId INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientHistoryId,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientHistoryId,PatientID
FROM 
(	
MERGE [DBO].PatientHistory AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,DateRecorded AS DateRecorded,
			[HistoryTypeCode] AS [HistoryTypeCode],[NPI] AS [NPI],[Comment] AS [Comment],
			0 AS DeleteInd, GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId
		FROM [dbo].PatientHistoryProcessQueue PPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  ([OrigPatientId],[PatientId],[SourceSystemID],[DateRecorded],[HistoryTypeCode],[NPI],[Comment],
	   [DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
ON ( target.PatientID = source.PatientID AND ISNULL(target.DateRecorded,'2099-01-01') = ISNULL(source.DateRecorded,'2099-01-01') )
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
--[PatientId]			= source.[PatientId],
--[DateRecorded]		= source.[DateRecorded],
[HistoryTypeCode]	= source.[HistoryTypeCode],
[NPI]				= source.[NPI],
[Comment]			= source.[Comment],
[DeleteInd]			= source.[DeleteInd],
--[CreateDateTime]		= source.[CreateDateTime],
[ModifyDateTime]		= source.[ModifyDateTime],
--[CreateLBUserId]		= source.[CreateLBUserId],
[ModifyLBUserId]		= source.[ModifyLBUserId]
WHEN NOT MATCHED THEN
INSERT ([PatientId],[DateRecorded],[HistoryTypeCode],[NPI],[Comment],
		[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId] )
VALUES (source.[PatientId],source.[DateRecorded],source.[HistoryTypeCode],source.[NPI],source.[Comment],
		source.[DeleteInd],source.[CreateDateTime],source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientHistoryId,INSERTED.PatientHistoryId) PatientHistoryId, source.PatientID
) x ;

IF OBJECT_ID('PatientHistory_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientHistory_PICT(PatientHistoryId,PatientId,SwitchDirectionId)
	SELECT RT.PatientHistoryId, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientHistoryProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'PatientHistory',GetDate()
FROM PatientHistoryProcessQueue
WHERE ProcessedInd = 1 


END


GO
