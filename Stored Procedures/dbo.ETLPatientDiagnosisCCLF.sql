SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-03-23
-- Description:	Load CCLF Patient Diagnosis Data into EDW
-- Modified By:  Youping
-- Modified Date: 2016-04-07 
-- Modification:  add the procedure running status to [dbo].[EdwProcedureRunLog]
-- Notes: @fullLoadFlag have values of 1 or 0
--       

-- Modified Date: 2016-06-02
-- Modified by: Youping
-- Modification: Add DRG code 
-- ===============================================================
/*
MODIFICATIONS
	Version     Date            Author		JIRA		Change  
   ========     ==========      ======      =========	=========	
    2.2.1	    2017-01-25		YL		    LBETL-591	 Needs to pull cclf5 diag code 2 - 8.  
    2.3.1       2017-03-15      YL          LBETL-1088   Fort Drum ICD9/ICD10 Descriptions

*/



CREATE PROCEDURE [dbo].[ETLPatientDiagnosisCCLF](@fullLoadFlag bit = 0)

AS
BEGIN	
    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientDiagnosis';
	DECLARE @dataSource VARCHAR(20) = NULL;
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientDiagnosisCCLF';
	DECLARE @maxSourceTimeStampProcessed DATETIME = NULL;
	DECLARE @maxSourceIdProcessed INT = 0;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();




IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
                WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'CCLF4' AND [EDWName] = DB_NAME())
  BEGIN

		 SET @dataSource = 'CCLF4';

		 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed;
  END

IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
                WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'CCLF5' AND [EDWName] = DB_NAME())
  BEGIN

		 SET @dataSource = 'CCLF5';

		 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed;
  END

IF @fullLoadFlag = 1 
  BEGIN
    UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
	   SET [maxSourceIdProcessed] = 0
    WHERE [EDWtableName] = @EDWtableName AND ([dataSource] = 'CCLF4' OR [dataSource] = 'CCLF5') AND [EDWName] = DB_NAME()

    --UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
	   --SET [maxSourceIdProcessed] = 0
    --WHERE [EDWtableName] = 'PatientDiagnosis' AND ([dataSource] ='CCLF3' OR [dataSource] ='CCLF5')AND [EDWName] = DB_NAME()
  END

--clean staging table
TRUNCATE TABLE [dbo].[PatientDiagnosisProcessQueue]
	
--set parameter values
DECLARE @maxEDWIDCCLF4 INT = (SELECT [maxSourceIdProcessed] 
								FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
								WHERE [EDWtableName] = @EDWtableName --'PatientDiagnosis' 
								AND [dataSource] ='CCLF4'AND [EDWName] = DB_NAME());
	
DECLARE @maxSourceIDCCLF4 INT = (SELECT COALESCE(MAX([id]),0) FROM [dbo].[CCLF_4_PartA_Diagnosis]);

IF @maxSourceIDCCLF4 > @maxEDWIDCCLF4
	BEGIN
	
		---Load new patient Diagnosies into staging table incrementally based on the ID intable CCLF4
		---Exclude the records that don't have vaules for DiagnosisCodingSystemName, DiagnosisCode, DiagnosisDateTime		
		INSERT INTO dbo.PatientDiagnosisProcessQueue
			(
			sourceIdCCLF4
			,[InterfacePatientDiagnosisId]
			,[InterfacePatientID]
			,[LbPatientID]
			,[InterfaceSystemId]
			,[DiagnosisDateTime]
			,[Clinician]
			,[InterfacePatientDiagnosisDiagnosisCodeId]
			,[DiagnosisCode]
			,[DiagnosisCodingSystemName]
			,[DiagnosisDescription]
			,[EncounterId]
			,[EncounterIdentifier]
			, DrgCode
			)

		SELECT 
		S.[id]
		,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID)  AS [InterfacePatientDiagnosisId]  
		,S.LbPatientID AS [InterfacePatientID] 
		,S.[LbPatientID]  
		,ISNULL(SSS.SourceSystemId,0) AS InterfaceSystemId 
		,S.CLM_FROM_DT AS [DiagnosisDateTime] 
		,LEFT(H.FAC_PRVDR_NPI_NUM,100) AS  [Clinician] 
		,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID)  AS [InterfacePatientDiagnosisDiagnosisCodeId]
		, LEFT(LTRIM(RTRIM(REPLACE(CLM_DGNS_CD,'.',''))),20)   AS [DiagnosisCode] ,
		--'ICD-9' AS [DiagnosisCodingSystemName] ,
		CASE WHEN S.CLM_FROM_DT >= CAST('2015-10-01' AS DATE)
		     THEN 'ICD-10'
			 ELSE 'ICD-9'
		END AS [DiagnosisCodingSystemName],	 
		'' AS [DiagnosisDescription] , 
		ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID)  As EncounterID, 
		S.CUR_CLM_UNIQ_ID AS EncounterIdentifier
		,H.DGNS_DRG_CD
		FROM [dbo].CCLF_4_PartA_Diagnosis S (NOLOCK)
		INNER JOIN CCLF_1_PartA_Header H (NOLOCK) ON S.CUR_CLM_UNIQ_ID = H.CUR_CLM_UNIQ_ID
		LEFT JOIN SourceSystemSetting SSS (NOLOCK) ON ISNULL(S.SourceFeed,'Default') = SSS.SettingValue AND SSS.SettingName = 'ResolvingCCLFName'
		WHERE CLM_DGNS_CD IS NOT NULL AND LTRIM(RTRIM(CLM_DGNS_CD)) <> '' 
		      AND S.CLM_FROM_DT IS NOT NULL
			  AND S.LbPatientID IS NOT NULL
		      AND S.[id] > @maxEDWIDCCLF4
				 
	END

DECLARE @maxEDWIDCCLF5 INT = (SELECT [maxSourceIdProcessed] 
	                            FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
								WHERE [EDWtableName] = @EDWtableName --'PatientDiagnosis'  
								AND [dataSource] ='CCLF5'AND [EDWName] = DB_NAME());
	
DECLARE @maxSourceIDCCLF5 INT = (SELECT COALESCE(MAX([id]),0) FROM [dbo].[CCLF_5_PartB_Physicians]);

IF @maxSourceIDCCLF5 > @maxEDWIDCCLF5
	BEGIN
	
	  ---Load new patient Diagnosis into staging table incrementally based on the ID intable CCLF5
	  ---Exclude the records that don't have vaules for DiagnosisCodingSystemName, DiagnosisCode, DiagnosisDateTime
	  	SELECT 	S.ID
		,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID)  AS [InterfacePatientDiagnosisId]  
		,S.LbPatientID AS [InterfacePatientID] 
		,S.[LbPatientID]  
		,S.CLM_FROM_DT [DiagnosisDateTime]
		,LEFT(RNDRG_PRVDR_NPI_NUM,100) AS  [Clinician] 
		,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID)  AS [InterfacePatientDiagnosisDiagnosisCodeId],
		--LEFT(LTRIM(RTRIM(REPLACE(CLM_LINE_DGNS_CD,'.',''))),20)  AS [DiagnosisCode] ,
		--,'ICD-9' AS [DiagnosisCodingSystemName] 
		CASE WHEN S.CLM_FROM_DT >= CAST('2015-10-01' AS DATE)
		     THEN 'ICD-10'
			 ELSE 'ICD-9'
		END AS [DiagnosisCodingSystemName]
		,'' AS [DiagnosisDescription]   
		,ISNULL(TRY_CONVERT(BIGINT,LTRIM(RTRIM(S.CUR_CLM_UNIQ_ID))),S.ID)  As EncounterID 
		,S.CUR_CLM_UNIQ_ID AS EncounterIdentifier
		,ROW_NUMBER() OVER(PARTITION BY S.CUR_CLM_UNIQ_ID, S.CLM_FROM_DT  
					ORDER BY S.ID DESC ) AS rowNbr
					,[CLM_DGNS_1_CD]
					,[CLM_DGNS_2_CD]
				  ,[CLM_DGNS_3_CD]
				  ,[CLM_DGNS_4_CD]
				  ,[CLM_DGNS_5_CD]
				  ,[CLM_DGNS_6_CD]
				  ,[CLM_DGNS_7_CD]
				  ,[CLM_DGNS_8_CD]
				  ,SourceFeed
		INTO #CCLF_5_Data
		FROM [dbo].CCLF_5_PartB_Physicians S (NOLOCK) 
		WHERE CLM_LINE_DGNS_CD IS NOT NULL AND LTRIM(RTRIM(CLM_LINE_DGNS_CD)) <> '' 
		      AND S.CLM_FROM_DT IS NOT NULL
			  AND S.LbPatientID IS NOT NULL
		      AND [id] > @maxEDWIDCCLF5

       SELECT * INTO #CCLF_5_DataDedup from  #CCLF_5_Data WHERE rowNbr=1

	   CREATE INDEX IDX_CCLF5_ID ON #CCLF_5_DataDedup ([ID]) INCLUDE ([CLM_DGNS_1_CD],[CLM_DGNS_2_CD],[CLM_DGNS_3_CD] ,[CLM_DGNS_4_CD] ,[CLM_DGNS_5_CD] ,[CLM_DGNS_6_CD] ,[CLM_DGNS_7_CD] ,[CLM_DGNS_8_CD]);

	   --get up to 8 diagnosis code for each ID
	   SELECT DISTINCT [ID],CLM_DGNS_CD
	   INTO #CCLF_5_DiagCodes
			FROM (SELECT [ID],[CLM_DGNS_1_CD]
				  ,[CLM_DGNS_2_CD]
				  ,[CLM_DGNS_3_CD]
				  ,[CLM_DGNS_4_CD]
				  ,[CLM_DGNS_5_CD]
				  ,[CLM_DGNS_6_CD]
				  ,[CLM_DGNS_7_CD]
				  ,[CLM_DGNS_8_CD]
			FROM #CCLF_5_DataDedup
			
			)  AS S

			UNPIVOT ( CLM_DGNS_CD FOR D_CODE IN ([CLM_DGNS_1_CD]
				  ,[CLM_DGNS_2_CD]
				  ,[CLM_DGNS_3_CD]
				  ,[CLM_DGNS_4_CD]
				  ,[CLM_DGNS_5_CD]
				  ,[CLM_DGNS_6_CD]
				  ,[CLM_DGNS_7_CD]
				  ,[CLM_DGNS_8_CD])) AS P
			WHERE LEN(CLM_DGNS_CD)>1

           CREATE INDEX IDX_CCLF5_ID_DiagCodes ON #CCLF_5_DiagCodes ([ID]) ; 

		INSERT INTO dbo.PatientDiagnosisProcessQueue
			(
				sourceIdCCLF5
				,[InterfacePatientDiagnosisId]
				,[InterfacePatientID]
				,[LbPatientID]
				,[InterfaceSystemId]
				,[DiagnosisDateTime]
				,[Clinician]
				,[InterfacePatientDiagnosisDiagnosisCodeId]
				,[DiagnosisCode]
				,[DiagnosisCodingSystemName]
				,[DiagnosisDescription]
				,[EncounterId]
				,[EncounterIdentifier]
				,DrgCode
			)
		SELECT 	S.ID
		,[InterfacePatientDiagnosisId]  
		,S.[InterfacePatientID] 
		,S.[LbPatientID]  
		,ISNULL(SSS.SourceSystemId,0) AS InterfaceSystemId
		,S.[DiagnosisDateTime]
		,S.[Clinician] 
		,S.[InterfacePatientDiagnosisDiagnosisCodeId],
		LEFT(LTRIM(RTRIM(REPLACE(S2.CLM_DGNS_CD,'.',''))),20)  AS [DiagnosisCode] ,
		--,'ICD-9' AS [DiagnosisCodingSystemName] 
		S.[DiagnosisCodingSystemName]
		,'' AS [DiagnosisDescription]   
		,S.EncounterID 
		,S.EncounterIdentifier
		,NULL
		FROM #CCLF_5_DataDedup S (NOLOCK) 
		INNER JOIN #CCLF_5_DiagCodes S2 ON S2.ID=S.ID
		LEFT JOIN SourceSystemSetting SSS (NOLOCK) ON ISNULL(S.SourceFeed,'Default') = SSS.SettingValue AND SSS.SettingName = 'ResolvingCCLFName'
	
		 
	END

--only process if there are data in processQueue table
IF EXISTS (SELECT 1 FROM dbo.PatientDiagnosisProcessQueue)
    BEGIN

	BEGIN TRY
    --get all codingSystemNema and ID into temp table
		SELECT
		DiagnosisCodingSystemTypeID,
		RefCodingSystemName
		INTO #DiagnosisCodingSystemType
		FROM(
			SELECT
			DCST.DiagnosisCodingSystemTypeID,
			DCST.NAME As RefCodingSystemName
			FROM [DiagnosisCodingSystemType] DCST 
			UNION 
			SELECT 
			DCST.DiagnosisCodingSystemTypeID,
			DCSTS.SynonymName As RefCodingSystemName
			FROM [DiagnosisCodingSystemType] DCST 
			INNER JOIN [DiagnosisCodingSystemTypeSynonyms] DCSTS 
			ON DCST.DiagnosisCodingSystemTypeID = DCSTS.DiagnosisCodingSystemTypeID
			) t 
			
		--loading Diagnosis dimension tables

		--EXEC [dbo].[ETLDiagnosisClassification]
		--EXEC [dbo].[ETLDiagnosisPriorityType]
		--EXEC [dbo].[ETLDiagnosisSeverityType]
		--EXEC [dbo].[ETLDiagnosisStatusType]
		--EXEC [dbo].[ETLDiagnosisType]
		EXEC [dbo].[ETLDiagnosisCode]	
				
		
		--SELECT	  DiagnosisCode,
		--          DiagnosisCodeID
		--INTO #DiagnosisCode 		
		--FROM ( SELECT [DiagnosisCodingSystemTypeId],
		--				LEFT( LTRIM(RTRIM(REPLACE([DiagnosisCode],'.',''))),20)  as DiagnosisCode,
		--				DiagnosisCodeID,
		--				DeleteInd 
		-- , ROW_NUMBER() OVER (partition by [DiagnosisCode] order by DeleteInd, DiagnosisCodeID) as rnum
		--FROM [dbo].[DiagnosisCode]
		

		--) a where rnum=1

		--CREATE CLUSTERED INDEX CIX_#DiagnosisCode_DiagnosisCode ON #DiagnosisCode(DiagnosisCode) ;
		SELECT DISTINCT 
		[DiagnosisCodingSystemTypeId],
		LEFT( LTRIM(RTRIM(REPLACE([DiagnosisCode],'.',''))),20)  as DiagnosisCode,
		 DiagnosisCodeID 
		INTO #DiagnosisCode 		
		FROM [dbo].[DiagnosisCode]
		WHERE DeleteInd=0
		CREATE CLUSTERED INDEX CIX_#DiagnosisCode_DiagnosisCodingSystemTypeId ON #DiagnosisCode(DiagnosisCodingSystemTypeID,DiagnosisCode)




		--load deduped patient diagnosis data into temp table fisrt, data deduped by sourceSystemID, LbpatientId, DiagnosisCodeID, and DiagnosisDateTime			     	  
		SELECT *
		INTO #patientDiagnosisCCLF
		FROM
		(
			SELECT
			LbPatientId AS PatientID
			,[InterfaceSystemId] AS SourceSystemId
			,t2.DiagnosisCodingSystemTypeID
			,t3.DiagnosisCodeID
			,ISNULL([DiagnosisTypeID],0) AS [DiagnosisTypeID]
			,ISNULL([DiagnosisStatusTypeID],0) AS [DiagnosisStatusTypeID]
			,ISNULL([DiagnosisSeverityTypeID],0) AS [DiagnosisSeverityTypeID]
			,[DiagnosisConfidentialityInd]
			,ISNULL([DiagnosisPriorityTypeId],0) AS [DiagnosisPriorityTypeId]
			,ISNULL([DiagnosisClassificationTypeId],0) AS [DiagnosisClassificationTypeId]
			,[DiagnosisDateTime]
			,[DiagnosisOnsetDate]
			,[DiagnosisResolvedDate]
			,[DiagnosisComment]
			,ISNULL([EncounterID],0) AS [EncounterID]
			,[EncounterIdentifier]
			,ISNULL([Clinician],'') AS [Clinician]
			,CASE WHEN(LEN(LTRIM(RTRIM(Clinician))) = 10 AND ISNUMERIC (LTRIM(RTRIM(Clinician))) = 1)
					THEN LTRIM(RTRIM(Clinician))
			 END AS RenderingProviderNPI
			--,[RenderingProviderNPI]
			,DrgCode
			,ROW_NUMBER() OVER(PARTITION BY [InterfaceSystemId],LbPatientID, t3.DiagnosisCodeId, Diagnosisdatetime
					ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
			FROM PatientDiagnosisProcessQueue t1
			INNER JOIN #DiagnosisCodingSystemType t2
			ON LTRIM(RTRIM(t1.DiagnosisCodingSystemName)) = LTRIM(RTRIM(t2.RefCodingSystemName))
			INNER JOIN #DiagnosisCode t3 ON t3.[DiagnosisCode]=t1.[DiagnosisCode]
			AND t2.[DiagnosisCodingSystemTypeId] = t3.[DiagnosisCodingSystemTypeId] 
			--AND LTRIM(RTRIM(REPLACE(t1.[DiagnosisCode],'.',''))) = LTRIM(RTRIM(REPLACE(t3.[DiagnosisCode],'.',''))) -- remove the dot from both source and target data when joining tables
			--AND t3.[DeleteInd] = 0 -- only join with the undeleted records
			) AS tmp
		WHERE rowNbr = 1;

			SET @RecordCountSource=@@ROWCOUNT;

			SELECT @RecordCountBefore=COUNT(1)
			FROM [dbo].PatientDiagnosis;
		CREATE CLUSTERED INDEX CIX_#patientDiagnosis_PatientIDDCID ON #patientDiagnosisCCLF([PatientID],[SourceSystemID],DiagnosisCodeID,DiagnosisDateTime)

		--Load PatientDiagnosis and [dbo].[PatientDiagnosisDiagnosisCode]
		BEGIN TRAN

			MERGE PatientDiagnosis T		 
			USING #patientDiagnosisCCLF S
			ON T.[PatientID] = S.[PatientID]
			AND T.[SourceSystemID] = S.[SourceSystemID]
			AND T.DiagnosisCodeID = S.DiagnosisCodeID
			AND T.DiagnosisDateTime = S.DiagnosisDateTime
			WHEN MATCHED 
			THEN UPDATE SET   T.[DiagnosisTypeID] = S.[DiagnosisTypeID],
							T.[DiagnosisStatusTypeID] = S.[DiagnosisStatusTypeID],
							T.[DiagnosisSeverityTypeID] = S.[DiagnosisSeverityTypeID],
							T.[DiagnosisConfidentialityInd] = S.[DiagnosisConfidentialityInd],
							T.[DiagnosisPriorityTypeId] = S.[DiagnosisPriorityTypeId],
							T.[DiagnosisClassificationTypeId] = S.[DiagnosisClassificationTypeId],
							T.[DiagnosisDateTime] = S.[DiagnosisDateTime],
							T.[DiagnosisOnsetDate] = S.[DiagnosisOnsetDate],
							T.[DiagnosisResolvedDate] = S.[DiagnosisResolvedDate],
							T.[DiagnosisComment] = S.[DiagnosisComment],
							T.[EncounterID] = S.[EncounterID],
							T.[EncounterIdentifier] = S.[EncounterIdentifier],
							T.[Clinician] = S.[Clinician],
							T.[RenderingProviderNPI] = S.[RenderingProviderNPI],
							T.[ActiveInd] = 1,
							T.[DeleteInd] = 0,
							T.[DrgCode]=S.[DrgCode],
							T.[ModifyDateTime] = GETUTCDATE()
			WHEN NOT MATCHED BY TARGET
			THEN INSERT ( PatientID,SourceSystemId,DiagnosisCodeID,[DiagnosisCodingSystemTypeId],[DiagnosisTypeID],[DiagnosisStatusTypeID],[DiagnosisSeverityTypeID],[DiagnosisConfidentialityInd],[DiagnosisPriorityTypeId],[DiagnosisClassificationTypeId],
							[DiagnosisDateTime],[DiagnosisOnsetDate],[DiagnosisResolvedDate],[DiagnosisComment], [EncounterID],EncounterIdentifier,[Clinician],[RenderingProviderNPI],
							[ActiveInd],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId], DrgCode)
				Values(	S.PatientID,S.SourceSystemId,S.DiagnosisCodeID,S.[DiagnosisCodingSystemTypeId], S.[DiagnosisTypeID],S.[DiagnosisStatusTypeID],S.[DiagnosisSeverityTypeID],S.[DiagnosisConfidentialityInd],S.[DiagnosisPriorityTypeId],S.[DiagnosisClassificationTypeId],
							S.[DiagnosisDateTime],S.[DiagnosisOnsetDate],S.[DiagnosisResolvedDate],S.[DiagnosisComment], S.[EncounterID],S.EncounterIdentifier,S.[Clinician],S.[RenderingProviderNPI],1,0,GETUTCDATE(),GETUTCDATE(),1,1,S.DrgCode);

			MERGE [dbo].[PatientDiagnosisDiagnosisCode] T
			USING (SELECT [PatientDiagnosisId], [DiagnosisCodeId], [DiagnosisCodingSystemTypeId] 
		        FROM patientDiagnosis 
				WHERE [DiagnosisCodeId] IS NOT NULL AND [DiagnosisCodingSystemTypeId] IS NOT NULL) S
			ON T.[PatientDiagnosisId] = S.[PatientDiagnosisId]
			WHEN NOT MATCHED BY TARGET
			THEN INSERT ([PatientDiagnosisId],[DiagnosisCodingSystemTypeId] ,[DiagnosisCodeId])
				VALUES(S.[PatientDiagnosisId],S.[DiagnosisCodingSystemTypeId] ,S.[DiagnosisCodeId]);


 -- get total records from [dbo].PatientDiagnosis after merge 
		       		SELECT @RecordCountAfter=COUNT(1)
			         FROM [dbo].PatientDiagnosis;

              -- Calculate records inserted and updated counts
	             SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	             SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;

		-- update maxSourceID
			UPDATE [Maintenance]..[EDWTableLoadTracking]
				SET [maxSourceIdProcessed] = (SELECT COALESCE(MAX(sourceIdCCLF4),@maxEDWIDCCLF4) FROM [dbo].PatientDiagnosisProcessQueue),
					[updateDateTime] = GETUTCDATE()
			WHERE [EDWtableName] = 'PatientDiagnosis' AND [dataSource] ='CCLF4'  AND [EDWName] = DB_NAME()

			UPDATE [Maintenance]..[EDWTableLoadTracking]
				SET [maxSourceIdProcessed] = (SELECT COALESCE(MAX(sourceIdCCLF5),@maxEDWIDCCLF5) FROM [dbo].PatientDiagnosisProcessQueue ),
					[updateDateTime] = GETUTCDATE()
			WHERE [EDWtableName] = 'PatientDiagnosis' AND [dataSource] ='CCLF5'  AND [EDWName] = DB_NAME()

		COMMIT
		
		DROP TABLE #patientDiagnosisCCLF, #DiagnosisCodingSystemType
	END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
  END

END
GO
