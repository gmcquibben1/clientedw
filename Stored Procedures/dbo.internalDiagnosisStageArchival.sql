SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalDiagnosisStageArchival] 
    @LookbackDays INT = 0  ,
	@LookbackSince DateTime = NULL,
	@ClearStage BIT = 0
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

    SELECT @LookbackSince = CASE WHEN @LookbackSince IS NULL THEN GETDATE() ELSE @LookbackSince END 

	MERGE InterfacePatientDiagnosisArchive AS TARGET
	USING (
			SELECT 
			   [InterfacePatientDiagnosisId]
			  ,[InterfacePatientID]
			  ,[InterfaceSystemId]
			  ,[DiagnosisType]
			  ,[DiagnosisStatus]
			  ,[DiagnosisSeverity]
			  ,[DiagnosisConfidentialityInd]
			  ,[DiagnosisPriority]
			  ,[DiagnosisClassification]
			  ,[DiagnosisDateTime]
			  ,[DiagnosisOnsetDate]
			  ,[DiagnosisResolvedDate]
			  ,[Clinician]
			  ,[DiagnosisComment]
			  ,[CreateDateTime]
		    FROM [dbo].vwPortal_InterfacePatientDiagnosis (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			([InterfacePatientDiagnosisId],[InterfacePatientID],[InterfaceSystemId],[DiagnosisType],[DiagnosisStatus]
			,[DiagnosisSeverity],[DiagnosisConfidentialityInd],[DiagnosisPriority],[DiagnosisClassification],[DiagnosisDateTime]
			,[DiagnosisOnsetDate],[DiagnosisResolvedDate],[Clinician],[DiagnosisComment],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientDiagnosisId] = SOURCE.[InterfacePatientDiagnosisId] 
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientDiagnosisId]	=Source.[InterfacePatientDiagnosisId],
			[InterfacePatientID]			=Source.[InterfacePatientID],
			[InterfaceSystemId]				=Source.[InterfaceSystemId],
			[DiagnosisType]					=Source.[DiagnosisType],
			[DiagnosisStatus]				=Source.[DiagnosisStatus],
			[DiagnosisSeverity]				=Source.[DiagnosisSeverity],
			[DiagnosisConfidentialityInd]	=Source.[DiagnosisConfidentialityInd],
			[DiagnosisPriority]				=Source.[DiagnosisPriority],
			[DiagnosisClassification]		=Source.[DiagnosisClassification],
			[DiagnosisDateTime]				=Source.[DiagnosisDateTime],
			[DiagnosisOnsetDate]			=Source.[DiagnosisOnsetDate],
			[DiagnosisResolvedDate]			=Source.[DiagnosisResolvedDate],
			[Clinician]						=Source.[Clinician],
			[DiagnosisComment]				=Source.[DiagnosisComment],
			[CreateDateTime]				=Source.[CreateDateTime]
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientDiagnosisId],[InterfacePatientID],[InterfaceSystemId],[DiagnosisType],[DiagnosisStatus]
			,[DiagnosisSeverity],[DiagnosisConfidentialityInd],[DiagnosisPriority],[DiagnosisClassification],[DiagnosisDateTime]
			,[DiagnosisOnsetDate],[DiagnosisResolvedDate],[Clinician],[DiagnosisComment],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientDiagnosisId],Source.[InterfacePatientID],Source.[InterfaceSystemId],Source.[DiagnosisType],Source.[DiagnosisStatus]
			,Source.[DiagnosisSeverity],Source.[DiagnosisConfidentialityInd],Source.[DiagnosisPriority],Source.[DiagnosisClassification],Source.[DiagnosisDateTime]
			,Source.[DiagnosisOnsetDate],Source.[DiagnosisResolvedDate],Source.[Clinician],Source.[DiagnosisComment],Source.[CreateDateTime]) ;

	MERGE InterfacePatientDiagnosisDiagnosisCodeArchive AS TARGET
	USING (
			SELECT 
			  [InterfacePatientDiagnosisDiagnosisCodeId]
			  ,[InterfacePatientDiagnosisId]
			  ,[DiagnosisCode]
			  ,[DiagnosisCodingSystemName]
			  ,[DiagnosisDescription]
			  ,[CreateDateTime]
			FROM [dbo].[vwPortal_InterfacePatientDiagnosisDiagnosisCode] (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			([InterfacePatientDiagnosisDiagnosisCodeId],[InterfacePatientDiagnosisId],[DiagnosisCode],[DiagnosisCodingSystemName],[DiagnosisDescription],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientDiagnosisDiagnosisCodeId] = SOURCE.[InterfacePatientDiagnosisDiagnosisCodeId] 
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientDiagnosisDiagnosisCodeId]	=Source.[InterfacePatientDiagnosisDiagnosisCodeId],
			[InterfacePatientDiagnosisId]				=Source.[InterfacePatientDiagnosisId],
			[DiagnosisCode]								=Source.[DiagnosisCode],
			[DiagnosisCodingSystemName]					=Source.[DiagnosisCodingSystemName],
			[DiagnosisDescription]						=Source.[DiagnosisDescription],
			[CreateDateTime]							=Source.[CreateDateTime]
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientDiagnosisDiagnosisCodeId],[InterfacePatientDiagnosisId],[DiagnosisCode],[DiagnosisCodingSystemName],[DiagnosisDescription],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientDiagnosisDiagnosisCodeId],Source.[InterfacePatientDiagnosisId],Source.[DiagnosisCode],Source.[DiagnosisCodingSystemName],Source.[DiagnosisDescription],Source.[CreateDateTime]) ;

	IF @ClearStage = 1
	BEGIN
		DELETE vwPortal_InterfacePatientDiagnosis FROM vwPortal_InterfacePatientDiagnosis A
		INNER JOIN InterfacePatientDiagnosisArchive B ON A.[InterfacePatientDiagnosisId] = B.[InterfacePatientDiagnosisId] 
		WHERE A.[CreateDateTime] >= GetDate() - @LookbackDays OR A.[CreateDateTime] > @LookbackSince

		DELETE vwPortal_InterfacePatientDiagnosisDiagnosisCode FROM vwPortal_InterfacePatientDiagnosisDiagnosisCode A
		INNER JOIN InterfacePatientDiagnosisDiagnosisCodeArchive B ON A.[InterfacePatientDiagnosisDiagnosisCodeId] = B.[InterfacePatientDiagnosisDiagnosisCodeId] 
		WHERE A.[CreateDateTime] >= GetDate() - @LookbackDays OR A.[CreateDateTime] > @LookbackSince
	END
		


GO
