SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[ETLMemberExtractInsertFromStage]

AS
BEGIN

	SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'MemberExtract';
	DECLARE @dataSource VARCHAR(20) = 'PatientIdsRegistryStage';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLMemberExtractInsertFromStage';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @COMMENT VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @LBUSERID            INT=1
	DECLARE @CurrentDateTime     DATETIME=GETUTCDATE()
	DECLARE @DeleteInd           BIT=0


	BEGIN TRY
				INSERT INTO [dbo].[MemberExtract] (
					 [lastName] ,[midInit],[firstName],[sex] ,[birthDate] ,[medicareNo]
					 ,mailAddrLine1,mailAddrLine2,mailCity,mailState,mailZip
					 ,homeAddrLine1,homeAddrLine2,homeCity,homeState,[homeZip]
					 ,homePhone,[family_id],[SOURCE]   ,[SourceFeed]
					 , SourceSystemId 
					 , PatientOtherIdentifier)
				SELECT DISTINCT
					 LTRIM(RTRIM(PIRS.[LastName])),
					 LTRIM(RTRIM(PIRS.[MiddleName])),
					 LTRIM(RTRIM(PIRS.[FirstName])),
					 ISNULL(PIRS.[Gender], 3),  -- added for NULL default to unknown Gender
					 PIRS.[DOBDateTime],
					 LEFT(PIRS.[DepositingId],50),
					 LEFT(PIRS.mailAddrLine1,50),PIRS.mailAddrLine2,PIRS.mailCity,PIRS.mailState,PIRS.mailZip,
					 LEFT(PIRS.homeAddrLine1,50),PIRS.homeAddrLine2,PIRS.homeCity,PIRS.homeState,PIRS.[homeZip],
					 PIRS.homephone,NEWID(),PIRS.DepositingIdSourceType,PIRS.DepositingIdSourceFeed
					 ,PIRS.SourceSystemId
					 ,PIRS.PatientOtherIdentifier
				FROM ( SELECT * FROM PatientIdsRegistryStage P WHERE  P.DepositingId IS NOT NULL)  PIRS
				LEFT JOIN [MemberExtract] ME 
				-- added SourceSystemId check.
				ON PIRS.DepositingId = ME.medicareNo  AND PIRS.SourceSystemId = ME.SourceSystemId
				WHERE  Me.medicareNo IS NULL 
				 SET @InsertedRecord= @@ROWCOUNT;

				TRUNCATE TABLE PatientIdsRegistryStage

		
		   
							-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
	    
		END TRY
		BEGIN CATCH
				   --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
					SET  @EndTime=GETUTCDATE();
					SET  @ErrorNumber =ERROR_NUMBER();
					SET  @ErrorState =ERROR_STATE();
					SET  @ErrorSeverity=ERROR_SEVERITY();
					SET  @ErrorLine=ERROR_LINE();
					SET  @COMMENT =LEFT(ERROR_MESSAGE(),400);
					EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
				
		END CATCH

	
	
END






GO
