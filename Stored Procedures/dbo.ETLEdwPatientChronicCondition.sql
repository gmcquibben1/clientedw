SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwPatientChronicCondition] 
AS
BEGIN
SET NOCOUNT ON

-- TRUNCATE AND RELOAD 
TRUNCATE TABLE PatientChronicCondition

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientChronicConditionId INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientChronicConditionId,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientChronicConditionId,PatientID
FROM 
(	
MERGE [DBO].PatientChronicCondition AS target
USING (	
		SELECT 
			PCCPQ.PatientID As OrigPatientId, ISNULL(SS.SwitchInId,PCCPQ.PatientID) as PatientID ,[DiagnosisCodeId],	[ProviderId],
			[DisplayOrder] ,[DeleteInd] ,PCCPQ.[CreateDateTime] ,	PCCPQ.[ModifyDateTime] ,	[CreateLBUserId] ,	[ModifyLBUserId] 
		FROM [PatientChronicConditionProcessQueue] PCCPQ 
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PCCPQ.PatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
	  ) AS source 
	  ([OrigPatientId],[PatientId] ,[DiagnosisCodeId],	[ProviderId],
	   [DisplayOrder] ,[DeleteInd] ,[CreateDateTime] ,	[ModifyDateTime] ,	[CreateLBUserId] ,	[ModifyLBUserId])
ON ( target.PatientID = source.PatientID AND ISNULL(target.[DiagnosisCodeId],0) = ISNULL(source.[DiagnosisCodeId],0) )
--WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
--	  --[PatientID]= source.[PatientID] 
WHEN NOT MATCHED THEN
INSERT ([PatientID],[DiagnosisCodeId],[ProviderId],[DisplayOrder] ,[DeleteInd] ,[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId] )
VALUES (source.[PatientID],source.[DiagnosisCodeId],source.[ProviderId],source.[DisplayOrder],source.[DeleteInd],source.[CreateDateTime],source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientChronicConditionId,INSERTED.PatientChronicConditionId) PatientChronicConditionId, source.PatientID
) x ;

IF OBJECT_ID('PatientChronicCondition_PICT') IS NOT NULL
BEGIN
	TRUNCATE TABLE PatientChronicCondition_PICT
	INSERT INTO PatientChronicCondition_PICT(PatientChronicConditionId,PatientId,SwitchDirectionId)
	SELECT RT.PatientChronicConditionId, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

END

GO
