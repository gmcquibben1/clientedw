SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalGetExternalNPIData]
AS
BEGIN


SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'NPI_Lookup';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalGetExternalNPIData';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();





BEGIN TRY 


/*TRUNCATE NPI LOOKUP TABLE*/  --texoma prev: 25362
IF OBJECT_ID('NPI_Lookup') IS NOT NULL
TRUNCATE TABLE NPI_Lookup


/*GATHER ALL THE NPI'S FROM CLAIM TABLES, WHO HAS NPI FIELDS*/
INSERT INTO NPI_Lookup (NPI)
SELECT DISTINCT LTRIM(RTRIM([FAC_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_1_PartA_Header] WHERE [FAC_PRVDR_NPI_NUM] NOT IN ('~         ', '')
UNION
SELECT DISTINCT LTRIM(RTRIM([OPRTG_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_1_PartA_Header] WHERE [OPRTG_PRVDR_NPI_NUM] NOT IN ('~         ', '')
UNION
SELECT DISTINCT LTRIM(RTRIM([ATNDG_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_1_PartA_Header] WHERE [ATNDG_PRVDR_NPI_NUM] NOT IN ('~         ', '')
UNION
SELECT DISTINCT LTRIM(RTRIM([OTHR_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_1_PartA_Header] WHERE [OTHR_PRVDR_NPI_NUM] NOT IN ('~         ', '')
UNION
SELECT DISTINCT LTRIM(RTRIM([RNDRG_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_5_PartB_Physicians] WHERE [RNDRG_PRVDR_NPI_NUM] NOT IN ('~         ', '')
UNION
SELECT DISTINCT LTRIM(RTRIM([PAYTO_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_6_PartB_DME] WHERE [PAYTO_PRVDR_NPI_NUM] NOT IN ('~         ', '')
UNION
SELECT DISTINCT LTRIM(RTRIM([ORDRG_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_6_PartB_DME] WHERE [ORDRG_PRVDR_NPI_NUM] NOT IN ('~         ', '')
UNION
SELECT DISTINCT LTRIM(RTRIM([ProviderNPI])) AS NPI FROM OrgHierarchy WHERE ([ProviderNPI] IS NOT NULL OR [ProviderNPI] <> '')
UNION 
SELECT DISTINCT LTRIM(RTRIM(PerformedByProviderID)) FROM PatientProcedure WHERE ISNUMERIC(PerformedByProviderID) = 1 AND LEN(PerformedByProviderID) = 10
UNION 
SELECT DISTINCT LTRIM(RTRIM(PerformedByClinician)) FROM PatientProcedure WHERE ISNUMERIC(PerformedByClinician) = 1 AND LEN(PerformedByClinician) = 10
UNION 
SELECT DISTINCT LTRIM(RTRIM(OrderedByProviderID)) FROM PatientProcedure WHERE ISNUMERIC(OrderedByProviderID) = 1 AND LEN(OrderedByProviderID) = 10
UNION 
SELECT DISTINCT LTRIM(RTRIM(OrderedByClinician)) FROM PatientProcedure WHERE ISNUMERIC(OrderedByClinician) = 1 AND LEN(OrderedByClinician) = 10
UNION 
SELECT DISTINCT LTRIM(RTRIM(RenderingProviderNPI)) FROM PatientProcedure WHERE ISNUMERIC(RenderingProviderNPI) = 1 AND LEN(RenderingProviderNPI) = 10
UNION 
SELECT DISTINCT LTRIM(RTRIM(IndividualNPI)) FROM MemberRosterArchives WHERE ISNUMERIC(IndividualNPI) = 1 AND LEN(IndividualNPI) = 10

/* ADDED EXTRA CHECKS TO MAKE SURE IT NOT TAKING ', ' AS FAR AS NULL VALUES WE ALWAYS NNA GET'EM AS WE ARE JOINING THE NPI_LOOKUP TO NPI FULL DATA. IH-5/11/15*/
UPDATE NL
SET [NAME] = (
      CASE
			WHEN [Entity Type Code] = 1 
			THEN LTRIM(RTRIM([Provider Last Name (Legal Name)])) + ', ' + LTRIM(RTRIM([Provider First Name]))
      ELSE
        CASE
  			WHEN ([Provider Other Organization Name] <> '' AND [Provider Other Organization Name] IS NOT NULL)
        THEN LTRIM(RTRIM([Provider Other Organization Name]))
        ELSE LTRIM(RTRIM([Provider Organization Name (Legal Business Name)]))
		    END
      END) 
FROM [dbo].[NPI_Lookup] NL
JOIN [NPIreference].[dbo].[npidata_FULL] NF
ON NF.NPI = NL.NPI
WHERE ([Provider Organization Name (Legal Business Name)] <> '' OR [Provider Last Name (Legal Name)] <> '')

/*UPDATE ACO FIELD*/
UPDATE N
SET N.ACO = 1
FROM NPI_Lookup N
JOIN vwPortal_ProviderActive PP 
		ON N.NPI = PP.NationalProviderIdentifier

INSERT INTO NPI_Lookup (NPI, Name, ACO)
SELECT DISTINCT(LTRIM(RTRIM(PE.ProviderNPI))), LTRIM(RTRIM(PE.ProviderLastName)) + ', ' + ISNULL(LTRIM(RTRIM(providerfirstname)),''), 1 
FROM ProviderExtract PE
LEFT JOIN [NPI_Lookup] NPI 
ON PE.ProviderNPI = NPI.NPI
WHERE NPI.NPI IS NULL 


SELECT NPI INTO #NPI FROM
(
SELECT DISTINCT LTRIM(RTRIM([FAC_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_1_PartA_Header] PE
LEFT JOIN [NPI_Lookup] NPI ON PE.[FAC_PRVDR_NPI_NUM] = NPI.NPI AND [FAC_PRVDR_NPI_NUM] <> '~         '
WHERE NPI.NPI IS NULL AND [FAC_PRVDR_NPI_NUM] <> '~' 
UNION
SELECT DISTINCT LTRIM(RTRIM([OPRTG_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_1_PartA_Header]  PE
LEFT JOIN [NPI_Lookup] NPI ON PE.[OPRTG_PRVDR_NPI_NUM] = NPI.NPI AND [OPRTG_PRVDR_NPI_NUM] <> '~         '
WHERE NPI.NPI IS NULL AND [OPRTG_PRVDR_NPI_NUM] <> '~' 
UNION
SELECT DISTINCT LTRIM(RTRIM([ATNDG_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_1_PartA_Header] PE
LEFT JOIN [NPI_Lookup] NPI ON PE.[ATNDG_PRVDR_NPI_NUM] = NPI.NPI AND [ATNDG_PRVDR_NPI_NUM] <> '~         '
WHERE NPI.NPI IS NULL AND [ATNDG_PRVDR_NPI_NUM] <> '~' 
UNION
SELECT DISTINCT LTRIM(RTRIM([OTHR_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_1_PartA_Header]  PE
LEFT JOIN [NPI_Lookup] NPI ON PE.[OTHR_PRVDR_NPI_NUM] = NPI.NPI AND [OTHR_PRVDR_NPI_NUM] <> '~         '
WHERE NPI.NPI IS NULL AND [OTHR_PRVDR_NPI_NUM] <> '~'
UNION
SELECT DISTINCT LTRIM(RTRIM([PAYTO_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_6_PartB_DME] PE
LEFT JOIN [NPI_Lookup] NPI ON PE.[PAYTO_PRVDR_NPI_NUM] = NPI.NPI AND [PAYTO_PRVDR_NPI_NUM] <> '~         '
WHERE NPI.NPI IS NULL AND [PAYTO_PRVDR_NPI_NUM] <> '~'
UNION
SELECT DISTINCT LTRIM(RTRIM([ORDRG_PRVDR_NPI_NUM])) AS NPI FROM [CCLF_6_PartB_DME] PE
LEFT JOIN [NPI_Lookup] NPI ON PE.[ORDRG_PRVDR_NPI_NUM] = NPI.NPI AND [ORDRG_PRVDR_NPI_NUM] <> '~         '
WHERE NPI.NPI IS NULL AND [ORDRG_PRVDR_NPI_NUM] <> '~'
) X

UPDATE NPI_Lookup SET City = LTRIM(RTRIM(NPI.[Provider Business Mailing Address City Name]))
, Zip = LEFT(LTRIM(RTRIM(NPI.[Provider Business Mailing Address Postal Code])),5)
, Taxonomy = 
case 
  when NPI.[Healthcare Provider Primary Taxonomy Switch_1] = 'Y' then NPI.[Healthcare Provider Taxonomy Code_1]
  when NPI.[Healthcare Provider Primary Taxonomy Switch_2] = 'y' then NPI.[Healthcare Provider Taxonomy Code_2]
  when NPI.[Healthcare Provider Primary Taxonomy Switch_3] = 'y' then NPI.[Healthcare Provider Taxonomy Code_3]
  when NPI.[Healthcare Provider Primary Taxonomy Switch_4] = 'y' then NPI.[Healthcare Provider Taxonomy Code_4]
  when NPI.[Healthcare Provider Primary Taxonomy Switch_5] = 'y' then NPI.[Healthcare Provider Taxonomy Code_5]
  when NPI.[Healthcare Provider Primary Taxonomy Switch_6] = 'y' then NPI.[Healthcare Provider Taxonomy Code_6]
  when NPI.[Healthcare Provider Primary Taxonomy Switch_7] = 'y' then NPI.[Healthcare Provider Taxonomy Code_7]
  when NPI.[Healthcare Provider Primary Taxonomy Switch_8] = 'y' then NPI.[Healthcare Provider Taxonomy Code_8]
  when NPI.[Healthcare Provider Primary Taxonomy Switch_9] = 'y' then NPI.[Healthcare Provider Taxonomy Code_9]
  else NPI.[Healthcare Provider Taxonomy Code_1]
end
FROM NPI_Lookup A
JOIN NPIreference..npidata_FULL NPI ON A.NPI = NPI.NPI

update NPI_Lookup set Specialty = TX.SPECIALTY
  FROM NPI_Lookup NPI 
  JOIN EdwReferenceData..Specialty_Taxonomy_AMA TX ON NPI.Taxonomy = TX.Taxonomy

INSERT INTO npi_lookup (NPI, Name, ACO)
SELECT DISTINCT NPI, 'Unattributed', 1
FROM #NPI


DROP TABLE #NPI


--DEDUP BLOCK 
;WITH CTE AS (
            SELECT ROW_NUMBER() OVER (PARTITION BY NPI ORDER BY NAME DESC) DEDUP
            FROM [dbo].[NPI_Lookup]
            )
DELETE FROM CTE WHERE DEDUP > 1


--update specialties from NPI Lookup table
UPDATE CCLF_5_PartB_Physicians SET 
CLM_PRVDR_SPCLTY_CD = NPI.Specialty 
FROM CCLF_5_PartB_Physicians B 
JOIN NPI_Lookup NPI ON B.RNDRG_PRVDR_NPI_NUM = NPI.NPI AND( B.CLM_PRVDR_SPCLTY_CD IS NULL or B.CLM_PRVDR_SPCLTY_CD = '')
where NPI.Specialty IS NOT NULL


--CHECKING IF TABLE HAS DUPLICATE DATA
--SELECT COUNT(NAME), NPI FROM NPI_Lookup GROUP BY NPI HAVING COUNT(NAME) > 1



	-----Log Information
		
	 
		SELECT @InsertedRecord=count(1)
		FROM [NPI_Lookup] ;

          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
