SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalMedicationStageArchival] 
    @LookbackDays INT = 0  ,
	@LookbackSince DateTime = NULL,
	@ClearStage BIT = 0
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

    SELECT @LookbackSince = CASE WHEN @LookbackSince IS NULL THEN GETDATE() ELSE @LookbackSince END 

	MERGE [InterfacePatientMedicationArchive] AS TARGET
	USING (
			SELECT 
			  [InterfacePatientMedicationId]
			  ,[InterfacePatientID]
			  ,[InterfaceSystemId]
			  ,[MedicationCode]
			  ,[MedicationCodeName]
			  ,[MedicationName]
			  ,[MedicationDose]
			  ,[MedicationForm]
			  ,[MedicationRoute]
			  ,[MedicationStartDate]
			  ,[MedicationEndDate]
			  ,[SIGCode]
			  ,[MedicationInstructions]
			  ,[MedicationQuantity]
			  ,[MedicationRefills]
			  ,[SampleInd]
			  ,[GenericAllowedInd]
			  ,[PrescribedByNPI]
			  ,[MedicationComment]
			  ,[CreateDateTime]
		    FROM [dbo].vwPortal_InterfacePatientMedication (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			([InterfacePatientMedicationId],[InterfacePatientID],[InterfaceSystemId],[MedicationCode],[MedicationCodeName],[MedicationName],[MedicationDose]
			,[MedicationForm],[MedicationRoute],[MedicationStartDate],[MedicationEndDate],[SIGCode],[MedicationInstructions],[MedicationQuantity],[MedicationRefills]
			,[SampleInd],[GenericAllowedInd],[PrescribedByNPI],[MedicationComment],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientMedicationId] = SOURCE.[InterfacePatientMedicationId] 
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientMedicationId]	=Source.[InterfacePatientMedicationId],
			[InterfacePatientID]			=Source.[InterfacePatientID],
			[InterfaceSystemId]				=Source.[InterfaceSystemId],
			[MedicationCode]				=Source.[MedicationCode],
			[MedicationCodeName]			=Source.[MedicationCodeName],
			[MedicationName]				=Source.[MedicationName],
			[MedicationDose]				=Source.[MedicationDose],
			[MedicationForm]				=Source.[MedicationForm],
			[MedicationRoute]				=Source.[MedicationRoute],
			[MedicationStartDate]			=Source.[MedicationStartDate],
			[MedicationEndDate]				=Source.[MedicationEndDate],
			[SIGCode]						=Source.[SIGCode],
			[MedicationInstructions]		=Source.[MedicationInstructions],
			[MedicationQuantity]			=Source.[MedicationQuantity],
			[MedicationRefills]				=Source.[MedicationRefills],
			[SampleInd]						=Source.[SampleInd],
			[GenericAllowedInd]				=Source.[GenericAllowedInd],
			[PrescribedByNPI]				=Source.[PrescribedByNPI],
			[MedicationComment]				=Source.[MedicationComment],
			[CreateDateTime]				=Source.[CreateDateTime]
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientMedicationId],[InterfacePatientID],[InterfaceSystemId],[MedicationCode],[MedicationCodeName],[MedicationName],[MedicationDose]
			,[MedicationForm],[MedicationRoute],[MedicationStartDate],[MedicationEndDate],[SIGCode],[MedicationInstructions],[MedicationQuantity],[MedicationRefills]
			,[SampleInd],[GenericAllowedInd],[PrescribedByNPI],[MedicationComment],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientMedicationId],Source.[InterfacePatientID],Source.[InterfaceSystemId],Source.[MedicationCode],Source.[MedicationCodeName],Source.[MedicationName],Source.[MedicationDose]
			,Source.[MedicationForm],Source.[MedicationRoute],Source.[MedicationStartDate],Source.[MedicationEndDate],Source.[SIGCode],Source.[MedicationInstructions],Source.[MedicationQuantity],Source.[MedicationRefills]
			,Source.[SampleInd],Source.[GenericAllowedInd],Source.[PrescribedByNPI],Source.[MedicationComment],Source.[CreateDateTime]) ;

	IF @ClearStage = 1
	BEGIN
		DELETE vwPortal_InterfacePatientMedication FROM vwPortal_InterfacePatientMedication  A
		INNER JOIN [InterfacePatientMedicationArchive] B ON A.[InterfacePatientMedicationId] = B.[InterfacePatientMedicationId] 
		WHERE A.[CreateDateTime] >= GetDate() - @LookbackDays OR A.[CreateDateTime] > @LookbackSince
	END
		


GO
