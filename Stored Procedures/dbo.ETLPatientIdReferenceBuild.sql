SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ETLPatientIdReferenceBuild]

AS
BEGIN
-- ================================================================
--
--	CREATED BY: 	Sam Guo
--	CREATED ON:		2016-09-07
--	INITIAL VER:	2.1.1
--	MODULE:			ETL
--	DESCRIPTION:	Build PatientIdReference Table
----
--	PARAMETERS:		
--		 None

--MODIFICATIONS
-- ===============================================================
/*
-- ================================================================
     Version       Date				Author   Change
     -------       ----------		------   ------
     2.1.1         2016-10-04		SG      Initial draft.
              
     Version       Date				Author   Change
     -------       ----------		------   ------
     2.2.1       2017-01-16			SG      LBETL-472 Fixed Deleted PID issue. 
	 															Added joins.
     2.3.1       2017-04-13         YL      LBETL-1416 Eliminate Kill and fill in PatientIdReference table

*/
	SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientIdReference';
	DECLARE @dataSource VARCHAR(20) = 'MemberExtract';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientIdReferenceBuild';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @COMMENT VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @LBUSERID            INT=1
	DECLARE @CurrentDateTime     DATETIME=GETUTCDATE()
	DECLARE @DeleteInd           BIT=0
	
	DECLARE @MergeChanges TABLE(Maction VARCHAR(30));



	BEGIN TRY
			-----------------------------------------------------------
			-- Rebuild PatientIdReference table
			-----------------------------------------------------------
			--truncate TABLE [dbo].[PatientIdReference]

			--INSERT INTO [dbo].[PatientIdReference]
			--		   ([LbPatientId]
			--		   ,[ExternalId]
			--		   ,[IdTypeDesc]
			--		   ,[SourceSystemId]
			--		   ,[PrimaryId]
			--		   ,[DeleteInd])
			
			SELECT DISTINCT [LbPatientId], LTRIM(RTRIM(medicareNo)) [ExternalId], 
				CONVERT(Varchar(50),CASE WHEN SourceFeed = 'Interface' THEN 'InterfacePatientIdentifier' 
					ELSE 'BENE_HIC_NUM' end) as IdTypeDesc,
				[SourceSystemId]
				into #PatientIdReference
					FROM [dbo].[MemberExtract]
					INNER JOIN dbo.vwportal_Patient P
					ON lbpatientId = P.patientId
					--WHERE medicareNo IS NOT NULL AND LbPatientId IS NOT NULL 
					WHERE ISNULl(medicareNo,'')<>''  AND ISNULL(LbPatientId,'')<>''  
					AND P.deleteInd = 0 ;

          
		  INSERT INTO #PatientIdReference ([LbPatientId],[ExternalId],[IdTypeDesc],[SourceSystemId])
		  		SELECT DISTINCT [LbPatientId], LTRIM(RTRIM(PatientOtherIdentifier)) [ExternalId], 
				'InterfacePatientOtherIdentifier'  as IdTypeDesc,
				[SourceSystemId]
				FROM [dbo].[MemberExtract]
					INNER JOIN dbo.vwportal_Patient P
					ON lbpatientId = P.patientId
					--WHERE PatientOtherIdentifier IS NOT NULL AND LbPatientId IS NOT NULL 
					WHERE SourceFeed = 'Interface' AND ISNULl(PatientOtherIdentifier,'')<>''  AND ISNULL(LbPatientId,'')<>''  
					AND P.deleteInd = 0 ;
		  
		  
		  
		    CREATE CLUSTERED INDEX CIDX_IDRef_Com on #PatientIdReference ([LbPatientId]
					   ,[ExternalId]
					   ,[IdTypeDesc]
					   ,[SourceSystemId]);
           


			MERGE PatientIdReference as T
			USING #PatientIdReference as S
			ON T.[LbPatientId]=S.[LbPatientId]
					   AND T.[ExternalId]=S.[ExternalId]
					   AND T.[IdTypeDesc]=S.[IdTypeDesc]
					   AND T.[SourceSystemId]=S.[SourceSystemId]
			WHEN NOT MATCHED BY Target
			   THEN INSERT([LbPatientId]
           ,[ExternalId]
           ,[IdTypeDesc]
           ,[SourceSystemId]
           ,[PrimaryId]
           ,[DeleteInd])
				VALUES (S.[LbPatientId]
           ,S.[ExternalId]
           ,S.[IdTypeDesc]
           ,S.[SourceSystemId]
           ,0
           ,0)
		   WHEN NOT MATCHED BY SOURCE 
		   THEN DELETE
		   OUTPUT $action INTO @MergeChanges
		   ;
		  
		   DROP TABLE #PatientIdReference;


		   
		   -- get records inserted 
	           SET @InsertedRecord= ISNULL((SELECT count(1) from @MergeChanges WHERE Maction='INSERT'),0);
			 
	         
							
	-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
	
	    
		           

			-- need to add index ....Youping 5/11/2016

			IF EXISTS(SELECT NAME FROM sys.indexes WHERE NAME = 'PatientIdReference_IdTypeDescDeleteIndCovering')
				BEGIN
						ALTER INDEX [PatientIdReference_IdTypeDescDeleteIndCovering] ON [PatientIdReference] REBUILD; 
				END
           ELSE
			   BEGIN
				CREATE NONCLUSTERED INDEX [PatientIdReference_IdTypeDescDeleteIndCovering] ON [dbo].[PatientIdReference]
					([SourceSystemId],[ExternalId],	[IdTypeDesc],	[DeleteInd] )  INCLUDE ([LbPatientId]) ;
			   END


					-- TRANSFER TO PORTAL 
					DECLARE @DynSql VARCHAR(8000),  @Portaldb VARCHAR(100)
					SET @Portaldb = REPLACE(@EDWName,'Edw','LbPortal')

					SET @DynSql = 'TRUNCATE TABLE  ' + @Portaldb + '.Dbo.PatientIdReference'
					EXEC (@DynSql)



					SET @DynSql = 'INSERT INTO ' + @Portaldb + '.Dbo.PatientIdReference' + 
						   '(LbPatientId, ExternalId, IdTypeDesc, SourceSystemId,PrimaryId,DeleteInd)
						 SELECT LbPatientId, ExternalId, IdTypeDesc, SourceSystemId,PrimaryId,DeleteInd FROM PatientIdReference'
					EXEC (@DynSql)



		END TRY
		BEGIN CATCH
				   --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
					SET  @EndTime=GETUTCDATE();
					SET  @ErrorNumber =ERROR_NUMBER();
					SET  @ErrorState =ERROR_STATE();
					SET  @ErrorSeverity=ERROR_SEVERITY();
					SET  @ErrorLine=ERROR_LINE();
					SET  @COMMENT =LEFT(ERROR_MESSAGE(),400);
					EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
				
		END CATCH

	
	
END
GO
