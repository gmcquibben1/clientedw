SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ETLPatientProcedureInterface]
	@fullLoadFlag bit = 0, 
	@BatchSize INT = 10000, 
	@InterfacePatientId VARCHAR(50) = NULL,
	@overrideMinIndex INT = NULL
	

AS
BEGIN	


	--DECLARE @fullLoadFlag bit = 1, 
	--@BatchSize INT = 10000, 
	--@InterfacePatientId VARCHAR(50) = NULL,
	--@overrideMinIndex INT = NULL
	

/*===============================================================
	CREATED BY: 	Lan Ma
	CREATED ON:		2016-03-23
	INITIAL VER:	2.0.1
	MODULE:			ETL Interface Process	
	DESCRIPTION:	Load Interface Patient Procedure Data into EDW
	PARAMETERS:		
		 @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
		 @fullLoadFlag =0 Merge date to the existing table
		 @BatchSize  Number of patients to do in each batch
		 @InterfacePatientId    Id of the patient identifier if we want to run this for a specific patietn
		 @overrideMinIndex		Minimum id of the InterfacepatientProcedure record to start processing from. Skips the
								Value stored in the [Maintenance].[dbo].[EDWTableLoadTracking] table


	RETURN VALUE(s)/OUTPUT:	
		Error and logging information will be written to the [EDWTableLoadTrackingInsert] table

	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
				2016-03-23		LM						Initial version
				2016-04-07 		YL						Add the procedure running status to [dbo].[EdwProcedureRunLog]
				2016-06-03		YL						Add Revenue
				2016-06-06		YL						FIX: timeout issue by breaking to two process for dedup process
	2.1.2		2016-12-29		CJL			LBETL-315	Greenway Procedure Updates with NULL place of service
	2.1.2		2017-02-05		CJL			LBETL-315	Implemented batching routines
	2.3.1		2017-03-14		CJL			LBETL-1087	Modify ETLPatientProcedureInterface to use encounter date if procedure date does not exist in InterfacePatientProcedure table
	2.2.1		2017-03-17		CJL			LBETL-1098	Address Place of service roll up issue, removed truncate call
			
=================================================================*/


    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientProcedure';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientProcedureInterface';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @RowCount INT =0;
	DECLARE @NumRows INT = 0;
 	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);



	BEGIN TRY	
			--set @fullLoadFlag =1
	
		--1) Retrieve the last run time id values.  We are going to pull both the date and the last run time id. If the last max id 
		--is null, calculate a best guese based on the 
		--We are changing away from using date values for query purposes as it is more efficient to query by the Id value.  Since
		--the id is a seeded, integer greater id values are implicitly newer. It also allows us to pick up records that come in while the 
		--procedure is running the next time it runs
		IF @fullLoadFlag = 1
		BEGIN
			UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME), maxSourceIdProcessed= 0
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
		END

		 SET @lastDateTime = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	
	
		
		SET  @lastIndex = (SELECT maxSourceIdProcessed
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex= (SELECT MIN([InterfacePatientProcedureId]) FROM  [vwPortal_InterfacePatientProcedure] IPS WITH (NOLOCK) WHERE IPS.CreateDateTime >= @lastDateTime ) 
		END

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex = 0 
		END


		if @overrideMinIndex IS NOT NULL
		BEGIN
			SET @lastIndex = @overrideMinIndex
		END 

		SET @maxIndex = (SELECT MAX([InterfacePatientProcedureId]) FROM  [vwPortal_InterfacePatientProcedure] IPS WITH (NOLOCK));
	
		

		/*
		DROP TABLE #tmpPatientProcedure
		DROP TABLE #ProcedureCodingSystemType
		DROP TABLE #PatientProcedureQuePatientIdentifieBATCH
		Drop table #PatientProcedureQuePatientIdentifier
		DROP TABLE #patientProcedure
		*/


		--2) Get the full list of patient identifiers that we are going to be dealing with
		--We are going to breaking this up into batches along patient id lines.
		SELECT  DISTINCT 
			IP.PatientIdentifier, IP.InterfaceSystemId
		INTO #PatientProcedureQuePatientIdentifier	
		FROM [dbo].[vwPortal_InterfacePatientProcedure] IPS with (nolock)
			INNER JOIN [dbo].[vwPortal_InterfacePatient] IP with (nolock) ON IPS.InterfacePatientId = IP.InterfacePatientId
			INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) ON IP.InterfaceSystemId = ISS.[InterfaceSystemId]
		WHERE ( IPS.[InterfacePatientProcedureId] >= @lastIndex  AND IPS.[InterfacePatientProcedureId] <= @maxIndex )
		 AND ( @InterfacePatientId IS NULL OR (IPS.InterfacePatientId = @InterfacePatientId))
			

					
				
		--- Truncate data
		--IF (@fullLoadFlag=1)
		--BEGIN
		--	Truncate table [dbo].PatientProcedure;
		--END
	
	    SELECT @RecordCountBefore=COUNT(1)	FROM [dbo].PatientProcedure  WITH (NOLOCK);

	
		--3) Loop throught all of the batches to improve memory efficency
		WHILE 1 = 1
		BEGIN	
	


	
				SELECT TOP (@BatchSize) PatientIdentifier, InterfaceSystemId INTO #PatientProcedureQuePatientIdentifieBATCH FROM #PatientProcedureQuePatientIdentifier;
				Create  NONCLUSTERED INDEX IDX_PatientProcedureQuePatientIdentifieBATCH_PatientIdentifier ON #PatientProcedureQuePatientIdentifieBATCH (PatientIdentifier, InterfaceSystemId);
			
    				SET @NUMROWS = ( SELECT COUNT(1) FROM #PatientProcedureQuePatientIdentifieBATCH)

				IF @NUMROWS <> 0 
				BEGIN


			


							---Load new patient Procedure into staging table incrementally based on the CreateDateTime in table interfacePatientProcedure
							---Exclude the records that don't have vaules for ProcedureCodingSystemName, ProcedureCode, ProcedureDateTime

								TRUNCATE TABLE dbo.PatientProcedureProcessQueue 
	
								INSERT INTO PatientProcedureProcessQueue
								(
										[InterfacePatientProcedureId],InterfacePatientID,[InterfaceSystemId], [LbPatientID], 
										 ProcedureFunctionTypeID,[ProcedureCodeModifier1],[ProcedureCodeModifier2]
								,[ProcedureCodeModifier3],[ProcedureCodeModifier4],[ProcedureDateTime]
								,[ProcedureComment],[PerformedByClinician],[OrderedByClinician]
								,IPP_CreateDateTime,InterfacePatientProcedureProcedureCodeId,ProcedureCode
								,ProcedureCodingSystemName,ProcedureDescription,IPPPC_CreateDateTime
								,EncounterID,EncounterIdentifier,TotalCharges,InternalChargeCode,ProcedureResult
								,[RenderingProviderNPI],[PlaceOfService],[RevenueCode]
								)
									SELECT 
									IPP.[InterfacePatientProcedureId],IPP.[InterfacePatientID],IPP.[InterfaceSystemId], idRef.LbPatientId,  0 ProcedureFunctionTypeID
									,IPP.[ProcedureCodeModifier1],IPP.[ProcedureCodeModifier2]	,IPP.[ProcedureCodeModifier3]
									,IPP.[ProcedureCodeModifier4], ISNULL(IPP.[ProcedureDateTime], IPE.EncounterDateTime) AS [ProcedureDateTime], IPP.[ProcedureComment],IPP.[PerformedByClinician]
									,IPP.[OrderedByClinician]	,IPP.[CreateDateTime] IPP_CreateDateTime,IPPPC.InterfacePatientProcedureProcedureCodeId
									,LTRIM(RTRIM(IPPPC.ProcedureCode)) AS ProcedureCode	,LTRIM(RTRIM(IPPPC.ProcedureCodingSystemName)) AS ProcedureCodingSystemName
									,IPPPC.ProcedureDescription,IPPPC.CreateDateTime AS IPPPC_CreateDateTime, NULL as EncounterID, IPP.EncounterIdentifier
									,IPP.TotalCharges,IPP.InternalChargeCode,IPP.ProcedureResult
									,IPP.[RenderingProviderNPI],IPP.[PlaceOfService],IPP.[RevenueCode]
									FROM 
									 [dbo].[vwPortal_InterfacePatientProcedureProcedureCode] IPPPC  WITH (NOLOCK), 
									 [dbo].[vwPortal_InterfacePatient] IP WITH (NOLOCK),
									 [dbo].[PatientIdReference] idRef WITH (NOLOCK),
									 #PatientProcedureQuePatientIdentifieBATCH batch WITH (NOLOCK), 
									 [dbo].[vwPortal_InterfaceSystem] ISS WITH (NOLOCK),
									  [dbo].[vwPortal_InterfacePatientProcedure] IPP WITH (NOLOCK)
									 LEFT OUTER JOIN  [dbo].[vwPortal_InterfacePatientEncounter] IPE WITH (NOLOCK)  ON IPE.EncounterIdentifier = IPP.EncounterIdentifier AND IPE.InterfaceSystemId = IPP.InterfaceSystemId 
								WHERE
								  IPPPC.ProcedureCode IS NOT NULL AND LTRIM(RTRIM(IPPPC.ProcedureCode)) <> ''
								  AND IPPPC.ProcedureCodingSystemName IS NOT NULL AND LTRIM(RTRIM(IPPPC.ProcedureCodingSystemName)) <> ''
								   AND IPP.InterfacePatientProcedureId = IPPPC.InterfacePatientProcedureId
									AND (IPP.[InterfacePatientProcedureId] >= @lastIndex  AND IPP.[InterfacePatientProcedureId] <= @maxIndex) 
									AND  IPP.InterfacePatientId = IP.InterfacePatientId AND  IP.InterfaceSystemId = ISS.[InterfaceSystemId] 
									AND IP.PatientIdentifier = batch.PatientIdentifier AND IP.InterfaceSystemId = batch.InterfaceSystemId
									AND  idRef.SourceSystemId = ISS.SourceSystemId 
										AND idRef.ExternalId = batch.PatientIdentifier 
										AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
										AND idRef.DeleteInd =0;





									--loading ProcedureCode dimension table
									EXEC [dbo].[ETLProcedureCode]
									--get all ProcedureCodingSystemName and ID into temp table
									SELECT
									ProcedureCodingSystemTypeID,
									LTRIM(RTRIM(RefCodingSystemName)) RefCodingSystemName
									INTO #ProcedureCodingSystemType
									FROM(
										  SELECT
										  PCST.ProcedureCodingSystemTypeID,
										  PCST.NAME As RefCodingSystemName
										  FROM ProcedureCodingSystemType PCST 
										  UNION
										  SELECT 
										  PCST.ProcedureCodingSystemTypeID,
										  PCSTS.SynonymName As RefCodingSystemName
										  FROM ProcedureCodingSystemType PCST 
										  INNER JOIN ProcedureCodingSystemTypeSynonyms PCSTS ON PCST.ProcedureCodingSystemTypeID = PCSTS.ProcedureCodingSystemTypeID
										) t

										SELECT
										 ISS.SourceSystemId	, ProcedureFunctionTypeID,[ProcedureCodeModifier1]
										,[ProcedureCodeModifier2],[ProcedureCodeModifier3],[ProcedureCodeModifier4]
										,[ProcedureDateTime],[ProcedureComment]	,[EncounterID]
										,[PerformedByClinician]	,[OrderedByClinician],EncounterIdentifier
										,TotalCharges,InternalChargeCode,ProcedureResult
										,[RenderingProviderNPI],[PlaceOfService],IP.PatientIdentifier, t1.LbPatientID
										,LTRIM(RTRIM(t1.[ProcedureCode])) AS ProcedureCode
										,LTRIM(RTRIM(t1.ProcedureCodingSystemName)) AS ProcedureCodingSystemName
										,t2.ProcedureCodingSystemTypeID
										,t3.ProcedureCodeID
										,t1.InterfacePatientProcedureId
										,RevenueCode
										INTO #tmpPatientProcedure
										FROM PatientProcedureProcessQueue t1
										INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS ON t1.InterfaceSystemId = ISS.[InterfaceSystemId]
										INNER JOIN [dbo].[vwPortal_InterfacePatient] IP ON t1.InterfacePatientId = IP.InterfacePatientId
										INNER JOIN #ProcedureCodingSystemType t2  ON t1.ProcedureCodingSystemName= t2.RefCodingSystemName
										INNER JOIN [dbo].[ProcedureCode] t3	ON t2.[ProcedureCodingSystemTypeId] = t3.[ProcedureCodingSystemTypeId] 	AND t1.[ProcedureCode] = t3.[ProcedureCode]
						
										 ;
          
									  CREATE INDEX IDX_SourceSystemIdPatientIdentifier  ON #tmpPatientProcedure (SourceSystemId, lbPatientId ) INCLUDE (ProcedureCodingSystemName,ProcedureCode,Proceduredatetime);

		  
		    
									 SELECT *
									INTO #patientProcedure
									FROM
									(
										SELECT
										 t1.LbPatientId As PatientId, t1.SourceSystemId, t1.ProcedureCodingSystemTypeID
										,t1.ProcedureCodeID	, ProcedureFunctionTypeID, t1.[ProcedureDateTime],
										pcm1.[ProcedureCodeModifier1], pcm2.[ProcedureCodeModifier2], pcm3.[ProcedureCodeModifier3], pcm4.[ProcedureCodeModifier4]
										, comment.[ProcedureComment], enc.[EncounterID]
										,performby.[PerformedByClinician],  orderby.[OrderedByClinician],
										encId.EncounterIdentifier
										,totalCharges.TotalCharges, chargeCode.InternalChargeCode,
										pResult.ProcedureResult	,renderingNPI.[RenderingProviderNPI],
										serviceLoc.[PlaceOfService],revCode.RevenueCode
										,ROW_NUMBER() OVER(PARTITION BY t1.SourceSystemId, t1.LbPatientId, t1.ProcedureCodeId, ISNULL(t1.Proceduredatetime, '1901-01-01')
												ORDER BY InterfacePatientProcedureId DESC ) AS rowNbr
										FROM #tmpPatientProcedure t1
										--INNER JOIN #ProcedureCodingSystemType t2  ON t1.ProcedureCodingSystemName= t2.RefCodingSystemName
										--INNER JOIN [dbo].[ProcedureCode] t3	ON t2.[ProcedureCodingSystemTypeId] = t3.[ProcedureCodingSystemTypeId] 	AND t1.[ProcedureCode] = t3.[ProcedureCode]
										LEFT JOIN
								(SELECT  LbPatientId, ProcedureCodeModifier1,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),  SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  ProcedureCodeModifier1 IS NOT NULL ) pcm1
								ON pcm1.LbPatientId = t1.LbPatientId AND ISNULL(pcm1.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01') AND t1.ProcedureCodeId = pcm1.ProcedureCodeId 
									AND pcm1.SourceSystemId =t1.SourceSystemId 
						LEFT JOIN
								(SELECT  LbPatientId, ProcedureCodeModifier2,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  ProcedureCodeModifier2 IS NOT NULL ) pcm2
								ON pcm2.LbPatientId = t1.LbPatientId AND ISNULL(pcm2.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')  AND t1.ProcedureCodeId = pcm2.ProcedureCodeId 
									AND pcm2.SourceSystemId =t1.SourceSystemId 
						LEFT JOIN
								(SELECT  LbPatientId, ProcedureCodeModifier3,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  ProcedureCodeModifier3 IS NOT NULL ) pcm3
								ON pcm3.LbPatientId = t1.LbPatientId AND ISNULL(pcm3.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')  AND t1.ProcedureCodeId = pcm3.ProcedureCodeId 
									AND pcm3.SourceSystemId =t1.SourceSystemId 
						LEFT JOIN
								(SELECT  LbPatientId, ProcedureCodeModifier4,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  ProcedureCodeModifier4 IS NOT NULL ) pcm4
								ON pcm4.LbPatientId = t1.LbPatientId AND ISNULL(pcm4.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')   AND t1.ProcedureCodeId = pcm4.ProcedureCodeId 
									AND pcm4.SourceSystemId =t1.SourceSystemId 
						LEFT JOIN
								(SELECT  LbPatientId, ProcedureComment,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  ProcedureComment IS NOT NULL ) comment
								ON comment.LbPatientId = t1.LbPatientId AND ISNULL(comment.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')    AND t1.ProcedureCodeId = comment.ProcedureCodeId 
									AND comment.SourceSystemId =t1.SourceSystemId 
					LEFT JOIN
								(SELECT  LbPatientId, EncounterID,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  EncounterID IS NOT NULL ) enc
								ON enc.LbPatientId = t1.LbPatientId AND ISNULL(enc.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')   AND t1.ProcedureCodeId = enc.ProcedureCodeId 
									AND enc.SourceSystemId =t1.SourceSystemId 
						LEFT JOIN
								(SELECT  LbPatientId, PerformedByClinician,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  PerformedByClinician IS NOT NULL ) performby
								ON performby.LbPatientId = t1.LbPatientId AND ISNULL(performby.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')   AND t1.ProcedureCodeId = performby.ProcedureCodeId 
									AND performby.SourceSystemId =t1.SourceSystemId 
					LEFT JOIN
								(SELECT  LbPatientId, OrderedByClinician,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  OrderedByClinician IS NOT NULL ) orderby
								ON orderby.LbPatientId = t1.LbPatientId AND ISNULL(orderby.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')  AND t1.ProcedureCodeId = orderby.ProcedureCodeId 
									AND orderby.SourceSystemId =t1.SourceSystemId 
						LEFT JOIN
								(SELECT  LbPatientId, EncounterIdentifier,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  EncounterIdentifier IS NOT NULL ) encId
								ON encId.LbPatientId = t1.LbPatientId AND ISNULL(encId.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')  AND t1.ProcedureCodeId = encId.ProcedureCodeId 
									AND encId.SourceSystemId =t1.SourceSystemId 
						LEFT JOIN
								(SELECT  LbPatientId, TotalCharges,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  TotalCharges IS NOT NULL ) totalCharges
								ON totalCharges.LbPatientId = t1.LbPatientId AND ISNULL(totalCharges.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')   AND t1.ProcedureCodeId = totalCharges.ProcedureCodeId 
									AND totalCharges.SourceSystemId =t1.SourceSystemId 
						LEFT JOIN
								(SELECT  LbPatientId, InternalChargeCode,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  InternalChargeCode IS NOT NULL ) chargeCode
								ON chargeCode.LbPatientId = t1.LbPatientId AND ISNULL(chargeCode.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')  AND t1.ProcedureCodeId = chargeCode.ProcedureCodeId 
									AND chargeCode.SourceSystemId =t1.SourceSystemId 	
						LEFT JOIN
								(SELECT  LbPatientId, ProcedureResult,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  ProcedureResult IS NOT NULL ) pResult
								ON pResult.LbPatientId = t1.LbPatientId AND ISNULL(pResult.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')   AND t1.ProcedureCodeId = pResult.ProcedureCodeId 
									AND pResult.SourceSystemId =t1.SourceSystemId 		 
						LEFT JOIN
								(SELECT  LbPatientId, RenderingProviderNPI,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
								(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
								FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  RenderingProviderNPI IS NOT NULL ) renderingNPI
								ON renderingNPI.LbPatientId = t1.LbPatientId AND ISNULL(renderingNPI.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')    AND t1.ProcedureCodeId = renderingNPI.ProcedureCodeId 
									AND renderingNPI.SourceSystemId =t1.SourceSystemId 		 
						LEFT JOIN
							(SELECT  LbPatientId, PlaceOfService,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
							(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
							FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  PlaceOfService IS NOT NULL ) serviceLoc
							ON serviceLoc.LbPatientId = t1.LbPatientId AND ISNULL(serviceLoc.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')    AND t1.ProcedureCodeId = serviceLoc.ProcedureCodeId 
								AND serviceLoc.SourceSystemId =t1.SourceSystemId 			 		 
						LEFT JOIN
							(SELECT  LbPatientId, RevenueCode,  SourceSystemId,  ProcedureDateTime, ProcedureCodeID, ROW_NUMBER() OVER 
							(PARTITION BY  LbPatientId,  ISNULL(  ProcedureDateTime,'1900-01-01'),   SourceSystemId, ProcedureCodeID ORDER BY  InterfacePatientProcedureId DESC) rowNumber 
							FROM #tmpPatientProcedure    WITH (NOLOCK) WHERE  RevenueCode IS NOT NULL ) revCode
							ON revCode.LbPatientId = t1.LbPatientId AND ISNULL(revCode.ProcedureDateTime,'1900-01-01') = ISNULL(t1.ProcedureDateTime,'1900-01-01')    AND t1.ProcedureCodeId = revCode.ProcedureCodeId 
								AND revCode.SourceSystemId =t1.SourceSystemId 				 			 			 							
										WHERE 
												 (pcm1.rowNumber = 1 OR pcm1.rowNumber IS NULL)
												AND (pcm2.rowNumber = 1 OR pcm2.rowNumber IS NULL)
												AND (pcm3.rowNumber = 1 OR pcm3.rowNumber IS NULL)
												AND (pcm4.rowNumber = 1 OR pcm4.rowNumber IS NULL)
												AND (comment.rowNumber = 1 OR comment.rowNumber IS NULL)
												AND (enc.rowNumber = 1 OR enc.rowNumber IS NULL)
												AND (performby.rowNumber = 1 OR performby.rowNumber IS NULL)
												AND (orderby.rowNumber = 1 OR orderby.rowNumber IS NULL)
												AND (encId.rowNumber = 1 OR encId.rowNumber IS NULL)
												AND (totalCharges.rowNumber = 1 OR totalCharges.rowNumber IS NULL)
												AND (chargeCode.rowNumber = 1 OR chargeCode.rowNumber IS NULL)
												AND (pResult.rowNumber = 1 OR pResult.rowNumber IS NULL)
												AND (renderingNPI.rowNumber = 1 OR renderingNPI.rowNumber IS NULL)
												AND (serviceLoc.rowNumber = 1 OR serviceLoc.rowNumber IS NULL)
												AND (revCode.rowNumber = 1 OR revCode.rowNumber IS NULL)

										
									
									 ) AS tmp
									 WHERE rowNbr = 1
       


		 								SET @RecordCountSource=@@ROWCOUNT;

										SELECT @RecordCountBefore=count(1)
										FROM [dbo].PatientProcedure;


								     CREATE CLUSTERED INDEX CIX_#patientProcedure_PatientIDPPID ON #patientProcedure([PatientID],[SourceSystemID],ProcedureCodeID,ProcedureDateTime)

							

									 MERGE PatientProcedure T		 
									 USING #patientProcedure S
									 ON T.[PatientID] = S.[PatientID]
									 AND T.[SourceSystemID] = S.[SourceSystemID]
									 AND T.ProcedureCodeID = S.ProcedureCodeID
									 AND ISNULL(T.ProcedureDateTime, '1901-01-01') = ISNULL(S.ProcedureDateTime,  '1901-01-01')
									 WHEN MATCHED 
									  THEN UPDATE SET   T.[ProcedureFunctionTypeID] = ISNULL( S.ProcedureFunctionTypeID,T.[ProcedureFunctionTypeID] ),
														T.[ProcedureCodeModifier1] = ISNULL(S.[ProcedureCodeModifier1],T.[ProcedureCodeModifier1]),
														T.[ProcedureCodeModifier2] = ISNULL( S.[ProcedureCodeModifier2],T.[ProcedureCodeModifier2]),
														T.[ProcedureCodeModifier3] = ISNULL(S.[ProcedureCodeModifier3],T.[ProcedureCodeModifier3] ),
														T.[ProcedureCodeModifier4] = ISNULL(S.[ProcedureCodeModifier4],T.[ProcedureCodeModifier4] ), 
														T.[ProcedureDateTime] = ISNULL(S.[ProcedureDateTime],T.[ProcedureDateTime] ), 
														T.[ProcedureComment] = ISNULL( S.[ProcedureComment],T.[ProcedureComment]),
														T.[EncounterID] = ISNULL( S.[EncounterID],T.[EncounterID]),
														T.[PerformedByClinician] = ISNULL(S.[PerformedByClinician],T.[PerformedByClinician]),
														T.[OrderedByClinician] = ISNULL(S.[OrderedByClinician],T.[OrderedByClinician]),
														T.EncounterIdentifier =  ISNULL(S.EncounterIdentifier,T.EncounterIdentifier),
														T.TotalCharges = ISNULL( S.TotalCharges,T.TotalCharges),
														T.InternalChargeCode = ISNULL(S.InternalChargeCode,T.InternalChargeCode ),
														T.ProcedureResult =  ISNULL(S.ProcedureResult,T.ProcedureResult),
														T.[RenderingProviderNPI] = ISNULL(S.[RenderingProviderNPI],T.[RenderingProviderNPI]),
														T.[PlaceOfService] = ISNULL( S.[PlaceOfService], T.[PlaceOfService]),		--LBETL-315, leave the existing alone if it is there
														T.[DeleteInd] = 0,
														T.[ModifyDateTime] = GETUTCDATE(),
														T.RevenueCode= ISNULL(S.RevenueCode, T.RevenueCode)
									 WHEN NOT MATCHED BY TARGET
									   THEN INSERT ( PatientID,SourceSystemId,ProcedureCodeID,ProcedureCodingSystemTypeID, ProcedureFunctionTypeID,[ProcedureCodeModifier1],[ProcedureCodeModifier2],[ProcedureCodeModifier3]
												   ,[ProcedureCodeModifier4],[ProcedureDateTime],[ProcedureComment],[EncounterID],[PerformedByClinician],[OrderedByClinician],EncounterIdentifier,TotalCharges,InternalChargeCode
												   ,ProcedureResult,[RenderingProviderNPI],[PlaceOfService],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId],RevenueCode)
											Values(	S.PatientID,S.SourceSystemId,S.ProcedureCodeID,S.ProcedureCodingSystemTypeID, S.ProcedureFunctionTypeID,S.[ProcedureCodeModifier1],S.[ProcedureCodeModifier2],S.[ProcedureCodeModifier3]
												   ,S.[ProcedureCodeModifier4],S.[ProcedureDateTime],S.[ProcedureComment],S.[EncounterID],S.[PerformedByClinician],S.[OrderedByClinician],S.EncounterIdentifier,S.TotalCharges,S.InternalChargeCode
												   ,S.ProcedureResult,S.[RenderingProviderNPI],S.[PlaceOfService],0,GETUTCDATE(),GETUTCDATE(),1,1,S.RevenueCode);

									 MERGE [dbo].[PatientProcedureProcedureCode] T
									 USING (SELECT [PatientProcedureId], [ProcedureCodeId], [ProcedureCodingSystemTypeId] 
											FROM patientProcedure 
											WHERE [ProcedureCodeId] IS NOT NULL AND [ProcedureCodingSystemTypeId] IS NOT NULL) S
									 ON T.[PatientProcedureId] = S.[PatientProcedureId]
									 WHEN NOT MATCHED BY TARGET
									 THEN INSERT ([PatientProcedureId],[ProcedureCodingSystemTypeId] ,[ProcedureCodeId])
										  VALUES(S.[PatientProcedureId],S.[ProcedureCodingSystemTypeId] ,S.[ProcedureCodeId]);
		

		
								--Update the max processed time and drop the temp tables
							--SET @tmpmaxSourceTimeStampProcessed = 	(SELECT MAX(CreateDateTime) FROM #PatientProcedure )
							--IF @tmpmaxSourceTimeStampProcessed > @maxSourceTimeStampProcessed
							--BEGIN
							--SET @maxSourceTimeStampProcessed = @tmpmaxSourceTimeStampProcessed
							--END


						DROP TABLE #PatientProcedure
						DROP TABLE #ProcedureCodingSystemType
						DROP TABLE #tmpPatientProcedure
				END
			
			
			
				--Move onto the next batch
				--DELETE FROM #PatientProcedureQuePatientIdentifier WHERE PatientIdentifier IN (SELECT PatientIdentifier FROM #PatientProcedureQuePatientIdentifieBATCH) OR PatientIdentifier IS NULL
			
				DELETE FROM #PatientProcedureQuePatientIdentifier FROM 
					#PatientProcedureQuePatientIdentifier id, #PatientProcedureQuePatientIdentifieBATCH batch
				WHERE id.PatientIdentifier = batch.PatientIdentifier AND id.interfacesystemid  = batch.interfacesystemid

  			    DELETE FROM #PatientProcedureQuePatientIdentifier WHERE PatientIdentifier IS NULL			
			DROP TABLE #PatientProcedureQuePatientIdentifieBATCH
			IF @NUMROWS = 0 BREAK;
  
		END
		
		
			-- update maxSourceTimeStamp with max CreateDateTime of #PatientProcedureQue
			UPDATE [Maintenance]..[EDWTableLoadTracking]
			SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


			-- get total records from PatientProcedure after merge 
			SELECT @RecordCountAfter=COUNT(1)  FROM PatientProcedure WITH (NOLOCK);

			-- Calculate records inserted and updated counts
			SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
			SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

			-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			SET  @EndTime=GETUTCDATE();
			EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	
		



	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END



GO
