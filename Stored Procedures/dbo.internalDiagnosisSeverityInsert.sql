SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalDiagnosisSeverityInsert] 
    @DiagnosisSeverityTypeId INT OUTPUT ,
	@DiagnosisSeverity VARCHAR(255)  = NULL
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @DiagnosisCodeId INT

	IF @DiagnosisSeverity IS NOT NULL 
	BEGIN

		BEGIN TRAN

		SELECT @DiagnosisSeverityTypeId = DiagnosisSeverityTypeId FROM DiagnosisSeverityType
		WHERE Name = @DiagnosisSeverity

		IF @DiagnosisSeverityTypeId IS NULL
		BEGIN
			INSERT INTO [dbo].DiagnosisSeverityType
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
			VALUES 
			(
				@DiagnosisSeverity,
				@DiagnosisSeverity,
				@DeleteInd,
				GetDate(),
				GetDate(),
				@LBUserId,
				@LBUserId
			)
			SELECT @intError = @@ERROR,
				@DiagnosisSeverityTypeId = SCOPE_IDENTITY()
		END

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
		END
		ELSE
		BEGIN
			COMMIT TRAN
		END

	END
		
	RETURN @intError



GO
