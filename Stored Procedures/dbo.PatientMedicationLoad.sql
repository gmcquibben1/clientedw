SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientMedicationLoad] 
/*===============================================================
	CREATED BY: 	
	CREATED ON:		
	INITIAL VER:	
	MODULE:			Portal - Patient Clinical Info
	DESCRIPTION:	The sp returns the data that displays in the Patient clinical info, medication view
	PARAMETERS:		@PatientId
	RETURN VALUE(s)/OUTPUT:	
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2017-05-16		  PM		LBPP-2556	Add one output parameter i.e "StatusDescription".

	 ============================================================= */
	@PatientId INTEGER
AS

-- Modified by Youping
-- Modified Date: 2016-08-03
-- dedup medication name, MedicationStartDate, MedicationEndDate to pick one has value for PatientMedication
-- Modified Date: 2016-08-24
-- modified to shown medication name if MedicationNameFromSource is Blank.
--	
SELECT 
	pm.NDCCode,
	dbo.SourceSystem.Description as SourceSystem,
	--(CASE WHEN pm.MedicationNameFromSource IS NOT NULL THEN pm.MedicationNameFromSource ELSE dbo.Medication.BrandName END) AS MedicationName,
	(CASE WHEN isnull(pm.MedicationNameFromSource,'')<> '' THEN pm.MedicationNameFromSource ELSE dbo.Medication.BrandName END) AS MedicationName,
	(CASE WHEN pm.MedicationDoseFromSource IS NOT NULL THEN pm.MedicationDoseFromSource ELSE dbo.Medication.MedicationDose END) AS MedicationDose,
	(CASE WHEN pm.MedicationFormFromSource IS NOT NULL THEN pm.MedicationFormFromSource ELSE dbo.MedicationFormType.DisplayValue END) AS MedicationForm,
	(CASE WHEN pm.MedicationRouteFromSource IS NOT NULL THEN pm.MedicationRouteFromSource ELSE dbo.MedicationRouteType.DisplayValue END) AS MedicationRoute,
	pm.MedicationStartDate,
	pm.MedicationEndDate,
	pm.Refills,
	pm.SIGCode,
	pm.Instructions,
	pm.SampleInd,
	pm.RxNormCode,
	pm.GenericAllowedInd,
	pm.Comment,
	pm.StatusDescription,
	Row_number() OVER(PARTITION BY dbo.SourceSystem.Description,(CASE WHEN isnull(pm.MedicationNameFromSource,'')<> '' THEN pm.MedicationNameFromSource ELSE dbo.Medication.BrandName END)
	,pm.MedicationStartDate, pm.MedicationEndDate
					ORDER BY pm.PatientMedicationId ) AS rowNbr
INTO #patmedication	
FROM dbo.PatientMedication pm INNER JOIN
	 dbo.Medication ON pm.MedicationId = Medication.MedicationId LEFT OUTER JOIN 
	 dbo.SourceSystem ON pm.SourceSystemId = SourceSystem.SourceSystemId LEFT OUTER JOIN
	 dbo.MedicationFormType ON Medication.MedicationFormTypeId = MedicationFormType.MedicationFormTypeId LEFT OUTER JOIN
	 dbo.MedicationRouteType ON Medication.MedicationRouteTypeId = MedicationRouteType.MedicationRouteTypeId	 

WHERE pm.PatientId = @Patientid AND pm.DeleteInd = 0

SELECT NDCCode,
	SourceSystem,
	MedicationName,
	MedicationDose,
	MedicationForm,
	MedicationRoute,
	MedicationStartDate,
	MedicationEndDate,
	Refills,
	SIGCode,
	Instructions,
	SampleInd,
	RxNormCode,
	GenericAllowedInd,
	Comment,
	StatusDescription
FROM #patmedication	
WHERE rowNbr=1
ORDER BY MedicationStartDate DESC, MedicationName ASC

GO
