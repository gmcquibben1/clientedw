SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientLabOrderLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 

AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT 
		plo.PatientId,
		plo.SourceSystemId,
		plo.PatientLabOrderId,
		plo.OrderNumber,
		plo.ServiceId,
		plo.ServiceDescription,
		plo.ServiceCodingSystemName,
		plo.OrderingProviderId,
		plo.OrderingProviderIdType,
		plo.OrderingProviderName
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientLabOrder plo ON plo.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
		AND (plo.CreateDateTime > @dtmStartDate OR plo.ModifyDateTime > @dtmStartDate)

END
GO
