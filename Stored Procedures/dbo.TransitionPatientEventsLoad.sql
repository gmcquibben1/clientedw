SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[TransitionPatientEventsLoad]
AS
BEGIN
	
	SELECT 
	dbo.PatientEvent.LBPatientId,
	dbo.PatientEvent.EventDateTime,
	dbo.PatientEventType.DisplayValue AS EventName
FROM 
	dbo.PatientEvent INNER JOIN
	dbo.PatientEventType ON PatientEvent.PatientEventTypeId = PatientEventType.PatientEventTypeId
WHERE PatientEvent.DeleteInd = 0 AND PatientEvent.EventDateTime >= DATEADD(DAY,-30,GETDATE())
ORDER BY
	PatientEvent.EventDateTime DESC
END

GO
