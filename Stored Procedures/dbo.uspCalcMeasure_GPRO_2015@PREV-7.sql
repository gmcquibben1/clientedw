SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@PREV-7] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];
	IF OBJECT_ID('tempdb..#ENC') IS NOT NULL 
		DROP TABLE [#ENC];
	IF OBJECT_ID('tempdb..#flu') IS NOT NULL 
		DROP TABLE [#flu];

	DECLARE  @yearend datetime, @measureBeginDate datetime, @BeginFluDt datetime, @EndFluDt datetime;

	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2015';

	set @BeginFluDt = dateadd(month,-3,@measureBeginDate); --oct of prior year
	set @EndFluDt = dateadd(day,-1,dateadd(month,3,@measureBeginDate));-- march 31 of measurement year

	--Establishing the Clinical Data sourcing indicators.
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END


	--Initial Patient pooling, for patients that have ranking for the care-2 measure. 
	SELECT r.LbPatientId,
		r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,@BeginFluDt) as Age,
		DATEDIFF(MONTH,r.Birthdate,@BeginFluDt) as Age_MONTH,
		r.Birthdate,
		GM.GproMeasureTypeID,
		GPM.PcFluShotConfirmed,
		GPM.PcFluShotReceived,
		GPM.PcFluShotComments, 
		GETUTCDATE() as ModifyDateTime
	INTO #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
	JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
	JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
	JOIN GproPatientMeasure GPM ON GPM.GproPatientRankingId = r.GproPatientRankingID
	WHERE GM.Name = 'PREV-7' 
		AND (GPM.PcFluShotConfirmed IS NULL OR GPM.PcFluShotReceived IS NULL)

	-- SELECT DISTINCT [VariableName] FROM [GproEvaluationCode] WHERE [ModuleType]= 'PREV' AND [ModuleIndicatorGPRO]='7' 
	
	---CHECK IF PATIENT HAS ENCOUNTER DURING MEASURMENT YEAR AND CALC AGE AT THE TIME OF EARLIEST ENCOUNTER
	SELECT 
		RP.LbPatientId, 
		rp.Age_MONTH, 
		rp.age, 
		COUNT(DISTINCT PP.ProcedureDateTime) VISIT_CNT
	INTO #ENC -- drop table #enc
	FROM #RankedPatients RP
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = RP.LbPatientId
			AND pp.ProcedureDateTime >= @BeginFluDt
			AND pp.ProcedureDateTime <= @EndFluDt
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
		INNER JOIN GproEvaluationCode hvsc ON hvsc.Code = pp.ProcedureCode
			AND hvsc.ModuleType = 'PREV'
			AND hvsc.ModuleIndicatorGPRO = '7'
			AND hvsc.VariableName IN ('ENCOUNTER_CODE')
	GROUP BY RP.LbPatientId, RP.Age_MONTH, RP.Age
	HAVING COUNT(DISTINCT pp.ProcedureDateTime) >= 2
	ORDER BY RP.LbPatientId
	
	--AGE confirmed 6 MONTHS AND OLDER
    UPDATE RP
    SET    RP.PcFluShotConfirmed = '2' -- yes
    FROM #RankedPatients RP 
	  JOIN #ENC ENC ON RP.LbPatientId = ENC.LbPatientId
    Where RP.PcFluShotConfirmed IS NULL
		AND ENC.Age_month >= 6
		AND RP.PcFluShotConfirmed IS NULL

	----influenza
	--SELECT sub.LbPatientId,  sub.ProcedureDateTime
	--INTO #flu -- drop table #flu
	--FROM (SELECT 
	--		RP.LbPatientId,  
	--		PP.ProcedureDateTime,
	--		ROW_NUMBER () OVER (PARTITION BY rp.LbPatientId ORDER BY pp.ProcedureDateTime DESC) earliest_dt
	--	  FROM #RankedPatients RP
	--		JOIN dbo.patientProcedure PP ON RP.LbPatientId = PP.PATIENTID
	--		JOIN dbo.patientProcedureProcedureCode PPC ON pp.[PatientProcedureId] = ppc.[PatientProcedureId]
	--		JOIN dbo.ProcedureCode PC ON PPC.PROCEDURECODEID = PC.PROCEDURECODEID
	--		JOIN DBO.[GproEvaluationCode] HVSC ON PC.ProcedureCode = HVSC.Code
	--						AND hvsc.[ModuleType] = 'PREV'
	--						AND hvsc.[ModuleIndicatorGPRO] = '7'
	--						AND HVSC.[VariableName] IN ('INFLUENZA_CODE')
	--						AND PP.ProcedureDateTime >=  @BeginFluDt--'10-1-2014'-- oct 1 of prior year
	--						AND PP.ProcedureDateTime <=  @EndFluDt -- '03-31-2015'--march 31 of meas year --
	--		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS        
	--	GROUP BY RP.LbPatientId,  PP.ProcedureDateTime
	--	) sub
	--WHERE sub.earliest_dt = 1 -- earliest date
	--ORDER by sub.LbPatientId

	--update numerator by procedure
	UPDATE rp
	SET rp.PcFluShotConfirmed = '2',
		rp.PcFluShotReceived = '2' -- yes
	FROM #RankedPatients rp
		INNER JOIN vwPatientProcedure pp ON pp.PatientId = rp.LbPatientId
			AND pp.ProcedureDateTime >= @BeginFluDt
			AND pp.ProcedureDateTime <= @EndFluDt
		INNER JOIN GproEvaluationCode gec ON gec.Code = pp.ProcedureCode
			AND gec.ModuleType= 'PREV'
			AND gec.ModuleIndicatorGPRO = '7'
			AND gec.VariableName = 'INFLUENZA_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = pp.SourceSystemId
	WHERE rp.PcFluShotReceived IS NULL
		AND (rp.PcFluShotConfirmed IS NULL OR rp.PcFluShotConfirmed = '2')

	--update numerator by immunization
	UPDATE rp
	SET rp.PcFluShotConfirmed = '2',
		rp.PcFluShotReceived = '2' -- yes
	FROM #RankedPatients rp
		INNER JOIN vwPatientImmunization imm ON imm.PatientId = rp.LbPatientId
			AND imm.ServiceDate >= @BeginFluDt
			AND imm.ServiceDate <= @EndFluDt
		INNER JOIN GproEvaluationCode gec ON gec.Code = imm.ImmunizationCode
			AND gec.ModuleType= 'PREV'
			AND gec.ModuleIndicatorGPRO = '7'
			AND gec.VariableName = 'INFLUENZA_CODE'
		INNER JOIN @SourceIdTable ss ON ss.SourceSystemId = imm.SourceSystemId
	WHERE rp.PcFluShotReceived IS NULL
		AND (rp.PcFluShotConfirmed IS NULL OR rp.PcFluShotConfirmed = '2')


	-- flu confirmed
 --   UPDATE RP
 --   SET RP.PcFluShotConfirmed = '2',
	--	RP.PcFluShotReceived = '2' -- yes
 --   FROM #RankedPatients RP 
	--	INNER JOIN #flu flu ON RP.LbPatientId = flu.LbPatientId
	--WHERE RP.PcFluShotReceived IS NULL
	--	AND (RP.PcFluShotConfirmed IS NULL OR RP.PcFluShotConfirmed = '2')

	--------------------------------------------------------------------------------------------------------          
   -- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	UPDATE gpm
	SET gpm.PcFluShotConfirmed = gpr.PcFluShotConfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		   JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcFluShotRank IS NOT NULL
		AND gpm.PcFluShotConfirmed IS NULL

	UPDATE gpm
	SET gpm.PcFluShotReceived = gpr.PcFluShotReceived, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		   JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcFluShotRank IS NOT NULL
		AND gpm.PcFluShotReceived IS NULL

END
		
GO
