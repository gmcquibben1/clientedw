SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLMemberExtractPatientAttribute]
/*
	Procedure:		ETLMemberExtractPatientAttribute

	Description:	Stored Procedure used to create patient Status on various patient attributes
	
	@PatientId -	id of patient 

    MODIFICATIONS
	Version     Date            Author      JIRA            Change
	=======     ==========      ======      =========       =========	
	1.1.1	    2016-11-11	    SG		                Old Procedure , with code for new "Status" attribute added.
	1.1.2	    2016-11-28	    SG		    LBETL-251       added changes requestd by LBETL-251.
	2.2.1       2017-02-22      YL          LBETL-918       Eliminate Delete/Insert large amount of data

*/
AS
BEGIN

	SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'vwPortal_PatientAttribute';
	DECLARE @dataSource VARCHAR(20) = 'MemberExtract';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLMemberExtractPatientAttribute';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @COMMENT VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @LBUSERID            INT=1
	DECLARE @CurrentDateTime     DATETIME=GETUTCDATE()
	DECLARE @DeleteInd           BIT=0


	BEGIN TRY
			--DELETE FROM vwPortal_PatientAttribute
		--DELETE FROM vwPortal_PatientAttribute WHERE   PatientAttributeTypeId IN (
		--	SELECT DISTINCT PatientAttributeTypeId FROM vwPortal_PatientAttributeType 
		--	WHERE NAME IN( 'Contract' 
		--			, 'ContractMemberIdentifier' 
		--			, 'Status'
		--		)	
		--)
		--;

	/*
	* Changed by SG: based on Mike's feedback, change logic for below part.
	*/
         DECLARE @PatientAttributeTypeId1 INT, @PatientAttributeTypeId2 INT
	     SELECT @PatientAttributeTypeId1= PatientAttributeTypeId FROM vwPortal_PatientAttributeType WHERE NAME = 'Contract';

			SELECT DISTINCT OH.LbPatientId,
				@PatientAttributeTypeId1 PatientAttributeTypeId,
				OH.ContractName
			INTO #PatientAttribute
			FROM dbo.OrgHierarchy OH
			JOIN dbo.vwportal_Patient P
			ON OH.LbPatientId = P.PatientID
			WHERE P.DeleteInd = 0 ;
			
			
			CREATE CLUSTERED INDEX CIDX_PATIENTID_TYPEID ON  #PatientAttribute (LbPatientId,PatientAttributeTypeId);

			MERGE vwPortal_PatientAttribute T
			USING  #PatientAttribute S ON T.PatientId=S.LbPatientId AND T.PatientAttributeTypeId=S.PatientAttributeTypeId
			WHEN MATCHED THEN
				UPDATE SET T.Value = S.ContractName
				,T.DeleteInd = 0
				,T.CreateLBUserId=1
				,T.ModifyLbUserId=1
                ,[ModifyDateTime]=@CurrentDateTime 
			WHEN NOT MATCHED BY target THEN
			INSERT (PatientId,PatientAttributeTypeId,Value,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
				VALUES (S.LbPatientId,S.PatientAttributeTypeId,S.ContractName,0,@CurrentDateTime,@CurrentDateTime ,1,1);

          -- remove some old records
		    DELETE vwPortal_PatientAttribute
		    WHERE PatientAttributeTypeId=@PatientAttributeTypeId1  and ModifyDateTime<@CurrentDateTime;


			/*
			 * Add Status rows from OrgHierarchy for 2.1.1 release
			 */

			 SELECT @PatientAttributeTypeId2= PatientAttributeTypeId FROM vwPortal_PatientAttributeType WHERE NAME = 'Status';

			SELECT DISTINCT OH.LbPatientId,
							@PatientAttributeTypeId2 PatientAttributeTypeId,
							OH.Patient_Status
			INTO #PatientAttribute2
			FROM dbo.OrgHierarchy OH
			JOIN dbo.vwportal_Patient P
			ON OH.LbPatientId = P.PatientID
			WHERE P.DeleteInd = 0 ;
			

			CREATE CLUSTERED INDEX CIDX_PATIENTID_TYPEID ON  #PatientAttribute2 (LbPatientId,PatientAttributeTypeId);

			MERGE vwPortal_PatientAttribute T
			USING  #PatientAttribute2 S ON T.PatientId=S.LbPatientId AND T.PatientAttributeTypeId=S.PatientAttributeTypeId
			WHEN MATCHED THEN
				UPDATE SET T.Value = S.Patient_Status
				,T.DeleteInd = 0
				,T.CreateLBUserId=1
				,T.ModifyLbUserId=1
                ,[ModifyDateTime]=@CurrentDateTime
			WHEN NOT MATCHED BY target THEN
			INSERT (PatientId,PatientAttributeTypeId,Value,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
				VALUES (S.LbPatientId,S.PatientAttributeTypeId,S.Patient_Status,0,@CurrentDateTime,@CurrentDateTime,1,1);

           -- remove some old records
		   DELETE vwPortal_PatientAttribute
		   WHERE PatientAttributeTypeId=@PatientAttributeTypeId2  and ModifyDateTime<@CurrentDateTime;

           SELECT @InsertedRecord=SUM(CASE WHEN CreateDateTime=@CurrentDateTime THEN 1 ELSE 0 END),
                  @UpdatedRecord=SUM(CASE WHEN CreateDateTime!=@CurrentDateTime AND  ModifyDateTime= @CurrentDateTime THEN 1 ELSE 0 END)
           FROM vwPortal_PatientAttribute
           WHERE  PatientAttributeTypeId in (@PatientAttributeTypeId1, @PatientAttributeTypeId2);
                   
            
						-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
						   SET  @EndTime=GETUTCDATE();
						   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;

	        DROP TABLE #PatientAttribute, #PatientAttribute2;
		END TRY
		BEGIN CATCH
				   --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
					SET  @EndTime=GETUTCDATE();
					SET  @ErrorNumber =ERROR_NUMBER();
					SET  @ErrorState =ERROR_STATE();
					SET  @ErrorSeverity=ERROR_SEVERITY();
					SET  @ErrorLine=ERROR_LINE();
					SET  @COMMENT =LEFT(ERROR_MESSAGE(),400);
					EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
				
		END CATCH

	
	
END







GO
