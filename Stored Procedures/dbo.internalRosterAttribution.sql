SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ======================================================================================
-- Author:		Mike Hoxter
-- Create date: 2015-12-18
-- Description:	set patient provider attribution from Patient Roster Enrollment data
-- Modified By: Lan Ma
-- Modified Date: 2016-04-05
-- Modified By: Youping
-- Modified Date: 2016-04-21  add LbPatientId IS NOT NULL
-- ======================================================================================

CREATE PROCEDURE [dbo].[internalRosterAttribution] 
AS

BEGIN	

  --select most recent roster w/ an NPI per patient per contract
  SELECT LbPatientId, SourceFeed, MAX(FileDate) AS FileDate 
  into #TPM_PRE
  FROM PatientRosterEnrollment
  WHERE ProviderNPI IS NOT NULL
  group by LbPatientId, SourceFeed

   --new code using the vwportal_ProviderActive to exclude the provides without healthcare org assigned, updated on 04/05/2016
	INSERT INTO [dbo].[vwPortal_PatientProvider]
           ([PatientID]
           ,[ProviderID]
           ,[SourceSystemID]
           ,[ExternalReferenceIdentifier]
           ,[OtherReferenceIdentifier]
           ,[ProviderRoleTypeID]
           ,[DeleteInd]
           ,[CreateDateTime]
           ,[ModifyDateTime]
           ,[CreateLBUserId]
           ,[ModifyLBUserId]
           ,[AttributionTypeId]
           )
	SELECT DISTINCT 
	 t1.LbPatientId AS [PatientID]
	,t2.[ProviderID]
	,t1.[SourceSystemID] AS [SourceSystemID]
	,t1.PatientExternalID AS [ExternalReferenceIdentifier]
	,t2.NationalProviderIdentifier AS [OtherReferenceIdentifier]
	,1 AS [ProviderRoleTypeID]
    ,0 AS [DeleteInd]
    ,getdate() AS [CreateDateTime]
    ,getdate() AS [ModifyDateTime]
    ,1 [CreateLBUserId]
    ,1 [ModifyLBUserId]
	,1 [AttributionTypeId]
	FROM PatientRosterEnrollment t1
  JOIN #TPM_PRE pre on t1.FileDate = pre.FileDate and t1.LbPatientId = pre.LbPatientId and t1.SourceFeed = pre.SourceFeed
	JOIN [vwPortal_ProviderActive] t2
	ON RTRIM(LTRIM(t1.ProviderNPI)) = RTRIM(LTRIM(t2.NationalProviderIdentifier))
	AND t1.LbPatientId IS NOT NULL  
	AND t1.ProviderNPI IS NOT NULL
	WHERE  t1.LbPatientId IS NOT NULL
/* --Commented the original code, keep it for reference
	exec InternalDropTempTables

	SELECT DISTINCT *
	into #PRE
	FROM PatientRosterEnrollment
	where LbPatientId IS NOT NULL and 
	ProviderNPI IS NOT NULL

	--  drop table #pre_1
	select distinct pre.*, vpp.ProviderID,vpp.NationalProviderIdentifier
	into #pre_1
	from #pre pre
	join [vwPortal_Provider] vpp on pre.ProviderNPI = vpp.NationalProviderIdentifier

	INSERT INTO [dbo].[vwPortal_PatientProvider]
           ([PatientID]
           ,[ProviderID]
           ,[SourceSystemID]
           ,[ExternalReferenceIdentifier]
           ,[OtherReferenceIdentifier]
           ,[ProviderRoleTypeID]
           ,[DeleteInd]
           ,[CreateDateTime]
           ,[ModifyDateTime]
           ,[CreateLBUserId]
           ,[ModifyLBUserId]
           ,[AttributionTypeId]
           )
	SELECT DISTINCT 
	pre.LbPatientId [PatientID]
	,ISNULL(pre.[ProviderID],0) [ProviderID]
	,pre.[SourceSystemID] [SourceSystemID]
	,pre.PatientExternalID [ExternalReferenceIdentifier]
	,ISNULL(pre.NationalProviderIdentifier,9999999999) [OtherReferenceIdentifier]
	,1 [ProviderRoleTypeID]
    ,0 [DeleteInd]
    ,getdate() [CreateDateTime]
    ,getdate() [ModifyDateTime]
    ,1 [CreateLBUserId]
    ,1 [ModifyLBUserId]
	,1 [AttributionTypeId]
	from #PRE_1 PRE
	left join [vwPortal_PatientProvider] vwPat on pre.LbPatientId = vwPat.PatientID 
		and pre.ProviderID = vwPat.ProviderID and [DeleteInd] = 0 AND AttributionTypeId = 1
	where vwPat.PatientID IS NULL
*/	
	  	
END 
GO
