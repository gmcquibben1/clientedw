SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproMeasureRankListLoad]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2015-12-29
	INITIAL VER:	1.6.1
	MODULE:			GPRO		
	DESCRIPTION:	Return the list of patient ranked for the specified measure and their status for that one measure
	PARAMETERS:		Business Unit id, GPro measure id, user id 
	RETURN VALUE(s)/OUTPUT:	a table containing the patient info, rank, and status for the given measure and bu
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.6.1		12.29.2015		BR						Initial Version
	2.2.1		10.27.2016		BR						Adding PREV-13, Statin
	2.2.1		10.31.2016		BR			LBPP-2027	Adding the Business unit name to the return values.  Refactored to make is cleaner and move status logic to sub SPs
	2.2.1		2016-11-09		BR			LBPP-2028	Added additional column to skipped and complete sets to comply w/ new stored proc return values
	2.2.1		11-23-2016		BR			LBPP-2033	Fixed a bug by changing indexes on temp table to use GproPatientRankingId.  This is because PatientId will be NULL if
														the patient can't be matched to an existing patient
														Fixed an issue with the return not listing the BU for items other than the root
	2.2.1		2016-12-19		WK						Updating the SP to take in a parameter which dictates whether to return results or insert them into a temp table.
=================================================================*/
(
	@businessUnitId INT,
	@measureId INT,
	@loggedInUserId INT = 1,
	@transactionalDbName VARCHAR(200) = NULL,
	@resultToTempTable BIT = 0
)
AS
BEGIN
    SET NOCOUNT ON;

	--declare @businessUnitId INT = 153
	--declare @measureId INT = 4

	DECLARE @sql NVARCHAR(4000);

	--------------------------------------------------------------------------------
	-- create temp tables to build up the status list
	--------------------------------------------------------------------------------
	CREATE TABLE #tmpBusinessUnits(
		BusinessUnitId INT
		UNIQUE CLUSTERED (BusinessUnitId)
	)

	CREATE TABLE #tmpRankingTable(
		GproPatientRankingId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	)


	CREATE TABLE #tmpGproPatientstatusTable(
			GProPatientMeasureId	INT,	--used to find the exact row in the GProPatientMeasure table this result refers to
			GProPatientRankingId	INT,	--used to find the exact row in the GProPatientRanking table this result refers to
			MeasureId				VARCHAR(8),
			MeasureName				VARCHAR(2000),
			MeasureStatus			VARCHAR(155),
			MeasureRank				INT,
			MedicareHicn			VARCHAR(15),
			BusinessUnitName		VARCHAR(250)
	)

	CREATE TABLE #CompletePatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	CREATE TABLE #SkippedPatients
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	--------------------------------------------------------------------------------
	-- find the name of the measure being displayed based on the id
	--------------------------------------------------------------------------------
	DECLARE @measureName varchar(20)
	SELECT @measureName = gmt.Name FROM  [GproMeasureType] gmt  WHERE gmt.GproMeasureTypeId = @measureId

	--------------------------------------------------------------------------------
	-- fill the temp table with the patients ranked for this measure and the status
	--------------------------------------------------------------------------------
	DECLARE @CurrentRankColumn VARCHAR(20) = NULL;

	SET @CurrentRankColumn = (SELECT RankColumnName FROM GproMeasureType WHERE Name = @measureName)
	IF (@CurrentRankColumn IS NOT NULL)
	BEGIN
		SET @Sql = 'INSERT INTO #CompletePatients EXEC GproGetCompletePatientList ''' + @measureName + ''''
		EXEC (@Sql);
		SET @Sql = 'INSERT INTO #SkippedPatients EXEC GproGetSkippedPatientList ''' + @measureName + ''''
		EXEC (@Sql);

		SET @Sql = 'INSERT INTO #tmpGproPatientstatusTable 
		(GProPatientMeasureId, GProPatientRankingId, MeasureId, MeasureName, MeasureStatus, MeasureRank, MedicareHicn)
			SELECT DISTINCT gpm.GProPatientMeasureId, gpm.GProPatientRankingId, gmt.Name, gmt.DisplayValue, 
				CASE 
					WHEN EXISTS (SELECT TOP 1 1 FROM #SkippedPatients sp WHERE sp.GproPatientRankingId = gpm.GproPatientRankingId) THEN ''Skipped''
					WHEN EXISTS (SELECT TOP 1 1 FROM #CompletePatients cp WHERE cp.GproPatientRankingId = gpm.GproPatientRankingId) THEN ''Complete''
					ELSE ''Incomplete''
				END,
				gpr.' + @CurrentRankColumn + ',
				gpr.MedicareHicn 
			FROM [GproPatientMeasure] gpm 
				INNER JOIN [GproPatientRanking] gpr ON gpr.GProPatientRankingId = gpm.GProPatientRankingId 
				INNER JOIN [GproMeasureType] gmt ON gmt.Name = ''' + @measureName + '''
			WHERE  gpr.' + @CurrentRankColumn + ' IS NOT NULL AND gpr.' + @CurrentRankColumn + '<> 0';

		EXEC (@Sql);
	END


	--------------------------------------------------------------------------------
	-- Find the transactional db to figure out which patients are allowed to be viewed for this business unit
	--------------------------------------------------------------------------------
	DECLARE @tranDbName VARCHAR(200);
	DECLARE @edwDbName VARCHAR(200) = DB_NAME();
	DECLARE @useGproAttribution SMALLINT = 0;

	SET @tranDbName = REPLACE(@edwDbName,'Edw','Lbportal') 

	SELECT @useGproAttribution = (SELECT (CASE WHEN SetValue = 'true' THEN 1 ELSE 0 END) FROM vwPortal_SystemSettings WHERE SettingType = 'GPro' AND SettingParameter = 'UseGproAttribution');

	--------------------------------------------------------------------------------
	-- get the set of patients related to this business unit
	--------------------------------------------------------------------------------
	INSERT INTO #tmpRankingTable EXEC GproGetPatientListByBusinessUnit @businessUnitId

	IF (@useGproAttribution = 1)
	BEGIN
		UPDATE tmpGproStatus 
		SET tmpGproStatus.BusinessUnitName = bu.Description
		FROM #tmpGproPatientstatusTable tmpGproStatus
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = tmpGproStatus.GproPatientRankingId
			INNER JOIN vwPortal_BusinessUnit bu ON bu.ExternalCode = gpr.ClinicIdentifier AND bu.DeleteInd <> 1;
	END
	ELSE
	BEGIN
		UPDATE tmpGproStatus 
		SET tmpGproStatus.BusinessUnitName = bu.Description
		FROM #tmpGproPatientstatusTable tmpGproStatus
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = tmpGproStatus.GproPatientRankingId
			INNER JOIN vwPortal_PatientHealthcareOrg pho ON pho.PatientId = gpr.LbPatientId AND pho.DeleteInd <> 1
			INNER JOIN vwPortal_HealthcareOrg ho ON ho.HealthcareOrgId = pho.HealthcareOrgId AND ho.DeleteInd <> 1
			INNER JOIN vwPortal_BusinessUnit bu ON bu.BusinessUnitId = ho.BusinessUnitId AND bu.DeleteInd <> 1;
	END

	--------------------------------------------------------------------------------
	-- return the rows as output
	--------------------------------------------------------------------------------
	IF(@resultToTempTable = 0)
	BEGIN
		IF NOT EXISTS (SELECT TOP 1 1 FROM vwPortal_BusinessUnit bu WHERE bu.BusinessUnitId = @businessUnitId AND bu.ParentBusinessUnitId IS NULL)
		BEGIN
			SELECT	DISTINCT  patRank.GProPatientRankingId
					, patRank.FirstName AS FirstName
					, patRank.LastName AS LastName
					, patRank.BirthDate AS BirthDate
					, patRank.GenderCode AS GenderCode
					, patRank.MedicareHicn AS MedicareHicn
					, tmpGproStatus.MeasureStatus AS GProStatusTypeId
					, rankMeasureType.MeasureRank AS MeasureRank
					, tmpGproStatus.BusinessUnitName
			FROM GproPatientRanking patRank
				INNER JOIN GproPatientRankingGproMeasureType rankMeasureType ON patRank.GProPatientRankingId = rankMeasureType.GproPatientRankingId
				INNER JOIN #tmpRankingTable tmpPat ON patRank.GproPatientRankingId = tmpPat.GproPatientRankingId
				INNER JOIN #tmpGproPatientstatusTable tmpGproStatus  ON patRank.GproPatientRankingId = tmpGproStatus.GproPatientRankingId
			WHERE rankMeasureType.GproMeasureTypeId = @measureId
		END
		ELSE
		BEGIN
			SELECT	DISTINCT  patRank.GProPatientRankingId
					, patRank.FirstName AS FirstName
					, patRank.LastName AS LastName
					, patRank.BirthDate AS BirthDate
					, patRank.GenderCode AS GenderCode
					, patRank.MedicareHicn AS MedicareHicn
					, tmpGproStatus.MeasureStatus AS GProStatusTypeId
					, rankMeasureType.MeasureRank AS MeasureRank
					, tmpGproStatus.BusinessUnitName
			FROM GproPatientRanking patRank
				INNER JOIN GproPatientRankingGproMeasureType rankMeasureType ON patRank.GProPatientRankingId = rankMeasureType.GproPatientRankingId
				INNER JOIN #tmpGproPatientstatusTable tmpGproStatus  ON patRank.GproPatientRankingId = tmpGproStatus.GproPatientRankingId
			WHERE rankMeasureType.GproMeasureTypeId = @measureId
		END
	END
	ELSE --populate rows to temp table
	BEGIN
		IF NOT EXISTS (SELECT TOP 1 1 FROM vwPortal_BusinessUnit bu WHERE bu.BusinessUnitId = @businessUnitId AND bu.ParentBusinessUnitId IS NULL)
		BEGIN
			INSERT INTO #TempGProPatientRanking
			SELECT	DISTINCT  patRank.GProPatientRankingId
					, patRank.FirstName AS FirstName
					, patRank.LastName AS LastName
					, patRank.BirthDate AS BirthDate
					, patRank.GenderCode AS GenderCode
					, patRank.MedicareHicn AS MedicareHicn
					, tmpGproStatus.MeasureStatus AS GProStatusTypeId
					, rankMeasureType.MeasureRank AS MeasureRank
					, tmpGproStatus.BusinessUnitName
			FROM GproPatientRanking patRank
				INNER JOIN GproPatientRankingGproMeasureType rankMeasureType ON patRank.GProPatientRankingId = rankMeasureType.GproPatientRankingId
				INNER JOIN #tmpRankingTable tmpPat ON patRank.GproPatientRankingId = tmpPat.GproPatientRankingId
				INNER JOIN #tmpGproPatientstatusTable tmpGproStatus  ON patRank.GproPatientRankingId = tmpGproStatus.GproPatientRankingId
			WHERE rankMeasureType.GproMeasureTypeId = @measureId
		END
		ELSE
		BEGIN
			INSERT INTO #TempGProPatientRanking
			SELECT	DISTINCT  patRank.GProPatientRankingId
					, patRank.FirstName AS FirstName
					, patRank.LastName AS LastName
					, patRank.BirthDate AS BirthDate
					, patRank.GenderCode AS GenderCode
					, patRank.MedicareHicn AS MedicareHicn
					, tmpGproStatus.MeasureStatus AS GProStatusTypeId
					, rankMeasureType.MeasureRank AS MeasureRank
					, tmpGproStatus.BusinessUnitName
			FROM GproPatientRanking patRank
				INNER JOIN GproPatientRankingGproMeasureType rankMeasureType ON patRank.GProPatientRankingId = rankMeasureType.GproPatientRankingId
				INNER JOIN #tmpGproPatientstatusTable tmpGproStatus  ON patRank.GproPatientRankingId = tmpGproStatus.GproPatientRankingId
			WHERE rankMeasureType.GproMeasureTypeId = @measureId
		END
	END
	--------------------------------------------------------------------------------
	-- cleanup
	--------------------------------------------------------------------------------
	DROP Table #tmpBusinessUnits
	DROP Table #tmpRankingTable
	DROP Table #tmpGproPatientstatusTable

END
GO
