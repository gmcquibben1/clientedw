SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLOrgHierarchyMMBuild] 
AS
BEGIN
  
EXEC dbo.InternalDropTempTables
  
SELECT distinct
t1.LbPatientID as LbPatientId,
t1.MemberMonth,
t10.[Name] AS [State], 
t9.City , 
t9.[PostalCode],
t2.[level1_buCode],
t2.[level1_buName],
t2.[level1_buType],
t2.[level2_buCode],
t2.[level2_buName],
t2.[level2_buType],
t2.[level3_buCode],
t2.[level3_buName],
t2.[level3_buType],
t2.[level4_buCode],
t2.[level4_buName],
t2.[level4_buType],
t2.[level5_buCode],
t2.[level5_buName],
t2.[level5_buType],
t2.[level6_buCode],
t2.[level6_buName],
t2.[level6_buType],
--CASE WHEN t1.sourceFeed = 'interface' then 'NoContract' else sourceFeed END,
--NULL,
LTRIM(RTRIM(t11.[ContractName]))  ContractName,
null as medicareNo,
LTRIM(RTRIM(t6.lastName)) + ', ' + LTRIM(RTRIM(t6.firstName)) MemberFullName,
--NULL,
--NULL,
--t1.medicareNo,
--'Active (WithClaims)',
t11.[ContractStatus] + ' (' + [ClaimStatus] + ')' Patient_Status,
COALESCE(t3.ProviderID,0) AS providerID,
COALESCE(LTRIM(RTRIM(t4.[LastName]))  + ' ' + LTRIM(RTRIM(t4.[FirstName])), 'unattributed') AS ProviderName,
COALESCE(t3.[NationalProviderIdentifier], '9999999999') AS ProviderNPI,
t7.BirthDate, 
t7.[GenderTypeID], 
GETDATE() LastUpdatedTimestamp, ROW_NUMBER() OVER (PARTITION BY t1.LbPatientId ORDER BY t1.ProviderNPI) AS RowRank
INTO #OrgHierarchy
FROM PatientRosterMonths t1
join vwPortal_ProviderActive t3 on t1.ProviderNPI = t3.NationalProviderIdentifier
join vwPortal_ProviderHealthcareOrg pho on t3.ProviderID = pho.ProviderID 
                                        and pho.DeleteInd = 0 and pho.DefaultInd = 1
JOIN vwPortal_HealthcareOrg ho on pho.HealthcareOrgID = ho.HealthcareOrgId
JOIN [dbo].[orgHierarchyStructure] t2
 on ho.BusinessUnitId = t2.buId
LEFT JOIN [dbo].[vwPortal_IndividualName] t4
ON t3.[IndividualID] = t4.IndividualID
AND t4.DeleteInd = 0
LEFT JOIN [dbo].[vwPortal_Patient] t5
ON t1.LbPatientId = t5.PatientId
AND t5.[DeleteInd] = 0
Left JOIN [dbo].[vwPortal_IndividualName] t6
ON t5.[IndividualID] = t6.IndividualID
--and t6.DeleteInd = 0
LEFT JOIN [dbo].[vwPortal_Individual] t7
ON t5.IndividualID = t7.IndividualID
--And t7.DeleteInd = 0
--LEFT JOIN [dbo].[vwPortal_IndividualStreetAddress] t8
LEFT JOIN 
(SELECT IndividualID, MAX(StreetAddressID) AS StreetAddressID
 FROM [dbo].[vwPortal_IndividualStreetAddress]
 WHERE DeleteInd = 0 
 GROUP BY IndividualID ) t8
ON t7.IndividualID = t8.IndividualID
--And t8.DeleteInd = 0
Left JOIN [dbo].[vwPortal_StreetAddress] t9
ON t8.StreetAddressID = t9.StreetAddressID
--And t9.DeleteInd = 0
LEFT JOIN [dbo].[vwPortal_TerritoryType] t10
ON t9.[TerritoryTypeID] = t10.[TerritoryTypeID]
LEFT JOIN [dbo].[PatientContract] t11
ON t1.[LbPatientID] = t11.[LbPatientId]
AND [IsCurrentInd] = 1


---Modify by Youping
-- Date: June 1, 2016
-- Modification: Add up to 4 level of Id and IDtype description
-- used by Mike Hoxter 8/30/16 for hierarchy history build

 CREATE CLUSTERED INDEX IDX_PatientID_OrgHierarchy ON #OrgHierarchy (LbPatientId);



----get up to 4 level 
Create table #IdTypeDesc (
ID int identity(1,1),
IdTypeDesc Varchar(100)

);



DECLARE @IdTypeValue varchar(200)
SET  @IdTypeValue=( SELECT TOP 1 [SetValue]  FROM vwPortal_SystemSettings WHERE [SettingType]='Patient');

DECLARE @IdTypeDescCnt int
select @IdTypeDescCnt =1+len(@IdTypeValue)- len(replace(@IdTypeValue,',',''));

---select  @IdTypeValue,@IdTypeDescCnt;
DECLARE @I INT=1;

WHILE @I<= @IdTypeDescCnt
BEGIN
   INSERT INTO #IdTypeDesc (IdTypeDesc)
   SELECT dbo.UFN_SEPARATES_COLUMNS(@IdTypeValue,@I,',');
   SET @I=@I+1;
END

---select * from #IdTypeDesc


 --  SELECT  DISTINCT  a.LbPatientId,a.[ExternalId],left(a.IdTypeDesc + ' - '+s.Name,100) IdTypeDesc, b.ID as sortvalue, s.Name as SourceName
	----	,b.[ExternalId]  AS ExternalId2 ,CASE WHEN b.[ExternalId] is not NULL THEN @IdTypeDesc2 ELSE NULL END AS IdTypeDesc2
	----	,c.[ExternalId] AS ExternalId3, CASE WHEN c.[ExternalId] is not NULL THEN @IdTypeDesc3 ELSE NULL END AS IdTypeDesc3
	----	,d.[ExternalId] AS ExternalId4, CASE WHEN d.[ExternalId] is not NULL THEN @IdTypeDesc4 ELSE NULL END AS IdTypeDesc4
	--	  FROM [dbo].[PatientIdReference] a
	--	  INNER JOIN #IdTypeDesc b on a.IdTypeDesc=b.IdTypeDesc
	--	  INNER JOIN [dbo].[SourceSystem] s on s.SourceSystemId=a.SourceSystemId
	--	---		left outer join [dbo].[PatientIdReference] b ON a.LbPatientId=b.LbPatientId and b.IdTypeDesc=@IdTypeDesc2
	--	---		left outer join [dbo].[PatientIdReference] c ON a.LbPatientId=c.LbPatientId and c.IdTypeDesc=@IdTypeDesc3
	--	--	left outer join [dbo].[PatientIdReference] d ON a.LbPatientId=d.LbPatientId and d.IdTypeDesc=@IdTypeDesc4
	--	-- WHERE  a.[IdTypeDesc]=@IdTypeDesc1

	
	   SELECT  DISTINCT  a.LbPatientId,a.[ExternalId],left(a.IdTypeDesc + ' - '+s.Name,125) IdTypeDesc, b.ID as sortvalue
	   INTO #tmpPatientIdType
		  FROM [dbo].[PatientIdReference] a
		  INNER JOIN #IdTypeDesc b on a.IdTypeDesc=b.IdTypeDesc
		  INNER JOIN [dbo].[SourceSystem] s on s.SourceSystemId=a.SourceSystemId


	  SELECT LbPatientId,[ExternalId],IdTypeDesc, RowNbr
	  INTO #PatientIdType
	  FROM (
	  SELECT LbPatientId,[ExternalId],IdTypeDesc,sortvalue, row_number() OVER(PARTITION BY LbPatientID ORDER BY sortvalue ) AS RowNbr
	  FROM #tmpPatientIdType
	  ) a
	  WHERE a.RowNbr<=4;

	  CREATE CLUSTERED INDEX IDX_PatientIdType ON #PatientIdType (LbPatientId,RowNbr);


	  SELECT a.LbPatientId, a.ExternalId,a.IdTypeDesc
	  , b.ExternalId AS ExternalId2 ,b.IdTypeDesc AS IdTypeDesc2
	  , c.ExternalId AS ExternalId3 ,c.IdTypeDesc AS IdTypeDesc3
	  , d.ExternalId AS ExternalId4 ,d.IdTypeDesc AS IdTypeDesc4
	  INTO #PatientIdTypeQue
	  FROM #PatientIdType a
	  left outer join #PatientIdType b on b.LbPatientId=a.LbPatientId and b.rownbr=2 and  a.RowNbr=1
	  Left outer join #PatientIdType c on c.LbPatientId=a.LbPatientId and c.rownbr=3 and  a.RowNbr=1
	  Left outer join #PatientIdType d on d.LbPatientId=a.LbPatientId and d.rownbr=4 and  a.RowNbr=1
	  WHERE a.RowNbr=1

	  CREATE CLUSTERED INDEX IDX_PatientIdType ON #PatientIdTypeQue (LbPatientId);


DELETE FROM OrgHierarchyHistory WHERE CreatedFromMemberMonths = 1

INSERT INTO OrgHierarchyHistory (
   LbPatientId
  ,State
  ,City
  ,Zip
  ,Level1Id
  ,Level1Name
  ,Level1TypeName
  ,Level2Id
  ,Level2Name
  ,Level2TypeName
  ,Level3Id
  ,Level3Name
  ,Level3TypeName
  ,Level4Id
  ,Level4Name
  ,Level4TypeName
  ,Level5Id
  ,Level5Name
  ,Level5TypeName
  ,Level6Id
  ,Level6Name
  ,Level6TypeName
  ,ContractName
  ,MemberFullName
  ,Patient_Status
  ,ProviderId
  ,ProviderName
  ,ProviderNPI
  ,BirthDate
  ,GenderId
  ,LastUpdatedTimestamp
  ,PatientId1TypeName
  ,PatientId1
  ,PatientId2TypeName
  ,PatientId2
  ,PatientId3TypeName
  ,PatientId3
  ,PatientId4TypeName
  ,PatientId4
  ,CreatedFromMemberMonths
  ,CreateDate
  ,Modifydate
)
  SELECT DISTINCT o.LbPatientId
	    ,o.[State], o.City , o.[PostalCode]
		,o.[level1_buCode],o.[level1_buName],o.[level1_buType]
		,o.[level2_buCode],o.[level2_buName],o.[level2_buType]
		,o.[level3_buCode],o.[level3_buName],o.[level3_buType]
		,o.[level4_buCode],o.[level4_buName],o.[level4_buType]
		,o.[level5_buCode],o.[level5_buName],o.[level5_buType]
		,o.[level6_buCode],o.[level6_buName],o.[level6_buType]
		,o.ContractName,o.MemberFullName
    ,o.Patient_Status
		,o.providerID
		,o.ProviderName
	    ,o.ProviderNPI
	    ,o.BirthDate 
 	    ,o.[GenderTypeID] 
	    ,o.LastUpdatedTimestamp
        ,p.IdTypeDesc
        ,p.ExternalId
	    ,p.IdTypeDesc2
	    ,p.ExternalId2 
	    ,p.IdTypeDesc3
	    ,p.ExternalId3 
	    ,p.IdTypeDesc4
	    ,p.ExternalId4, 1 as CreatedFromMemberMonths, o.MemberMonth as CreateDateTime, o.MemberMonth as ModifyDateTime
  FROM #OrgHierarchy o 
  LEFT OUTER JOIN #PatientIdTypeQue p on o.LbPatientId=p.LbPatientId
  WHERE RowRank = 1

END
GO
