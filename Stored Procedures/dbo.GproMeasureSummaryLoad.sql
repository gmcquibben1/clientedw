SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproMeasureSummaryLoad]
	@BusinessUnitId INT = NULL
AS 
BEGIN
	DECLARE 
	 @BusinessUnitName VARCHAR(255) = (SELECT Name FROM vwPortal_BusinessUnit WHERE BusinessUnitId = @BusinessUnitId AND DeleteInd = 0)
	,@businessUnitNameList NVARCHAR(MAX) 
	,@businessUnitSelectList NVARCHAR(MAX)
	,@buinessUnitIdList NVARCHAR(MAX)
	,@SQL NVARCHAR(MAX)

	CREATE TABLE #TempBusinessUnitList (RowNumber INT IDENTITY PRIMARY KEY NOT NULL, Name VARCHAR(255) NULL)
	INSERT INTO #TempBusinessUnitList (Name) SELECT Name FROM vwPortal_BusinessUnit WHERE ParentBusinessUnitId = @BusinessUnitId AND DeleteInd = 0 group by [Name] order by [Name]
	INSERT INTO #TempBusinessUnitList (Name) SELECT @BusinessUnitName

	SET @businessUnitSelectList = 'MeasureTypeName, MeasureTypeDescription,' + STUFF((SELECT  ', ['+ Name +'] AS Facility' + CAST(RowNumber AS VARCHAR) FROM #TempBusinessUnitList FOR XML PATH('')),1,1, N'')
	DECLARE @NullPadding INT = (SELECT COUNT(RowNumber) FROM #TempBusinessUnitList) + 1
	DECLARE @x INT = 0
	WHILE @NullPadding < 26
	BEGIN
		SET @businessUnitSelectList = @businessUnitSelectList + ', '''' AS Facility' + CAST(@NullPadding AS VARCHAR)
		SET @NullPadding += 1
	END

	SET @businessUnitNameList = STUFF((SELECT  ', ['+ Name +']' FROM #TempBusinessUnitList FOR XML PATH('')),1,1, N'')
	SET @buinessUnitIdList =  STUFF((SELECT  ', ' + CAST(BusinessUnitId AS VARCHAR(50)) FROM vwPortal_BusinessUnit WHERE (ParentBusinessUnitId = @BusinessUnitId OR BusinessUnitId = @BusinessUnitId)AND DeleteInd = 0 group by [BusinessUnitId] order by [BusinessUnitId] FOR XML PATH('')),1,1, N'')

	SET @SQL = 
	'SELECT ' + @businessUnitSelectList + ' FROM (
		SELECT MeasureTypeName, MeasureTypeDescription, CompletedPercentMeasureMet, BusinessUnitName
		FROM GProMeasureProgressHistory WHERE BusinessUnitId IN  (' + @buinessUnitIdList + ')
	) src
	PIVOT(
		MAX(CompletedPercentMeasureMet) FOR BusinessUnitName IN (' + @businessUnitNameList + ')
	 ) piv;'
	EXECUTE sp_executesql @SQL
END

GO
