SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproMonitorLoad]
/*===============================================================
	CREATED BY: 	BG
	CREATED ON:		2016-11-17
	INITIAL VER:	2.2.1
	MODULE:			GPRO		
	DESCRIPTION:	Populate the GproPatientMonitor table with relevant clinical data to complete the GPRO measures
	PARAMETERS:		none
	RETURN VALUE(s)/OUTPUT:	none
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-11-17		BG			LBPP-2026	Initial version 
	2.2.1       2017-01-11      BG                      Added Codes for Med Rec 
	2.2.1       2017-01-11      BG                      Added new fields for Care 3 for Smoking 
	2.2.1       2017-01-12      BG                      Added new Fields for Depression Screening
	2.2.1       2017-01-11      BG                      Made a change to allow for claims to be used for FLu, Claims data is used when available to pre-populate the field used in the numerator for PREV-7 (influenza immunization).
	2.2.1       2017-01-19      BG                      Make chages to limit BMI data to Max encounter - 6
	2.2.1       2017-01-19      BG                      Added filters to be same day
	2.2.1       2017-01-26      BG	                    Removed the createtimestamp from social hx and only use recored date in for prev 10 

=================================================================*/
AS
BEGIN
    SET NOCOUNT ON;

	DECLARE @MeasurementYear varchar(4) = '2016';
	DECLARE @MeasurePeriodEnd DATETIME = '2017-01-01';
	DECLARE @MeasurePeriodBegin DATETIME = DateAdd( YEAR, -1, @MeasurePeriodEnd );
	DECLARE @Measureprogram varchar(25) = 'ACO Measures Program-2016';
    DECLARE @FluSeasonEnd   DATETIME = DateAdd(day, 89, @MeasurePeriodBegin); -- 3/31/2016 (spec specifically mentioned 89 days even for leap years)
    DECLARE @FluSeasonBegin DATETIME = DateAdd(day, -153 , @MeasurePeriodBegin); -- 9/1/2015
    DECLARE @HTNDiagEnd DATETIME = DateAdd( MONTH, -6, @MeasurePeriodEnd )
	DECLARE @BCSPeriodBegin datetime = dateadd(day, 1, dateadd(month, -27, @MeasurePeriodEnd))
	DECLARE @UseClinicalDataOnlyInd bit = 1;
	DECLARE @PHQ9INTSTART Datetime = '2014-12-01'
    DECLARE @PHQ9INTEND Datetime = '2015-11-30'
	DECLARE @Care2 int, @Care3 int, @CAD7 int, @DM2 int, @DM7 int, @HF6 int, @HTN2 int, @IVD2 Int, @MH1 int;
	DECLARE @Prev5 int, @Prev6 Int, @Prev7 int, @Prev8 int, @Prev9 int, @Prev10 int, @Prev11 int, @Prev12 int, @Prev13 int, @Prev13B int, @Prev13C int;
	----------------------------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------------------------

	--Get System Setting for if we shoudl use clinical data only for GPRO 
	SELECT @UseClinicalDataOnlyInd = setvalue
	FROM vwportal_systemsettings WHERE settingtype = 'GPRO' 
	AND settingparameter = 'ClinicalOnlyEnabled'

	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @UseClinicalDataOnlyInd = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = @UseClinicalDataOnlyInd;
	END


	--Create a temp table to hold GPROValueSetCodes for the measure 
	IF OBJECT_ID( 'tempdb..#GPROMeasureIdAndNames' ) IS NOT NULL DROP TABLE [#GPROMeasureIdAndNames];
	CREATE TABLE [#GPROMeasureIdAndNames]
	(
		Measureid int,
		MeasureName varchar(200)
	);
  
	INSERT INTO [#GPROMeasureIdAndNames]
	(
		Measureid ,
		MeasureName      
	)

	SELECT MeasureID ,
		   MeasureName 
	FROM   Cmeasure_definitions
	WHERE  Measureprogram = @Measureprogram
	ORDER BY measureabbr


	SELECT @Care2 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Falls: Screening for Fall Risk ACO #13'
	SELECT @Care3 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Documentation of Current Medications in the Medical Record ACO #39'
	SELECT @CAD7 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Angiotensin-Converting Enzyme (ACE) Inhibitor or Angiotensin Receptor Blocker (ARB) Therapy - Diabetes or Left Ventricular Systolic Dysfunction (LVEF < 40%) ACO #33'
	SELECT @DM2 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Diabetes Mellitus: Hemoglobin A1c Poor Control (>9.0%) ACO #27'
	SELECT @DM7 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Diabetic Retinal Screening ACO #41'
	SELECT @HF6 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Heart Failure: Beta-Blocker Therapy for Left Ventricular Systolic Dysfunction (LVSD) ACO #31'
	SELECT @HTN2 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Hypertension (HTN): Blood Pressure Control ACO #28'
	SELECT @IVD2 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Ischemic Vascular Disease (IVD): Use of Aspirin or Another Antithrombotic ACO #30'
	SELECT @MH1 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Deprssion Remission at Twelve Months'
	SELECT @Prev5 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Mammography Screening ACO #20'
	SELECT @Prev6  = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Colorectal Cancer Screening ACO #19'
	SELECT @Prev7 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Influenza Immunization ACO #14'
	SELECT @Prev8 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Pneumococcal Vaccination ACO #15'
	SELECT @Prev9 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'BMI Screening and Follow Up'
	SELECT @Prev10  = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Tobacco Use Assessment and Tobacco Cessation Intervention ACO #17'
	SELECT @Prev11 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Proportion of Adults 18+ who had their Blood Pressure and a recommended follow-up plan is documented ACO #21'
	SELECT @Prev12 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Depression Screening ACO #18'
	SELECT @Prev13 = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Statin Therapy for the Prevention and Treatment of Cardiovascular Disease ASCVD ACO #42'
	SELECT @Prev13B = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Statin Therapy for the Prevention and Treatment of Cardiovascular Disease LDL ACO #42'
	SELECT @Prev13C = measureid FROM [#GPROMeasureIdAndNames] WHERE measurename = 'Statin Therapy for the Prevention and Treatment of Cardiovascular Disease DIAB  ACO #42'
	
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--Create a temp table to hold GPROValueSetCodes for the measure 
	IF OBJECT_ID( 'tempdb..#GPROValueSetCodes' ) IS NOT NULL DROP TABLE [#GPROValueSetCodes];
	CREATE TABLE [#GPROValueSetCodes]
	(
		ValueSetName varchar(50),
		CodeSystem varchar(50),
		Code varchar(50),
	);
  
	INSERT INTO [#GPROValueSetCodes]
	(
		ValueSetName ,
		CodeSystem ,
		Code 
	)

	SELECT VariableName ,
		CodeSystem ,
		Code
	--FROM [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	FROM vwGProValueSet
	WHERE   Year = @MeasurementYear

	-----------------------------------------------------------------------------------------------------------------------------------------
   --Create a temp table to hold GPROValueSetCodes for the measure 
	IF OBJECT_ID( 'tempdb..#GPROEncValueSetCodes' ) IS NOT NULL DROP TABLE [#GPROEncValueSetCodes];
	CREATE TABLE [#GPROEncValueSetCodes]
	(
		ValueSetName varchar(50),
		CodeSystem varchar(50),
		Code varchar(50),
	);
  
	INSERT INTO [#GPROEncValueSetCodes]
	(
		ValueSetName ,
		CodeSystem ,
		Code 
	)

	SELECT VariableName ,
	   CodeSystem ,
	   Code
	--FROM [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	FROM vwGProValueSet
	WHERE   Year = @MeasurementYear AND   [ModuleType] = 'PREV' AND [ModuleIndicator] = '7' 
	AND VariableName IN ('ENCOUNTER_CODE')


	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	IF OBJECT_ID( 'tempdb..#GproPatientMonitor' ) IS NOT NULL DROP TABLE [#GproPatientMonitor];
	CREATE TABLE #GproPatientMonitor
	(
		lbpatientid int not null,
		Memberfullname varchar(350),
		PatientAge int,
		PatientStatus varchar(50) null,
		ContractName varchar(100) null,
		Gender tinyint null,
		BirthDate date null, 
		Prev7EligibleInd bit,
		InfluenzaLastPerformedDate datetime null,
		InfluenzaCode varchar(15) null,
		InfluenzaDueDate datetime null,
		Prev7Numerator bit,
		Prev7Exclusion bit,
		Prev8EligibleInd bit,
		PnueVaxLastPerformedDate datetime null,
		PnueVaxCode varchar(15) null,
		PnueVaxDueDate datetime null,
		Prev8Numerator bit,
		Prev8Exclusion bit,
		Prev5EligibleInd bit,
		BreastExamLastPerformedDate datetime null,
		BreastExamCode varchar(15) null,
		BreastExamDueDate datetime null,
		Prev5Numerator bit,
		Prev5Exclusion bit,
		Prev6EligibleInd bit,
		ColonExamLastPerformedDate datetime null,
		ColonExamDueDate datetime null,
		ColonExamCode varchar(15) null,
		Prev6Numerator bit,
		Prev6Exclusion bit,
		DiabeticPatientEligibleInd bit null,
		DiabeticDate datetime null,
		DiabeticCode varchar(50),
		DM2EligibleInd bit,
		HemoglobinA1cTestLastPerformedDate datetime null,
		HemoglobinA1cTestCode varchar(15) null,
		HemoglobinA1cTestValue varchar(50) null,
		HemoglobinA1cTestDueDate datetime null,
		DM2Numerator bit,
		DM2Exclusion bit,
		Prev9EligibleInd bit,
		BMILastReadingDate datetime null,
		BMILastReadingHeight decimal(5,2),
		BMILastReadingWeight decimal(5,2),
		BMIReadingValue decimal(5,2),
		BMINormal smallint NULL,
		BMIFollowupCode varchar(15) NULL,
		BMIFollowupDate datetime NULL,
		Prev9Numerator bit,
		Prev9Exclusion bit,
		HTN2EligibleInd bit,
		HypertensionDate datetime NULL,
		HypertensionCode varchar(50) NULL,
		HTN2Numerator bit,
		HTN2Exclusion bit,
		DM7EligibleInd bit,
		DiabeticEyeDateLastPerformed datetime NULL,
		DiabeticEyeCode varchar(20) NULL,
		DM7Numerator bit,
		DM7Exclusion bit,
		BloodPressureReadingDate datetime null,
		BloodPressureSystolic smallint null,
		BloodPressureDiastolic smallint null,
		BloodPressureNormal smallint null,
		BloodPressureFollowupCode varchar(20) null,
		BloodPressureFollowupDate datetime null,
		HF6EligibleInd bit,
		HeartFailureDate datetime null,
		HeartFailureCode varchar(20) NULL,
		HF6Numerator bit,
		HF6Exclusion bit,
		Care2EligibleInd bit,
		FallScreenCode varchar(20) NULL,
		FallScreenDate datetime NULL,
		FallScreenResult varchar(20) NULL,
		Care2Numerator bit,
		Care2Exclusion bit,
		DepressionPatientEligibleInd bit,
		DepressionCode varchar(20) NULL,
		DepressionDate datetime NULL,
		DepressionLatestPHQ9Code varchar(20) NULL,
		DepressionLatestPHQ9ScreeningDate datetime NULL,
		DepressionLatestPHQ9ScreenScore varchar(20) NULL,
		DepressionInitialPHQ9Code varchar(20),
		DepressionInitialPHQ9ScreeningDate datetime, 
		DepressionInitialPHQ9ScreenScore varchar(20),
		LVSDPatientInd bit,
		LVSDCode varchar(20) NULL,
		LVSDDate datetime NULL,
		LVSDResult varchar(25) NULL,
		PatientOnACEorARB bit,
		ACEorARBStartDate datetime NULL,
		ACEorARBCode varchar(20) NULL,
		CAD7EligibleInd bit,
		CADCode varchar(20) null,
		CADDate datetime NULL,
		CAD7Numerator bit,
		CAD7Exclusion bit,
		PatientOnAspirin bit,
		AspirinStartDate datetime NULL,
		AspirinCode varchar(20) NULL,
		PatientOnBetaBlocker bit,
		BetaBlockerStartDate datetime NULL,
		BetaBlockerCode varchar(20) NULL,
		PatientOnStatin bit,
		StatinStartDate datetime NULL,
		StatinCode varchar(20) NULL,
		PREV13EligibleInd bit,
		PREV13Numerator bit,
		PREV13Exclusion bit,
		PREV13BEligibleInd bit,
		PREV13BNumerator bit,
		PREV13BExclusion bit,
		PREV13CEligibleInd bit,
		PREV13CNumerator bit,
		PREV13CExclusion bit,
		ASCVDPatientEligibleInd bit,
		ASCVDCode Varchar(20),
		ASCVDDate Datetime,
		LDL190PatientEligibleInd bit,
		LDL70189PatientEligibleInd bit,
		IVDEligibleInd bit,
		IVDNumerator bit,
		IVDExclusion bit,
		MH1EligibleInd bit,
		MH1Numerator bit,
		MH1Exclusion bit,
		PREV10EligibleInd bit,
		PREV10Numerator bit,
		PREV10Exclusion bit,
		PREV10TobaccoScreeningDate datetime Null,
		PREV10TobaccoScreeningCode varchar(20) Null,
		PREV10TobaccoCounselingDate datetime Null,
		PREV10TobaccoCounselingCode varchar(20) Null,
		PatientIsSmokerInd bit Null,
		PREV11EligibleInd bit,
		PREV11Numerator bit,
		PREV11Exclusion bit,
		PREV12EligibleInd bit,
		PREV12Numerator bit,
		PREV12Exclusion bit,
		Care3EligibleInd bit,
		Care3Numerator bit,
		Care3Exclusion bit,
		DepressionScreeningPatientEligibleInd bit,
	    DepressionScreenCode varchar(20) null,
	    DepressionScreenDate Datetime null,
		DepressionScreenPositiveInd bit,
		DepressionScreenFollowUpInd bit,
		DepressionScreenFollowUpCode varchar(20),
		DepressionScreenFollowUpDate DateTime,
		LastEncounterDate datetime NULL,
		LastEncounterCode varchar(20) NULL,
		SecondLastEncounterDate datetime NULL,
		SecondlastEncounterCode varchar(20) NULL,
		CreateLbUserId int not null,
		ModifyLbUserId int not null,
		createdatetime datetime not null,
		modifydatetime datetime not null
	);

	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempFlu' ) IS NOT NULL DROP TABLE [#TempFlu];
	CREATE TABLE #TempFlu
	(
		[LbPatientId]       [int] NOT NULL,
		ProcedureDate datetime,
		ProcedureCode varchar(15)
	);


	-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempPneumonia' ) IS NOT NULL DROP TABLE [#TempPneumonia];
	CREATE TABLE #TempPneumonia
	(
		[LbPatientId]       [int] NOT NULL,
		ProcedureDate datetime,
		ProcedureCode varchar(15)
	);

	-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempColonScreening' ) IS NOT NULL DROP TABLE [#TempColonScreening];
	CREATE TABLE #TempColonScreening
	(
		[LbPatientId]       [int] NOT NULL,
		ProcedureDate datetime,
		ProcedureCode varchar(15)
	);


	-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempBreastScreening' ) IS NOT NULL DROP TABLE [#TempBreastScreening];
	CREATE TABLE #TempBreastScreening
	(
		[LbPatientId]       [int] NOT NULL,
		ProcedureDate datetime,
		ProcedureCode varchar(15)
	);
		

			-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempDiabetic' ) IS NOT NULL DROP TABLE [#TempDiabetic];
	CREATE TABLE #TempDiabetic
	(
		[LbPatientId]       [int] NOT NULL,
		DiagnosisDate datetime,
		DiagnosisCode varchar(20)
	);


			-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempDiabeticEye' ) IS NOT NULL DROP TABLE [#TempDiabeticEye];
	CREATE TABLE #TempDiabeticEye
	(
		[LbPatientId]       [int] NOT NULL,
		ProcedureDate datetime,
		ProcedureCode varchar(20)
	);


			-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempHTN' ) IS NOT NULL DROP TABLE [#TempHTN];
	CREATE TABLE #TempHTN
	(
		[LbPatientId]       [int] NOT NULL,
		HypertensionDate datetime,
		HypertensionCode varchar(20)
	);


				-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#HeartFailure' ) IS NOT NULL DROP TABLE [#HeartFailure];
	CREATE TABLE #HeartFailure
	(
		[LbPatientId]       [int] NOT NULL,
		HeartFailureDate datetime,
		HeartFailureCode varchar(20)
	);


	
			-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempHemoglobinA1c' ) IS NOT NULL DROP TABLE [#TempHemoglobinA1c];
	CREATE TABLE #TempHemoglobinA1c
	(
		[LbPatientId]       [int] NOT NULL,
		TestDate datetime,
		TestCode varchar(15),
		TestValue varchar(50)
	);


				-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempBMI' ) IS NOT NULL DROP TABLE [#TempBMI];
	CREATE TABLE #TempBMI
	(
		[LbPatientId]       [int] NOT NULL,
		TestDate datetime,
		PTHeightCM decimal(8,2),
		PTWeightKG decimal(8,2),
		BMIReadingValue decimal(6,2)		
	);


	IF OBJECT_ID( 'tempdb..#TempBloodPressure' ) IS NOT NULL DROP TABLE [#TempBloodPressure];
	CREATE TABLE #TempBloodPressure
	(
		[LbPatientId]       [int] NOT NULL,
		BPReadingDate datetime,
		BloodPressureSystolic smallint,
		BloodPressureDiastolic smallint,
	);


		 --Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempLastEncounter' ) IS NOT NULL DROP TABLE [#TempLastEncounter];
	CREATE TABLE #TempLastEncounter
	(
		[LbPatientId] [int] NOT NULL,
		ProcedureDate datetime,
		ProcedureCode varchar(20)
	);


			 --Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempAllFallScreening' ) IS NOT NULL DROP TABLE [#TempAllFallScreening];
	CREATE TABLE #TempAllFallScreening
	(
		[LbPatientId]       [int] NOT NULL,
		FallScreenCode varchar(20),
		FallScreenDate datetime,
		FallScreenResult varchar(20)
	);

				 --Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempFallScreen' ) IS NOT NULL DROP TABLE [#TempFallScreen];
	CREATE TABLE #TempFallScreen
	(
		[LbPatientId]       [int] NOT NULL,
		FallScreenCode varchar(20),
		FallScreenDate datetime,
		FallScreenResult varchar(20)
	);


			-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempAllPHQ9DepressionScreen' ) IS NOT NULL DROP TABLE [#TempAllPHQ9DepressionScreen];
	CREATE TABLE #TempAllPHQ9DepressionScreen
	(
		[LbPatientId]       [int] NOT NULL,
		DepressionScreenDate datetime,
		DepressionScreenCode varchar(20),
		DepressionScreenScore varchar(20)
	);

			-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#DepressionScreen' ) IS NOT NULL DROP TABLE [#DepressionScreen];
	CREATE TABLE #DepressionScreen
	(
		[LbPatientId]       [int] NOT NULL,
		DepressionEligibleInd bit,
		DepressionScreenCode varchar(20),
		DepressionScreenDate datetime,
		DepressionScreenScore varchar(20)
	);


	-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#DepressionPatients' ) IS NOT NULL DROP TABLE [#DepressionPatients];
	CREATE TABLE #DepressionPatients
	(
		[LbPatientId]       [int] NOT NULL,
		DepressionDate datetime,
		DepressionCode varchar(20)		
	);



			-- Create a temp table to hold intermediate results.
	IF OBJECT_ID( 'tempdb..#TempCAD' ) IS NOT NULL DROP TABLE [#TempCAD];
	CREATE TABLE #TempCAD
	(
		[LbPatientId]       [int] NOT NULL,
		CADDate datetime,
		CADCode varchar(20)
	);


	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--Getting a list of all patients that can be used for the health monitor
	insert into #GproPatientMonitor
	(lbpatientid,Memberfullname,PatientAge,PatientStatus,ContractName,Gender,BirthDate, 
	Prev7EligibleInd,InfluenzaLastPerformedDate,InfluenzaCode,InfluenzaDueDate,Prev7Numerator,Prev7Exclusion,
	Prev8EligibleInd,PnueVaxLastPerformedDate,PnueVaxCode,PnueVaxDueDate,Prev8Numerator,Prev8Exclusion,
	Prev5EligibleInd,BreastExamLastPerformedDate,BreastExamCode,BreastExamDueDate,Prev5numerator,Prev5Exclusion,
	Prev6EligibleInd,ColonExamLastPerformedDate,ColonExamCode,ColonExamDueDate,Prev6numerator,Prev6Exclusion,
	DiabeticPatientEligibleInd,DiabeticDate,DiabeticCode,
	DM2EligibleInd,HemoglobinA1cTestLastPerformedDate,HemoglobinA1cTestCode,HemoglobinA1cTestValue,HemoglobinA1cTestDueDate,DM2Numerator,DM2Exclusion,
	DM7EligibleInd,DiabeticEyeDateLastPerformed,DiabeticEyeCode,DM7Numerator,DM7Exclusion,
	Prev9EligibleInd,BMILastReadingDate,BMILastReadingHeight,BMILastReadingWeight,BMIReadingValue,BMINormal,BMIFollowupCode,BMIFollowupDate, Prev9Numerator,Prev9Exclusion,
	HTN2EligibleInd,HypertensionDate,HypertensionCode,HTN2Numerator,HTN2Exclusion,
	BloodPressureReadingDate,BloodPressureSystolic,BloodPressureDiastolic,BloodPressureNormal,BloodPressureFollowupCode, BloodPressureFollowupDate,
	HF6EligibleInd,HeartFailureDate,HeartFailureCode,HF6Numerator,HF6Exclusion,
	Care2EligibleInd,FallScreenCode,FallScreenDate,FallScreenResult,Care2Numerator,Care2Exclusion,
	DepressionPatientEligibleInd,DepressionCode,DepressionDate,DepressionLatestPHQ9Code,DepressionLatestPHQ9ScreeningDate,DepressionLatestPHQ9ScreenScore,
	DepressionInitialPHQ9Code, DepressionInitialPHQ9ScreeningDate, DepressionInitialPHQ9ScreenScore ,
	LVSDPatientInd,LVSDCode,LVSDDate,LVSDResult,
	PatientOnACEorARB, ACEorARBStartDate, ACEorARBCode,
	CAD7EligibleInd,CADCode,CADDate,CAD7Numerator,CAD7Exclusion,
	PatientOnAspirin,AspirinStartDate,AspirinCode,
	PatientOnBetaBlocker,BetaBlockerStartDate,BetaBlockerCode,
	PatientOnStatin,StatinStartDate,StatinCode,
	PREV13EligibleInd,PREV13Numerator,PREV13Exclusion,
	PREV13BEligibleInd,PREV13BNumerator, PREV13BExclusion,
	PREV13CEligibleInd, PREV13CNumerator, PREV13CExclusion,
	ASCVDPatientEligibleInd,ASCVDCode,ASCVDDate,
	LDL190PatientEligibleInd,LDL70189PatientEligibleInd,
	IVDEligibleInd, IVDNumerator, IVDExclusion,
	MH1EligibleInd, MH1Numerator, MH1Exclusion,
	PREV10EligibleInd,PREV10Numerator,PREV10Exclusion,PREV10TobaccoScreeningDate,PREV10TobaccoScreeningCode,PREV10TobaccoCounselingDate,PREV10TobaccoCounselingCode,PatientIsSmokerInd,
	PREV11EligibleInd,PREV11Numerator,PREV11Exclusion,
	PREV12EligibleInd,PREV12Numerator,PREV12Exclusion,
	Care3EligibleInd, Care3Numerator, Care3Exclusion,
	DepressionScreeningPatientEligibleInd,DepressionScreenCode, DepressionScreenDate,
	DepressionScreenPositiveInd,DepressionScreenFollowUpInd,DepressionScreenFollowUpCode,DepressionScreenFollowUpDate,
	LastEncounterDate,
	LastEncounterCode,
	SecondLastEncounterDate,
	SecondlastEncounterCode,
	CreateLbUserId,
	ModifyLbUserId,
	createdatetime,
	modifydatetime)
	SELECT oh.lbpatientid,Memberfullname,DATEDIFF(hour,oh.birthdate,GETDATE())/8766 as patientage, patient_status,contractname,genderid,oh.birthdate,
	--Influenza PREV 7
	0,Null,Null,Null,0,0,
	--PnueVax PREV 8 
	0,Null,Null,Null,0,0,
	--BreastExam PREV 5
	0,Null,Null,Null,0,0,
	--ColonExam PREV 6
	0,Null,Null,Null,0,0,
	--Diabetic
	0,Null,Null,
	--HemoglobinA1c DM-2
	0,Null,Null,Null,Null,0,0,
	--Diabetic EYE DM-7
	0,Null,Null,0,0,
	--BMI Prev 9
	0,Null,Null,Null,Null,Null,Null,Null,0,0,
	--Hypertension
	0,Null,Null,0,0,
	--Blood Preasure Prev 11
	Null,Null,Null,Null,Null,Null,
	--Heart Failure
	0,Null,Null,0,0,
	--FallScreen Care 2
	0,Null,Null,Null,0,0,
	--Depression
	0,Null,Null,Null,Null,Null,Null,Null,Null,
	--LVSD
	0,Null,Null,Null,
	--ACE and ARB
	0, Null, Null,
	--CAD
	0,Null,Null,0,0,
	--Aspirin
	0,Null,Null,
	--Beta Blocker
	0,Null,Null,
	--Statin 
	0,Null,Null,
	--Prev 13 Statin Therapy
	0,0,0,
	--Prev 13C Statin Therapy
	0,0,0,
	--Prev 13C Statin Therapy
	0,0,0,
	--ASCVD
	0,Null,Null,
	--LDL
	0,0,
	--IVD 2
	0, 0, 0,
	--MH1
	0, 0, 0,
	--PREV10
	0, 0, 0,Null,Null,Null,Null,Null,
	--PREV11
	0, 0, 0,
	--PREV12
	0, 0, 0,
	--Care 3
	0,0,0,
	--Depression Screening
	0,Null,Null,0,0,Null,Null,
	--LastEncounter
	Null,Null,Null,Null,
	--User ID and Modifytimestamp
	1,1,getdate(),getdate()
	FROM orghierarchy OH
	JOIN GproPatientRanking GPR
	ON OH.Lbpatientid = GPR.Lbpatientid


	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--Fill in Last Encounter for Patient

	Insert into #TempLastEncounter
	(LbPatientId,ProcedureDate,ProcedureCode)
	SELECT 
	[PatientID],
	isnull(procedureDateTime,'2016-01-01'),
	ProcedureCode
	FROM vwpatientprocedure pp
	JOIN #GproPatientMonitor 
		 ON #GproPatientMonitor.lbpatientid = pp.patientid
	JOIN [#GPROEncValueSetCodes] AVC 
	ON pp.procedurecode = AVC.Code
	AND AVC.ValueSetName  IN ('ENCOUNTER_CODE','PREVENTIVE_CARE_VISITS')
	AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
	AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID];
 

 
	WITH LastEncounter AS  
	(  
	SELECT 
		LBPatientid,ProcedureDate,ProcedureCode,
		ROW_NUMBER() OVER (Partition by LBPatientid ORDER BY ProcedureDate DESC) AS RowNumber
	FROM #TempLastEncounter pp
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.LastEncounterCode = LastEncounter.ProcedureCode,
	#GproPatientMonitor.LastEncounterDate = LastEncounter.ProcedureDate
	FROM #GproPatientMonitor, LastEncounter 
	WHERE #GproPatientMonitor.lbpatientid = LastEncounter.lbpatientid  
	AND  RowNumber = 1;


	WITH LastEncounter AS  
	(  
	SELECT 
		LBPatientid,ProcedureDate,ProcedureCode,
		ROW_NUMBER() OVER (Partition by LBPatientid ORDER BY ProcedureDate DESC) AS RowNumber
	FROM #TempLastEncounter pp
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.SecondlastEncounterCode = LastEncounter.ProcedureCode,
	#GproPatientMonitor.SecondLastEncounterDate = LastEncounter.ProcedureDate
	FROM #GproPatientMonitor, LastEncounter 
	WHERE #GproPatientMonitor.lbpatientid = LastEncounter.lbpatientid  
	AND  RowNumber = 2;


	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--Care 2 Fall Screening
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Care2EligibleInd = 1,
	#GproPatientMonitor.Care2Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.Care2Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Care2;


	DELETE FROM  [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'CARE' and [ModuleIndicator] = '2' and Year = '2016'




	Insert into #TempAllFallScreening
	(LbPatientId,	FallScreenCode,FallScreenDate)
	SELECT 	Patientid,ProcedureCode,ProcedureDatetime
	FROM vwpatientprocedure pp
	JOIN #GproPatientMonitor 
		 ON #GproPatientMonitor.lbpatientid = pp.patientid
		JOIN [#GPROValueSetCodes] AVC 
		ON pp.procedurecode = AVC.Code
		AND AVC.ValueSetName  IN ('FALLS_SCREEN_CODE')
			AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
			AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]
	UNION
	SELECT plo.patientid, plr.ObservationCode1, plr.ObservationDate
	FROM [PatientLabOrder] plo
		JOIN [PatientLabResult] plr
			ON  plo.[PatientLabOrderID] = plr.[PatientLabOrderID]
		JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = plo.patientid
		JOIN [#GPROValueSetCodes] AVC
			ON plr.[ObservationCode1] = AVC.[Code]
			AND AVC.ValueSetName = 'FALLS_SCREEN_CODE'
			AND plr.[ObservationDate] >= @MeasurePeriodBegin
			AND plr.[ObservationDate] <  @MeasurePeriodEnd
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = plo.[SourceSystemID];


	WITH FallScreen AS  
	(  
	SELECT 
		LbPatientId,FallScreenCode,FallScreenDate,
		ROW_NUMBER() OVER (Partition by LbPatientId ORDER BY FallScreenDate DESC) AS RowNumber
	FROM #TempAllFallScreening 
	)   
	Insert into #TempFallScreen
	(LbPatientId,FallScreenCode,FallScreenDate) 
	SELECT LbPatientId,FallScreenCode,FallScreenDate
	FROM FallScreen   
	WHERE RowNumber = 1;



	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.FallScreenDate = #TempFallScreen.FallScreenDate,
	#GproPatientMonitor.FallScreenCode = #TempFallScreen.FallScreenCode
	FROM #GproPatientMonitor, #TempFallScreen 
	WHERE #GproPatientMonitor.lbpatientid = #TempFallScreen.lbpatientid;

	IF OBJECT_ID( 'tempdb..#TempAllFallScreening' ) IS NOT NULL DROP TABLE [#TempAllFallScreening];
	IF OBJECT_ID( 'tempdb..#TempFallScreening' ) IS NOT NULL DROP TABLE [#TempFallScreening];


	--SELECT lbpatientid,Care2EligibleInd,FallScreenCode,FallScreenDate,Care2Numerator,Care2Exclusion,LastEncounterDate,
	--LastEncounterCode,SecondLastEncounterDate,SecondlastEncounterCode 
	--FROM  #GproPatientMonitor WHERE Care2EligibleInd =1

---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
	
	--Care 3 Smoking
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Care3EligibleInd = 1,
	#GproPatientMonitor.Care3Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.Care3Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Care3;

	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--Prev 5 Breast Cancer Screning
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev5EligibleInd = 1,
	#GproPatientMonitor.Prev5Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.Prev5Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev5;


	DELETE FROM  [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'PREV' and [ModuleIndicator] = '5' and Year = @MeasurementYear;


	Insert into #TempBreastScreening
	SELECT [PatientID],ProcedureDateTime,ProcedureCode
	FROM vwpatientprocedure pp
	JOIN #GproPatientMonitor  GPM
	ON GPM.lbpatientid = pp.patientid
	JOIN [#GPROValueSetCodes] AVC 
	ON pp.procedurecode = AVC.Code
	AND AVC.ValueSetName  IN ('MAMMO_CODE')
	AND PP.ProcedureDateTime >= (CASE     -- Patient is 52 or older by the end of the reporting period.  Use a lookback period of 27 months.
                                WHEN GPM.[BirthDate] <= DateAdd( YEAR, -52, @MeasurePeriodBegin ) 
                                THEN @BCSPeriodBegin
                                ELSE DATEADD(YEAR,50,GPM.[BirthDate])				-- Patient is 50 or 51 years old, only look back to their 50th birthday.
                                END)
            AND PP.ProcedureDateTime <= @MeasurePeriodEnd
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]
	UNION
		SELECT PLO.Patientid,PLR.ObservationDate,PLR.ObservationCode1
		FROM DBO.PatientLabOrder PLO
			JOIN DBO.PatientLabResult PLR
			ON PLO.PatientLabOrderId = PLR.PatientLabOrderId
			JOIN #GproPatientMonitor GPM
			ON GPM.lbpatientid = plo.patientid
			JOIN [#GPROValueSetCodes] GVSC  
			ON  PLR.ObservationCode1 = GVSC.CODE
			AND GVSC.[ValueSetName] = ('MAMMO_CODE')
			 AND  PLR.ObservationDate  >=  (CASE     -- Patient is 52 or older by the end of the reporting period.  Use a lookback period of 27 months.
                                        			   WHEN GPM.[BirthDate] <= DateAdd( YEAR, -52, @MeasurePeriodBegin ) 
                                                       THEN @BCSPeriodBegin
                                  			           ELSE DATEADD(YEAR,50,GPM.[BirthDate])				-- Patient is 50 or 51 years old, only look back to their 50th birthday.
                                  			           END)	
            AND  PLR.ObservationDate <= @MeasurePeriodEnd
    		JOIN @SourceIdTable SS  ON SS.[SourceSystemId] = plo.[SourceSystemID];


	WITH BreastScreening AS  
	(  
	SELECT 
		LBPatientid,ProcedureDate,ProcedureCode,
		ROW_NUMBER() OVER (Partition by LBPatientid ORDER BY ProcedureDate DESC) AS RowNumber
	FROM #TempBreastScreening pp
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.BreastExamCode = BreastScreening.ProcedureCode,
	#GproPatientMonitor.BreastExamLastPerformedDate = BreastScreening.ProcedureDate
	FROM #GproPatientMonitor, BreastScreening 
	WHERE #GproPatientMonitor.lbpatientid = BreastScreening.lbpatientid  
	AND #GproPatientMonitor.Gender = 2
	AND  RowNumber = 1;


	--SELECT lbpatientid,	Memberfullname,	PatientAge,	PatientStatus,	ContractName,	Gender,	BirthDate,
	--Prev5EligibleInd,	BreastExamLastPerformedDate,	BreastExamCode,	BreastExamDueDate,	Prev5Numertor,	Prev5Exclusion
	--FROM #GproPatientMonitor

	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--Prev 6 Colon Screening

	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev6EligibleInd = 1,
	#GproPatientMonitor.Prev6Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.Prev6Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev6;


	DELETE FROM  [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'PREV' and [ModuleIndicator] = '6' and Year = @MeasurementYear;



	Insert into #TempColonScreening
	SELECT pp.patientid,pp.proceduredatetime,pp.procedurecode
	FROM vwPatientProcedure pp
			JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pp.patientid
			join [#GPROValueSetCodes] GVSC
			on pp.[ProcedureCode] = GVSC.[Code]
			AND  GVSC.[ValueSetName] IN ( 'FLEX_SIG_CODE','FOBT_CODE','COLONOSCOPY_CODE')
			AND pp.[ProcedureDateTime] >=
						CASE GVSC.[ValueSetName]
							WHEN 'FOBT_CODE' THEN DateAdd( YEAR, -1, @MeasurePeriodEnd )
							WHEN 'FLEX_SIG_CODE' THEN DateAdd( YEAR, -5, @MeasurePeriodEnd )
							WHEN 'COLONOSCOPY_CODE' THEN DateAdd( YEAR, -10, @MeasurePeriodEnd )
						END
       		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID] 
	UNION	
		SELECT PLO.Patientid,PLR.ObservationDate,PLR.ObservationCode1
		FROM DBO.PatientLabOrder PLO
			JOIN DBO.PatientLabResult PLR
				ON PLO.PatientLabOrderId = PLR.PatientLabOrderId
			AND plr.[ObservationDate] >= @MeasurePeriodBegin
			AND plr.[ObservationDate] <  @MeasurePeriodEnd
			JOIN #GproPatientMonitor 
				ON #GproPatientMonitor.lbpatientid = plo.patientid
			JOIN [#GPROValueSetCodes] GVSC  
				ON  PLR.ObservationCode1 = GVSC.CODE
			AND GVSC.[ValueSetName] = ('FOBT_CODE')
    		JOIN @SourceIdTable SS  ON SS.[SourceSystemId] = plo.[SourceSystemID];


	WITH ColonScreening AS  
	(  
	SELECT 
		LbPatientid,ProcedureDate,ProcedureCode,
		ROW_NUMBER() OVER (Partition by LBPatientid ORDER BY ProcedureDate DESC) AS RowNumber
	FROM #TempColonScreening 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.ColonExamCode = ColonScreening.ProcedureCode,
	#GproPatientMonitor.ColonExamLastPerformedDate = ColonScreening.ProcedureDate
	FROM #GproPatientMonitor, ColonScreening 
	WHERE #GproPatientMonitor.lbpatientid = ColonScreening.lbpatientid  
	AND  RowNumber = 1;


	--SELECT lbpatientid,	Memberfullname,	PatientAge,	PatientStatus,	ContractName,	Gender,	BirthDate,
	--Prev6EligibleInd,	ColonExamLastPerformedDate,	ColonExamCode,	Prev6Numertor,	Prev6Exclusion
	--FROM #GproPatientMonitor


	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--Prev 7 - Flu shot is for patients 6 months and older
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.PREV7EligibleInd = 1,
	#GproPatientMonitor.Prev7Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.PREV7Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev7



	DELETE FROM  [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'PREV' and [ModuleIndicator] = '7' and Year = @MeasurementYear;



	INSERT INTO #TempFlu
	SELECT patientid,proceduredatetime,procedurecode
	FROM vwpatientprocedure pp
	JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pp.patientid
	JOIN [#GPROValueSetCodes] AVC 
		ON pp.procedurecode = AVC.Code
	AND AVC.ValueSetName  IN ('INFLUENZA_CODE','PREVIOUS_RECEIPT_CODE')
	AND pp.[ProcedureDateTime] >= @FluSeasonBegin
	AND pp.[ProcedureDateTime] <= @FluSeasonEnd
	--JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID] 
	UNION
	SELECT patientid,servicedate,ImmunizationCode
		FROM [DBO].[VWPATIENTIMMUNIZATION] PIMM
		JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pimm.patientid
		JOIN [#GPROValueSetCodes] AVC 
		    ON  PIMM.ImmunizationCode = AVC.CODE                   
	AND AVC.[ValueSetName] in ('INFLUENZA_CODE','PREVIOUS_RECEIPT_CODE')
	AND PIMM.[ServiceDate] >= @FluSeasonBegin
	AND PIMM.[ServiceDate] <= @FluSeasonEnd;
	--JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pimm.[SourceSystemID] ;


	WITH InfluenzaImmunization AS  
	(  
	SELECT 
		LbPatientid,ProcedureDate,ProcedureCode,
		ROW_NUMBER() OVER (Partition by LBPatientid ORDER BY ProcedureDate DESC) AS RowNumber
	FROM #TempFlu 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.InfluenzaCode = InfluenzaImmunization.ProcedureCode,
	#GproPatientMonitor.InfluenzaLastPerformedDate = InfluenzaImmunization.ProcedureDate
	FROM #GproPatientMonitor, InfluenzaImmunization 
	WHERE #GproPatientMonitor.lbpatientid = InfluenzaImmunization.lbpatientid  
	AND  RowNumber = 1;


	--SELECT lbpatientid,	Memberfullname,	PatientAge,	PatientStatus,	ContractName,	Gender,	BirthDate,
	--Prev7EligibleInd,	InfluenzaLastPerformedDate,	InfluenzaCode,	Prev7Numerator,	Prev7Exclusion
	--FROM #GproPatientMonitor




	 ---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--PREV 8 - Pneu shot is for patients 65 and older
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev8EligibleInd = 1,
	#GproPatientMonitor.Prev8Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.PREV8Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev8


	DELETE FROM  [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'PREV' and [ModuleIndicator] = '8' and Year = @MeasurementYear;


	INSERT INTO #TempPneumonia
	SELECT patientid,proceduredatetime,procedurecode
	FROM vwpatientprocedure pp
	JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pp.patientid
	JOIN [#GPROValueSetCodes] AVC 
	ON pp.procedurecode = AVC.Code
	AND AVC.ValueSetName  IN ('PNEUMO_CODE')
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID] 
	UNION
	SELECT patientid,servicedate,ImmunizationCode
		FROM [DBO].[VWPATIENTIMMUNIZATION] PIMM
		JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pimm.patientid
				JOIN [#GPROValueSetCodes] AVC 
				  ON  PIMM.ImmunizationCode = AVC.CODE                   
	AND AVC.[ValueSetName] in ('PNEUMO_CODE')
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = pimm.[SourceSystemID] ;


	WITH PneumoniaVaccination AS  
	(  
	SELECT 
		LbPatientid,ProcedureDate,ProcedureCode,
		ROW_NUMBER() OVER (Partition by LBPatientid ORDER BY ProcedureDate DESC) AS RowNumber
	FROM #TempPneumonia 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.PnueVaxCode = PneumoniaVaccination.ProcedureCode,
	#GproPatientMonitor.PnueVaxLastPerformedDate = PneumoniaVaccination.ProcedureDate
	FROM #GproPatientMonitor, PneumoniaVaccination 
	WHERE #GproPatientMonitor.lbpatientid = PneumoniaVaccination.lbpatientid  
	AND  RowNumber = 1;


	--SELECT lbpatientid,	Memberfullname,	PatientAge,	PatientStatus,	ContractName,	Gender,	BirthDate,
	--Prev8EligibleInd,	PnueVaxLastPerformedDate,	PnueVaxCode,	Prev8Numerator,	Prev8Exclusion,lastencounterdate,lastencountercode,SecondlastEncounterDate,SecondlastEncounterCode
	--FROM #GproPatientMonitor


	-----------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------


	DELETE FROM  [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'DM'  and Year = '2016';


	WITH LatestDiabetic AS  
	(  
	SELECT 
		Patientid,DiagnosisDatetime,DiagnosisCode,
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY DiagnosisDatetime DESC) AS RowNumber
	FROM vwpatientdiagnosis pd
	JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pd.patientid
	JOIN [#GPROValueSetCodes] AVC 
	ON pd.diagnosiscode = AVC.Code
	AND AVC.ValueSetName  IN ('DM_DX_CODE')
	AND pd.DiagnosisDatetime < @MeasurePeriodEnd
	AND ISNULL(DiagnosisResolvedDate,'2099-12-31') > GETDATE()
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID] 
	)   
	Insert into #TempDiabetic
	SELECT [PatientID],DiagnosisDateTime,DiagnosisCode
	FROM LatestDiabetic   
	WHERE RowNumber = 1
	order by patientid ;


	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DiabeticPatientEligibleInd = 1,
	#GproPatientMonitor.DiabeticDate = #TempDiabetic.DiagnosisDate,
	#GproPatientMonitor.DiabeticCode = #TempDiabetic.DiagnosisCode
	FROM #GproPatientMonitor, #TempDiabetic 
	WHERE #GproPatientMonitor.lbpatientid = #TempDiabetic.lbpatientid;


	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	--DM2 and DM7
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DM2EligibleInd = 1,
	#GproPatientMonitor.DM2Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.DM2Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @DM2;


	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DM7EligibleInd = 1,
	#GproPatientMonitor.DM7Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.DM7Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @DM7;


	WITH LatestHBA1C AS  
	(  
	SELECT 
		plo.[PatientID] AS [PatientID],
		plr.[ObservationDate],
		plr.[ObservationCode1],
		Try_Convert( DECIMAL(5, 2), plr.[Value], 0 ) AS [Result], -- If the patient has a <= 9.0 result, Patient is NonCompliant
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY plr.[ObservationDate] DESC) AS RowNumber
	FROM
	  [dbo].[PatientLabOrder] plo
		JOIN [dbo].[PatientLabResult] plr
			ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]				
		JOIN [#GPROValueSetCodes] AVC
			ON  AVC.[Code] = plr.[ObservationCode1]
			AND AVC.[ValueSetName] = 'A1C_CODE'
			AND plr.[ObservationDate] >= @MeasurePeriodBegin
			AND plr.[ObservationDate] <  @MeasurePeriodEnd
	    JOIN @SourceIdTable SS  ON SS.[SourceSystemId] = plo.[SourceSystemID]
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.HemoglobinA1cTestLastPerformedDate = LatestHBA1C.[ObservationDate], 
	#GproPatientMonitor.HemoglobinA1cTestCode = LatestHBA1C.[ObservationCode1],
	#GproPatientMonitor.HemoglobinA1cTestValue = LatestHBA1C.[Result]
	FROM #GproPatientMonitor, LatestHBA1C 
	WHERE #GproPatientMonitor.lbpatientid = LatestHBA1C.[PatientID]
	AND RowNumber = 1




	INSERT INTO  #TempDiabeticEye
		([LbPatientId],ProcedureCode, ProcedureDate)
		SELECT patientid,procedurecode,proceduredatetime
			FROM vwPatientProcedure pp
			JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pp.patientid
			JOIN [#GPROValueSetCodes] GVSC
			ON pp.[ProcedureCode] = GVSC.[Code]
			AND GVSC.ValueSetName = 'EYE_EXAM_CODE'
			AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
			AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]
	UNION	
			SELECT patientid,procedurecode,proceduredatetime
			FROM vwPatientProcedure pp
			JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pp.patientid
			JOIN [#GPROValueSetCodes] GVSC
			ON pp.[ProcedureCode] = GVSC.[Code]
			AND GVSC.ValueSetName = 'EYE_EXAM_CODE_OPH'
			AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
			AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd
			AND PP.RenderingProviderSpecialtyCode IN ('PO', '18', 'OPH','41') 
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]
	UNION
		SELECT patientid,procedurecode,proceduredatetime
		FROM vwPatientProcedure pp
		JOIN #GproPatientMonitor 
		ON #GproPatientMonitor.lbpatientid = pp.patientid
		JOIN [#GPROValueSetCodes] GVSC
		ON pp.[ProcedureCode] = GVSC.[Code]
		AND GVSC.ValueSetName = 'NEGATIVE_FINDING_CODE'
		AND pp.[ProcedureDateTime] >= DateAdd( YEAR, -1, @MeasurePeriodBegin )
		AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd 
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID];


	WITH DMEYE AS  
	(  
	SELECT 
		LbPatientid,ProcedureDate,ProcedureCode,
		ROW_NUMBER() OVER (Partition by LBPatientid ORDER BY ProcedureDate DESC) AS RowNumber
	FROM #TempDiabeticEye 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DiabeticEyeCode = DMEYE.ProcedureCode,
	#GproPatientMonitor.DiabeticEyeDateLastPerformed = DMEYE.ProcedureDate
	FROM #GproPatientMonitor, DMEYE 
	WHERE #GproPatientMonitor.lbpatientid = DMEYE.lbpatientid  
	AND  RowNumber = 1;



	--SELECT lbpatientid,	Memberfullname,	PatientAge,	PatientStatus,	ContractName,	Gender,	BirthDate,
	--DiabeticPatientEligibleInd,DM2EligibleInd,DM7EligibleInd,DiabeticDate,DiabeticCode, DM2Numerator, DM7Numerator, DM7Exclusion,
	--HemoglobinA1cTestLastPerformedDate,HemoglobinA1cTestCode,HemoglobinA1cTestValue,DiabeticEyeDateLastPerformed,DiabeticEyeCode,
	--lastencounterdate,lastencountercode,SecondlastEncounterDate,SecondlastEncounterCode
	--FROM #GproPatientMonitor
	--WHERE DiabeticPatientEligibleInd = 1

 
 
	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------


	--Prev 9
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev9EligibleInd = 1,
	#GproPatientMonitor.Prev9Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.Prev9Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev9;


	DELETE FROM  [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'PREV' and [ModuleIndicator] = '9' and Year = @MeasurementYear;



	--Getting The last Height we have for a patient
	IF OBJECT_ID( 'tempdb..#PvHeight' ) IS NOT NULL DROP TABLE [#PvHeight];
	SELECT pv.patientid, PV.HeightCM, pv.servicedatetime,
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY pv.servicedatetime DESC) AS RowNumber
	  INTO #PvHeight
	FROM  PatientVitals pv
	JOIN #GproPatientMonitor LBHM
	ON LBHM.lbpatientid = pv.patientid
		AND TRY_CONVERT(FLOAT, PV.HeightCM) IS NOT NULL 
		AND TRY_CONVERT(FLOAT, PV.HeightCM) <> 0.0
		AND pv.servicedatetime >= dateadd(Month,-6,LBHM.LastEncounterDate)
		AND pv.servicedatetime <= LBHM.LastEncounterDate

	--Getting the last weight we have for a patient
	IF OBJECT_ID( 'tempdb..#PvWeight' ) IS NOT NULL DROP TABLE [#PvWeight];
	SELECT  pv.patientid, PV.WeightKG, pv.servicedatetime,
			ROW_NUMBER() OVER (Partition by Patientid ORDER BY pv.servicedatetime DESC) AS RowNumber
	INTO #PvWeight
	FROM  PatientVitals pv
	JOIN #GproPatientMonitor LBHM
	ON LBHM.lbpatientid = pv.patientid
    		AND TRY_CONVERT(FLOAT, PV.WeightKG) IS NOT NULL 
			AND TRY_CONVERT(FLOAT, PV.WeightKG) <> 0.0
			AND pv.servicedatetime >= dateadd(Month,-6,LBHM.LastEncounterDate)
		    AND pv.servicedatetime <= LBHM.LastEncounterDate

	--Fill in temp table with ids and weights
	Insert into [#TempBMI]
	([LbPatientId], TestDate, PTWeightKG,BMIReadingValue)
	SELECT patientid,servicedatetime,WeightKG,null
	FROM #PvWeight WHERE RowNumber = 1

	--Update to temp table with latest Height
	UPDATE [#TempBMI] 
	SET [#TempBMI].PTHeightCM = #PvHeight.HeightCM, [#TempBMI].TestDate = #PvHeight.servicedatetime
	FROM [#TempBMI], #PvHeight 
	WHERE [#TempBMI].lbpatientid = #PvHeight.patientid
	AND #PvHeight.RowNumber = 1

	--select PTHeightCM,PTWeightKG,ROUND(([#TempBMI].PTWeightKG/[#TempBMI].PTHeightCM/[#TempBMI].PTHeightCM)*10000,2) from [#TempBMI] WHERE [#TempBMI].PTWeightKG IS NOT NULL AND [#TempBMI].PTHeightCM IS NOT NULL AND [#TempBMI].PTHeightCM > 0 
	--Calculate BMI
	UPDATE [#TempBMI] 
	SET [#TempBMI].BMIReadingValue = CONVERT(DECIMAL(10,2),ROUND(([#TempBMI].PTWeightKG/[#TempBMI].PTHeightCM/[#TempBMI].PTHeightCM)*10000,2))
	WHERE [#TempBMI].PTWeightKG IS NOT NULL 
		AND [#TempBMI].PTWeightKG IS NOT NULL
		AND [#TempBMI].PTHeightCM IS NOT NULL 
		AND [#TempBMI].PTHeightCM > 0
		AND CONVERT(DECIMAL(10,2),ROUND(([#TempBMI].PTWeightKG/[#TempBMI].PTHeightCM/[#TempBMI].PTHeightCM)*10000,2)) < 199.00


	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.BMILastReadingDate = [#TempBMI].TestDate,
	#GproPatientMonitor.BMILastReadingHeight = [#TempBMI].PTHeightCM,
	#GproPatientMonitor.BMILastReadingWeight = [#TempBMI].PTWeightKG,
	#GproPatientMonitor.BMIReadingValue = [#TempBMI].BMIReadingValue,
	#GproPatientMonitor.BMINormal = CASE  
											--WHEN #TempBMI.BMIReadingValue between 23 and 29 THEN 1  
											--ELSE 0
											WHEN  #GproPatientMonitor.PatientAge >= 65 AND (#TempBMI.BMIReadingValue >= 23 AND #TempBMI.BMIReadingValue < 30) THEN 1
											WHEN  #GproPatientMonitor.PatientAge BETWEEN 18 and 64 AND (#TempBMI.BMIReadingValue >= 18.5 AND #TempBMI.BMIReadingValue < 25) THEN 1
											Else 0
									  END      
	FROM #GproPatientMonitor, [#TempBMI] 
	WHERE #GproPatientMonitor.lbpatientid = [#TempBMI].lbpatientid

	IF OBJECT_ID( 'tempdb..#followup' ) IS NOT NULL DROP TABLE [#followup];    
	 SELECT PATIENTID, proceduredatetime, procedurecode
	  INTO #FOLLOWUP 
	  FROM [dbo].[VWPatientProcedure] pp
			JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pp.patientid
			JOIN [#GPROValueSetCodes] GVSC 
			ON  PP.ProcedureCode = GVSC.CODE
			AND	GVSC.[ValueSetName] IN ('REFERRAL_CODE','BMI_FOLLOW_UP_CODE','BMI_ABNORMAL_CODE')
        		AND pp.[ProcedureDateTime] >= DATEADD(MONTH,-6,#GproPatientMonitor.BMILastReadingDate)
           		AND pp.[ProcedureDateTime] <= #GproPatientMonitor.BMILastReadingDate
			 JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]	
	 UNION
	  SELECT PATIENTID, Diagnosisdatetime,DiagnosisCode
	   FROM [dbo].[VWPatientDiagnosis] pd
	   JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pd.patientid
			AND pd.[DiagnosisDateTime] >= DATEADD(MONTH,-6,#GproPatientMonitor.BMILastReadingDate)
       		AND pd.[DiagnosisDateTime] <= #GproPatientMonitor.BMILastReadingDate
	   JOIN [#GPROValueSetCodes] GVSC 
			ON  PD.DiagnosisCode = GVSC.CODE
			AND GVSC.[ValueSetName] IN ('REFERRAL_CODE','BMI_FOLLOW_UP_CODE') 
		JOIN @SourceIdTable SS 
			 ON SS.[SourceSystemId] = Pd.[SourceSystemID]	
	UNION              
	  SELECT patientid,medicationstartdate,ndccode
			 FROM [dbo].[VWPatientMedication] pm
			 JOIN #GproPatientMonitor 
				ON #GproPatientMonitor.lbpatientid = pm.patientid
			 JOIN [#GPROValueSetCodes] GVSC 
				ON  pm.RxNormCode = GVSC.CODE
				AND GVSC.[ValueSetName] IN ('BMI_DRUG_CODE') 
				AND pm.[MedicationStartDate] >= DATEADD(MONTH,-6,#GproPatientMonitor.BMILastReadingDate)
    			AND pm.[MedicationStartDate] <= #GproPatientMonitor.BMILastReadingDate
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = Pm.[SourceSystemID];	
        
	WITH BMIFollowUp AS  
	(  
	SELECT 
		PatientId,
		proceduredatetime, 
		procedurecode,
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY proceduredatetime DESC) AS RowNumber
	FROM #FOLLOWUP
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.BMIFollowUpCode = BMIFollowUp.procedurecode,
	#GproPatientMonitor.BMIFollowUpDate = BMIFollowUp.proceduredatetime
	FROM #GproPatientMonitor, BMIFollowUp 
	WHERE #GproPatientMonitor.lbpatientid = BMIFollowUp.patientid  
	AND  RowNumber = 1;      



	--SELECT Patientage,Prev9EligibleInd ,BMILastReadingDate, BMILastReadingHeight, BMILastReadingWeight, BMIReadingValue, BMINormal,BMIFollowupCode,BMIFollowupDate,Prev9Numerator,Prev9Exclusion
	--FROM #GproPatientMonitor
	--WHERE Prev9EligibleInd = 1 and Prev9Numerator = 1 and BMINormal = 0
	-------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------


	-- HF 6 Heart Failure
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.HF6EligibleInd = 1,
	#GproPatientMonitor.HF6Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.HF6Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @HF6;

	DELETE FROM  [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'HF' and Year = @MeasurementYear;




	WITH LatestHeartFailure AS  
	(  
	SELECT 
		Patientid,DiagnosisDatetime,DiagnosisCode,
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY DiagnosisDatetime DESC) AS RowNumber
	FROM vwpatientdiagnosis pd
	JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pd.patientid
	JOIN [#GPROValueSetCodes] AVC 
	ON pd.diagnosiscode = AVC.Code
	AND AVC.ValueSetName  IN ('HF_DX_CODE')
	AND pd.DiagnosisDatetime < @MeasurePeriodEnd
	JOIN @SourceIdTable SS  ON SS.[SourceSystemId] = pd.[SourceSystemID]
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.HeartFailureDate = LatestHeartFailure.DiagnosisDateTime,
	#GproPatientMonitor.HeartFailureCode = LatestHeartFailure.DiagnosisCode
	FROM #GproPatientMonitor, LatestHeartFailure 
	WHERE #GproPatientMonitor.lbpatientid = LatestHeartFailure.patientid
	AND RowNumber = 1;


	IF OBJECT_ID( 'tempdb..#LVEF' ) IS NOT NULL DROP TABLE [#LVEF];
	SELECT pd.patientid,pd.diagnosisdatetime,pd.diagnosiscode,null as result  INTO [#LVEF]
	 FROM vwPatientDiagnosis PD with (nolock)
	 JOIN #GproPatientMonitor 
	 On #GproPatientMonitor.lbpatientid = PD.patientid
	 JOIN [#GPROValueSetCodes] GVSC
		  ON  GVSC.[Code] = pd.[DiagnosisCode]
		  AND gvsc.valuesetname IN ('LVSD_DX_CODE','MOD_LVSD_CODE','SEV_LVSD_CODE')--'SEVERITY_MODIFIER'
		  AND gvsc.codesystem in ('I10','I9','SNM')
	 JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID]    --PROCS
	UNION
	SELECT pp.patientid,pp.proceduredatetime,pp.ProcedureCode,null  
	 FROM vwPatientProcedure PP with (nolock)
	 JOIN #GproPatientMonitor 
	 On #GproPatientMonitor.lbpatientid = PP.patientid
	 JOIN [#GPROValueSetCodes] GVSC
		  ON  GVSC.[Code] = PP.ProcedureCode
		  AND gvsc.valuesetname IN ('LVSD_DX_CODE','MOD_LVSD_CODE','SEV_LVSD_CODE')--'SEVERITY_MODIFIER'
		  AND gvsc.codesystem in ('C4','SNM')
	 JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
	UNION
	SELECT plo.patientid,plr.ObservationDate,plr.ObservationCode1,plr.Value
  			FROM [dbo].[PatientLabOrder] plo
				JOIN #GproPatientMonitor 
					On #GproPatientMonitor.lbpatientid = Plo.patientid
				JOIN [dbo].[PatientLabResult] plr
					ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
					AND IsNull( Try_Convert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 ) <= 40.0
				 JOIN [#GPROValueSetCodes] GVSC
					ON  GVSC.[Code] = plr.[ObservationCode1]
					AND GVSC.[valuesetname] = 'EF_CODE'
				JOIN @SourceIdTable SS ON SS.[SourceSystemId] = plo.[SourceSystemID];

	WITH LVSD AS  
	(  
	SELECT 
		patientid, diagnosisdatetime, diagnosiscode, result,
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY diagnosisdatetime DESC) AS RowNumber
	FROM #LVEF 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.LVSDPatientInd = 1,
	#GproPatientMonitor.LVSDCode = LVSD.diagnosiscode,
	#GproPatientMonitor.LVSDDate = LVSD.diagnosisdatetime,
	#GproPatientMonitor.LVSDResult = LVSD.result
	FROM #GproPatientMonitor, LVSD
	WHERE #GproPatientMonitor.lbpatientid = LVSD.PatientID 
	AND  RowNumber = 1;


	IF OBJECT_ID( 'tempdb..#BETABLOCKER' ) IS NOT NULL DROP TABLE [#BETABLOCKER];
		SELECT PatientID,rxnormCode, MedicationStartDate
		INTO #BETABLOCKER
			FROM vwpatientmedication PM
			 JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = PM.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			ON  pm.rxnormCode = AVC.[Code]
				   AND AVC.ValueSetName IN ('LVSD_BB_DRUG_CODE')
				   AND AVC.codesystem in ('RXNORM')
	 			   AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @MeasurePeriodEnd
				   AND IsNull( pm.[MedicationEndDate], @MeasurePeriodBegin ) >= @MeasurePeriodBegin
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PM.[SourceSystemID] 
		UNION
		SELECT PatientID,NDCCode,MedicationStartDate
		FROM vwpatientmedication PM
			 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PM.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pm.NDCCode = AVC.[Code]
				   AND AVC.ValueSetName IN ('LVSD_BB_DRUG_CODE')
				   AND AVC.codesystem in ('NDC')
        	   		AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @MeasurePeriodEnd
					AND IsNull( pm.[MedicationEndDate], @MeasurePeriodBegin ) >= @MeasurePeriodBegin
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PM.[SourceSystemID] 
		UNION
		SELECT PatientID, procedurecode,proceduredatetime
			FROM vwpatientprocedure PP
			 JOIN #GproPatientMonitor 
				ON #GproPatientMonitor.lbpatientid = PP.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pp.Procedurecode = AVC.[Code]
				   AND AVC.ValueSetName IN ('LVSD_BB_DRUG_CODE')
				   AND AVC.codesystem in ('C4')
				  AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
				   AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]; 

		 
	WITH BETABLOCKER AS  
	(  
		SELECT PatientID,rxnormCode,MedicationStartDate,	
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY MedicationStartDate DESC) AS RowNumber
		FROM #BETABLOCKER
	)
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.PatientOnBetaBlocker = 1,
	#GproPatientMonitor.BetaBlockerCode = rxnormCode,
	#GproPatientMonitor.BetaBlockerStartDate = MedicationStartDate
	FROM #GproPatientMonitor, BETABLOCKER 
	WHERE #GproPatientMonitor.lbpatientid = BETABLOCKER.patientid 
	AND RowNumber = 1;




	--SELECT lbpatientid,	Memberfullname,	PatientAge,	PatientStatus,	ContractName,	Gender,	BirthDate,
	--HF6EligibleInd,	HeartFailureDate,	HeartFailureCode,	HF6Numerator,	HF6Exclusion,lastencounterdate,lastencountercode,
	--  SecondlastEncounterDate,SecondlastEncounterCode,PatientOnBetaBlocker,LVSDPatientInd ,LVSDCode ,LVSDDate ,LVSDResult FROM #GproPatientMonitor 
	--  WHERE HF6EligibleInd = 1


	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------


	-- Prev12
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev12EligibleInd = 1,
	#GproPatientMonitor.Prev12Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.Prev12Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev12;


	-- MH1
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.MH1EligibleInd = 1,
	#GproPatientMonitor.MH1Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.MH1Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @MH1;


	DELETE FROM  [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'MH'  and Year = '2016';


	WITH DepressionPatients AS  
	(  
	SELECT 
		Patientid,DiagnosisDatetime,DiagnosisCode,
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY DiagnosisDatetime DESC) AS RowNumber
	FROM vwpatientdiagnosis pd
	 JOIN #GproPatientMonitor 
	 On #GproPatientMonitor.lbpatientid = pd.patientid
	JOIN [#GPROValueSetCodes] AVC 
	ON pd.diagnosiscode = AVC.Code
	AND AVC.ValueSetName  IN ('DEPRESSION_CODE','DYSTHYMIA_CODE')
	AND pd.DiagnosisDatetime < @MeasurePeriodEnd
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID] 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DepressionPatientEligibleInd = 1,
	#GproPatientMonitor.DepressionCode = DepressionPatients.DiagnosisCode,
	#GproPatientMonitor.DepressionDate = DepressionPatients.DiagnosisDatetime
	FROM #GproPatientMonitor, DepressionPatients 
	WHERE #GproPatientMonitor.lbpatientid = DepressionPatients.patientid
	AND RowNumber = 1;




	Insert into #TempAllPHQ9DepressionScreen
	(LbPatientId, DepressionScreenDate,	DepressionScreenCode,DepressionScreenScore)
	SELECT 
		plo.[PatientID] AS [PatientID],
		plr.[ObservationDate],
		plr.[ObservationCode1],
		Try_Convert( DECIMAL(5, 2), plr.[Value], 0 ) AS [Result] -- If the patient has a <= 9.0 result, Patient is NonCompliant
	FROM
	  [dbo].[PatientLabOrder] plo
		JOIN [dbo].[PatientLabResult] plr
			ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
		 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = plo.patientid
		JOIN  [#GPROValueSetCodes] AVC 
			ON  AVC.[Code] = plr.[ObservationCode1]
			AND AVC.[ValueSetName] = 'PHQ9_TOOL_CODE'
			AND Try_Convert( DECIMAL(5, 2), plr.[Value], 0 ) > 0
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID] 
	UNION
	 SELECT pp.patientid, pp.proceduredatetime, pc.procedurecode, pp.procedureresult 
	 FROM [dbo].[PatientProcedure] pp
		  JOIN [dbo].[PatientProcedureProcedureCode] pppc
				ON	pp.[PatientProcedureId] = pppc.[PatientProcedureId]				
		  JOIN [dbo].[ProcedureCode] pc
				ON  pppc.[ProcedureCodeId] =  pc.[ProcedureCodeId]
		  JOIN #GproPatientMonitor 
				ON #GproPatientMonitor.lbpatientid = PP.patientid
				JOIN  [#GPROValueSetCodes] AVC 
			ON  AVC.[Code] = pc.ProcedureCode
			AND AVC.[ValueSetName] = 'PHQ9_TOOL_CODE'
			AND Try_Convert( DECIMAL(5, 2), procedureresult, 0 ) > 0
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID];



	WITH LatestPHQ9 AS  
	(  
	SELECT 
		LbPatientId,
		DepressionScreenDate,
		DepressionScreenCode,
		Try_Convert( DECIMAL(5, 2), DepressionScreenScore, 0 ) AS [Result], -- If the patient has a <= 9.0 result, Patient is NonCompliant
		ROW_NUMBER() OVER (Partition by LbPatientid ORDER BY DepressionScreenDate DESC) AS RowNumber
	FROM #TempAllPHQ9DepressionScreen PHQA
		JOIN [#GPROValueSetCodes] AVC 
			ON  AVC.[Code] = PHQA.DepressionScreenCode
			AND AVC.[ValueSetName] = 'PHQ9_TOOL_CODE'
			AND DepressionScreenDate < @MeasurePeriodEnd
			AND Try_Convert( DECIMAL(5, 2), DepressionScreenScore, 0 ) > 0
		
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DepressionLatestPHQ9Code = LatestPHQ9.DepressionScreenCode,
	#GproPatientMonitor.DepressionLatestPHQ9ScreeningDate = LatestPHQ9.DepressionScreenDate,
	#GproPatientMonitor.DepressionLatestPHQ9ScreenScore = LatestPHQ9.[Result]
	FROM #GproPatientMonitor, LatestPHQ9 
	WHERE #GproPatientMonitor.lbpatientid = LatestPHQ9.lbpatientid  
	AND  RowNumber = 1;





	WITH IntPHQ9 AS  
	(  
	SELECT 
		LbPatientId,
		DepressionScreenDate,
		DepressionScreenCode,
		Try_Convert( DECIMAL(5, 2), DepressionScreenScore, 0 ) AS [Result], -- If the patient has a <= 9.0 result, Patient is NonCompliant
		ROW_NUMBER() OVER (Partition by LbPatientid ORDER BY DepressionScreenDate) AS RowNumber
	FROM #TempAllPHQ9DepressionScreen PHQA
		JOIN [#GPROValueSetCodes] AVC 
			ON  AVC.[Code] = PHQA.DepressionScreenCode
			AND AVC.[ValueSetName] = 'PHQ9_TOOL_CODE'
			AND DepressionScreenDate >= @PHQ9INTSTART
			AND DepressionScreenDate <= @PHQ9INTEND
			AND Try_Convert( DECIMAL(5, 2), DepressionScreenScore, 0 ) > 0		
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DepressionInitialPHQ9Code = IntPHQ9.DepressionScreenCode,
	#GproPatientMonitor.DepressionInitialPHQ9ScreeningDate = IntPHQ9.DepressionScreenDate,
	#GproPatientMonitor.DepressionInitialPHQ9ScreenScore = IntPHQ9.[Result]
	FROM #GproPatientMonitor, IntPHQ9 
	WHERE #GproPatientMonitor.lbpatientid = IntPHQ9.lbpatientid  
	AND  RowNumber = 1;


	

	DELETE FROM [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	  SELECT VariableName ,
			 CodeSystem ,
			 Code
	 -- FROM [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	  FROM vwGProValueSet
	  WHERE  [ModuleType] = 'PREV' and [ModuleIndicator] = '12' and Year = '2016';


WITH DepressionScreening AS  
	(  
	SELECT pp.patientid,pp.proceduredatetime,pp.procedurecode,
	ROW_NUMBER() OVER (Partition by Patientid ORDER BY proceduredatetime DESC) AS RowNumber
      FROM vwPatientProcedure PP with (nolock)
		JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PP.patientid
		JOIN [#GPROValueSetCodes] GVSC
	    ON  GVSC.[Code] = pp.[procedureCode]
		AND gvsc.valuesetname = 'SCREENING_CODE'
		AND GVSC.CodeSystem in ('C4','HCPCS','SNM','LN')
		AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
		AND pp.[ProcedureDateTime] < @MeasurePeriodEnd
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DepressionScreeningPatientEligibleInd = 1,
	#GproPatientMonitor.DepressionScreenCode = DepressionScreening.ProcedureCode,
	#GproPatientMonitor.DepressionScreenDate = DepressionScreening.ProcedureDateTime	
	FROM #GproPatientMonitor, DepressionScreening 
	WHERE #GproPatientMonitor.lbpatientid = DepressionScreening.patientid  
	AND  RowNumber = 1;      


WITH POSDepressionScreening AS  
(
select pp.patientid,pp.proceduredatetime,pp.procedurecode ,
       ROW_NUMBER() OVER (Partition by Patientid ORDER BY proceduredatetime DESC) AS RowNumber
		FROM vwpatientprocedure pp with (nolock)
		JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PP.patientid
		JOIN [#GPROValueSetCodes] GVSC
		ON  GVSC.[Code] = pp.[procedureCode]
		AND gvsc.valuesetname =  'POS_SCREENING_CODE'
		AND GVSC.CodeSystem in ('C4','HCPCS','SNM','LN')
		AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
        AND pp.[ProcedureDateTime] < @MeasurePeriodEnd
       	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
)
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DepressionScreenPositiveInd = 1
	FROM #GproPatientMonitor, POSDepressionScreening 
	WHERE #GproPatientMonitor.lbpatientid = POSDepressionScreening.patientid  
	AND  RowNumber = 1;      




WITH DepressionScreeningFU AS  
(
		SELECT pp.patientid,pp.proceduredatetime,pp.procedurecode,
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY proceduredatetime DESC) AS RowNumber
		FROM vwpatientprocedure pp with (nolock)
		JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PP.patientid
		JOIN [#GPROValueSetCodes] GVSC
		ON  GVSC.[Code] = pp.[procedureCode]
		AND gvsc.valuesetname in ('ADDITIONAL_EVAL_CODE','FOLLOW_UP_CODE','REFERRAL_CODE','SUICIDE_RISK_CODE')
		AND GVSC.CodeSystem in ('C4','HCPCS','SNM','LN')
		AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
        AND pp.[ProcedureDateTime] < @MeasurePeriodEnd
    	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
)
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.DepressionScreenFollowUpInd = 1,
	#GproPatientMonitor.DepressionScreenFollowUpCode = DepressionScreeningFU.procedurecode,
	#GproPatientMonitor.DepressionScreenFollowUpDate = DepressionScreeningFU.proceduredatetime
	FROM #GproPatientMonitor, DepressionScreeningFU 
	WHERE #GproPatientMonitor.lbpatientid = DepressionScreeningFU.patientid  
	AND  RowNumber = 1;     



	--SELECT lbpatientid,PatientAge,DepressionPatientEligibleInd,Prev12EligibleInd, Prev12Numerator, Prev12Exclusion,DepressionCode,
	--DepressionLatestPHQ9Code,DepressionLatestPHQ9Screening9Date,DepressionLatestPHQ9ScreenScore,
	--lastencounterdate,lastencountercode,SecondlastEncounterDate,SecondlastEncounterCode
	--FROM #GproPatientMonitor
	--WHERE DepressionPatientEligibleInd = 1

	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	-- Prev11
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev11EligibleInd = 1,
	#GproPatientMonitor.Prev11Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.Prev11Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev11;




	--HTN 2 
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.HTN2EligibleInd = 1,
	#GproPatientMonitor.HTN2Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.HTN2Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @HTN2;



	DELETE FROM [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	  --FROM [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'HTN' and Year = '2016';



	WITH LatestHTN AS  
	(  
	SELECT 
		Patientid,DiagnosisDatetime,DiagnosisCode,
		ROW_NUMBER() OVER (Partition by pd.Patientid ORDER BY pd.DiagnosisDatetime DESC) AS RowNumber
	FROM vwpatientdiagnosis pd
	 JOIN #GproPatientMonitor 
	 On #GproPatientMonitor.lbpatientid = pd.patientid
	JOIN [#GPROValueSetCodes] AVC 
	ON pd.diagnosiscode = AVC.Code
	AND AVC.ValueSetName  IN ('HTN_DX_CODE')
	AND pd.[DiagnosisDateTime] <= @HTNDiagEnd 
    AND (DiagnosisResolvedDate >= @MeasurePeriodBegin OR DiagnosisResolvedDate is null) 
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID] 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.HypertensionDate = LatestHTN.DiagnosisDatetime,
	#GproPatientMonitor.HypertensionCode = LatestHTN.DiagnosisCode
	FROM #GproPatientMonitor, LatestHTN 
	WHERE #GproPatientMonitor.lbpatientid = LatestHTN.patientid
	AND Rownumber = 1;


	DELETE FROM [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	  --FROM [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'PREV' AND [ModuleIndicator] = '11' AND Year = '2016';

	--Getting the latest BP reading
	WITH LatestBP AS  
	(  
	SELECT 
		pv.Patientid,pv.ServiceDateTime,pv.BloodPressureSystolic,pv.BloodPressureDiastolic,
		ROW_NUMBER() OVER (Partition by pv.Patientid ORDER BY pv.ServiceDateTime DESC) AS RowNumber
	FROM vwpatientvitals pv
	 JOIN #GproPatientMonitor 
	 On #GproPatientMonitor.lbpatientid = pv.patientid
	WHERE pv.BloodPressureSystolic IS NOT NULL 
	AND pv.BloodPressureDiastolic IS NOT NULL 
	AND pv.[ServiceDateTime] >= @MeasurePeriodBegin 
	AND pv.[ServiceDateTime] < @MeasurePeriodEnd 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.BloodPressureReadingDate = LatestBP.ServiceDateTime, 
	#GproPatientMonitor.BloodPressureSystolic = LatestBP.BloodPressureSystolic,
	#GproPatientMonitor.BloodPressureDiastolic = LatestBP.BloodPressureDiastolic,
	#GproPatientMonitor.BloodPressureNormal =  CASE WHEN (LatestBP.BloodPressureSystolic >= 120 OR LatestBP.BloodPressureDiastolic >= 80) THEN 0 ELSE 1 END
	FROM #GproPatientMonitor, LatestBP 
	WHERE #GproPatientMonitor.lbpatientid = LatestBP.patientid
	AND RowNumber = 1



	--- temp table for the patients who had follow up documented
	IF OBJECT_ID( 'tempdb..#BPFollowup' ) IS NOT NULL DROP TABLE [#BPFollowup];
	-- for the patients who didn't have a normal BP test, looking for follow up
	-- procedure code follow up
	SELECT patientid, proceduredatetime,procedurecode INTO #BPFollowup
	FROM  vwPatientProcedure pp 
	JOIN #GproPatientMonitor 
		ON #GproPatientMonitor.lbpatientid = pp.patientid
	JOIN #gproValueSetCodes gvsc 
		on pp.ProcedureCode = gvsc.code
		and gvsc.valuesetname in ('ALTERNATE_PROVIDER_CODE','DIETARY_CODE','ETOH_CODE','FOLLOW_UP_CODE',
								  'LIFESTYLE_CODE','WEIGHT_CODE','PHYSICAL_ACTIVITY_CODE')
		and gvsc.codesystem in ('HCPCS', 'SNM')
		and pp.ProcedureDateTime >= @MeasurePeriodBegin
		and pp.ProcedureDateTime < @MeasurePeriodEnd
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]
	UNION
	SELECT patientid, diagnosisdatetime,diagnosiscode
	FROM  vwPatientDiagnosis pd 
	JOIN #GproPatientMonitor 
		ON #GproPatientMonitor.lbpatientid = pd.patientid
	JOIN #gproValueSetCodes gvsc 
		on pd.DiagnosisCode = gvsc.code
		and gvsc.valuesetname in ('ALTERNATE_PROVIDER_CODE','DIETARY_CODE','ETOH_CODE','FOLLOW_UP_CODE',
								  'LIFESTYLE_CODE','WEIGHT_CODE','PHYSICAL_ACTIVITY_CODE')
		and gvsc.codesystem in ('I9', 'I10','SNM')
		and pd.DiagnosisDateTime >= @MeasurePeriodBegin 
		and pd.DiagnosisDateTime < @MeasurePeriodEnd
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID]
	Union
	SELECT plo.patientid,plr.observationdate,plr.observationcode1 
	FROM vwpatientlaborder plo
	JOIN vwpatientlabresult plr
	ON plo.[PatientLabOrderId] = plr.[PatientLabOrderId]
	JOIN #GproPatientMonitor 
		ON #GproPatientMonitor.lbpatientid = plo.patientid
	JOIN [#GPROValueSetCodes] GVSC 
		ON  plr.ObservationCode1 = GVSC.CODE
		AND GVSC.[ValueSetName] IN ('LABORATORY_TEST_CODE','DIAGNOSTIC_STUDY_CODE')
		AND plr.[ObservationDate] >= @MeasurePeriodBegin
			AND plr.[ObservationDate] <  @MeasurePeriodEnd
			JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID]
	UNION
	SELECT patientid, MedicationStartDate,RxNormCode
	FROM [dbo].[VWPatientMedication] pm
	JOIN #GproPatientMonitor 
		ON #GproPatientMonitor.lbpatientid = pm.patientid
	JOIN [#GPROValueSetCodes] GVSC
		on gvsc.code = pm.RxNormCode
		and gvsc.valueSetName in ('BP_DRUG_CODE')
		and pm.MedicationStartDate >= @MeasurePeriodBegin
		and pm.MedicationStartDate < @MeasurePeriodEnd
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PM.[SourceSystemID];


	WITH BloodPressureFollowUp AS  
	(  
	SELECT PatientId,	proceduredatetime, procedurecode,
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY proceduredatetime DESC) AS RowNumber
	FROM #BPFOLLOWUP
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.BloodPressureFollowupCode = BloodPressureFollowUp.procedurecode,
	#GproPatientMonitor.BloodPressureFollowupDate = BloodPressureFollowUp.proceduredatetime
	FROM #GproPatientMonitor, BloodPressureFollowUp 
	WHERE #GproPatientMonitor.lbpatientid = BloodPressureFollowUp.patientid  
	AND  RowNumber = 1;      


	--SELECT memberfullname,PatientAge,lbpatientid, HTN2EligibleInd,HypertensionDate,HypertensionCode,BloodPressureReadingDate,
	--HTN2Numerator,HTN2Exclusion,BloodPressureSystolic,BloodPressureDiastolic,BloodPressureNormal,
	--Prev11EligibleInd,Prev11Numerator,Prev11Exclusion,
	--BloodPressureFollowupCode,BloodPressureFollowupDate,
	--LastEncounterDate,LastEncounterCode,SecondLastEncounterDate,SecondlastEncounterCode
	--FROM #GproPatientMonitor 




	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	-- IVD2
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.IVDEligibleInd = 1,
	#GproPatientMonitor.IVDNumerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.IVDExclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @IVD2;

	DELETE FROM [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	  --FROM [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'IVD' and Year = '2016';



	IF OBJECT_ID( 'tempdb..#AspirinPatients' ) IS NOT NULL DROP TABLE [#AspirinPatients];
		SELECT PatientID,rxnormCode,MedicationStartDate
		INTO #AspirinPatients
		FROM vwpatientmedication PM
	 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PM.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pm.rxnormCode = AVC.[Code]
				   AND AVC.ValueSetName IN ('ASA_DRUG_CODE')
				   AND AVC.codesystem in ('RXNORM')
				   AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @MeasurePeriodEnd
				   AND IsNull( pm.[MedicationEndDate], @MeasurePeriodBegin ) >= @MeasurePeriodBegin
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PM.[SourceSystemID] 
		UNION
		SELECT PatientID,NDCCode,MedicationStartDate
		FROM vwpatientmedication PM
	 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PM.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pm.NDCCode = AVC.[Code]
				   AND AVC.ValueSetName IN ('ASA_DRUG_CODE')
				   AND AVC.codesystem in ('NDC')
	    	   		AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @MeasurePeriodEnd
					AND IsNull( pm.[MedicationEndDate], @MeasurePeriodBegin ) >= @MeasurePeriodBegin
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PM.[SourceSystemID] 
		UNION
		SELECT Distinct PatientID,procedurecode,proceduredatetime
			FROM vwpatientprocedure PP
	 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PP.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pp.Procedurecode = AVC.[Code]
				   AND AVC.ValueSetName IN ('ASA_DRUG_CODE')
				   AND AVC.codesystem in ('C4')
	 			   AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
				   AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]; 


	WITH ASPIRIN AS  
	(  
		SELECT PatientID,rxnormCode,MedicationStartDate,	
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY MedicationStartDate DESC) AS RowNumber
		FROM #AspirinPatients PM
	)
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.PatientOnAspirin = 1,
	#GproPatientMonitor.AspirinCode = rxnormCode,
	#GproPatientMonitor.AspirinStartDate = MedicationStartDate
	FROM #GproPatientMonitor, ASPIRIN 
	WHERE #GproPatientMonitor.lbpatientid = ASPIRIN.patientid 
	AND RowNumber = 1;





	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------


	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	-- Prev10
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev10EligibleInd = 1,
	#GproPatientMonitor.Prev10Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.Prev10Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev10;


DELETE FROM [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	  SELECT VariableName ,
			 CodeSystem ,
			 Code
	 -- FROM [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	  FROM vwGProValueSet
	  WHERE  [ModuleType] = 'PREV' and [ModuleIndicator] = '10' and Year = '2016';


--Find everyone that had a screening
 IF OBJECT_ID( 'tempdb..#SCREENING' ) IS NOT NULL DROP TABLE [#SCREENING];
 select pp.patientid,pp.proceduredatetime,pp.procedurecode,3 AS [PosNegInd]
 --ROW_NUMBER () OVER (PARTITION BY pp.PatientId ORDER BY pp.ProcedureDateTime DESC)LATEST_SCREENING
 INTO [#SCREENING]
 from vwPatientProcedure PP with (nolock)
		JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = pp.patientid
		JOIN [#GPROValueSetCodes] GVSC
	    ON  GVSC.[Code] = pp.[procedureCode]
		and gvsc.valuesetname in ('TOBACCO_SCREEN_CODE','TOBACCO_NON_USER_CODE','TOBACCO_USER_CODE')
		AND GVSC.CodeSystem in ('C4','HCPCS','SNM','LN')
		AND pp.[ProcedureDateTime] >= DATEADD(year,-2,@MeasurePeriodEND)
		AND pp.[ProcedureDateTime] < @MeasurePeriodEND
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
		GROUP BY
	     pp.patientid,pp.proceduredatetime,pp.procedurecode
UNION
 SELECT pd.patientid,pd.DiagnosisDateTime,pd.DiagnosisCode,3 AS [PosNegInd]
 --ROW_NUMBER () OVER (PARTITION BY pd.PatientId ORDER BY pd.ProcedureDateTime DESC)LATEST_SCREENING
  FROM vwPatientDiagnosis PD with (nolock)
		JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = PD.patientid
		JOIN [#GPROValueSetCodes] GVSC
	    ON  GVSC.[Code] = pd.[DiagnosisCode]
		and gvsc.valuesetname in ('TOBACCO_SCREEN_CODE','TOBACCO_NON_USER_CODE','TOBACCO_USER_CODE')
		AND GVSC.CodeSystem in ('I9','I10','SNM','LN')
		AND pd.[DiagnosisDateTime] >= DATEADD(year,-2,@MeasurePeriodEND)
		and pd.[DiagnosisDateTime] <  @MeasurePeriodEND
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID]    --PROCS
		GROUP BY
	    pd.patientid,pd.DiagnosisDateTime,pd.DiagnosisCode
	UNION
    select psh.patientid,psh.RecordedDateTime,psh.SmokingStatus,3 AS [PosNegInd]
	--ROW_NUMBER () OVER (PARTITION BY psh.PatientId ORDER BY psh.CreateDateTime DESC)LATEST_SCREENING
	From  [dbo].[PatientSocialHistory] psh
		JOIN #GproPatientMonitor 
			ON #GproPatientMonitor.lbpatientid = psh.patientid
		AND psh.RecordedDateTime >= DATEADD(year,-2,@MeasurePeriodEND)
		AND psh.RecordedDateTime <  @MeasurePeriodEnd
		AND psh.RecordedDateTime IS NOT NULL
		JOIN [#GPROValueSetCodes] GVSC
	    ON  GVSC.[Code] = psh.SmokingStatus
		and gvsc.valuesetname in ('TOBACCO_SCREEN_CODE','TOBACCO_NON_USER_CODE','TOBACCO_USER_CODE')
		AND GVSC.CodeSystem in ('I9','I10','SNM','LN')
GROUP BY
	psh.patientid,psh.RecordedDateTime,psh.SmokingStatus


Update #SCREENING set [PosNegInd] = 0
where procedurecode in ( select code from  [#GPROValueSetCodes] 
	    where  valuesetname in ('TOBACCO_NON_USER_CODE')
		AND CodeSystem in ('C4','HCPCS','I9','I10','SNM','LN'));


Update #SCREENING set [PosNegInd] = 1
where procedurecode in ( select code from [#GPROValueSetCodes] 
	    where  valuesetname in ('TOBACCO_USER_CODE')
		AND CodeSystem in ('C4','HCPCS','I9','I10','SNM','LN'));


IF OBJECT_ID( 'tempdb..#MAXSCREENINGRESULT' ) IS NOT NULL DROP TABLE [#MAXSCREENINGRESULT];
select patientid,proceduredatetime,procedurecode ,PosNegInd ,
ROW_NUMBER () OVER (PARTITION BY S.PatientId ORDER BY S.procedureDateTime DESC)LATEST_SCREENING
into [#MAXSCREENINGRESULT] 
from  #SCREENING S
group by  patientid,proceduredatetime,procedurecode ,PosNegInd 



UPDATE #GproPatientMonitor 
SET #GproPatientMonitor.PatientIsSmokerInd = #MAXSCREENINGRESULT.PosNegInd,
#GproPatientMonitor.PREV10TobaccoScreeningDate = #MAXSCREENINGRESULT.proceduredatetime,
#GproPatientMonitor.PREV10TobaccoScreeningCode = #MAXSCREENINGRESULT.procedurecode
FROM #GproPatientMonitor, #MAXSCREENINGRESULT 
WHERE #GproPatientMonitor.lbpatientid = #MAXSCREENINGRESULT.patientid  
AND  LATEST_SCREENING = 1;


 IF OBJECT_ID( 'tempdb..#SCREENING' ) IS NOT NULL DROP TABLE [#SCREENING];
 IF OBJECT_ID( 'tempdb..#MAXSCREENINGRESULT' ) IS NOT NULL DROP TABLE [#MAXSCREENINGRESULT];
 

	--SELECT lbpatientid,memberfullname,patientage, Prev10EligibleInd,
	--Prev10Numerator,Prev10Exclusion,LastEncounterDate,LastEncounterCode,
	--SecondLastEncounterDate,SecondlastEncounterCode FROM #GproPatientMonitor


	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------
	-- Prev13
	--	 Create a temp table to hold intermediate results.
		IF OBJECT_ID( 'tempdb..#PREV13Patients' ) IS NOT NULL DROP TABLE [#PREV13Patients];
		CREATE TABLE #PREV13Patients
		(
			measureid int not null,
			[LbPatientId]       [int] NOT NULL,
			PREV13Numerator tinyint,
			PREV13Exclusion tinyint
		);

	INSERT INTO #PREV13Patients
	(measureid, [LbPatientId], PREV13Numerator, PREV13Exclusion)
	SELECT measureid,cd.LbPatientId, Numerator,Exclusion
	FROM cmeasure_detail cd
	 JOIN #GproPatientMonitor 
	 ON #GproPatientMonitor.lbpatientid = cd.[LbPatientId]
	WHERE cd.measureid in (@Prev13, @Prev13B, @Prev13C);


	DELETE FROM [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	  SELECT VariableName ,
			 CodeSystem ,
			 Code
	 -- FROM [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	  FROM vwGProValueSet
	  WHERE  [ModuleType] = 'PREV' and [ModuleIndicator] = '13' and Year = '2016';





	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev13EligibleInd = 1,
	#GproPatientMonitor.Prev13Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.PREV13Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev13;


	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev13BEligibleInd = 1,
	#GproPatientMonitor.Prev13BNumerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.PREV13BExclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev13B;
	
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.Prev13CEligibleInd = 1,
	#GproPatientMonitor.Prev13CNumerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.PREV13CExclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @Prev13C;





WITH ASCVD AS  
	(  
	 SELECT Patientid,DiagnosisCode,Diagnosisdatetime,
	 ROW_NUMBER() OVER (Partition by Patientid ORDER BY Diagnosisdatetime DESC) AS RowNumber
     FROM vwPatientDiagnosis PD
     JOIN #GproPatientMonitor 
	 ON #GproPatientMonitor.lbpatientid = pd.patientid
	 JOIN [#GPROValueSetCodes] AVC 
	 ON pd.diagnosiscode = AVC.Code
	 AND AVC.ValueSetName  IN ('ASCVD_CODE','CABG_CODE','CAROTID_CODE','MI_CODE','PCI_CODE')
	 AND AVC.CodeSystem in ('I9','I10','SNM')
     AND pd.[DiagnosisDateTime] < @MeasurePeriodEnd
     JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID] 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.ASCVDPatientEligibleInd = 1,
	#GproPatientMonitor.ASCVDCode = ASCVD.DiagnosisCode, 
	#GproPatientMonitor.ASCVDDate = ASCVD.Diagnosisdatetime
	FROM #GproPatientMonitor, ASCVD 
	WHERE #GproPatientMonitor.lbpatientid = ASCVD.patientid
	AND RowNumber = 1;


WITH LDL190 AS  
	(  
	  SELECT DISTINCT PLO.PatientID
	  FROM VWPatientLabOrder PLO 
	  JOIN VWPatientLabResult PLR
	  ON PLO.PatientLabOrderId = PLR.PatientLabOrderId
	  JOIN #GproPatientMonitor 
	  ON #GproPatientMonitor.lbpatientid = PLO.patientid
	  JOIN [#GPROValueSetCodes] AVC
	  ON   AVC.[Code] = PLR.ObservationCode1
	  AND AVC.ValueSetName = 'LDL_CODE' 
	  AND PLR.ObservationDate < @MeasurePeriodEnd
	  AND isnull(try_convert(Int,PLR.Value),0)  >=190
	  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID] 
  	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.LDL190PatientEligibleInd = 1
	FROM #GproPatientMonitor, LDL190 
	WHERE #GproPatientMonitor.lbpatientid = LDL190.patientid;


WITH LDL70189 AS  
	(  
	SELECT PLO.PatientID,
	isnull(Max(try_convert(Int,PLR.Value)),0) AS LDL 	 
	FROM VWPatientLabOrder PLO 
	JOIN VWPatientLabResult PLR
	ON PLO.PatientLabOrderId = PLR.PatientLabOrderId
	JOIN #GproPatientMonitor 
	ON #GproPatientMonitor.lbpatientid = PLO.patientid	  
	JOIN [#GPROValueSetCodes] AVC
	ON   AVC.[Code] = PLR.ObservationCode1
	AND AVC.ValueSetName = 'LDL_CODE' 
	AND PLR.ObservationDate  >=  DATEADD (YEAR,-2, @MeasurePeriodBegin)
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID] 
	GROUP BY plo.PatientID
	HAVING   isnull(Max(try_convert(Int,PLR.Value)),0) between 70 and 189 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.LDL70189PatientEligibleInd = 1
	FROM #GproPatientMonitor, LDL70189 
	WHERE #GproPatientMonitor.lbpatientid = LDL70189.patientid;



	IF OBJECT_ID( 'tempdb..#StatinPatients' ) IS NOT NULL DROP TABLE [#StatinPatients];
	SELECT PatientID,rxnormCode,MedicationStartDate
	INTO #StatinPatients
	FROM vwpatientmedication PM
	 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PM.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pm.rxnormCode = AVC.[Code]
				   AND AVC.ValueSetName IN ('STATIN_DRUG_CODE')
				   AND AVC.codesystem in ('RXNORM')
	   			   AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @MeasurePeriodEnd
				   AND IsNull( pm.[MedicationEndDate], @MeasurePeriodBegin ) >= @MeasurePeriodBegin
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PM.[SourceSystemID] 
		UNION
		SELECT PatientID,NDCCode,MedicationStartDate
		FROM vwpatientmedication PM
	 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PM.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pm.NDCCode = AVC.[Code]
				   AND AVC.ValueSetName IN ('STATIN_DRUG_CODE')
				   AND AVC.codesystem in ('NDC')
	           		AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @MeasurePeriodEnd
					AND IsNull( pm.[MedicationEndDate], @MeasurePeriodBegin ) >= @MeasurePeriodBegin
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PM.[SourceSystemID] 
		UNION
		SELECT PatientID,procedurecode,ProcedureDateTime
			FROM vwpatientprocedure pp 
	 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PP.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pp.Procedurecode = AVC.[Code]
				   AND AVC.ValueSetName IN ('STATIN_DRUG_CODE')
				   AND AVC.codesystem in ('C4')
				  AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
				  AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID];	 


	
	WITH STATIN AS  
	(  
		SELECT PatientID,rxnormCode,MedicationStartDate,	
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY MedicationStartDate DESC) AS RowNumber
		FROM #StatinPatients PM
	)
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.PatientOnStatin = 1,
	#GproPatientMonitor.StatinCode = STATIN.rxnormCode, 
	#GproPatientMonitor.StatinStartDate = STATIN.MedicationStartDate
	FROM #GproPatientMonitor, STATIN 
	WHERE #GproPatientMonitor.lbpatientid = STATIN.patientid
	and RowNumber = 1  ;



	---------------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--CAD 7 Information
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.CAD7EligibleInd = 1,
	#GproPatientMonitor.CAD7Numerator = CMeasure_Detail.Numerator,
	#GproPatientMonitor.CAD7Exclusion = CMeasure_Detail.Exclusion
	FROM #GproPatientMonitor, CMeasure_Detail
	WHERE #GproPatientMonitor.lbpatientid = CMeasure_Detail.LbPatientID 
	AND measureid = @CAD7;



	DELETE FROM [#GPROValueSetCodes];

	INSERT INTO [#GPROValueSetCodes]
	  (
		 ValueSetName ,
		 CodeSystem ,
		 Code 
	  )
	SELECT VariableName ,
		 CodeSystem ,
		 Code
	  --FROM [EdwReferenceData].[dbo].[GPROValueSetsCodes]
	FROM vwGProValueSet
	WHERE  [ModuleType] = 'CAD' and Year = '2016';



	WITH LatestCAD AS  
	(  
	SELECT 
		Patientid,DiagnosisDatetime,DiagnosisCode,
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY DiagnosisDatetime DESC) AS RowNumber
	FROM vwpatientdiagnosis pd
		 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = pd.patientid
	JOIN [#GPROValueSetCodes] AVC 
	ON pd.diagnosiscode = AVC.Code
	AND AVC.ValueSetName  IN ('CAD_DX_CODE')
	AND pd.[DiagnosisDateTime] < @MeasurePeriodEnd
	JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID] 
	)   
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.CADDate = LatestCAD.DiagnosisDatetime,
	#GproPatientMonitor.CADCode = LatestCAD.DiagnosisCode
	FROM #GproPatientMonitor, LatestCAD 
	WHERE #GproPatientMonitor.lbpatientid = LatestCAD.patientid;




	IF OBJECT_ID( 'tempdb..#ACEARB' ) IS NOT NULL DROP TABLE [#ACEARB];
	SELECT PatientID,rxnormCode,MedicationStartDate
	INTO #ACEARB
	FROM vwpatientmedication PM
	 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PM.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pm.rxnormCode = AVC.[Code]
				   AND AVC.ValueSetName IN ('ACE_ARB_CODE')
				   AND AVC.codesystem in ('RXNORM')
				   AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @MeasurePeriodEnd
				   AND IsNull( pm.[MedicationEndDate], @MeasurePeriodBegin ) >= @MeasurePeriodBegin
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PM.[SourceSystemID] 
		UNION
		SELECT PatientID, NDCCode, MedicationStartDate
		FROM vwpatientmedication PM
	 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PM.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pm.NDCCode = AVC.[Code]
				   AND AVC.ValueSetName IN ('ACE_ARB_CODE')
				   AND AVC.codesystem in ('NDC')
	    	   		AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @MeasurePeriodEnd
					AND IsNull( pm.[MedicationEndDate], @MeasurePeriodBegin ) >= @MeasurePeriodBegin
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PM.[SourceSystemID] 
		UNION
		SELECT PatientID,procedurecode,proceduredatetime
			FROM vwpatientprocedure PP
	 JOIN #GproPatientMonitor 
			On #GproPatientMonitor.lbpatientid = PP.patientid
			  JOIN [#GPROValueSetCodes] AVC 
			   on pp.Procedurecode = AVC.[Code]
				   AND AVC.ValueSetName IN ('ACE_ARB_CODE')
				   AND AVC.codesystem in ('C4')
	 			   AND pp.[ProcedureDateTime] >= @MeasurePeriodBegin
				   AND pp.[ProcedureDateTime] <  @MeasurePeriodEnd
			  JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID];


	WITH ACEARB AS  
	(  
		SELECT PatientID,rxnormCode,MedicationStartDate,	
		ROW_NUMBER() OVER (Partition by Patientid ORDER BY MedicationStartDate DESC) AS RowNumber
		FROM #ACEARB 
	)
	UPDATE #GproPatientMonitor 
	SET #GproPatientMonitor.PatientOnACEorARB = 1,
	#GproPatientMonitor.ACEorARBCode = ACEARB.rxnormCode, 
	#GproPatientMonitor.ACEorARBStartDate = ACEARB.MedicationStartDate
	FROM #GproPatientMonitor, ACEARB 
	WHERE #GproPatientMonitor.lbpatientid = ACEARB.patientid
	and RowNumber = 1;

	

	--SELECT lbpatientid,DiabeticPatientEligibleInd,DiabeticCode,DiabeticDate,
	--CAD7EligibleInd,CAD7Numerator,CAD7Exclusion,CADDate,CADCode,
	--LVSDPatientInd, LVSDCode,LVSDDate ,LVSDResult ,PatientOnACEorARB
	--FROM #GproPatientMonitor
	--WHERE cad7Eligibleind = 1


	Truncate Table GproPatientMonitor

	Insert into GproPatientMonitor
	(lbpatientid,Memberfullname,PatientAge,PatientStatus,ContractName,Gender,BirthDate, 
	Prev7EligibleInd,InfluenzaLastPerformedDate,InfluenzaCode,InfluenzaDueDate,
	Prev7Numerator,Prev7Exclusion,Prev8EligibleInd,PnueVaxLastPerformedDate,PnueVaxCode,
	PnueVaxDueDate,Prev8Numerator,Prev8Exclusion,Prev5EligibleInd,
	BreastExamLastPerformedDate,BreastExamCode,BreastExamDueDate,Prev5Numerator,
	Prev5Exclusion,Prev6EligibleInd,ColonExamLastPerformedDate,ColonExamDueDate,
	ColonExamCode,Prev6Numerator,Prev6Exclusion,DiabeticPatientEligibleInd,
	DiabeticDate,DiabeticCode,DM2EligibleInd ,HemoglobinA1cTestLastPerformedDate,HemoglobinA1cTestCode,
	HemoglobinA1cTestValue,HemoglobinA1cTestDueDate,DM2Numerator,DM2Exclusion,
	Prev9EligibleInd,BMILastReadingDate,BMILastReadingHeight,BMILastReadingWeight,
	BMIReadingValue,BMINormal,BMIFollowupCode,BMIFollowupDate,Prev9Numerator,
	Prev9Exclusion,HTN2EligibleInd,HypertensionDate,HypertensionCode,HTN2Numerator,HTN2Exclusion,DM7EligibleInd,
	DiabeticEyeDateLastPerformed,DiabeticEyeCode,DM7Numerator,DM7Exclusion,BloodPressureReadingDate,BloodPressureSystolic,BloodPressureDiastolic,
	BloodPressureNormal,BloodPressureFollowupCode,BloodPressureFollowupDate,HF6EligibleInd,HeartFailureDate,HeartFailureCode,
	HF6Numerator,HF6Exclusion,Care2EligibleInd,FallScreenCode,FallScreenDate,FallScreenResult,
	Care2Numerator,Care2Exclusion,DepressionPatientEligibleInd,DepressionCode,DepressionDate,DepressionLatestPHQ9Code,
	DepressionLatestPHQ9ScreeningDate,DepressionLatestPHQ9ScreenScore,DepressionInitialPHQ9Code, 
	DepressionInitialPHQ9ScreeningDate, DepressionInitialPHQ9ScreenScore ,LVSDPatientInd,LVSDCode,LVSDDate,
	LVSDResult,PatientOnACEorARB,ACEorARBStartDate,ACEorARBCode,CAD7EligibleInd,
	CADCode,CADDate,CAD7Numerator,CAD7Exclusion,PatientOnAspirin,AspirinStartDate,AspirinCode,PatientOnBetaBlocker,
	BetaBlockerStartDate,BetaBlockerCode,PatientOnStatin,StatinStartDate,StatinCode,
	PREV13EligibleInd,PREV13Numerator,PREV13Exclusion,
	PREV13BEligibleInd,PREV13BNumerator, PREV13BExclusion,
	PREV13CEligibleInd, PREV13CNumerator, PREV13CExclusion,
	ASCVDPatientEligibleInd,ASCVDCode, ASCVDDate,
	LDL190PatientEligibleInd,LDL70189PatientEligibleInd,
	IVDEligibleInd,IVDNumerator,IVDExclusion,MH1EligibleInd,
	MH1Numerator,MH1Exclusion,PREV10EligibleInd,PREV10Numerator,PREV10Exclusion,PREV10TobaccoScreeningDate,PREV10TobaccoScreeningCode,
	PREV10TobaccoCounselingDate,PREV10TobaccoCounselingCode,PatientIsSmokerInd,PREV11EligibleInd,PREV11Numerator,PREV11Exclusion,
	PREV12EligibleInd,PREV12Numerator,PREV12Exclusion,Care3EligibleInd, Care3Numerator, Care3Exclusion,
	DepressionScreeningPatientEligibleInd,DepressionScreenCode, DepressionScreenDate,
	DepressionScreenPositiveInd,DepressionScreenFollowUpInd,DepressionScreenFollowUpCode,DepressionScreenFollowUpDate,
	LastEncounterDate,LastEncounterCode,SecondLastEncounterDate,SecondlastEncounterCode,
	CreateLbUserId,ModifyLbUserId,createdatetime,modifydatetime)
	SELECT  lbpatientid,Memberfullname,PatientAge,PatientStatus,ContractName,Gender,BirthDate, 
	Prev7EligibleInd,InfluenzaLastPerformedDate,InfluenzaCode,InfluenzaDueDate,Prev7Numerator,Prev7Exclusion,Prev8EligibleInd,
	PnueVaxLastPerformedDate,PnueVaxCode,PnueVaxDueDate,Prev8Numerator,Prev8Exclusion,Prev5EligibleInd,BreastExamLastPerformedDate,
	BreastExamCode,BreastExamDueDate,Prev5Numerator,Prev5Exclusion,Prev6EligibleInd,ColonExamLastPerformedDate,ColonExamDueDate,
	ColonExamCode,Prev6Numerator,Prev6Exclusion,DiabeticPatientEligibleInd,DiabeticDate,DiabeticCode,
	DM2EligibleInd ,HemoglobinA1cTestLastPerformedDate,HemoglobinA1cTestCode,HemoglobinA1cTestValue,HemoglobinA1cTestDueDate,
	DM2Numerator,DM2Exclusion,Prev9EligibleInd,BMILastReadingDate,BMILastReadingHeight,BMILastReadingWeight,BMIReadingValue,
	BMINormal,BMIFollowupCode,BMIFollowupDate,Prev9Numerator,Prev9Exclusion,HTN2EligibleInd,HypertensionDate,HypertensionCode,
	HTN2Numerator,HTN2Exclusion,DM7EligibleInd,DiabeticEyeDateLastPerformed,DiabeticEyeCode,DM7Numerator,DM7Exclusion,
	BloodPressureReadingDate,BloodPressureSystolic,BloodPressureDiastolic,BloodPressureNormal,BloodPressureFollowupCode,BloodPressureFollowupDate,
	HF6EligibleInd,HeartFailureDate,HeartFailureCode,HF6Numerator,HF6Exclusion,Care2EligibleInd,FallScreenCode,
	FallScreenDate,FallScreenResult,Care2Numerator,Care2Exclusion,DepressionPatientEligibleInd,DepressionCode,
	DepressionDate,DepressionLatestPHQ9Code,DepressionLatestPHQ9ScreeningDate,DepressionLatestPHQ9ScreenScore,
	DepressionInitialPHQ9Code, DepressionInitialPHQ9ScreeningDate, DepressionInitialPHQ9ScreenScore ,LVSDPatientInd,LVSDCode,
	LVSDDate,LVSDResult,PatientOnACEorARB,ACEorARBStartDate,ACEorARBCode,CAD7EligibleInd,CADCode,
	CADDate,CAD7Numerator,CAD7Exclusion,PatientOnAspirin,AspirinStartDate,AspirinCode,
	PatientOnBetaBlocker,BetaBlockerStartDate,BetaBlockerCode,PatientOnStatin,StatinStartDate,StatinCode,
	PREV13EligibleInd,PREV13Numerator,PREV13Exclusion,
	PREV13BEligibleInd,PREV13BNumerator, PREV13BExclusion,
	PREV13CEligibleInd, PREV13CNumerator, PREV13CExclusion,
	ASCVDPatientEligibleInd,ASCVDCode, ASCVDDate,
	LDL190PatientEligibleInd,LDL70189PatientEligibleInd,
	IVDEligibleInd,IVDNumerator,IVDExclusion,MH1EligibleInd,
	MH1Numerator,MH1Exclusion,PREV10EligibleInd,PREV10Numerator,PREV10Exclusion,PREV10TobaccoScreeningDate,PREV10TobaccoScreeningCode,
	PREV10TobaccoCounselingDate,PREV10TobaccoCounselingCode,PatientIsSmokerInd,PREV11EligibleInd,PREV11Numerator,PREV11Exclusion,PREV12EligibleInd,
	PREV12Numerator,PREV12Exclusion,Care3EligibleInd, Care3Numerator, Care3Exclusion,
	DepressionScreeningPatientEligibleInd,DepressionScreenCode, DepressionScreenDate,
	DepressionScreenPositiveInd,DepressionScreenFollowUpInd,DepressionScreenFollowUpCode,DepressionScreenFollowUpDate,
	LastEncounterDate,LastEncounterCode,SecondLastEncounterDate,SecondlastEncounterCode,
	CreateLbUserId,ModifyLbUserId,createdatetime,modifydatetime
	FROM #GproPatientMonitor


END
GO
