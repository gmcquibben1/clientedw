SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GProPatientStatusSet]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		12.30.2015
	INITIAL VER:	1.6.1
	MODULE:			GPRO		
	DESCRIPTION:	Set the over all patient-level status for every ranked GPRO patient
	PARAMETERS:		none
	RETURN VALUE(s)/OUTPUT:	none
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.6.1		12.30.2015		BR						Initial Version
	2.2.1		11.08.2016		BR			LBPP-2028	Refactored to include the statin measure 
														allow this sp to be called for a single patient or all patients
=================================================================*/
(
	@GproPatientRankingId INT = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

	CREATE TABLE #patientList
	(
		PatientId INT 
	)

	CREATE TABLE #completeList
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
	);

	CREATE TABLE #skippedList
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
	);

	-------------------------------------------------------------
	-- find the set of patients to update
	-------------------------------------------------------------
	IF @GproPatientRankingId IS NULL
		INSERT INTO #patientList (PatientId) SELECT LbPatientId FROM GproPatientRanking;
	ELSE
		INSERT INTO #patientList (PatientId) SELECT LbPatientId FROM GproPatientRanking WHERE GproPatientRankingId = @GproPatientRankingId;

	-------------------------------------------------------------
	-- Get the total set of skipped and complete measures
	-------------------------------------------------------------
	INSERT INTO #completeList EXEC GproGetCompletePatientList;
	INSERT INTO #skippedList EXEC GproGetSkippedPatientList;

	-------------------------------------------------------------
	--default every patient to Incomplete
	-------------------------------------------------------------
	UPDATE gpr
		SET gpr.ModifyDateTime = GETUTCDATE(), gpr.GproStatusTypeId = (SELECT GProStatusTypeId FROM GProStatusType WHERE Name = 'Incomplete')
	FROM GproPatientRanking gpr
	INNER JOIN #patientList pl ON pl.PatientId = gpr.LbPatientId;

	-------------------------------------------------------------
	-- Any patient where every measure is skipped should be skipped
	-------------------------------------------------------------
	UPDATE gpr
		SET gpr.ModifyDateTime = GETUTCDATE(), gpr.GproStatusTypeId = (SELECT GProStatusTypeId FROM GProStatusType WHERE Name = 'Skipped')
	FROM GProPatientRanking gpr
		INNER JOIN GProPatientMeasure gpm ON gpm.GProPatientRankingId = gpr.GProPatientRankingId
		INNER JOIN #patientList pl ON pl.PatientId = gpr.LbPatientId
	WHERE gpm.MedicalRecordFound IN (3, 9)
		OR (gpm.MedicalRecordFound = 2
			AND EXISTS (SELECT TOP 1 1 FROM #skippedList s WHERE s.LbPatientId = pl.PatientId )
			AND NOT EXISTS (SELECT TOP 1 1 FROM #completeList c WHERE c.LbPatientId = pl.PatientId )
			AND NOT EXISTS (SELECT TOP 1 1 FROM GproPatientRankingGproMeasureType gmt 
				WHERE gmt.GproPatientRankingId = gpr.GproPatientRankingId AND NOT EXISTS (SELECT TOP 1 1 FROM #completeList c2 WHERe c2.LbPatientId = pl.PatientId AND c2.GproMeasureTypeId = gmt.GproMeasureTypeId) 
																		  AND NOT EXISTS (SELECT TOP 1 1 FROM #skippedList s2 WHERe s2.LbPatientId = pl.PatientId AND s2.GproMeasureTypeId = gmt.GproMeasureTypeId)
							)
			);

	-------------------------------------------------------------
	-- Any patient that has no incomplete measures and at least one complete measure is complete
	-------------------------------------------------------------
	UPDATE gpr
		SET gpr.GproStatusTypeId = (SELECT GProStatusTypeId FROM GProStatusType WHERE Name = 'Complete')
	FROM GProPatientRanking gpr
		INNER JOIN GProPatientMeasure gpm ON gpm.GProPatientRankingId = gpr.GProPatientRankingId
		INNER JOIN #patientList pl ON pl.PatientId = gpr.LbPatientId
	WHERE (gpm.MedicalRecordFound = 2
			AND EXISTS (SELECT TOP 1 1 FROM #completeList c WHERE c.LbPatientId = pl.PatientId )
			AND NOT EXISTS (SELECT TOP 1 1 FROM GproPatientRankingGproMeasureType gmt 
				WHERE gmt.GproPatientRankingId = gpr.GproPatientRankingId AND NOT EXISTS (SELECT TOP 1 1 FROM #completeList c2 WHERe c2.LbPatientId = pl.PatientId AND c2.GproMeasureTypeId = gmt.GproMeasureTypeId) 
																		  AND NOT EXISTS (SELECT TOP 1 1 FROM #skippedList s2 WHERe s2.LbPatientId = pl.PatientId AND s2.GproMeasureTypeId = gmt.GproMeasureTypeId)
							)
			);

END
GO
