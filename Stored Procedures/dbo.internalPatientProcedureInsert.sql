SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalPatientProcedureInsert]
	@LbPatientID INT
  ,@SourceSystemID INT = -1
  ,@ProcedureCodingSystemTypeId INT = -1
	,@Procedure_code VARCHAR(255)
	,@ProcedureDateTime datetime
	,@Comments VARCHAR(255) = NULL
	,@Description VARCHAR(255) = ''
	,@ProcedureCodeModifier1 VARCHAR(255) = NULL
	,@ProcedureCodeModifier2 VARCHAR(255) = NULL
	,@ProcedureCodeModifier3 VARCHAR(255) = NULL
	,@ProcedureCodeModifier4 VARCHAR(255) = NULL
AS


	
--LbPatientID, Procedure code, Source System ID, Procedure DateTime, Comments, Description

--DECLARE @LbPatientID INT = '87371'
--,@SourceSystemID INT = -1
--,@ProcedureCodingSystemTypeId INT = -1
--,@Procedure_code VARCHAR(255) = 'G0123'
--,@ProcedureDateTime datetime = 'Apr 30 2015 12:00AM'
--,@Comments VARCHAR(255) = ''
--,@Description VARCHAR(255) = ''
--,@ProcedureCodeModifier1 VARCHAR(255) = NULL
--,@ProcedureCodeModifier2 VARCHAR(255) = NULL
--,@ProcedureCodeModifier3 VARCHAR(255) = NULL
--,@ProcedureCodeModifier4 VARCHAR(255) = NULL

DECLARE
@PatientProcedureId INT = NULL
,@ProcedureCodeId int = NULL

--add @SourceSystemID
--add @ProcedureCodingSystemTypeId
IF (@SourceSystemID = -1)
BEGIN
	SET @SourceSystemID = 1
END
--print @SourceSystemID

IF @ProcedureCodingSystemTypeId = -1
BEGIN
	SET @ProcedureCodingSystemTypeId = 1
END
--print @ProcedureCodingSystemTypeId
--IF @Description = '-1'
--BEGIN
--	SET @Description = ''
--END
--print @Description

BEGIN

--internalProcedureCodeInsert 


	--DECLARE @PatientProcedureId INT ,@ProcedureCodeId int
	
	IF NOT EXISTS
	(
		SELECT DISTINCT pp.[PatientProcedureId]
		FROM [dbo].[PatientProcedure] pp 
		INNER JOIN [dbo].[PatientProcedureProcedureCode] pppc ON pp.[PatientProcedureId] = pppc.[PatientProcedureId]
		INNER JOIN [dbo].[ProcedureCode] pc on pppc.procedurecodeid = pc.procedurecodeid
		where pc.[ProcedureCode] = @Procedure_code
		and pp.patientID = @LbPatientID
		and pp.[ProcedureDateTime] = @ProcedureDateTime
		and PP.sourcesystemID = @SourceSystemID
	)

	BEGIN


	--/*

		EXEC internalProcedureCodeInsert 
			@ProcedureCodeId OUTPUT , 
			@ProcedureCodingSystemTypeId, 
			@Procedure_code,
			@Description 

			--print @ProcedureCodeId

		--*/

		--*********************************************************************************
		--insert into PatientProcedure table
		--/*		

		SET NOCOUNT ON
			SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
			DECLARE @intError INTEGER, 
					@DeleteInd BIT = 0,
					@LBUserId INT = 1

			BEGIN TRAN

			INSERT INTO [dbo].PatientProcedure
			(
				[PatientID]
				,sourcesystemID
				,[ProcedureDateTime]
				,[ProcedureCodeModifier1]
				,[ProcedureCodeModifier2]
				,[ProcedureCodeModifier3]
				,[ProcedureCodeModifier4]
				,[DeleteInd]
				,[CreateDateTime]
				,[ModifyDateTime]
				,[CreateLBUserId]
				,[ModifyLBUserId]
			)
			VALUES 
			(
				@LbPatientID
				,@SourceSystemID
				,@ProcedureDateTime
				,@ProcedureCodeModifier1
				,@ProcedureCodeModifier2
				,@ProcedureCodeModifier3
				,@ProcedureCodeModifier4
				,@DeleteInd
				,GetDate()
				,GetDate()
				,@LBUserId
				,@LBUserId
			)
			SELECT @intError = @@ERROR,
			@PatientProcedureId = SCOPE_IDENTITY()

			IF @@ERROR <> 0 
			BEGIN
				ROLLBACK TRAN
				RETURN @intError
			END
			--print @PatientProcedureId
		--*/

		----*********************************************************************************
		----insert into [PatientProcedureProcedureCode] table
		----internalPatientProcedureProcedureCodeInsert

		----/*
		DECLARE @PatientProcedureProcedureCodeId INT

			INSERT INTO [dbo].PatientProcedureProcedureCode
				(
				[PatientProcedureId]
				,[ProcedureCodingSystemTypeId]
				,[ProcedureCodeId]
				)
				VALUES 
				(
					@PatientProcedureId
					,@ProcedureCodingSystemTypeId
					,@ProcedureCodeId
				)
				SELECT @intError = @@ERROR,
					@PatientProcedureProcedureCodeId = SCOPE_IDENTITY()

			IF @@ERROR <> 0 
			BEGIN
				ROLLBACK TRAN
			END
			ELSE
			BEGIN
				COMMIT TRAN
			END
		--print @PatientProcedureProcedureCodeId

			RETURN @intError

		--*/
	END --IF NOT EXIST
END
GO
