SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[internalETLCCLFDataLastUpdate]
AS
/*===============================================================
	CREATED BY:     Youping
	CREATED ON:     2017-02-17
	INITIAL VER:    2.2.1
	MODULE:	        ETL Data Load
	DESCRIPTION:    This stored procedure will calculate all of the last update values for CCLF1 to CCLF8
	PARAMETERS:		
	  		
    RETURN VALUE(s)/OUTPUT: NA
	
    MODIFICATIONS
	Version     Date            Author      JIRA            Change
	=======     ==========      ======      =========       =========	
	2.2.1	    2017-02-17      YL          LBETL-849       Initial
	 
=================================================================*/


BEGIN

SET NOCOUNT ON;


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128);
	DECLARE @dataSource VARCHAR(20) = 'history';
	DECLARE @ProcedureName VARCHAR(128) = 'internalETLCCLFDataLastUpdate';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @Today DATE=GETUTCDATE();

	DECLARE @TypeId INT 
	DECLARE @i INT;


  --get all CCLF tables
  CREATE TABLE #cclftablelist (id int identity(1,1), CCLFTable varchar(50));

  --check to see if ClinicalDataType has records for CCLF1 -CCLF8
  INSERT INTO #cclftablelist (CCLFTable)
  SELECT distinct a.name as CCLFTable
  FROM [dbo].[ClinicalDataType] a
  WHERE LEFT(a.[name],6) BETWEEN 'CCLF_1' AND 'CCLF_8' ;

  SET @i=@@Rowcount;

  IF @i = 0

  BEGIN
      ;
		  WITH CCLFtable AS (SELECT LEFT([name],6) AS CCLF, MIN(LEN([name])) as lens
		  FROM sys.objects WHERE [type]='U'
		  AND LEFT([name],6) BETWEEN 'CCLF_1' AND 'CCLF_8' 
		  GROUP BY left([name],6)
		  )
		  INSERT INTO #cclftablelist (CCLFTable)
		  SELECT distinct left(a.name,b.lens) as CCLFTable
		  FROM sys.objects a 
		  INNER JOIN CCLFtable b on left(a.name,6)=b.CCLF
		   WHERE [type]='U' AND LEFT([name],6) BETWEEN 'CCLF_1' AND 'CCLF_8' ;
          
		  INSERT INTO [dbo].[ClinicalDataType] ([NAME],[DisplayValue],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
                  SELECT CCLFTable, left(replace(CCLFTable,'_',' '),50) , 0, getdate(),getdate(),1,1
	          FROM #cclftablelist a
		  LEFT OUTER JOIN [dbo].[ClinicalDataType] b on a.CCLFTable=b.[name]
		  WHERE b.[ClinicalDataTypeID] is NULL;
		  SET @i=@@Rowcount;

  END


   -- select * from #cclftablelist ;

   --loop to update cclf 1 through cclf 8
   DECLARE @sqlQuery varchar(500), @CCLFTable varchar(50)
  
   WHILE @i >0
   BEGIN
   
	   SELECT @CCLFTable=CCLFTable FROM #cclftablelist WHERE id=@i;
       SELECT @TypeId=[ClinicalDataTypeID] from [dbo].[ClinicalDataType] where [NAME]=@CCLFTable;
       SET @EDWtableName=@CCLFTable
	   
	   -- get total records before merge 
	   SELECT @RecordCountBefore = (SELECT COUNT(1)  FROM [ClinicalDataLastUpdate] WITH (NOLOCK) WHERE [ClinicalDataTypeID] =  @TypeId );

	   IF OBJECT_ID('tmpCCLFlastupdate', 'U') IS NOT NULL
			  DROP TABLE tmpCCLFlastupdate
  
		set @sqlQuery='SELECT max([fileDate]) as fileDate,[LbPatientId]
		INTO tmpCCLFlastupdate  
		FROM '+@CCLFTable +' WHERE LbPatientId is NOT NULL
		GROUP BY [LbPatientId]'

		exec(@sqlQuery);
		
	    SET @RecordCountSource =@@RowCount;
	
		CREATE CLUSTERED INDEX IDX_tmpCCLFlastupdate_PatientID ON tmpCCLFlastupdate (LbPatientId);

			
		MERGE INTO [ClinicalDataLastUpdate] AS target
	        USING tmpCCLFlastupdate AS source
	        ON target.PatientId = source.LBPatientId AND target.[ClinicalDataTypeID] = @TypeId
	        WHEN MATCHED THEN 
			     UPDATE SET target.ModifyDateTime = source.fileDate
	        WHEN NOT MATCHED BY target THEN
		        INSERT (PatientID, ModifyDateTime,[ClinicalDataTypeID])
		         VALUES (source.LbPatientID,source.fileDate,@TypeId);
		
		    set @i=@i -1
	       
		   	-- get total records after merge 
	       SELECT @RecordCountAfter=( SELECT COUNT(1)  FROM [ClinicalDataLastUpdate] WITH (NOLOCK) WHERE [ClinicalDataTypeID] =  @TypeId )

		   		-- Calculate records inserted and updated counts
			SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
			SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

			-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			SET  @EndTime=GETUTCDATE();
			EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;



	        DROP TABLE tmpCCLFlastupdate;
	
	END

	DROP TABLE #cclftablelist


END
GO
