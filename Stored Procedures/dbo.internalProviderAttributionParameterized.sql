SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalProviderAttributionParameterized] 
		@PatientContractFilter VARCHAR(100) = NULL,  @ProviderTypeFilter VARCHAR(100) = NULL,
		@SourceSpottingFilter VARCHAR(100), @TimeWindowFilter DateTime = NULL,
		@RankingBySum BIT = 0, @RankingByCount BIT = 0, @RankingUseOfPcpCodes BIT = 0, @RankingUseOfCCLF BIT = 0 , @RankingUseOfClinical BIT = 0 
AS
BEGIN
-- DEBUG 
-- EXEC [dbo].[internalProviderAttributionParameterized] @PatientContractFilter = NULL, @ProviderTypeFilter = NULL, @SourceSpottingFilter = NULL ,
--		@TimeWindowFilter = NULL, @RankingBySum = 0, @RankingByCount = 0, @RankingUseOfPcpCodes = 0, @RankingUseOfCCLF = 0 , @RankingUseOfClinical = 0 
--DECLARE @PatientContractFilter VARCHAR(100) = NULL,  @ProviderTypeFilter VARCHAR(100) = NULL,
--		@SourceSpottingFilter VARCHAR(100), @TimeWindowFilter DateTime = NULL,
--		@RankingBySum BIT = 0, @RankingByCount BIT = 0, @RankingUseOfPcpCodes BIT = 0, @RankingUseOfCCLF BIT = 0 , @RankingUseOfClinical BIT = 0 
--SET @SourceSpottingFilter = 'MSSP' SET  @RankingByCount = 1  SET @RankingUseOfClinical = 1 

CREATE TABLE #Pats		(LbpatientId INT,HICNO VARCHAR(100))					-- DROP TABLE #Pats
CREATE TABLE #Provs		(NPI VARCHAR(100))										-- DROP TABLE #Provs
CREATE TABLE #PatProvs  (LbpatientId INT,HICNO VARCHAR(100),NPI VARCHAR(100))	-- DROP TABLE #PatProvs	
CREATE TABLE #PatProvsRanked  (HICNO VARCHAR(100),NPI VARCHAR(100), RANKNO INT)	-- DROP TABLE #PatProvsRanked

-- LIST OF ELIGIBLE PATIENTS 
INSERT INTO #Pats (LbpatientId, HICNO)
SELECT PIR.IdValue, Me.medicareNo 
FROM MemberExtract ME 
INNER JOIN PatientIdsRegistry PIR ON ME.family_id = PIR.Family_Id AND PIR.IdType = 'LbPatientId'
WHERE Me.Source = ISNULL(@PatientContractFilter,ME.Source) AND Me.ProviderId IS NULL 

-- LIST OF ELIGIBLE PROVIDERS 
INSERT INTO #Provs (NPI)
SELECT PE.ProviderNPI
FROM ProviderExtract PE 
INNER JOIN vwPortal_Provider VPP ON LTRIM(RTRIM(PE.ProviderNPI)) = LTRIM(RTRIM(VPP.NationalProviderIdentifier)) 
WHERE ISNULL(VPP.OtherReferenceIdentifier,'') = ISNULL(@ProviderTypeFilter,ISNULL(VPP.OtherReferenceIdentifier,''))  

-- MRA
IF @SourceSpottingFilter = 'MRA' 
BEGIN
	;WITH CTE0 AS (
		SELECT HICNO , FileInputName, FileTimeLine, ROW_NUMBER() OVER(PARTITION BY HICNO ORDER BY fileTimeline DESC  ) SNO 
		FROM [MemberRosterArchives_ProviderTab]
	)
	INSERT INTO #PatProvs (LbpatientId,HICNO,NPI)
	SELECT DISTINCT A.LbpatientId, MRA.HICNO, [Individual NPI] ProviderNPI
	FROM #Pats A 
	INNER JOIN  [MemberRosterArchives_ProviderTab] MRA ON A.HICNO = MRA.HICNO 
	INNER JOIN #Provs B ON MRA.[Individual NPI] = B.NPI
	INNER JOIN CTE0 ON MRA.HICNO = CTE0.HICNO AND MRA.FileInputName = CTE0.FileInputName AND CTE0.SNO = 1
END 

-- CCLF
IF @SourceSpottingFilter = 'CCLF' 
BEGIN
	INSERT INTO #PatProvs (LbpatientId,HICNO,NPI)
	SELECT DISTINCT  A.LbpatientId, MRA.BENE_HIC_NUM, RNDRG_PRVDR_NPI_NUM ProviderNPI
	FROM #Pats A 
	INNER JOIN  (SELECT DISTINCT BENE_HIC_NUM, RNDRG_PRVDR_NPI_NUM  
				 FROM CCLF_5_PartB_Physicians WHERE CLM_FROM_DT <= ISNULL(@TimeWindowFilter,CLM_FROM_DT) 
				)MRA ON A.HICNO = MRA.BENE_HIC_NUM 
	INNER JOIN #Provs B ON MRA.RNDRG_PRVDR_NPI_NUM = B.NPI

	INSERT INTO #PatProvs (LbpatientId,HICNO,NPI)
	SELECT DISTINCT  A.LbpatientId, MRA.BENE_HIC_NUM, OPRTG_PRVDR_NPI_NUM ProviderNPI
	FROM #Pats A 
	INNER JOIN  (SELECT DISTINCT BENE_HIC_NUM, OPRTG_PRVDR_NPI_NUM  
				 FROM CCLF_1_PartA_Header WHERE CLM_FROM_DT <= ISNULL(@TimeWindowFilter,CLM_FROM_DT) 
				) MRA ON A.HICNO = MRA.BENE_HIC_NUM 
	INNER JOIN #Provs B ON MRA.OPRTG_PRVDR_NPI_NUM = B.NPI
END

-- CLINICAL
IF @SourceSpottingFilter = 'CLINICAL' 
BEGIN
	INSERT INTO #PatProvs (LbpatientId,HICNO,NPI)
	SELECT DISTINCT  A.LbpatientId, A.HICNO, ProviderNPI
	FROM #Pats A 
	INNER JOIN  (SELECT DISTINCT PatientId, PerformedbyClinician  ProviderNPI
				 FROM PatientProcedure WHERE ProcedureDateTime <= ISNULL(@TimeWindowFilter,ProcedureDateTime) 
				)MRA ON A.LbpatientId = MRA.PatientId 
	INNER JOIN #Provs B ON MRA.ProviderNPI = B.NPI
END

-- IPA
IF @SourceSpottingFilter = 'IPA' 
BEGIN
	INSERT INTO #PatProvs (LbpatientId,HICNO,NPI)
	SELECT DISTINCT  A.LbpatientId, A.HICNO, Y.ProviderNPI ProviderNPI
	FROM #Pats A 
	INNER JOIN (
			SELECT IPM.UToken, IPA.ProviderNPI,
			ROW_NUMBER() OVER(Partition by UToken ORDER BY IPA.CreateDateTime DESC) SNO 
			FROM vwPortal_InterfacePatientAttribution  IPA
			INNER JOIN vwPortal_InterfacePatientMatched IPM ON IPA.PatientIdentifier = IPM.PatientIdentifier
			WHERE IPA.ProviderNPI <> '' 
	) Y  ON A.HICNO = Y.UToken  WHERE SNO = 1  -- UNCOMMENT TO NOT DO SO 
END 

-- CUSTOM IMPLEMENTATION
IF @SourceSpottingFilter LIKE 'CUSTOM.%.%.%.%' -- THIS IS ADHOC AND DYNAMIC, CUSTOM.<ROSTER TABLE NAME>.<Member ID Column Name>.<Prodivder NPI Column Name>.<Ordering Column>
BEGIN
	DECLARE @DynSql VARCHAR(MAX), @TableName VARCHAR(100), @MemberIdColumn VARCHAR(100), @ProviderIdColumn VARCHAR(100), @OrderingColumn VARCHAR(100)
	SET @TableName			= SUBSTRING(@SourceSpottingFilter,8,CHARINDEX('.',@SourceSpottingFilter,9)-8)

	SELECT TOP 1 @MemberIdColumn	= SUBSTRING(@SourceSpottingFilter, P2.Pos + 1, P3.Pos - P2.Pos - 1) ,
				 @ProviderIdColumn	= SUBSTRING(@SourceSpottingFilter, P3.Pos + 1, P4.Pos - P3.Pos - 1) ,
				 @OrderingColumn	= SUBSTRING(@SourceSpottingFilter, P4.Pos + 1, 1000) 
	FROM SYS.servers
	CROSS APPLY (SELECT (charindex('.', @SourceSpottingFilter))) as P1(Pos)
	CROSS APPLY (SELECT (charindex('.', @SourceSpottingFilter, P1.Pos+1))) as P2(Pos)
	CROSS APPLY (SELECT (charindex('.', @SourceSpottingFilter, P2.Pos+1))) as P3(Pos)
	CROSS APPLY (SELECT (charindex('.', @SourceSpottingFilter, P3.Pos+1))) as P4(Pos)

	SET @DynSql = '
	INSERT INTO #PatProvs (LbpatientId,HICNO,NPI)
	SELECT LbpatientId , HICNO, ProviderNPI 
	FROM (
	SELECT A.LbpatientId, A.HICNO, Y.' + @ProviderIdColumn + ' ProviderNPI, ROW_NUMBER() OVER(PARTITION BY A.HICNO ORDER BY ' + @OrderingColumn + ' DESC) SNO
	FROM #Pats A 
	INNER JOIN ' + @TableName + ' Y  ON Y.' + @MemberIdColumn + '  = A.HICNO  
	INNER JOIN #Provs B ON Y.' + @ProviderIdColumn + ' = B.NPI
		) X WHERE SNO = 1  '
	EXECUTE SP_EXECUTESQL @DynSQL   
END 

IF @RankingUseOfCCLF = 1 
BEGIN
	IF @RankingBySum = 1 
	BEGIN
		INSERT INTO #PatProvsRanked (HICNO ,NPI , RANKNO )
		SELECT BENE_HIC_NUM , NPI , ROW_NUMBER() OVER(PARTITION BY BENE_HIC_NUM ORDER BY CLM_SUM DESC, CLM_FROM_DT DESC ) AS RANKNO 
		FROM (
			SELECT BENE_HIC_NUM, NPI, SUM(CLM_SUM) AS CLM_SUM, MAX(CLM_FROM_DT) CLM_FROM_DT   
			FROM (
				SELECT 
				   BENE_HIC_NUM ,RNDRG_PRVDR_NPI_NUM AS NPI ,SUM(CLM_LINE_CVRD_PD_AMT) AS CLM_SUM, MAX(CLM_FROM_DT) CLM_FROM_DT         
				FROM #PatProvs PP 
				INNER JOIN CCLF_5_PartB_Physicians PARTB ON PP.HICNO = PARTB.BENE_HIC_NUM AND PP.NPI = PARTB.RNDRG_PRVDR_NPI_NUM	 
				WHERE CLM_FROM_DT >= ISNULL(@TimeWindowFilter,CLM_FROM_DT)
				AND (
						(EXISTS (SELECT 1 FROM RefTable_Wellness W WHERE PARTB.CLM_LINE_HCPCS_CD = W.code AND W.field = 'hcpc') AND @RankingUseOfPcpCodes = 1) OR @RankingUseOfPcpCodes = 0
					)
				GROUP BY BENE_HIC_NUM, RNDRG_PRVDR_NPI_NUM
				UNION
				SELECT 
				   PARTA.BENE_HIC_NUM  ,OPRTG_PRVDR_NPI_NUM AS NPI,SUM(CLM_LINE_CVRD_PD_AMT) AS CLM_SUM, MAX(PARTA.CLM_FROM_DT) CLM_FROM_DT             
				FROM #PatProvs PP  
				INNER JOIN CCLF_1_PartA_Header PARTA  ON PP.HICNO = PARTA.BENE_HIC_NUM AND PP.NPI = PARTA.OPRTG_PRVDR_NPI_NUM	
				INNER JOIN [CCLF_2_PartA_RCDetail] RCD ON PARTA.CUR_CLM_UNIQ_ID = RCD.CUR_CLM_UNIQ_ID
				WHERE PARTA.CLM_FROM_DT >= ISNULL(@TimeWindowFilter,PARTA.CLM_FROM_DT) 
				AND (
					 (@RankingUseOfPcpCodes = 1 AND (EXISTS(SELECT 1 FROM RefTable_Wellness W WHERE field = 'revcode' AND RCD.CLM_LINE_REV_CTR_CD = W.code) 
													 OR EXISTS(SELECT 1 FROM RefTable_Wellness  W WHERE field = 'hcpc AND RCD.CLM_LINE_HCPCS_CD = W.code')) )
					  OR @RankingUseOfPcpCodes = 0
					 )
				GROUP BY PARTA.BENE_HIC_NUM, OPRTG_PRVDR_NPI_NUM
			) X GROUP BY BENE_HIC_NUM, NPI
		) Y
	END
	IF @RankingByCount = 1 
	BEGIN
		INSERT INTO #PatProvsRanked (HICNO ,NPI , RANKNO )
		SELECT BENE_HIC_NUM , NPI , ROW_NUMBER() OVER(PARTITION BY BENE_HIC_NUM ORDER BY CLM_COUNT DESC, CLM_FROM_DT DESC) AS RANKNO 
		FROM (
			SELECT BENE_HIC_NUM, NPI, SUM(CLM_COUNT) AS CLM_COUNT, MAX(CLM_FROM_DT) CLM_FROM_DT   
			FROM (
				SELECT 
				   BENE_HIC_NUM ,RNDRG_PRVDR_NPI_NUM AS NPI ,COUNT(DISTINCT PARTB.CUR_CLM_UNIQ_ID) AS CLM_COUNT , MAX(CLM_FROM_DT) CLM_FROM_DT             
				FROM #PatProvs PP 
				INNER JOIN CCLF_5_PartB_Physicians PARTB ON PP.HICNO = PARTB.BENE_HIC_NUM AND PP.NPI = PARTB.RNDRG_PRVDR_NPI_NUM	 
				WHERE CLM_FROM_DT >= ISNULL(@TimeWindowFilter,CLM_FROM_DT)
				AND (
						(EXISTS (SELECT 1 FROM RefTable_Wellness W WHERE PARTB.CLM_LINE_HCPCS_CD = W.code AND W.field = 'hcpc') AND @RankingUseOfPcpCodes = 1) OR @RankingUseOfPcpCodes = 0
					)
				GROUP BY BENE_HIC_NUM, RNDRG_PRVDR_NPI_NUM
				UNION
				SELECT 
				   PARTA.BENE_HIC_NUM  ,OPRTG_PRVDR_NPI_NUM AS NPI,COUNT(DISTINCT PARTA.CUR_CLM_UNIQ_ID) AS CLM_COUNT , MAX(PARTA.CLM_FROM_DT) CLM_FROM_DT                 
				FROM #PatProvs PP  
				INNER JOIN CCLF_1_PartA_Header PARTA  ON PP.HICNO = PARTA.BENE_HIC_NUM AND PP.NPI = PARTA.OPRTG_PRVDR_NPI_NUM	
				INNER JOIN [CCLF_2_PartA_RCDetail] RCD ON PARTA.CUR_CLM_UNIQ_ID = RCD.CUR_CLM_UNIQ_ID
				WHERE PARTA.CLM_FROM_DT >= ISNULL(@TimeWindowFilter,PARTA.CLM_FROM_DT) 
				AND (
					 (@RankingUseOfPcpCodes = 1 AND (EXISTS(SELECT 1 FROM RefTable_Wellness W WHERE field = 'revcode' AND RCD.CLM_LINE_REV_CTR_CD = W.code) 
													 OR EXISTS(SELECT 1 FROM RefTable_Wellness  W WHERE field = 'hcpc AND RCD.CLM_LINE_HCPCS_CD = W.code')) )
					  OR @RankingUseOfPcpCodes = 0
					 )
				GROUP BY PARTA.BENE_HIC_NUM, OPRTG_PRVDR_NPI_NUM
			) X GROUP BY BENE_HIC_NUM, NPI
		) Y
	END
END

IF @RankingUseOfClinical = 1 
BEGIN
	IF @RankingBySum = 1 
	BEGIN
		INSERT INTO #PatProvsRanked (HICNO ,NPI , RANKNO )
		SELECT HICNO , NPI , ROW_NUMBER() OVER(PARTITION BY HICNO ORDER BY CLM_SUM DESC, CLM_FROM_DT DESC) AS RANKNO 
		FROM (
			SELECT HICNO, NPI, SUM(CLM_SUM) AS CLM_SUM  , MAX(CLM_FROM_DT) CLM_FROM_DT
			FROM (
				SELECT 
				   HICNO ,PerformedbyClinician AS NPI ,SUM(0) AS CLM_SUM  , MAX(ProcedureDateTime) CLM_FROM_DT            
				FROM #PatProvs PProv 
				INNER JOIN PatientProcedure PP ON PProv.LbpatientId = PP.PatientID AND PProv.NPI = PP.PerformedbyClinician
				INNER JOIN PatientProcedureProcedureCode PPPC ON PP.PatientID = PPPC.PatientProcedureId 
				INNER JOIN ProcedureCode PC ON PPPC.ProcedureCodeId = PC.ProcedureCodeId	 
				WHERE ProcedureDateTime >= ISNULL(@TimeWindowFilter,ProcedureDateTime)
				AND (
						(EXISTS (SELECT 1 FROM RefTable_Wellness W WHERE PC.ProcedureCode = W.code AND W.field = 'hcpc') AND @RankingUseOfPcpCodes = 1) OR @RankingUseOfPcpCodes = 0
					)
				GROUP BY HICNO, PerformedbyClinician
			) X GROUP BY HICNO, NPI
		) Y
	END
	IF @RankingByCount = 1 
	BEGIN
		INSERT INTO #PatProvsRanked (HICNO ,NPI , RANKNO )
		SELECT HICNO , NPI , ROW_NUMBER() OVER(PARTITION BY HICNO ORDER BY CLM_SUM DESC, CLM_FROM_DT DESC) AS RANKNO 
		FROM (
			SELECT HICNO, NPI, SUM(CLM_SUM) AS CLM_SUM , MAX(CLM_FROM_DT) CLM_FROM_DT 
			FROM (
				SELECT 
				   HICNO ,PerformedbyClinician AS NPI ,COUNT(DISTINCT PP.PatientProcedureId) AS CLM_SUM, MAX(ProcedureDateTime) CLM_FROM_DT        
				FROM #PatProvs PProv 
				INNER JOIN PatientProcedure PP ON PProv.LbpatientId = PP.PatientID AND PProv.NPI = PP.PerformedbyClinician
				INNER JOIN PatientProcedureProcedureCode PPPC ON PP.PatientID = PPPC.PatientProcedureId 
				INNER JOIN ProcedureCode PC ON PPPC.ProcedureCodeId = PC.ProcedureCodeId	 
				WHERE ProcedureDateTime >= ISNULL(@TimeWindowFilter,ProcedureDateTime)
				AND (
						(EXISTS (SELECT 1 FROM RefTable_Wellness W WHERE PC.ProcedureCode = W.code AND W.field = 'hcpc') AND @RankingUseOfPcpCodes = 1) OR @RankingUseOfPcpCodes = 0
					)
				GROUP BY HICNO, PerformedbyClinician
			) X GROUP BY HICNO, NPI
		) Y
	END
END

-- INSERTING UNRANKED
INSERT INTO #PatProvsRanked (HICNO ,NPI , RANKNO )
SELECT HICNO , NPI , 1 FROM (
SELECT PP.HICNO, PP.NPI, ROW_NUMBER() OVER(PARTITION BY PP.HICNO ORDER BY PP.HICNO) SNO
FROM #PatProvs PP 
LEFT JOIN #PatProvsRanked PPR ON PP.HICNO = PPR.HICNO 
WHERE PPR.HICNO  IS NULL ) X WHERE SNO = 1 

-- WRAP UP
--SELECT PPR.HICNO, PPR.NPI, PPR.RANKNO
--FROM #PatProvsRanked PPR 
--WHERE PPR.RANKNO = 1 

UPDATE MemberExtract 
SET ProviderId = CA.NPI
FROM MemberExtract ME INNER JOIN #PatProvsRanked CA ON ME.medicareNo = CA.HICNO WHERE ME.ProviderId IS NULL AND CA.RANKNO = 1 

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId]
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.family_id = pir.Family_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.Head_Id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId]
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.Family_Id = pir.Head_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.family_id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

END
GO
