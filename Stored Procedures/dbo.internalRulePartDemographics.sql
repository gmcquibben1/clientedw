SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalRulePartDemographics] @yearend datetime = '12/31/2015', @gender varchar = '1,2', @ageMinimum int = 0, 
@ageMaximum int = 99, @MetricName varchar(100), @MetricType varchar(50), @RulePartId int = null, @MetricValue varchar(50) = 1
WITH EXEC AS CALLER
AS
DELETE FROM PatientMetric WHERE MetricName = @MetricName

INSERT INTO PatientMetric (
   LbPatientId
  ,MetricName
  ,MetricType
  ,MetricValue
) SELECT DISTINCT
  BENE.LbPatientID AS LbPatientId            -- LbPatientId - int
  ,@MetricName AS MetricName             -- MetricName - varchar(100)
  ,@MetricType AS MetricType             -- MetricType - varchar(50)
  ,@MetricValue AS MetricValue            -- MetricValue - varchar(50)
			FROM  OrgHierarchy Bene
			WHERE Bene.BirthDate <= dateadd(year,-@ageMinimum,@yearend)  
      and Bene.BirthDate >= dateadd(year,-@ageMaximum,@yearend) 
      and GenderId in ( @gender ) 
			AND   ISNULL(Bene.Patient_Status,'x') <> 'Deceased%'
GO
