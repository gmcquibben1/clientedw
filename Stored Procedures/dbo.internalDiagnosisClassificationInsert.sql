SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalDiagnosisClassificationInsert] 
    @DiagnosisClassificationTypeId INT OUTPUT ,
	@DiagnosisClassification VARCHAR(255) = NULL  
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @DiagnosisCodeId INT

	IF @DiagnosisClassification IS NOT NULL 
	BEGIN

		BEGIN TRAN

		SELECT @DiagnosisClassificationTypeId = DiagnosisClassificationTypeId FROM DiagnosisClassificationType
		WHERE Name = @DiagnosisClassification

		IF @DiagnosisClassificationTypeId IS NULL
		BEGIN
			INSERT INTO [dbo].DiagnosisClassificationType
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
			VALUES 
			(
				@DiagnosisClassification,
				@DiagnosisClassification,
				@DeleteInd,
				GetDate(),
				GetDate(),
				@LBUserId,
				@LBUserId
			)
			SELECT @intError = @@ERROR,
				@DiagnosisClassificationTypeId = SCOPE_IDENTITY()
		END

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
		END
		ELSE
		BEGIN
			COMMIT TRAN
		END

	END
		
	RETURN @intError


GO
