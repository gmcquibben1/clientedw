SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalProcessMemberRosterArchives] 
AS 

UPDATE MemberRosterArchives
SET InitialRoster = 'N' WHERE InitialRoster IS NULL 

UPDATE MemberRosterArchives
SET TimeLine = 
	Substring(FileTimeLine,6,4) + '-' +
	CASE CASE WHEN Substring(FileTimeLine,10,1) = ',' THEN Substring(FileTimeLine,20,1) ELSE Substring(FileTimeLine,19,1) END 
		 WHEN 1 THEN '01' WHEN 2 THEN '04' WHEN 3 THEN '07' WHEN 4 THEN '10' END + '-01'
WHERE TimeLine IS NULL 

UPDATE MRA1
SET TimelineID =  MRA2.TimelineId
FROM MemberRosterArchives MRA1
INNER JOIN (Select Timeline, DENSE_RANK() OVER ( ORDER BY Timeline DESC  )TimelineId FROM MemberRosterArchives ) MRA2 ON MRA1.Timeline = MRA2.Timeline

-- UPDATE FOR NEW/RETURNED/CONTINUING 
;WITH CTE1 AS 
(
	SELECT 
		Cur.[HIcNO], StatusDesc,Cur.TimelineID,
		ISNULL((SELECT COUNT(*) FROM [MemberRosterArchives] Prev1 WHERE Cur.[HIcNO] = Prev1.[HIcNo] AND Cur.TimelineID = Prev1.TimelineID - 1
								AND Prev1.InitialRoster = 'N' AND ISNULL(Prev1.StatusDesc,'X') <> 'DROPPED'
			   ),0) AS Prev1Count,
		ISNULL((SELECT COUNT(*) FROM [MemberRosterArchives] Prev2 WHERE Cur.[HIcNO] = Prev2.[HIcNo] AND Cur.TimelineID < Prev2.TimelineID - 2
								AND Prev2.InitialRoster = 'N' AND ISNULL(Prev2.StatusDesc,'X') <> 'DROPPED'
			   ),0) AS Prev2Count--,
		--ISNULL((SELECT COUNT(*) FROM [MemberRosterArchives] Prev3 WHERE Cur.[HIcNO] = Prev3.[HIcNo] AND Cur.TimelineID > Prev3.TimelineID 
		--						AND Prev3.InitialRoster = 'Y' 
		--	   ),0) AS Prev3Count
	FROM [MemberRosterArchives] Cur 
	WHERE InitialRoster = 'N'
)
UPDATE CTE1 
SET StatusDesc = CASE WHEN Prev1Count > 0 THEN 'CONTINUING'
					  WHEN Prev1Count = 0 AND Prev2Count > 0 THEN 'RETURNING'
					  WHEN Prev1Count = 0 AND Prev2Count = 0 THEN 'NEW'
				 END
WHERE ISNULL(StatusDesc,'') <> 'DROPPED'				   

-- REGENERATE DROPPEDs
DELETE FROM MemberRosterArchives WHERE StatusDesc = 'DROPPED'
WHILE (1=1)
BEGIN
	INSERT INTO [MemberRosterArchives] ([HICNO],[Last Name],[First Name],[Sex1],[Birth Date],
				[Deceased Beneficiary Flag2],[ACO Participant TIN Number],[ACO],[Timeline],[TimelineId],[StatusDesc])
	SELECT  DISTINCT Prev1.HicNo, Prev1.[Last Name], prev1.[First Name], prev1.Sex1, prev1.[Birth Date],
			prev1.[Deceased Beneficiary Flag2], prev1.[ACO Participant TIN Number], prev1.ACO,MRA.Timeline,
			Prev1.TimelineID - 1, 'DROPPED'
	FROM [MemberRosterArchives] Prev1 
	LEFT JOIN [MemberRosterArchives] CUR 
	ON Cur.[HIcNO] = Prev1.[HIcNo] AND Prev1.TimelineID - 1 = Cur.TimelineID
	CROSS APPLY (SELECT DISTINCT TimeLine FROM [MemberRosterArchives] MRA WHERE MRA.TimelineID + 1 = Prev1.TimelineID) MRA
	WHERE CUR.[HIcno] IS NULL --AND ISNULL(Prev1.StatusDesc,'X') <> 'DROPPED' AND ISNULL(CUR.StatusDesc,'X') <> 'DROPPED' DROPPEDS GO FORWARD
	AND Prev1.TimelineID <> 1 AND Prev1.InitialRoster = 'N'

	IF @@ROWCOUNT = 0 
	BEGIN
		BREAK
	END 
END 

UPDATE MemberRosterArchives 
SET medicare_status = 
	CASE WHEN BENE_MDCR_STUS_CD = '10' THEN 'Aged without ESRD'
	WHEN BENE_MDCR_STUS_CD = '11' THEN 'Aged with ESRD'
	WHEN BENE_MDCR_STUS_CD = '20' THEN 'Disabled without ESRD'
	WHEN BENE_MDCR_STUS_CD = '21' THEN 'Disabled with ESRD'
	WHEN BENE_MDCR_STUS_CD = '31' THEN 'ESRD only '
	END 
FROM MemberRosterArchives ACO INNER JOIN CCLF_8_BeneDemo demo ON ACO.HICNO = demo.BENE_HIC_NUM
WHERE medicare_status IS NULL

GO
