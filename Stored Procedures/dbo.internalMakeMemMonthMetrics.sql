SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create PROCEDURE [dbo].[internalMakeMemMonthMetrics]
AS
BEGIN

SET NOCOUNT ON


       DECLARE @EDWName VARCHAR(128) = DB_NAME();      
       DECLARE @EDWtableName VARCHAR(128) = 'MemMonth_Metrics';
       DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
       DECLARE @ProcedureName VARCHAR(128) = 'internalMakeMemMonthMetrics';
       DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
       DECLARE @maxSourceIdProcessed INT = NULL;
       DECLARE @StartTime DATETIME = GETUTCDATE();
       DECLARE @EndTime DATETIME = NULL;
       DECLARE @InsertedRecord INT=0;
       DECLARE @UpdatedRecord INT=0;
       DECLARE @Comment VARCHAR(400) = '';
       DECLARE @ErrorNumber INT =0;
       DECLARE @ErrorState INT =0;
       DECLARE @ErrorSeverity INT=0;
       DECLARE @ErrorLine INT=0;
       DECLARE @CountingMethod Varchar(20) = 'Date and Location'
--     DECLARE @CountingMethod Varchar(20) = 'Claim Number'
--     DECLARE @CountingMethod Varchar(20) = 'Event Date'
       --DECLARE @RecordCountBefore INT=0;
       --DECLARE @RecordCountAfter  INT=0;
       --DECLARE @RecordCountSource INT=0;

       DECLARE @Today DATE=GETUTCDATE();

BEGIN TRY 

       TRUNCATE TABLE MemMonth_Metrics

declare @MinPaid INT = 0
       
IF EXISTS (SELECT SetValue FROM vwPortal_SystemSettings WHERE SettingType = 'AnalyticsSetting' AND SettingParameter = 'MinPaidSum')
BEGIN
SET @MinPaid = (SELECT isnull(try_convert(INT,SetValue), 0) FROM vwPortal_SystemSettings WHERE SettingType = 'AnalyticsSetting' AND SettingParameter = 'MinPaidSum')
END


/* create counts for all fields that don't require special logic using vwReference_MemberMonthMetricsConfig as a control table */

--Note: When bit fields/flags are added to the control table and CCLF0 then then the joins will have to be updated.  
--	For convenience, you can use the following SQL to create the join logic dynamically.

--			use [EdwReferenceData]
--				select case when x.ordinal_position = (select min(ordinal_position) from information_schema.columns
--					where table_name = 'EdwReferenceData.dbo.MemberMonthMetricsConfig' and data_type = 'bit') then ' ' else ' and ' end+
--					'a.'+column_name+' in (isnull(b.'+COLUMN_NAME+',0),isnull(b.'+COLUMN_NAME+',1))'
--				from INFORMATION_SCHEMA.columns x
--				where table_name = 'EdwReferenceData.dbo.MemberMonthMetricsConfig'
--				and data_type = 'bit'

Insert into MemMonth_Metrics (Title, lbpatientid, DOS_First, [Value])
select Display_Value+' Visits',lbpatientid,CLM_FROM_DT_1ST,count (distinct (format(clm_from_dt,'d','en-US')+isnull(fac_prvdr_npi_num,'butt')))
from (--select from subquery w/ threshold check
select b.Display_Value, c.lbpatientid, a.CLM_FROM_DT,c.CLM_FROM_DT_1ST, c.fac_prvdr_npi_num
       ,sum(c.CLM_PMT_AMT) as SumPaid
       from [CCLF_0_PartA_CLM_TYPES] a
       join cclf_1_parta_header c on (a.id = c.id) 
join vwReference_MemberMonthMetricsConfig b on (
              a.inp in (isnull(b.inp,0),isnull(b.inp,1)) and a.snf in (isnull(b.snf,0),isnull(b.snf,1)) 
              and a.[OUT] in (isnull(b.[OUT],1),isnull(b.[OUT],0)) 
              and a.hospice in (isnull(b.hospice,1),isnull(b.hospice,0)) 
              and a.HH in (isnull(b.HH,1),isnull(b.HH,0)) 
              and a.ER in (isnull(b.ER,1),isnull(b.ER,0)) 
              and a.ER_REV in (isnull(b.ER_REV,1),isnull(b.ER_REV,0)) 
              and a.ER_POS in (isnull(b.ER_POS,1),isnull(b.ER_POS,0)) 
              and a.chronic in (isnull(b.chronic,1),isnull(b.chronic,0)) 
              and a.RehabRev in (isnull(b.RehabRev,1),isnull(b.RehabRev,0)) 
              and a.Observation in (isnull(b.Observation,1),isnull(b.Observation,0)) 
              and a.Rehab in (isnull(b.Rehab,1),isnull(b.Rehab,0)) 
              and a.NonPay in (isnull(b.NonPay,1),isnull(b.NonPay,0)) 
       )
       where c.LbPatientId is not null
       group by b.Display_Value, c.lbpatientid, c.CLM_FROM_DT_1ST,a.CLM_FROM_DT, c.fac_prvdr_npi_num
       having sum(c.CLM_PMT_AMT) > @MinPaid
       ) x --subquery w/ threshold check
       group by Display_Value+' Visits',lbpatientid,CLM_FROM_DT_1ST
/* Same as above, but with Admits because we're creative with naming conventions */

Insert into MemMonth_Metrics (Title, lbpatientid, DOS_First, [Value])
select Display_Value+' Admits',lbpatientid,CLM_FROM_DT_1ST,count (distinct (format(clm_from_dt,'d','en-US')+isnull(fac_prvdr_npi_num,'butt')))
from (--select from subquery w/ threshold check
select b.Display_Value, c.lbpatientid, a.CLM_FROM_DT,c.CLM_FROM_DT_1ST, c.fac_prvdr_npi_num
       ,sum(c.CLM_PMT_AMT) as SumPaid
       from [CCLF_0_PartA_CLM_TYPES] a
       join cclf_1_parta_header c on (a.id = c.id) 
join vwReference_MemberMonthMetricsConfig b on (
              a.inp in (isnull(b.inp,0),isnull(b.inp,1)) and a.snf in (isnull(b.snf,0),isnull(b.snf,1)) 
              and a.[OUT] in (isnull(b.[OUT],1),isnull(b.[OUT],0)) 
              and a.hospice in (isnull(b.hospice,1),isnull(b.hospice,0)) 
              and a.HH in (isnull(b.HH,1),isnull(b.HH,0)) 
              and a.ER in (isnull(b.ER,1),isnull(b.ER,0)) 
              and a.ER_REV in (isnull(b.ER_REV,1),isnull(b.ER_REV,0)) 
              and a.ER_POS in (isnull(b.ER_POS,1),isnull(b.ER_POS,0)) 
              and a.chronic in (isnull(b.chronic,1),isnull(b.chronic,0)) 
              and a.RehabRev in (isnull(b.RehabRev,1),isnull(b.RehabRev,0)) 
              and a.Observation in (isnull(b.Observation,1),isnull(b.Observation,0)) 
              and a.Rehab in (isnull(b.Rehab,1),isnull(b.Rehab,0)) 
              and a.NonPay in (isnull(b.NonPay,1),isnull(b.NonPay,0)) 
       )
       where c.LbPatientId is not null
       group by b.Display_Value, c.lbpatientid, c.CLM_FROM_DT_1ST,a.CLM_FROM_DT, c.fac_prvdr_npi_num
       having sum(c.CLM_PMT_AMT) > @MinPaid
       ) x --subquery w/ threshold check
       group by Display_Value+' Admits',lbpatientid,CLM_FROM_DT_1ST

/* Same as above, but calculate costs */
Insert into MemMonth_Metrics (Title, lbpatientid, DOS_First, [Value])
select b.Display_Value+' Cost', c.lbpatientid, 
       c.CLM_FROM_DT_1ST,
       sum(c.clm_pmt_amt)
       from [CCLF_0_PartA_CLM_TYPES] a
       join cclf_1_parta_header c on (a.id = c.id) 
join vwReference_MemberMonthMetricsConfig b on (
              a.inp in (isnull(b.inp,0),isnull(b.inp,1)) and a.snf in (isnull(b.snf,0),isnull(b.snf,1)) 
              and a.[OUT] in (isnull(b.[OUT],1),isnull(b.[OUT],0)) 
              and a.hospice in (isnull(b.hospice,1),isnull(b.hospice,0)) 
              and a.HH in (isnull(b.HH,1),isnull(b.HH,0)) 
              and a.ER in (isnull(b.ER,1),isnull(b.ER,0)) 
              and a.ER_REV in (isnull(b.ER_REV,1),isnull(b.ER_REV,0)) 
              and a.ER_POS in (isnull(b.ER_POS,1),isnull(b.ER_POS,0)) 
              and a.chronic in (isnull(b.chronic,1),isnull(b.chronic,0)) 
              and a.RehabRev in (isnull(b.RehabRev,1),isnull(b.RehabRev,0)) 
              and a.Observation in (isnull(b.Observation,1),isnull(b.Observation,0)) 
              and a.Rehab in (isnull(b.Rehab,1),isnull(b.Rehab,0)) 
              and a.NonPay in (isnull(b.NonPay,1),isnull(b.NonPay,0)) 
       )
       group by b.Display_Value, c.lbpatientid, 
       c.CLM_FROM_DT_1ST


 

       /*  INSERT Office Visits  
       */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
         'Office Visits', LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, count(distinct(CUR_CLM_UNIQ_ID))
         FROM CCLF_5_PartB_Physicians 
         JOIN BETOS_CPT CPT ON dbo.CCLF_5_PartB_Physicians.CLM_LINE_HCPCS_CD = CPT.HCPC
         JOIN BETOS_GROUPS BG ON CPT.BETOSGroupID = BG.GroupID and BG.Cat2 = 'Office visits'
       WHERE 
         ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2)  
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)


       --insert specialist visits

       /* Total Specialist Cost  added by Mike Hoxter on 07/5/2015*/

       INSERT INTO MemMonth_Metrics (
       Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
       'Specialist Visits', 
         LbPatientId,
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         count(distinct(CUR_CLM_UNIQ_ID)) --METRIC TO BE RECORDED
         FROM CCLF_5_PartB_Physicians B
         JOIN BETOS_CPT BC ON B.CLM_LINE_HCPCS_CD = BC.HCPC
         JOIN BETOS_GROUPS BG ON BC.BETOSGroupID = BG.GroupID and BG.Cat2 = 'Specialist'
              WHERE 
                ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2)
                GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)

       /* BETOS Cat1 Costs  added by Mike Hoxter on 07/5/2015 */



       /*  INSERT INPATIENT BED DAYS  
       */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
         'Inpatient Bed Days', 
         LbPatientId, 
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         SUM(ABS(LOS))
       FROM CCLF_0_PartA_READMITS
       WHERE  
         CLM_FROM_DT != '1/1/1000 12:00:00 AM'
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)


       /*  INSERT INPATIENT Re-ADMITS  
       */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
         'Inpatient Re-Admits', 
         LbPatientId, 
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         COUNT(DISTINCT(CUR_CLM_UNIQ_ID)) 
       FROM CCLF_0_PartA_READMITS
       WHERE  
         CLM_FROM_DT != '1/1/1000 12:00:00 AM' AND 
          [30Day_ReAdmit] = 1
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)



       /*  INSERT INPATIENT SHORT STAYS  
       */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
         'Inpatient Short Stays', 
         LbPatientId, 
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         COUNT(DISTINCT(CUR_CLM_UNIQ_ID)) 
       FROM CCLF_0_PartA_READMITS
       WHERE 
         LOS > 0 AND LOS < 3
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)


       /*  INSERT INPATIENT SHORT STAYS  
       */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT DISTINCT 'IP Short Stay Cost', A.LbPatientId, DATEADD(DAY, -(DAY(A.CLM_FROM_DT) - 1), A.CLM_FROM_DT) AS DOS_First, SUM(A.CLM_PMT_AMT) 
       FROM DBO.CCLF_1_PartA_Header A
       INNER JOIN DBO.CCLF_0_PartA_READMITS R 
       ON A.CUR_CLM_UNIQ_ID = R.CUR_CLM_UNIQ_ID
       WHERE (A.CLM_MDCR_NPMT_RSN_CD = '' OR A.CLM_MDCR_NPMT_RSN_CD IS NULL) AND R.LOS > 0 AND R.LOS < 3 AND A.CLM_FROM_DT <> '1000-01-01' 
       GROUP BY A.LbPatientId, DATEADD(DAY, -(DAY(A.CLM_FROM_DT) - 1), A.CLM_FROM_DT);

	/*  INSERT SNF COUNT  
	*/

 
	--drop table #SNFPatients

		SELECT DISTINCT
		LbPatientId,
		CLM_FROM_DT,
		CLM_THRU_DT
		INTO #SNFPatients
		FROM CCLF_1_PartA_Header
		WHERE LbPatientID IS NOT NULL
		AND	(try_convert(int,CLM_TYPE_CD)= 20 or try_convert(int,CLM_TYPE_CD ) = 30)
		AND CLM_FROM_DT !='1/1/1000 12:00:00 AM' 
		AND (CLM_MDCR_NPMT_RSN_CD='' or CLM_MDCR_NPMT_RSN_CD IS NULL) 
		AND  ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2)
		ORDER BY LbPatientId

		--This provides our anchor per member. We know that any changes
		--in enrollment must have occurred between those dates. The anchor
		--is important and can be used to easily change the logic in case
		--you are only interested in certain date ranges.
		SELECT
		LbPatientId
		,MIN(CLM_FROM_DT) AS CLM_FROM_DT
		,MAX(CLM_THRU_DT) AS CLM_THRU_DT
		INTO #Anchor
		FROM #SNFPatients
		GROUP BY LbPatientId

		--drop table #Anchor

		--This step establishes all the segment change dates per member
		--that occurred during the anchor period. The term date being
		--returned will alway be from the anchor record. The actual term
		--date for each segment will be determined in the next step.
		--A change point is defined as any new effective date during the
		--anchor period. We use term dates to establish new effective
		--dates as well by adding a day to it. This gives us the ability
		--to establish all of the segments we need.
        SELECT
        dt.LbPatientId,
        dt.CLM_FROM_DT,
        dt.CLM_THRU_DT,
        ROW_NUMBER() OVER(PARTITION BY dt.LbPatientID ORDER BY dt.CLM_FROM_DT) AS Sequence_ID
        INTO #Changes
        FROM (
               SELECT
               a.LbPatientId,
               e.CLM_FROM_DT,
               a.CLM_THRU_DT
               FROM #Anchor a
               INNER JOIN #SNFPatients e
               ON e.LbPatientID = a.LbPatientID
               AND e.CLM_FROM_DT BETWEEN a.CLM_FROM_DT AND a.CLM_THRU_DT                             
               --==============
               UNION
               --==============
               SELECT
               a.LbPatientId,
               DATEADD(DAY, 1, e.CLM_THRU_DT) AS CLM_FROM_DT,
               a.CLM_THRU_DT
               FROM #Anchor a
               INNER JOIN #SNFPatients e
			   ON e.LbPatientID = a.LbPatientID
			   AND e.CLM_THRU_DT BETWEEN a.CLM_FROM_DT AND a.CLM_THRU_DT
			   AND e.CLM_THRU_DT < a.CLM_THRU_DT
             ) dt
              
		--This step establishes all of the continuous non-overlapping segments
		--of the anchor period. It uses the sequence ID to look at the next record
		--to establish the term date by subtracting one day from the effective
		--date of the next segment. This ensures there are no overlapping records.
		SELECT
		c1.LbPatientID,
		c1.CLM_FROM_DT,
		CASE WHEN c2.LbPatientID IS NULL 
			 THEN  c1.CLM_THRU_DT  
			 ELSE DATEADD(DAY, -1, c2.CLM_FROM_DT)     
		END AS CLM_THRU_DT,                                             
		c1.Sequence_ID, 
		Enrollment_Count = (SELECT COUNT(*) FROM #SNFPatients e
				            WHERE e.LbPatientID = c1.LbPatientId
				            AND c1.CLM_FROM_DT BETWEEN e.CLM_FROM_DT AND e.CLM_THRU_DT) 			                                  				
		INTO #Segments
		FROM #Changes c1
		LEFT OUTER JOIN #Changes c2      
		ON c2.LbPatientId = c1.LbPatientId
		AND c2.Sequence_ID = c1.Sequence_ID + 1 --join with the next record                            

	   --select * from #Segments where Enrollment_Count >0
       --This step establishes all the segment gap dates per member
       --that occurred during the anchor period. The term date being
       --returned will always be from the anchor record. The actual term
       --date for each segment will be determined in the next step.
        SELECT
        dt.LbPatientID, 
        dt.CLM_FROM_DT, 
        dt.CLM_THRU_DT, 
        ROW_NUMBER() OVER(PARTITION BY dt.LbPatientID ORDER BY dt.CLM_FROM_DT) AS Sequence_ID
        INTO #ContinuousChanges
        FROM (             
               SELECT
               a.LbPatientID,
               a.CLM_FROM_DT,
               a.CLM_THRU_DT
               FROM #Anchor a
              --==============
               UNION
              --==============
               SELECT
			   a.LbPatientID,
			   s.CLM_FROM_DT,
			   a.CLM_THRU_DT
               FROM #Anchor a
               INNER JOIN #Segments s
               ON s.LbPatientID = a.LbPatientID       
               AND s.Enrollment_Count = 0                                           
              --==============
               UNION
              --==============
               SELECT
               a.LbPatientID,
               DATEADD(DAY, 1, s.CLM_THRU_DT) AS CLM_FROM_DT,
               a.CLM_THRU_DT
               FROM #Anchor a
               INNER JOIN #Segments s        
               ON s.LbPatientID = a.LbPatientID
               AND s.Enrollment_Count = 0
               AND s.CLM_THRU_DT BETWEEN a.CLM_FROM_DT AND a.CLM_THRU_DT
               AND s.CLM_THRU_DT < a.CLM_THRU_DT   --order by lbpatientid   
			 ) dt

		--This step establishes all of the continuous non-overlapping segments
		--of the anchor period using the gaps above. It uses the sequence ID
		--to look at the next record to establish the term date by subtracting
		--one day from the effective date of the next segment. This ensures there
		--are no overlapping records.
        SELECT
        c1.LbPatientID, 
        c1.CLM_FROM_DT, 
        CASE WHEN c2.LbPatientID IS NULL 
			 THEN c1.CLM_THRU_DT
             ELSE DATEADD(DAY, -1, c2.CLM_FROM_DT)
        END AS CLM_THRU_DT, 
        c1.Sequence_ID, 
        s.Enrollment_Count  
	    INTO #ContinuousSegments         
        FROM #ContinuousChanges c1
        LEFT OUTER JOIN #ContinuousChanges c2
        ON c2.LbPatientID = c1.LbPatientID
        AND c2.Sequence_ID = c1.Sequence_ID + 1 --join with the next record
        INNER JOIN #Segments s
        ON s.LbPatientID = c1.LbPatientID
        AND c1.CLM_FROM_DT BETWEEN s.CLM_FROM_DT AND s.CLM_THRU_DT  
	    WHERE s.Enrollment_Count>0  

	   --select * from #ContinuousSegments order by lbpatientId

		SELECT 
		LbPatientId
		,DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First
		,COUNT(1) AS SNFAdmitCount
		INTO #SNFFinal
		FROM #ContinuousSegments cs
		--where Enrollment_Count>0
		GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)
		order by LbPatientID

		INSERT INTO MemMonth_Metrics (
		  Title
		  ,LbPatientId
		  ,DOS_First
		  ,[Value]
		)
		SELECT 
			'SNF Admits',
			LbPatientId, 
			DOS_First, 
			SNFAdmitCount
		FROM #SNFFinal




       /*  INSERT TOTAL PROFESSIONAL COST 
       */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 'Professional Cost', P.LbPatientId, DATEADD(DAY, -(DAY(P.CLM_FROM_DT) - 1), P.CLM_FROM_DT) AS DOS_First, SUM(P.CLM_LINE_CVRD_PD_AMT)
       FROM DBO.CCLF_5_PartB_Physicians P
       GROUP BY P.LbPatientId, DATEADD(DAY, -(DAY(P.CLM_FROM_DT) - 1), P.CLM_FROM_DT)


       /*  INSERT PROFESSIONAL COST BY SPECIALTY 
       */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       )
       SELECT 
        dbo.SPECIALTY_CODES.[Description]+' Cost', 
        dbo.CCLF_5_PartB_Physicians.LbPatientId, 
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First,
       SUM(dbo.CCLF_5_PartB_Physicians.CLM_LINE_CVRD_PD_AMT)
       FROM CCLF_5_PartB_Physicians JOIN SPECIALTY_CODES
       ON dbo.CCLF_5_PartB_Physicians.CLM_PRVDR_SPCLTY_CD = dbo.SPECIALTY_CODES.Code
       GROUP BY dbo.SPECIALTY_CODES.[Description]+' Cost', LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)


       /*  Insert Cost Values by UB04 Rev Codes
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       )
       SELECT
       'UB04='+CCLF_2_PartA_RCDetail.CLM_LINE_REV_CTR_CD+' Cost' AS Title, 
         CCLF_1_PartA_Header.LbPatientId, 
         DATEADD(DAY, -(DAY(CCLF_1_PartA_Header.CLM_FROM_DT) - 1), CCLF_1_PartA_Header.CLM_FROM_DT) AS DOS_First, 
         SUM(dbo.CCLF_1_PartA_Header.CLM_PMT_AMT) --METRIC TO BE RECORDED
       FROM CCLF_1_PartA_Header 
       INNER JOIN CCLF_2_PartA_RCDetail ON
       CCLF_1_PartA_Header.CUR_CLM_UNIQ_ID = CCLF_2_PartA_RCDetail.CUR_CLM_UNIQ_ID
       WHERE 
         (CCLF_1_PartA_Header.CLM_TYPE_CD = 50) and
         CCLF_1_PartA_Header.CLM_FROM_DT != '1/1/1000 12:00:00 AM' and
         dbo.CCLF_1_PartA_Header.CLM_ADJSMT_TYPE_CD = 0
       GROUP BY 'UB04='+CCLF_2_PartA_RCDetail.CLM_LINE_REV_CTR_CD+' Cost', CCLF_1_PartA_Header.LbPatientId, DATEADD(DAY, -(DAY(CCLF_1_PartA_Header.CLM_FROM_DT) - 1), CCLF_1_PartA_Header.CLM_FROM_DT)

       */

       /*  Insert DME Spend    
       */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       )
       SELECT 
       'DME Cost',  
        LbPatientId,
       DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, 
        SUM(isnull(CLM_LINE_CVRD_PD_AMT, 0)) 
       FROM CCLF_6_PartB_DME
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)

       /* Part A Cost */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
         'Part-A Cost', 
         LbPatientId, 
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         SUM(dbo.CCLF_1_PartA_Header.CLM_PMT_AMT) --METRIC TO BE RECORDED
       FROM CCLF_1_PartA_Header
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)


       /* Part B Cost */
       INSERT INTO MemMonth_Metrics (
         Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
         'Part-B Cost', 
         LbPatientId, 
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         SUM(CLM_LINE_CVRD_PD_AMT) --METRIC TO BE RECORDED
       FROM CCLF_5_PartB_Physicians
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)



       /* Pharmacy Cost */
       INSERT INTO MemMonth_Metrics (
       Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
       'Pharmacy Payer Cost', 
       LbPatientId, 
       DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First,
       SUM(CLM_LINE_PAYER_PAID_AMT)
       FROM CCLF_7_PartD 
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* Pharmacy Cost */
       INSERT INTO MemMonth_Metrics (
       Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
       'Pharmacy Cost', 
       LbPatientId, 
       DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First,
       SUM(CLM_LINE_BENE_PMT_AMT)
       FROM CCLF_7_PartD 
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* TOTAL COST W/ PHARMACY */
       INSERT INTO MemMonth_Metrics (
       Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
       'Total Cost w/ Pharmacy', 
         LbPatientId,
         DOS_First,
         sum([Value]) 
         FROM MemMonth_Metrics
         WHERE Title in ('Part-A Cost','Part-B Cost','Pharmacy Cost','DME Cost')
         GROUP BY LbPatientId, DOS_First

       /* Total Cost w/out Pharmacy */

       INSERT INTO MemMonth_Metrics (
       Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
       'Total Cost w/out Pharmacy', 
         LbPatientId,
         DOS_First,
         sum([Value]) 
         FROM MemMonth_Metrics
         WHERE Title in ('Part-A Cost','Part-B Cost','DME Cost')
         GROUP BY LbPatientId, DOS_First


       /* Total Lab Cost - A & B & Total  added by Mike Hoxter on 07/5/2015*/

       INSERT INTO MemMonth_Metrics (
       Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
       'Total Professional Lab Cost', 
         LbPatientId,
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         SUM(CLM_LINE_CVRD_PD_AMT) --METRIC TO BE RECORDED
         FROM CCLF_5_PartB_Physicians B
         JOIN BETOS_CPT BC ON B.CLM_LINE_HCPCS_CD = BC.HCPC
         JOIN BETOS_GROUPS BG ON BC.BETOSGroupID = BG.GroupID
              WHERE BG.Cat2 = 'Lab tests'
                GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)
  
        /* Total Office Visit Cost  added by Mike Hoxter on 07/5/2015*/

       INSERT INTO MemMonth_Metrics (
       Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
       'Office Visit Cost', 
         LbPatientId,
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         SUM(CLM_LINE_CVRD_PD_AMT) --METRIC TO BE RECORDED
         FROM CCLF_5_PartB_Physicians B
         JOIN BETOS_CPT BC ON B.CLM_LINE_HCPCS_CD = BC.HCPC
         JOIN BETOS_GROUPS BG ON BC.BETOSGroupID = BG.GroupID
              WHERE BG.Cat2 = 'Office visits'
                GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)

  
        /* Total Specialist Cost  added by Mike Hoxter on 07/5/2015*/

       INSERT INTO MemMonth_Metrics (
       Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
       'Specialist Visit Cost', 
         LbPatientId,
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         SUM(CLM_LINE_CVRD_PD_AMT) --METRIC TO BE RECORDED
         FROM CCLF_5_PartB_Physicians B
         JOIN BETOS_CPT BC ON B.CLM_LINE_HCPCS_CD = BC.HCPC
         JOIN BETOS_GROUPS BG ON BC.BETOSGroupID = BG.GroupID
              WHERE BG.Cat2 = 'Specialist'
                GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)

       /* BETOS Cat1 Costs  added by Mike Hoxter on 07/5/2015 */

       INSERT INTO MemMonth_Metrics (
       Title
         ,LbPatientId
         ,DOS_First
         ,[Value]
       ) 
       SELECT 
       Cat1+' Cost', 
         LbPatientId,
         DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First, 
         SUM(CLM_LINE_CVRD_PD_AMT) --METRIC TO BE RECORDED
         FROM CCLF_5_PartB_Physicians B
         JOIN BETOS_CPT BC ON B.CLM_LINE_HCPCS_CD = BC.HCPC
         JOIN BETOS_GROUPS BG ON BC.BETOSGroupID = BG.GroupID
                GROUP BY Cat1+' Cost', LbPatientId, DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT)

  
  
       /* Pharmacy Count - added by Ismail on 06/17/15 */
       INSERT INTO MemMonth_Metrics (Title, LbPatientId, DOS_First, [Value]) 
       SELECT 'Pharmacy Count', LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, COUNT(DISTINCT(CUR_CLM_UNIQ_ID)) 
       FROM CCLF_7_PartD 
       WHERE ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2)
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* Pharmacy Count - Retail added by Mike Hoxter on 07/5/2015 */
       INSERT INTO MemMonth_Metrics (Title, LbPatientId, DOS_First, [Value]) 
       SELECT 'Pharmacy Count - Retail', LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, COUNT(DISTINCT(CUR_CLM_UNIQ_ID)) 
       FROM CCLF_7_PartD 
       WHERE ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2) and Retail = 'R'
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* Pharmacy Count - Mail Order added by Mike Hoxter on 07/5/2015 */
       INSERT INTO MemMonth_Metrics (Title, LbPatientId, DOS_First, [Value]) 
       SELECT 'Pharmacy Count - Mail Order', LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, COUNT(DISTINCT(CUR_CLM_UNIQ_ID)) 
       FROM CCLF_7_PartD 
       WHERE ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2) and Retail = 'M'
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* Pharmacy Count - Retail added by Mike Hoxter on 07/5/2015 */
       INSERT INTO MemMonth_Metrics (Title, LbPatientId, DOS_First, [Value]) 
       SELECT 'Pharmacy Cost - Retail', LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, SUM(CLM_LINE_PAYER_PAID_AMT)
       FROM CCLF_7_PartD 
       WHERE ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2) and Retail = 'R'
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* Pharmacy Count - Mail Order added by Mike Hoxter on 07/5/2015 */
       INSERT INTO MemMonth_Metrics (Title, LbPatientId, DOS_First, [Value]) 
       SELECT 'Pharmacy Cost - Mail Order', LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, 
       SUM(CLM_LINE_PAYER_PAID_AMT) 
       FROM CCLF_7_PartD 
       WHERE ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2) and Retail = 'M'
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* Pharmacy Count - Retail added by Mike Hoxter on 07/5/2015 */
       INSERT INTO MemMonth_Metrics (Title, LbPatientId, DOS_First, [Value]) 
       SELECT 'Pharmacy Count - Generic', LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, COUNT(DISTINCT(CUR_CLM_UNIQ_ID)) 
       FROM CCLF_7_PartD 
       WHERE ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2) and Generic = 'G'
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* Pharmacy Count - Mail Order added by Mike Hoxter on 07/5/2015 */
       INSERT INTO MemMonth_Metrics (Title, LbPatientId, DOS_First, [Value]) 
       SELECT 'Pharmacy Count - Brand', LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, COUNT(DISTINCT(CUR_CLM_UNIQ_ID)) 
       FROM CCLF_7_PartD 
       WHERE ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2) and Generic = 'B'
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* Pharmacy Count - Retail added by Mike Hoxter on 07/5/2015 */
       INSERT INTO MemMonth_Metrics (Title, LbPatientId, DOS_First, [Value]) 
       SELECT 'Pharmacy Cost - Generic', LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, 
       SUM(CLM_LINE_PAYER_PAID_AMT)
       FROM CCLF_7_PartD 
       WHERE ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2) and Generic = 'G'
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


       /* Pharmacy Count - Mail Order added by Mike Hoxter on 07/5/2015 */
       INSERT INTO MemMonth_Metrics (Title, LbPatientId, DOS_First, [Value]) 
       SELECT 'Pharmacy Cost - Brand', LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT) AS DOS_First, 
       SUM(CLM_LINE_PAYER_PAID_AMT) 
       FROM CCLF_7_PartD 
       WHERE ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2) and Generic = 'B'
       GROUP BY LbPatientId, DATEADD(DAY, -(DAY(CLM_LINE_FROM_DT) - 1), CLM_LINE_FROM_DT)


         --CODE INSERTED ON 01/12/2015 BY ISMAIL
              --SELECT DISTINCT [Title]
              --INTO EdwReferenceData.[dbo].[MasterMetricList]
              --FROM [dbo].[MemMonth_Metrics]

              INSERT INTO [dbo].[MemMonth_Metrics] (LbPatientId, DOS_First, Title, Value)
              SELECT DISTINCT '123456789', '01/01/2014', ML.[Title], 0
              FROM [dbo].[MemMonth_Metrics] MM
              RIGHT JOIN EdwReferenceData..[MasterMetricsList] ML  
              ON MM.[Title] = ML.[Title]
              WHERE MM.[Title] IS NULL

       EXEC internalTargetBuild;

       -----Log Information
              SELECT @InsertedRecord=count(1)
              FROM [MemMonth_Metrics] ;
              
       
          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
                       SET  @EndTime=GETUTCDATE();
                       EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
       
  END TRY

       BEGIN CATCH
                  --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
                           SET  @EndTime=GETUTCDATE();
                           SET  @ErrorNumber =ERROR_NUMBER();
                           SET  @ErrorState =ERROR_STATE();
                           SET  @ErrorSeverity=ERROR_SEVERITY();
                           SET  @ErrorLine=ERROR_LINE();
                           SET  @Comment =left(ERROR_MESSAGE(),400);
                           EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
                           
       END CATCH

END

GO
