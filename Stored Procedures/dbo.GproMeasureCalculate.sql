SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproMeasureCalculate]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2016-11-09
	INITIAL VER:	2.2.1
	MODULE:			GPRO		
	DESCRIPTION:	Calculate and auto-complete as many measures as possible
	PARAMETERS:		none
	RETURN VALUE(s)/OUTPUT:	none
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-11-09		BR			LBDM-1438	Initial version 
	2.2.1		2016-12-01		BR			LBPP-2028	Changing the PREV-13 fields
	2.2.1		2016-12-29		BR			LBDM-1438	Updated PREV-13 and MH-1 with new fields from GproPatientMonitor
	2.2.1		2017-01-03		BR			LBDM-1438	Fixed the calculations for PREV-10, PREV-13, added update for MedicalRecordFound flag
	2.2.1		2017-01-11		BR			LBDM-1761	Added code to set the CARE-3 confirmed flag
	2.2.1		2017-01-11		BR			LBDM-1761	Included diabetic check in CAD-7
	2.2.1		2017-01-13		BR						Modified PREV12 to match up to changes in the GproPatientMonitor table
	2.2.1		2017-01-18		BR						Removed checks for the numerator flag to auto-complete measures
	2.2.1		2017-01-28		BR						Removed exception skip comments, checked MH-1 scores to be sure they are in range
=================================================================*/
AS
BEGIN
    SET NOCOUNT ON;

	DECLARE @StartTime DATETIME = GETUTCDATE();
	--drop table #updatedRows
	CREATE TABLE #updatedRows
	(
		GproPatientMeasureId INT
		UNIQUE CLUSTERED (GproPatientMeasureId)
	)

	DECLARE @MeasurePeriodBegin DATE = CONVERT(DATE,'01/01/2016')
	DECLARE @MeasurePeriodEnd DATE = CONVERT(DATE,'12/31/2016')
	DECLARE @MhIndexDateBegin DATE = CONVERT(DATE,'12/1/2014') 
	DECLARE @MhIndexDateEnd DATE = CONVERT(DATE,'11/30/2015')

	------------------------------------------------------------
	-- Set Medical Record found = true if we have clinical data for this patient
	------------------------------------------------------------
	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1, gpm.MedicalRecordFound = ISNULL(gpm.MedicalRecordFound,2)
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN PatientIdReference idRef ON idRef.LbPatientId = gpr.LbPatientId 
		INNER JOIN SourceSystem ss ON ss.SourceSystemId = idRef.SourceSystemId
	WHERE ss.ClinicalInd = 1
	
	------------------------------------------------------------
	-- Calculate the CARE-2 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Care2EligibleInd = 1 OR glm.Care2Numerator = 1 OR glm.FallScreenCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.CareFallsConfirmed = (CASE WHEN gpm.CareFallsConfirmed IS NULL AND glm.Care2EligibleInd = 1 THEN 2 ELSE gpm.CareFallsConfirmed END),
		gpm.FallsScreening = (CASE WHEN gpm.FallsScreening IS NULL AND glm.FallScreenCode IS NOT NULL AND glm.FallScreenDate IS NOT NULL THEN 2 ELSE gpm.FallsScreening END),
		gpm.CareFallsComments = (CASE WHEN glm.Care2Numerator = 1 AND glm.FallScreenCode IS NOT NULL AND glm.FallScreenDate IS NOT NULL THEN 'Screening Code ' + glm.FallScreenCode + ' on date ' + CONVERT(VARCHAR,glm.FallScreenDate,101) 
									  ELSE gpm.CareFallsComments 
								END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.CareFallsRank IS NOT NULL

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.CareFallsComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'CARE-2'
	WHERE gpm.CareFallsConfirmed = 2 AND gpm.FallsScreening = 2 


	------------------------------------------------------------
	-- Calculate the CARE-3 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND glm.Care3EligibleInd = 1 THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.CareMeddocConfirmed = (CASE WHEN gpm.CareMeddocConfirmed IS NULL AND glm.Care3EligibleInd = 1 THEN 2 ELSE gpm.CareMeddocConfirmed END),
		gpm.CareMeddocComments = (CASE WHEN gpm.CareMeddocComments IS NULL AND glm.Care3EligibleInd = 1 AND glm.Care3Numerator = 1 THEN 'LB Confirmed, patient in numerator' 
									   WHEN gpm.CareMeddocComments IS NULL AND glm.Care3EligibleInd = 1 AND glm.Care3Numerator = 0 THEN 'LB Confirmed' 
									   WHEN gpm.CareMeddocComments IS NULL AND glm.Care3EligibleInd = 1 AND glm.Care3Exclusion = 1 THEN 'LB Confirmed, exclusion' 
									   ELSE gpm.CareMeddocComments 
								  END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.CareMeddocRank IS NOT NULL


	EXEC GproMedicationMonitorLoad;


	------------------------------------------------------------
	-- Calculate the CAD-7 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Cad7EligibleInd = 1 OR glm.LvsdPatientInd = 1 OR glm.PatientOnACEorArb = 1 OR glm.LVSDCode IS NOT NULL OR glm.ACEorARBCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.CadConfirmed = (CASE WHEN gpm.CadConfirmed IS NULL AND glm.Cad7EligibleInd = 1 THEN 2 ELSE gpm.CadConfirmed END),
		gpm.CadDiabetesLvsd = (CASE WHEN gpm.CadDiabetesLvsd IS NULL AND (glm.LvsdPatientInd = 1 OR glm.DiabeticPatientEligibleInd = 1) THEN 2 ELSE gpm.CadDiabetesLvsd END),
		gpm.CadAceArb = (CASE WHEN gpm.CadAceArb IS NULL AND glm.PatientOnACEorArb = 1 THEN 2 ELSE gpm.CadAceArb END),
		gpm.CadComments = (CASE WHEN gpm.CadComments IS NULL AND glm.LvsdPatientInd = 1 AND glm.PatientOnACEorArb = 0 AND glm.LVSDCode IS NOT NULL AND glm.LVSDDate IS NOT NULL THEN 'LVSD Code ' + glm.LVSDCode + ' on date ' + CONVERT(VARCHAR,glm.LVSDDate,101) 
								WHEN gpm.CadComments IS NULL AND glm.PatientOnACEorArb = 1 AND glm.LvsdPatientInd = 0 AND glm.ACEorARBCode IS NOT NULL AND glm.ACEorARBStartDate IS NOT NULL THEN 'ACE/ARB Code ' + glm.ACEorARBCode + ' on date ' + CONVERT(VARCHAR,glm.ACEorARBStartDate,101) 
								WHEN gpm.CadComments IS NULL AND glm.LvsdPatientInd = 1 AND glm.PatientOnACEorArb = 1 AND glm.LVSDCode IS NOT NULL AND glm.LVSDDate IS NOT NULL AND glm.ACEorARBCode IS NOT NULL AND glm.ACEorARBStartDate IS NOT NULL THEN 'LB: LVSD Code ' + glm.LVSDCode + ' on date ' + CONVERT(VARCHAR,glm.LVSDDate,101) + ', ACE/ARB Code ' + glm.ACEorARBCode + ' on date ' + CONVERT(VARCHAR,glm.ACEorARBStartDate,101) 
								ELSE gpm.CadComments 
							END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.CadRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.CadComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'CAD-7'
	WHERE gpm.CadConfirmed = 2 AND gpm.CadDiabetesLvsd = 2 AND gpm.CadAceArb = 2 
		

	------------------------------------------------------------
	-- Calculate the DM-2 and DM-7 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;
	
	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Dm2EligibleInd = 1 OR glm.Dm7EligibleInd = 1 OR glm.HemoglobinA1cTestLastPerformedDate IS NOT NULL OR glm.HemoglobinA1cTestValue IS NOT NULL OR glm.DiabeticCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.DmConfirmed = (CASE WHEN gpm.DmConfirmed IS NULL AND ((glm.Dm2EligibleInd = 1 OR glm.Dm7EligibleInd = 1) OR glm.DiabeticPatientEligibleInd = 1) THEN 2 ELSE gpm.DmConfirmed END),
		gpm.DmHba1cTest = (CASE WHEN gpm.DmHba1cTest IS NULL AND glm.HemoglobinA1cTestLastPerformedDate IS NOT NULL AND glm.HemoglobinA1cTestLastPerformedDate >= @MeasurePeriodBegin AND glm.HemoglobinA1cTestLastPerformedDate <= @MeasurePeriodEnd THEN 2 ELSE gpm.DmHba1cTest END),
		gpm.DmHba1cDate = (CASE WHEN gpm.DmHba1cDate IS NULL AND glm.HemoglobinA1cTestLastPerformedDate IS NOT NULL AND glm.HemoglobinA1cTestLastPerformedDate >= @MeasurePeriodBegin AND glm.HemoglobinA1cTestLastPerformedDate <= @MeasurePeriodEnd THEN CONVERT(VARCHAR,glm.HemoglobinA1cTestLastPerformedDate,101)  ELSE gpm.DmHba1cDate END),
		gpm.DmHba1cValue = (CASE WHEN gpm.DmHba1cValue IS NULL AND glm.HemoglobinA1cTestValue IS NOT NULL AND CONVERT(DECIMAL(5,2),glm.HemoglobinA1cTestValue) BETWEEN CONVERT(DECIMAL(5,2),0.00) AND CONVERT(DECIMAL(5,2),25.00) THEN CONVERT(DECIMAL(5,2),glm.HemoglobinA1cTestValue) ELSE gpm.DmHba1cValue END),
		gpm.DmEyeExam = (CASE WHEN gpm.DmEyeExam IS NULL AND glm.DiabeticEyeDateLastPerformed IS NOT NULL THEN 2 ELSE gpm.DmEyeExam END),
		gpm.DmComments = (CASE WHEN gpm.DmComments IS NULL AND glm.DiabeticDate IS NOT NULL AND glm.DiabeticCode IS NOT NULL AND glm.DiabeticEyeDateLastPerformed IS NULL THEN 'DM Code ' + glm.DiabeticCode + ' on date ' + CONVERT(VARCHAR,glm.DiabeticDate,101) 
								WHEN gpm.DmComments IS NULL AND glm.DiabeticDate IS NOT NULL AND glm.DiabeticCode IS NOT NULL AND glm.DiabeticEyeDateLastPerformed IS NOT NULL AND glm.DiabeticEyeCode IS NOT NULL THEN 'DM Code ' + glm.DiabeticCode + ' on date ' + CONVERT(VARCHAR,glm.DiabeticDate,101) + ', Eye Exam Code ' + glm.DiabeticEyeCode + ' on date ' + CONVERT(VARCHAR,glm.DiabeticEyeDateLastPerformed,101) 
								ELSE gpm.DmComments 
							END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.DmRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.DmComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'DM-2'
	WHERE gpm.DmConfirmed = 2 AND gpm.DmHba1cTest = 2 AND gpm.DmHba1cDate IS NOT NULL AND gpm.DmHba1cValue IS NOT NULL 

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.DmComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'DM-7'
	WHERE gpm.DmConfirmed = 2 AND gpm.DmEyeExam = 2 

	------------------------------------------------------------
	-- Calculate the HTN-2 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.HTN2EligibleInd = 1 OR glm.HTN2Numerator = 1 OR glm.BloodPressureReadingDate IS NOT NULL OR glm.HypertensionCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.HtnConfirmed = (CASE WHEN gpm.HtnConfirmed IS NULL AND glm.HTN2EligibleInd = 1 THEN 2 
								 --WHEN gpm.HtnConfirmed IS NULL AND glm.HTN2Exclusion = 1 THEN 17 
								 ELSE gpm.HtnConfirmed 
							END),
		gpm.HtnRecentBp = (CASE WHEN gpm.HtnRecentBp IS NULL AND (glm.BloodPressureReadingDate IS NOT NULL AND glm.BloodPressureReadingDate >= @MeasurePeriodBegin AND glm.BloodPressureReadingDate <= @MeasurePeriodEnd ) THEN 2
							ELSE gpm.HtnRecentBp END),
		gpm.HtnBpDate = (CASE WHEN gpm.HtnBpDate IS NULL AND glm.BloodPressureReadingDate IS NOT NULL AND glm.BloodPressureReadingDate >= @MeasurePeriodBegin AND glm.BloodPressureReadingDate <= @MeasurePeriodEnd THEN CONVERT(VARCHAR,glm.BloodPressureReadingDate,101) ELSE gpm.HtnBpDate END),
		gpm.HtnBpSystolic = (CASE WHEN gpm.HtnBpSystolic IS NULL AND glm.BloodPressureReadingDate IS NOT NULL AND glm.BloodPressureReadingDate >= @MeasurePeriodBegin AND glm.BloodPressureReadingDate <= @MeasurePeriodEnd AND glm.BloodPressureSystolic IS NOT NULL AND glm.BloodPressureSystolic BETWEEN 0 AND 350 THEN glm.BloodPressureSystolic 	
							 ELSE gpm.HtnBpSystolic END),
		gpm.HtnBpDiastolic = (CASE WHEN gpm.HtnBpDiastolic IS NULL AND glm.BloodPressureReadingDate IS NOT NULL AND glm.BloodPressureReadingDate >= @MeasurePeriodBegin AND glm.BloodPressureReadingDate <= @MeasurePeriodEnd AND glm.BloodPressureDiastolic IS NOT NULL AND glm.BloodPressureDiastolic BETWEEN 0 AND 200 THEN glm.BloodPressureDiastolic 
							  ELSE gpm.HtnBpDiastolic END),
		gpm.HtnComments = (CASE WHEN gpm.HtnComments IS NULL AND glm.HTN2EligibleInd = 1 AND glm.HypertensionDate IS NOT NULL AND glm.HypertensionCode IS NOT NULL THEN 'HTN Code ' + glm.HypertensionCode + ' on date ' + CONVERT(VARCHAR,glm.HypertensionDate,101) 
								ELSE gpm.HtnComments 
							END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.HtnRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.HtnComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'HTN-2'
	WHERE gpm.HtnConfirmed = 2 AND gpm.HtnRecentBp = 2 AND gpm.HtnBpDate IS NOT NULL AND gpm.HtnBpSystolic IS NOT NULL AND gpm.HtnBpDiastolic IS NOT NULL

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Skipped.  ' + gpm.HtnComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'HTN-2'
	WHERE gpm.HtnConfirmed = 17

	------------------------------------------------------------
	-- Calculate the HF-6 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.HF6EligibleInd = 1 OR glm.HF6Numerator = 1 OR glm.LVSDPatientInd = 1 OR glm.HeartFailureCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.HfConfirmed = (CASE WHEN gpm.HfConfirmed IS NULL AND glm.HeartFailureCode IS NOT NULL THEN 2 
								ELSE gpm.HfConfirmed 
							END),
		gpm.HfLvsd = (CASE WHEN gpm.HfLvsd IS NULL AND (glm.LVSDPatientInd = 1 OR glm.LVSDCode IS NOT NULL) THEN 2 ELSE gpm.HfLvsd END),
		gpm.HfBetaBlocker = (CASE WHEN gpm.HfBetaBlocker IS NULL AND (glm.PatientOnBetaBlocker = 1 OR glm.BetaBlockerCode IS NOT NULL) THEN 2 
								  --WHEN gpm.HfBetaBlocker IS NULL AND glm.HF6Exclusion = 1 THEN 4 
								  ELSE gpm.HfBetaBlocker 
							END),
		gpm.HfComments = (CASE WHEN gpm.HfComments IS NULL AND glm.HF6EligibleInd = 1 AND glm.HeartFailureDate IS NOT NULL AND glm.LVSDCode IS NULL AND glm.BetaBlockerCode IS NULL THEN 'HF Code ' + glm.HeartFailureCode + ' on date ' + CONVERT(VARCHAR,glm.HeartFailureDate,101) 
							   WHEN gpm.HfComments IS NULL AND glm.HF6EligibleInd = 1 AND glm.HeartFailureDate IS NOT NULL AND glm.LVSDCode IS NOT NULL AND glm.BetaBlockerCode IS NULL THEN 'HF Code ' + glm.HeartFailureCode + ' on date ' + CONVERT(VARCHAR,glm.HeartFailureDate,101)  + ', LVSD Code ' + glm.LVSDCode + ' on date ' + CONVERT(VARCHAR,glm.LVSDDate,101)
							   WHEN gpm.HfComments IS NULL AND glm.HF6EligibleInd = 1 AND glm.HeartFailureDate IS NOT NULL AND glm.LVSDCode IS NOT NULL AND glm.BetaBlockerCode IS NOT NULL THEN 'HF Code ' + glm.HeartFailureCode + ' on date ' + CONVERT(VARCHAR,glm.HeartFailureDate,101)  + ', LVSD Code ' + glm.LVSDCode + ' on date ' + CONVERT(VARCHAR,glm.LVSDDate,101) + ', BetaBlocker Code ' + glm.BetaBlockerCode + ' on date ' + CONVERT(VARCHAR,glm.BetaBlockerStartDate,101)
							   WHEN gpm.HfComments IS NULL AND glm.HF6EligibleInd = 1 AND glm.HeartFailureDate IS NULL AND glm.LVSDCode IS NOT NULL AND glm.BetaBlockerCode IS NOT NULL THEN 'LVSD Code ' + glm.LVSDCode + ' on date ' + CONVERT(VARCHAR,glm.LVSDDate,101) + ', BetaBlocker Code ' + glm.BetaBlockerCode + ' on date ' + CONVERT(VARCHAR,glm.BetaBlockerStartDate,101)
							   WHEN gpm.HfComments IS NULL AND glm.HF6EligibleInd = 1 AND glm.HeartFailureDate IS NULL AND glm.LVSDCode IS NULL AND glm.BetaBlockerCode IS NOT NULL THEN 'BetaBlocker Code ' + glm.BetaBlockerCode + ' on date ' + CONVERT(VARCHAR,glm.BetaBlockerStartDate,101)
							   ELSE gpm.HfComments 
						 END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.HfRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.HfComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'HF-6'
	WHERE gpm.HfConfirmed = 2 AND gpm.HfLvsd = 2 AND gpm.HfBetaBlocker = 2

	------------------------------------------------------------
	-- Calculate the IVD-2 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.IVDEligibleInd = 1 OR glm.IVDNumerator = 1 OR glm.PatientOnAspirin = 1 OR glm.AspirinCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.IvdConfirmed = (CASE WHEN gpm.IvdConfirmed IS NULL AND glm.IVDEligibleInd = 1 THEN 2 ELSE gpm.IvdConfirmed END),
		gpm.IvdAntithrombotic = (CASE WHEN gpm.IvdAntithrombotic IS NULL AND glm.PatientOnAspirin = 1 THEN 1 ELSE gpm.IvdAntithrombotic END),
		gpm.IvdComments = (CASE WHEN gpm.IvdComments IS NULL AND glm.IVDEligibleInd = 1 AND glm.PatientOnAspirin = 1 THEN 'Aspirin Code ' + glm.AspirinCode + ' on date ' + CONVERT(VARCHAR,glm.AspirinStartDate,101) 
								ELSE gpm.IvdComments 
						 END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.IvdRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.IvdComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'IVD-2'
	WHERE gpm.IvdConfirmed = 2 AND gpm.IvdAntithrombotic = 2

	------------------------------------------------------------
	-- Calculate the MH-1 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;
	
	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.MH1EligibleInd = 1 OR glm.MH1Numerator = 1 OR glm.DepressionInitialPHQ9ScreeningDate IS NOT NULL OR glm.DepressionLatestPHQ9ScreeningDate IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.MhConfirmed = (CASE WHEN gpm.MhConfirmed IS NULL AND glm.MH1EligibleInd = 1 THEN 2 
								--WHEN gpm.MhConfirmed IS NULL AND glm.MH1Exclusion = 1 THEN 17
								ELSE gpm.MhConfirmed 
							END),
		gpm.MhIndexPerformed = (CASE WHEN gpm.MhIndexPerformed IS NULL AND glm.DepressionInitialPHQ9ScreeningDate IS NOT NULL AND (glm.DepressionInitialPHQ9ScreeningDate BETWEEN @MhIndexDateBegin AND @MhIndexDateEnd) THEN 2 
									 ELSE gpm.MhIndexPerformed 
								END),  
		gpm.MhIndexTest = (CASE WHEN gpm.MhIndexTest IS NULL AND CONVERT(INT,ROUND(glm.DepressionInitialPHQ9ScreenScore,0)) > 9 THEN 2 
								WHEN gpm.MhIndexTest IS NULL AND CONVERT(INT,ROUND(glm.DepressionInitialPHQ9ScreenScore,0)) <= 9 THEN 1
								ELSE gpm.MhIndexTest 
							END), 
		gpm.MhIndexDate = (CASE WHEN gpm.MhIndexDate IS NULL AND (glm.DepressionInitialPHQ9ScreeningDate IS NOT NULL AND glm.DepressionInitialPHQ9ScreeningDate BETWEEN @MhIndexDateBegin AND @MhIndexDateEnd) THEN CONVERT(VARCHAR,glm.DepressionInitialPHQ9ScreeningDate,101) ELSE gpm.MhIndexDate END),
		gpm.MhIndexScore = (CASE WHEN gpm.MhIndexScore IS NULL AND glm.MH1Numerator = 1 AND glm.DepressionInitialPHQ9ScreenScore IS NOT NULL AND CONVERT(INT,ROUND(glm.DepressionInitialPHQ9ScreenScore,0)) > 9 THEN CONVERT(INT,ROUND(glm.DepressionInitialPHQ9ScreenScore,0)) ELSE gpm.MhIndexScore END),
		gpm.MhFollowupPerformed = (CASE WHEN gpm.MhFollowupPerformed IS NULL AND glm.DepressionLatestPHQ9ScreeningDate BETWEEN (glm.DepressionInitialPHQ9ScreeningDate + 335) AND (glm.DepressionInitialPHQ9ScreeningDate + 395) THEN 2 
										ELSE gpm.MhFollowupPerformed 
									END),
		gpm.MhFollowupTest = (CASE WHEN gpm.MhFollowupTest IS NULL AND CONVERT(INT,ROUND(glm.DepressionLatestPHQ9ScreenScore,0)) < 5 THEN 2 
								   WHEN gpm.MhFollowupTest IS NULL AND CONVERT(INT,ROUND(glm.DepressionLatestPHQ9ScreenScore,0)) >= 5 THEN 1
								   ELSE gpm.MhFollowupTest 
							  END),
		gpm.MhFollowupDate = (CASE WHEN gpm.MhFollowupDate IS NULL AND glm.DepressionLatestPHQ9ScreeningDate IS NOT NULL THEN  CONVERT(VARCHAR,glm.DepressionLatestPHQ9ScreeningDate,101) ELSE gpm.MhFollowupDate END),
		gpm.MhFollowupScore = (CASE WHEN gpm.MhFollowupScore IS NULL AND glm.DepressionLatestPHQ9ScreenScore IS NOT NULL AND CONVERT(INT,ROUND(glm.DepressionLatestPHQ9ScreenScore,0)) < 5 THEN CONVERT(INT,ROUND(glm.DepressionLatestPHQ9ScreenScore,0)) ELSE gpm.MhFollowupScore END),
		gpm.MhComments = (CASE WHEN gpm.MhComments IS NULL AND glm.DepressionLatestPHQ9ScreenScore IS NOT NULL AND glm.DepressionLatestPHQ9ScreeningDate IS NOT NULL THEN 'Last PHQ9 Score ' + CONVERT(VARCHAR,glm.DepressionLatestPHQ9ScreenScore) + ' on date ' + CONVERT(VARCHAR,glm.DepressionLatestPHQ9ScreeningDate,101)
							   ELSE gpm.MhComments 
						 END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.MhRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.MhComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'MH-1'
	WHERE gpm.MhConfirmed = 2 AND gpm.MhIndexPerformed = 2 AND (gpm.MhIndexTest = 1 OR (gpm.MhIndexTest = 2 AND gpm.MhIndexDate IS NOT NULL AND gpm.MhIndexScore IS NOT NULL))

	--INSERT INTO GproAuditHistory
	--	(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	--SELECT
	--	gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Skipped.  ' + gpm.MhComments, GETUTCDATE(),GETUTCDATE(),1,1
	--FROM GproPatientMeasure gpm 
	--	INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
	--	INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
	--	INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'MH-1'
	--WHERE gpm.MhConfirmed = 17 OR gpm.MhIndexTest = 1 OR gpm.MhFollowupTest = 1

	------------------------------------------------------------
	-- Calculate the PREV-5 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Prev5EligibleInd = 1 OR glm.Prev5Numerator = 1 OR glm.BreastExamCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.PcMammogramConfirmed = (CASE WHEN gpm.PcMammogramConfirmed IS NULL AND glm.Prev5EligibleInd = 1 AND glm.Prev5Exclusion = 0 THEN 2 
										 --WHEN gpm.PcMammogramConfirmed IS NULL AND glm.Prev5EligibleInd = 1 AND glm.Prev5Exclusion = 1 THEN 17 
										 ELSE gpm.PcMammogramConfirmed 
									END),
		gpm.PcMammogramPerformed = (CASE WHEN gpm.PcMammogramPerformed IS NULL AND glm.BreastExamCode IS NOT NULL THEN 2 ELSE gpm.PcMammogramPerformed END),
		gpm.PcMammogramComments = (CASE WHEN gpm.PcMammogramComments IS NULL AND glm.BreastExamCode IS NOT NULL THEN 'Exam Code ' + glm.BreastExamCode + ' on date ' + CONVERT(VARCHAR,glm.BreastExamLastPerformedDate,101) 
										--WHEN gpm.PcMammogramComments IS NULL AND glm.Prev5EligibleInd = 1 AND glm.Prev5Exclusion = 1 THEN 'Exception Skip'
										ELSE gpm.PcMammogramComments 
									END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.PcMammogramRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.PcMammogramComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-5'
	WHERE gpm.PcMammogramConfirmed = 2 AND gpm.PcMammogramPerformed = 2

	------------------------------------------------------------
	-- Calculate the PREV-6 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Prev6EligibleInd = 1 OR glm.Prev6Numerator = 1 OR glm.ColonExamCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.PcColorectalConfirmed = (CASE WHEN gpm.PcColorectalConfirmed IS NULL AND glm.Prev6EligibleInd = 1 AND glm.Prev6Exclusion = 0 THEN 2 
										  --WHEN gpm.PcColorectalConfirmed IS NULL AND glm.Prev6EligibleInd = 1 AND glm.Prev6Exclusion = 1 THEN 17
										  ELSE gpm.PcColorectalConfirmed 
									  END),
		gpm.PcColorectalPerformed = (CASE WHEN gpm.PcColorectalPerformed IS NULL AND glm.ColonExamCode IS NOT NULL THEN 2 ELSE gpm.PcColorectalPerformed END),
		gpm.PcColorectalComments = (CASE WHEN gpm.PcColorectalComments IS NULL AND glm.ColonExamCode IS NOT NULL THEN 'Screening Code ' + glm.ColonExamCode + ' on date ' + CONVERT(VARCHAR,glm.ColonExamLastPerformedDate,101) 
										--WHEN gpm.PcColorectalComments IS NULL AND glm.Prev6EligibleInd = 1 AND glm.Prev6Exclusion = 1 THEN 'Exception Skip'
										ELSE gpm.PcColorectalComments 
									END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.PcColorectalRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.PcColorectalComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-6'
	WHERE gpm.PcColorectalConfirmed = 2 AND gpm.PcColorectalPerformed = 2

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Skipped.  ' + gpm.PcColorectalComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-6'
	WHERE gpm.PcColorectalConfirmed = 17

	------------------------------------------------------------
	-- Calculate the PREV-7 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Prev7EligibleInd = 1 OR glm.Prev7Numerator = 1 OR glm.InfluenzaCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.PcFluShotConfirmed = (CASE WHEN gpm.PcFluShotConfirmed IS NULL AND glm.Prev7EligibleInd = 1 THEN 2
									   ELSE gpm.PcFluShotConfirmed 
								  END),
		gpm.PcFluShotReceived = (CASE WHEN gpm.PcFluShotReceived IS NULL AND (glm.Prev7Numerator = 1 OR glm.InfluenzaCode IS NOT NULL) THEN 2
									  --WHEN gpm.PcFluShotReceived IS NULL AND glm.Prev7Exclusion = 1 THEN 4
									  ELSE gpm.PcFluShotReceived 
								  END),
		gpm.PcFluShotComments = (CASE WHEN gpm.PcFluShotComments IS NULL AND glm.InfluenzaCode IS NOT NULL THEN 'Flu Code ' + glm.InfluenzaCode + ' on date ' + CONVERT(VARCHAR,glm.InfluenzaLastPerformedDate,101) 
									  ELSE gpm.PcFluShotComments 
								 END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.PcFluShotRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.PcFluShotComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-7'
	WHERE gpm.PcFluShotConfirmed = 2 AND gpm.PcFluShotReceived IN (2,4)

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Skipped.  ' + gpm.PcFluShotComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-7'
	WHERE gpm.PcFluShotConfirmed = 17

	------------------------------------------------------------
	-- Calculate the PREV-8 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Prev8EligibleInd = 1 OR glm.Prev8Numerator = 1 OR glm.PnueVaxCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.PcPneumoShotConfirmed = (CASE WHEN gpm.PcPneumoShotConfirmed IS NULL AND glm.Prev8EligibleInd = 1 THEN 2 ELSE gpm.PcPneumoShotConfirmed END),
		gpm.PcPneumoShotReceived = (CASE WHEN gpm.PcPneumoShotReceived IS NULL AND glm.PnueVaxCode IS NOT NULL THEN 2 ELSE gpm.PcPneumoShotReceived END),
		gpm.PcPneumoShotComments = (CASE WHEN gpm.PcPneumoShotComments IS NULL AND glm.PnueVaxCode IS NOT NULL THEN 'Pneumo Code ' + glm.PnueVaxCode + ' on date ' + CONVERT(VARCHAR,glm.PnueVaxLastPerformedDate,101) 
										 ELSE gpm.PcPneumoShotComments 
								    END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.PcFluShotRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.PcPneumoShotComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-8'
	WHERE gpm.PcPneumoShotConfirmed = 2 AND gpm.PcPneumoShotReceived = 2

	------------------------------------------------------------
	-- Calculate the PREV-9 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Prev9EligibleInd = 1 OR glm.Prev9Numerator = 1 OR glm.BMIReadingValue IS NOT NULL OR glm.BMINormal = 1) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.PcBmiScreenConfirmed = (CASE WHEN gpm.PcBmiScreenConfirmed IS NULL AND glm.Prev9EligibleInd = 1 AND glm.Prev9Exclusion = 0 THEN 2 
										 --WHEN gpm.PcBmiScreenConfirmed IS NULL AND glm.Prev9EligibleInd = 1 AND glm.Prev9Exclusion = 1 THEN 17
										 ELSE gpm.PcBmiScreenConfirmed 
									END),
		gpm.PcBmiCalculated = (CASE WHEN gpm.PcBmiCalculated IS NULL AND glm.BMIReadingValue IS NOT NULL THEN 2
									--WHEN gpm.PcBmiCalculated IS NULL AND glm.Prev9Exclusion = 1 THEN 4
									ELSE gpm.PcBmiCalculated 
								END),
		gpm.PcBmiNormal = (CASE WHEN gpm.PcBmiNormal IS NULL AND glm.BMINormal = 1 THEN 1
								WHEN gpm.PcBmiNormal IS NULL AND glm.BMIReadingValue IS NOT NULL AND glm.BMINormal = 0 THEN 0
								ELSE gpm.PcBmiNormal 
							END),
		gpm.PcBmiPlan = (CASE WHEN gpm.PcBmiPlan IS NULL AND glm.BmiFollowUpCode IS NOT NULL THEN 2 ELSE gpm.PcBmiPlan END),
		gpm.PcBmiComments = (CASE WHEN gpm.PcBmiComments IS NULL AND glm.BMIReadingValue IS NOT NULL AND glm.BMIFollowUpCode IS NULL THEN 'BMI ' + CONVERT(VARCHAR,glm.BMIReadingValue) + ' on date ' + CONVERT(VARCHAR,glm.BMILastReadingDate,101) 
								  WHEN gpm.PcBmiComments IS NULL AND glm.BMIReadingValue IS NOT NULL AND glm.BMIFollowUpCode IS NOT NULL THEN 'BMI ' + CONVERT(VARCHAR,glm.BMIReadingValue) + ' on date ' + CONVERT(VARCHAR,glm.BMILastReadingDate,101) + ', Follow Up Code ' + glm.BMIFollowUpCode + ' on date ' + CONVERT(VARCHAR,glm.BMIFollowupDate,101) 
								  --WHEN gpm.PcBmiComments IS NULL AND glm.Prev9EligibleInd = 1 AND glm.Prev9Exclusion = 1 THEN 'Exception Skip'
								  ELSE gpm.PcBmiComments 
							 END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.PcBmiScreenRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.PcBmiComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-9'
	WHERE gpm.PcBmiScreenConfirmed = 2 AND gpm.PcBmiCalculated = 2 AND (gpm.PcBmiNormal = 2 OR (gpm.PcBmiNormal = 1 AND gpm.PcBmiPlan = 2));

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Skipped.  ' + gpm.PcBmiComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-9'
	WHERE gpm.PcBmiScreenConfirmed = 17 OR gpm.PcBmiCalculated = 4

	------------------------------------------------------------
	-- Calculate the PREV-10 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Prev10EligibleInd = 1 OR glm.Prev10Numerator = 1 OR glm.PREV10TobaccoCounselingDate IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.PcTobaccoConfirmed = (CASE WHEN gpm.PcTobaccoConfirmed IS NULL AND glm.Prev10EligibleInd = 1 THEN 2 ELSE gpm.PcTobaccoConfirmed END),
		gpm.PcTobaccoScreen = (CASE WHEN gpm.PcTobaccoScreen IS NULL AND glm.PatientIsSmokerInd IS NOT NULL AND glm.PatientIsSmokerInd = 1 THEN 2 
									WHEN gpm.PcTobaccoScreen IS NULL AND glm.PatientIsSmokerInd IS NOT NULL AND glm.PatientIsSmokerInd = 0 THEN 1 
									ELSE gpm.PcTobaccoScreen 
								END),
		gpm.PcTobaccoCounsel = (CASE WHEN gpm.PcTobaccoCounsel IS NULL AND (glm.PREV10TobaccoCounselingDate IS NOT NULL) THEN 2 ELSE gpm.PcTobaccoCounsel END),
		gpm.PcTobaccoComments = (CASE WHEN gpm.PcTobaccoComments IS NULL AND glm.PREV10TobaccoScreeningCode IS NOT NULL AND glm.PREV10TobaccoCounselingDate IS NULL THEN 'Screening Code ' + glm.PREV10TobaccoScreeningCode + ' on date ' + CONVERT(VARCHAR,glm.PREV10TobaccoScreeningDate,101) 
									  WHEN gpm.PcTobaccoComments IS NULL AND glm.PREV10TobaccoScreeningCode IS NOT NULL AND glm.PREV10TobaccoCounselingDate IS NOT NULL THEN 'Screening Code ' + glm.PREV10TobaccoScreeningCode + ' on date ' + CONVERT(VARCHAR,glm.PREV10TobaccoScreeningDate,101) + ', Counseling Code ' + glm.PREV10TobaccoCounselingCode + ' on date ' + CONVERT(VARCHAR,glm.PREV10TobaccoCounselingDate,101)
									  ELSE gpm.PcTobaccoComments 
								 END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.PcTobaccoRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.PcTobaccoComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-10'
	WHERE gpm.PcTobaccoConfirmed = 2 AND ((gpm.PcTobaccoScreen = 1) OR (gpm.PcTobaccoScreen = 1 AND gpm.PcTobaccoCounsel = 2))

	------------------------------------------------------------
	-- Calculate the PREV-11 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.PREV11EligibleInd = 1 OR glm.PREV11Numerator = 1 OR glm.BloodPressureFollowupCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.PcBloodPressureConfirmed = (CASE WHEN gpm.PcBloodPressureConfirmed IS NULL AND glm.PREV11EligibleInd = 1 AND glm.PREV11Exclusion = 0 THEN 2
											 --WHEN gpm.PcBloodPressureConfirmed IS NULL AND glm.PREV11EligibleInd = 1 AND glm.PREV11Exclusion = 1 THEN 17
											ELSE gpm.PcBloodPressureConfirmed 
										END),
		gpm.PcBloodPressureScreen = (CASE WHEN gpm.PcBloodPressureScreen IS NULL AND glm.BloodPressureSystolic IS NOT NULL AND glm.BloodPressureDiastolic IS NOT NULL THEN 2
										  --WHEN gpm.PcBloodPressureScreen IS NULL AND glm.PREV11Exclusion = 1 THEN 4
										  ELSE gpm.PcBloodPressureScreen 
									  END),
		gpm.PcBloodPressureNormal = (CASE WHEN gpm.PcBloodPressureNormal IS NULL AND glm.BloodPressureNormal = 1 THEN 2
										  WHEN gpm.PcBloodPressureNormal IS NULL AND glm.BloodPressureNormal = 0 THEN 1
										  ELSE gpm.PcBloodPressureNormal 
									  END),
		gpm.PcBloodPressureFollowUp = (CASE WHEN gpm.PcBloodPressureFollowUp IS NULL AND glm.BloodPressureNormal = 0 AND glm.BloodPressureSystolic IS NOT NULL AND glm.BloodPressureDiastolic IS NOT NULL THEN 2
											WHEN gpm.PcBloodPressureFollowup IS NULL AND glm.BloodPressureNormal = 0 AND glm.BloodPressureFollowupCode IS NOT NULL THEN 2
											ELSE gpm.PcBloodPressureFollowUp 
										END),
		gpm.PcBloodPressureComments = (CASE WHEN gpm.PcBloodPressureComments IS NULL AND glm.PREV11Numerator = 1 AND glm.BloodPressureFollowupDate IS NULL THEN 'BP ' + CONVERT(VARCHAR,glm.BloodPressureSystolic) + '/' + CONVERT(VARCHAR,glm.BloodPressureDiastolic) + ' on date ' + CONVERT(VARCHAR,glm.BloodPressureReadingDate,101) 
											WHEN gpm.PcBloodPressureComments IS NULL AND glm.PREV11Numerator = 1 AND glm.BloodPressureFollowupDate IS NOT NULL THEN 'BP ' + CONVERT(VARCHAR,glm.BloodPressureSystolic) + '/' + CONVERT(VARCHAR,glm.BloodPressureDiastolic) + ' on date ' + CONVERT(VARCHAR,glm.BloodPressureReadingDate,101) + ', Follow-up Code ' + glm.BloodPressureFollowupCode + ' on date ' + CONVERT(VARCHAR,glm.BloodPressureFollowupDate,101) 
											--WHEN gpm.PcBloodPressureComments IS NULL AND glm.PREV11EligibleInd = 1 AND glm.PREV11Exclusion = 1 THEN 'Exception Skip'
											ELSE gpm.PcBloodPressureComments 
										END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.PcBloodPressureRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.PcBloodPressureComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-11'
	WHERE gpm.PcBloodPressureConfirmed = 2 AND gpm.PcBloodPressureScreen = 2 AND (gpm.PcBloodPressureNormal = 2 OR (gpm.PcBloodPressureNormal = 1 AND gpm.PcBloodPressureFollowUp = 2))

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Skipped.  ' + gpm.PcFluShotComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-11'
	WHERE gpm.PcBloodPressureConfirmed = 17 or gpm.PcBloodPressureScreen = 4

	------------------------------------------------------------
	-- Calculate the PREV-12 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.Prev12EligibleInd = 1 OR glm.Prev12Numerator = 1 OR glm.DepressionCode IS NOT NULL) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.PcDepressionConfirmed = (CASE WHEN gpm.PcDepressionConfirmed IS NULL AND glm.Prev12EligibleInd = 1 AND glm.Prev12Exclusion = 0 THEN 2 
										  --WHEN gpm.PcDepressionConfirmed IS NULL AND glm.Prev12EligibleInd = 1 AND glm.Prev12Exclusion = 1 THEN 17
										  ELSE gpm.PcDepressionConfirmed 
									 END),
		gpm.PcDepressionScreen = (CASE WHEN gpm.PcDepressionScreen IS NULL AND glm.DepressionScreeningPatientEligibleInd = 1 THEN 2 ELSE gpm.PcDepressionScreen END),
		gpm.PcDepressionPositive = (CASE WHEN gpm.PcDepressionPositive IS NULL AND glm.DepressionScreenPositiveInd = 1 THEN 2 
										 WHEN gpm.PcDepressionPositive IS NULL AND glm.DepressionScreeningPatientEligibleInd = 1 AND glm.DepressionScreenPositiveInd = 0 THEN 1 
										 ELSE gpm.PcDepressionPositive 
									END),
		gpm.PcDepressionPlan = (CASE WHEN gpm.PcDepressionPlan IS NULL AND glm.DepressionScreenFollowUpInd = 1 THEN 2 ELSE gpm.PcDepressionPlan END),
		gpm.PcDepressionComments = (CASE WHEN gpm.PcDepressionComments IS NULL AND glm.DepressionScreenCode IS NOT NULL AND glm.DepressionScreenFollowUpCode IS NULL THEN 'Screening Code ' + glm.DepressionScreenCode + ' on date ' + CONVERT(VARCHAR,glm.DepressionScreenDate,101) 
										 WHEN gpm.PcDepressionComments IS NULL AND glm.DepressionScreenCode IS NOT NULL AND glm.DepressionScreenFollowUpCode IS NOT NULL THEN 'Screening Code ' + glm.DepressionScreenCode + ' on date ' + CONVERT(VARCHAR,glm.DepressionScreenDate,101) + ', Follow Up ' + glm.DepressionScreenFollowUpCode + ' on date ' + CONVERT(VARCHAR,glm.DepressionScreenFollowUpDate,101)
										 --WHEN gpm.PcDepressionComments IS NULL AND glm.Prev12EligibleInd = 1 AND glm.Prev12Exclusion = 1 THEN 'Exception Skip'
										 ELSE gpm.PcDepressionComments 
								    END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.PcDepressionRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.PcDepressionComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-12'
	WHERE gpm.PcDepressionConfirmed = 2 AND gpm.PcDepressionScreen = 2 AND (gpm.PcDepressionPositive = 1 OR (gpm.PcDepressionPositive = 2 AND gpm.PcDepressionPlan = 2))

	------------------------------------------------------------
	-- Calculate the PREV-13 measure completion
	------------------------------------------------------------
	TRUNCATE TABLE #updatedRows;

	UPDATE gpm
		SET gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1,
		gpm.MedicalRecordFound = (CASE WHEN gpm.MedicalRecordFound IS NULL AND (glm.ASCVDPatientEligibleInd = 1 OR glm.LDL190PatientEligibleInd = 1 OR glm.LDL70189PatientEligibleInd = 1 OR glm.PatientOnStatin = 1) THEN 2 ELSE gpm.MedicalRecordFound END),
		gpm.PcStatinAscvd = (CASE WHEN gpm.PcStatinAscvd IS NULL AND glm.ASCVDPatientEligibleInd = 1 THEN 2 
								  WHEN gpm.PcStatinAscvd IS NULL AND glm.ASCVDPatientEligibleInd = 0 AND glm.LDL190PatientEligibleInd = 1 THEN 23
								  ELSE gpm.PcStatinAscvd 
							 END),
		gpm.PcStatinLdlc = (CASE WHEN gpm.PcStatinLdlc IS NULL AND glm.LDL190PatientEligibleInd = 1 THEN 2 ELSE gpm.PcStatinLdlc END),
		gpm.PcStatinDiabetes = (CASE WHEN gpm.PcStatinDiabetes IS NULL AND glm.DiabeticPatientEligibleInd = 1 AND glm.DiabeticCode IS NOT NULL THEN 2 ELSE gpm.PcStatinDiabetes END),
		gpm.PcStatinLdlcReading = (CASE WHEN gpm.PcStatinLdlcReading IS NULL AND glm.LDL70189PatientEligibleInd = 1 THEN 2 ELSE gpm.PcStatinLdlcReading END),
		gpm.PcStatinPrescribed = (CASE WHEN gpm.PcStatinPrescribed IS NULL AND glm.PatientOnStatin = 1 THEN 2 ELSE gpm.PcStatinPrescribed END),
		gpm.PcStatinComments = (CASE WHEN gpm.PcStatinComments IS NULL AND glm.ASCVDCode IS NOT NULL AND glm.StatinCode IS NULL THEN 'ASCVD code ' + glm.ASCVDCode + ' on date ' +  CONVERT(VARCHAR,glm.ASCVDDate,101)
									WHEN gpm.PcStatinComments IS NULL AND glm.ASCVDCode IS NOT NULL AND glm.StatinCode IS NOT NULL THEN 'ASCVD code ' + glm.ASCVDCode + ' on date ' +  CONVERT(VARCHAR,glm.ASCVDDate,101) + ', Statin Code ' + glm.StatinCode + ' on date ' + CONVERT(VARCHAR,glm.StatinStartDate,101) 
									WHEN gpm.PcStatinComments IS NULL AND glm.ASCVDCode IS NULL AND glm.DiabeticCode IS NOT NULL AND glm.StatinCode IS NULL THEN 'Diabetic code ' + glm.DiabeticCode + ' on date ' +  CONVERT(VARCHAR,glm.DiabeticDate,101) 
									WHEN gpm.PcStatinComments IS NULL AND glm.ASCVDCode IS NULL AND glm.DiabeticCode IS NOT NULL AND glm.StatinCode IS NOT NULL THEN 'Diabetic code ' + glm.DiabeticCode + ' on date ' +  CONVERT(VARCHAR,glm.DiabeticDate,101) + ', Statin Code ' + glm.StatinCode + ' on date ' + CONVERT(VARCHAR,glm.StatinStartDate,101) 
									WHEN gpm.PcStatinComments IS NULL AND glm.ASCVDCode IS NULL AND glm.DiabeticCode IS NULL AND glm.StatinCode IS NOT NULL THEN 'Statin Code ' + glm.StatinCode + ' on date ' + CONVERT(VARCHAR,glm.StatinStartDate,101) 
									ELSE gpm.PcStatinComments 
								END)
	OUTPUT INSERTED.GproPatientMeasureId INTO #updatedRows
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatientMonitor glm ON glm.LbPatientId = gpr.LbPatientId
	WHERE gpm.PcStatinRank IS NOT NULL;

	INSERT INTO GproAuditHistory
		(GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId, Comment, CreateDateTime,ModifyDateTime, CreateLbUserId, ModifyLbUserId)
	SELECT
		gpm.GproPatientRankingId, gpm.GproPatientMeasureId, gprgmt.GproMeasureTypeId, 'Automatically Completed.  ' + gpm.PcStatinComments, GETUTCDATE(),GETUTCDATE(),1,1
	FROM GproPatientMeasure gpm 
		INNER JOIN #updatedRows ur ON ur.GproPatientMeasureId = gpm.GproPatientMeasureId
		INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-13'
	WHERE (gpm.PcStatinAscvd = 2 AND gpm.PcStatinPrescribed = 2)
		OR (gpm.PcStatinAscvd = 1 AND gpm.PcStatinLdlc = 2 AND gpm.PcStatinPrescribed = 2)
		OR (gpm.PcStatinAscvd = 1 AND gpm.PcStatinLdlc = 1 AND gpm.PcStatinDiabetes = 2 AND gpm.PcStatinLdlcReading = 2 AND gpm.PcStatinPrescribed = 2)

	------------------------------------------------------------
	-- Calculate the status of all of the GPRO patients
	------------------------------------------------------------
	EXEC GproPatientStatusSet;
END

--exec GproMeasureCalculate
--select * from GproPatientMonitor
--drop table #updatedRows
GO
