SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create PROCEDURE [dbo].[ETLPatientVitals] (
	@fullLoadFlag bit = 0, 
	@BatchSize INT = 10000, 
	@InterfacePatientId VARCHAR(50) = NULL,
	@overrideMinIndex INT = NULL
	
	)
AS
BEGIN

  /*===============================================================
	CREATED BY: 	Youping
	CREATED ON:		2016-06-23
	INITIAL VER:	2.0.1
	MODULE:			ETL
	DESCRIPTION:	Load Interface PatientVitals Data into EDW
	PARAMETERS:		
		 @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
		 @fullLoadFlag =0 Merge date to the existing table
		 @BatchSize = Batch size to run.
	
	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.0.1		2016-06-28		Youping					merge failed due to duplications of value NULL/0
				2016-07-28		Youping					Add procedure running log to [Maintenance].[dbo].[EdwProcedureRunLog]
				2016-09-08		Youping	    LBETL-58	Change dedup logic SourceSystemId,PatientID,ServiceDateTime		
	2.1.2		2016-12-08		CJL			LBETL-289	Changed logic in the extraction routines to roll up vitals to the last not-null values
	2.1.2		2016-12-10		CJL			LBETL-289	Added compensentory change to treat 0 values int he following fields as 'null
															HeightCM, WeightKG, TemperatureCelcius, Pulse, Respiration, BloodPressureSystolic, BloodPressureDiastolic
	
	2.1.2		2016-12-31		CJL			LBETL-399	Made changes to the stored procedure to handle the vitals in batches, to reduce the load on the temp tables
														Removed the sorts on create dates. Since the records in the Interface table are never updated and it has a incremental seeded key, we can use that 
														to implicitly sort. higher keys are newer records
	2.1.2		2016-12-31		CJL			LBDM-1416	Fixed an issue in which the measurements do not roll up if the time. For example, 
														 2015-11-24 01:25:55.000
														 2015-11-24 01:25:56.000
	2.1.2		2016-12-31		CJL			LBDM-1416	Fixed an issue to compensate for missing values int he patient identifier field. We are joining on the interfacepatientid as opposed to the 
	2.1.2		2016-12-31		CJL			LBDM-892	Fixed an issue to compensate for missing values int he patient identifier field. We are joining on the patientId as opposed to interfacepatientid now to reduce merge conflicts
				

														 


=================================================================*/


	SET NOCOUNT ON
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientVitals';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientVitals';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @RowCount INT =0;
	DECLARE @NumRows INT = 0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);

	DECLARE @Today DATE=GETUTCDATE();
	BEGIN TRY
	
		IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] =@EDWName)
		BEGIN
				EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed
		END


		--1) Retrieve the last run time id values.  We are going to pull both the date and the last run time id. If the last max id 
		--is null, calculate a best guese based on the 
		--We are changing away from using date values for query purposes as it is more efficient to query by the Id value.  Since
		--the id is a seeded, integer greater id values are implicitly newer. It also allows us to pick up records that come in while the 
		--procedure is running the next time it runs
		IF @fullLoadFlag = 1
		BEGIN
			UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME), maxSourceIdProcessed= 0
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
		END

		 SET @lastDateTime = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	
	
		
		SET  @lastIndex = (SELECT maxSourceIdProcessed
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex= (SELECT MIN(InterfacePatientVitalsId) FROM  [vwPortal_InterfacePatientVitals] IPV WITH (NOLOCK) WHERE IPV.CreateDateTime >= @lastDateTime ) 
		END
		IF @overrideMinIndex IS NOT NULL
		BEGIN
			SET @lastIndex= @overrideMinIndex
		END


		SET @maxIndex = (SELECT MAX(InterfacePatientVitalsId) FROM  [vwPortal_InterfacePatientVitals] IPV WITH (NOLOCK));
	
		--2) Get the full list of patient identifiers that we are going to be dealing with
		--We are going to breaking this up into batches along patient id lines.
		SELECT DISTINCT IP.PatientIdentifier, IPV.InterfaceSystemId INTO #PatientVitalsQuePatientIdentifier 
			FROM [dbo].[vwPortal_InterfacePatientVitals] IPV with (nolock)
				INNER JOIN [dbo].[vwPortal_InterfacePatient] IP with (nolock) ON IPV.InterfacePatientId = IP.InterfacePatientId AND  IPV.InterfaceSystemId = IP.InterfaceSystemId
				INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) ON IP.InterfaceSystemId = ISS.[InterfaceSystemId]
				WHERE ( IPV.InterfacePatientVitalsId >= @lastIndex  AND IPV.InterfacePatientVitalsId <= @maxIndex )
				AND ( ( @InterfacePatientId IS NULL )  OR ( @InterfacePatientId = IP.InterfacePatientId))
				AND [ServiceDateTime] IS NOT NULL 
		
		

		SELECT @RecordCountBefore=COUNT(1) FROM [dbo].[PatientVitals] WITH (NOLOCK);


		--- Truncate data if we are doing a full load
		IF (@fullLoadFlag=1)
		BEGIN
			Truncate table [dbo].[PatientVitals];
		END

		
		--3) Loop throught all of the batches to improve memory efficency
		WHILE 1 = 1
		BEGIN
    			SELECT TOP (@BatchSize) PatientIdentifier, InterfaceSystemId 
					INTO #PatientVitalsQuePatientIdentifieBATCH FROM #PatientVitalsQuePatientIdentifier;
				Create  NONCLUSTERED INDEX IDX_PatientVitalsQuePatientIdentifieBATCH_PatientIdentifier ON #PatientVitalsQuePatientIdentifieBATCH (PatientIdentifier);
			
    			SET @NUMROWS = ( SELECT COUNT(1) FROM #PatientVitalsQuePatientIdentifieBATCH)

			IF @NUMROWS <> 0 
			BEGIN

						--Create working set of patient vitals to process based on the first batch of patients
						SELECT  [InterfacePatientVitalsId]
									,ISS.SourceSystemId
									,idRef.LBPatientId as PatientId,
									--,IP.PatientIdentifier,
									--IP.InterfacePatientId,
									CAST(CONVERT(VARCHAR(16), [ServiceDateTime], 120) as datetime)  as [ServiceDateTime]  --LBDM-1614, round service date to the latest minutes.
									,[HeightCM]
									,[HeightComments]
									,[WeightKG]
									,[WeightComments]
									,[TemperatureCelcius]
									,[TemperatureComments]
									,[Pulse]
									,[PulseComments]
									,[Respiration]
									,[RespirationComments]
									,[BloodPressureSystolic]
									,[BloodPressureDiastolic]
									,[BloodPressureComment]
									,[OxygenSaturationSP02]
									,[OxygenSaturationComment]
									,[Clinician]
									,[PatientVitalsComment]
									,IPV.[CreateDateTime]
									,IPV.EncounterIdentifier
									,[RenderingProviderNPI]
								INTO 	#PatientVitalsQue  
								FROM [dbo].[vwPortal_InterfacePatientVitals] IPV with (nolock),
									[dbo].[vwPortal_InterfacePatient] IP with (nolock), 
									[dbo].[vwPortal_InterfaceSystem]  ISS with (nolock), 
									 #PatientVitalsQuePatientIdentifieBATCH batch, 
										PatientIdReference idRef with (nolock) 	  
								WHERE (IPV.InterfacePatientVitalsId >= @lastIndex  AND IPV.InterfacePatientVitalsId <= @maxIndex)
								  AND  IPV.InterfacePatientId = IP.InterfacePatientId AND  IP.InterfaceSystemId = ISS.[InterfaceSystemId] 
								AND IP.PatientIdentifier = batch.PatientIdentifier AND IP.InterfaceSystemId = batch.InterfaceSystemId 	AND [ServiceDateTime] IS NOT NULL
								AND  idRef.SourceSystemId = ISS.SourceSystemId 
									  AND idRef.ExternalId = batch.PatientIdentifier 
									  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
									  AND idRef.DeleteInd =0	



 						Create  NONCLUSTERED INDEX IDX_PatientIdentifier_SourceSystemId ON #PatientVitalsQue (PatientId,SourceSystemId);
     


						--LBETL-289 Changed the staging query to roll the data up to the latest non-null value
						SELECT * 
								INTO #PatientVitals
								FROM (

						SELECT DISTINCT
								ipv.PatientID, ipv.SourceSystemId,  ipv.ServiceDateTime, h.HeightCM, 
								h.HeightComments, w.WeightKG,  w.WeightComments,  t.TemperatureCelcius,
								t.TemperatureComments, p.Pulse,  p.PulseComments,  r.Respiration,
								r.RespirationComments,    bps.BloodPressureSystolic,   bpd.BloodPressureDiastolic, bps.BloodPressureComment,
								o.OxygenSaturationSP02, o.OxygenSaturationComment,  c.Clinician,  vc.PatientVitalsComment,
								e.EncounterIdentifier,   npi.RenderingProviderNPI
						FROM 
								(SELECT PatientId,  SourceSystemId, ServiceDateTime, EncounterIdentifier,
								ROW_NUMBER() OVER (PARTITION BY PatientId,  ServiceDateTime,  SourceSystemID, ISNULL(EncounterIdentifier,'') ORDER BY InterfacePatientVitalsId DESC) rowNumber 
								FROM #PatientVitalsQue WITH (NOLOCK)) ipv 
  				--		INNER JOIN dbo.PatientIdReference idRef with (nolock) ON idRef.SourceSystemId = ipv.SourceSystemId 
						--				AND idRef.ExternalId = ipv.PatientIdentifier 
						--				AND idRef.IdTypeDesc ='PatientIdentifier'
						--				AND idRef.DeleteInd =0
						LEFT OUTER JOIN
								(SELECT v1.PatientId,v1.ServiceDateTime,v1.HeightCM, v1.SourceSystemId, v1.EncounterIdentifier, 
								v1.HeightComments, ROW_NUMBER() OVER 
								(PARTITION BY v1.PatientId, v1.ServiceDateTime, v1.SourceSystemId, ISNULL(EncounterIdentifier,'') ORDER BY v1.InterfacePatientVitalsId DESC) rowNumber 
								FROM #PatientVitalsQue  v1 WITH (NOLOCK) WHERE v1.heightCM IS NOT NULL  AND v1.heightCM <> 0.00 ) h ON h.PatientId = ipv.PatientId 
								AND h.ServiceDateTime = ipv.ServiceDateTime  AND h.SourceSystemId = ipv.SourceSystemId  AND ISNULL(h.EncounterIdentifier,'') =  ISNULL(ipv.EncounterIdentifier,'')
									
						LEFT OUTER JOIN
								(SELECT v2.PatientId,v2.ServiceDateTime,v2.WeightKG, v2.WeightComments, v2.SourceSystemId ,  v2.EncounterIdentifier, 
									ROW_NUMBER() OVER (PARTITION BY v2.PatientId, v2.ServiceDateTime, v2.SourceSystemId, ISNULL(EncounterIdentifier,'') ORDER BY v2.InterfacePatientVitalsId DESC) rowNumber
									FROM #PatientVitalsQue v2 WITH (NOLOCK) WHERE v2.WeightKG IS NOT NULL AND v2.WeightKG <> 0.00  ) w ON w.PatientId = ipv.PatientId 
									AND w.ServiceDateTime = ipv.ServiceDateTime AND w.SourceSystemId = ipv.SourceSystemId AND ISNULL( w.EncounterIdentifier,'') =  ISNULL(ipv.EncounterIdentifier,'')
									
						LEFT OUTER JOIN
								(SELECT v3.PatientId,v3.ServiceDateTime,v3.TemperatureCelcius,v3.TemperatureComments, v3.SourceSystemId,  v3.EncounterIdentifier, 
								ROW_NUMBER() OVER (PARTITION BY v3.PatientId,v3.ServiceDateTime, v3.SourceSystemId,ISNULL(EncounterIdentifier,'') ORDER BY v3.InterfacePatientVitalsId DESC) 
								rowNumber FROM #PatientVitalsQue v3 WITH (NOLOCK)  WHERE v3.TemperatureCelcius IS NOT NULL and v3.TemperatureCelcius <> 0.00 ) t ON t.PatientId = ipv.PatientId
								 AND t.ServiceDateTime = ipv.ServiceDateTime  AND t.SourceSystemId = ipv.SourceSystemId  AND ISNULL( t.EncounterIdentifier,'') = ISNULL( ipv.EncounterIdentifier,'')
								 	
						LEFT OUTER JOIN
								(SELECT v4.PatientId, v4.ServiceDateTime, v4.Pulse, v4.PulseComments, v4.SourceSystemId, v4.EncounterIdentifier,   ROW_NUMBER() OVER
								(PARTITION BY v4.PatientId,v4.ServiceDateTime,v4.SourceSystemId,ISNULL(EncounterIdentifier,'') ORDER BY v4.InterfacePatientVitalsId DESC) rowNumber 
								FROM #PatientVitalsQue v4 WITH (NOLOCK) WHERE v4.Pulse IS NOT NULL  AND v4.Pulse <> 0) p ON p.PatientId = ipv.PatientId 
								AND p.ServiceDateTime = ipv.ServiceDateTime AND p.SourceSystemId = ipv.SourceSystemId AND ISNULL( p.EncounterIdentifier,'') = ISNULL(ipv.EncounterIdentifier,'')
								
						LEFT OUTER JOIN
								(SELECT v5.PatientId,v5.ServiceDateTime,v5.Respiration,v5.RespirationComments, v5.SourceSystemId,  v5.EncounterIdentifier, 
								ROW_NUMBER() OVER (PARTITION BY v5.PatientId, v5.ServiceDateTime, v5.SourceSystemId,ISNULL(EncounterIdentifier,'') ORDER BY v5.InterfacePatientVitalsId DESC)
								rowNumber FROM #PatientVitalsQue v5 WITH (NOLOCK) WHERE v5.Respiration IS NOT NULL and v5.Respiration <> 0 ) r 
								ON r.PatientId = ipv.PatientId 
								AND r.ServiceDateTime = ipv.ServiceDateTime  AND r.SourceSystemId = ipv.SourceSystemId  AND ISNULL(r.EncounterIdentifier,'') = ISNULL(ipv.EncounterIdentifier,'')
								
						LEFT OUTER JOIN
								(SELECT v6.PatientId,v6.ServiceDateTime,v6.BloodPressureSystolic,v6.BloodPressureComment, v6.SourceSystemId,  v6.EncounterIdentifier, 
								ROW_NUMBER() OVER (PARTITION BY v6.PatientId, v6.ServiceDateTime, v6.SourceSystemId, ISNULL(EncounterIdentifier,'')
								ORDER BY v6.InterfacePatientVitalsId DESC) rowNumber FROM #PatientVitalsQue v6 WITH (NOLOCK) WHERE v6.BloodPressureSystolic IS NOT NULL  and v6.BloodPressureSystolic <> 0 )
								bps ON bps.PatientId = ipv.PatientId AND bps.ServiceDateTime = ipv.ServiceDateTime
								AND bps.SourceSystemId = ipv.SourceSystemId  AND ISNULL(bps.EncounterIdentifier,'') = ISNULL(ipv.EncounterIdentifier ,'')
						LEFT OUTER JOIN 
								(SELECT v7.PatientId,v7.ServiceDateTime,v7.BloodPressureDiastolic,v7.BloodPressureComment, v7.SourceSystemId,  v7.EncounterIdentifier, 
								ROW_NUMBER() OVER (PARTITION BY v7.PatientId, v7.ServiceDateTime, v7.SourceSystemId, ISNULL(EncounterIdentifier,'') ORDER BY v7.InterfacePatientVitalsId DESC) 
								rowNumber FROM #PatientVitalsQue v7 WITH (NOLOCK) WHERE v7.BloodPressureDiastolic IS NOT NULL and v7.BloodPressureDiastolic <> 0  )
								bpd ON bpd.PatientId = ipv.PatientId AND bpd.ServiceDateTime = ipv.ServiceDateTime
								  AND bpd.SourceSystemId = ipv.SourceSystemId  AND ISNULL( bpd.EncounterIdentifier, '') = ISNULL(ipv.EncounterIdentifier,'')
						LEFT OUTER JOIN
								(SELECT v8.PatientId,v8.ServiceDateTime,v8.OxygenSaturationSP02,v8.OxygenSaturationComment, v8.SourceSystemId ,  v8.EncounterIdentifier, 
								ROW_NUMBER() OVER (PARTITION BY v8.PatientId, v8.ServiceDateTime, v8.SourceSystemId, ISNULL(EncounterIdentifier,'') ORDER BY v8.InterfacePatientVitalsId DESC)
								rowNumber FROM #PatientVitalsQue v8 WITH (NOLOCK) WHERE v8.OxygenSaturationSP02 IS NOT NULL ) o 
								ON o.PatientId = ipv.PatientId AND o.ServiceDateTime = ipv.ServiceDateTime
								  AND o.SourceSystemId = ipv.SourceSystemId  AND ISNULL(o.EncounterIdentifier,'') = ISNULL(ipv.EncounterIdentifier,'')
						LEFT OUTER JOIN 
								(SELECT v9.PatientId,v9.ServiceDateTime,v9.Clinician, v9.SourceSystemId,  v9.EncounterIdentifier,  ROW_NUMBER() OVER
								(PARTITION BY v9.PatientId, v9.ServiceDateTime, v9.SourceSystemId, ISNULL(EncounterIdentifier,'') ORDER BY v9.InterfacePatientVitalsId DESC) rowNumber 
								FROM #PatientVitalsQue v9 WITH (NOLOCK) WHERE v9.Clinician IS NOT NULL ) c 
								ON c.PatientId = ipv.PatientId AND c.ServiceDateTime = ipv.ServiceDateTime
								 AND c.SourceSystemId = ipv.SourceSystemId AND ISNULL(c.EncounterIdentifier,'') =  ISNULL(ipv.EncounterIdentifier,'')
						LEFT OUTER JOIN
								(SELECT v10.PatientId,v10.ServiceDateTime,v10.PatientVitalsComment, v10.SourceSystemId,  v10.EncounterIdentifier,  ROW_NUMBER() OVER 
								(PARTITION BY v10.PatientId, v10.ServiceDateTime, v10.SourceSystemId, ISNULL(EncounterIdentifier,'') ORDER BY v10.InterfacePatientVitalsId DESC) rowNumber 
								FROM #PatientVitalsQue v10 WITH (NOLOCK) WHERE v10.PatientVitalsComment IS NOT NULL ) 
								vc ON vc.PatientId = ipv.PatientId AND vc.ServiceDateTime = ipv.ServiceDateTime
								 AND vc.SourceSystemId = ipv.SourceSystemId  AND ISNULL( vc.EncounterIdentifier,'') = ISNULL(ipv.EncounterIdentifier,'')
						LEFT OUTER JOIN
								(SELECT v11.PatientId,v11.ServiceDateTime,v11.EncounterIdentifier, v11.SourceSystemId,    ROW_NUMBER() OVER 
								(PARTITION BY v11.PatientId, v11.ServiceDateTime, v11.SourceSystemId, ISNULL(EncounterIdentifier,'') ORDER BY v11.InterfacePatientVitalsId DESC) 
								rowNumber FROM #PatientVitalsQue v11 WITH (NOLOCK) WHERE v11.EncounterIdentifier IS NOT NULL ) e 
								ON e.PatientId = ipv.PatientId AND e.ServiceDateTime = ipv.ServiceDateTime
								  AND e.SourceSystemId = ipv.SourceSystemId  AND ISNULL(e.EncounterIdentifier,'') = ISNULL(ipv.EncounterIdentifier, '')
						LEFT OUTER JOIN
								(SELECT v12.PatientId,v12.ServiceDateTime,v12.RenderingProviderNPI, v12.SourceSystemId,   v12.EncounterIdentifier, ROW_NUMBER() 
								OVER (PARTITION BY v12.PatientId,v12.ServiceDateTime, v12.SourceSystemId, ISNULL(EncounterIdentifier,'') ORDER BY v12.InterfacePatientVitalsId DESC) rowNumber 
								FROM #PatientVitalsQue v12 WITH (NOLOCK) WHERE v12.RenderingProviderNPI IS NOT NULL ) npi
								ON npi.PatientId = ipv.PatientId AND npi.ServiceDateTime = ipv.ServiceDateTime
								 AND npi.SourceSystemId = ipv.SourceSystemId  AND ISNULL(npi.EncounterIdentifier,'') = ISNULL(ipv.EncounterIdentifier,'')
						WHERE ipv.rowNumber = 1
								AND (h.rowNumber = 1 OR h.rowNumber IS NULL)
								AND (w.rowNumber = 1 OR w.rowNumber IS NULL)
								AND (t.rowNumber = 1 OR t.rowNumber IS NULL)
								AND (p.rowNumber = 1 OR p.rowNumber IS NULL)
								AND (r.rowNumber = 1 OR r.rowNumber IS NULL)
								AND (bps.rowNumber = 1 OR bps.rowNumber IS NULL)
								AND (bpd.rowNumber = 1 OR bpd.rowNumber IS NULL)
								AND (o.rowNumber = 1 OR o.rowNumber IS NULL)
								AND (c.rowNumber = 1 OR c.rowNumber IS NULL)
								AND (vc.rowNumber = 1 OR vc.rowNumber IS NULL)
								AND (e.rowNumber = 1 OR e.rowNumber IS NULL)
								AND (npi.rowNumber = 1 OR npi.rowNumber IS NULL)
							) A


						--add index 
   						Create  NonCLUSTERED INDEX IDX_PatientVitalsPatientID_SourceSystemId ON #PatientVitals (SourceSystemId,PatientID, ServiceDateTime, EncounterIdentifier);

						SET @RecordCountSource = @RecordCountSource + (SELECT COUNT(1) FROM #PatientVitals);	             


						--Merge in the records from the batch table
							MERGE [DBO].PatientVitals AS target
								USING #PatientVitals AS source 
						ON (target.PatientID = source.PatientID AND target.ServiceDateTime =  source.ServiceDateTime
							 and target.[SourceSystemID] = source.[SourceSystemID]  and ISNULL(target.[EncounterIdentifier],'') = ISNULL(source.[EncounterIdentifier],'') 
						)
						WHEN MATCHED  THEN UPDATE SET 
								[HeightCM]  = ISNULL(source.[HeightCM], target.[HeightCM]), 
								[WeightKG]  = ISNULL(source.[WeightKG], target.[WeightKG]), 
								[TemperatureCelcius]  = ISNULL(source.[TemperatureCelcius], target.[TemperatureCelcius]), 
								[Pulse]  = ISNULL(source.[Pulse], target.[Pulse]), 
								[Respiration]  = ISNULL(source.[Respiration], target.[Respiration]), 
								[BloodPressureSystolic]  = ISNULL(source.[BloodPressureSystolic], target.[BloodPressureSystolic]), 
								[BloodPressureDiastolic]  = ISNULL(source.[BloodPressureDiastolic], target.[BloodPressureDiastolic]), 
								[OxygenSaturationSP02]  = ISNULL(NULLIF(source.[OxygenSaturationSP02],''), target.[OxygenSaturationSP02]), 
								[HeightComments] = ISNULL(NULLIF(source.[HeightComments],''), target.[HeightComments]), 
								[WeightComments] = ISNULL(NULLIF(source.[WeightComments],''), target.[WeightComments]),
								[TemperatureComments] = ISNULL(NULLIF(source.[TemperatureComments],''), target.[TemperatureComments]),
								[PulseComments] = ISNULL(NULLIF(source.[PulseComments],''),  target.[PulseComments]),
								[RespirationComments] = ISNULL(NULLIF( source.[RespirationComments],''), target.[RespirationComments] ),
								[BloodPressureComment] = ISNULL(NULLIF(source.[BloodPressureComment],''), target.[BloodPressureComment] ),
								[OxygenSaturationComment] =  ISNULL(NULLIF(source.[OxygenSaturationComment],''),  target.[OxygenSaturationComment]),
								[Clinician] =  ISNULL(NULLIF(source.[Clinician],''), target.[Clinician]),
								[PatientVitalsComment] = ISNULL(NULLIF( source.[PatientVitalsComment],''), target.[PatientVitalsComment] ), 
								[DeleteInd] = 0, 
								[ModifyDateTime] = @Today , 
								[ModifyLBUserId] = 1,
								[EncounterIdentifier] = ISNULL(source.[EncounterIdentifier],target.[EncounterIdentifier]),
								[RenderingProviderNPI]= ISNULL(source.[RenderingProviderNPI], target.[RenderingProviderNPI])
						WHEN NOT MATCHED THEN
						INSERT ([PatientID] ,[SourceSystemId] ,[ServiceDateTime],[HeightCM] ,[HeightComments]
								,[WeightKG]  ,[WeightComments] ,[TemperatureCelcius] ,[TemperatureComments] ,[Pulse]
								,[PulseComments] ,[Respiration],[RespirationComments] ,[BloodPressureSystolic],[BloodPressureDiastolic]
								,[BloodPressureComment],[OxygenSaturationSP02] ,[OxygenSaturationComment]      ,[Clinician],[PatientVitalsComment]
								,[DeleteInd] ,[CreateDateTime] ,[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId],[EncounterIdentifier],[RenderingProviderNPI]
								)
						VALUES (source.[PatientID] ,source.[SourceSystemId] ,source.[ServiceDateTime],source.[HeightCM] ,source.[HeightComments]
								,source.[WeightKG]  ,source.[WeightComments] ,source.[TemperatureCelcius] ,source.[TemperatureComments] ,source.[Pulse]
								,source.[PulseComments] ,source.[Respiration],source.[RespirationComments] ,source.[BloodPressureSystolic],source.[BloodPressureDiastolic]
								,source.[BloodPressureComment],source.[OxygenSaturationSP02] ,source.[OxygenSaturationComment]      ,source.[Clinician],[PatientVitalsComment]
								,0 ,@Today,@Today,1,1,source.[EncounterIdentifier],source.[RenderingProviderNPI]);
						




						
   							--Update the max processed time and drop the temp tables
							SET @tmpmaxSourceTimeStampProcessed = 	(SELECT MAX(CreateDateTime) FROM #PatientVitalsQue )
							IF @tmpmaxSourceTimeStampProcessed > @maxSourceTimeStampProcessed
							BEGIN
							SET @maxSourceTimeStampProcessed = @tmpmaxSourceTimeStampProcessed
							END


						DROP TABLE #PatientVitals
						DROP TABLE #PatientVitalsQue

				END
			
			
			--Move onto the next batch
			DELETE FROM #PatientVitalsQuePatientIdentifier WHERE PatientIdentifier IN (SELECT PatientIdentifier FROM #PatientVitalsQuePatientIdentifieBATCH)
			DROP TABLE #PatientVitalsQuePatientIdentifieBATCH
			IF @NUMROWS = 0 BREAK;
  
		END
		

		DROP table #PatientVitalsQuePatientIdentifier
		-- update lastdate

					
               		   
		-- update maxSourceTimeStamp with max CreateDateTime of #PatientVitalsQue
		UPDATE [Maintenance]..[EDWTableLoadTracking]
		SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex, updatedatetime = GETUTCDATE()
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


		-- get total records from PatientVitals after merge 
		SELECT @RecordCountAfter=COUNT(1)  FROM PatientVitals WITH (NOLOCK);

		-- Calculate records inserted and updated counts
		SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
		SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	


	END TRY
	BEGIN CATCH
	        --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			SET  @EndTime=GETUTCDATE();
			SET  @ErrorNumber =ERROR_NUMBER();
			SET  @ErrorState =ERROR_STATE();
			SET  @ErrorSeverity=ERROR_SEVERITY();
			SET  @ErrorLine=ERROR_LINE();
			SET  @Comment =left(ERROR_MESSAGE(),400);
			EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
END


GO
