SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ETLPatientDiagnosisInterface]
	@fullLoadFlag bit = 0, 
	@BatchSize INT = 10000, 
	@InterfacePatientId VARCHAR(50) = NULL,
	@overrideMinIndex INT = NULL
AS
BEGIN	



  /*===============================================================
	CREATED BY: 	Lan Ma
	CREATED ON:		2016-06-23
	INITIAL VER:	2.0.1
	MODULE:			ETL
	DESCRIPTION:	Load Interface Patient Diagnosis Data into EDW
	PARAMETERS:		
		 @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
		 @fullLoadFlag =0 Merge date to the existing table

	
	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.0.1		2016-03-23		Lan Ma					Created
	2.0.1		2016-04-07 		Youping					add the procedure running status to dbo.EdwProcedureRunLog
														Notes: @fullLoadFlag have values of 1 or 0
	2.0.1		2016-05-09		Youping	    			add DiagnosisOnsetDate for the unique keys
	2.0.1		2016-05-20		Youping	    			break large join to two separate process to improve performance
	2.0.1		2016-05-23		Youping	    			fix: string function replace change data lenth to varchar(8000) 
	2.0.1		2016-05-24		Youping	    			fix: -- fix merger error due to duplication of NULL and '1900-01-01'
	2.0.1		2016-05-25		Youping	    			fix: -- Missmatch ICD9/10 name
	2.0.1		2016-07-19		Youping	    			add new field ChronicInd 
	2.1.2		2016-12-08		CJL			LBETL-634	Added compensation code for missing diagnosis date, use diagnosis onset date if it is null
	2.3.1       2017-03-15      YL			LBETL-1088  Fort Drum ICD9/ICD10 Descriptions
	2.3.1		2017-04-25		CJL						Rolled  up results to the latest non-null values
														 

=================================================================*/

 BEGIN TRY

    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientDiagnosis';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientDiagnosisInterface';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @RowCount INT =0;
	DECLARE @NumRows INT = 0;
 	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @defaultDateTime DATETIME = CAST('2999-01-01' AS DATETIME);





	IF NOT EXISTS ( SELECT 1 FROM Maintenance.dbo.EDWTableLoadTracking  WHERE EDWtableName = @EDWtableName AND dataSource = @dataSource AND EDWName = DB_NAME())
	BEGIN
		 EXEC Maintenance.dbo.EDWTableLoadTrackingInsert @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed
	END




		IF @fullLoadFlag = 1
		BEGIN
			UPDATE Maintenance.dbo.EDWTableLoadTracking 
				SET maxSourceTimeStampProcessed = CAST('1900-01-01' AS DATETIME), maxSourceIdProcessed= 0
				WHERE EDWtableName = @EDWtableName AND dataSource = @dataSource AND EDWName =@EDWName
		END

		 SET @lastDateTime = (SELECT COALESCE(maxSourceTimeStampProcessed,CAST('1900-01-01' AS DATETIME)) 
									FROM Maintenance.dbo.EDWTableLoadTracking 
									WHERE EDWtableName = @EDWtableName 
									AND dataSource =@dataSource AND EDWName =@EDWName);
	
	
		
		SET  @lastIndex = (SELECT maxSourceIdProcessed
									FROM Maintenance.dbo.EDWTableLoadTracking 
									WHERE EDWtableName = @EDWtableName 
									AND dataSource =@dataSource AND EDWName =@EDWName);

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex= (SELECT MIN(InterfacePatientDiagnosisId) FROM  vwPortal_InterfacePatientDiagnosis IPS WITH (NOLOCK) WHERE IPS.CreateDateTime >= @lastDateTime ) 
		END

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex = 0 
		END


		if @overrideMinIndex IS NOT NULL
		BEGIN
			SET @lastIndex = @overrideMinIndex
		END 

		SET @maxIndex = (SELECT MAX(InterfacePatientDiagnosisId) FROM  vwPortal_InterfacePatientDiagnosis IPS WITH (NOLOCK));
	
		



		
		--2) Get the full list of patient identifiers that we are going to be dealing with
		--We are going to breaking this up into batches along patient id lines.
		SELECT  DISTINCT 
			IP.PatientIdentifier, IP.InterfaceSystemId
		INTO #PatientDiagnosisQuePatientIdentifier	
		FROM dbo.vwPortal_InterfacePatientDiagnosis IPS with (nolock)
			INNER JOIN dbo.vwPortal_InterfacePatient IP with (nolock) ON IPS.InterfacePatientId = IP.InterfacePatientId
			INNER JOIN dbo.vwPortal_InterfaceSystem  ISS with (nolock) ON IP.InterfaceSystemId = ISS.InterfaceSystemId
		WHERE ( IPS.InterfacePatientDiagnosisId >= @lastIndex  AND IPS.InterfacePatientDiagnosisId <= @maxIndex )
		 AND ( @InterfacePatientId IS NULL OR (IPS.InterfacePatientId = @InterfacePatientId))
			

		--get all codingSystemName and ID into temp table
		SELECT
		DiagnosisCodingSystemTypeID, RefCodingSystemName
		INTO #DiagnosisCodingSystemType
		FROM(
			SELECT  DCST.DiagnosisCodingSystemTypeID, DCST.NAME As RefCodingSystemName
			FROM DiagnosisCodingSystemType DCST WITH (NOLOCK)
			UNION 
			SELECT 	DCST.DiagnosisCodingSystemTypeID,DCSTS.SynonymName As RefCodingSystemName
			FROM DiagnosisCodingSystemType DCST  WITH (NOLOCK)
			INNER JOIN DiagnosisCodingSystemTypeSynonyms DCSTS  WITH (NOLOCK) 
			ON DCST.DiagnosisCodingSystemTypeID = DCSTS.DiagnosisCodingSystemTypeID
			) t

      	  


		SELECT @RecordCountBefore=COUNT(1)	FROM dbo.PatientDiagnosis  WITH (NOLOCK);



			--3) Loop throught all of the batches to improve memory efficency
		WHILE 1 = 1
		BEGIN	 -- While loop



		
				SELECT TOP (@BatchSize) PatientIdentifier, InterfaceSystemId INTO #PatientDiagnosisQuePatientIdentifieBATCH FROM #PatientDiagnosisQuePatientIdentifier;
				Create  NONCLUSTERED INDEX IDX_PatientDiagnosisQuePatientIdentifieBATCH_PatientIdentifier ON #PatientDiagnosisQuePatientIdentifieBATCH (PatientIdentifier, InterfaceSystemId);
			
    			SET @NUMROWS = ( SELECT COUNT(1) FROM #PatientDiagnosisQuePatientIdentifieBATCH)

				IF @NUMROWS <> 0 
				BEGIN   -- if num rows
 


					---Load new patient Diagnosis into staging table incrementally based on the CreateDateTime in table interfacePatientDiagnosis
					---Exclude the records that don't have vaules for DiagnosisCodingSystemName, DiagnosisCode, DiagnosisDateTime
						TRUNCATE TABLE dbo.PatientDiagnosisProcessQueue
	
						INSERT INTO dbo.PatientDiagnosisProcessQueue
						(
						   InterfacePatientDiagnosisId
						  ,InterfacePatientID
						  ,lbPatientId
						  ,InterfaceSystemId
						  ,DiagnosisType
						  ,DiagnosisStatus
						  ,DiagnosisSeverity
						  ,DiagnosisClassification
						  ,DiagnosisPriority
						  ,DiagnosisConfidentialityInd
						  ,DiagnosisDateTime
						  ,DiagnosisOnsetDate
						  ,DiagnosisResolvedDate
						  ,DiagnosisComment
						  ,Clinician
						  ,IPD_CreateDateTime
						  ,InterfacePatientDiagnosisDiagnosisCodeId
						  ,DiagnosisCode
						  ,DiagnosisCodingSystemName
						  ,DiagnosisDescription
						  ,IPDDC_CreateDateTime
						  ,EncounterId
						  ,EncounterIdentifier
						  ,RenderingProviderNPI
						  ,ChronicInd  --new added
						)
						SELECT IPD.InterfacePatientDiagnosisId
						,IPD.InterfacePatientID
						,idRef.lbPatientId
						,ISS.SourceSystemId  as InterfaceSystemId --  IPD.InterfaceSystemId
						,ISNULL(LTRIM(RTRIM(IPD.DiagnosisType)), '') AS DiagnosisType
						,ISNULL(LTRIM(RTRIM(IPD.DiagnosisStatus)), '') AS DiagnosisStatus
						,ISNULL(LTRIM(RTRIM(IPD.DiagnosisSeverity)), '')  AS DiagnosisSeverity
						,ISNULL(LTRIM(RTRIM(IPD.DiagnosisClassification)), '') AS DiagnosisClassification 
						,ISNULL(LTRIM(RTRIM(IPD.DiagnosisPriority)), '') AS DiagnosisPriority
						,IPD.DiagnosisConfidentialityInd		
						,ISNULL(IPD.DiagnosisDateTime, DiagnosisOnsetDate) as DiagnosisDateTime
						,IPD.DiagnosisOnsetDate     ---ISNULL(IPD.DiagnosisOnsetDate,'1900-01-01') as  DiagnosisOnsetDate  -- fix merger error due to duplication of NULL and '1900-01-01'
						,IPD.DiagnosisResolvedDate		
						,IPD.DiagnosisComment
						,ISNULL( IPD.Clinician, '') AS Clinician
						,IPD.CreateDateTime
						,IPDDC.InterfacePatientDiagnosisDiagnosisCodeId
						,IPDDC.DiagnosisCode  --LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20)
						,LTRIM(RTRIM(IPDDC.DiagnosisCodingSystemName)) as DiagnosisCodingSystemName
						,IPDDC.DiagnosisDescription
						,IPDDC.CreateDateTime IPDDC_CreateDateTime
						, NULL as EncounterId
						,ISNULL( IPD.EncounterIdentifier, 0)
						,RenderingProviderNPI
						,IPD.ChronicInd
						FROM
							 dbo.vwPortal_InterfacePatientDiagnosis IPD WITH (NOLOCK),
							 dbo.vwPortal_InterfacePatientDiagnosisDiagnosisCode IPDDC WITH (NOLOCK),
							[dbo].[vwPortal_InterfaceSystem]  ISS WITH (NOLOCK),
							 [dbo].[PatientIdReference] idRef WITH (NOLOCK),
							          [dbo].[vwPortal_InterfacePatient] IP  with (nolock),
							 #PatientDiagnosisQuePatientIdentifieBATCH batch WITH (NOLOCK)
						WHERE (IPD.[InterfacePatientDiagnosisId] >= @lastIndex  AND IPD.[InterfacePatientDiagnosisId] <= @maxIndex) 
							AND IPDDC.DiagnosisCode IS NOT NULL AND LTRIM(RTRIM(IPDDC.DiagnosisCode)) <> ''
							AND IPDDC.DiagnosisCodingSystemName IS NOT NULL AND LTRIM(RTRIM(IPDDC.DiagnosisCodingSystemName)) <> ''
							--AND (DiagnosisDateTime IS NOT NULL OR DiagnosisOnsetDate IS NOT NULL)
							AND IPD.InterfacePatientDiagnosisId = IPDDC.InterfacePatientDiagnosisId
							AND IPD.InterfacePatientId = IP.InterfacePatientId
							AND IP.InterfaceSystemId = ISS.[InterfaceSystemId]
							AND IP.PatientIdentifier = batch.PatientIdentifier AND IP.InterfaceSystemId = batch.InterfaceSystemId
							AND idRef.SourceSystemId = ISS.SourceSystemId 
							AND idRef.ExternalId = batch.PatientIdentifier 
							AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
							AND idRef.DeleteInd =0				

					ALTER INDEX idx_Patient_Diagnosis ON PatientDiagnosisProcessQueue REBUILD; 





					--loading Diagnosis dimension tables

					EXEC dbo.ETLDiagnosisClassification
					EXEC dbo.ETLDiagnosisPriorityType
					EXEC dbo.ETLDiagnosisSeverityType
					EXEC dbo.ETLDiagnosisStatusType
					EXEC dbo.ETLDiagnosisType
					EXEC dbo.ETLDiagnosisCode


		

					
					--CREATE NONCLUSTERED INDEX NCIX_#patientDiagnosisQueue_DiagnosisCodingSystemName ON #patientDiagnosisQueue(DiagnosisCodingSystemName) 

	     
					--SELECT DISTINCT
					--DiagnosisCodingSystemTypeId,
					--LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20)  as DiagnosisCode,
					-- DiagnosisCodeID
					--INTO #DiagnosisCode 		
					--FROM dbo.DiagnosisCode
					--WHERE DeleteInd=0
					--CREATE CLUSTERED INDEX CIX_#DiagnosisCode_DiagnosisCodingSystemTypeId ON #DiagnosisCode(DiagnosisCodingSystemTypeID,DiagnosisCode)


					--SELECT * FROm #DiagnosisCode
	
					
					--CREATE NONCLUSTERED INDEX NCIX_#patientDiagnosisQueue_DiagnosisCode ON #patientDiagnosisQueue(DiagnosisCode) 


					   --drop table #patientDiagnosis
	   
						SELECT *
						INTO #patientDiagnosis
						FROM
						(
								SELECT
								 pd.lbPatientID
								,pd.InterfaceSystemId as SourceSystemId
								,t2.DiagnosisCodingSystemTypeID
								,t3.DiagnosisCodeID
								,ISNULL(t8.DiagnosisTypeID,0) AS DiagnosisTypeID
								,ISNULL(t7.DiagnosisStatusTypeID,0) AS DiagnosisStatusTypeID
								,ISNULL(t6.DiagnosisSeverityTypeID,0) AS DiagnosisSeverityTypeID
								,confidentiality.DiagnosisConfidentialityInd
								,ISNULL(t5.DiagnosisPriorityTypeId,0) AS DiagnosisPriorityTypeId
								,ISNULL(t4.DiagnosisClassificationTypeId,0) AS DiagnosisClassificationTypeId
								,pd.DiagnosisDateTime
								,pd.DiagnosisOnsetDate
								,resolved.DiagnosisResolvedDate
								,comment.DiagnosisComment
								,ISNULL(encId.EncounterID,0) AS EncounterID
								,encIdentifier.EncounterIdentifier
								,ISNULL(clinician.Clinician,'') AS Clinician
								,renderingProvId.RenderingProviderNPI
								,chronic.ChronicInd
								FROM 
									(SELECT   lbPatientId, InterfaceSystemId,  LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20) AS DiagnosisCode, DiagnosisCodingSystemName, Diagnosisdatetime, DiagnosisOnsetDate, 
									DiagnosisClassification, DiagnosisPriority, DiagnosisSeverity, DiagnosisStatus, DiagnosisType
									,ROW_NUMBER() OVER
									(	PARTITION BY
									   lbPatientId, InterfaceSystemId, LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20),
										DiagnosisCodingSystemName,  ISNULL(Diagnosisdatetime, @defaultDateTime),
										ISNULL( DiagnosisOnsetDate, @defaultDateTime)
										ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
									FROM PatientDiagnosisProcessQueue WITH (NOLOCK)
									) pd

								INNER JOIN #DiagnosisCodingSystemType t2 ON LTRIM(RTRIM(pd.DiagnosisCodingSystemName)) = LTRIM(RTRIM(t2.RefCodingSystemName))
								INNER JOIN DiagnosisCode t3  WITH (NOLOCK) ON   LEFT( LTRIM(RTRIM(REPLACE(t3.DiagnosisCode,'.',''))), 20) = pd.DiagnosisCode 	AND t2.DiagnosisCodingSystemTypeId = t3.DiagnosisCodingSystemTypeId 
								LEFT OUTER JOIN dbo.DiagnosisClassificationType t4 WITH (NOLOCK)  ON ISNULL(LTRIM(RTRIM(pd.DiagnosisClassification)),'') = ISNULL(LTRIM(RTRIM(t4.Name)),'')
								LEFT OUTER JOIN dbo.DiagnosisPriorityType t5 WITH (NOLOCK)   ON ISNULL(LTRIM(RTRIM(pd.DiagnosisPriority)),'') = ISNULL(LTRIM(RTRIM(t5.Name)),'')
								LEFT OUTER JOIN dbo.DiagnosisSeverityType t6 WITH (NOLOCK)   ON ISNULL(LTRIM(RTRIM(pd.DiagnosisSeverity)),'') = ISNULL(LTRIM(RTRIM(t6.Name)),'')
								LEFT OUTER JOIN dbo.DiagnosisStatusType t7 WITH (NOLOCK)   ON ISNULL(LTRIM(RTRIM(pd.DiagnosisStatus)),'') = ISNULL(LTRIM(RTRIM(t7.Name)),'')
								LEFT OUTER JOIN dbo.DiagnosisType t8 WITH (NOLOCK)   ON ISNULL(LTRIM(RTRIM(pd.DiagnosisType)),'') = ISNULL(LTRIM(RTRIM(t8.Name)),'')
								LEFT OUTER JOIN 

									(SELECT   
									lbPatientId, InterfaceSystemId,  
									LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20) AS DiagnosisCode, 
									DiagnosisCodingSystemName, Diagnosisdatetime, DiagnosisOnsetDate,
									DiagnosisConfidentialityInd,
									ROW_NUMBER() OVER
									(	PARTITION BY
									   lbPatientId, InterfaceSystemId, LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20),
										DiagnosisCodingSystemName, ISNULL(Diagnosisdatetime, @defaultDateTime),
										ISNULL( DiagnosisOnsetDate, @defaultDateTime)
										ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
									FROM PatientDiagnosisProcessQueue WITH (NOLOCK) WHERE  DiagnosisConfidentialityInd IS NOT NULL
									) confidentiality
									ON confidentiality.lbPatientId = pd.lbPatientId AND confidentiality.DiagnosisCode = pd.DiagnosisCode 
									AND  confidentiality.DiagnosisCodingSystemName = pd.DiagnosisCodingSystemName
									AND  ISNULL( confidentiality.Diagnosisdatetime ,@defaultDateTime) = ISNULL(pd.Diagnosisdatetime ,@defaultDateTime)
									AND  ISNULL( confidentiality.DiagnosisOnsetDate ,@defaultDateTime) = ISNULL(pd.DiagnosisOnsetDate ,@defaultDateTime)
									AND confidentiality.InterfaceSystemId = pd.InterfaceSystemId
 								LEFT OUTER JOIN 

									(SELECT   
									lbPatientId, InterfaceSystemId,  
									LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20) AS DiagnosisCode, 
									DiagnosisCodingSystemName, Diagnosisdatetime, DiagnosisOnsetDate,
									DiagnosisResolvedDate,
									ROW_NUMBER() OVER
									(	PARTITION BY
									   lbPatientId, InterfaceSystemId, LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20),
										DiagnosisCodingSystemName, ISNULL(Diagnosisdatetime, @defaultDateTime),
										ISNULL( DiagnosisOnsetDate, @defaultDateTime)
										ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
									FROM PatientDiagnosisProcessQueue WITH (NOLOCK) WHERE  DiagnosisResolvedDate IS NOT NULL
									) resolved
									ON resolved.lbPatientId = pd.lbPatientId AND resolved.DiagnosisCode = pd.DiagnosisCode 
									AND  resolved.DiagnosisCodingSystemName = pd.DiagnosisCodingSystemName
									AND  ISNULL( resolved.Diagnosisdatetime ,@defaultDateTime) = ISNULL(pd.Diagnosisdatetime ,@defaultDateTime)
									AND  ISNULL( resolved.DiagnosisOnsetDate ,@defaultDateTime) = ISNULL(pd.DiagnosisOnsetDate ,@defaultDateTime)
									AND resolved.InterfaceSystemId = pd.InterfaceSystemId

 								LEFT OUTER JOIN 

									(SELECT   
									lbPatientId, InterfaceSystemId,  
									LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20) AS DiagnosisCode, 
									DiagnosisCodingSystemName, Diagnosisdatetime, DiagnosisOnsetDate,
									DiagnosisComment,
									ROW_NUMBER() OVER
									(	PARTITION BY
									   lbPatientId, InterfaceSystemId, LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20),
										DiagnosisCodingSystemName, ISNULL(Diagnosisdatetime, @defaultDateTime),
										ISNULL( DiagnosisOnsetDate, @defaultDateTime)
										ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
									FROM PatientDiagnosisProcessQueue WITH (NOLOCK) WHERE  DiagnosisComment IS NOT NULL  AND DiagnosisComment <> ''
									) comment
									ON comment.lbPatientId = pd.lbPatientId AND comment.DiagnosisCode = pd.DiagnosisCode 
									AND  comment.DiagnosisCodingSystemName = pd.DiagnosisCodingSystemName
									AND  ISNULL( comment.Diagnosisdatetime ,@defaultDateTime) = ISNULL(pd.Diagnosisdatetime ,@defaultDateTime)
									AND  ISNULL( comment.DiagnosisOnsetDate ,@defaultDateTime) = ISNULL(pd.DiagnosisOnsetDate ,@defaultDateTime)
									AND comment.InterfaceSystemId = pd.InterfaceSystemId

 								LEFT OUTER JOIN 

									(SELECT   
									lbPatientId, InterfaceSystemId,  
									LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20) AS DiagnosisCode, 
									DiagnosisCodingSystemName, Diagnosisdatetime, DiagnosisOnsetDate,
									EncounterID,
									ROW_NUMBER() OVER
									(	PARTITION BY
									   lbPatientId, InterfaceSystemId, LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20),
										DiagnosisCodingSystemName, ISNULL(Diagnosisdatetime, @defaultDateTime),
										ISNULL( DiagnosisOnsetDate, @defaultDateTime)
										ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
									FROM PatientDiagnosisProcessQueue WITH (NOLOCK) WHERE  EncounterID IS NOT NULL 
									) encId
									ON encId.lbPatientId = pd.lbPatientId AND encId.DiagnosisCode = pd.DiagnosisCode 
									AND  encId.DiagnosisCodingSystemName = pd.DiagnosisCodingSystemName
									AND  ISNULL( encId.Diagnosisdatetime ,@defaultDateTime) = ISNULL(pd.Diagnosisdatetime ,@defaultDateTime)
									AND  ISNULL( encId.DiagnosisOnsetDate ,@defaultDateTime) = ISNULL(pd.DiagnosisOnsetDate ,@defaultDateTime)
									AND encId.InterfaceSystemId = pd.InterfaceSystemId


 								LEFT OUTER JOIN 

									(SELECT   
									lbPatientId, InterfaceSystemId,  
									LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20) AS DiagnosisCode, 
									DiagnosisCodingSystemName, Diagnosisdatetime, DiagnosisOnsetDate,
									EncounterIdentifier,
									ROW_NUMBER() OVER
									(	PARTITION BY
									   lbPatientId, InterfaceSystemId, LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20),
										DiagnosisCodingSystemName, ISNULL(Diagnosisdatetime, @defaultDateTime),
										ISNULL( DiagnosisOnsetDate, @defaultDateTime)
										ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
									FROM PatientDiagnosisProcessQueue WITH (NOLOCK) WHERE  EncounterIdentifier IS NOT NULL  AND EncounterIdentifier<> ''
									) encIdentifier
									ON encIdentifier.lbPatientId = pd.lbPatientId AND encIdentifier.DiagnosisCode = pd.DiagnosisCode 
									AND  encIdentifier.DiagnosisCodingSystemName = pd.DiagnosisCodingSystemName
									AND  ISNULL( encIdentifier.Diagnosisdatetime ,@defaultDateTime) = ISNULL(pd.Diagnosisdatetime ,@defaultDateTime)
									AND  ISNULL( encIdentifier.DiagnosisOnsetDate ,@defaultDateTime) = ISNULL(pd.DiagnosisOnsetDate ,@defaultDateTime)
									AND encIdentifier.InterfaceSystemId = pd.InterfaceSystemId

 								LEFT OUTER JOIN 

									(SELECT   
									lbPatientId, InterfaceSystemId,  
									LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20) AS DiagnosisCode, 
									DiagnosisCodingSystemName, Diagnosisdatetime, DiagnosisOnsetDate,
									Clinician,
									ROW_NUMBER() OVER
									(	PARTITION BY
									   lbPatientId, InterfaceSystemId, LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20),
										DiagnosisCodingSystemName, ISNULL(Diagnosisdatetime, @defaultDateTime),
										ISNULL( DiagnosisOnsetDate, @defaultDateTime)
										ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
									FROM PatientDiagnosisProcessQueue WITH (NOLOCK) WHERE  Clinician IS NOT NULL  AND Clinician<> ''
									) clinician
									ON clinician.lbPatientId = pd.lbPatientId AND clinician.DiagnosisCode = pd.DiagnosisCode 
									AND  clinician.DiagnosisCodingSystemName = pd.DiagnosisCodingSystemName
									AND  ISNULL( clinician.Diagnosisdatetime ,@defaultDateTime) = ISNULL(pd.Diagnosisdatetime ,@defaultDateTime)
									AND  ISNULL( clinician.DiagnosisOnsetDate ,@defaultDateTime) = ISNULL(pd.DiagnosisOnsetDate ,@defaultDateTime)
									AND clinician.InterfaceSystemId = pd.InterfaceSystemId
				

 								LEFT OUTER JOIN 

									(SELECT   
									lbPatientId, InterfaceSystemId,  
									LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20) AS DiagnosisCode, 
									DiagnosisCodingSystemName, Diagnosisdatetime, DiagnosisOnsetDate,
									RenderingProviderNPI,
									ROW_NUMBER() OVER
									(	PARTITION BY
									   lbPatientId, InterfaceSystemId, LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20),
										DiagnosisCodingSystemName, ISNULL(Diagnosisdatetime, @defaultDateTime),
										ISNULL( DiagnosisOnsetDate, @defaultDateTime)
										ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
									FROM PatientDiagnosisProcessQueue WITH (NOLOCK) WHERE  RenderingProviderNPI IS NOT NULL  AND RenderingProviderNPI<> ''
									) renderingProvId
									ON renderingProvId.lbPatientId = pd.lbPatientId AND renderingProvId.DiagnosisCode = pd.DiagnosisCode 
									AND  renderingProvId.DiagnosisCodingSystemName = pd.DiagnosisCodingSystemName
									AND  ISNULL( renderingProvId.Diagnosisdatetime ,@defaultDateTime) = ISNULL(pd.Diagnosisdatetime ,@defaultDateTime)
									AND  ISNULL( renderingProvId.DiagnosisOnsetDate ,@defaultDateTime) = ISNULL(pd.DiagnosisOnsetDate ,@defaultDateTime)
									AND renderingProvId.InterfaceSystemId = pd.InterfaceSystemId
 								LEFT OUTER JOIN 

									(SELECT   
									lbPatientId, InterfaceSystemId,  
									LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20) AS DiagnosisCode, 
									DiagnosisCodingSystemName, Diagnosisdatetime, DiagnosisOnsetDate,
									ChronicInd,
									ROW_NUMBER() OVER
									(	PARTITION BY
									   lbPatientId, InterfaceSystemId, LEFT( LTRIM(RTRIM(REPLACE(DiagnosisCode,'.',''))),20),
										DiagnosisCodingSystemName, ISNULL(Diagnosisdatetime, @defaultDateTime),
										ISNULL( DiagnosisOnsetDate, @defaultDateTime)
										ORDER BY InterfacePatientDiagnosisId DESC ) AS rowNbr
									FROM PatientDiagnosisProcessQueue WITH (NOLOCK) WHERE  ChronicInd IS NOT NULL  AND ChronicInd<> ''
									) chronic
									ON chronic.lbPatientId = pd.lbPatientId AND chronic.DiagnosisCode = pd.DiagnosisCode 
									AND  chronic.DiagnosisCodingSystemName = pd.DiagnosisCodingSystemName
									AND  ISNULL( chronic.Diagnosisdatetime ,@defaultDateTime) = ISNULL(pd.Diagnosisdatetime ,@defaultDateTime)
									AND  ISNULL( chronic.DiagnosisOnsetDate ,@defaultDateTime) = ISNULL(pd.DiagnosisOnsetDate ,@defaultDateTime)
									AND chronic.InterfaceSystemId = pd.InterfaceSystemId
		
								WHERE pd.rowNbr = 1
									AND ( confidentiality.rowNbr = 1 oR confidentiality.rowNbr IS NULL ) 
									AND ( resolved.rowNbr = 1 oR resolved.rowNbr IS NULL ) 
									AND ( comment.rowNbr = 1 oR comment.rowNbr IS NULL ) 
									AND ( encId.rowNbr = 1 oR encId.rowNbr IS NULL ) 
									AND ( encIdentifier.rowNbr = 1 oR encIdentifier.rowNbr IS NULL ) 
									AND ( clinician.rowNbr = 1 oR clinician.rowNbr IS NULL ) 
									AND ( renderingProvId.rowNbr = 1 oR renderingProvId.rowNbr IS NULL ) 
									AND ( chronic.rowNbr = 1 oR chronic.rowNbr IS NULL ) 
							 ) AS tmp
		


	
						

						CREATE CLUSTERED INDEX CIX_#patientDiagnosis_PatientIDDCID ON #patientDiagnosis(lbPatientID,SourceSystemID,DiagnosisCodeID,DiagnosisDateTime,DiagnosisOnsetDate)



							 MERGE PatientDiagnosis T		 
							 USING #patientDiagnosis S
							 ON T.PatientID = S.lbPatientID
									 AND T.SourceSystemID = S.SourceSystemID
									 AND T.DiagnosisCodeID = S.DiagnosisCodeID
									 AND isnull(T.DiagnosisDateTime,@defaultDateTime) =isnull(S.DiagnosisDateTime,@defaultDateTime)
									 AND isnull(T.DiagnosisOnsetDate,@defaultDateTime) =isnull(S.DiagnosisOnsetDate,@defaultDateTime)
							 WHEN MATCHED 
						THEN UPDATE SET   T.DiagnosisTypeID = ISNULL(S.DiagnosisTypeID,T.DiagnosisTypeID),
									T.DiagnosisStatusTypeID = ISNULL(S.DiagnosisStatusTypeID,T.DiagnosisStatusTypeID), 
									T.DiagnosisSeverityTypeID = ISNULL(S.DiagnosisSeverityTypeID,T.DiagnosisSeverityTypeID), 
									T.DiagnosisConfidentialityInd =ISNULL( S.DiagnosisConfidentialityInd,T.DiagnosisConfidentialityInd),
									T.DiagnosisPriorityTypeId = ISNULL(S.DiagnosisPriorityTypeId, T.DiagnosisPriorityTypeId), 
									T.DiagnosisClassificationTypeId = ISNULL(S.DiagnosisClassificationTypeId,T.DiagnosisClassificationTypeId), 
									T.DiagnosisDateTime = ISNULL(S.DiagnosisDateTime, T.DiagnosisDateTime) ,
									T.DiagnosisOnsetDate = ISNULL(S.DiagnosisOnsetDate, T.DiagnosisOnsetDate), 
									T.DiagnosisResolvedDate = ISNULL(S.DiagnosisResolvedDate, T.DiagnosisResolvedDate), 
									T.DiagnosisComment = ISNULL(S.DiagnosisComment,T.DiagnosisComment), 
									T.EncounterID = ISNULL(S.EncounterID, T.EncounterID), 
									T.EncounterIdentifier = ISNULL( S.EncounterIdentifier, T.EncounterIdentifier), 
									T.Clinician = ISNULL( S.Clinician, T.Clinician) , 
									T.RenderingProviderNPI = ISNULL( S.RenderingProviderNPI,  T.RenderingProviderNPI), 
									T.ActiveInd = 1,
									T.DeleteInd = 0,
									T.ModifyDateTime = GETUTCDATE(),
									T.ChronicInd= ISNULL(S.ChronicInd, T.ChronicInd)
							 WHEN NOT MATCHED BY TARGET
							   THEN INSERT ( PatientID,SourceSystemId,DiagnosisCodeID,DiagnosisCodingSystemTypeId,DiagnosisTypeID,DiagnosisStatusTypeID,DiagnosisSeverityTypeID,DiagnosisConfidentialityInd,DiagnosisPriorityTypeId,DiagnosisClassificationTypeId,
											 DiagnosisDateTime,DiagnosisOnsetDate,DiagnosisResolvedDate,DiagnosisComment, EncounterID,EncounterIdentifier,Clinician,RenderingProviderNPI,
											 ActiveInd,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,ChronicInd)
									Values(	S.lbPatientID,S.SourceSystemId,S.DiagnosisCodeID,S.DiagnosisCodingSystemTypeId, S.DiagnosisTypeID,S.DiagnosisStatusTypeID,S.DiagnosisSeverityTypeID,S.DiagnosisConfidentialityInd,S.DiagnosisPriorityTypeId,S.DiagnosisClassificationTypeId,
											 S.DiagnosisDateTime,S.DiagnosisOnsetDate,S.DiagnosisResolvedDate,S.DiagnosisComment, S.EncounterID,S.EncounterIdentifier,S.Clinician,S.RenderingProviderNPI,1,0,GETUTCDATE(),GETUTCDATE(),1,1,S.ChronicInd);

					 
								 MERGE dbo.PatientDiagnosisDiagnosisCode T
								 USING (SELECT PatientDiagnosisId, DiagnosisCodeId, DiagnosisCodingSystemTypeId 
										FROM patientDiagnosis
										WHERE DiagnosisCodeId IS NOT NULL AND DiagnosisCodingSystemTypeId IS NOT NULL) S
								 ON T.PatientDiagnosisId = S.PatientDiagnosisId
								 WHEN NOT MATCHED BY TARGET
								 THEN INSERT (PatientDiagnosisId,DiagnosisCodingSystemTypeId ,DiagnosisCodeId)
									  VALUES(S.PatientDiagnosisId,S.DiagnosisCodingSystemTypeId ,S.DiagnosisCodeId);
		   
		   
		   
		
						-- update lastdate
               		   
							--Update the max processed time and drop the temp tables
							--SET @tmpmaxSourceTimeStampProcessed = 	(SELECT MAX(CreateDateTime) FROM #PatientDiagnosis )
							--IF @tmpmaxSourceTimeStampProcessed > @maxSourceTimeStampProcessed
							--BEGIN
							--SET @maxSourceTimeStampProcessed = @tmpmaxSourceTimeStampProcessed
							--END

					  --clean
					  DROP TABLE  #PatientDiagnosis
	
 				
					END  -- If Num Records <> 0 
			
			
				--Move onto the next batch
				--DELETE FROM #PatientDiagnosisQuePatientIdentifier WHERE PatientIdentifier IN (SELECT PatientIdentifier FROM #PatientDiagnosisQuePatientIdentifieBATCH) OR PatientIdentifier IS NULL

				DELETE FROM #PatientDiagnosisQuePatientIdentifier FROM 
					#PatientDiagnosisQuePatientIdentifier id, #PatientDiagnosisQuePatientIdentifieBATCH batch
				WHERE id.PatientIdentifier = batch.PatientIdentifier AND id.interfacesystemid  = batch.interfacesystemid

  			    DELETE FROM #PatientDiagnosisQuePatientIdentifier WHERE PatientIdentifier IS NULL

				DROP TABLE #PatientDiagnosisQuePatientIdentifieBATCH
				IF @NUMROWS = 0 BREAK;
	
			END  -- While loop
		
			DROP TABLE #PatientDiagnosisQuePatientIdentifier, #DiagnosisCodingSystemType
		
			-- update maxSourceTimeStamp with max CreateDateTime of #PatientDiagnosisQue
			UPDATE Maintenance..EDWTableLoadTracking
			SET maxSourceTimeStampProcessed = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex
			WHERE EDWtableName = @EDWtableName AND dataSource =@dataSource  AND EDWName = @EDWName


			-- get total records from PatientDiagnosis after merge 
			SELECT @RecordCountAfter=COUNT(1)  FROM PatientDiagnosis WITH (NOLOCK);

			-- Calculate records inserted and updated counts
			SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
			SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

			-- insert log information to Maintenance.dbo.EdwProcedureRunLog
			SET  @EndTime=GETUTCDATE();
			EXEC Maintenance.dbo.EdwInsertProcedureRunLog @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
		
		

	END TRY
	BEGIN CATCH
	           --- insert error log information to Maintenance.dbo.EdwProcedureRunLog
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC Maintenance.dbo.EdwInsertProcedureRunLog @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
  END




GO
