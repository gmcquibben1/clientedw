SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[patientHealthOrgBuild]
AS
BEGIN

SELECT distinct
 pp.PatientID
,pho.HealthcareOrgID
,pho.SourceSystemID
INTO #patientHealthOrg
from vwPortal_PatientProvider pp 
INNER JOIN [dbo].[vwPortal_ProviderHealthcareOrg] pho
on pp.[ProviderID] = pho.[ProviderID]
WHERE pp.DeleteInd <>1 OR pho.DeleteInd <>1

INSERT INTO vwPortal_PatientHealthcareOrg
(PatientID
,HealthcareOrgID
,SourceSystemID
,ExternalReferenceIdentifier
,OtherReferenceIdentifier
,DeleteInd
,CreateDateTime
,ModifyDateTime
,ModifyLBUserId
,CreateLBUserId)
SELECT 
 tph.PatientID
,tph.HealthcareOrgID
,tph.SourceSystemID
,NULL
,NULL
,0
,GETUTCDATE()
,GETUTCDATE()
,1
,1	
from #patientHealthOrg tph
LEFT JOIN vwPortal_PatientHealthcareOrg pah 
ON tph.PatientID = pah.PatientID
AND tph.HealthcareOrgID =pah.HealthcareOrgID
WHERE pah.PatientID IS NULL

DROP TABLE #patientHealthOrg

END

GO
