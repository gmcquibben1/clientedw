SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwAppointment] 
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES FOR NULL COLUMNS
--UPDATE PatientAppointmentProcessQueue SET AppointmentDate_YYYYMMDD = '2000-01-01' WHERE AppointmentDate_YYYYMMDD IS NULL

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId,LbPatientID, ISNULL(AppointmentType,''), ISNULL([AppointmentDate],''), [BeginTime],[EndTime]   
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				InterfacePatientAppointmentID
				DESC 
			) SNO
	FROM PatientAppointmentProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientAppointmentProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientAppointmentID INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientAppointmentID,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientAppointmentID,PatientID
FROM 
(	
MERGE [DBO].PatientAppointment AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,NULL AS SourceAppointmentId,
			[AppointmentDate] AS [AppointmentDate],[BeginTime] AS [BeginTime],[EndTime] AS [EndTime],[DurationInMinutes] AS [Duration],
			[AppointmentResource],[AppointmentReason],[AppointmentTypeId],[AppointmentDetail],[ProviderNPI],0 AS  [CancelInd], 0 AS DeleteInd,
			GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId
		FROM [dbo].PatientAppointmentProcessQueue PPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  ([OrigPatientId],[PatientId],[SourceSystemID],[SourceAppointmentId],[AppointmentDate],[BeginTime],[EndTime],
		[Duration],[AppointmentResource],[AppointmentReason],[AppointmentTypeId],[AppointmentDetail],[ProviderNPI],
		[CancelInd],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
ON ( target.[SourceSystemID] = source.[SourceSystemID] AND target.PatientID = source.PatientID 
     AND ISNULL(target.AppointmentTypeID,0) = ISNULL(source.AppointmentTypeID,0) AND ISNULL(target.[AppointmentDate],'') = ISNULL(source.[AppointmentDate],'') 
	 AND ISNULL(target.[BeginTime],0) = ISNULL(source.[BeginTime],0)  AND ISNULL(target.[EndTime],0) = ISNULL(source.[EndTime],0)
	 )
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
--[PatientId]		= source.[PatientId],
[SourceSystemID]		= source.[SourceSystemID],
[SourceAppointmentId]		= source.[SourceAppointmentId],
[AppointmentDate]		= source.[AppointmentDate],
[BeginTime]		= source.[BeginTime],
[EndTime]		= source.[EndTime],
[Duration]		= source.[Duration],
[AppointmentResource]		= source.[AppointmentResource],
[AppointmentReason]		= source.[AppointmentReason],
[AppointmentTypeId]		= source.[AppointmentTypeId],
[AppointmentDetail]		= source.[AppointmentDetail],
[ProviderNPI]		= source.[ProviderNPI],
[CancelInd]		= source.[CancelInd],
[DeleteInd]		= source.[DeleteInd],
--[CreateDateTime]		= source.[CreateDateTime],
[ModifyDateTime]		= source.[ModifyDateTime],
--[CreateLBUserId]		= source.[CreateLBUserId],
[ModifyLBUserId]		= source.[ModifyLBUserId]

WHEN NOT MATCHED THEN
INSERT ([PatientId],[SourceSystemID],[SourceAppointmentId],[AppointmentDate],[BeginTime],[EndTime],
		[Duration],[AppointmentResource],[AppointmentReason],[AppointmentTypeId],[AppointmentDetail],[ProviderNPI],
		[CancelInd],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId]
	   )
VALUES (source.[PatientId],source.[SourceSystemID],source.[SourceAppointmentId],source.[AppointmentDate],source.[BeginTime],source.[EndTime],
		source.[Duration],source.[AppointmentResource],source.[AppointmentReason],source.[AppointmentTypeId],source.[AppointmentDetail],source.[ProviderNPI],
		source.[CancelInd],source.[DeleteInd],source.[CreateDateTime],source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientAppointmentID,INSERTED.PatientAppointmentID) PatientAppointmentID, source.PatientID
) x ;

IF OBJECT_ID('PatientAppointment_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientAppointment_PICT(PatientAppointmentID,PatientId,SwitchDirectionId)
	SELECT RT.PatientAppointmentID, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientAppointmentProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'Appointment',GetDate()
FROM PatientAppointmentProcessQueue
WHERE ProcessedInd = 1 

UPDATE A 
SET HoldingReleaseInd = 1 
FROM [dbo].[PatientAppointmentProcessQueue_HoldingBay] A 
INNER JOIN PatientAppointmentProcessQueue B ON A.InterfacePatientAppointmentID = B.InterfacePatientAppointmentID
WHERE B.ProcessedInd = 1 

--INSERT INTO[dbo].[PatientAppointmentProcessQueue_HoldingBay]
--([InterfacePatientAppointmentID],[InterfacePatientID],[InterfaceSystemId],[AppointmentDate],[BeginTime]
--,[EndTime],[DurationInMinutes],[AppointmentResource],[AppointmentReason],[AppointmentTypeId],[AppointmentType]
--,[AppointmentDetail],[ProviderNPI],[ProviderClinicianName],[LbPatientId]
--,[InterfacePatientAppointmentID_HoldingOverride],[InterfacePatientID_HoldingOverride],[InterfaceSystemId_HoldingOverride]
--,[AppointmentDate_HoldingOverride],[BeginTime_HoldingOverride],[EndTime_HoldingOverride]
--,[DurationInMinutes_HoldingOverride],[AppointmentResource_HoldingOverride],[AppointmentReason_HoldingOverride]
--,[AppointmentTypeId_HoldingOverride],[AppointmentType_HoldingOverride],[AppointmentDetail_HoldingOverride]
--,[ProviderNPI_HoldingOverride],[ProviderClinicianName_HoldingOverride],[LbPatientId_HoldingOverride]
--,[HoldingStartDate],[HoldingStartReason])
-- SELECT 
--	 A.[InterfacePatientAppointmentID],A.[InterfacePatientID],A.[InterfaceSystemId],A.[AppointmentDate],A.[BeginTime]
--    ,A.[EndTime],A.[DurationInMinutes],A.[AppointmentResource],A.[AppointmentReason],A.[AppointmentTypeId],A.[AppointmentType]
--    ,A.[AppointmentDetail],A.[ProviderNPI],A.[ProviderClinicianName],A.[LbPatientId]
--	,A.[InterfacePatientAppointmentID],A.[InterfacePatientID],A.[InterfaceSystemId],A.[AppointmentDate],A.[BeginTime]
--    ,A.[EndTime],A.[DurationInMinutes],A.[AppointmentResource],A.[AppointmentReason],A.[AppointmentTypeId],A.[AppointmentType]
--    ,A.[AppointmentDetail],A.[ProviderNPI],A.[ProviderClinicianName],A.[LbPatientId]
--    ,GetDate(),'Duplicacy'
--FROM PatientAppointmentProcessQueue  A 
--LEFT JOIN [dbo].[PatientAppointmentProcessQueue_HoldingBay] B ON A.InterfacePatientAppointmentID = B.InterfacePatientAppointmentID
--WHERE ISNULL(A.ProcessedInd,0) <> 1 AND B.InterfacePatientAppointmentID IS NULL 

END

GO
