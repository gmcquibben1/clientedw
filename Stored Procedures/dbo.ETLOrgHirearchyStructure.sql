SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLOrgHirearchyStructure] 
@rebuildAll bit = 0
AS
BEGIN
  /*===============================================================
	CREATED BY: 	Youping
	CREATED ON:		2016
	INITIAL VER:	2.0.1
	MODULE:			ETL
	DESCRIPTION:	This stored procedure will populate the org hierarchy structure table
	PARAMETERS:		
		 @rebuildAll  Setting this value to 1 will rebuild the entire table as opposed to only differential records

	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	
	2.1.2		2017-01-20		CJL			LBETL-531	Added trim statements to the id values, added rebuild all table
	2.2.1		2016-12-30	YL		LBETL-393 add procedure run log to OrgHierarchy related procedures
	2.2.1		2017-01-03	YL		LEFT(DisplayValue,100) replace with DisplayValue; Left(externalcode,50) with externalcode
	                                LEFT(name,100) with Name from [dbo].[vwPortal_BusinessUnit] 

=================================================================*/
	SET NOCOUNT ON

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
		DECLARE @EDWtableName VARCHAR(100) = 'orgHierarchyStructure';
		DECLARE @dataSource VARCHAR(20) = 'interface';
		DECLARE @ProcedureName VARCHAR(128) = 'ETLOrgHirearchyStructure';
		DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
		DECLARE @maxSourceIdProcessed INT = NULL;
		DECLARE @StartTime DATETIME = GETUTCDATE();
		DECLARE @EndTime DATETIME = NULL;
		DECLARE @InsertedRecord INT=0;
		DECLARE @UpdatedRecord INT=0;
		DECLARE @Comment VARCHAR(400) = '';
		DECLARE @ErrorNumber INT =0;
		DECLARE @ErrorState INT =0;
		DECLARE @ErrorSeverity INT=0;
		DECLARE @ErrorLine INT=0;
		DECLARE @RecordCountBefore INT=0;
		DECLARE @RecordCountAfter  INT=0;
		DECLARE @RecordCountSource INT=0;

		DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY


			--set @fullLoadFlag =1
	
			IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] =@EDWName)
			  BEGIN
			  
					 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed

			  END

			   		      -- get total records from PatientVitals after merge 
		       			SELECT @RecordCountBefore=COUNT(1)
			            FROM orgHierarchyStructure;


	IF  OBJECT_ID('orgHierarchyStaging') IS NULL 

	BEGIN

	CREATE TABLE orgHierarchyStaging
	(buId INT, 
	levelId INT,
	level1_buId INT, 
	level1_buCode VARCHAR(50),
	level1_buName VARCHAR(100),
	level1_buType VARCHAR(100),
	level2_buId INT, 
	level2_buCode VARCHAR(50),
	level2_buName VARCHAR(100),
	level2_buType VARCHAR(100),
	level3_buId INT, 
	level3_buCode VARCHAR(50),
	level3_buName VARCHAR(100),
	level3_buType VARCHAR(100),
	level4_buId INT, 
	level4_buCode VARCHAR(50),
	level4_buName VARCHAR(100),
	level4_buType VARCHAR(100),
	level5_buId INT, 
	level5_buCode VARCHAR(50),
	level5_buName VARCHAR(100),
	level5_buType VARCHAR(100),
	level6_buId INT, 
	level6_buCode VARCHAR(50),
	level6_buName VARCHAR(100),
	level6_buType VARCHAR(100),
	modified_datetime DATETIME
	)

	END 

	IF   OBJECT_ID('orgHierarchyStructure') IS NULL 

	BEGIN
	CREATE TABLE [dbo].[orgHierarchyStructure](
		[buId] [INT] NULL unique,
		[levelId] [INT] NULL,
		[level1_buId] [INT] NULL,
		[level1_buCode] [VARCHAR](50) NULL,
		[level1_buName] [VARCHAR](100) NULL,
		[level1_buType] [VARCHAR](100) NULL,
		[level2_buId] [INT] NULL,
		[level2_buCode] [VARCHAR](50) NULL,
		[level2_buName] [VARCHAR](100) NULL,
		[level2_buType] [VARCHAR](100) NULL,
		[level3_buId] [INT] NULL,
		[level3_buCode] [VARCHAR](50) NULL,
		[level3_buName] [VARCHAR](100) NULL,
		[level3_buType] [VARCHAR](100) NULL,
		[level4_buId] [INT] NULL,
		[level4_buCode] [VARCHAR](50) NULL,
		[level4_buName] [VARCHAR](100) NULL,
		[level4_buType] [VARCHAR](100) NULL,
		[level5_buId] [INT] NULL,
		[level5_buCode] [VARCHAR](50) NULL,
		[level5_buName] [VARCHAR](100) NULL,
		[level5_buType] [VARCHAR](100) NULL,
		[level6_buId] [INT] NULL,
		[level6_buCode] [VARCHAR](50) NULL,
		[level6_buName] [VARCHAR](100) NULL,
		[level6_buType] [VARCHAR](100) NULL,
		[modified_datetime] [DATETIME] NULL
	) ON [PRIMARY]

	END


	DECLARE @sourceMaxModifyTime DATETIME = (SELECT MAX([ModifyDateTime]) FROM
	(
	SELECT MAX([ModifyDateTime]) AS ModifyDateTime FROM [dbo].[vwPortal_BusinessUnit]
	UNION 
	SELECT MAX([ModifyDateTime])  AS ModifyDateTime FROM [dbo].[vwPortal_BusinessUnitType]
	) t)
	DECLARE @targetMaxModifyTime DATETIME = (SELECT COALESCE(MAX([modified_datetime]),CAST('1900-01-01' AS DATETIME)) FROM [dbo].[orghierarchyStructure])



	if @rebuildAll = 1
	BEGIN
		SET @targetMaxModifyTime =  CAST('1900-01-01' AS DATETIME);
	END


	IF @sourceMaxModifyTime > @targetMaxModifyTime

	BEGIN

	SELECT BU1.BusinessUnitId, BU1.ParentBusinessUnitId, BU1.ExternalCode,BU1.Name
	INTO #allBusinessUnits --  select * FROM #allBusinessUnits 
	FROM vwPortal_BusinessUnit BU1
	WHERE BU1.DeleteInd = 0

	;WITH CTE1(businessunitid, levelId, parentbusinessunitid, externalcode, name) AS 
	(
		SELECT businessunitid, 1 AS levelId, parentbusinessunitid, externalcode,name
		FROM #allBusinessUnits
		WHERE parentbusinessunitid IS NULL
		UNION ALL  
		SELECT bu.businessunitid, CTE1.levelId + 1, bu.parentbusinessunitid, bu.externalcode, bu.name  
		FROM #allBusinessUnits bu 
		INNER JOIN CTE1 ON bu.ParentBusinessUnitId = CTE1.businessunitid 
		WHERE bu.parentbusinessunitid IS NOT NULL
	) 

	SELECT businessunitid, levelId, parentbusinessunitid, externalcode, CTE1.name
	INTO #bulist 
	FROM CTE1 

 
	-- get total records from PatientVitals after merge 
	SELECT @RecordCountSource=COUNT(1)
	FROM #bulist;

 	DECLARE @buType0 VARCHAR(100) = (select LEFT(DisplayValue,100) from vwPortal_BusinessUnitType where HierarchyLevelNumber= 0 AND [DeleteInd] = 0)
	DECLARE @buType1 VARCHAR(100) = (select LEFT(DisplayValue,100) from vwPortal_BusinessUnitType where HierarchyLevelNumber= 1 AND [DeleteInd] = 0)
	DECLARE @buType2 VARCHAR(100) = (select LEFT(DisplayValue,100) from vwPortal_BusinessUnitType where HierarchyLevelNumber= 2 AND [DeleteInd] = 0)
	DECLARE @buType3 VARCHAR(100) = (select LEFT(DisplayValue,100) from vwPortal_BusinessUnitType where HierarchyLevelNumber= 3 AND [DeleteInd] = 0)
	DECLARE @buType4 VARCHAR(100) = (select LEFT(DisplayValue,100) from vwPortal_BusinessUnitType where HierarchyLevelNumber= 4 AND [DeleteInd] = 0)
	DECLARE @buType5 VARCHAR(100) = (select LEFT(DisplayValue,100) from vwPortal_BusinessUnitType where HierarchyLevelNumber= 5 AND [DeleteInd] = 0)
	DECLARE @buType6 VARCHAR(100) = (select LEFT(DisplayValue,100) from vwPortal_BusinessUnitType where HierarchyLevelNumber= 6 AND [DeleteInd] = 0)

	TRUNCATE TABLE orgHierarchyStaging

	INSERT INTO orgHierarchyStaging
	select
	L1.BusinessUnitId,
	L1.levelId,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM( L1.name)), 100),  @buType1, 
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50),LEFT(LTRIM(RTRIM(L1.name)), 100),  @buType2,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM(L1.name)), 100),  @buType3,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM(L1.name)), 100),  @buType4,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM(L1.name)), 100),  @buType5,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM(L1.name)), 100),  @buType6,
	getdate()
	from #bulist L1
	where L1.levelId = 1

	INSERT INTO orgHierarchyStaging
	select
	L2.BusinessUnitId,
	L2.levelId,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM( L1.name)), 100),  @buType1, 
	L2.BusinessUnitId, LEFT(LTRIM(RTRIM(L2.externalcode)), 50),LEFT(LTRIM(RTRIM(L2.name)), 100),  @buType2,
	L2.BusinessUnitId, LEFT(LTRIM(RTRIM(L2.externalcode)), 50), LEFT(LTRIM(RTRIM(L2.name)), 100),  @buType3,
	L2.BusinessUnitId, LEFT(LTRIM(RTRIM(L2.externalcode)), 50), LEFT(LTRIM(RTRIM(L2.name)), 100),  @buType4,
	L2.BusinessUnitId, LEFT(LTRIM(RTRIM(L2.externalcode)), 50), LEFT(LTRIM(RTRIM(L2.name)), 100),  @buType5,
	L2.BusinessUnitId, LEFT(LTRIM(RTRIM(L2.externalcode)), 50), LEFT(LTRIM(RTRIM(L2.name)), 100),  @buType6,
	@sourceMaxModifyTime
	from #bulist L2
	join vwPortal_BusinessUnit L1
	on L2.parentbusinessunitid = L1.BusinessUnitId
	where L2.levelId = 2


	INSERT INTO orgHierarchyStaging
	select
	L3.BusinessUnitId,
	L3.levelId,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM( L1.name)), 100),  @buType1, 
	L2.BusinessUnitId, LEFT(LTRIM(RTRIM(L2.externalcode)), 50),LEFT(LTRIM(RTRIM(L2.name)), 100),  @buType2,
	L3.BusinessUnitId, LEFT(LTRIM(RTRIM(L3.externalcode)), 50), LEFT(LTRIM(RTRIM(L3.name)), 100),  @buType3,
	L3.BusinessUnitId, LEFT(LTRIM(RTRIM(L3.externalcode)), 50), LEFT(LTRIM(RTRIM(L3.name)), 100),  @buType4,
	L3.BusinessUnitId, LEFT(LTRIM(RTRIM(L3.externalcode)), 50), LEFT(LTRIM(RTRIM(L3.name)), 100),  @buType5,
	L3.BusinessUnitId, LEFT(LTRIM(RTRIM(L3.externalcode)), 50), LEFT(LTRIM(RTRIM(L3.name)), 100),  @buType6,
	@sourceMaxModifyTime
	from #bulist L3
	join vwPortal_BusinessUnit L2
	on L3.parentbusinessunitid = L2.BusinessUnitId
	join vwPortal_BusinessUnit L1
	on L2.parentbusinessunitid = L1.BusinessUnitId
	where L3.levelId = 3 


	INSERT INTO orgHierarchyStaging
	select
	L4.BusinessUnitId,
	L4.levelId,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM( L1.name)), 100),  @buType1, 
	L2.BusinessUnitId, LEFT(LTRIM(RTRIM(L2.externalcode)), 50),LEFT(LTRIM(RTRIM(L2.name)), 100),  @buType2,
	L3.BusinessUnitId, LEFT(LTRIM(RTRIM(L3.externalcode)), 50), LEFT(LTRIM(RTRIM(L3.name)), 100),  @buType3,
	L4.BusinessUnitId, LEFT(LTRIM(RTRIM(L4.externalcode)), 50), LEFT(LTRIM(RTRIM(L4.name)), 100),  @buType4,
	L4.BusinessUnitId, LEFT(LTRIM(RTRIM(L4.externalcode)), 50), LEFT(LTRIM(RTRIM(L4.name)), 100),  @buType5,
	L4.BusinessUnitId, LEFT(LTRIM(RTRIM(L4.externalcode)), 50), LEFT(LTRIM(RTRIM(L4.name)), 100),  @buType6,
	@sourceMaxModifyTime
	from #bulist L4
	join vwPortal_BusinessUnit L3
	on L4.parentbusinessunitid = L3.BusinessUnitId
	join vwPortal_BusinessUnit L2
	on L3.parentbusinessunitid = L2.BusinessUnitId
	join vwPortal_BusinessUnit L1
	on L2.parentbusinessunitid = L1.BusinessUnitId
	where L4.levelId = 4

	INSERT INTO orgHierarchyStaging
	select
	L5.BusinessUnitId,
	L5.levelId,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM( L1.name)), 100),  @buType1, 
	L2.BusinessUnitId, LEFT(LTRIM(RTRIM(L2.externalcode)), 50),LEFT(LTRIM(RTRIM(L2.name)), 100),  @buType2,
	L3.BusinessUnitId, LEFT(LTRIM(RTRIM(L3.externalcode)), 50), LEFT(LTRIM(RTRIM(L3.name)), 100),  @buType3,
	L4.BusinessUnitId, LEFT(LTRIM(RTRIM(L4.externalcode)), 50), LEFT(LTRIM(RTRIM(L4.name)), 100),  @buType4,
	L5.BusinessUnitId, LEFT(LTRIM(RTRIM(L5.externalcode)), 50), LEFT(LTRIM(RTRIM(L5.name)), 100),  @buType5,
	L5.BusinessUnitId, LEFT(LTRIM(RTRIM(L5.externalcode)), 50), LEFT(LTRIM(RTRIM(L5.name)), 100),  @buType6,
	@sourceMaxModifyTime
	from #bulist L5
	join vwPortal_BusinessUnit L4
	on L5.parentbusinessunitid = L4.BusinessUnitId
	join vwPortal_BusinessUnit L3
	on L4.parentbusinessunitid = L3.BusinessUnitId
	join vwPortal_BusinessUnit L2
	on L3.parentbusinessunitid = L2.BusinessUnitId
	join vwPortal_BusinessUnit L1
	on L2.parentbusinessunitid = L1.BusinessUnitId
	where L5.levelId = 5

	INSERT INTO orgHierarchyStaging
	select
	L6.BusinessUnitId,
	L6.levelId,
	L1.BusinessUnitId, LEFT(LTRIM(RTRIM(L1.externalcode)), 50), LEFT(LTRIM(RTRIM( L1.name)), 100),  @buType1, 
	L2.BusinessUnitId, LEFT(LTRIM(RTRIM(L2.externalcode)), 50),LEFT(LTRIM(RTRIM(L2.name)), 100),  @buType2,
	L3.BusinessUnitId, LEFT(LTRIM(RTRIM(L3.externalcode)), 50), LEFT(LTRIM(RTRIM(L3.name)), 100),  @buType3,
	L4.BusinessUnitId, LEFT(LTRIM(RTRIM(L4.externalcode)), 50), LEFT(LTRIM(RTRIM(L4.name)), 100),  @buType4,
	L5.BusinessUnitId, LEFT(LTRIM(RTRIM(L5.externalcode)), 50), LEFT(LTRIM(RTRIM(L5.name)), 100),  @buType5,
	L6.BusinessUnitId, LEFT(LTRIM(RTRIM(L6.externalcode)), 50), LEFT(LTRIM(RTRIM(L6.name)), 100),  @buType6,
	@sourceMaxModifyTime
	from #bulist L6
	join vwPortal_BusinessUnit L5
	on L6.parentbusinessunitid = L5.BusinessUnitId
	join vwPortal_BusinessUnit L4
	on L5.parentbusinessunitid = L4.BusinessUnitId
	join vwPortal_BusinessUnit L3
	on L4.parentbusinessunitid = L3.BusinessUnitId
	join vwPortal_BusinessUnit L2
	on L3.parentbusinessunitid = L2.BusinessUnitId
	join vwPortal_BusinessUnit L1
	on L2.parentbusinessunitid = L1.BusinessUnitId
	where L6.levelId = 6


	DROP TABLE #allBusinessUnits
	DROP TABLE #bulist

	BEGIN Tran
	TRUNCATE TABLE orgHierarchyStructure

	INSERT INTO orgHierarchyStructure SELECT * FROm orgHierarchyStaging
	COMMIT

	END

	
		    -- update lastdate
               		   
			 -- update maxSourceTimeStamp with max CreateDateTime of #PatientVitalsQue
			 UPDATE [Maintenance]..[EDWTableLoadTracking]
			 SET [maxSourceTimeStampProcessed] = @sourceMaxModifyTime,
				 [updateDateTime] =GETUTCDATE()
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


			 		      -- get total records from PatientVitals after merge 
		       			SELECT @RecordCountAfter=COUNT(1)
			            FROM orgHierarchyStructure;

              -- Calculate records inserted and updated counts
	             SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	             SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			   SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	

END TRY

		BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
END

GO
