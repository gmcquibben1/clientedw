SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLPatientEventInterface](@fullLoadFlag bit = 0)
AS
BEGIN	
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientEvent';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientEventInterface';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY
		-- Retrieve the last time the SP ran and set the boundary variables
		IF NOT EXISTS ( SELECT top 1 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] = @EDWName)
		BEGIN
			EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed
		END

		IF @fullLoadFlag = 1
		BEGIN
			UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
			SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME)
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] = @EDWName
		END

		DECLARE @lastDateTime DATETIME = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
										  FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
										  WHERE [EDWtableName] = @EDWtableName 
											AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	
		DECLARE @maxSourceDateTime DATETIME = (SELECT MAX([CreateDateTime]) FROM [dbo].[vwPortal_InterfacePatientEvent]);

		SELECT @RecordCountBefore=count(1) FROM [dbo].[PatientEvent];

		--if there are any rows in the interface newer than the last time this ran bring them in
        IF @maxSourceDateTime > @lastDateTime
		BEGIN

			--create PatientEvent for admit
			MERGE INTO PatientEvent AS target
			USING (SELECT
					idRef.LbPatientId,
					ifs.SourceSystemid,
					pet.PatientEventTypeId,
					ipe.EventDateTime AS EventDateTime,
					ipe.Comment AS EventDetail
				   FROM vwPortal_InterfacePatientEvent ipe
						INNER JOIN vwPortal_InterfacePatient ip ON ip.InterfacePatientId = ipe.InterfacePatientId
						INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ipe.InterfaceSystemId
						INNER JOIN PatientIdReference idRef ON idRef.ExternalId = ip.PatientIdentifier AND idRef.SourceSystemId = ifs.SourceSystemId
						INNER JOIN PatientEventType pet ON pet.Name = ipe.PatientEvent
					WHERE ipe.CreateDateTime > @lastDateTime) AS source
			ON source.LbPatientId = target.LbPatientId 
				AND source.PatientEventTypeId = target.PatientEventTypeId
				AND source.EventDateTime = target.EventDateTime
			WHEN NOT MATCHED BY target THEN
				INSERT (LBPatientId,SourceSystemId,PatientEventTypeId,EventDateTime,EventDetail,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
				VALUES (source.LBPatientId,source.SourceSystemid,source.PatientEventTypeId,source.EventDateTime,source.EventDetail,0,GETUTCDATE(),GETUTCDATE(),1,1);
	
			
		END

		-- Record the execution stats --------------------------------------------------------
		SELECT @RecordCountAfter=count(1) FROM [dbo].[PatientEvent];

        -- Calculate records inserted and updated counts
	    SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	    SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	    -- update maxSourceTimeStamp with max LR_CreateDateTime of target table
		UPDATE [Maintenance].dbo.[EDWTableLoadTracking]
			SET [maxSourceTimeStampProcessed] = (SELECT MAX(convert(DATE,[CreateDateTime])) FROM [dbo].[PatientEvent] ),
					 [updateDateTime] = GETUTCDATE()
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
