SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalFinancialScripts] @ER_POS23 bit = 0
AS
BEGIN
	exec internalSpecialtyCodeClaimLookup --before they start, look up specialties
	select 'internalSpecialtyCodeClaimLookup'
	exec internalMakeReadmissionStats
	select 'internalMakeReadmissionStats'
	exec internalMakeMemberMonths					--run 0 - internalMakeMemberMonths
	select 'internalMakeMemberMonths'
	exec internalMakeMemMonthMetrics				--run 1 - internalMakeMemMonthMetrics
	select 'internalMakeMemMonthMetrics'
	exec internalCrossTabPivotTableMMMetrics		--run 2 - internalCrossTabPivotTableMMMetrics
	select 'internalCrossTabPivotTableMMMetrics'
	--exec internalCrossTabPivotTableMMMetricsUAM	--run 2 - internalCrossTabPivotTableMMMetricsUAM
	--exec internalMakeFinancialSummaryData			--run 3 - internalMakeFinancialSummaryData
	--select 'internalMakeFinancialSummaryData'
	--exec internalMkCclfClmSummaryACO				--run 4 - internalMkCclfClmSummaryACO
	--select 'internalMkCclfClmSummaryACO'
	exec internalPatientSummaryMetrics				--run 6 - internalPatientSummaryMetrics
	select 'internalPatientSummaryMetrics'
	exec internalGetExternalNPIData					--run 8 - internalGetExternalNPIData
	select 'internalGetExternalNPIData'
	exec internalPhysicianSummaryMetrics			--run 9 - internalPhysicianSummaryMetrics
	select 'internalPhysicianSummaryMetrics'
	exec internalMakeCCLFWellness --run 10 Wellness Visits
	select 'internalMakeCCLFWellness'
END
 
GO
