SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[uspDedupeCCLF5]
AS

-- ===========================================================================
-- Source Code Location:
--     BitBucket /...?
--
-- EDW Base
-- ---------------------------------------------------------------------------
--
-- Object Type:  SQL Server Stored Procedure
--
--
-- Name:  [dbo].[uspDedupeCCLF5]
--
--
-- Description:
-- 		
-- 		Takes incoming data from the [dbo].[CCLF_5_PartB_Physicians_raw] table, deduplicates that data based
-- 		upon the [CUR_CLM_UNIQ_ID] and [CLM_LINE_NUM] column values (keeping the rows with the most recent
-- 		[fileDate]), and uses a MERGE statement to effect inserts or updates to the
-- 		[dbo].[CCLF_5_PartB_Physicians] table as appropriate.
-- 		
-- 		When complete, the procedure truncates the [dbo].[CCLF_5_PartB_Physicians_raw] table.
--
--
-- Input Variables:  None.
--
--
-- Output Variables:  None.
--
--
-- Calls:  None.
--
--
-- Return Status Values:
-- 		
-- 		    0 - Successful Completion.
-- 		51001 - Selection of most recent version of staging table rows FAILED.
-- 		51002 - Deleting rows with a bad SSNK from temp staging table FAILED.
-- 		51003 - Addition of PK to temp staging table FAILED.
-- 		51004 - Merge from indexed temp staging table source to target FAILED.
-- 		51005 - Commit of Merge transaction FAILED.
-- 		51006 - Delete of processed incoming source records FAILED.
-- 		51007 - Commit of Truncate transaction FAILED.
--
--
-- ---------------------------------------------------------------------------
-- Modification History:
--
-- Date        Modified by       Description
-- ---------------------------------------------------------------------------
-- ??/??/2014  ?????             Initial creation.
-- 11/14/2014  Bill Gibney       Rewritten to implement incremental data staging.
-- 09/17/2015  Bill Gibney       Removed dropped columns, added stats update on temp table.
-- ===========================================================================
--

-- ===========================================================================
--
--                           B E G I N   S C R I P T
--
-- ===========================================================================
--

-- ---------------------------------------------------------------------------
--
--                         S E S S I O N   S E T - U P
--
-- ---------------------------------------------------------------------------
--

-- Turn off row counts.
--
SET NOCOUNT ON

-- Reset Row Limit.
--
SET ROWCOUNT 0

-- Session settings.
--
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

-- ---------------------------------------------------------------------------
--
--                           D E C L A R A T I O N S
--
-- ---------------------------------------------------------------------------
--

-- For Error handling / output messages / return status.
--
DECLARE
	@Error INTEGER,
	@RowCount INTEGER,
	@Return INTEGER = 0,
	@ErrorNumber INTEGER = 0,
	@ErrorSeverity INTEGER = 0,
	@ErrorState INTEGER = 0,
	@ScriptName VARCHAR( 80 ) = QuoteName( OBJECT_SCHEMA_NAME( @@PROCID, DB_ID() ) ) + '.' + QuoteName( OBJECT_NAME( @@PROCID, DB_ID() ) ),
	@ErrorProcedure VARCHAR(200),
	@ErrorLine INTEGER = 0,
	@ErrorMessage VARCHAR(MAX)

-- Working variables.
--
DECLARE
	@SQL VARCHAR(1024)


SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Executing ' + @ScriptName + '...'
RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
--
BEGIN TRY
	-- ---------------------------------------------------------------------------
	--
	--                         I N I T I A L I Z A T I O N
	--
	-- ---------------------------------------------------------------------------
	--
	
	-- ---------------------------------------------------------------------------
	-- Initialize Script Variables
	-- ---------------------------------------------------------------------------
	-- n/a
	
	-- ---------------------------------------------------------------------------
	-- Validate Script Arguments
	-- ---------------------------------------------------------------------------
	--
	-- n/a
	
	
	
	-- ---------------------------------------------------------------------------
	-- Other Initializations
	-- ---------------------------------------------------------------------------
	--
	-- n/a
	
	
	
	-- ---------------------------------------------------------------------------
	--
	--                           E X E C U T I O N
	--
	-- ---------------------------------------------------------------------------
	--

	-- ---------------------------------------------------------------------------
	-- Select most recent version of staging table rows.
	-- ---------------------------------------------------------------------------
	-- 
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Selecting most recent version of staging table rows...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51001
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Selection of most recent version of staging table rows FAILED.'
	--
	;WITH
	[ids] AS
	(
	SELECT
		IsNull( LTrim( RTrim( r.[CUR_CLM_UNIQ_ID] ) ), '' ) AS [CUR_CLM_UNIQ_ID],
		IsNull( LTrim( RTrim( r.[CLM_LINE_NUM] ) ), '' ) AS [CLM_LINE_NUM],
		IsNull( r.[fileDate], GetUTCDate() ) AS [fileDate],
		r.[ID],
		Row_Number() OVER (
			PARTITION BY
				r.[CUR_CLM_UNIQ_ID],
				r.[CLM_LINE_NUM]
			ORDER BY
				r.[fileDate] DESC
			) AS [SequenceNum]
	FROM
		[dbo].[CCLF_5_PartB_Physicians_raw] r WITH (TABLOCK)
	),
	[Sorted] AS
	(
	SELECT
		ids.[CUR_CLM_UNIQ_ID],
		ids.[CLM_LINE_NUM],
		ids.[fileDate],
		ids.[ID],
		ids.[SequenceNum],
		--r.[PREV_CLM_UNIQ_ID],
		r.[BENE_HIC_NUM],
		r.[CLM_TYPE_CD],
		r.[CLM_FROM_DT],
		r.[CLM_THRU_DT],
		r.[RNDRG_PRVDR_TYPE_CD],
		r.[RNDRG_PRVDR_FIPS_ST_CD],
		r.[CLM_PRVDR_SPCLTY_CD],
		r.[CLM_FED_TYPE_SRVC_CD],
		r.[CLM_POS_CD],
		r.[CLM_LINE_FROM_DT],
		r.[CLM_LINE_THRU_DT],
		r.[CLM_LINE_HCPCS_CD],
		r.[CLM_LINE_CVRD_PD_AMT],
		r.[CLM_PRMRY_PYR_CD],
		r.[CLM_LINE_DGNS_CD],
		r.[CLM_RNDRG_PRVDR_TAX_NUM],
		r.[RNDRG_PRVDR_NPI_NUM],
		r.[CLM_CARR_PMT_DNL_CD],
		r.[CLM_PRCSG_IND_CD],
		r.[CLM_ADJSMT_TYPE_CD],
		r.[CLM_EFCTV_DT],
		r.[CLM_IDR_LD_DT],
		r.[CLM_CNTL_NUM],
		r.[BENE_EQTBL_BIC_HICN_NUM],
		r.[CLM_LINE_ALOWD_CHRG_AMT],
		r.[CLM_LINE_ALOWD_UNIT_QTY],
		r.[HCPCS_1_MDFR_CD],
		r.[HCPCS_2_MDFR_CD],
		r.[HCPCS_3_MDFR_CD],
		r.[HCPCS_4_MDFR_CD],
		r.[HCPCS_5_MDFR_CD],
		r.[CLM_DISP_CD],
		r.[CLM_DGNS_1_CD],
		r.[CLM_DGNS_2_CD],
		r.[CLM_DGNS_3_CD],
		r.[CLM_DGNS_4_CD],
		r.[CLM_DGNS_5_CD],
		r.[CLM_DGNS_6_CD],
		r.[CLM_DGNS_7_CD],
		r.[CLM_DGNS_8_CD],
		--r.[CLM_LINE_SRVC_UNIT_QTY],
		r.[ICD_TYPE],
		r.[SOURCEFEED],
		r.[CLM_PBP_INCLSN_AMT] ,
		R.[CLM_PBP_RDCTN_AMT] 
	FROM
		[ids] WITH (TABLOCK)
		JOIN [dbo].[CCLF_5_PartB_Physicians_raw] r WITH (TABLOCK)
			ON  r.[id] = ids.[id]
	)
	SELECT
		[CUR_CLM_UNIQ_ID],
		[CLM_LINE_NUM],
		[fileDate],
		[ID],
		--[PREV_CLM_UNIQ_ID],
		[BENE_HIC_NUM],
		[CLM_TYPE_CD],
		[CLM_FROM_DT],
		[CLM_THRU_DT],
		[RNDRG_PRVDR_TYPE_CD],
		[RNDRG_PRVDR_FIPS_ST_CD],
		[CLM_PRVDR_SPCLTY_CD],
		[CLM_FED_TYPE_SRVC_CD],
		[CLM_POS_CD],
		[CLM_LINE_FROM_DT],
		[CLM_LINE_THRU_DT],
		[CLM_LINE_HCPCS_CD],
		[CLM_LINE_CVRD_PD_AMT],
		[CLM_PRMRY_PYR_CD],
		[CLM_LINE_DGNS_CD],
		[CLM_RNDRG_PRVDR_TAX_NUM],
		[RNDRG_PRVDR_NPI_NUM],
		[CLM_CARR_PMT_DNL_CD],
		[CLM_PRCSG_IND_CD],
		[CLM_ADJSMT_TYPE_CD],
		[CLM_EFCTV_DT],
		[CLM_IDR_LD_DT],
		[CLM_CNTL_NUM],
		[BENE_EQTBL_BIC_HICN_NUM],
		[CLM_LINE_ALOWD_CHRG_AMT],
		[CLM_LINE_ALOWD_UNIT_QTY],
		[HCPCS_1_MDFR_CD],
		[HCPCS_2_MDFR_CD],
		[HCPCS_3_MDFR_CD],
		[HCPCS_4_MDFR_CD],
		[HCPCS_5_MDFR_CD],
		[CLM_DISP_CD],
		[CLM_DGNS_1_CD],
		[CLM_DGNS_2_CD],
		[CLM_DGNS_3_CD],
		[CLM_DGNS_4_CD],
		[CLM_DGNS_5_CD],
		[CLM_DGNS_6_CD],
		[CLM_DGNS_7_CD],
		[CLM_DGNS_8_CD],
		--[CLM_LINE_SRVC_UNIT_QTY],
		[ICD_TYPE],
		[SOURCEFEED],
		[CLM_PBP_INCLSN_AMT] ,
		[CLM_PBP_RDCTN_AMT] 
	INTO
		[#Source]
	FROM
		[Sorted] WITH (TABLOCK)
	WHERE
		    [SequenceNum] = 1
	;
	SET @RowCount = @@RowCount;
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' +
		'Selection of most recent version of staging table rows complete.' + ':  ' +
		'Rows Selected: ' + Replace( Convert( VARCHAR, Cast( @RowCount AS MONEY ), 1 ), '.00', '' )
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	

	-- ---------------------------------------------------------------------------
	-- Delete rows with a bad SSNK.
	-- ---------------------------------------------------------------------------
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Deleting rows with a bad SSNK from temp staging table...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51002
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Deleting rows with a bad SSNK from temp staging table FAILED.'
	--
	DELETE [#Source] WHERE NullIf( [CUR_CLM_UNIQ_ID], '' ) IS NULL OR NullIf( [CLM_LINE_NUM], '' ) IS NULL;
	SET @RowCount = @@RowCount;
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' +
		'Deleting rows with a bad SSNK from temp staging table complete.' + ':  ' +
		'Rows Deleted: ' + Replace( Convert( VARCHAR, Cast( @RowCount AS MONEY ), 1 ), '.00', '' )
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;


	-- ---------------------------------------------------------------------------
	-- Add PK to temp staging table.
	-- ---------------------------------------------------------------------------
	-- 
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Adding PK to temp staging table...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51003
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Addition of PK to temp staging table FAILED.'
	--
	SET @SQL = '
	ALTER TABLE [#Source] ADD CONSTRAINT [PK_Source_' + Cast( NEWID() AS VARCHAR( 36 ) ) + '] PRIMARY KEY
	(
		[CUR_CLM_UNIQ_ID],
		[CLM_LINE_NUM]
	);'
	EXECUTE( @SQL );
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Addition of PK to temp staging table complete.'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	

	-- ---------------------------------------------------------------------------
	-- Update the statistics on the temp table
	-- See OPTION (RECOMPILE) on MERGE statement below, too.
	-- ---------------------------------------------------------------------------
	--	
	UPDATE STATISTICS [#Source] WITH FULLSCAN;
	
	
	-- ---------------------------------------------------------------------------
	-- Begin a transaction.
	-- ---------------------------------------------------------------------------
	--
	BEGIN TRAN;
	
	
	-- ---------------------------------------------------------------------------
	-- Merge from indexed temp staging table source to target.
	-- ---------------------------------------------------------------------------
	-- 
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Merging from indexed temp staging table source to target...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51004
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Merge from indexed temp staging table source to target FAILED.'
	--
	MERGE INTO [dbo].[CCLF_5_PartB_Physicians] tgt
	USING [#Source] src
		ON  tgt.[CUR_CLM_UNIQ_ID] = src.[CUR_CLM_UNIQ_ID]
		AND tgt.[CLM_LINE_NUM] = src.[CLM_LINE_NUM]
	WHEN MATCHED THEN
		UPDATE
		SET
--			[CUR_CLM_UNIQ_ID] = src.[CUR_CLM_UNIQ_ID],
--			[CLM_LINE_NUM] = src.[CLM_LINE_NUM],
			[fileDate] = src.[fileDate],
--			[ID] = src.[ID],
			--[PREV_CLM_UNIQ_ID] = src.[PREV_CLM_UNIQ_ID],
			[BENE_HIC_NUM] = src.[BENE_HIC_NUM],
			[CLM_TYPE_CD] = src.[CLM_TYPE_CD],
			[CLM_FROM_DT] = src.[CLM_FROM_DT],
			[CLM_THRU_DT] = src.[CLM_THRU_DT],
			[RNDRG_PRVDR_TYPE_CD] = src.[RNDRG_PRVDR_TYPE_CD],
			[RNDRG_PRVDR_FIPS_ST_CD] = src.[RNDRG_PRVDR_FIPS_ST_CD],
			[CLM_PRVDR_SPCLTY_CD] = src.[CLM_PRVDR_SPCLTY_CD],
			[CLM_FED_TYPE_SRVC_CD] = src.[CLM_FED_TYPE_SRVC_CD],
			[CLM_POS_CD] = src.[CLM_POS_CD],
			[CLM_LINE_FROM_DT] = src.[CLM_LINE_FROM_DT],
			[CLM_LINE_THRU_DT] = src.[CLM_LINE_THRU_DT],
			[CLM_LINE_HCPCS_CD] = src.[CLM_LINE_HCPCS_CD],
			[CLM_LINE_CVRD_PD_AMT] = src.[CLM_LINE_CVRD_PD_AMT],
			[CLM_PRMRY_PYR_CD] = src.[CLM_PRMRY_PYR_CD],
			[CLM_LINE_DGNS_CD] = src.[CLM_LINE_DGNS_CD],
			[CLM_RNDRG_PRVDR_TAX_NUM] = src.[CLM_RNDRG_PRVDR_TAX_NUM],
			[RNDRG_PRVDR_NPI_NUM] = src.[RNDRG_PRVDR_NPI_NUM],
			[CLM_CARR_PMT_DNL_CD] = src.[CLM_CARR_PMT_DNL_CD],
			[CLM_PRCSG_IND_CD] = src.[CLM_PRCSG_IND_CD],
			[CLM_ADJSMT_TYPE_CD] = src.[CLM_ADJSMT_TYPE_CD],
			[CLM_EFCTV_DT] = src.[CLM_EFCTV_DT],
			[CLM_IDR_LD_DT] = src.[CLM_IDR_LD_DT],
			[CLM_CNTL_NUM] = src.[CLM_CNTL_NUM],
			[BENE_EQTBL_BIC_HICN_NUM] = src.[BENE_EQTBL_BIC_HICN_NUM],
			[CLM_LINE_ALOWD_CHRG_AMT] = src.[CLM_LINE_ALOWD_CHRG_AMT],
			[CLM_LINE_ALOWD_UNIT_QTY] = src.[CLM_LINE_ALOWD_UNIT_QTY],
			[HCPCS_1_MDFR_CD] = src.[HCPCS_1_MDFR_CD],
			[HCPCS_2_MDFR_CD] = src.[HCPCS_2_MDFR_CD],
			[HCPCS_3_MDFR_CD] = src.[HCPCS_3_MDFR_CD],
			[HCPCS_4_MDFR_CD] = src.[HCPCS_4_MDFR_CD],
			[HCPCS_5_MDFR_CD] = src.[HCPCS_5_MDFR_CD],
			[CLM_DISP_CD] = src.[CLM_DISP_CD],
			[CLM_DGNS_1_CD] = src.[CLM_DGNS_1_CD],
			[CLM_DGNS_2_CD] = src.[CLM_DGNS_2_CD],
			[CLM_DGNS_3_CD] = src.[CLM_DGNS_3_CD],
			[CLM_DGNS_4_CD] = src.[CLM_DGNS_4_CD],
			[CLM_DGNS_5_CD] = src.[CLM_DGNS_5_CD],
			[CLM_DGNS_6_CD] = src.[CLM_DGNS_6_CD],
			[CLM_DGNS_7_CD] = src.[CLM_DGNS_7_CD],
			[CLM_DGNS_8_CD] = src.[CLM_DGNS_8_CD],
			--[CLM_LINE_SRVC_UNIT_QTY] = src.[CLM_LINE_SRVC_UNIT_QTY],
			[ICD_TYPE] = src.[ICD_TYPE],
			[SOURCEFEED] = src.[SOURCEFEED],
			[CLM_PBP_INCLSN_AMT] = src.[CLM_PBP_INCLSN_AMT],
			[CLM_PBP_RDCTN_AMT] = src.[CLM_PBP_RDCTN_AMT]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
		(
			[CUR_CLM_UNIQ_ID],
			[CLM_LINE_NUM],
			[fileDate],
--			[ID],
			--[PREV_CLM_UNIQ_ID],
			[BENE_HIC_NUM],
			[CLM_TYPE_CD],
			[CLM_FROM_DT],
			[CLM_THRU_DT],
			[RNDRG_PRVDR_TYPE_CD],
			[RNDRG_PRVDR_FIPS_ST_CD],
			[CLM_PRVDR_SPCLTY_CD],
			[CLM_FED_TYPE_SRVC_CD],
			[CLM_POS_CD],
			[CLM_LINE_FROM_DT],
			[CLM_LINE_THRU_DT],
			[CLM_LINE_HCPCS_CD],
			[CLM_LINE_CVRD_PD_AMT],
			[CLM_PRMRY_PYR_CD],
			[CLM_LINE_DGNS_CD],
			[CLM_RNDRG_PRVDR_TAX_NUM],
			[RNDRG_PRVDR_NPI_NUM],
			[CLM_CARR_PMT_DNL_CD],
			[CLM_PRCSG_IND_CD],
			[CLM_ADJSMT_TYPE_CD],
			[CLM_EFCTV_DT],
			[CLM_IDR_LD_DT],
			[CLM_CNTL_NUM],
			[BENE_EQTBL_BIC_HICN_NUM],
			[CLM_LINE_ALOWD_CHRG_AMT],
			[CLM_LINE_ALOWD_UNIT_QTY],
			[HCPCS_1_MDFR_CD],
			[HCPCS_2_MDFR_CD],
			[HCPCS_3_MDFR_CD],
			[HCPCS_4_MDFR_CD],
			[HCPCS_5_MDFR_CD],
			[CLM_DISP_CD],
			[CLM_DGNS_1_CD],
			[CLM_DGNS_2_CD],
			[CLM_DGNS_3_CD],
			[CLM_DGNS_4_CD],
			[CLM_DGNS_5_CD],
			[CLM_DGNS_6_CD],
			[CLM_DGNS_7_CD],
			[CLM_DGNS_8_CD],
			--[CLM_LINE_SRVC_UNIT_QTY],
      [SOURCEFEED],
			[CLM_PBP_INCLSN_AMT] ,
			[CLM_PBP_RDCTN_AMT] 
		)
		VALUES
		(
			src.[CUR_CLM_UNIQ_ID],
			src.[CLM_LINE_NUM],
			src.[fileDate],
--			src.[ID],
			--src.[PREV_CLM_UNIQ_ID],
			src.[BENE_HIC_NUM],
			src.[CLM_TYPE_CD],
			src.[CLM_FROM_DT],
			src.[CLM_THRU_DT],
			src.[RNDRG_PRVDR_TYPE_CD],
			src.[RNDRG_PRVDR_FIPS_ST_CD],
			src.[CLM_PRVDR_SPCLTY_CD],
			src.[CLM_FED_TYPE_SRVC_CD],
			src.[CLM_POS_CD],
			src.[CLM_LINE_FROM_DT],
			src.[CLM_LINE_THRU_DT],
			src.[CLM_LINE_HCPCS_CD],
			src.[CLM_LINE_CVRD_PD_AMT],
			src.[CLM_PRMRY_PYR_CD],
			src.[CLM_LINE_DGNS_CD],
			src.[CLM_RNDRG_PRVDR_TAX_NUM],
			src.[RNDRG_PRVDR_NPI_NUM],
			src.[CLM_CARR_PMT_DNL_CD],
			src.[CLM_PRCSG_IND_CD],
			src.[CLM_ADJSMT_TYPE_CD],
			src.[CLM_EFCTV_DT],
			src.[CLM_IDR_LD_DT],
			src.[CLM_CNTL_NUM],
			src.[BENE_EQTBL_BIC_HICN_NUM],
			src.[CLM_LINE_ALOWD_CHRG_AMT],
			src.[CLM_LINE_ALOWD_UNIT_QTY],
			src.[HCPCS_1_MDFR_CD],
			src.[HCPCS_2_MDFR_CD],
			src.[HCPCS_3_MDFR_CD],
			src.[HCPCS_4_MDFR_CD],
			src.[HCPCS_5_MDFR_CD],
			src.[CLM_DISP_CD],
			src.[CLM_DGNS_1_CD],
			src.[CLM_DGNS_2_CD],
			src.[CLM_DGNS_3_CD],
			src.[CLM_DGNS_4_CD],
			src.[CLM_DGNS_5_CD],
			src.[CLM_DGNS_6_CD],
			src.[CLM_DGNS_7_CD],
			src.[CLM_DGNS_8_CD],
			--src.[CLM_LINE_SRVC_UNIT_QTY],
      src.[SOURCEFEED],
			src.[CLM_PBP_INCLSN_AMT] ,
			src.[CLM_PBP_RDCTN_AMT] 
		)
	OPTION (RECOMPILE)
	;
	SET @RowCount = @@RowCount;
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' +
		'Merge from indexed temp staging table source to target complete.' + ':  ' +
		'Rows Merged:  ' + Replace( Convert( VARCHAR, Cast( @RowCount AS MONEY ), 1 ), '.00', '' )
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	
	
	-- ---------------------------------------------------------------------------
	-- Commit the Merge transaction.
	-- ---------------------------------------------------------------------------
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Committing Merge transaction...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51005
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Commit of Merge transaction FAILED.'
	--
	COMMIT;
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Commit of Merge transaction complete.'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	
	
	-- ---------------------------------------------------------------------------
	-- Begin another transaction.
	-- ---------------------------------------------------------------------------
	--
	BEGIN TRAN;
	
	
	-- ---------------------------------------------------------------------------
	-- Truncate the [dbo].[CCLF_5_PartB_Physicians_raw] table.
	-- ---------------------------------------------------------------------------
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Truncating the [dbo].[CCLF_5_PartB_Physicians_raw] table...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51006
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Truncation of the [dbo].[CCLF_5_PartB_Physicians_raw] table FAILED.'
	--
	TRUNCATE TABLE [dbo].[CCLF_5_PartB_Physicians_raw];
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Truncation of the [dbo].[CCLF_5_PartB_Physicians_raw] table complete.'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	
	
	-- ---------------------------------------------------------------------------
	-- Commit the Truncate transaction.
	-- ---------------------------------------------------------------------------
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Committing Truncate transaction...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51007
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Commit of Truncate transaction FAILED.'
	--
	COMMIT;
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Commit of Truncate transaction complete.'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	
	
	
	-- ---------------------------------------------------------------------------
	--
	--                                W R A P - U P
	--
	-- ---------------------------------------------------------------------------
	--
	
	SUCCESS:
	-- Success.
	--
	SET @ErrorNumber = 0
	-- PRINT ''
	-- PRINT '***************************************************************************'
	-- PRINT '*                                                                         *'
	-- PRINT '*                     S C R I P T   S U C C E E D E D                     *'
	-- PRINT '*                                                                         *'
	-- PRINT '***************************************************************************'
	-- PRINT ''
END TRY


BEGIN CATCH
	-- Capture outstanding Error information prior to doing any ROLLBACK.
	--
	SELECT
		@ErrorNumber = Coalesce( ERROR_NUMBER(), 50099 ),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = Coalesce( ERROR_PROCEDURE(), @ScriptName, 'SQL Script' ),
		@ErrorMessage = ERROR_MESSAGE()
	;
	-- Build a message string that contains error information.
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' +
		    'Error # ' + Cast( @ErrorNumber AS VARCHAR ) +
		' :: Severity ' + Cast( @ErrorSeverity AS VARCHAR ) +
		' :: State ' + Cast( ERROR_STATE() AS VARCHAR ) +
		' :: Procedure ' + @ErrorProcedure + ' (Line ' + Cast( @ErrorLine AS VARCHAR ) + ')' +
		' :: Message:  ' + @ErrorMessage
	
	-- Rollback any outstanding or uncommittable transaction.
	--
	IF XACT_STATE() <> 0
	BEGIN
		WHILE XACT_STATE() <> 0 ROLLBACK TRANSACTION;
		SET @ErrorMessage = @ErrorMessage + ' :: ROLLBACK TRANSACTION has occurred.'
	END
	
	RAISERROR( '', 0, 0 ) WITH NOWAIT;
	RAISERROR( '***************************************************************************', 0, 0 ) WITH NOWAIT;
	RAISERROR( '*                                                                         *', 0, 0 ) WITH NOWAIT;
	RAISERROR( '*                        S C R I P T   F A I L E D                        *', 0, 0 ) WITH NOWAIT;
	RAISERROR( '*                                                                         *', 0, 0 ) WITH NOWAIT;
	RAISERROR( '***************************************************************************', 0, 0 ) WITH NOWAIT;
	RAISERROR( '', 0, 0 ) WITH NOWAIT;
	
	-- Raise the appropriate error using RAISERRROR.
	--
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
END CATCH


SCRIPT_EXIT:
-- ===========================================================================
--
--                         E N D   S C R I P T
--
-- ===========================================================================
--
-- Return status.
--
SET @Return = @ErrorNumber
--
SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Executing ' + @ScriptName + ' Complete.  Return Value:  (' + Cast( @Return AS VARCHAR ) + ').'
RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
RAISERROR( '', 0, 0 ) WITH NOWAIT;
--
RETURN @Return


GO
