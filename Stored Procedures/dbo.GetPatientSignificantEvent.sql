SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GetPatientSignificantEvent] (@patientId INT,@alertDuration INT)
AS
BEGIN
	DECLARE @now DATETIME = GETUTCDATE();

	SELECT TOP 1 'ALERT - ' + CASE WHEN A.PatientEventTypeId = 1 THEN 'Admitted' WHEN A.PatientEventTypeId = 2 
	THEN 'Discharged' WHEN A.PatientEventTypeId = 3 THEN 'Transfered' 
	WHEN A.PatientEventTypeId = 4 THEN 'Registered' END + ' on ' +
	 CAST(B.EventDateTime AS VARCHAR(50)) AS EventDescription FROM [dbo].[PatientEventType] A
	INNER JOIN [dbo].[PatientEvent] B ON A.PatientEventTypeId = B.PatientEventTypeId
	WHERE B.LBPatientId = @patientId
	AND DATEDIFF(DAY, B.EventDateTime, @now) <= @alertDuration
	ORDER BY B.EventDateTime DESC;
END

GO
