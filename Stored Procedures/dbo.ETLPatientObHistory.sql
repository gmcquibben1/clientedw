SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Youping
-- Create date: 2016-08-11
-- Description:	Load Interface PatientObHistory Data into EDW
-- @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
-- @fullLoadFlag =0 Merge date to the existing table
-- Add procedure running log to [Maintenance].[dbo].[EdwProcedureRunLog]
-- ===============================================================

Create PROCEDURE [dbo].[ETLPatientObHistory](@fullLoadFlag bit = 0)

AS
BEGIN	
 
    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientObHistory';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientObHistory';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY

  		
			--set @fullLoadFlag =1
	
			IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] =@EDWName)
			  BEGIN
			  
					 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed

			  END

			IF @fullLoadFlag = 1
			  BEGIN
				UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				   SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME)
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
			  END

			DECLARE @lastDateTime DATETIME = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
											FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
											WHERE [EDWtableName] = @EDWtableName 
											AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	



			
			SELECT  [InterfacePatientOBHistoryId]
			     	,ISS.SourceSystemId
				   ,IP.PatientIdentifier
				  ,[DateRecorded]
				  ,[Gravidity]
				  ,[Parity1]
				  ,[Parity2]
				  ,[Parity3]
				  ,[Parity4]
				  ,[LastMenstrualPeriodDate]
				  ,IPH.[CreateDateTime]
				INTO #PatientObHistoryQue	
					  
					FROM [dbo].[vwPortal_InterfacePatientObHistory] IPH with (nolock)
					INNER JOIN [dbo].[vwPortal_InterfacePatient] IP with (nolock) ON IPH.InterfacePatientId = IP.InterfacePatientId
					INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) ON IPH.InterfaceSystemId = ISS.[InterfaceSystemId]
					WHERE IPH.CreateDateTime >= @lastDateTime 
				
				;




              --- add Index
			  Create  NONCLUSTERED INDEX IDX_PatientIdentifier_SourceSystemId ON #PatientObHistoryQue (PatientIdentifier,SourceSystemId) INCLUDE (DateRecorded  );
            			
				
	-----   dedup data prepare for Merge
			 SELECT * 
			 INTO #PatientObHistory
			 FROM (
				SELECT  [InterfacePatientObHistoryId]
						,PH.SourceSystemId
						, idRef.LbPatientId AS PatientID
				  ,[DateRecorded]
				  ,[Gravidity]
				  ,[Parity1]
				  ,[Parity2]
				  ,[Parity3]
				  ,[Parity4]
				  ,[LastMenstrualPeriodDate]
				  ,PH.[CreateDateTime]
					, ROW_NUMBER() OVER ( PARTITION BY 	 idRef.LbPatientId, DateRecorded 
					ORDER BY 	InterfacePatientObHistoryId DESC ) RowNbr
					FROM  #PatientObHistoryQue PH with (nolock)
					 INNER JOIN dbo.PatientIdReference idRef with (nolock) ON idRef.SourceSystemId = PH.SourceSystemId 
					  AND idRef.ExternalId = PH.PatientIdentifier 
					  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
					  AND idRef.DeleteInd =0
				
				) A
				WHERE A.RowNbr=1

			--- Truncate data
			 IF (@fullLoadFlag=1)
			   BEGIN
				   Truncate table [dbo].[PatientObHistory];
			   END
	
			  CREATE CLUSTERED INDEX CIX_#PatientID_DiagnosisCode ON #PatientObHistory([PatientId],DateRecorded) ;

			SELECT @RecordCountBefore=COUNT(1)
			FROM [dbo].[PatientObHistory];


			SELECT @RecordCountSource=COUNT(1)
			FROM #PatientObHistory;	    

			  --- test select * from #PatientObHistory


				MERGE [DBO].PatientObHistory AS [target]
				USING #PatientObHistory AS [source] 

				ON ( target.PatientID =[source].PatientID  AND ISNULL(target.DateRecorded,'1900-01-01') = ISNULL(source.DateRecorded,'1900-01-01')
					)
                WHEN MATCHED  THEN UPDATE SET 
				      target.[Gravidity]				= source.[Gravidity]
					,target.[Parity1]	= source.[Parity1]
					,target.[Parity2]	= source.[Parity2]
					,target.[Parity3]	= source.[Parity3]
					,target.[Parity4]   = source.[Parity4]
					,target.[LastMenstrualPeriod]	= source.[LastMenstrualPeriodDate]
					,target.[CreateDateTime]=source.[CreateDateTime]
					,target.[ModifyDateTime]		= @Today
					,target.[CreateLBUserId]=1
					,target.[ModifyLBUserId]=1
				WHEN NOT MATCHED THEN
				INSERT ([PatientId]
						,[DateRecorded]
						,[Gravidity]
						,[Parity1]
						,[Parity2]
						,[Parity3]
						,[Parity4]
						,[LastMenstrualPeriod]
						,[DeleteInd]
						,[CreateDateTime]
						,[ModifyDateTime]
						,[CreateLBUserId]
						,[ModifyLBUserId])
				VALUES (source.[PatientId],source.[DateRecorded],
						source.[Gravidity],source.[Parity1],source.[Parity2],
						source.[Parity3],source.[Parity4],source.[LastMenstrualPeriodDate],	0,source.[CreateDateTime],@Today,1,1);

	    
		

		    -- update lastdate
               		   
			 -- update maxSourceTimeStamp with max CreateDateTime of #PatientPatientObHistoryQue
			 UPDATE [Maintenance]..[EDWTableLoadTracking]
			 SET [maxSourceTimeStampProcessed] = (SELECT MAX(CreateDateTime) FROM #PatientObHistoryQue ),
				 [updateDateTime] =GETUTCDATE()
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


					
		   -- get total records from PatientPatientObHistory after merge 
		    			SELECT @RecordCountAfter=COUNT(1)
			            FROM PatientObHistory;

              -- Calculate records inserted and updated counts
	             SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	             SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			   SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
			  --clean
			  drop table  #PatientObHistory,#PatientObHistoryQue
	
 	END TRY

		BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END


GO
