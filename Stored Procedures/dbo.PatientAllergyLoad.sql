SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientAllergyLoad] 
	@PatientId INTEGER
AS

SELECT DISTINCT       
	dbo.AllergyType.DisplayValue as AllergyType,
	dbo.PatientAllergy.AllergyDescription, 
	dbo.SourceSystem.Description as SourceSystem,
	dbo.AllergySeverityType.DisplayValue as AllergySeverity,
	dbo.AllergyReactionType.DisplayValue as AllergyReaction,
	dbo.AllergyStatusType.DisplayValue as AllergyStatus,
	dbo.PatientAllergy.OnsetDate,
	dbo.PatientAllergy.AllergyComment,
	dbo.PatientAllergy.ActiveInd

FROM dbo.PatientAllergy LEFT OUTER JOIN
	 dbo.SourceSystem ON PatientAllergy.SourceSystemId = SourceSystem.SourceSystemId LEFT OUTER JOIN
     dbo.AllergyType ON dbo.PatientAllergy.AllergyTypeId = dbo.AllergyType.AllergyTypeId LEFT OUTER JOIN
     dbo.AllergySeverityType ON dbo.PatientAllergy.AllergySeverityTypeId = dbo.AllergySeverityType.AllergySeverityTypeId LEFT OUTER JOIN
     dbo.AllergyReactionType ON dbo.PatientAllergy.AllergyReactionTypeId = dbo.AllergyReactionType.AllergyReactionTypeId LEFT OUTER JOIN
     dbo.AllergyStatusType ON dbo.PatientAllergy.AllergyStatusTypeId = dbo.AllergyStatusType.AllergyStatusTypeId

WHERE dbo.PatientAllergy.PatientId = @Patientid AND dbo.PatientAllergy.DeleteInd = 0 AND
      dbo.PatientAllergy.ActiveInd = 1 

ORDER BY PatientAllergy.AllergyDescription ASC



GO
