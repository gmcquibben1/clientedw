SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[GproRawRankingLoad] 
/*===============================================================
	CREATED BY: 	BRR
	CREATED ON:		2017-01-06
	INITIAL VER:	2.2.1
	MODULE:			GPRO		
	DESCRIPTION:	List all of the patients associated with a given BU and their rank for every measure
	PARAMETERS:		BusinessUnitId
	RETURN VALUE(s)/OUTPUT:	a table containing all of the available patient list. 
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		01-06-2017		BR			LBPP-2025	Initial Version

=================================================================*/
(
    @BusinessUnitId INT 
)
AS
BEGIN
	--DECLARE @businessUnitid INT = 153
	DECLARE @RankColumnName VARCHAR(50);

	IF OBJECT_ID( 'tempdb..#patientList' ) IS NOT NULL DROP TABLE [#patientList];
	CREATE TABLE #patientList
	(
		GproPatientRankingId INT
		UNIQUE CLUSTERED (GproPatientRankingId)
	);

	INSERT INTO #patientList EXEC GproGetPatientListByBusinessUnit @businessUnitId;

	IF OBJECT_ID( 'tempdb..#RankingOutput' ) IS NOT NULL DROP TABLE [#RankingOutput];
	CREATE TABLE #RankingOutput
	(
	 GproPatientRankingId int 
	 ,GroupTinName varchar(100)
	 ,ClinicName varchar(100)
	 ,FirstName varchar(50)
	 ,LastName varchar(50)
	 ,BirthDate varchar(20)
	 ,Care2Rank int
	 ,Care3Rank int
	 ,CadRank int
	 ,DmRank int
	 ,HfRank int
	 ,HtnRank int
	 ,IvdRank int
	 ,MhRank int
	 ,Prev5Rank int
	 ,Prev6Rank int
	 ,Prev7Rank int
	 ,Prev8Rank int
	 ,Prev9Rank int
	 ,Prev10Rank int
	 ,Prev11Rank int
	 ,Prev12Rank int
	 ,Prev13Rank int
	)

	--------------------------------------------------------------------------------
	-- Get the data from GproPatientRanking
	--------------------------------------------------------------------------------
	INSERT INTO #RankingOutput
		(GproPatientRankingId,GroupTinName,ClinicName,FirstName,LastName,BirthDate,
		Care2Rank,Care3Rank,CadRank,DmRank,HfRank,HtnRank,IvdRank,MhRank,
		Prev5Rank,Prev6Rank,Prev7Rank,Prev8Rank,
		Prev9Rank,Prev10Rank,Prev11Rank,Prev12Rank,Prev13Rank)
	SELECT
		gpr.GproPatientRankingId,bu1.Description,bu2.Description,gpr.Firstname,gpr.LastName,gpr.BirthDate,
		ISNULL(gpr.CareFallsRank,0),ISNULL(gpr.CareMeddocRank,0),ISNULL(gpr.CadRank,0),ISNULL(gpr.DmRank,0),ISNULL(gpr.HfRank,0),ISNULL(gpr.HtnRank,0),ISNULL(gpr.IvdRank,0),ISNULL(gpr.MhRank,0),
		ISNULL(gpr.PcMammogramRank,0),ISNULL(gpr.PcColorectalRank,0),ISNULL(gpr.PcFluShotRank,0),ISNULL(gpr.PcPneumoshotRank,0),
		ISNULL(gpr.PcBmiScreenRank,0),ISNULL(gpr.PcTobaccoUseRank,0),ISNULL(gpr.PcBloodPressureRank,0),ISNULL(gpr.PcDepressionRank,0),ISNULL(gpr.PcStatinRank,0)
	FROM GproPatientRanking gpr
		INNER JOIN #patientList pl ON pl.GproPatientRankingId = gpr.GproPatientRankingId
		LEFT OUTER JOIN vwPortal_BusinessUnit bu1 ON bu1.GroupTin = gpr.GroupTin
		LEFT OUTER JOIN vwPortal_BusinessUnit bu2 ON bu2.ExternalCode = gpr.ClinicIdentifier

	--------------------------------------------------------------------------------
	-- Return the result row with the totals
	--------------------------------------------------------------------------------
	SELECT * FROM #RankingOutput;

END









GO
