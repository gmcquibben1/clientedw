SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ===============================================================================================================
-- Author:		Lan Ma
-- Create date: 2016-10-07
-- Description: This SP determin a single SNF Admit based on Contiguous Claim From and Claim Through Date, then 
-- put a single SNF Admit's Original Claim ID, Initial From Date of Service, Last Thru Date of Service and 
-- sum of paid amount in a single row.
-- Modified By: Mike Hoxter
-- Modified Date: 1/11/2017
-- Description:	Adding additional patient data elements, such as risk scores. Also creating fields for future use such as 
--              ER During Stay, IP During Stay, and IP Readmission fields
-- ===============================================================================================================

CREATE PROCEDURE [dbo].[SPAnalyticsSNFAdmitSummary] 

AS
BEGIN

		SET NOCOUNT ON;

		SELECT DISTINCT
		LbPatientId,
		CLM_FROM_DT,
		CLM_THRU_DT
		INTO #SNFPatients
		FROM CCLF_1_PartA_Header
		WHERE LbPatientID IS NOT NULL
		AND	(try_convert(int,CLM_TYPE_CD)= 20 or try_convert(int,CLM_TYPE_CD ) = 30)
		AND CLM_FROM_DT !='1/1/1000 12:00:00 AM' 
		AND (CLM_MDCR_NPMT_RSN_CD='' or CLM_MDCR_NPMT_RSN_CD IS NULL) 
		--AND  ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2)
		ORDER BY LbPatientId
 
		--This provides our anchor per member. We know that any changes
		--in enrollment must have occurred between those dates. The anchor
		--is important and can be used to easily change the logic in case
		--you are only interested in certain date ranges.
		SELECT
		LbPatientId
		,MIN(CLM_FROM_DT) AS CLM_FROM_DT
		,MAX(CLM_THRU_DT) AS CLM_THRU_DT
		INTO #Anchor
		FROM #SNFPatients
		GROUP BY LbPatientId

		--drop table #Anchor

		--This step establishes all the segment change dates per member
		--that occurred during the anchor period. The term date being
		--returned will alway be from the anchor record. The actual term
		--date for each segment will be determined in the next step.
		--A change point is defined as any new effective date during the
		--anchor period. We use term dates to establish new effective
		--dates as well by adding a day to it. This gives us the ability
		--to establish all of the segments we need.
        SELECT
        dt.LbPatientId,
        dt.CLM_FROM_DT,
        dt.CLM_THRU_DT,
        ROW_NUMBER() OVER(PARTITION BY dt.LbPatientID ORDER BY dt.CLM_FROM_DT) AS Sequence_ID
        INTO #Changes
        FROM (
               SELECT
               a.LbPatientId,
               e.CLM_FROM_DT,
               a.CLM_THRU_DT
               FROM #Anchor a
               INNER JOIN #SNFPatients e
               ON e.LbPatientID = a.LbPatientID
               AND e.CLM_FROM_DT BETWEEN a.CLM_FROM_DT AND a.CLM_THRU_DT                             
               --==============
               UNION
               --==============
               SELECT
               a.LbPatientId,
               DATEADD(DAY, 1, e.CLM_THRU_DT) AS CLM_FROM_DT,
               a.CLM_THRU_DT
               FROM #Anchor a
               INNER JOIN #SNFPatients e
			   ON e.LbPatientID = a.LbPatientID
			   AND e.CLM_THRU_DT BETWEEN a.CLM_FROM_DT AND a.CLM_THRU_DT
			   AND e.CLM_THRU_DT < a.CLM_THRU_DT
             ) dt
              
		--This step establishes all of the continuous non-overlapping segments
		--of the anchor period. It uses the sequence ID to look at the next record
		--to establish the term date by subtracting one day from the effective
		--date of the next segment. This ensures there are no overlapping records.
		SELECT
		c1.LbPatientID,
		c1.CLM_FROM_DT,
		CASE WHEN c2.LbPatientID IS NULL 
			 THEN  c1.CLM_THRU_DT  
			 ELSE DATEADD(DAY, -1, c2.CLM_FROM_DT)     
		END AS CLM_THRU_DT,                                             
		c1.Sequence_ID, 
		Enrollment_Count = (SELECT COUNT(*) FROM #SNFPatients e
				            WHERE e.LbPatientID = c1.LbPatientId
				            AND c1.CLM_FROM_DT BETWEEN e.CLM_FROM_DT AND e.CLM_THRU_DT) 			                                  				
		INTO #Segments
		FROM #Changes c1
		LEFT OUTER JOIN #Changes c2      
		ON c2.LbPatientId = c1.LbPatientId
		AND c2.Sequence_ID = c1.Sequence_ID + 1 --join with the next record                            

	   --select * from #Segments where Enrollment_Count >0
       --This step establishes all the segment gap dates per member
       --that occurred during the anchor period. The term date being
       --returned will always be from the anchor record. The actual term
       --date for each segment will be determined in the next step.
        SELECT
        dt.LbPatientID, 
        dt.CLM_FROM_DT, 
        dt.CLM_THRU_DT, 
        ROW_NUMBER() OVER(PARTITION BY dt.LbPatientID ORDER BY dt.CLM_FROM_DT) AS Sequence_ID
        INTO #ContinuousChanges
        FROM (             
               SELECT
               a.LbPatientID,
               a.CLM_FROM_DT,
               a.CLM_THRU_DT
               FROM #Anchor a
              --==============
               UNION
              --==============
               SELECT
			   a.LbPatientID,
			   s.CLM_FROM_DT,
			   a.CLM_THRU_DT
               FROM #Anchor a
               INNER JOIN #Segments s
               ON s.LbPatientID = a.LbPatientID       
               AND s.Enrollment_Count = 0                                           
              --==============
               UNION
              --==============
               SELECT
               a.LbPatientID,
               DATEADD(DAY, 1, s.CLM_THRU_DT) AS CLM_FROM_DT,
               a.CLM_THRU_DT
               FROM #Anchor a
               INNER JOIN #Segments s        
               ON s.LbPatientID = a.LbPatientID
               AND s.Enrollment_Count = 0
               AND s.CLM_THRU_DT BETWEEN a.CLM_FROM_DT AND a.CLM_THRU_DT
               AND s.CLM_THRU_DT < a.CLM_THRU_DT   --order by lbpatientid   
			 ) dt

		--This step establishes all of the continuous non-overlapping segments
		--of the anchor period using the gaps above. It uses the sequence ID
		--to look at the next record to establish the term date by subtracting
		--one day from the effective date of the next segment. This ensures there
		--are no overlapping records.
        SELECT
        c1.LbPatientID, 
        c1.CLM_FROM_DT, 
        CASE WHEN c2.LbPatientID IS NULL 
			 THEN c1.CLM_THRU_DT
             ELSE DATEADD(DAY, -1, c2.CLM_FROM_DT)
        END AS CLM_THRU_DT, 
        c1.Sequence_ID, 
        s.Enrollment_Count  
	    INTO #ContinuousSegments         
        FROM #ContinuousChanges c1
        LEFT OUTER JOIN #ContinuousChanges c2
        ON c2.LbPatientID = c1.LbPatientID
        AND c2.Sequence_ID = c1.Sequence_ID + 1 --join with the next record
        INNER JOIN #Segments s
        ON s.LbPatientID = c1.LbPatientID
        AND c1.CLM_FROM_DT BETWEEN s.CLM_FROM_DT AND s.CLM_THRU_DT  
	    WHERE s.Enrollment_Count>0  

	   --select * from #ContinuousSegments order by lbpatientId
	
		SELECT --DISTINCT
		LbPatientId,
		CUR_CLM_UNIQ_ID,
		CLM_FROM_DT,
		CLM_THRU_DT,
		ISNULL(CLM_PMT_AMT,0.00) AS CLM_PMT_AMT
		INTO #SNFPatientClaim
		FROM CCLF_1_PartA_Header
		WHERE LbPatientID IS NOT NULL
			AND	(try_convert(int,CLM_TYPE_CD)= 20 or try_convert(int,CLM_TYPE_CD ) = 30)
			AND CLM_FROM_DT !='1/1/1000 12:00:00 AM' 
			AND (CLM_MDCR_NPMT_RSN_CD='' or CLM_MDCR_NPMT_RSN_CD IS NULL) 
			--AND  ISNULL(try_convert(int,CLM_ADJSMT_TYPE_CD),0) NOT IN (1,2)
		 ORDER BY LbPatientId
	
		 SELECT 
		 c.LbPatientId,    
		 c.CLM_FROM_DT,
		 c.CLM_THRU_DT,
		 MIN(s.CUR_CLM_UNIQ_ID)  OVER(PARTITION BY c.LbPatientId,c.CLM_FROM_DT) AS Initial_CUR_CLM_UNIQ_ID,
		 SUM(CLM_PMT_AMT)  OVER(PARTITION BY c.LbPatientId,c.CLM_FROM_DT ) AS Total_CLM_PMT_AMT, 
		 s.CUR_CLM_UNIQ_ID,
		 ROW_NUMBER() OVER(PARTITION BY c.LbPatientId,c.CLM_FROM_DT ORDER BY s.CLM_FROM_DT,s.CUR_CLM_UNIQ_ID) AS rowNbr,
     0 as [ErDuringStay], 0 as [IpDuringStay], 0 as [IpAfterDischarge]
		 INTO #SNFClaimSummary
		 FROM #ContinuousSegments c
		 JOIN #SNFPatientClaim s
		 ON c.LbPatientID = s.LbPatientID
		 AND s.CLM_FROM_DT Between c.CLM_FROM_DT AND c.CLM_THRU_DT
		 AND s.CLM_THRU_DT Between c.CLM_FROM_DT AND c.CLM_THRU_DT
		 ORDER BY c.LbPatientId
	
  --Add queries to determine if ER happened during stay, IP During Stay (up to 5 days after discharge), or after discharge (5-30 days after discharge date)  
  
		 SELECT DISTINCT
      org.*, --OrgHierarchy
      PRNCPL_DGNS_CD, FAC_PRVDR_NPI_NUM,--from a
      dg.Level1 as DiagGroupLevel1, dg.Level2 as DiagGroupLevel2, dg.Level3 DiagGroupLevel3,dg.CodeDescription as DiagDesc, --from dg
      npi.[NAME] as [Facility Name],--from NPI
      DRG.Type as DrgType, DRG.Description as DrgDescription, --from DRG
     c.CLM_FROM_DT,
		 c.CLM_THRU_DT,
		 Initial_CUR_CLM_UNIQ_ID,
		 Total_CLM_PMT_AMT, 
     psp.[Risk Score],ErDuringStay, IpDuringStay, IpAfterDischarge,
		 STUFF((SELECT ','+ t.CUR_CLM_UNIQ_ID from #SNFClaimSummary t where t.LbPatientID = c.LbPatientID 
		 AND t.CLM_FROM_DT = c.CLM_FROM_DT for XML path('')),1,1,'') 
		 FROM #SNFClaimSummary c
     JOIN OrgHierarchy Org on c.LbPatientId = Org.LbPatientId
     join Patient_Summary_Pivot psp on psp.LbPatientId = Org.LbPatientId
     JOIN CCLF_1_PartA_Header a ON a.CUR_CLM_UNIQ_ID = c.Initial_CUR_CLM_UNIQ_ID
     left join DiagnosisGroup dg on a.PRNCPL_DGNS_CD = dg.DiagnosisCode
     left join NPI_Lookup npi on a.FAC_PRVDR_NPI_NUM = npi.NPI
     LEFT JOIN vwReference_MsDrgType DRG ON TRY_CONVERT(INT,a.DGNS_DRG_CD) = TRY_CONVERT(INT,DRG.DRG)
		 ORDER BY Org.LbPatientId 

END
GO
