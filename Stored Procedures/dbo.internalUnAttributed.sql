SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalUnAttributed] @NPI varchar(100) = '9999999999', @AttributionTypeId int = 99
AS
-- =====================================================================================
-- Author:		Mike Hoxter
-- Create date: 2015-12-18
-- Description:	set unattributed patient provider 
-- Modified By: Youping
-- Modified Date: 2016-04-21  add LbPatientId IS NOT NULL
-- Modified Date: 2017-01-25  adding 
-- ======================================================================================

BEGIN

DECLARE @ProviderId INT = ISNULL((select ProviderID FROM vwPortal_ProviderActive where NationalProviderIdentifier = @NPI),0)

	DELETE FROM vwPortal_PatientProvider WHERE AttributionTypeId = @AttributionTypeId

	INSERT INTO [dbo].[vwPortal_PatientProvider]
			   ([PatientID]
			   ,[ProviderID]
			   ,[SourceSystemID]
			   ,[ExternalReferenceIdentifier]
			   ,[OtherReferenceIdentifier]
			   ,[ProviderRoleTypeID]
			   ,[DeleteInd]
			   ,[CreateDateTime]
			   ,[ModifyDateTime]
			   ,[CreateLBUserId]
			   ,[ModifyLBUserId]
			   ,[AttributionTypeId]
			   )
	SELECT distinct pat.[PatientID]
			   ,@ProviderId [ProviderID]
			   ,1 [SourceSystemID]
			   ,PI.ExternalReferenceIdentifier [ExternalReferenceIdentifier]
			   ,@NPI [OtherReferenceIdentifier]
			   ,1 [ProviderRoleTypeID]
			   ,0 [DeleteInd]
				,getdate() [CreateDateTime]
				,getdate() [ModifyDateTime]
				,1 [CreateLBUserId]
				,1 [ModifyLBUserId]
			   ,@AttributionTypeId [AttributionTypeId]
	FROM [vwPortal_Patient] pat 
	JOIN vwPortal_Individual PI ON pat.IndividualID = PI.IndividualID
	WHERE pat.DeleteInd = 0 and PI.DeleteInd = 0 AND pat.[PatientID] IS NOT NULL

END
GO
