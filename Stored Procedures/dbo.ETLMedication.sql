SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ================================================================
-- Author:		Youping
-- Create date: 2016-04-04
-- Description:	Load Data into EDW Medication 
-- Modified By: Youping
-- Modified Date: 2016-05-09
-- Fix: remove filter len(code)>3
-- ===============================================================
--
-- Modified By: Youping
-- Modified Date: 2016-07-11
-- Fix:  NDCCode,RxNormCode duplications issue
--
-- Modified By: Youping
-- Modified Date: 2016-08-24
-- Update: for missing medication name using names from FDBMedication 
--

CREATE PROCEDURE [dbo].[ETLMedication]

AS
BEGIN	
		SET NOCOUNT ON;

		DECLARE @Today DATE;

		SET @Today=GETUTCDATE();
		


			---Load new medication
			--MERGE INTO Medication AS target
			--USING (SELECT DISTINCT
			--			ipm.MedicationCode,
			--			ipm.MedicationCodeName,
			--			ipm.MedicationName,
			--			mft.MedicationFormTypeId,
			--			mrt.MedicationRouteTypeId,
			--			ipm.MedicationDose
			--		FROM vwPortal_InterfacePatientMedication ipm
			--			INNER JOIN MedicationFormType mft ON mft.Name = ipm.MedicationForm
			--			INNER JOIN MedicationRouteType mrt ON mrt.Name = ipm.MedicationRoute
			--		WHERE ipm.CreateDateTime > @lastDateTime) AS source
			--ON target.NDCCode = source.MedicationCode OR target.RxNormCode = source.MedicationCode
			--WHEN NOT MATCHED BY TARGET THEN
			--INSERT (BrandName,GenericName,MedicationFormTypeID,MedicationRouteTypeID,MedicationDose,NDCCode,RxNormCode,
			--		RetiredInd,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
			--VALUES (source.MedicationName,source.MedicationName,source.MedicationFormTypeId,source.MedicationRouteTypeId,source.MedicationDose,
			--	(CASE WHEN source.MedicationCodeName LIKE '%NDC%' THEN source.MedicationCode ELSE NULL END),(CASE WHEN source.MedicationCodeName = '%RX%' THEN source.MedicationCode ELSE NULL END),
			--		0,0,GETUTCDATE(),GETUTCDATE(),1,1);

			SELECT DISTINCT
					    ISNULL(CASE WHEN LEFT(LTRIM(ipm.MedicationCodeName),2)='RX' THEN NULL ELSE ipm.MedicationCode END,'') AS NDCCode,
						ISNULL(CASE WHEN LEFT(LTRIM(ipm.MedicationCodeName),2)='RX' THEN ipm.MedicationCode ELSE NULL END,'') AS RxNormCode,
						ipm.MedicationCodeName,
						ipm.MedicationName,
						ISNULL(mft.MedicationFormTypeId,0) MedicationFormTypeId,
						ISNULL(mrt.MedicationRouteTypeId,0) MedicationRouteTypeId,
						ipm.MedicationDose
                into #Medication
             
					FROM --vwPortal_InterfacePatientMedication ipm
					    [dbo].[PatientMedicationProcessQueue] ipm
						Left JOIN MedicationFormType mft ON mft.Name = ipm.MedicationForm
						Left JOIN MedicationRouteType mrt ON mrt.Name = ipm.MedicationRoute
					
					--WHERE LEN(ISNULL(ipm.MedicationCode,''))>3 
                    WHERE LEN(ISNULL(ipm.MedicationCode,''))>0

            CREATE CLUSTERED INDEX IDX_Medication ON #Medication (NDCCode,RxNormCode);
			-- dedup NDCCode,RxNormCode
			SELECT *
			INTO #Medication2
			FROM (
			select *,
			ROW_NUMBER() OVER(PARTITION BY NDCCode,RxNormCode ORDER BY  MedicationDose  ) AS rowNbr
			from #Medication
			) M
			WHERE roWNbr=1;

		   ---Load new medication
		   CREATE CLUSTERED INDEX IDX_Medication2 ON #Medication2 (NDCCode,RxNormCode);

		   INSERT INTO Medication (BrandName,GenericName,MedicationFormTypeID,MedicationRouteTypeID,MedicationDose,NDCCode,RxNormCode,
					RetiredInd,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
     		SELECT m.MedicationName,m.MedicationName,m.MedicationFormTypeId,m.MedicationRouteTypeId,m.MedicationDose,
				m.NDCCode ,m.RxNormCode,0,0,@Today,@Today,1,1
			FROM #Medication m
			left outer join Medication M0 ON m0.NDCCode=m.NDCCode and m0.RxNormCode=m.RxNormCode
			WHERE m0.[MedicationID] is null

			-- for missing Genericname update names from FdbMedication in [EdwReferenceData]
			update [dbo].[Medication]
				set [BrandName]=[MedicationBrandName],
				[GenericName]=[MedicationLabelName]
				from  [dbo].[Medication] m
				inner join [EdwReferenceData].[dbo].[FdbMedication] f on f.ndc=m.NDCCode
				where isnull(m.GenericName,'')= ''

END
GO
