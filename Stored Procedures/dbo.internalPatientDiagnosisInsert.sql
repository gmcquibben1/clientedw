SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalPatientDiagnosisInsert]
	@LbPatientID INT
  ,@SourceSystemID INT = -1
  ,@DiagnosisCodingSystemTypeId INT = -1
	,@DiagnosisCode VARCHAR(255)
	,@DiagnosisDateTime datetime
	,@DiagnosisComment VARCHAR(255) = NULL
	,@DiagnosisDescription VARCHAR(255) = ''
AS


	
--LbPatientID, Diagnosis code, Source System ID, Diagnosis DateTime, DiagnosisComment, DiagnosisDescription

--DECLARE @LbPatientID INT = '2824'
--,@SourceSystemID INT = 2
--,@DiagnosisCodingSystemTypeId INT = NULL
--,@DiagnosisCode VARCHAR(255) = 'xyz'
--,@DiagnosisDateTime datetime = '2012-07-30 00:00:00.000'
--,@DiagnosisComment VARCHAR(255) = ''
--,@DiagnosisDescription VARCHAR(255) = 'test'
--,@PatientDiagnosisId INT = NULL
--,@DiagnosisCodeId int = NULL

--add @SourceSystemID
--add @DiagnosisCodingSystemTypeId

declare @PatientDiagnosisId INT = NULL
,@DiagnosisCodeId int = NULL


IF @SourceSystemID = -1
BEGIN
	SET @SourceSystemID = 1
END

IF @DiagnosisCodingSystemTypeId = -1
BEGIN
	SET @DiagnosisCodingSystemTypeId = 1
END

BEGIN

--internalProcedureCodeInsert 


	--DECLARE @PatientDiagnosisId INT ,@DiagnosisCodeId int
	
	IF NOT EXISTS
	(
		SELECT DISTINCT pd.[PatientDiagnosisId]
		FROM [dbo].[PatientDiagnosis] pd 
		INNER JOIN [dbo].[PatientDiagnosisDiagnosisCode] pddc ON pd.[PatientDiagnosisId] = pddc.[PatientDiagnosisId]
		INNER JOIN [dbo].[DiagnosisCode] dc on pddc.DiagnosisCodeId = dc.DiagnosisCodeId
		where LTRIM(RTRIM(REPLACE(DiagnosisCodeRaw,'.',''))) = LTRIM(RTRIM(REPLACE(@DiagnosisCode,'.','')))--dc.[DiagnosisCode] = @DiagnosisCode
		and pd.patientID = @LbPatientID
		and pd.[DiagnosisDateTime] = @DiagnosisDateTime
		and pd.sourcesystemID = @SourceSystemID

	)

	BEGIN


	--/*
--    @DiagnosisCodeId INT OUTPUT ,
--	@DiagnosisCodingSystemTypeId INT ,
--	@DiagnosisCode VARCHAR(255)  ,
--	@DiagnosisDiagnosisDescription VARCHAR(255) 

		EXEC internalDiagnosisCodeInsert 
			@DiagnosisCodeId OUTPUT , 
			@DiagnosisCodingSystemTypeId, 
			@DiagnosisCode,
			@DiagnosisDescription 

			--print @PROC_CodeID

		--*/

		--*********************************************************************************
		--insert into [PatientDiagnosis] table
		--/*		

		SET NOCOUNT ON
			SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
			DECLARE @intError INTEGER, 
					@DeleteInd BIT = 0,
					@LBUserId INT = 1

			BEGIN TRAN

			INSERT INTO [dbo].PatientDiagnosis
			(
				[PatientID]
				,sourcesystemID
				,[DiagnosisDateTime]
				,[DeleteInd]
				,[CreateDateTime]
				,[ModifyDateTime]
				,[CreateLBUserId]
				,[ModifyLBUserId]
			)
			VALUES 
			(
				@LbPatientID
				,@SourceSystemID
				,@DiagnosisDateTime
				,@DeleteInd
				,GetDate()
				,GetDate()
				,@LBUserId
				,@LBUserId
			)
			SELECT @intError = @@ERROR,
			@PatientDiagnosisId = SCOPE_IDENTITY()

			IF @@ERROR <> 0 
			BEGIN
				ROLLBACK TRAN
				RETURN @intError
			END
		--*/

		--*********************************************************************************
		--insert into [PatientProcedureProcedureCode] table
		--internalPatientProcedureProcedureCodeInsert

		--/*
		DECLARE @PatientDiagnosisDiagnosisCodeId INT

			INSERT INTO [dbo].PatientDiagnosisDiagnosisCode
				(
				[PatientDiagnosisId]
				,[DiagnosisCodingSystemTypeId]
				,[DiagnosisCodeId]
				)
				VALUES 
				(
					@PatientDiagnosisId
					,@DiagnosisCodingSystemTypeId
					,@DiagnosisCodeId
				)
				SELECT @intError = @@ERROR,
					@PatientDiagnosisDiagnosisCodeId = SCOPE_IDENTITY()

			IF @@ERROR <> 0 
			BEGIN
				ROLLBACK TRAN
			END
			ELSE
			BEGIN
				COMMIT TRAN
			END
		
			RETURN @intError

		--*/
	END --IF NOT EXIST
END
GO
