SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ClinicalDecisionPatientImmunizationsLoad]
@PatientID INT
AS
 
--This stored procedure will return all the anonymous vaccination history information for the clinical decisions 
--Evaluation Request
--LBPP - 1463 - Introduced in version 2.01
--CJL - 02.17.2016  -- First Cut
SELECT
i.PatientImmunizationId, i.PatientId, i.ServiceDate, ic.ImmunizationCode, ic.ImmunizationDescription, icst.Name as 'CodingSystem'
FROM 
	PatientImmunization i WITH (NOLOCK), 
	ImmunizationCode ic WITH (NOLOCK) , 
	ImmunizationCodingSystemType icst WITH (NOLOCK)
WHERE
i.patientId = @PatientID AND i.immunizationcodeid = ic.immunizationcodeid AND ic.ImmunizationCodingSystemTypeId = icst.ImmunizationCodingSystemTypeID
and i.DeleteInd = 0 

GO
