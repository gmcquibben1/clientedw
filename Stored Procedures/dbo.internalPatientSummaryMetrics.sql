SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPatientSummaryMetrics]
/*======================================================================================================================
	CREATED BY: 	Mike Hoxter
	CREATED ON:		2013-12-01
	INITIAL VER:	Initial Application Version
	MODULE:			(Optional) Module it applies to 		
	DESCRIPTION:	This SP build PatientSummary and Patient_Summary_Pivot tables.
	PARAMETERS:		(Optional) All non-obvious parameters must be described
	RETURN VALUE(s)/OUTPUT:	(Optional) Especially when returning specific values or error codes.
	PROGRAMMIN NOTES: (Optional) This is sometimes helpful to explain some implementation details that may not be obvious.
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=====================	
	2.1.2		2016-11-09		Lan			LBAN-3286	Use EDWReferenceData.dbo.PatientPivotMastList to add all the metric 
	                                                    columns for Patient_Summary_Pivot table.
    2.3.0       2017-01-04      Lan         LBAN-3374   Use System Setting to get the month offset for all metrics' yearly 
	                                                    calculation time frame, its default is 4 months
														Change '12 Mo Cost' to be source from 'Total Cost w/out Pharmacy' 
														instead of from 'Total Cost w/ Pharmacy'
============================================================================================================================*/
AS
BEGIN


SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientSummary';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalPatientSummaryMetrics';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();
	
	DECLARE @monthOffset INT = ISNULL(TRY_CONVERT(int,(SELECT SetValue FROM dbo.vwPortal_SystemSettings WHERE SettingType = 'AnalyticsSetting' and SettingParameter = '12 Month Offset (Months)')),4)
	
	--convert the begin date and end date to be first day of the month since DOS already converted to the first day of its service month
	DECLARE @beginDate DATE = (SELECT DATEADD(mm,DATEDIFF(mm, 0, GETDATE())-(@monthOffset+11),0))

	DECLARE @endDate DATE = (SELECT DATEADD(mm,DATEDIFF(mm, 0, GETDATE())- @monthOffset,0))


BEGIN TRY 


--truncate table PatientSummary
TRUNCATE TABLE PatientSummary;

/*--add last PCP Date of Service
INSERT INTO PatientSummary (
LbPatientId
,SUM_TYPE
,SUM_DETAIL
) SELECT 
LbPatientId
,'Last Visit Date'
,LAST_DOS 
FROM CCLF_WELLNESS_AGING

*/
--add yearly financials
INSERT INTO PatientSummary (
SUM_TYPE
,LbPatientId
,SUM_DETAIL
) 
SELECT CAST(DatePart("Year",DOS_First) as varchar(4))+' Cost' AS SUM_TYPE, LbPatientId, sum([Total Cost w/ Pharmacy]) 
FROM MM_Metics_Pivot
--WHERE DOS_First > DATEADD(mm,-16,GETUTCDATE()) and DOS_First < DATEADD(mm,-4,GETUTCDATE()) 
GROUP BY CAST(DatePart("Year",DOS_First) as varchar(4))+' Cost', LbPatientId

--add last 4-16 months financials
INSERT INTO PatientSummary (
SUM_TYPE
,LbPatientId
,SUM_DETAIL
) 
SELECT '12 Mo Cost' AS SUM_TYPE, LbPatientId, 
CASE 
WHEN sum([Total Cost w/out Pharmacy]) > 0 THEN sum([Total Cost w/out Pharmacy])
else 0
END
FROM MM_Metics_Pivot
--WHERE DOS_First > DATEADD(mm,-16,GETUTCDATE()) and DOS_First < DATEADD(mm,-4,GETUTCDATE()) 
WHERE DOS_First BETWEEN @beginDate AND @endDate
GROUP BY LbPatientId

--add last 2012 financials
INSERT INTO PatientSummary (
SUM_TYPE
,LbPatientId
,SUM_DETAIL
) 
/* ACO_Measure_Detail is depricated, moving to CMeasure_Detail where HighLow = 1
SELECT 'Measure Opps', PatientMemberID, COUNT FROM ACO_Measure_Detail
WHERE [Value] = 0
GROUP BY PatientMemberID
*/
SELECT 'Measure Opps', o.LbPatientId, COUNT(*) FROM OrgHierarchy o 
left join CMeasure_Detail C on o.LbPatientId = C.LbPatientID and  C.Numerator = 0
left JOIN CMeasure_Definitions CD ON C.MeasureID = CD.MeasureID AND CD.HighLow = 1
GROUP BY o.LbPatientId


--get default risk type setting
DECLARE @DefaultRisk varchar(100) = ISNULL((select SetValue FROM vwPortal_SystemSettings where SettingType = 'AnalyticsSetting' and SettingParameter = 'DefaultRisk'), 'JHACG')

--add if JHACG is risk type
if @DefaultRisk = 'JHACG'
BEGIN
--Add Risk Scores
IF OBJECT_ID('[Risk Scores]') IS NOT NULL
BEGIN
if (select count(*) from [Risk Scores]) > 1 
BEGIN
INSERT INTO PatientSummary (SUM_TYPE, LbPatientId, SUM_DETAIL) 
SELECT 'Risk Score', LbPatientId , [Risk Score] 
FROM [Risk Scores] -- [Risk Scores] exists, [JhacgPatientDetail] does not exist 
END
END
ELSE
BEGIN
IF OBJECT_ID('[JhacgPatientDetail]') IS NOT NULL --changed to use JHACG11 table JhacgPatientDetail
INSERT INTO PatientSummary (SUM_TYPE, LbPatientId, SUM_DETAIL) 
SELECT 'Risk Score', TRY_CONVERT(int, Patient_Id), rescaled_total_cost_pred_risk--rescaled_total_cost_resource_index 
FROM JhacgPatientDetail -- [Risk Scores] does not exist, [JhacgPatientDetail] does exist
ELSE
INSERT INTO PatientSummary (SUM_TYPE, LbPatientId, SUM_DETAIL) 
SELECT 'Risk Score', LbPatientId, 1 
FROM OrgHierarchy -- [Risk Scores] does not exist, [JhacgPatientDetail] does exist
END
END

IF @DefaultRisk like 'HCC%'
BEGIN
DECLARE @HccYear int = right(@DefaultRisk, 4)
INSERT INTO PatientSummary (
   SUM_TYPE
  ,LbPatientId
  ,SUM_DETAIL
) 
SELECT 'Risk Score', LbPatientId, RiskAdjustmentFactor FROM HccPatHistory
WHERE [Year] = @HccYear
END

--Resource_Utilization section
IF OBJECT_ID('[JhacgPatientDetail]') IS NOT NULL
INSERT INTO PatientSummary (SUM_TYPE, LbPatientId, SUM_DETAIL)
SELECT 'Resource_Utilization', TRY_CONVERT(int, Patient_Id), ISNULL([resource_utilization_band], 0)
FROM JhacgPatientDetail -- [JhacgPatientDetail] does exist

--Make Chronic Designations
--adding icd9 descriptions
INSERT INTO PatientSummary (SUM_TYPE, LbPatientId, SUM_DETAIL)
SELECT I.[Description], IDR.LbPatientId, 1 AS SUMDETAIL
--FROM PatientProcedure PP 
--JOIN PatientProcedureProcedureCode PPPC ON PP.PatientProcedureId = PPPC.PatientProcedureId
--JOIN ProcedureCode PC ON PPPC.ProcedureCodeId = PC.ProcedureCodeId
FROM PatientDiagnosis PD
JOIN dbo.PatientDiagnosisDiagnosisCode PDDC ON PDDC.PatientDiagnosisId = PD.PatientDiagnosisId
JOIN DiagnosisCode DC ON PDDC.DiagnosisCodeId = DC.DiagnosisCodeId
JOIN Ref_ChronicICD9s I ON DC.DiagnosisCode = I.[ICD9]
JOIN PatientIdReference IDR ON PD.PatientID = IDR.LbPatientId 
--WHERE PD.DiagnosisDateTime > DATEADD(MM, -15, GetDate()) 
WHERE DiagnosisDateTime BETWEEN @beginDate AND @endDate
AND IDR.LbPatientId IN (SELECT LbPatientId FROM OrgHierarchy)
GROUP BY I.[Description], IDR.LbPatientId
ORDER BY IDR.LbPatientId


--add ESRD based on additional logic
SELECT DISTINCT A.LbPatientId 
INTO #ESRD_TEMP
FROM CCLF_1_PartA_Header A 
JOIN CCLF_2_PartA_RCDetail rcd ON A.CUR_CLM_UNIQ_ID = rcd.CUR_CLM_UNIQ_ID
WHERE TRY_CONVERT(INT,rcd.CLM_LINE_REV_CTR_CD) IN (800,801,802,803,804,809,820,821,822,823,824,825,829,830,831,832,833,834,835,839,840,841,842,843,844,845,849,850,851,852,853,854,855,859,880,881,882,889) OR 
(CLM_BILL_FAC_TYPE_CD = '7' and CLM_BILL_CLSFCTN_CD = '2')
OR 
rcd.CLM_LINE_HCPCS_CD IN ('90935','90937','90940','90945','90947','90951','90952','90953','90954','90955',
'90956','90957','90958','90959','90960','90961','90962','90963','90964','90965','90966','90967','90968','90969','90970','90989',
'90993','90997','90999','93990','90993') 
OR (CLM_BILL_FAC_TYPE_CD = '7' and CLM_BILL_CLSFCTN_CD = '2')

--insert based on cpts
INSERT INTO #ESRD_TEMP (LbPatientId)
SELECT DISTINCT LbPatientId
FROM CCLF_5_PartB_Physicians
where CLM_LINE_HCPCS_CD IN ('90935','90937','90940','90945','90947','90951','90952','90953','90954','90955',
'90956','90957','90958','90959','90960','90961','90962','90963','90964','90965','90966','90967','90968','90969','90970','90989',
'90993','90997','90999','93990','90993') 
and CLM_FROM_DT > DATEADD(MM, -15, GetDate())

INSERT INTO PatientSummary (
   LbPatientId
  ,SUM_TYPE
  ,SUM_DETAIL
) 
SELECT E.LbPatientId, 'ESRD', 1 FROM #ESRD_TEMP E
LEFT JOIN PatientSummary PS ON E.LbPatientId = PS.LbPatientId AND SUM_TYPE = 'ESRD'
WHERE PS.id IS NULL

DROP TABLE #ESRD_TEMP

--Insert ATI Score (by: Mike Hoxter 9/16/2014)
IF OBJECT_ID('dbo.ATI_PatFactor') IS NOT NULL
INSERT INTO PatientSummary (
SUM_TYPE
,LbPatientId
,SUM_DETAIL
) 
SELECT 'ATI', dbo.ATI_PatFactor.LbPatientId, ATIScore FROM ATI_PatFactor JOIN OrgHierarchy ON dbo.ATI_PatFactor.LbPatientId = dbo.OrgHierarchy.LbPatientId

--Insert lastest MedicareStatus
IF OBJECT_ID('dbo.CCLF_8_BeneDemo') IS NOT NULL
INSERT INTO PatientSummary (
SUM_TYPE
,LbPatientId
,SUM_DETAIL
) 
SELECT DISTINCT 'MedicareStatus', LbPatientId, TRY_CONVERT(INT,[BENE_MDCR_STUS_CD]) FROM CCLF_8_BeneDemo

--INSERT DUAL STATUS CODES FROM CCLF_8
IF OBJECT_ID('dbo.CCLF_8_BeneDemo') IS NOT NULL
INSERT INTO PatientSummary (
SUM_TYPE
,LbPatientId
,SUM_DETAIL
) 
SELECT DISTINCT 'DualStatus', LbPatientId, TRY_CONVERT(INT, [BENE_DUAL_STUS_CD]) FROM CCLF_8_BeneDemo

--add Inpatient Re-Admits
INSERT INTO PatientSummary (LbPatientId, SUM_TYPE, SUM_DETAIL)
SELECT LbPatientId, 'IP Re-Admits', sum([Inpatient Re-Admits]) FROM MM_Metics_Pivot
--where DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, DOS_First, GETDATE()) -15 , DOS_First)
WHERE DOS_First BETWEEN @beginDate AND @endDate
group by LbPatientId

--add Part-A
INSERT INTO PatientSummary (LbPatientId, SUM_TYPE, SUM_DETAIL)
SELECT LbPatientId, 'Part-A Cost', sum([Part-A Cost]) FROM MM_Metics_Pivot
--where DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, DOS_First, GETDATE()) -15 , DOS_First)
WHERE DOS_First BETWEEN @beginDate AND @endDate
group by LbPatientId

--add Part-B
INSERT INTO PatientSummary (LbPatientId, SUM_TYPE, SUM_DETAIL)
SELECT LbPatientId, 'Part-B Cost', sum([Part-B Cost]) FROM MM_Metics_Pivot
--where DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, DOS_First, GETDATE()) -15 , DOS_First)
WHERE DOS_First BETWEEN @beginDate AND @endDate
group by LbPatientId

--add Pharmacy cost
INSERT INTO PatientSummary (LbPatientId, SUM_TYPE, SUM_DETAIL)
SELECT LbPatientId, 'Pharmacy Cost', sum([Pharmacy Cost]) FROM MM_Metics_Pivot
--where DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, DOS_First, GETDATE()) -15 , DOS_First)
WHERE DOS_First BETWEEN @beginDate AND @endDate
group by LbPatientId

--add DME cost
INSERT INTO PatientSummary (LbPatientId, SUM_TYPE, SUM_DETAIL)
SELECT LbPatientId, 'DME Cost', sum([DME Cost]) FROM MM_Metics_Pivot
--where DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, DOS_First, GETDATE()) -15 , DOS_First)
WHERE DOS_First BETWEEN @beginDate AND @endDate
group by LbPatientId
--add ER Visits
INSERT INTO PatientSummary (LbPatientId, SUM_TYPE, SUM_DETAIL)
SELECT LbPatientId, 'ER Visits', sum([ER Visits]) FROM MM_Metics_Pivot
--where DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, DOS_First, GETDATE()) -15 , DOS_First)
WHERE DOS_First BETWEEN @beginDate AND @endDate
group by LbPatientId

--add IP Admits
INSERT INTO PatientSummary (LbPatientId, SUM_TYPE, SUM_DETAIL)
SELECT LbPatientId, 'IP Admits', sum([Inpatient Admits]) FROM MM_Metics_Pivot
--where DOS_First > DATEADD(MONTH, DATEDIFF(MONTH, DOS_First, GETDATE()) -15 , DOS_First)
WHERE DOS_First BETWEEN @beginDate AND @endDate
group by LbPatientId

--insert dummy patient record into patient summary W/ default values
/*
insert into PatientSummary (
   LbPatientId
  ,SUM_TYPE
  ,SUM_DETAIL
) VALUES ('123456789','2014 Cost',1),('123456789','2015 Cost',1),('123456789','12 Mo Cost',1),('123456789','Measure Opps',1),
('123456789','Risk Score',1),('123456789','ATI',1),('123456789','MedicareStatus',1),
('123456789','DualStatus',1),('123456789','IP Re-Admits',1),('123456789','Part-A Cost',1),
('123456789','Part-B Cost',1),('123456789','Pharmacy Cost',1),
('123456789','DME Cost',1),('123456789','ER Visits',1),('123456789','IP Admits',1 )
,('123456789','COPD',1 ),('123456789','Asthma',1 ),('123456789','CHF',1 )
,('123456789','Diabetes',1 ),('123456789','Hypertension',1 ),('123456789','Oncology',1 )
,('123456789','Mental Health',1 ),('123456789','Chronic Pain',1 )
*/
--replace the above part so no code change needed if new metric added to this table
INSERT INTO [dbo].PatientSummary (LbPatientId, SUM_TYPE, SUM_DETAIL)
SELECT DISTINCT '123456789', SUM_TYPE, 1
FROM EdwReferenceData..[PatientPivotMetricsList]
WHERE SUM_TYPE NOT IN ( SELECT DISTINCT SUM_TYPE FROM  [dbo].PatientSummary)


--make pivot table
IF OBJECT_ID('dbo.Patient_Summary_Pivot', 'U') IS NOT NULL
DROP TABLE dbo.Patient_Summary_Pivot
--declaring varialbes
IF OBJECT_ID('dbo.Patient_Summary_Pivot', 'U') IS NOT NULL
  DROP TABLE dbo.Patient_Summary_Pivot
  
  --declaring varialbes
DECLARE @colBottom NVARCHAR(MAX), @colTop NVARCHAR(MAX), @SQL NVARCHAR(MAX), @createTable NVARCHAR(MAX), @colTable NVARCHAR(MAX)

--concatenating the distinct items of column title in one row using FOR XML PATH
SET @colTable = STUFF((SELECT DISTINCT ', [' + CAST(SUM_TYPE AS VARCHAR(200)) + ']' + ' INT' FROM PatientSummary FOR XML PATH('')),1,1, N'');
SET @colBottom = STUFF((SELECT DISTINCT ', [' + CAST(SUM_TYPE AS VARCHAR(200)) + ']' FROM PatientSummary FOR XML PATH('')),1,1, N'');
SET @colTop = STUFF((SELECT DISTINCT ',ISNULL([' + SUM_TYPE + '], 0) AS ' + '['+CAST(SUM_TYPE AS VARCHAR(200))+']' FROM PatientSummary FOR XML PATH('')),1,1, N'')

--creating a table
SET @createTable = N'Create Table Patient_Summary_Pivot ( LbPatientId VARCHAR(12), ' + @colTable + ') '

--populating the records in a variable and global temp table
SET @SQL = '(SELECT LbPatientId, '+@colTop+' INTO Patient_Summary_Pivot FROM 
			(SELECT DISTINCT LbPatientId, SUM_TYPE, SUM_DETAIL
			FROM PatientSummary) 
			 derivedTable
			PIVOT
			(SUM(SUM_DETAIL) FOR SUM_TYPE IN ('+@colBottom+')) pivotTable)
			ORDER BY LbPatientId DESC'

EXECUTE sp_executesql @sql

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'COPD')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD COPD Decimal(17,3);
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'Asthma')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD Asthma Decimal(17,3);
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'CHF')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD CHF Decimal(17,3);
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'Diabetes')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD Diabetes Decimal(17,3);
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'Hypertension')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD Hypertension Decimal(17,3);
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'ESRD')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD ESRD Decimal(17,3);
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'Oncology')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD Oncology Decimal(17,3);
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'Mental Health')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD [Mental Health] Decimal(17,3);
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'Chronic Pain')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD [Chronic Pain] Decimal(17,3);
	END

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'Resource_Utilization')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD [Resource_Utilization] Decimal(38,3);
	END
  
  	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = '2012 Cost')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD [2012 Cost] Decimal(17,3);
	END

    	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = '2013 Cost')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD [2013 Cost] Decimal(17,3);
	END

    	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = '2014 Cost')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD [2014 Cost] Decimal(17,3);
	END
  
    	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = '2015 Cost')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD [2015 Cost] Decimal(17,3);
	END

    	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = '2016 Cost')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD [2016 Cost] Decimal(17,3);
	END

    	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Patient_Summary_Pivot' AND COLUMN_NAME = 'Risk Score')
	BEGIN
		ALTER TABLE [Patient_Summary_Pivot] ADD [Risk Score] Decimal(17,3);
	END


--ADDED BY ISMAIL ON 05/20/15
--DECLARE @VALUELIST varchar(8000), @POS INT, @LEN INT, @VALUE varchar(8000), @SQL VARCHAR(MAX)
--SET @VALUELIST ='2014 Cost,12 Mo Cost,Measure Opps,Risk Score,ATI,MedicareStatus,DualStatus,IP Re-Admits,Part-A Cost,Part-B Cost,Pharmacy Cost,DME Cost,ER Visits,IP Admits'
--SET @POS = 0
--SET @LEN = 0
--WHILE CHARINDEX(',', @VALUELIST, @POS + 1) > 0
--BEGIN
--SET @LEN = CHARINDEX(',', @VALUELIST, @POS + 1) - @POS
--SET @VALUE = SUBSTRING(@VALUELIST, @POS, @LEN)
--SET @SQL = 'IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ''Patient_Summary_Pivot'' AND COLUMN_NAME = '''@VALUE''')
--BEGIN
--ALTER TABLE [Patient_Summary_Pivot] ADD ''['+''+@VALUE+''+']'' decimal(38, 3) NOT NULL DEFAULT(0)
--END'
--SET @POS = CHARINDEX(',', @VALUELIST, @POS + @LEN) + 1
--EXECUTE(@SQL)	
--END
--ALTER TABLE [dbo].[Patient_Summary_Pivot] ADD [ID] [INT] IDENTITY(1,1) PRIMARY KEY
--

		SELECT @InsertedRecord=count(1)
		FROM [PatientSummary] ;


	-----Log Information
		
	 
          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
