SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ETLFuzzyExternalIDMatch]
	 @MatchScoreMin int = 1
AS
/*=================================================================================================
	CREATED BY: 	Mike H
	CREATED ON:		2017-01-18
	INITIAL VER:	2.2.2
	MODULE:			Fuzzy Match Type for External ID/Source System ID	
	DESCRIPTION:   Each patient record where we have created a duplicate patient with the same External ID from the same 
                  Source System, we will create a fuzzy match result record to merge the patient information together
                  we will keep the largest (latest) version of the patient in this case.
	MODIFICATIONS
	Version     Date            Author		  JIRA  		Change
	=======     ==========      ======      =========	=========	
  2.2.2       1/24/2017         MH        LBAN-3490   Initial Version
  2.2.2       03.07.2017		CJL		  LBAN-3490   Added logging, removed hardcoded value type of 3, handled issues in which duplicate records would be inserted


====================================================================================================*/



	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'ETLFuzzyExternalIDMatch';
	DECLARE @dataSource VARCHAR(20) = 'EDW';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLFuzzyExternalIDMatch';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @NUMROWS INT

	
	BEGIN TRY



	

		DECLARE @FuzzyMatchTypeId int = (SELECT FuzzyMatchTypeId FROM FuzzyMatchType WHERE SPName = 'ETLFuzzyExternalIDMatch')  
		
		-- get total records from PatientMedication after merge 
		SELECT @RecordCountBefore= (SELECT COUNT(1)  FROM FuzzyMatchResult WITH (NOLOCK));

		--GATHER POENTIAL PATIENTS FROM MemberExtract
		SELECT medicareNo, SourceSystemId, COUNT(DISTINCT LbPatientId) AS [DISTINCT IDs]
		INTO #MultipleIdList
		FROM MemberExtract ME
		GROUP BY medicareNo, SourceSystemId
		HAVING COUNT(DISTINCT LbPatientId) > 1

		
		--Insert the values into a staging table, we are going to do a join against this staging table to prevent
		--duplicate values into the Fuzzy Match result able.
		SELECT @FuzzyMatchTypeId as FuzzyMatchTypeId, 
		-- inPid is smaller than matchedPid
			matched.FirstName as inFirstName, matched.LastName as inLastName, matched.LbPatientId as inPid,
				 matched.MedicareNo as inExternalId, matched.SourceSystemId as inSourceSystemId, 
				PIN.LbPatientId as matchedPid, PIN.FirstName as matchedFirstName, PIN.LastName as MatchedLastName, 
				PIN.birthDate as matchedDOB, PIN.SourceSystemId as matchedSourceSystemId,  PIN.sex as Gender, PIN.MedicareNo as ExternalId, 
				.95 as SimilarityScore, 0 as isMerged
			INTO #FuzzyMatchStaging
		FROM (
				SELECT 
				 ME.LbPatientId, ME.firstName, ME.lastName, ME.birthDate, ME.SourceSystemId,ME.sex,  ME.medicareNo,
				 ROW_NUMBER() OVER (PARTITION BY ME.medicareNo, ME.SourceSystemId ORDER BY ME.LbPatientId DESC) MaxRank
				FROM MemberExtract ME
				JOIN #MultipleIdList MIL 
					  ON ME.medicareNo = MIL.medicareNo 
					  AND ME.SourceSystemId = MIL.SourceSystemId) 
				PIN
		 JOIN 
				 (SELECT 
				 ME.firstName, ME.lastName, ME.birthDate, ME.sex, ME.LbPatientId, ME.medicareNo, ME.SourceSystemId, 
				 ROW_NUMBER() OVER (PARTITION BY ME.medicareNo, ME.SourceSystemId ORDER BY ME.LbPatientId ASC) MinRank
				FROM MemberExtract ME
				JOIN #MultipleIdList MIL 
					  ON ME.medicareNo = MIL.medicareNo 
					  AND ME.SourceSystemId = MIL.SourceSystemId
				) matched
		  ON PIN.MedicareNo = matched.MedicareNo and PIN.SourceSystemId = matched.SourceSystemId
		  WHERE PIN.MaxRank = 1 and matched.MinRank = 1



		  --Insert new values into the table, exlcuding any values that already exist
		  INSERT INTO FuzzyMatchResult (
		   FuzzyMatchTypeId
		  ,inFirstName
		  ,inLastName
		  ,inPid
		  ,inExternalId
		  ,inSourceSystemId
		  ,matchedPid
		  ,matchedFirstName
		  ,MatchedLastName
		  ,matchedDOB
		  ,matchedSourceSystemId
		  ,Gender
		  ,ExternalId
		  ,similarityScore
		  ,isMerged
		) 
		(SELECT 
			s.FuzzyMatchTypeId
		  ,s.inFirstName
		  ,s.inLastName
		  ,s.inPid
		  ,s.inExternalId
		  ,s.inSourceSystemId
		  ,s.matchedPid
		  ,s.matchedFirstName
		  ,s.MatchedLastName
		  ,s.matchedDOB
		  ,s.matchedSourceSystemId
		  ,s.Gender
		  ,s.ExternalId
		  ,s.similarityScore
		  ,s.isMerged
		  FROM 	#FuzzyMatchStaging s
		  LEFT JOIN FuzzyMatchResult mr WITH (NOLOCK) ON mr.matchedPid =  s.matchedPid AND mr.inPid =  s.inPid AND s.FuzzyMatchTypeId = @FuzzyMatchTypeId AND mr.FuzzyMatchTypeId = @FuzzyMatchTypeId
		  WHERE mr.matchedPid IS NULL
		)



		-- update maxSourceTimeStamp with max CreateDateTime of #PatientMedicationQue
		UPDATE [Maintenance]..[EDWTableLoadTracking]
		SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex, updatedatetime = GETUTCDATE()
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] =@dataSource  AND [EDWName] = @EDWName


		-- get total records from PatientMedication after merge 
		SELECT @RecordCountAfter= (SELECT COUNT(1)  FROM FuzzyMatchResult WITH (NOLOCK));

		-- Calculate records inserted and updated counts
		SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
		SET @UpdatedRecord = 0

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;



	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH



GO
