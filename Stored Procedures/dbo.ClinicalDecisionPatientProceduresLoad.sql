SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ClinicalDecisionPatientProceduresLoad]
@PatientID INT
AS 


--This stored procedure will return all the anonymous medical procedure information for the clinical decisions 
--Evaluation Request
--LBPP - 1463 - Introduced in version 2.01
--CJL - 02.17.2016  -- First Cut


SELECT 
	pcst.Name as 'CodingSystemName', pc.ProcedureCode, pc.ProcedureDescription,  pp.ProcedureDateTime, pc.ProcedureCodeId, pp.PatientProcedureId
FROM 
	PatientProcedureProcedureCode pppc WITH (NOLOCK), 
	ProcedureCode pc WITH (NOLOCK), 
	PatientProcedure pp WITH (NOLOCK), 
	ProcedureCodingSystemType pcst WITH (NOLOCK)
WHERE
	pcst.ProcedureCodingSystemTypeID = pc.ProcedureCodingSystemTypeId
	AND pppc.ProcedureCodeId = pc.ProcedureCodeId
	AND pp.PatientProcedureId = pppc.PatientProcedureId 
	AND pp.PatientID = @PatientId 
	ORDER by pp.PatientProcedureId

GO
