SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



Create PROCEDURE [dbo].[ETLPatientImmunization]
	@fullLoadFlag bit = 0, 
	@BatchSize INT = 10000, 
	@InterfacePatientId VARCHAR(50) = NULL,
	@overrideMinIndex INT = NULL
AS

  /*===============================================================
	CREATED BY: 	Youping
	CREATED ON:		2016-06-22
	INITIAL VER:	2.0.1
	MODULE:			ETL
	DESCRIPTION:	Load Interface PatientImmunization Data into EDW
	PARAMETERS:		
		@fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
		 @fullLoadFlag =0 Merge date to the existing table
		 @BatchSize  Number of patients to do in each batch
		 @InterfacePatientId    Id of the patient identifier if we want to run this for a specific patietn
		 @overrideMinIndex		Minimum id of the InterfacePatientImmunization record to start processing from. Skips the
								Value stored in the [Maintenance].[dbo].[EDWTableLoadTracking] table

	RETURN VALUE(s)/OUTPUT:	
		Error and logging information will be written to the [EDWTableLoadTrackingInsert] table

	
	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.0.1		2016-06-22		Youping					Created
	2.1.2       2016-07-28      YL						 Add procedure running log to [Maintenance].[dbo].[EdwProcedureRunLog]
	2.1.2       2016-07-16      YL						change ISNULL([ImmunizationDescription],'') 
	2.1.2		2016-12-28		CL			LBETL-358    OVER() window function in ETLPatientImmunization does not take NULL into account for ServiceDate
	2.1.2		2017-01-11		CL			LBETL-358	 Corrected issue with missed ISNULL() Service date.
	2.1.2		2017-02-06		CJL			LBETL-634	Added interpolation code to attempt to detemine the actual immunization
													    code taxononmy based on the format of the Immunization code

														 

=================================================================*/

BEGIN	
 
    SET NOCOUNT OFF;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'PatientImmunization';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientImmunization';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @RowCount INT =0;
	DECLARE @NumRows INT = 0;
 	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);

BEGIN TRY
	--DECLARE @fullLoadFlag bit = 1, 
	--@BatchSize INT = 10000, 
	--@InterfacePatientId VARCHAR(50) = NULL,
	--@overrideMinIndex INT = NULL


  		
				--set @fullLoadFlag =1
	
		--1) Retrieve the last run time id values.  We are going to pull both the date and the last run time id. If the last max id 
		--is null, calculate a best guese based on the 
		--We are changing away from using date values for query purposes as it is more efficient to query by the Id value.  Since
		--the id is a seeded, integer greater id values are implicitly newer. It also allows us to pick up records that come in while the 
		--procedure is running the next time it runs
		IF @fullLoadFlag = 1
		BEGIN
			UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME), maxSourceIdProcessed= 0
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
		END

		 SET @lastDateTime = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	
	
		
		SET  @lastIndex = (SELECT maxSourceIdProcessed
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex= (SELECT MIN([InterfacePatientImmunizationId]) FROM  [vwPortal_InterfacePatientImmunization] IPS WITH (NOLOCK) WHERE IPS.CreateDateTime >= @lastDateTime ) 
		END

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex = 0 
		END


		if @overrideMinIndex IS NOT NULL
		BEGIN
			SET @lastIndex = @overrideMinIndex
		END 

		SET @maxIndex = (SELECT MAX([InterfacePatientImmunizationId]) FROM  [vwPortal_InterfacePatientImmunization] IPS WITH (NOLOCK));
	
		

		/*
		DROP TABLE #PatientImmunizationQue
		DROP TABLE #tmpPatientImmunization
		DROP TABLE #ProcedureCodingSystemType
		DROP TABLE #PatientImmunizationQuePatientIdentifieBATCH
		Drop table #PatientImmunizationQuePatientIdentifier
		DROP TABLE #PatientImmunization
		DROP TABLE #ImmunizationCodingSystemType
		DROP TABLE #ImmunizationCode
		*/


		--2) Get the full list of patient identifiers that we are going to be dealing with
		--We are going to breaking this up into batches along patient id lines.
		SELECT  DISTINCT 
			IP.PatientIdentifier, IP.InterfaceSystemId
		INTO #PatientImmunizationQuePatientIdentifier	
		FROM [dbo].[vwPortal_InterfacePatientImmunization] IPS with (nolock)
			INNER JOIN [dbo].[vwPortal_InterfacePatient] IP with (nolock) ON IPS.InterfacePatientId = IP.InterfacePatientId
			INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) ON IP.InterfaceSystemId = ISS.[InterfaceSystemId]
		WHERE ( IPS.[InterfacePatientImmunizationId] >= @lastIndex  AND IPS.[InterfacePatientImmunizationId] <= @maxIndex )
		 AND ( @InterfacePatientId IS NULL OR (IPS.InterfacePatientId = @InterfacePatientId))
			

					
				
		--- Truncate data
		IF (@fullLoadFlag=1)
		BEGIN
			Truncate table [dbo].PatientImmunization;
		END
	
	    SELECT @RecordCountBefore=COUNT(1)	FROM [dbo].PatientImmunization  WITH (NOLOCK);

	
		--3) Loop throught all of the batches to improve memory efficency
		WHILE 1 = 1
		BEGIN	



		
				SELECT TOP (@BatchSize) PatientIdentifier, InterfaceSystemId INTO #PatientImmunizationQuePatientIdentifieBATCH FROM #PatientImmunizationQuePatientIdentifier;
				Create  NONCLUSTERED INDEX IDX_PatientImmunizationQuePatientIdentifieBATCH_PatientIdentifier ON #PatientImmunizationQuePatientIdentifieBATCH (PatientIdentifier, InterfaceSystemId);
			
    				SET @NUMROWS = ( SELECT COUNT(1) FROM #PatientImmunizationQuePatientIdentifieBATCH)

				IF @NUMROWS <> 0 
				BEGIN


								SELECT * INTO #PatientImmunizationQue FROM (
								SELECT   IPI.[InterfacePatientImmunizationId]
										,IPI.[InterfacePatientID]
										,IP.PatientIdentifier
										, idRef.LbPatientId
										,ISS.SourceSystemId
										,[ServiceDate]
										,LTRIM(RTRIM([ImmunizationCode])) as [ImmunizationCode]
										,dbo.fnInterpolateVaccineTaxonomy(ImmunizationCode, ImmunizationCodeSystemName) as  [ImmunizationCodeSystemName]
										,[ImmunizationDescription]
										,[ImmunizationDose]
										,[ImmunizationUnits]
										,[MaterialLotNumber]
										,[MaterialManufacturer]
										,LTRIM(RTRIM([RouteCode])) [RouteCode]
										,[RouteCodeSystemName]
										,[RouteDescription]
										,[PerformingClinician]
										,IPI.[CreateDateTime]
										,IPI.EncounterIdentifier
										,[RenderingProviderNPI]
										--, ROW_NUMBER() OVER ( PARTITION BY ISS.SourceSystemId,IPI.[InterfacePatientID], ISNULL(ServiceDate, '1901-01-01'), [ImmunizationCode],
										-- dbo.fnInterpolateVaccineTaxonomy([ImmunizationCode], [ImmunizationCodeSystemName])
										--ORDER BY 	InterfacePatientImmunizationId DESC ) RowNbr
										FROM 
										 [dbo].[vwPortal_InterfacePatientImmunization] IPI with (nolock), 
										 [dbo].[vwPortal_InterfacePatient] IP with (nolock),
										 [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock),
						 				 [dbo].[PatientIdReference] idRef WITH (NOLOCK),
										 #PatientImmunizationQuePatientIdentifieBATCH batch WITH (NOLOCK)
										WHERE 
										 (IPI.[InterfacePatientImmunizationId] >= @lastIndex  AND IPI.[InterfacePatientImmunizationId] <= @maxIndex) 
										AND ISNULL(IPI.[ImmunizationCode],'')<> '' AND  IPI.InterfacePatientId = IP.InterfacePatientId
										AND IP.InterfaceSystemId = ISS.[InterfaceSystemId]
										AND IP.PatientIdentifier = batch.PatientIdentifier AND IP.InterfaceSystemId = batch.InterfaceSystemId
										AND idRef.SourceSystemId = ISS.SourceSystemId 
										AND idRef.ExternalId = batch.PatientIdentifier 
										AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
										AND idRef.DeleteInd =0				
		 						) A -- WHERE RowNbr=1;



								  --- add Index
								  CREATE  NONCLUSTERED INDEX IDX_PatientIdentifier_SourceSystemId ON #PatientImmunizationQue (PatientIdentifier,SourceSystemId);
								  CREATE  NONCLUSTERED INDEX IDX_ImmunizationCode_ImmunizationCodeSystemName ON #PatientImmunizationQue ([ImmunizationCode],[ImmunizationCodeSystemName]);
								  CREATE  NONCLUSTERED INDEX IDX_RouteCode ON #PatientImmunizationQue ([RouteCode]);
              
			

								  --get all ImmunizationCodingSystemName and ID into temp table
									SELECT
									ImmunizationCodingSystemTypeID,
									RefCodingSystemName
									INTO #ImmunizationCodingSystemType
									FROM(
										  SELECT
										  PCST.ImmunizationCodingSystemTypeID,
										  PCST.NAME As RefCodingSystemName
										  FROM ImmunizationCodingSystemType PCST 
										  UNION
										  SELECT 
										  PCST.ImmunizationCodingSystemTypeID,
										  PCSTS.SynonymName As RefCodingSystemName
										  FROM ImmunizationCodingSystemType PCST 
										  INNER JOIN ImmunizationCodingSystemTypeSynonyms PCSTS ON PCST.ImmunizationCodingSystemTypeID = PCSTS.ImmunizationCodingSystemTypeID
										) t

									-- test	select * from #ImmunizationCodingSystemType
         
									 SELECT * 
									 INTO #ImmunizationCode
									 FROM (
											SELECT 	t2.[ImmunizationCodingSystemTypeId],
											t1.[ImmunizationCode],
											t1.[ImmunizationDescription]
											, ROW_NUMBER() OVER ( PARTITION BY 	t2.[ImmunizationCodingSystemTypeId], t1.[ImmunizationCode]
													ORDER BY 	InterfacePatientImmunizationId DESC ) RowNbr
				  
									FROM #PatientImmunizationQue  t1
									INNER JOIN #ImmunizationCodingSystemType t2
									ON t1.[ImmunizationCodeSystemName] = t2.RefCodingSystemName
		
								) A WHERE RowNbr=1;

							  Create  CLUSTERED INDEX IDX_ImmunizationCodingSystemTypeIdImmunizationCode ON #ImmunizationCode (ImmunizationCodingSystemTypeId,ImmunizationCode);
           
								  ----Merge with [dbo].[ImmunizationCode]
								   INSERT INTO [dbo].[ImmunizationCode]
								   ([ImmunizationCodingSystemTypeId]
								   ,[ImmunizationCode]
								   ,[ImmunizationDescription]
								   ,[ImmunizationDescriptionAlternate]
								   ,[DeleteInd]
								   ,[CreateDateTime]
								   ,[ModifyDateTime]
								   ,[CreateLBUserId]
								   ,[ModifyLBUserId])

      
								SELECT 	t1.[ImmunizationCodingSystemTypeId]
										,t1.[ImmunizationCode]
										,t1.[ImmunizationDescription]
										,t1.[ImmunizationDescription]
										,0
										,@Today
										,@Today
										,1
										,1
								FROM #ImmunizationCode t1
								LEFT OUTER JOIN  [ImmunizationCode] t2
								ON t1.ImmunizationCodingSystemTypeId=t2.ImmunizationCodingSystemTypeId AND t1.[ImmunizationCode] = t2.ImmunizationCode
								WHERE t2.ImmunizationCodeId IS NULL   
		

						---   Insert new [RouteCode] INTO MedicationRouteType
							   MERGE INTO MedicationRouteType AS target
									USING (SELECT distinct [RouteCode]
										   FROM #PatientImmunizationQue 
										  where ISNULL([RouteCode],'')<> ''  ) AS source
									ON source.[RouteCode] = target.[Name]
									WHEN NOT MATCHED BY TARGET THEN
										INSERT ([Name],DisplayValue,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
										VALUES (source.[RouteCode],source.[RouteCode],0,@Today,@Today,1,1);

						-----   dedup data prepare for Merge
								 SELECT * 
								 INTO #PatientImmunization
								 FROM (
									SELECT  
											 PIQ.SourceSystemId
											, PIQ.LbPatientId 
											,PIQ.[ServiceDate]
											,IC.[ImmunizationCodeId]
											,ISNULL(dose.[ImmunizationDose],'') [ImmunizationDose]
											,units.[ImmunizationUnits]
											,lotNumber.[MaterialLotNumber]
											,manufacturer.[MaterialManufacturer]
											,ISNULL(MR.[MedicationRouteTypeID],0) MedicationRouteTypeID
											,ISNULL(pClinician.[PerformingClinician],'') as PerformingClinician
											,PIQ.[CreateDateTime]
											,PIQ.EncounterIdentifier
											,rNPI.[RenderingProviderNPI]
										, ROW_NUMBER() OVER
										 ( PARTITION BY 	
											PIQ.SourceSystemId, PIQ.LbPatientId, ISNULL(PIQ.[ServiceDate], '1901-01-01'),
											IC.[ImmunizationCodeId]
											ORDER BY 	InterfacePatientImmunizationId DESC ) RowNbr
										FROM  #PatientImmunizationQue PIQ with (nolock)
										INNER JOIN #ImmunizationCodingSystemType ICT ON PIQ.[ImmunizationCodeSystemName] = RefCodingSystemName
										INNER JOIN [ImmunizationCode] IC ON IC.[ImmunizationCode]=PIQ.[ImmunizationCode] AND IC.ImmunizationCodingSystemTypeId=ICT.ImmunizationCodingSystemTypeId
										LEFT OUTER JOIN MedicationRouteType MR ON MR.[Name]=PIQ.[RouteCode]
										LEFT JOIN
												(SELECT  LbPatientId, [ImmunizationDose],  SourceSystemId,  [ServiceDate], ROW_NUMBER() OVER 
												(PARTITION BY  LbPatientId,  ISNULL(  [ServiceDate],'1900-01-01'),  SourceSystemId ORDER BY  InterfacePatientImmunizationId DESC) rowNumber 
												FROM #PatientImmunizationQue    WITH (NOLOCK) WHERE  [ImmunizationDose] IS NOT NULL AND [ImmunizationDose] <> '' ) dose
												ON dose.LbPatientId = PIQ.LbPatientId AND ISNULL(dose.[ServiceDate],'1900-01-01') = ISNULL(PIQ.[ServiceDate],'1900-01-01')  
												 AND dose.SourceSystemId =PIQ.SourceSystemId 
										LEFT JOIN
												(SELECT  LbPatientId, [ImmunizationUnits],  SourceSystemId,  [ServiceDate], ROW_NUMBER() OVER 
												(PARTITION BY  LbPatientId,  ISNULL(  [ServiceDate],'1900-01-01'),  SourceSystemId ORDER BY  InterfacePatientImmunizationId DESC) rowNumber 
												FROM #PatientImmunizationQue    WITH (NOLOCK) WHERE  [ImmunizationUnits] IS NOT NULL AND [ImmunizationUnits] <> '' ) units
												ON units.LbPatientId = PIQ.LbPatientId AND ISNULL(units.[ServiceDate],'1900-01-01') = ISNULL(PIQ.[ServiceDate],'1900-01-01')  
												 AND units.SourceSystemId =PIQ.SourceSystemId 
										LEFT JOIN
												(SELECT  LbPatientId, [MaterialLotNumber],  SourceSystemId,  [ServiceDate], ROW_NUMBER() OVER 
												(PARTITION BY  LbPatientId,  ISNULL(  [ServiceDate],'1900-01-01'),  SourceSystemId ORDER BY  InterfacePatientImmunizationId DESC) rowNumber 
												FROM #PatientImmunizationQue    WITH (NOLOCK) WHERE  [MaterialLotNumber] IS NOT NULL AND [MaterialLotNumber] <> '') lotNumber
												ON lotNumber.LbPatientId = PIQ.LbPatientId AND ISNULL(lotNumber.[ServiceDate],'1900-01-01') = ISNULL(PIQ.[ServiceDate],'1900-01-01')  
												 AND lotNumber.SourceSystemId =PIQ.SourceSystemId 
										LEFT JOIN
												(SELECT  LbPatientId, [MaterialManufacturer],  SourceSystemId,  [ServiceDate], ROW_NUMBER() OVER 
												(PARTITION BY  LbPatientId,  ISNULL(  [ServiceDate],'1900-01-01'),  SourceSystemId ORDER BY  InterfacePatientImmunizationId DESC) rowNumber 
												FROM #PatientImmunizationQue    WITH (NOLOCK) WHERE  [MaterialManufacturer] IS NOT NULL AND [MaterialManufacturer] <> '' ) manufacturer
												ON manufacturer.LbPatientId = PIQ.LbPatientId AND ISNULL(manufacturer.[ServiceDate],'1900-01-01') = ISNULL(PIQ.[ServiceDate],'1900-01-01')  
												 AND manufacturer.SourceSystemId =PIQ.SourceSystemId 					
										LEFT JOIN
												(SELECT  LbPatientId, PerformingClinician,  SourceSystemId,  [ServiceDate], ROW_NUMBER() OVER 
												(PARTITION BY  LbPatientId,  ISNULL(  [ServiceDate],'1900-01-01'),  SourceSystemId ORDER BY  InterfacePatientImmunizationId DESC) rowNumber 
												FROM #PatientImmunizationQue    WITH (NOLOCK) WHERE  PerformingClinician IS NOT NULL AND PerformingClinician <> '' ) pClinician
												ON pClinician.LbPatientId = PIQ.LbPatientId AND ISNULL(pClinician.[ServiceDate],'1900-01-01') = ISNULL(PIQ.[ServiceDate],'1900-01-01')  
												 AND pClinician.SourceSystemId =PIQ.SourceSystemId 											
										LEFT JOIN
												(SELECT  LbPatientId, [RenderingProviderNPI],  SourceSystemId,  [ServiceDate], ROW_NUMBER() OVER 
												(PARTITION BY  LbPatientId,  ISNULL(  [ServiceDate],'1900-01-01'),  SourceSystemId ORDER BY  InterfacePatientImmunizationId DESC) rowNumber 
												FROM #PatientImmunizationQue    WITH (NOLOCK) WHERE  [RenderingProviderNPI] IS NOT NULL AND [RenderingProviderNPI] <> '' ) rNPI
												ON rNPI.LbPatientId = PIQ.LbPatientId AND ISNULL(rNPI.[ServiceDate],'1900-01-01') = ISNULL(PIQ.[ServiceDate],'1900-01-01')  
												 AND rNPI.SourceSystemId =PIQ.SourceSystemId 		
											WHERE 
												 (dose.rowNumber = 1 OR dose.rowNumber IS NULL)
												AND (units.rowNumber = 1 OR units.rowNumber IS NULL)
												AND (lotNumber.rowNumber = 1 OR lotNumber.rowNumber IS NULL)
												AND (manufacturer.rowNumber = 1 OR manufacturer.rowNumber IS NULL)
												AND (pClinician.rowNumber = 1 OR pClinician.rowNumber IS NULL)
												AND (rNPI.rowNumber = 1 OR rNPI.rowNumber IS NULL)
		
									) A
									WHERE A.RowNbr=1
							

	
								  CREATE CLUSTERED INDEX CIX_#PatientID_DiagnosisCode ON #PatientImmunization(SourceSystemId, [LbPatientId],[ServiceDate],[ImmunizationCodeId]) ;

								  --- test select * from #PatientImmunization


									MERGE [DBO].PatientImmunization AS [target]
									USING #PatientImmunization AS [source] 

									ON (target.[SourceSystemID] = [source].[SourceSystemID] AND target.PatientID =[source].[LbPatientId] 
										AND ISNULL(target.ServiceDate,'2099-12-31') = ISNULL([source].ServiceDate,'2099-12-31') AND target.ImmunizationCodeId = [source].ImmunizationCodeId 
										)
									WHEN MATCHED THEN UPDATE SET 
											[target].[ImmunizationDose] =  ISNULL([source].[ImmunizationDose], [target].[ImmunizationDose]), 
											[target].[ImmunizationUnits] =  ISNULL([source].[ImmunizationUnits], [target].[ImmunizationUnits]),
											[target].[MaterialLotNumber] =  ISNULL([source].[MaterialLotNumber], [target].[MaterialLotNumber]),
											[target].[MaterialManufacturer] =  ISNULL( [source].[MaterialManufacturer], [target].[MaterialManufacturer] ),
											[target].[MedicationRouteTypeID] =  ISNULL([source].[MedicationRouteTypeID], [target].[MedicationRouteTypeID] ),
											[target].[PerformedByClinician] =  ISNULL([source].PerformingClinician, [target].[PerformedByClinician]),
											[target].[DeleteInd] =  0, 
											[target].[CreateDateTime]= [source].[CreateDateTime],
											[target].[ModifyDateTime] =  @Today ,
											[target].[ModifyLBUserId] =  1,
											[target].[EncounterIdentifier] = ISNULL([source].[EncounterIdentifier], [target].[EncounterIdentifier]), 
											[target].[RenderingProviderNPI] = ISNULL( [source].[RenderingProviderNPI], [target].[RenderingProviderNPI])
									WHEN NOT MATCHED THEN
									INSERT ([PatientID] ,[SourceSystemId],[ServiceDate],[ImmunizationCodeId] ,[ImmunizationDose]
										  ,[ImmunizationUnits],[MaterialLotNumber],[MaterialManufacturer] ,[MedicationRouteTypeID],[PerformedByClinician]
										  ,[DeleteInd],[CreateDateTime],[ModifyDateTime] ,[CreateLBUserId],[ModifyLBUserId],[EncounterIdentifier],[RenderingProviderNPI]
										   )
									VALUES ([source].[LbPatientId] ,[source].[SourceSystemId],[source].[ServiceDate],[source].[ImmunizationCodeId] ,[source].[ImmunizationDose]
										  ,[source].[ImmunizationUnits],[source].[MaterialLotNumber],[source].[MaterialManufacturer] ,[source].[MedicationRouteTypeID],[source].PerformingClinician
										  ,0,@Today,@Today ,1,1,[source].[EncounterIdentifier],[source].[RenderingProviderNPI]);

	    
		

					-- update lastdate
               		   
						--Update the max processed time and drop the temp tables
						SET @tmpmaxSourceTimeStampProcessed = 	(SELECT MAX(CreateDateTime) FROM #PatientImmunization )
						IF @tmpmaxSourceTimeStampProcessed > @maxSourceTimeStampProcessed
						BEGIN
						SET @maxSourceTimeStampProcessed = @tmpmaxSourceTimeStampProcessed
						END

					  --clean
					  drop table #ImmunizationCodingSystemType, #ImmunizationCode , #PatientImmunization,#PatientImmunizationQue
	
 				
					END
			
			
				--Move onto the next batch
				DELETE FROM #PatientImmunizationQuePatientIdentifier WHERE PatientIdentifier IN (SELECT PatientIdentifier FROM #PatientImmunizationQuePatientIdentifieBATCH) OR PatientIdentifier IS NULL
				DROP TABLE #PatientImmunizationQuePatientIdentifieBATCH
				IF @NUMROWS = 0 BREAK;
  
			END
		
			DROP TABLE #PatientImmunizationQuePatientIdentifier
		
			-- update maxSourceTimeStamp with max CreateDateTime of #PatientImmunizationQue
			UPDATE [Maintenance]..[EDWTableLoadTracking]
			SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


			-- get total records from PatientImmunization after merge 
			SELECT @RecordCountAfter=COUNT(1)  FROM PatientImmunization WITH (NOLOCK);

			-- Calculate records inserted and updated counts
			SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
			SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

			-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			SET  @EndTime=GETUTCDATE();
			EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	
		



	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END


GO
