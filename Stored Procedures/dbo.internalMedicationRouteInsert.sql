SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalMedicationRouteInsert] 
    @MedicationRouteTypeId INT OUTPUT ,
	@MedicationRoute VARCHAR(255)  = NULL
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @DiagnosisCodeId INT

	IF @MedicationRoute IS NOT NULL 
	BEGIN

		BEGIN TRAN

		SELECT @MedicationRouteTypeId = MedicationRouteTypeId FROM MedicationRouteType
		WHERE Name = @MedicationRoute

		IF @MedicationRouteTypeId IS NULL
		BEGIN
			INSERT INTO [dbo].MedicationRouteType
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
			VALUES 
			(
				@MedicationRoute,
				@MedicationRoute,
				@DeleteInd,
				GetDate(),
				GetDate(),
				@LBUserId,
				@LBUserId
			)
			SELECT @intError = @@ERROR,
				@MedicationRouteTypeId = SCOPE_IDENTITY()
		END

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
		END
		ELSE
		BEGIN
			COMMIT TRAN
		END

	END
		
	RETURN @intError



GO
