SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[internalETLClinicalDataLastUpdatePatientAppointment]
AS
/*===============================================================
	CREATED BY: 	Christopher Lutz
	CREATED ON:		2017-02-15
	INITIAL VER:	2.2.1
	MODULE:			ETL Data Load
	DESCRIPTION:	This stored procedure will calculate all of the last update values for patient Appointment
	PARAMETERS:		
	  		

	RETURN VALUE(s)/OUTPUT: NA
	
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2017-02-15		CJL			LBETL-865	 Initial
	 
=================================================================*/
BEGIN



	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'ClinicalDataLastUpdatePatientAppointment';
	DECLARE @dataSource VARCHAR(20) = 'history';
	DECLARE @ProcedureName VARCHAR(128) = 'internalETLClinicalDataLastUpdatePatientAppointment';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @NUMROWS INT
	DECLARE @TypeName VARCHAR(100) = 'PatientAppointment'
	DECLARE @TypeId INT 

	BEGIN TRY	
			
	EXEC ClinicalDataTypeGet @TypeId OUTPUT, @TypeName

	IF NOT EXISTS ( SELECT top 1 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] = @dataSource AND [EDWName] =@EDWName)
	BEGIN
			EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed
	END

	           		   
	-- get total records from PatientMedication after merge 
	SELECT @RecordCountBefore = (SELECT COUNT(1)  FROM [ClinicalDataLastUpdate] WITH (NOLOCK) WHERE [ClinicalDataTypeID] =  @TypeId );




	SET @RecordCountSource = (SELECT COUNT(DISTINCT patientid) FROM vwportal_PatientAppointment WITH (NOLOCK) WHERE DeleteInd = 0 )
	
	--Calculate all of the maximum last modified data time stamp
	--
	-------------------------------------------
	MERGE INTO [ClinicalDataLastUpdate] AS target
	USING 
			(SELECT patientid, MAX(ModifyDateTime) as ModifyDateTime FROM vwportal_PatientAppointment WITH (NOLOCK) WHERE DeleteInd = 0    GROUP BY patientid) source
	ON 
		target.PatientId = source.PatientId AND target.[ClinicalDataTypeID] = @TypeId
	WHEN MATCHED THEN
		UPDATE SET target.ModifyDateTime = source.ModifyDateTime
	WHEN NOT MATCHED BY target THEN
		INSERT (PatientID, ModifyDateTime,[ClinicalDataTypeID])
		VALUES (source.PatientID,source.ModifyDateTime,@TypeId);
		   





	-- get total records from PatientMedication after merge 
	SELECT @RecordCountAfter=( SELECT COUNT(1)  FROM [ClinicalDataLastUpdate] WITH (NOLOCK) WHERE [ClinicalDataTypeID] =  @TypeId )


	-- update maxSourceTimeStamp with max CreateDateTime of
	UPDATE [Maintenance]..[EDWTableLoadTracking]
	SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex, updatedatetime = GETUTCDATE()
	WHERE [EDWtableName] = @EDWtableName AND [dataSource] =@dataSource  AND [EDWName] = @EDWName

			-- Calculate records inserted and updated counts
	SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

	-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
	SET  @EndTime=GETUTCDATE();
	EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;





	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
	

END

GO
