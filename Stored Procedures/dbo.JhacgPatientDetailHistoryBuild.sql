SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==============================================================================================
-- Author:		Lan Ma
-- Create date: 2016-08-19
-- Description:	this SP load the JhacgPatientDetail data into its history table, remove the history data 
--that are 2 months old, but only keep the firt record that was created in that month
-- Modified By:
-- Modified Date:
-- ==============================================================================================


CREATE PROCEDURE [dbo].[JhacgPatientDetailHistoryBuild]

AS
BEGIN	

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'JhacgPatientDetailHistory';
	DECLARE @ProcedureName VARCHAR(128) = 'JhacgPatientDetailHistoryBuild';
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;


    BEGIN TRY
		--delete the records that have been loaded into history at the same day
		DELETE FROM [JhacgPatientDetailHistory]          
		WHERE [CreateDate]= (SELECT DISTINCT CAST(CreateDateTime AS DATE) FROM [dbo].[JhacgPatientDetail])

		-- load orghierarchy data into history table
		INSERT INTO [dbo].[JhacgPatientDetailHistory]
		SELECT  JD.*    
			   ,CAST(CreateDateTime AS DATE)
		FROM [dbo].[JhacgPatientDetail] JD

		SET @InsertedRecord =@@ROWCOUNT

		--find the first record that was created in the 2nd previouse month for a patient, this reocrd's createdate will be set
		--as the first day of that month, and will be kept for that month in the history table
		SELECT
		t.*
		--,DATEADD(month, DATEDIFF(month, 0, [CreateDate]), 0) AS CreateDate --the first day of the month in which the record was created
		INTO #JhacgPatientDetailHistory
		FROM
		(           
		SELECT 
		JD.*
		,ROW_NUMBER() OVER(PARTITION BY Patient_Id ORDER BY CreateDate ASC) AS rowNbr
		FROM [dbo].[JhacgPatientDetailHistory] JD
		WHERE CreateDate >= dateadd(month,-2,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) -- get first day of previous 2nd month
		AND CreateDate < dateadd(month,-1,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) --get first day of previous month
		) t WHERE rowNbr = 1

		ALTER TABLE #JhacgPatientDetailHistory
		DROP COLUMN rowNbr,CreateDate

		 --Delete all of 2nd prevouse month's data
		DELETE
		FROM [dbo].[JhacgPatientDetailHistory]
		WHERE CreateDate >= dateadd(month,-2,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) -- get first day of previous 2nd month
		AND CreateDate < dateadd(month,-1,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) --get first day of previous month

		INSERT INTO [JhacgPatientDetailHistory]
		SELECT jh.*,
			   DATEADD(month, DATEDIFF(month, 0, CAST([CreateDateTime] AS DATE)), 0) AS CreateDate --the first day of the month in which the record was created
		FROM #JhacgPatientDetailHistory jh

		DROP TABLE #JhacgPatientDetailHistory


		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	END TRY

	BEGIN CATCH
	    --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		SET  @ErrorNumber =ERROR_NUMBER();
		SET  @ErrorState =ERROR_STATE();
		SET  @ErrorSeverity=ERROR_SEVERITY();
		SET  @ErrorLine=ERROR_LINE();
		SET  @Comment =left(ERROR_MESSAGE(),400);
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
