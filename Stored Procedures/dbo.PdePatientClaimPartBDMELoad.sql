SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientClaimPartBDMELoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT DISTINCT
		pa.PatientId,
		pa.ClaimId, 
		pa.CardholderId,
		pa.ClaimTypeCode, 
		pa.FromDate, 
		pa.ThruDate, 
		pa.CPT, 
		pa.CPTDesc, 
		pa.PaidAmt, 
		pa.RenderingNPI, 
		pa.RenderingName, 
		pa.OrderingNPI, 
		pa.OrderingName, 
		pa.AdjustmentCode
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientClaimPartBDME pa ON pa.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (pa.FromDate > @dtmStartDate)
 
END
GO
