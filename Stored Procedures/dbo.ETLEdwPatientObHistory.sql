SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwPatientObHistory] 
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES FOR NULL COLUMNS

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId,LbPatientID, DateRecorded 
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				InterfacePatientObHistoryId
				DESC 
			) SNO
	FROM PatientObHistoryProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientObHistoryProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientObHistoryId INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientObHistoryId,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientObHistoryId,PatientID
FROM 
(	
MERGE [DBO].PatientObHistory AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,DateRecorded AS DateRecorded,
			[Gravidity] AS [Gravidity],[Parity1] AS [Parity1],[Parity2] AS [Parity2],[Parity3] AS [Parity3],[Parity4] AS [Parity4],LastMenstrualPeriod,
			0 AS DeleteInd, GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId
		FROM [dbo].PatientObHistoryProcessQueue PPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  ([OrigPatientId],[PatientId],[SourceSystemID],[DateRecorded],
	   [Gravidity],[Parity1],[Parity2],[Parity3],[Parity4],LastMenstrualPeriod,
	   [DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
ON ( target.PatientID = source.PatientID AND ISNULL(target.DateRecorded,'2099-01-01') = ISNULL(source.DateRecorded,'2099-01-01') )
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
--[PatientId]			= source.[PatientId],
--[DateRecorded]		= source.[DateRecorded],
[Gravidity]				= source.[Gravidity],
[Parity1]				= source.[Parity1],
[Parity2]				= source.[Parity2],
[Parity3]				= source.[Parity3],
[Parity4]			= source.[Parity4],
[LastMenstrualPeriod]			= source.[LastMenstrualPeriod],
[DeleteInd]			= source.[DeleteInd],
--[CreateDateTime]		= source.[CreateDateTime],
[ModifyDateTime]		= source.[ModifyDateTime],
--[CreateLBUserId]		= source.[CreateLBUserId],
[ModifyLBUserId]		= source.[ModifyLBUserId]
WHEN NOT MATCHED THEN
INSERT ([PatientId],[DateRecorded],
	   [Gravidity],[Parity1],[Parity2],[Parity3],[Parity4],LastMenstrualPeriod,
	   [DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId] )
VALUES (source.[PatientId],source.[DateRecorded],
	   source.[Gravidity],source.[Parity1],source.[Parity2],source.[Parity3],source.[Parity4],source.LastMenstrualPeriod,
	   source.[DeleteInd],source.[CreateDateTime],source.[ModifyDateTime],source.[CreateLBUserId],source.[ModifyLBUserId])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientObHistoryId,INSERTED.PatientObHistoryId) PatientObHistoryId, source.PatientID
) x ;

IF OBJECT_ID('PatientObHistory_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientObHistory_PICT(PatientObHistoryId,PatientId,SwitchDirectionId)
	SELECT RT.PatientObHistoryId, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientObHistoryProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'PatientObHistory',GetDate()
FROM PatientObHistoryProcessQueue
WHERE ProcessedInd = 1 


END


GO
