SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[InternalCMeasureDetailHistory]
AS
BEGIN
/*

-Keep two months

-For each patientid, measureid for each week Keep latest for that patient.
After 6 months keep latest for each patientid, measureid for each month Keep latest for that patient.
--UPDATE ALL TO NOT CURRENT
UPDATE PatientContract SET IsCurrentInd = 0 WHERE IsCurrentInd = 1
--DELETE ALL GREATER THAN 13 DAYS, NOT ON A FRIDAY
DELETE FROM PatientContract WHERE DATEDIFF("day", RunDateTime, getdate()) > 13 AND DatePart("WeekDay", RunDateTime) <> 6 and IsCurrentInd = 0

*/

--  truncate table [CMeasure_DetailHistory]
INSERT INTO [dbo].CMeasure_DetailHistory
           (MeasurePatientID
		   ,[MeasureID]
           ,[PatientMemberID]
           ,[LbPatientID]
           ,[Denominator]
           ,[Exclusion]
           ,[Numerator]
           ,[MeasureOverrideStatus]
           ,[LbTaskID]
           ,[TaskStatus]
           ,[TaskOverrideStatus]
           ,[GPRO]
           ,[CreateTimestamp]
           ,[ModifyTimestamp]
		   )
select (TRY_CONVERT(varchar(20),cmd.[MeasureID]) + '-' + TRY_CONVERT(varchar(20),cmd.[LbPatientID])) MeasurePatientID
			,cmd.[MeasureID]
           ,cmd.[PatientMemberID]
           ,cmd.[LbPatientID]
           ,cmd.[Denominator]
           ,cmd.[Exclusion]
           ,cmd.[Numerator]
           ,cmd.[MeasureOverrideStatus]
           ,cmd.[LbTaskID]
           ,cmd.[TaskStatus]
           ,cmd.[TaskOverrideStatus]
           ,cmd.[GPRO]
           ,TRY_CONVERT(DATE,cmd.[CreateTimestamp])
           ,TRY_CONVERT(DATE,cmd.[ModifyTimestamp])
from CMeasure_Detail cmd
left join CMeasure_DetailHistory cmhd on cmd.LbPatientid = cmhd.LbPatientid
		and cmd.[MeasureID] = cmhd.[MeasureID]
		and TRY_CONVERT(DATE,cmd.[CreateTimestamp]) = TRY_CONVERT(DATE,cmhd.[CreateTimestamp])
where cmhd.LbPatientid IS NULL

--CREATE CLUSTERED INDEX CMeasure_DetailHistoryIndex
--ON CMeasure_DetailHistory (LbPatientid,[MeasureID],[CreateTimestamp])

--CREATE CLUSTERED INDEX CMeasure_DetailHistoryIndex
--ON CMeasure_Detail (LbPatientid,[MeasureID],[CreateTimestamp])

-- truncate table CMeasure_DetailHistory


--select distinct CreateTimestamp
--from CMeasure_DetailHistory
--order by createtimestamp


--DELETE 

EXEC InternalDropTempTables 

SELECT DISTINCT MeasurePatientID,min(createtimestamp) createtimestamp
into #1
FROM CMeasure_DetailHistory
WHERE CreateTimestamp >= dateadd(month,-2,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) -- get first day of previous 2nd month
and CreateTimestamp < dateadd(month,-1,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) --get first day of previous month
group by MeasurePatientID
ORDER BY MeasurePatientID
--862398

-- select distinct [CreateTimestamp] from #1

--Get previous 2nd month's data into a temp table

SELECT DISTINCT 
cm.MeasurePatientID
,cm.[MeasureID]
,cm.[PatientMemberID]
,cm.[LbPatientID]
,cm.[Denominator]
,cm.[Exclusion]
,cm.[Numerator]
,cm.[MeasureOverrideStatus]
,cm.[LbTaskID]
,cm.[TaskStatus]
,cm.[TaskOverrideStatus]
,cm.[GPRO]
, DATEADD(month, DATEDIFF(month, 0, cm.[CreateTimestamp]), 0) [CreateTimestamp]--get first month
,cm.[ModifyTimestamp]
into #2
FROM CMeasure_DetailHistory cm
join #1 a on cm.MeasurePatientID = a.MeasurePatientID and cm.createtimestamp = a.createtimestamp

--select distinct [CreateTimestamp] from #2

--Delete all of last month's data
 delete
-- SELECT DISTINCT CreateTimestamp
FROM CMeasure_DetailHistory
WHERE CreateTimestamp >= dateadd(month,-2,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) -- get first day of previous 2nd month
and CreateTimestamp < dateadd(month,-1,(DATEADD(month, DATEDIFF(month, 0, getdate()), 0))) --get first day of previous month


--Insert previous 2nd month's data with CreateTimestamp as the first of the month
insert into CMeasure_DetailHistory
(
cm.MeasurePatientID
,cm.[MeasureID]
,cm.[PatientMemberID]
,cm.[LbPatientID]
,cm.[Denominator]
,cm.[Exclusion]
,cm.[Numerator]
,cm.[MeasureOverrideStatus]
,cm.[LbTaskID]
,cm.[TaskStatus]
,cm.[TaskOverrideStatus]
,cm.[GPRO]
, [CreateTimestamp]
,cm.[ModifyTimestamp]
)
SELECT DISTINCT 
cm.MeasurePatientID
,cm.[MeasureID]
,cm.[PatientMemberID]
,cm.[LbPatientID]
,cm.[Denominator]
,cm.[Exclusion]
,cm.[Numerator]
,cm.[MeasureOverrideStatus]
,cm.[LbTaskID]
,cm.[TaskStatus]
,cm.[TaskOverrideStatus]
,cm.[GPRO]
, [CreateTimestamp]
,cm.[ModifyTimestamp]
from #2 cm

EXEC InternalDropTempTables 
end
GO
