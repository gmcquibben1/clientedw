SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =====================================================================================
-- Author:		Mike Hoxter
-- Create date: 2015-12-18
-- Description:	set patient provider attribution from interfacePatient demographics data
-- Modified By: Lan Ma
-- Modified Date: 2016-04-05
-- Modified By: Youping
-- Modified Date: 2016-04-21  add PatientID IS NOT NULL
-- ======================================================================================

CREATE PROCEDURE [dbo].[internalAttributionDemographics]
AS
BEGIN
	--select * from princetonmedlbportalprod..attributiontype
	DECLARE @AttributionTypeId int
	SET @AttributionTypeId = 2

	--Step 1: patient-provider from Interface
	SELECT [InterfaceSystemId],[PatientIdentifier], [InterfacePatientId], [PrimaryCareProviderNPI], SourceSystemId
	INTO #patientList
	FROM
	(
		SELECT 
		ip.[InterfaceSystemId]
		,ip.[PatientIdentifier]
		,ip.[InterfacePatientId]
		,dbo.vwPortal_InterfaceSystem.SourceSystemId 
		,ip.[PrimaryCareProviderNPI]
		,ROW_NUMBER() OVER(PARTITION BY ip.[InterfaceSystemId],[PatientIdentifier] ORDER BY ip.[CreateDateTime] DESC) AS rowNbr
		FROM [dbo].[vwPortal_InterfacePatient] ip
		JOIN vwPortal_InterfaceSystem 
		ON ip.InterfaceSystemId = dbo.vwPortal_InterfaceSystem.InterfaceSystemId
	) t1
	WHERE rowNbr =1 AND [PrimaryCareProviderNPI] IS NOT NULL

   --select * from #patientList

	--using the vwportal_ProviderActive to exclude the provides without healthcare org assigned, updated on 04/05/2016
	INSERT INTO [dbo].[vwPortal_PatientProvider]
        ([PatientID]
        ,[ProviderID]
        ,[SourceSystemID]
        ,[ExternalReferenceIdentifier]
        ,[OtherReferenceIdentifier]
        ,[ProviderRoleTypeID]
        ,[DeleteInd]
        ,[CreateDateTime]
        ,[ModifyDateTime]
        ,[CreateLBUserId]
        ,[ModifyLBUserId]
        ,[AttributionTypeId]
        )
	SELECT 
		[LbPatientId] AS PatientID 
		,ProviderID  
		,t1.SourceSystemID 
		,'|' + cast(t1.[InterfaceSystemId] AS varchar(5)) + '|' +  PatientIdentifier + '|' AS [ExternalReferenceIdentifier]
		,NationalProviderIdentifier
		,1 [ProviderRoleTypeID]
		,0 [DeleteInd]
		,getdate() [CreateDateTime]
		,getdate() [ModifyDateTime]
		,1 [CreateLBUserId]
		,1 [ModifyLBUserId]
		,2 [AttributionTypeId]
	FROM #patientList t1
	JOIN [dbo].[PatientIdReference] t2
	ON t1.[PatientIdentifier] = t2.ExternalId
	AND t1.SourceSystemID = t2.SourceSystemID
	AND LbPatientId IS NOT NULL AND [IdTypeDesc] = 'InterfacePatientIdentifier'  AND [DeleteInd] = 0
	JOIN [dbo].[vwPortal_ProviderActive] t4 
	ON RTRIM(LTRIM(t1.PrimaryCareProviderNPI)) = RTRIM(LTRIM(t4.NationalProviderIdentifier))
	WHERE [LbPatientId] IS NOT NULL

/* --original code for reference
	--select distinct idtypedesc from patientidreference
	SELECT PatientID,ProviderID, NationalProviderIdentifier,SourceSystemID, medicareNo, 'Interface' as sourcefeed
	INTO #patientProvider
	From(
			SELECT 
			[LbPatientId] AS PatientID, 
			COALESCE(ProviderID,0) AS ProviderID, 
			t1.[PrimaryCareProviderNPI] AS NationalProviderIdentifier, 
			t1.SourceSystemID, 
			'|' + cast(t1.[InterfaceSystemId] AS varchar(5)) + '|' +  PatientIdentifier + '|' as medicareNo,
			ROW_NUMBER() OVER(PARTITION BY [LbPatientId] ORDER BY COALESCE(t1.[PrimaryCareProviderNPI], '0') DESC) AS rowNbr
			FROM #patientList t1
			JOIN [dbo].[PatientIdReference] t2
			ON t1.[PatientIdentifier] = t2.ExternalId
			AND LbPatientId IS NOT NULL AND [IdTypeDesc] = 'InterfacePatientIdentifier'  AND [DeleteInd] = 0
			LEFT JOIN [dbo].[vwPortal_Provider] t4 
			ON t1.PrimaryCareProviderNPI = t4.[NationalProviderIdentifier]
			AND t4.[DeleteInd] = 0
	) tp where rowNbr = 1
	--select * from #patientProvider order by patientid


-- step 2: uninon all patient together

select PatientID
INTO #allPatientList
From (
select PatientID 
from #patientProvider
) t


--Step 3 build patient-provider relationship
select 
p1.PatientID, 
p2.ProviderID AS ProviderID,
p2.NationalProviderIdentifier AS NationalProviderIdentifier,
p2.SourceSystemID AS SourceSystemID,
p2.medicareNo AS  medicareNo,
p2.sourcefeed AS sourcefeed
INTO #allPatientProvider
from #allPatientList p1
Left JOIN #patientProvider p2
on p1.PatientID = p2.PatientID
--select * from #allPatientProvider

update vwportal_patientprovider set deleteind = 1,modifydatetime = getdate() where AttributionTypeId = 2



MERGE INTO vwportal_patientprovider AS target
USING (SELECT PCP.PatientID, PCP.ProviderID, PCP.SourceSystemId,
 PCP.NationalProviderIdentifier 
FROM #allPatientProvider PCP) AS SOURCE
ON source.patientid = target.patientid and source.SourceSystemId = target.SourceSystemID
WHEN MATCHED THEN
       UPDATE SET target.ProviderID = source.providerid, target.modifydatetime = getdate(), target.deleteind = 0, target.OtherReferenceIdentifier = source.NationalProviderIdentifier
WHEN NOT MATCHED BY TARGET THEN
      INSERT (PatientID,ProviderID,SourceSystemID,OtherReferenceIdentifier,DeleteInd
      ,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,ProviderRoleTypeId,AttributionTypeId) 
      values
      (source.PatientID, source.ProviderID, source.SourceSystemId,source.NationalProviderIdentifier, 0, 
	  getdate(), getdate(), 1,1,1, 2);


update vwportal_PatientProvider 

set OtherReferenceIdentifier = '9999999999' 

where AttributionTypeId = @AttributionTypeId 

and isnull(OtherReferenceIdentifier,'') = ''



drop table #allPatientProvider

drop table #allPatientList


drop table #patientProvider
*/
DROP TABLE #patientList

END

GO
