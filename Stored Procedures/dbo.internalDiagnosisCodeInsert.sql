SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalDiagnosisCodeInsert] 
    @DiagnosisCodeId INT OUTPUT ,
	@DiagnosisCodingSystemTypeId INT ,
	@DiagnosisCode VARCHAR(255)  ,
	@DiagnosisDescription VARCHAR(255) --,
	--@DiagnosisDescriptionAlternate VARCHAR(255) = ''
AS

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @DiagnosisCodeId INT

	BEGIN TRAN

	SELECT @DiagnosisCodeId = DiagnosisCodeId FROM [DiagnosisCode]
	WHERE DiagnosisCodingSystemTypeId  = @DiagnosisCodingSystemTypeId
	AND LTRIM(RTRIM(REPLACE(DiagnosisCodeRaw,'.',''))) = LTRIM(RTRIM(REPLACE(@DiagnosisCode,'.',''))) 

	IF @DiagnosisCodeId IS NULL
	BEGIN
		INSERT INTO [dbo].[DiagnosisCode]
			   ([DiagnosisCodingSystemTypeId]
			   ,[DiagnosisCode]
			   ,[DiagnosisCodeRaw]
			   ,[DiagnosisDescription]
			   ,[DiagnosisDescriptionAlternate]
			   ,[DeleteInd]
			   ,[CreateDateTime]
			   ,[ModifyDateTime]
			   ,[CreateLBUserId]
			   ,[ModifyLBUserId])
		VALUES 
		(
			@DiagnosisCodingSystemTypeId,
			Replace(@DiagnosisCode ,'.',''),
			@DiagnosisCode,
			@DiagnosisDescription,
			@DiagnosisDescription,
			@DeleteInd,
			GetDate(),
			GetDate(),
			@LBUserId,
			@LBUserId
		)
		SELECT @intError = @@ERROR,
			@DiagnosisCodeId = SCOPE_IDENTITY()
	END

	IF @@ERROR <> 0 
	BEGIN
		ROLLBACK TRAN
	END
	ELSE
	BEGIN
		COMMIT TRAN
	END
		
	RETURN @intError


GO
