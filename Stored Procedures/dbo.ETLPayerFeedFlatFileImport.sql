SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Samuel Owusu
-- Create date: 2016-06-14
-- Description:	Load Flat Files
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[ETLPayerFeedFlatFileImport]

AS
BEGIN	

/* This script loads data files to staging tables. 
Below are the steps 
Step 1. The script starts by getting a list of all the payers from a particular database.
After getting the payer list, it then loops through payer by payer using a while loop.

Step 2. For each payer, it loops through file by file using a while loop.  For each file, it goes to the payerfeedsinfo table and gets the info needed to load the files for that payer.
This info includes the following below;
UnprocessedFilePickup: Path to where file that needs to be process is located.
ProcessedFileDrop: Path to where file will be moved to after it gets loaded. 
FormatFilePickup: Path to where the format file is located.
FileNamePattern: File name pattern to look for in the UnprocessedFilePickup folder. 
ClaimsTable: Is the name of the staging table where the data is first loaded to. 
FileDateExp: Is the file date, which is part of the filename. Is usually a script
HeaderPresent: Indicates whether the file has headers or not
FailedFileDrop: Path to where failed files would be drop. If a file fails to load, it will be moved to the failed folder

The script will check and make sure that all the folders(UnprocessedFilePickup,ProcessedFileDrop,FailedFileDrop) 
and files(FormatFilePickup) are present/exist before it precedes, in a case where the path does not exist, an error 
would be raised indicating file or folder does not exist. 

The script will will also check and make sure that the ClaimTable(which is the staging table) exist, in case where the
table does not exist, an error would also be raised indicating table does not exist. 

After validating that the Unprocessed folder, Processed folder, Failed folder, format file and staging table exist for 
a file name pattern. It goes to the Unprocessed folder path and loads the files with that file name pattern to a variable table called @Files. 

Step 3. In the final step, I use a while loop to loop through each file that matches the filename pattern in the @Files table
For each file, I do a validation on the FileDatExp(file date expected) first, if it fails, the file does not get process, and 
a record is inserted to the FilesAudit table indicating file failed due to a file date issue. Then the loop moves on to the next file.

If the file date expected is valid, the next step is the actual file validation using the file format. I first do a count for the number of records in the file, 
instead of doing a select all. If the file is valid, then it should be able to do a count. If it fails to do get a count due to an error, a record is inserted 
to the FilesAudit table indicating file failure and the reason for why it failed. 

I also got a script in there that updates the unfinishedprocessingflag column on the payerfeedsinfo table to 1.

If the file passes the record count test, it gets loaded to the staging table. After it gets loaded a record is inserted to the FilesAudit table
indicating file passed, the start and end datetime along with the number of records for the file.



Also I went ahead and added FileDate and FileName columns to the new staging tables that I've created for clients. 
I plan on implementing this change as I work the tickets. But for script to work, the staging tables has to have a 
FileDate and FileName columns. If this script tries to load to a table that does not have a FileDate or FileName 
column, the file will fail. 
*/
SET NOCOUNT ON

DECLARE @PayerName VARCHAR(100)
		,@UnprocessedFolder VARCHAR(250)
		,@ProcessedFolder VARCHAR(250)
		,@FormatFile VARCHAR(250)
		,@FileNamePattern VARCHAR(100)
		,@ClaimsTable VARCHAR(100)
		,@FileDateExp VARCHAR(100)
		,@FirstRow VARCHAR(2)
		,@FailedFolder VARCHAR(250)
		,@Cmd NVARCHAR(MAX)
		,@File2Process VARCHAR(100)
		,@FileDate VARCHAR(50)
		,@DatabaseName VARCHAR(100)
		,@Count INT
		,@Message VARCHAR(MAX)
		,@ValidationStatus VARCHAR(25)
		,@Folder VARCHAR(250)
		,@CheckListID INT

--Create a variable table for list of payers 
DECLARE @PayerList TABLE (Payer VARCHAR(100))
--Create a variable table for list of files to load
DECLARE @Files TABLE([FileName] VARCHAR(100))
--Create a variable table for payerfeedsinfo
DECLARE @PayerFeedsInfo TABLE 
(
	PayerName VARCHAR(100)
	,UnprocessedFolder VARCHAR(250)
	,ProcessedFolder VARCHAR(250)
	,FileFormat VARCHAR(250)
	,FileNamePattern VARCHAR(100)
	,ClaimsTable VARCHAR(100)
	,FileDateExp VARCHAR(100)
	,HeaderPresent VARCHAR(2)
	,FailedFolder VARCHAR(250)
)

--Create variable table for folder/table validations
DECLARE @CheckList TABLE(CheckListID INT IDENTITY(1,1), FolderName VARCHAR(250), FolderType VARCHAR(100))
DECLARE @Directory_Results TABLE(FileExists INT, File_Is_A_Directory INT, Parent_Directory_Exists INT)

SET @DatabaseName=(SELECT db_name())

--infor notes
PRINT 'Database: '+@DatabaseName

--Get a list of all the payers for this database
INSERT INTO @PayerList
SELECT DISTINCT PayerName FROM dbo.PayerFeedsInfo


--Loop through payer by payer
WHILE (SELECT COUNT(*) FROM @PayerList) > 0
BEGIN
	SET @PayerName=(SELECT TOP 1 Payer FROM @PayerList)

	--infor notes
	PRINT 'Payer: '+@PayerName

	--Get all the payerfeedsinfo for the payer
	INSERT INTO @PayerFeedsInfo
	SELECT 
		PayerName 
		,UnprocessedFilePickup 
		,ProcessedFileDrop
		,FormatFilePickup 
		,FileNamePattern 
		,ClaimsTable 
		,FileDateExp 
		,HeaderPresent
		,FailedFileDrop 
	FROM dbo.PayerFeedsInfo
	WHERE PayerName=@PayerName

	--Loop through each filepattern for the payer
	WHILE (SELECT COUNT(*) FROM @PayerFeedsInfo)>0
	BEGIN
		SET @FileNamePattern=(SELECT TOP 1 FileNamePattern FROM @PayerFeedsInfo)

		--infor notes
		PRINT 'FileName Pattern: '+@FileNamePattern

		--For each file, below are the sets for the folders, files, staging table and file date expected
		SET @ClaimsTable=(SELECT ClaimsTable FROM @PayerFeedsInfo WHERE FileNamePattern=@FileNamePattern)
		SET @FormatFile=(SELECT FileFormat FROM @PayerFeedsInfo WHERE FileNamePattern=@FileNamePattern)
		SET @FirstRow=(SELECT CASE HeaderPresent WHEN 'N' THEN 1 ELSE 2 END FROM @PayerFeedsInfo WHERE FileNamePattern=@FileNamePattern)
		SET @UnprocessedFolder=(SELECT UnprocessedFolder FROM @PayerFeedsInfo WHERE FileNamePattern=@FileNamePattern)
		SET @FileNamePattern=(SELECT FileNamePattern FROM @PayerFeedsInfo WHERE FileNamePattern=@FileNamePattern)
		SET @FileDateExp=(SELECT FileDateExp FROM @PayerFeedsInfo WHERE FileNamePattern=@FileNamePattern)
		SET @ProcessedFolder=(SELECT ProcessedFolder FROM @PayerFeedsInfo WHERE FileNamePattern=@FileNamePattern)
		SET @FailedFolder=(SELECT FailedFolder FROM @PayerFeedsInfo WHERE FileNamePattern=@FileNamePattern)

		--Check if folders/file exist first for the payer
		--Load all the folder paths and format file path to a variable table
		INSERT INTO @CheckList
		VALUES(@UnprocessedFolder,'Unprocessed')
		,(@ProcessedFolder,'Processed')
		,(@FailedFolder,'Failed')
		,(@FormatFile,'Format')
		,(@ClaimsTable,'Table')

		--Loop through the paths and validate that they exist. 
		WHILE (SELECT COUNT(*) FROM @CheckList)>0
		BEGIN 
			SET @CheckListID=(SELECT TOP 1 CheckListID FROM @CheckList)
			SET @Folder=(SELECT FolderName FROM @CheckList WHERE CheckListID=@CheckListID)

			INSERT INTO @Directory_Results
			EXEC Master.dbo.xp_fileexist @Folder
			
			BEGIN TRY
				IF EXISTS(SELECT 1 FROM @CheckList WHERE CheckListID=@CheckListID AND FolderType='Format')
				BEGIN
					--if format file path does not exist or is null, raise an error
					IF NOT EXISTS(SELECT 1 FROM @Directory_Results WHERE FileExists=1)
					BEGIN				
						SET @Message='Format file does not exist for file pattern '''+@FileNamePattern+'''.'
						RAISERROR(@Message,16,1)
					END
				END
				IF EXISTS(SELECT 1 FROM @CheckList WHERE CheckListID=@CheckListID AND FolderType='Table')
				BEGIN
					--if folder path does not exist or is null, raise an error
					IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @ClaimsTable)
					BEGIN
						SET @Message='Staging table does not exist for file pattern '''+@FileNamePattern+'''.'
						RAISERROR(@Message,16,1)
					END
				END
				IF EXISTS(SELECT 1 FROM @CheckList WHERE CheckListID=@CheckListID AND FolderType NOT IN ('Format','Table'))
				BEGIN
					--if folder path does not exist or is null, raise an error
					IF NOT EXISTS(SELECT 1 FROM @Directory_Results WHERE File_Is_A_Directory=1)
					BEGIN
						SET @Message=(SELECT FolderType+' folder does not exist for '+@FileNamePattern FROM @CheckList WHERE CheckListID=@CheckListID)
						RAISERROR(@Message,16,1)
					END
				END

				SET @ValidationStatus='Passed'
			END TRY
			BEGIN CATCH
				--Insert into FilesAudit table
				INSERT INTO ClientMaster..FilesAudit(DatabaseName, PayerName, OutputStatus, OutputMessage, StartDate)
				SELECT @DatabaseName, @PayerName, 'Failed', ERROR_MESSAGE(), GETDATE()

				SET @ValidationStatus='Failed'
			END CATCH
			DELETE FROM @Directory_Results
			DELETE FROM @CheckList WHERE CheckListID=@CheckListID
		END
	


	IF @ValidationStatus='Passed'
	BEGIN
		--Loads all the files on the Unprocessed folder for payer to a variable table
		INSERT INTO @Files
		EXEC ('xp_cmdshell ''dir '+@UnprocessedFolder+' /b''')

		--infor notes
		IF (SELECT COUNT(*) FROM @Files WHERE [FileName] LIKE '%'+REPLACE(@FileNamePattern,'?','_')+'%')=0
		BEGIN
			PRINT 'There are no files to be process'
		END

		--Loops through the files and find files with the specific file pattern for payer
		WHILE (SELECT COUNT(*) FROM @Files WHERE [FileName] LIKE '%'+REPLACE(@FileNamePattern,'?','_')+'%')>0
		BEGIN
			SET @File2Process=(SELECT TOP 1 [FileName] FROM @Files WHERE [FileName] LIKE '%'+REPLACE(@FileNamePattern,'?','_')+'%')
			SET @Cmd='SELECT @FileDate='+REPLACE(@FileDateExp,'?',''''+@File2Process+'''')

			PRINT 'File: '+@File2Process 
			
			BEGIN TRY
				--Get FileDate for file
				SET @Count=NULL
				EXEC sp_executeSQl @Cmd, N'@FileDate VARCHAR(50) OUTPUT', @FileDate=@FileDate OUTPUT;
		
				SET @Cmd='SELECT @Count=COUNT(*) FROM OPENROWSET 
						 (BULK '''+@UnprocessedFolder+'\'+@File2Process+'''
						 ,FIRSTROW='+@FirstRow+'
						 ,FORMATFILE='''+@FormatFile+''')x'
				
				--Validate file	by getting count
				EXEC sp_executeSQl @Cmd, N'@Count INT OUTPUT', @Count=@Count OUTPUT

				--Load the file
				EXEC ('INSERT INTO dbo.'+@ClaimsTable+' 
					  SELECT *,'''+@File2Process+''','''+@FileDate+'''
					  FROM OPENROWSET
					  (BULK '''+@UnprocessedFolder+'\'+@File2Process+'''
					  ,FIRSTROW='+@FirstRow+'
					  ,FORMATFILE='''+@FormatFile+''')x')

				--Update UnfinishedProcessFlag column on payerfeedsinfo
				UPDATE PayerFeedsInfo
				SET UnFinishedProcessFlag=1
				WHERE FileNamePattern=@FileNamePattern

				--Insert into files audit table
				INSERT INTO ClientMaster..FilesAudit
				(
					[FileName], 
					DatabaseName, 
					PayerName, 
					OutputStatus, 
					OutputMessage, 
					RowCounts,
					Destination,
					StartDate
				)
				SELECT 
					@File2Process
					,@DatabaseName
					,@PayerName
					,'Passed'
					,NULL
					,@Count
					,@ClaimsTable
					,GETDATE()

				UPDATE ClientMaster..FilesAudit
				SET EndDate=GETDATE()
				WHERE [FileName]=@File2Process
				AND OutputStatus='Passed'

				--infor notes
				PRINT 'File loaded successfully'
				
				--Move file to Processed Folder
				EXEC ('master..xp_cmdshell ''MOVE '+ @UnprocessedFolder + '\' + @File2Process +'  ' + @ProcessedFolder +'''')
				
			END TRY
			BEGIN CATCH
				--infor notes
				PRINT 'File has errors'
				
				--Insert into FilesAudit table
				INSERT INTO ClientMaster..FilesAudit
				(
					[FileName], 
					DatabaseName, 
					PayerName, 
					OutputStatus, 
					OutputMessage, 
					RowCounts,
					Destination,
					StartDate
				)
				SELECT 
					@File2Process
					,@DatabaseName
					,@PayerName
					,'Failed'
					,ERROR_MESSAGE()
					,@Count
					,@FailedFolder
					,GETDATE()
				
				--Move file to Failed folder
				EXEC('master..xp_cmdshell ''MOVE '+ @UnprocessedFolder + '\' + @File2Process +'  ' + @FailedFolder +'''')		
			END CATCH
			
			DELETE FROM @Files WHERE [FileName]=@File2Process
		END
	END
		DELETE FROM @PayerFeedsInfo WHERE FileNamePattern=@FileNamePattern
	END

DELETE FROM @PayerList WHERE Payer=@PayerName
END
END
GO
