SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create PROCEDURE [dbo].[ETLPatientEncounter] (@fullLoadFlag bit = 0)


-- ================================================================
-- Author:		Youping Liu
-- Create date: 2016-06-09
-- Description:	Load PatientEncounter Data into EDW
-- Modified By:
-- Modified Date:
-- Notes: @fullLoadFlag have values of 1 or 0
--
-- 
-- Modified Youping on 2016-07-28
-- Add procedure running log to [Maintenance].[dbo].[EdwProcedureRunLog]
-- ===============================================================
AS

BEGIN	

	SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'PatientEncounter';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientEncounter';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();


	 BEGIN TRY



			-- 1.remove records from PatientEncounterProcessQueue
			TRUNCATE TABLE PatientEncounterProcessQueue


		
			--set @fullLoadFlag =1
	
			IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] =@EDWName)
			  BEGIN
			  
					 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed

			  END

			IF @fullLoadFlag = 1
			  BEGIN
				UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				   SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME)
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
			  END

			DECLARE @lastDateTime DATETIME = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
											FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
											WHERE [EDWtableName] = @EDWtableName 
											AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	
			DECLARE @maxSourceDateTime DATETIME = (SELECT MAX([CreateDateTime]) FROM [dbo].[vwPortal_InterfacePatientEncounter]);




			-- test select @maxSourceDateTime, @lastDateTime 

			-- 2. populate data into PatientEncounterProcessQueue
			INSERT INTO [dbo].[PatientEncounterProcessQueue]
					([InterfacePatientEncounterId]
					,[InterfacePatientId]
					,[InterfaceSystemId]
					,[SourceEncounterId]
					,[EncounterDate]
					,[EncounterTime]
					,[EncounterType]
					,[Clinician]
					,[Location]
					,[Comment]
					,[ReasonForVisit_ICD9]
					,[ReasonForVisit_ICD10]
					,[ReasonForVisit_LOINC]
					,[ReasonForVisit_SNOMED]
					,PatientIdentifier
					,CreateDateTime
				)

			SELECT   [InterfacePatientEncounterId]
			        ,IPM.InterfacePatientId
			        ,ISS.SourceSystemId AS InterfaceSystemId
					,IPM.[EncounterIdentifier]
					,CONVERT(date,[EncounterDateTime])   As EncounterDate
                    ,left(CONVERT(TIME,[EncounterDateTime]),20) As EncounterTime
                    ,[EncounterType]
					,[Clinician]
					,[Location]
					,[Comment]
					,[ReasonForVisit] AS [ReasonForVisit_ICD9]
					,[ReasonForVisit] AS [ReasonForVisit_ICD10]
					,[ReasonForVisit] AS [ReasonForVisit_LOINC]
					,[ReasonForVisit] AS [ReasonForVisit_SNOMED]
		            ,IP.PatientIdentifier
					,IPM.CreateDateTime
			FROM [dbo].[vwPortal_InterfacePatientEncounter]  IPM WITH (NOLOCK)
			INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS WITH (NOLOCK) ON IPM.InterfaceSystemId = ISS.InterfaceSystemId
			INNER JOIN [dbo].[vwPortal_InterfacePatient] IP WITH (NOLOCK) ON IPM.InterfacePatientId = IP.InterfacePatientId
			WHERE ( IPM.[CreateDateTime] >=@lastDateTime )
		   
			
		  -- 3. [dbo].[EncounterType]
		  		MERGE INTO [dbo].[EncounterType] AS target
			USING (SELECT DISTINCT
						[EncounterType]
					FROM PatientEncounterProcessQueue 
					WHERE ISNULL([EncounterType],'')<> '')   AS source
			ON target.[Name]= source.[EncounterType] 
			WHEN NOT MATCHED BY TARGET THEN
			INSERT ([Name],[DisplayValue],DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
			VALUES (source.[EncounterType],source.[EncounterType],0,GETUTCDATE(),GETUTCDATE(),1,1);


	       -- 4. add index for speeding
	    	    
		 	IF EXISTS(SELECT NAME FROM sys.indexes WHERE NAME = 'CIX_#patientEncounter_PatientIDate')
				BEGIN
						ALTER INDEX [CIX_#patientEncounter_PatientIDate] ON PatientEncounterProcessQueue REBUILD; 
				END
           ELSE
			   BEGIN
				   CREATE NONCLUSTERED INDEX CIX_#patientEncounter_PatientIDate on PatientEncounterProcessQueue (PatientIdentifier,InterfaceSystemId) include (EncounterType,[EncounterDate],[EncounterTime]);
			   END



           -- 4. Dedup
	 	    SELECT *
			INTO #PatientEncounter
			FROM  (

		   	SELECT  [InterfacePatientEncounterId]
					,[InterfacePatientId] 
					,[InterfaceSystemId] as SourceSystemId
					,[SourceEncounterId]
					,[EncounterDate]
					,[EncounterTime]
					,[EncounterType]
					,[Clinician]
					,[Location]
					,[Comment]
					,[ReasonForVisit_ICD9]
					,[ReasonForVisit_ICD10]
					,[ReasonForVisit_LOINC]
					,[ReasonForVisit_SNOMED]
					,idRef.[LbPatientId]
					,ISNULL(ET.EncounterTypeID,0) EncounterTypeID
					,t1.CreateDateTime
					, ROW_NUMBER() OVER(PARTITION BY t1.InterfaceSystemId, idRef.LbPatientID,ET.EncounterTypeID, t1.[EncounterDate], t1.[EncounterTime] 
						ORDER BY t1.InterfacePatientEncounterId	DESC ) row_nbr
         
			FROM PatientEncounterProcessQueue t1		
			INNER JOIN dbo.PatientIdReference idRef ON idRef.SourceSystemId = t1.InterfaceSystemId
				  AND idRef.ExternalId =  t1.PatientIdentifier 
				  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
				  AND idRef.DeleteInd =0
            LEFT OUTER JOIN [dbo].[EncounterType] ET ON ET.[Name]=t1.EncounterType


            ) A
			WHERE row_nbr=1;


			-- get source records counts
			SET @RecordCountSource=@@ROWCOUNT;

			
			 IF (@fullLoadFlag=1)
			   BEGIN
				   Truncate table [dbo].[PatientEncounter];
			   END

			SELECT @RecordCountBefore=COUNT(1)
			FROM [dbo].[PatientEncounter];
                

			---for merge key
		    CREATE CLUSTERED INDEX CIX_#patientLabOrder_PatientID ON #PatientEncounter ([LbPatientID],[SourceSystemID],EncounterTypeID,EncounterDate,EncounterTime);


			-- 5. Merge to PatientEncounter
			
			MERGE [DBO].PatientEncounter AS target
		    USING #PatientEncounter AS [source] 
	        ON ([target].SourceSystemID = [source].SourceSystemId AND [target].PatientID = [source].[LbPatientID] AND
		     ISNULL(target.EncounterTypeID,0) = ISNULL(source.EncounterTypeID,0) 
			 AND ISNULL(target.[EncounterDate],'1900-01-01') = ISNULL(source.[EncounterDate],'1900-01-01') 
	         AND ISNULL(target.[EncounterTime],'') = ISNULL(source.[EncounterTime],'')
			)
			WHEN MATCHED  THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
			--[PatientId]				=source.[PatientId],
			[SourceSystemID]		=source.[SourceSystemID],
			[SourceEncounterId]		=source.[SourceEncounterId],
			[EncounterDate]			=source.[EncounterDate],
			[EncounterTime]			=source.[EncounterTime],
			[EncounterTypeID]		=source.[EncounterTypeID],
			[Clinician]				=source.[Clinician],
			[Location]				=source.[Location],
			[Comment]				=source.[Comment],
			[ReasonForVisit_ICD9]	=source.[ReasonForVisit_ICD9],
			[ReasonForVisit_ICD10]	=source.[ReasonForVisit_ICD10],
			[ReasonForVisit_LOINC]	=source.[ReasonForVisit_LOINC],
			[ReasonForVisit_SNOMED]	=source.[ReasonForVisit_SNOMED],
			--[CreateDateTime]		=source.[CreateDateTime],
			[ModifyDateTime]		=@Today,
			--[CreateLBUserId]		=source.[CreateLBUserId],
			[ModifyLBUserId]		=1
			WHEN NOT MATCHED THEN
			INSERT ([PatientId],[SourceSystemID],[SourceEncounterId],[EncounterDate],[EncounterTime],
					[EncounterTypeID],[Clinician],[Location],[Comment],[ReasonForVisit_ICD9],[ReasonForVisit_ICD10],
					[ReasonForVisit_LOINC],[ReasonForVisit_SNOMED],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId]
				   )
			VALUES (source.[LbPatientId],source.[SourceSystemID],source.[SourceEncounterId],source.[EncounterDate],source.[EncounterTime],
					source.[EncounterTypeID],source.[Clinician],source.[Location],source.[Comment],source.[ReasonForVisit_ICD9],source.[ReasonForVisit_ICD10],
					source.[ReasonForVisit_LOINC],source.[ReasonForVisit_SNOMED],CreateDateTime,@Today,1,1)
			--		OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientEncounterID,INSERTED.PatientEncounterID) PatientEncounterID, source.PatientID

          ;

		      -- get total records from PatientEncounter after merge 
		       			SELECT @RecordCountAfter=COUNT(1)
			            FROM PatientEncounter;

              -- Calculate records inserted and updated counts
	             SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	             SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			   SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
	               -- update maxSourceTimeStamp with max LR_CreateDateTime of PatientEncounterProcessQueue
				 UPDATE [Maintenance].dbo.[EDWTableLoadTracking]
				 SET [maxSourceTimeStampProcessed] = (SELECT MAX(convert(DATE,CreateDateTime)) FROM [dbo].[PatientEncounterProcessQueue] ),
					 [updateDateTime] =GETUTCDATE()
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName

            --clean tables
			DROP TABLE #PatientEncounter;
			
			

	END TRY

		BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END



GO
