SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproGetCompletePatientList]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2016-10-24
	INITIAL VER:	2.2.1
	MODULE:			GPRO		
	DESCRIPTION:	For the measure return the list of patients that are COMPLETE
	PARAMETERS:		GPro measure name 
	RETURN VALUE(s)/OUTPUT:	a table containing the patient id, rank, and gpro ids of patients ranked for this measure that are skipped
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-10-24		BR			LBPP-2026	Initial version 
	2.2.1		2016-11-09		BR			LBPP-2026	Added additional column to enable the patient-level status checking
														If the parameter is NULL it includes all measures
	2.2.1		2016-12-01		BR			LBPP-2028	Updated the PREV-13 calculation to include the additional fields added
	2.2.1		2017-01-12		BR			LBDM-1761	Fixed the calculation of CARE-3, it was picking up rows where med rec = no
	2.2.1		2017-01-16		BR			LBPP-2213	Fixed HF-6, if LVSD = 1 they are complete
	2.2.1		2017-01-24		BR						Added check for denominator exception in PREV-13 statin prescribed
	2.2.1		2017-02-07		BR						Undoing LBPP-2213, there is conflicting documentation
	2.2.1		2017-04-20		BR			LBPP-2467	Changed the way CARE-3 and DM-2 are calculated to accurately match the reports from CMS
=================================================================*/
(
	@GproMeasureName VARCHAR(20) = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

	-----------------------------------------------------------------------
	-- define variables
	-----------------------------------------------------------------------
	CREATE TABLE #completeList
	(
		LbPatientId INT,
		MeasureRank INT,
		GproPatientRankingId INT,
		GproPatientMeasureId INT,
		GproMeasureTypeId INT
		--UNIQUE CLUSTERED (LbPatientId)
	);

	-----------------------------------------------------------------------
	-- CARE-2
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'CARE-2'  OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.CareFallsRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'CARE-2'
		WHERE gpr.CareFallsRank IS NOT NULL 
			AND gpm.[MedicalRecordFound] = 2 AND gpm.[CareFallsConfirmed] = 2 AND gpm.[FallsScreening] IS NOT NULL AND gpm.[FallsScreening] IN (1,2,4)
	END

	-----------------------------------------------------------------------
	-- CARE-3
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'CARE-3' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.CareMeddocRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'CARE-3'
		WHERE gpr.CareMeddocRank IS NOT NULL 
			AND gpm.[MedicalRecordFound] = 2 
			--AND gpm.[CareMeddocConfirmed] = 2
			AND EXISTS (SELECT TOP 1 1 FROM GproPatientMedication med WHERE med.GproPatientMeasureId = gpm.GproPatientMeasureId)
			AND NOT EXISTS (SELECT TOP 1 1 FROM GproPatientMedication med WHERE med.GproPatientMeasureId = gpm.GproPatientMeasureId AND (med.CaremeddocVisitConfirmed IS NULL OR (med.CaremeddocVisitConfirmed IS NOT NULL AND med.CareMeddocVisitConfirmed = 2 AND med.MedicationDocumented IS NULL)))
			--AND EXISTS (SELECT TOP 1 1 FROM GproPatientMedication med WHERE med.GproPatientMeasureId = gpm.GproPatientMeasureId AND med.CareMeddocVisitConfirmed = 2)
	END

	-----------------------------------------------------------------------
	-- CAD-7
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'CAD-7' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.CadRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'CAD-7'
		WHERE gpr.CadRank IS NOT NULL 
			AND (gpm.[MedicalRecordFound] = 2 AND gpm.[CadConfirmed] = 2 AND gpm.[CadDiabetesLvsd] = 2 AND gpm.[CadAceArb] IS NOT NULL)
	END

	-----------------------------------------------------------------------
	-- DM-2
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'DM-2' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.DmRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'DM-2'
		WHERE gpm.[MedicalRecordFound] = 2 AND gpm.[DmConfirmed] = 2 
			AND (gpm.[DmHba1cTest] IN (1, 17) OR (gpm.[DmHba1cTest] = 2 AND gpm.[DmHba1cDate] IS NOT NULL AND gpm.[DmHba1cValue] >=0 AND gpm.[DmHba1cValue] < 26 AND [DmEyeExam] IS NOT NULL AND [DmEyeExam] IN (1, 2)))
	END

	-----------------------------------------------------------------------
	-- DM-7
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'DM-7' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.DmRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'DM-7'
		WHERE gpm.[MedicalRecordFound] = 2 
			AND gpm.[DmConfirmed] = 2 
			AND [DmEyeExam] IS NOT NULL 
			AND [DmEyeExam] IN (1, 2)
	END

	-----------------------------------------------------------------------
	-- HF-6
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'HF-6' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.HfRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'HF-6'
		WHERE gpr.HfRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 
			AND [HfConfirmed] = 2
			AND ([HfLvsd] = 2 AND [HfBetaBlocker] IS NOT NULL AND [HfBetaBlocker] IN (1,2,4,5,6))
	END

	-----------------------------------------------------------------------
	-- HTN-2
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'HTN-2' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.HtnRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'HTN-2'
		WHERE gpr.HtnRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 
			AND [HtnConfirmed] = 2
			AND ([HtnRecentBp] = 0 
				OR ([HtnRecentBp] = 1 AND [HtnBpDate] IS NOT NULL AND [HtnBpSystolic] IS NOT NULL 
					AND [HtnBpSystolic] >= 0 AND [HtnBpSystolic] <= 350 AND [HtnBpDiastolic] >= 0 AND [HtnBpDiastolic] <= 200))
	END

	-----------------------------------------------------------------------
	-- IVD-2
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'IVD-2' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.IvdRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'IVD-2'
		WHERE gpr.IvdRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 
			AND [IvdConfirmed] = 2 
			AND [IvdAntithrombotic] IS NOT NULL AND [IvdAntithrombotic] IN (0, 1)
	END

	-----------------------------------------------------------------------
	-- MH-1
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'MH-1' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.MhRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'MH-1'
		WHERE gpr.MhRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 
			AND [MhConfirmed] = 2 
			AND ([MhIndexPerformed] = 2 
				AND (([MhIndexTest] = 2 AND ([MhIndexDate] IS NOT NULL AND [MhIndexScore] IS NOT NULL AND [MhFollowupPerformed] = 1))
					OR ([MhIndexTest] = 2 AND [MhIndexDate] IS NOT NULL AND [MhIndexScore] IS NOT NULL AND [MhFollowupPerformed] = 2 AND [MhFollowupTest] = 1)
					OR ([MhIndexTest] = 2 AND [MhIndexDate] IS NOT NULL AND [MhIndexScore] IS NOT NULL AND [MhFollowupPerformed] = 2 AND [MhFollowupDate] IS NOT NULL AND [MhFollowupScore]IS NOT NULL)
					)
				)
	END

	-----------------------------------------------------------------------
	-- PREV-5
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-5' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcMammogramRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-5'
		WHERE gpr.PcMammogramRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 AND [PcMammogramConfirmed] = 2 AND [PcMammogramPerformed] IS NOT NULL AND [PcMammogramPerformed] IN (0, 1)
	END

	-----------------------------------------------------------------------
	-- PREV-6
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-6' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcColorectalRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-6'
		WHERE gpr.PcColorectalRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 AND [PcColorectalConfirmed] = 2 AND [PcColorectalPerformed] IS NOT NULL AND [PcColorectalPerformed] IN (0,1)
	END

	-----------------------------------------------------------------------
	-- PREV-7
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-7' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcFlushotRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-7'
		WHERE gpr.PcFluShotRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 AND [PcFluShotConfirmed] = 2 AND [PcFluShotReceived] IS NOT NULL AND [PcFluShotReceived] IN (1,2,4,5,6)
	END

	-----------------------------------------------------------------------
	-- PREV-8
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-8' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcPneumoshotRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-8'
		WHERE gpr.PcPneumoShotRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 AND [PcPneumoShotConfirmed] = 2 AND [PcPneumoShotReceived] IS NOT NULL AND [PcPneumoShotReceived] IN (0,1)
	END

	-----------------------------------------------------------------------
	-- PREV-9
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-9' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcBmiscreenRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-9'
		WHERE gpr.PcBmiScreenRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 
			AND [PcBmiScreenConfirmed] = 2 
			AND (([PcBmiCalculated] IS NOT NULL AND [PcBmiCalculated] = 1) OR ([PcBmiCalculated] =2 AND [PcBmiNormal] = 1) OR ([PcBmiCalculated] =2 AND [PcBmiNormal] = 0 AND [PcBmiPlan] IS NOT NULL AND [PcBmiPlan] IN (0,1)))
	END

	-----------------------------------------------------------------------
	-- PREV-10
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-10' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcTobaccouseRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-10'
		WHERE gpr.PcTobaccoUseRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 
			AND [PcTobaccoConfirmed] = 2 
			AND (([PcTobaccoScreen] IS NOT NULL AND [PcTobaccoScreen] IN (1,4,14)
				OR ([PcTobaccoScreen] = 2 AND [PcTobaccoCounsel] IS NOT NULL AND [PcTobaccoCounsel] IN (0,1))))
	END

	-----------------------------------------------------------------------
	-- PREV-11
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-11' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcBloodPressureRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-11'
		WHERE gpr.PcBloodPressureRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 
			AND [PcBloodPressureConfirmed] = 2 
			AND (([PcBloodPressureScreen] IS NOT NULL AND [PcBloodPressureScreen] IN (1,4,5) 
				OR ([PcBloodPressureScreen] = 2 AND [PcBloodPressureNormal] = 1)
				OR ([PcBloodPressureScreen] = 2 AND [PcBloodPressureNormal] = 0 AND [PcBloodPressureFollowUp] IS NOT NULL)))
	END

	-----------------------------------------------------------------------
	-- PREV-12
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-12' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcDepressionRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-12'
		WHERE gpr.PcDepressionRank IS NOT NULL
			AND [MedicalRecordFound] = 2 
			AND [PcDepressionConfirmed] = 2 
			AND (([PcDepressionScreen] IS NOT NULL AND [PcDepressionScreen] IN (1,4,5))
				OR ([PcDepressionScreen] = 2 AND [PcDepressionPositive] = 1)
				OR ([PcDepressionScreen] = 2 AND [PcDepressionPositive] = 2 AND [PcDepressionPlan] IS NOT NULL AND [PcDepressionPlan] IN (1,2)))
	END

	-----------------------------------------------------------------------
	-- PREV-13
	-----------------------------------------------------------------------
	IF @GproMeasureName = 'PREV-13' OR @GproMeasureName IS NULL
	BEGIN
		INSERT INTO #completeList
			(LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId,GproMeasureTypeId)
		SELECT
			gpr.LbPatientId, gpr.PcStatinRank, gpr.GproPatientRankingId, gpm.GproPatientMeasureId,gmt.GproMeasureTypeId
		FROM GproPatientMeasure gpm 
			INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId 
			INNER JOIN GproPatientRankingGproMeasureType gprgmt ON gprgmt.GproPatientRankingId = gpr.GproPatientRankingId 
			INNER JOIN GproMeasureType gmt ON gmt.GproMeasureTypeId = gprgmt.GproMeasureTypeId AND gmt.Name = 'PREV-13'
		WHERE gpr.PcStatinRank IS NOT NULL 
			AND [MedicalRecordFound] = 2 
			AND (([PcStatinAscvd] = 2 AND [PcStatinPrescribed] IS NOT NULL)
				OR ([PcStatinAscvd] = 23 AND [PcStatinLdlc] = 2 AND [PcStatinPrescribed] IS NOT NULL)
				OR ([PcStatinAscvd] = 23 AND [PcStatinLdlc] = 1 AND [PcStatinDiabetes] = 2 AND [PcStatinLdlcReading] = 2 AND [PcStatinPrescribed] IS NOT NULL)
				)
	END

	-----------------------------------------------------------------------
	-- Return the list of patients
	-----------------------------------------------------------------------
	SELECT LbPatientId, MeasureRank, GproPatientRankingId, GproPatientMeasureId, GproMeasureTypeId FROM #completeList
END

GO
