SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ================================================================
-- Author:		Youping
-- Create date: 2016-04-04
-- Description:	Load Data into EDW MedicationRouteType
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[ETLMedicationRouteType]

AS
BEGIN	
		DECLARE @Today DATE;

		SET @Today=GETUTCDATE();
		
	       
	   MERGE INTO MedicationRouteType AS target
			USING (SELECT distinct MedicationRoute
				   --FROM vwPortal_InterfacePatientMedication ipm
				   --WHERE CreateDateTime > @lastDateTime
				  FROM [dbo].[PatientMedicationProcessQueue]
				  where ISNULL(MedicationRoute,'')<> ''  ) AS source
			ON source.MedicationRoute = target.[Name]
			WHEN NOT MATCHED BY TARGET THEN
				INSERT ([Name],DisplayValue,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
				VALUES (source.MedicationRoute,source.MedicationRoute,0,@Today,@Today,1,1);
END
GO
