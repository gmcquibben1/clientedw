SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ClinicalDecisionPatientAllergyLoad]
@PatientId INT		--ID of the patient
AS
 
--This stored procedure will return all the anonymous diagnosis information for the clinical decisions 
--Evaluation Request
--LBPP - 1463 - Introduced in version 2.01
--CJL - 02.17.2016  -- First Cut


SELECT pa.PatientAllergyID, AllergyDescription, ActiveInd, pa.DeleteInd,  OnsetDate, 
	at.Name as 'AllergyType', ast.Name as 'Severity', ar.Name as 'Reaction'
FROM 
	PatientAllergy pa WITH (NOLOCK), 
	AllergyType at WITH (NOLOCK), 
	AllergySeverityType ast WITH (NOLOCK), 
	AllergyReactionType ar WITH (NOLOCK)
WHERE pa.PatientId  = @PatientId and at.DeleteInd = 0 
	AND pa.AllergyTypeID = at.AllergyTypeID AND ast.AllergySeverityTypeID = pa.AllergySeverityTypeID
	AND ar.AllergyReactionTypeID = pa.AllergyReactionTypeID



GO
