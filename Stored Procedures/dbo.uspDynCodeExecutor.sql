SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[uspDynCodeExecutor] @MainFilter VARCHAR(50) = '', @SubFilter1 VARCHAR(50) = NULL, @SubFilter2 VARCHAR(50) = NULL, @DisplayOnly BIT = 0
-- How to run examples :- EXEC [dbo].[uspDynCodeExecutor] @MainFilter = 'ClaimsImport',  @SubFilter1 = 'MSSP'
AS 

DECLARE @ExecStatement NVARCHAR(MAX)
DECLARE MyCur CURSOR LOCAL FAST_FORWARD FOR 
SELECT 
	Code2Execute 
FROM CodeBaseDynExecution 
WHERE EnableFlag = 'Y' 
AND MainFilter = @MainFilter  
AND ISNULL(SubFilter1,'') = ISNULL(@SubFilter1,ISNULL(SubFilter1,''))  
AND ISNULL(SubFilter2,'') = ISNULL(@SubFilter2,ISNULL(SubFilter2,''))
ORDER BY MainFilterExecutionOrderSno, SubFilter1ExecutionOrderSno,SubFilter2ExecutionOrderSno

OPEN MyCur
FETCH NEXT FROM MyCur INTO @ExecStatement

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @ExecStatement
	IF @DisplayOnly = 0 
	BEGIN
		EXECUTE SP_EXECUTESQL @ExecStatement
	END 
FETCH NEXT FROM MyCur INTO @ExecStatement 
END
CLOSE MyCur 
DEALLOCATE MyCur 
GO
