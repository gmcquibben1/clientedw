SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[internalHCCMeasurement1SG] @CompareDate DATE = '12/31/2017'
AS
BEGIN
--=============================================================
--             Initial Date: 10/30/2015
--             Initial Author: Mike Hoxter
-- Modified By: Lan
-- Modified Date: 2016-09-06
-- Description: switched to Jhacg11 table JhacgPatientDetail  
--=============================================================
SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'HCCPatDiags';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalHCCMeasurement1SG';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @COMMENT VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

	-- add triage variables
	DECLARE @ERRMSG VARCHAR(4000)=''
	DECLARE @DBNAME VARCHAR(100) =''
	SET @DBNAME = DB_Name()




BEGIN TRY 

DECLARE @CompareDateBegin DATE = dateadd(DAY, -365, @CompareDAte)

EXEC InternalDropTempTables

	SET @ERRMSG = 'Step 1 : build temp table : #PartB ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	/* Truncate Diag Table */
	TRUNCATE TABLE HCCPatDiags

SELECT DISTINCT LbPatientId,BENE_HIC_NUM,
CLM_FROM_DT, CLM_LINE_DGNS_CD AS CLM_DGNS_CD, [RNDRG_PRVDR_NPI_NUM], SourceFeed
INTO #PartB
FROM CCLF_5_PartB_Physicians
UNION ALL SELECT  LbPatientId,BENE_HIC_NUM,CLM_FROM_DT , [RNDRG_PRVDR_NPI_NUM], SourceFeed, LTRIM(RTRIM(CLM_DGNS_1_CD)) AS CLM_DGNS_CD FROM CCLF_5_PartB_Physicians WHERE CLM_DGNS_1_CD IS NOT NULL AND CLM_FROM_DT <= @CompareDate
UNION ALL SELECT  LbPatientId,BENE_HIC_NUM,CLM_FROM_DT , [RNDRG_PRVDR_NPI_NUM], SourceFeed,LTRIM(RTRIM(CLM_DGNS_2_CD)) AS CLM_DGNS_CD FROM CCLF_5_PartB_Physicians WHERE CLM_DGNS_2_CD IS NOT NULL AND CLM_FROM_DT <= @CompareDate  
UNION ALL SELECT LbPatientId,BENE_HIC_NUM, CLM_FROM_DT , [RNDRG_PRVDR_NPI_NUM], SourceFeed,LTRIM(RTRIM(CLM_DGNS_3_CD)) AS CLM_DGNS_CD FROM CCLF_5_PartB_Physicians WHERE CLM_DGNS_3_CD IS NOT NULL AND CLM_FROM_DT <= @CompareDate 
UNION ALL SELECT LbPatientId,BENE_HIC_NUM, CLM_FROM_DT , [RNDRG_PRVDR_NPI_NUM], SourceFeed,LTRIM(RTRIM(CLM_DGNS_4_CD)) AS CLM_DGNS_CD FROM CCLF_5_PartB_Physicians WHERE CLM_DGNS_4_CD IS NOT NULL AND CLM_FROM_DT <= @CompareDate 
UNION ALL SELECT LbPatientId,BENE_HIC_NUM, CLM_FROM_DT , [RNDRG_PRVDR_NPI_NUM], SourceFeed,LTRIM(RTRIM(CLM_DGNS_5_CD)) AS CLM_DGNS_CD FROM CCLF_5_PartB_Physicians WHERE CLM_DGNS_5_CD IS NOT NULL AND CLM_FROM_DT <= @CompareDate 
UNION ALL SELECT  LbPatientId,BENE_HIC_NUM,CLM_FROM_DT , [RNDRG_PRVDR_NPI_NUM], SourceFeed,LTRIM(RTRIM(CLM_DGNS_6_CD)) AS CLM_DGNS_CD FROM CCLF_5_PartB_Physicians WHERE CLM_DGNS_6_CD IS NOT NULL AND CLM_FROM_DT <= @CompareDate 
UNION ALL SELECT LbPatientId,BENE_HIC_NUM, CLM_FROM_DT ,[RNDRG_PRVDR_NPI_NUM], SourceFeed, LTRIM(RTRIM(CLM_DGNS_7_CD)) AS CLM_DGNS_CD FROM CCLF_5_PartB_Physicians WHERE CLM_DGNS_7_CD IS NOT NULL AND CLM_FROM_DT <= @CompareDate 
UNION ALL SELECT LbPatientId,BENE_HIC_NUM, CLM_FROM_DT ,[RNDRG_PRVDR_NPI_NUM], SourceFeed, LTRIM(RTRIM(CLM_DGNS_8_CD)) AS CLM_DGNS_CD FROM CCLF_5_PartB_Physicians WHERE CLM_DGNS_8_CD IS NOT NULL AND CLM_FROM_DT <= @CompareDate 


	SET @ERRMSG = 'Step 2 : build  table : HCCPatDiags ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	/*Insert All Patient Diagnosis */
	INSERT INTO HCCPatDiags (
	   ICD9
	  ,[Description]
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,PatientID
	  ,RenderingProviderNPI
	  ,SourceFeed
	)

	--From priot to aggregated diagnosis table

	SELECT DISTINCT
	TRY_CONVERT(VARCHAR(9),CCLF_4_PartA_Diagnosis.CLM_DGNS_CD) AS ICD9, 
	TRY_CONVERT(VARCHAR(56),dbo.HCC_Diagnosis.[SHORT DESCRIPTION]) AS DIAGDESC,
	TRY_CONVERT(INT,dbo.HCC_Diagnosis.[2014HCC]) AS HCC,
	TRY_CONVERT(VARCHAR(129),dbo.HCCFactors2014.[Disease GROUP]) AS DGROUP,
	dbo.HCCFactors2014.[2014 Factor] AS FACTOR,
	TRY_CONVERT(DATE,CCLF_4_PartA_Diagnosis.CLM_FROM_DT) AS CLMDATE, 
	TRY_CONVERT(VARCHAR(100),[CCLF_1_PartA_Header].LbPatientId) AS PatientID ,
	TRY_CONVERT(VARCHAR(25),[ATNDG_PRVDR_NPI_NUM]) AS NPI, [CCLF_1_PartA_Header].SourceFeed
	FROM CCLF_4_PartA_Diagnosis
	JOIN [CCLF_1_PartA_Header]
	ON [CCLF_1_PartA_Header].[CUR_CLM_UNIQ_ID] = CCLF_4_PartA_Diagnosis.[CUR_CLM_UNIQ_ID]
	JOIN HCC_Diagnosis 
	ON dbo.CCLF_4_PartA_Diagnosis.CLM_DGNS_CD = dbo.HCC_Diagnosis.[DIAGNOSIS CODE]
	JOIN HCCFactors2014 
	ON dbo.HCC_Diagnosis.[2014HCC] = dbo.HCCFactors2014.HCCNo
	WHERE dbo.CCLF_4_PartA_Diagnosis.CLM_FROM_DT <= @CompareDate 
	UNION
	SELECT DISTINCT 
	TRY_CONVERT(VARCHAR(9),PartB.CLM_DGNS_CD) AS ICD9, 
	TRY_CONVERT(VARCHAR(56),dbo.HCC_Diagnosis.[SHORT DESCRIPTION]) AS DIAGDESC,
	dbo.HCC_Diagnosis.[2014HCC] AS HCC,
	TRY_CONVERT(VARCHAR(129),dbo.HCCFactors2014.[Disease GROUP]) AS DGROUP,
	dbo.HCCFactors2014.[2014 Factor] AS FACTOR,
	CLM_FROM_DT AS CLMDATE, 
	LbPatientId AS PatientId , 
	[RNDRG_PRVDR_NPI_NUM] AS NPI, SourceFeed
  FROM #PartB PartB
	JOIN HCC_Diagnosis 
	ON PartB.CLM_DGNS_CD = dbo.HCC_Diagnosis.[DIAGNOSIS CODE]
	JOIN HCCFactors2014 
	ON dbo.HCC_Diagnosis.[2014HCC] = dbo.HCCFactors2014.HCCNo
	WHERE PartB.CLM_FROM_DT <= @CompareDate

	SET @ERRMSG = 'Step 3 : build  table : HCCPatAging ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	/* Truncate Aging Table */
	TRUNCATE TABLE HCCPatAging
 

	/* FILL MOST RECENT HCC */
	INSERT INTO HCCPatAging (
	   PatientID, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) SELECT PatientID,PatientID, HCCNo, DiseaseGroup, Factor, MAX(DateOfService), COUNT(*), 'Covered' FROM HCCPatDiags
	GROUP BY PatientID,PatientID, HCCNo, DiseaseGroup, Factor
 

 	SET @ERRMSG = 'Step 4 : build  table : #BeneAge ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	/***Retrieve Gender and Age***/
	IF(OBJECT_ID('tempdb..#BeneAge') IS NOT NULL)
	DROP TABLE #BeneAge
	SELECT DISTINCT t1.LbPatientId AS PatientId
		   ,t1.GenderId 
		   ,CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) AS Age
		   , CASE WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 34.9 THEN '0 to 34'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 34.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 44.9 THEN '35 to 44'
					  WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 44.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 54.9 THEN '45 to 54'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 54.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 59.9 THEN '55 to 59'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 59.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 64.9 THEN '60 to 64'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 64.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 69.9 THEN '65 to 69'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 69.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 74.9 THEN '70 to 74'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 74.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 79.9 THEN '75 to 79'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 79.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 84.9 THEN '80 to 84'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 84.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 89.9 THEN '85 to 89'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 89.9 AND CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) <= 94.9 THEN '90 to 94'
				WHEN CAST(DATEDIFF(yy,CONVERT(DATETIME, t1.BirthDate),GETDATE()) AS INT) > 94.9 THEN '95 and above'
				ELSE '' end AS 'AgeGroup'
	INTO #BeneAge         
	FROM OrgHierarchy t1 
 

 
 	SET @ERRMSG = 'Step 5 : build  table : HCCPatAgeFactor ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	TRUNCATE TABLE HCCPatAgeFactor
 


	/*Insert All the patients and their Age factor based on Gender*/

	INSERT INTO HCCPatAgeFactor (
	   PatientId
	  ,GenderId
	  ,Age
	  ,AgeGroup
	  ,AgeFactor
	)

	SELECT t1.*
		  ,CASE WHEN t1.GenderId = 2 THEN t2.Female 
			 ELSE t2.Male end AS 'AgeFactor'

	/*into HCCPatAgeFactor*/
	FROM #BeneAge t1 INNER JOIN
		 HCCAgeFactors2014 t2 /***this table is based on Table 1. 2014 CMS-HCC Model Relative Factors for Community and Institutional Beneficiaries***/
		 ON t1.AgeGroup = t2.AgeGroup 

	/***Drop HCC Factors based on Aging - anything over 1 year old is dropped due to aging ***/
	UPDATE HCCPatAging 
	SET ValidFlag = 'Not Covered'
	WHERE DATEDIFF("MONTH", DateOfService, @CompareDate) >12

	/***Drop HCC Factors based on Aging - anything over 1 year old is dropped due to aging ***/
  IF @CompareDate = getdate()
  BEGIN
	UPDATE HCCPatAging 
	SET ValidFlag = 'About to Expire'
	WHERE DATEDIFF("MONTH", DateOfService, @CompareDate) >10
	AND DATEDIFF("MONTH", DateOfService, @CompareDate) <12
  END

   	SET @ERRMSG = 'Step 6 : build  table : #HCCDROP ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	/***Build list of HCC Factors to Drop based on HCCHierarchy2014 ***/
	IF(OBJECT_ID('tempdb..#HCCDROP') IS NOT NULL)
	DROP TABLE #HCCDROP
 
	SELECT PatientID, HCCNo, B.HCC_Drop 
	INTO #HCCDROP
	FROM HCCPatAging A
	JOIN HCCHierarchy2014 B 
	ON B.HCC_Retain = A.HCCNo
	WHERE A.PatientID IS NOT NULL AND A.PatientID <> '' AND ValidFlag NOT IN ('Not Covered','About to Expire')

	/*** DROP HCC FACTORS TO "Another Diagnosis Takes Presidence" ***/
	UPDATE HCCPatAging SET
	  ValidFlag = 'Another Diagnosis Takes Presidence' -- varchar(50)
	FROM HCCPatAging H
	JOIN #HCCDROP D
	ON H.PatientID = D.PatientID AND H.HCCNo = D.HCC_Drop
 

    	SET @ERRMSG = 'Step 7 : build  table : #DualPats ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	/*** DROP #HCCDROP TEMP TABLE ***/
	DROP TABLE #HCCDROP
 
 SELECT DISTINCT LbPatientId, .177 AS DualRAFAddition
 INTO #DualPats
 FROM Patient_Summary_Pivot
 WHERE DualStatus > 0
 
	SELECT dbo.HCCPatAgeFactor.PatientID, SUM(isnull(Factor,0)) AS [TotalHCC], AVG(try_convert(DECIMAL(28,10), dbo.HCCPatAgeFactor.AgeFactor )) AS AgeFactor 
  , SUM(isnull(dp.DualRAFAddition,0)) AS [DualElibible]
  INTO #TOTALHCC
	FROM HCCPatAgeFactor
  LEFT JOIN HCCPatAging ON dbo.HCCPatAgeFactor.PatientId = dbo.HCCPatAging.PatientID
  LEFT JOIN #DualPats dp ON dbo.HCCPatAging.LbPatientId = dp.LbPatientId
	WHERE ValidFlag = 'Covered' OR dbo.HCCPatAging.id IS NULL
	GROUP BY dbo.HCCPatAgeFactor.PatientID

    	SET @ERRMSG = 'Step 8 : build  table : HCCPatRAFScore ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

  TRUNCATE TABLE HCCPatRAFScore

INSERT INTO HCCPatRAFScore (
   PatientId
  ,AgeFactor
  ,TotalHCCScore
  ,RAFScore
) 
SELECT PatientId, AgeFactor, TotalHCC, isnull(AgeFactor,0)+isnull(TotalHCC,0) +isnull(DualElibible,0)
FROM #TOTALHCC

	DROP TABLE #TOTALHCC

UPDATE HCCPatAging SET LbPatientId = PatientID 
WHERE LbPatientId IS NULL AND PatientID IS NOT NULL

UPDATE HCCPatAging SET PatientId = LbPatientID 
WHERE LbPatientId IS NOT NULL AND PatientID IS NULL


--	UPDATE HCCPatAging SET LbPatientId = IDR.LbPatientId
--	FROM HCCPatAging HCC
--	join PatientIdReference IDR ON HCC.PatientID = IDR.ExternalId

    	SET @ERRMSG = 'Step 8 : build  table : #HCC_CLINICAL ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	----MAKE HCC CLINICAL DATA
	-- 1,340,781 rows in 1 second
	SELECT HP.*, CAST(HP.PatientId AS INT) AS newPid
	INTO #TH1
	FROM dbo.HCCPatDiags HP
	-- 2s
	CREATE NONCLUSTERED INDEX IDX_TMP_TH1  ON [dbo].[#TH1] ([HCCNo],[newPid]) INCLUDE ([HCCPatDiags_ID],[PatientID])
	--drop index IDX_TMP_TH1  ON [dbo].[#TH1] 

	SELECT DC.DiagnosisCode, 
	DC.DiagnosisDescription, 
	HCCD.[2014HCC], 
	HCCF.[Disease GROUP], HCCF.[2014 Factor], 
	PD.DiagnosisDateTime, 	PD.PatientId, PD.RenderingProviderNPI, 
	SS.[Description] AS SourceFeed
	INTO #HCC_CLINICAL
	FROM PatientDiagnosis PD
	JOIN PatientDiagnosisDiagnosisCode PDDC ON PD.PatientDiagnosisId = PDDC.PatientDiagnosisId
	JOIN DiagnosisCode DC ON PDDC.DiagnosisCodeId = DC.DiagnosisCodeId
	JOIN HCC_Diagnosis HCCD ON DC.DiagnosisCode = HCCD.[DIAGNOSIS CODE] 
	JOIN HCCFactors2014 HCCF ON  HCCD.[2014HCC] = HCCF.HCCNo
	JOIN SourceSystem SS ON SS.SourceSystemID = PD.SourceSystemID
	LEFT JOIN #TH1 NODIAG ON HCCF.HCCNo = NODIAG.HCCNo AND PD.PatientId = NODIAG.NewPid
	WHERE NODIAG.HCCPatDiags_ID IS NULL


	--SELECT DC.DiagnosisCode, DC.DiagnosisDescription, HCCD.[2014HCC], HCCF.[Disease Group], HCCF.[2014 Factor], PD.DiagnosisDateTime, 
	--PD.PatientId, PD.RenderingProviderNPI, ss.[Description] as SourceFeed
	--INTO #HCC_CLINICAL
	--FROM PatientDiagnosis PD
	--JOIN PatientDiagnosisDiagnosisCode PDDC ON PD.PatientDiagnosisId = PDDC.PatientDiagnosisId
	--JOIN DiagnosisCode DC ON PDDC.DiagnosisCodeId = DC.DiagnosisCodeId
	--JOIN HCC_Diagnosis HCCD ON DC.DiagnosisCode = HCCD.[DIAGNOSIS CODE] 
	--JOIN HCCFactors2014 HCCF ON  HCCD.[2014HCC] = HCCF.HCCNo
	--join SourceSystem ss on SS.SourceSystemID = PD.SourceSystemID
	--LEFT JOIN HCCPatDiags NODIAG ON HCCF.HCCNo = NODIAG.HCCNo AND PD.PatientId = cast(NODIAG.PatientID as int)
	--WHERE NODIAG.HCCPatDiags_ID IS NULL




    	SET @ERRMSG = 'Step 9 : insert  table : HCCPatDiags ' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	INSERT INTO HCCPatDiags (
	   ICD9
	  ,[Description]
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,PatientID
	  ,RenderingProviderNPI, SourceFeed
	) SELECT DISTINCT 
	 DiagnosisCode, LEFT(DiagnosisDescription,200), [2014HCC], [Disease GROUP], [2014 Factor], DiagnosisDateTime, 
	PatientId, RenderingProviderNPI, SourceFeed
	FROM #HCC_CLINICAL
  
     	SET @ERRMSG = 'Step 10 : insert  table : HCCPatAging I' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	INSERT INTO HCCPatAging (
	   PatientId, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) 
	SELECT 
	 HCC.PatientId,HCC.PatientId, HCC.[2014HCC], HCC.[Disease GROUP], HCC.[2014 Factor], MAX(HCC.DiagnosisDateTime), COUNT(1), 'Recommendation (EMR Only)'
	FROM #HCC_CLINICAL HCC
	LEFT JOIN HCCPatAging AGE ON HCC.[Disease GROUP] = AGE.DiseaseGroup AND HCC.PatientId = AGE.LbPatientID
	WHERE AGE.id IS NULL
	 GROUP BY HCC.PatientId, HCC.[2014HCC], HCC.[Disease GROUP], HCC.[2014 Factor]

	DROP TABLE #HCC_CLINICAL

     	SET @ERRMSG = 'Step 11 : insert  table : HCCPatAging II' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	--MAKE HCC RECOMMENDATIONS (JOHNS HOPKINS) - COPD
	INSERT INTO HCCPatAging (
	   PatientId, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) 
	SELECT JH.patient_id ,JH.patient_id , HCC.HCCno , HCC.[Disease GROUP], HCC.[2014 Factor],getdate() AS DateofService, 
	1 AS [COUNT], 'Recommendation (LB-'+JH.chronic_obs_pul_dis_condition+')' AS ValidFlag
	FROM JhacgPatientDetail JH
	JOIN HCCFactors2014 HCC ON HCC.HCCNo = 111 
	--JOIN PatientIdReference IDR ON JH.patient_id = IDR.ExternalId
	LEFT JOIN HCCPatAging AGE ON  HCC.HCCNo = AGE.HCCNo AND JH.patient_id  = AGE.LbPatientId
	WHERE AGE.id IS NULL AND 
	JH.chronic_obs_pul_dis_condition <> 'NP'


     	SET @ERRMSG = 'Step 12 : insert  table : HCCPatAging III' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	--MAKE HCC RECOMMENDATIONS (JOHNS HOPKINS) - RA
	INSERT INTO HCCPatAging (
	   PatientId, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) 
	SELECT JH.patient_id ,JH.patient_id , HCC.HCCno , HCC.[Disease GROUP], HCC.[2014 Factor],getdate() AS DateofService, 
	1 AS [COUNT], 'Recommendation (LB-'+JH.rheumatoid_arthr_condition+')' AS ValidFlag
	FROM JhacgPatientDetail JH
	JOIN HCCFactors2014 HCC ON HCC.HCCNo = 40 
	--JOIN PatientIdReference IDR ON JH.patient_id = IDR.ExternalId
	LEFT JOIN HCCPatAging AGE ON  HCC.HCCNo = AGE.HCCNo AND JH.patient_id  = AGE.LbPatientId
	WHERE AGE.id IS NULL AND 
	JH.rheumatoid_arthr_condition <> 'NP'

     	SET @ERRMSG = 'Step 13 : insert  table : HCCPatAging IV' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	--MAKE HCC RECOMMENDATIONS (JOHNS HOPKINS) - Schizophrenia
	INSERT INTO HCCPatAging (
	   PatientId, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) 
	SELECT JH.patient_id ,JH.patient_id , HCC.HCCno , HCC.[Disease GROUP], HCC.[2014 Factor],getdate() AS DateofService, 
	1 AS [COUNT], 'Recommendation (LB-'+JH.schizophrenia_condition+')' AS ValidFlag
	FROM JhacgPatientDetail JH
	JOIN HCCFactors2014 HCC ON HCC.HCCNo = 57 
	--JOIN PatientIdReference IDR ON JH.patient_id = IDR.ExternalId
	LEFT JOIN HCCPatAging AGE ON  HCC.HCCNo = AGE.HCCNo AND JH.patient_id  = AGE.LbPatientId
	WHERE AGE.id IS NULL AND 
	JH.schizophrenia_condition <> 'NP'

     	SET @ERRMSG = 'Step 14 : insert  table : HCCPatAging V' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	--MAKE HCC RECOMMENDATIONS (JOHNS HOPKINS) - Diabetes
	INSERT INTO HCCPatAging (
	   PatientId, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) 
	SELECT JH.patient_id ,JH.patient_id , HCC.HCCno , HCC.[Disease GROUP], HCC.[2014 Factor],getdate() AS DateofService, 
	1 AS [COUNT], 'Recommendation (LB-'+JH.diabetes_condition+')' AS ValidFlag
	FROM JhacgPatientDetail JH
	JOIN HCCFactors2014 HCC ON HCC.HCCNo = 19  
	--JOIN PatientIdReference IDR ON JH.patient_id = IDR.ExternalId
	LEFT JOIN HCCPatAging AGE ON  HCC.HCCNo = AGE.HCCNo AND JH.patient_id  = AGE.LbPatientId
	WHERE AGE.id IS NULL AND 
	JH.diabetes_condition <> 'NP'

     	SET @ERRMSG = 'Step 15 : insert  table : HCCPatAging VI' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	--MAKE HCC RECOMMENDATIONS (JOHNS HOPKINS) - CHF
	INSERT INTO HCCPatAging (
	   PatientId, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) 
	SELECT JH.patient_id ,JH.patient_id , HCC.HCCno , HCC.[Disease GROUP], HCC.[2014 Factor],getdate() AS DateofService, 
	1 AS [COUNT], 'Recommendation (LB-'+JH.cong_heart_fail_condition+')' AS ValidFlag
	FROM JhacgPatientDetail JH
	JOIN HCCFactors2014 HCC ON HCC.HCCNo = 85  
	--JOIN PatientIdReference IDR ON JH.patient_id = IDR.ExternalId
	LEFT JOIN HCCPatAging AGE ON  HCC.HCCNo = AGE.HCCNo AND JH.patient_id  = AGE.LbPatientId
	WHERE AGE.id IS NULL AND 
	JH.cong_heart_fail_condition <> 'NP'

     	SET @ERRMSG = 'Step 16 : insert  table : HCCPatAging VII' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	--MAKE HCC RECOMMENDATIONS (JOHNS HOPKINS) - HIV
	INSERT INTO HCCPatAging (
	   PatientId, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) 
	SELECT JH.patient_id ,JH.patient_id , HCC.HCCno , HCC.[Disease GROUP], HCC.[2014 Factor],getdate() AS DateofService, 
	1 AS [COUNT], 'Recommendation (LB-'+JH.human_imm_virus_condition+')' AS ValidFlag
	FROM JhacgPatientDetail JH
	JOIN HCCFactors2014 HCC ON HCC.HCCNo = 1  
	--JOIN PatientIdReference IDR ON JH.patient_id = IDR.ExternalId
	LEFT JOIN HCCPatAging AGE ON  HCC.HCCNo = AGE.HCCNo AND JH.patient_id  = AGE.LbPatientId
	WHERE AGE.id IS NULL AND 
	JH.human_imm_virus_condition <> 'NP'

     	SET @ERRMSG = 'Step 17 : insert  table : HCCPatAging VIII' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	--MAKE HCC RECOMMENDATIONS (JOHNS HOPKINS) - Parkinsons
	INSERT INTO HCCPatAging (
	   PatientId, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) 
	SELECT JH.patient_id ,JH.patient_id , HCC.HCCno , HCC.[Disease GROUP], HCC.[2014 Factor],getdate() AS DateofService, 
	1 AS [COUNT], 'Recommendation (LB-'+JH.parkinsons_dis_condition+')' AS ValidFlag
	FROM JhacgPatientDetail JH
	JOIN HCCFactors2014 HCC ON HCC.HCCNo = 78  
	--JOIN PatientIdReference IDR ON JH.patient_id = IDR.ExternalId
	LEFT JOIN HCCPatAging AGE ON  HCC.HCCNo = AGE.HCCNo AND JH.patient_id  = AGE.LbPatientId
	WHERE AGE.id IS NULL AND 
	JH.parkinsons_dis_condition <> 'NP'

     	SET @ERRMSG = 'Step 18 : insert  table : HCCPatAging 9' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

	--MAKE HCC RECOMMENDATIONS (JOHNS HOPKINS) - Renal
	INSERT INTO HCCPatAging (
	   PatientId, LbPatientId
	  ,HCCNo
	  ,DiseaseGroup
	  ,Factor
	  ,DateOfService
	  ,[COUNT]
	  ,ValidFlag
	) 
	SELECT JH.patient_id ,JH.patient_id , HCC.HCCno , HCC.[Disease GROUP], HCC.[2014 Factor],getdate() AS DateofService, 
	1 AS [COUNT],  'Recommendation (LB-'+JH.bipolar_disorder_condition+')' AS ValidFlag
	FROM JhacgPatientDetail JH
	JOIN HCCFactors2014 HCC ON HCC.HCCNo = 58  
	--JOIN PatientIdReference IDR ON JH.patient_id = IDR.ExternalId
	LEFT JOIN HCCPatAging AGE ON  HCC.HCCNo = AGE.HCCNo AND JH.patient_id  = AGE.LbPatientId
	WHERE AGE.id IS NULL AND 
	JH.bipolar_disorder_condition <> 'NP' 

	     	SET @ERRMSG = 'Step 19 : update  table : HCCPatAging 10' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

--UPDATE PatientId based on LbPatientId
--	update HCCPatAging set PatientID = O.BENE_HIC_NUM
--	FROM HCCPatAging HCC JOIN OrgHierarchy O ON HCC.LbPatientId = O.LbPatientId
--	where HCC.PatientId IS NULL
UPDATE HCCPatAging SET LbPatientId = PatientID 
WHERE LbPatientId IS NULL AND PatientID IS NOT NULL

UPDATE HCCPatAging SET PatientId = LbPatientID 
WHERE LbPatientId IS NOT NULL AND PatientID IS NULL

   	SET @ERRMSG = 'Step 20 : insert  table : HCCPatAging 10' 
	INSERT INTO Maintenance.DBO.ETLExecutionLog ([ETLDatabase] ,[ETLSPName], [DebugMsg],[StartTime],[FinishTime])
     VALUES( @DBNAME, 'internalHCCMeasurement1SG_Sam', @ERRMSG, GETUTCDATE(), GETUTCDATE() );

INSERT INTO HCCPatDiags (
   ICD9
  ,[Description]
  ,HCCNo
  ,DiseaseGroup
  ,Factor
  ,PatientID
  ,LbPatientId
  ,SourceFeed
) SELECT 'NoDiag', ValidFlag, HCCNo, DiseaseGroup, Factor, LbPatientId, PatientID, 'Johns Hopkins ACG' 
FROM HCCPatAging
WHERE ValidFlag LIKE 'Recommendation%'

	-----Log Information
		
	 
		SELECT @InsertedRecord=COUNT(1)
		FROM [HCCPatAging] ;

          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @COMMENT =LEFT(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@COMMENT,@EDWtableName;
				
	END CATCH


END
GO
