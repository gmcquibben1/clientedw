SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GproMeasureByTaskingSave]
AS
BEGIN
	------------------------------------------------------------
	-- Get the care gap action type w/ dynamic SQL because
	-- there is no view to get it
	------------------------------------------------------------
	IF OBJECT_ID(N'tempdb..#LocalCareGapActionType') IS NOT NULL 
		DROP TABLE #LocalCareGapActionType;

	CREATE TABLE #LocalCareGapActionType
	(
		CareGapActionTypeId INT,
		Name VARCHAR(250)
	);

	DECLARE @EdwDbName VARCHAR(200) = DB_NAME();
	DECLARE @TranDbName VARCHAR(200) = REPLACE(@edwDbName,'Edw','Lbportal') 
	DECLARE @Sql NVARCHAR(4000);
	SET @Sql = 'SELECT * INTO #LocalCareGapActionType FROM ' + @TranDbName + '..CareGapActionType';
	EXEC (@Sql);

	------------------------------------------------------------
	-- CARE-2 Falls 
	------------------------------------------------------------
	UPDATE gpm
	SET gpm.CareFallsConfirmed = 2,
		gpm.FallsScreening = 2,
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1		
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN vwPortal_Task t ON t.PatientId = gpr.LbPatientId 
		INNER JOIN vwPortal_CareGap cg ON t.TaskId = cg.TaskId
		INNER JOIN vwPortal_CareGapType cgt on cgt.CareGapTypeId = cg.CareGapTypeId and cgt.Name = 'ACO Measures Program-2015'
		INNER JOIN vwPortal_CareGapRuleType cgrt on cgrt.CareGapTypeId = cg.CareGapTypeId AND cgrt.Name = 'Falls: Screening for Fall Risk ACO #13'
		INNER JOIN #LocalCareGapActionType cgat on cgat.CareGapActionTypeId = cg.CareGapActionTypeId AND cgat.Name = 'Certify'
	WHERE gpm.CareFallsRank IS NOT NULL 
		AND (gpm.CareFallsConfirmed = 2 OR gpm.CareFallsConfirmed IS NULL)
		AND gpm.FallsScreening IS NULL


	------------------------------------------------------------
	-- PREV-5 Mammogram 
	------------------------------------------------------------
	UPDATE gpm
	SET gpm.PcMammogramConfirmed = 2,
		gpm.PcMammogramPerformed = 2,
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1		
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN vwPortal_Task t ON t.PatientId = gpr.LbPatientId 
		INNER JOIN vwPortal_CareGap cg ON t.TaskId = cg.TaskId
		INNER JOIN vwPortal_CareGapType cgt on cgt.CareGapTypeId = cg.CareGapTypeId and cgt.Name = 'ACO Measures Program-2015'
		INNER JOIN vwPortal_CareGapRuleType cgrt on cgrt.CareGapTypeId = cg.CareGapTypeId AND cgrt.Name = 'Mammography Screening ACO #20'
		INNER JOIN #LocalCareGapActionType cgat on cgat.CareGapActionTypeId = cg.CareGapActionTypeId AND cgat.Name = 'Certify'
	WHERE gpm.PcMammogramRank IS NOT NULL 
		AND (gpm.PcMammogramConfirmed = 2 OR gpm.PcMammogramConfirmed IS NULL)
		AND gpm.PcMammogramPerformed IS NULL

	------------------------------------------------------------
	-- PREV-6 colorectal
	------------------------------------------------------------
	UPDATE gpm
	SET gpm.PcColorectalConfirmed = 2,
		gpm.PcColorectalPerformed = 2,
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1		
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN vwPortal_Task t ON t.PatientId = gpr.LbPatientId 
		INNER JOIN vwPortal_CareGap cg ON t.TaskId = cg.TaskId
		INNER JOIN vwPortal_CareGapType cgt on cgt.CareGapTypeId = cg.CareGapTypeId and cgt.Name = 'ACO Measures Program-2015'
		INNER JOIN vwPortal_CareGapRuleType cgrt on cgrt.CareGapTypeId = cg.CareGapTypeId AND cgrt.Name = 'Colorectal Cancer Screening ACO #19'
		INNER JOIN #LocalCareGapActionType cgat on cgat.CareGapActionTypeId = cg.CareGapActionTypeId AND cgat.Name = 'Certify'
	WHERE gpm.PcColorectalRank IS NOT NULL 
		AND (gpm.PcColorectalConfirmed = 2 OR gpm.PcColorectalConfirmed IS NULL)
		AND gpm.PcColorectalPerformed IS NULL

	------------------------------------------------------------
	-- DM-7 Eye Exam
	------------------------------------------------------------
	UPDATE gpm
	SET gpm.DmConfirmed = 2,
		gpm.DmEyeExam = 2,
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1		
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN vwPortal_Task t ON t.PatientId = gpr.LbPatientId 
		INNER JOIN vwPortal_CareGap cg ON t.TaskId = cg.TaskId
		INNER JOIN vwPortal_CareGapType cgt on cgt.CareGapTypeId = cg.CareGapTypeId and cgt.Name = 'ACO Measures Program-2015'
		INNER JOIN vwPortal_CareGapRuleType cgrt on cgrt.CareGapTypeId = cg.CareGapTypeId AND cgrt.Name = 'Diabetic Retinal Screening ACO #41'
		INNER JOIN #LocalCareGapActionType cgat on cgat.CareGapActionTypeId = cg.CareGapActionTypeId AND cgat.Name = 'Certify'
	WHERE gpm.DmRank IS NOT NULL 
		AND (gpm.DmConfirmed = 2 OR gpm.DmConfirmed IS NULL)
		AND gpm.DmEyeExam IS NULL


	------------------------------------------------------------
	-- PREV pneumoccocal vaccine
	------------------------------------------------------------
	UPDATE gpm
	SET gpm.PcPneumoShotConfirmed = 2,
		gpm.PcPneumoShotReceived = 2,
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1		
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN vwPortal_Task t ON t.PatientId = gpr.LbPatientId 
		INNER JOIN vwPortal_CareGap cg ON t.TaskId = cg.TaskId
		INNER JOIN vwPortal_CareGapType cgt on cgt.CareGapTypeId = cg.CareGapTypeId and cgt.Name = 'ACO Measures Program-2015'
		INNER JOIN vwPortal_CareGapRuleType cgrt on cgrt.CareGapTypeId = cg.CareGapTypeId AND cgrt.Name = 'Pneumococcal Vaccination ACO #15'
		INNER JOIN #LocalCareGapActionType cgat on cgat.CareGapActionTypeId = cg.CareGapActionTypeId AND cgat.Name = 'Certify'
	WHERE gpm.PcPneumoShotRank IS NOT NULL 
		AND (gpm.PcPneumoShotConfirmed = 2 OR gpm.PcPneumoShotConfirmed IS NULL)
		AND gpm.PcPneumoShotReceived IS NULL

	------------------------------------------------------------
	-- PREV flu vaccine
	------------------------------------------------------------
	UPDATE gpm
	SET gpm.PcFluShotConfirmed = 2,
		gpm.PcFluShotReceived = 2,
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1		
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN vwPortal_Task t ON t.PatientId = gpr.LbPatientId 
		INNER JOIN vwPortal_CareGap cg ON t.TaskId = cg.TaskId
		INNER JOIN vwPortal_CareGapType cgt on cgt.CareGapTypeId = cg.CareGapTypeId and cgt.Name = 'ACO Measures Program-2015'
		INNER JOIN vwPortal_CareGapRuleType cgrt on cgrt.CareGapTypeId = cg.CareGapTypeId AND cgrt.Name = 'Influenza Immunization ACO #14'
		INNER JOIN #LocalCareGapActionType cgat on cgat.CareGapActionTypeId = cg.CareGapActionTypeId AND cgat.Name = 'Certify'
	WHERE gpm.PcFluShotRank IS NOT NULL 
		AND (gpm.PcFluShotConfirmed = 2 OR gpm.PcFluShotConfirmed IS NULL)
		AND gpm.PcFluShotReceived IS NULL


	IF OBJECT_ID(N'tempdb..#LocalCareGapActionType') IS NOT NULL 
		DROP TABLE #LocalCareGapActionType;
END
GO
