SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientClaimPartBProfessionalLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT DISTINCT
		pa.ClaimId, 
		pa.ClaimLinenum,
		pa.PatientId,
		pa.ClaimFromDate, 
		pa.ClaimThruDate, 
		pa.SpecialtyCode, 
		pa.SpecialtyDescription,
		pa.CPT, 
		--pa.CPTGroup1, 
		--pa.CPTGroup2, 
		--pa.CPTGroup,
		pa.CPTDescription,
		pa.CPTMod1, 
		pa.CPTMod2, 
		pa.CPTMod3, 
		pa.CPTMod4, 
		pa.CPTMod5,
		pa.RenderingNPI, 
		pa.RenderingName, 
		pa.PaidAmt, 
		pa.PrimaryDiag,
		pa.DiagnosisDescription
		--pa.PrimaryDiagGroup1, 
		--pa.PrimaryDiagGroup2, 
		--pa.PrimaryDiagGroup3
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientClaimPartBProfessional pa ON pa.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (pa.ClaimFromDate > @dtmStartDate)
 
END
GO
