SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PatientDiagnosisLoad] 
	@PatientId INTEGER
AS

SELECT       
	dbo.DiagnosisCode.DiagnosisCode,
	dbo.DiagnosisCode.DiagnosisCodeDisplay,
	dbo.DiagnosisCode.DiagnosisDescription,
	dbo.SourceSystem.Description as SourceSystem,
	dbo.DiagnosisType.DisplayValue as DiagnosisType,
	dbo.DiagnosisCodingSystemType.DisplayValue as DiagnosisCodingSystemType,
	dbo.DiagnosisStatusType.DisplayValue as DiagnosisStatusType,
	dbo.DiagnosisSeverityType.DisplayValue as DiagnosisSeverityType,
	dbo.PatientDiagnosis.DiagnosisConfidentialityInd, 
	dbo.DiagnosisPriorityType.DisplayValue as DiagnosisPriorityType,
	dbo.DiagnosisClassificationType.DisplayValue as DiagnosisClassificationType,
	dbo.PatientDiagnosis.DiagnosisDateTime,
	dbo.PatientDiagnosis.DiagnosisOnsetDate,
	dbo.PatientDiagnosis.DiagnosisResolvedDate,
	dbo.PatientDiagnosis.DiagnosisComment,
	dbo.PatientDiagnosis.ActiveInd,
	dbo.PatientDiagnosis.ChronicInd,
	dbo.PatientDiagnosis.ProviderId,
	dbo.PatientDiagnosis.Clinician,
	Row_number() OVER(PARTITION BY PatientDiagnosis.SourceSystemId,PatientDiagnosis.PatientId, PatientDiagnosis.PatientDiagnosisId, PatientDiagnosis.DiagnosisDateTime
					ORDER BY PatientDiagnosis.PatientDiagnosisId DESC ) AS rowNbr
into #tmpPatientDiagnosis
FROM dbo.PatientDiagnosis INNER JOIN
	 dbo.PatientDiagnosisDiagnosisCode ON PatientDiagnosis.PatientDiagnosisId = PatientDiagnosisDiagnosisCode.PatientDiagnosisId INNER JOIN
	 dbo.DiagnosisCode ON PatientDiagnosisDiagnosisCode.DiagnosisCodeId = DiagnosisCode.DiagnosisCodeId INNER JOIN 
	 dbo.DiagnosisCodingSystemType ON DiagnosisCode.DiagnosisCodingSystemTypeId = DiagnosisCodingSystemType.DiagnosisCodingSystemTypeId LEFT OUTER JOIN
	 dbo.DiagnosisType ON PatientDiagnosis.DiagnosisTypeId = DiagnosisType.DiagnosisTypeId LEFT OUTER JOIN 
	 dbo.DiagnosisStatusType ON PatientDiagnosis.DiagnosisStatusTypeId = DiagnosisStatusType.DiagnosisStatusTypeId LEFT OUTER JOIN 
	 dbo.DiagnosisSeverityType ON PatientDiagnosis.DiagnosisSeverityTypeId = DiagnosisSeverityType.DiagnosisSeverityTypeID LEFT OUTER JOIN 
	 dbo.DiagnosisPriorityType ON PatientDiagnosis.DiagnosisPriorityTypeId = DiagnosisPriorityType.DiagnosisPriorityTypeId LEFT OUTER JOIN 
	 dbo.DiagnosisClassificationType ON PatientDiagnosis.DiagnosisClassificationTypeId = DiagnosisClassificationType.DiagnosisClassificationTypeId LEFT OUTER JOIN 
 	 dbo.SourceSystem ON PatientDiagnosis.SourceSystemId = SourceSystem.SourceSystemId 

WHERE dbo.PatientDiagnosis.PatientId = @Patientid AND dbo.PatientDiagnosis.DeleteInd = 0 AND 
		dbo.PatientDiagnosis.ActiveInd = 1




select DiagnosisCode,
	DiagnosisCodeDisplay,
	DiagnosisDescription,
	SourceSystem,
	DiagnosisType,
	DiagnosisCodingSystemType,
	DiagnosisStatusType,
	DiagnosisSeverityType,
	DiagnosisConfidentialityInd, 
	DiagnosisPriorityType,
	DiagnosisClassificationType,
	DiagnosisDateTime,
	Case when Year(DiagnosisOnsetDate)=1900 then NULL else  DiagnosisOnsetDate END as DiagnosisOnsetDate,
	DiagnosisResolvedDate,
	DiagnosisComment,
	ActiveInd,
	ChronicInd,
	ProviderId,
	Clinician

from #tmpPatientDiagnosis where rowNbr=1

ORDER BY DiagnosisDateTime DESC, DiagnosisDescription ASC



GO
