SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwDiagnosis] 
@Debug int=2
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES FOR NULL
UPDATE PatientDiagnosisProcessQueue SET Clinician = '' WHERE Clinician IS NULL
UPDATE PatientDiagnosisProcessQueue SET EncounterId = 0 WHERE EncounterId IS NULL

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId,LbPatientID, DiagnosisDateTime, DiagnosisCodingSystemTypeId, DiagnosisCodeId --, Clinician ,    EncounterId 
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				--LEN(ISNULL(DiagnosisComment,''))  -- THE BIGGER THE DiagnosisComment THE BETTER IT IS 
				InterfacePatientDiagnosisId
				DESC 
			) SNO
	FROM PatientDiagnosisProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientDiagnosisProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientDiagnosisId INT, PatientID INT,DiagnosisDateTime DateTime, 
						   Clinician VARCHAR(150), EncounterId Bigint, SourceSystemId Int,
						   ExternalReferenceIdentifier Int)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientDiagnosisId,PatientID,DiagnosisDateTime,
						  Clinician,EncounterId,SourceSystemId,
						  ExternalReferenceIdentifier)
SELECT
						  MergeAction,OrigPatientID,PatientDiagnosisId,PatientID,DiagnosisDateTime,
						  Clinician,EncounterId,SourceSystemId,
						  ExternalReferenceIdentifier
FROM
(
MERGE [DBO].[PatientDiagnosis] AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,DiagnosisTypeID,DiagnosisStatusTypeID,DiagnosisSeverityTypeID,DiagnosisConfidentialityInd,DiagnosisPriorityTypeId,DiagnosisClassificationTypeId,
		DiagnosisDateTime DiagnosisDateTime,DiagnosisOnsetDate,DiagnosisResolvedDate,DiagnosisComment,EncounterID,NULL AS ProviderID,Clinician,1 As ActiveInd, 0 AS DeleteInd,
		GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId,EncounterIdentifier,
		BINARY_CHECKSUM(DiagnosisCodeId,DiagnosisCodingSystemTypeId) AS ExternalReferenceIdentifier
		FROM [dbo].PatientDiagnosisProcessQueue PPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  ([OrigPatientId],[PatientId],[SourceSystemID],[DiagnosisTypeID],	[DiagnosisStatusTypeID],[DiagnosisSeverityTypeID],[DiagnosisConfidentialityInd],
  	   [DiagnosisPriorityTypeId],[DiagnosisClassificationTypeId],[DiagnosisDateTime],[DiagnosisOnsetDate],[DiagnosisResolvedDate],[DiagnosisComment],	
	   [EncounterID],[ProviderID],	[Clinician],	[ActiveInd],	[DeleteInd],	[CreateDateTime],	[ModifyDateTime],	[CreateLBUserId],[ModifyLBUserId], [EncounterIdentifier], [ExternalReferenceIdentifier]
	   )
ON (target.[SourceSystemID] = source.[SourceSystemID] AND target.PatientID = source.PatientID 
    AND ISNULL(target.DiagnosisDateTime,'2099-12-31') = ISNULL(source.DiagnosisDateTime,'2099-12-31') AND target.[ExternalReferenceIdentifier] = source.[ExternalReferenceIdentifier] 
	--AND ISNULL(target.Clinician,'') = ISNULL(source.Clinician,'')  AND ISNULL(target.Clinician,'') = ISNULL(source.Clinician,'') AND target.EncounterId = source.EncounterId
	 )
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
	--[PatientId]		=source.[PatientId],
	[SourceSystemID]		=source.[SourceSystemID],
	[DiagnosisTypeID]		=source.[DiagnosisTypeID],
	[DiagnosisStatusTypeID]		=source.[DiagnosisStatusTypeID],
	[DiagnosisSeverityTypeID]		=source.[DiagnosisSeverityTypeID],
	[DiagnosisConfidentialityInd]		=source.[DiagnosisConfidentialityInd],
	[DiagnosisPriorityTypeId]		=source.[DiagnosisPriorityTypeId],
	[DiagnosisClassificationTypeId]		=source.[DiagnosisClassificationTypeId],
	[DiagnosisDateTime]		=source.[DiagnosisDateTime],
	[DiagnosisOnsetDate]		=source.[DiagnosisOnsetDate],
	[DiagnosisResolvedDate]		=source.[DiagnosisResolvedDate],
	[DiagnosisComment]		=source.[DiagnosisComment],
	[EncounterID]		=source.[EncounterID],
	[ProviderID]		=source.[ProviderID],
	[Clinician]		=source.[Clinician],
	[ActiveInd]		=source.[ActiveInd],
	[DeleteInd]		=source.[DeleteInd],
	[ModifyDateTime]		=source.[ModifyDateTime],
	--[CreateLBUserId]		=source.[CreateLBUserId],
	[ModifyLBUserId]		=source.[ModifyLBUserId],
	[EncounterIdentifier] = source.[EncounterIdentifier]
WHEN NOT MATCHED THEN
INSERT ([PatientId],[SourceSystemID],[DiagnosisTypeID],	[DiagnosisStatusTypeID],[DiagnosisSeverityTypeID],[DiagnosisConfidentialityInd],
  	   [DiagnosisPriorityTypeId],[DiagnosisClassificationTypeId],[DiagnosisDateTime],[DiagnosisOnsetDate],[DiagnosisResolvedDate],[DiagnosisComment],	
	   [EncounterID],[ProviderID],	[Clinician],	[ActiveInd],	[DeleteInd],	[CreateDateTime],	[ModifyDateTime],	[CreateLBUserId],[ModifyLBUserId],[ExternalReferenceIdentifier],[EncounterIdentifier]
	   )
VALUES (source.PatientId,source.SourceSystemID,source.DiagnosisTypeID,	source.DiagnosisStatusTypeID,source.DiagnosisSeverityTypeID,source.DiagnosisConfidentialityInd,
  	   source.DiagnosisPriorityTypeId,source.DiagnosisClassificationTypeId,source.DiagnosisDateTime,source.DiagnosisOnsetDate,source.DiagnosisResolvedDate,source.DiagnosisComment,	
	   source.EncounterID,source.ProviderID,source.Clinician,source.ActiveInd,source.DeleteInd,	source.CreateDateTime,source.ModifyDateTime,source.CreateLBUserId,source.ModifyLBUserId,source.ExternalReferenceIdentifier, source.EncounterIdentifier)
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientDiagnosisId,INSERTED.PatientDiagnosisId) PatientDiagnosisId, source.PatientID,source.DiagnosisDateTime,
							source.Clinician, Source.EncounterId,source.SourceSystemID,
							source.ExternalReferenceIdentifier
) x;

UPDATE PDPQ
SET PatientDiagnosisID = RT.PatientDiagnosisID
FROM PatientDiagnosisProcessQueue PDPQ
INNER JOIN #ResultsTemp RT  ON PDPQ.InterfaceSystemId = RT.SourceSystemID AND PDPQ.LbPatientID = RT.OrigPatientId AND PDPQ.DiagnosisDateTime =  RT.DiagnosisDateTime AND BINARY_CHECKSUM(PDPQ.DiagnosisCodeId,PDPQ.DiagnosisCodingSystemTypeId)  = RT.ExternalReferenceIdentifier
							--AND ISNULL(PDPQ.Clinician,'') = ISNULL(RT.Clinician,'') AND PDPQ.EncounterId =  RT.EncounterId 
			AND (RT.MergeAction = 'INSERT' OR RT.OrigPatientId = RT.PatientId)

IF OBJECT_ID('PatientDiagnosis_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientDiagnosis_PICT(PatientDiagnosisId,PatientId,SwitchDirectionId)
	SELECT RT.PatientDiagnosisId, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

MERGE [DBO].PatientDiagnosisDiagnosisCode AS target
USING (	
		SELECT DISTINCT PatientDiagnosisId,DiagnosisCodingSystemTypeId,DiagnosisCodeId --,DiagnosisDescription
		FROM [dbo].PatientDiagnosisProcessQueue PPQ WHERE PatientDiagnosisId IS NOT NULL
	  ) AS source 
	  (PatientDiagnosisId,DiagnosisCodingSystemTypeId,DiagnosisCodeId)
ON (target.PatientDiagnosisID = source.PatientDiagnosisID AND Target.DiagnosisCodeId = source.DiagnosisCodeId )
--WHEN MATCHED THEN 
--    UPDATE SET 
--	DiagnosisDescription	 = source.DiagnosisDescription
WHEN NOT MATCHED THEN
INSERT (PatientDiagnosisId,DiagnosisCodingSystemTypeId,DiagnosisCodeId)
VALUES (source.PatientDiagnosisId,source.DiagnosisCodingSystemTypeId,source.DiagnosisCodeId);

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientDiagnosisProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'Diagnosis',GetDate()
FROM PatientDiagnosisProcessQueue
WHERE ProcessedInd = 1 

UPDATE DiagnosisCode 
SET DiagnosisDescription = ICD9.shortdescription, DiagnosisDescriptionAlternate = ICD9.ShortDescription
FROM DiagnosisCode DC INNER JOIN ICD9Mstr ICD9 ON DC.DiagnosisCode = ICD9.ICD9
WHERE DC.DiagnosisDescription LIKE 'DUMMY%' OR DiagnosisDescription = ''

UPDATE  DiagnosisCode 
SET DiagnosisCodeDisplay = DiagnosisCodeRaw WHERE DiagnosisCodeDisplay IS NULL 

UPDATE diagnosiscode SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 3) + '.' + right(DiagnosisCodeDisplay, 2) 
  WHERE DiagnosisCodeDisplay LIKE '[0-9]%' AND LEN(DiagnosisCodeDisplay) = 5 AND DiagnosisCodeDisplay NOT LIKE '%.%'

UPDATE diagnosiscode SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 3) + '.' + right(DiagnosisCodeDisplay, 1) 
  WHERE DiagnosisCodeDisplay LIKE '[0-9]%' AND LEN(DiagnosisCodeDisplay) = 4 AND DiagnosisCodeDisplay NOT LIKE '%.%'

-- V CODES
UPDATE diagnosiscode SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 3) + '.' + right(DiagnosisCodeDisplay, 2) 
  WHERE DiagnosisCodeDisplay LIKE '[v]%'   AND LEN(DiagnosisCodeDisplay) = 5 AND DiagnosisCodeDisplay NOT LIKE '%.%'

UPDATE diagnosiscode SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 3) + '.' + right(DiagnosisCodeDisplay, 1) 
  WHERE DiagnosisCodeDisplay LIKE '[v]%'   AND LEN(DiagnosisCodeDisplay) = 4 AND DiagnosisCodeDisplay NOT LIKE '%.%'

-- E CODES
UPDATE diagnosiscode SET DiagnosisCodeDisplay = left(DiagnosisCodeDisplay, 4) + '.' + right(DiagnosisCodeDisplay, 1) 
  WHERE DiagnosisCodeDisplay like '[e]%'   AND LEN(DiagnosisCodeDisplay) = 5 AND DiagnosisCodeDisplay NOT LIKE '%.%'

UPDATE A 
SET HoldingReleaseInd = 1 
FROM [dbo].[PatientDiagnosisProcessQueue_HoldingBay] A 
INNER JOIN PatientDiagnosisProcessQueue B ON A.[InterfacePatientDiagnosisId] = B.[InterfacePatientDiagnosisId]
WHERE B.ProcessedInd = 1 

--INSERT INTO [dbo].[PatientDiagnosisProcessQueue_HoldingBay]
--([InterfacePatientDiagnosisId],[InterfacePatientID],[LbPatientID]
--,[InterfaceSystemId],[DiagnosisType],[DiagnosisTypeId]
--,[DiagnosisStatus],[DiagnosisStatusTypeId],[DiagnosisSeverity]
--,[DiagnosisSeverityTypeId],[DiagnosisClassification],[DiagnosisClassificationTypeId]
--,[DiagnosisPriority],[DiagnosisPriorityTypeId],[DiagnosisConfidentialityInd]
--,[DiagnosisDateTime],[DiagnosisOnsetDate],[DiagnosisResolvedDate]
--,[DiagnosisComment],[Clinician],[IPD_CreateDateTime]
--,[InterfacePatientDiagnosisDiagnosisCodeId],[DiagnosisCode],[DiagnosisCodeId]
--,[DiagnosisCodingSystemName],[DiagnosisCodingSystemTypeId],[DiagnosisDescription]
--,[IPDDC_CreateDateTime],[PatientDiagnosisID],[EncounterId]
--,[InterfacePatientDiagnosisId_HoldingOverride],[InterfacePatientID_HoldingOverride],[LbPatientID_HoldingOverride]
--,[InterfaceSystemId_HoldingOverride],[DiagnosisType_HoldingOverride],[DiagnosisTypeId_HoldingOverride]
--,[DiagnosisStatus_HoldingOverride],[DiagnosisStatusTypeId_HoldingOverride],[DiagnosisSeverity_HoldingOverride]
--,[DiagnosisSeverityTypeId_HoldingOverride],[DiagnosisClassification_HoldingOverride],[DiagnosisClassificationTypeId_HoldingOverride]
--,[DiagnosisPriority_HoldingOverride],[DiagnosisPriorityTypeId_HoldingOverride],[DiagnosisConfidentialityInd_HoldingOverride]
--,[DiagnosisDateTime_HoldingOverride],[DiagnosisOnsetDate_HoldingOverride],[DiagnosisResolvedDate_HoldingOverride]
--,[DiagnosisComment_HoldingOverride],[Clinician_HoldingOverride],[IPD_CreateDateTime_HoldingOverride]
--,[InterfacePatientDiagnosisDiagnosisCodeId_HoldingOverride],[DiagnosisCode_HoldingOverride],[DiagnosisCodeId_HoldingOverride]
--,[DiagnosisCodingSystemName_HoldingOverride],[DiagnosisCodingSystemTypeId_HoldingOverride],[DiagnosisDescription_HoldingOverride]
--,[IPDDC_CreateDateTime_HoldingOverride],[PatientDiagnosisID_HoldingOverride],[EncounterId_HoldingOverride]
--,[HoldingStartDate],[HoldingStartReason]
--)
--SELECT 
--A.[InterfacePatientDiagnosisId],A.[InterfacePatientID],A.[LbPatientID]
--,A.[InterfaceSystemId],A.[DiagnosisType],A.[DiagnosisTypeId]
--,A.[DiagnosisStatus],A.[DiagnosisStatusTypeId],A.[DiagnosisSeverity]
--,A.[DiagnosisSeverityTypeId],A.[DiagnosisClassification],A.[DiagnosisClassificationTypeId]
--,A.[DiagnosisPriority],A.[DiagnosisPriorityTypeId],A.[DiagnosisConfidentialityInd]
--,A.[DiagnosisDateTime],A.[DiagnosisOnsetDate],A.[DiagnosisResolvedDate]
--,A.[DiagnosisComment],A.[Clinician],A.[IPD_CreateDateTime]
--,A.[InterfacePatientDiagnosisDiagnosisCodeId],A.[DiagnosisCode],A.[DiagnosisCodeId]
--,A.[DiagnosisCodingSystemName],A.[DiagnosisCodingSystemTypeId],A.[DiagnosisDescription]
--,A.[IPDDC_CreateDateTime],A.[PatientDiagnosisID],A.[EncounterId]
--,A.[InterfacePatientDiagnosisId],A.[InterfacePatientID],A.[LbPatientID]
--,A.[InterfaceSystemId],A.[DiagnosisType],A.[DiagnosisTypeId]
--,A.[DiagnosisStatus],A.[DiagnosisStatusTypeId],A.[DiagnosisSeverity]
--,A.[DiagnosisSeverityTypeId],A.[DiagnosisClassification],A.[DiagnosisClassificationTypeId]
--,A.[DiagnosisPriority],A.[DiagnosisPriorityTypeId],A.[DiagnosisConfidentialityInd]
--,A.[DiagnosisDateTime],A.[DiagnosisOnsetDate],A.[DiagnosisResolvedDate]
--,A.[DiagnosisComment],A.[Clinician],A.[IPD_CreateDateTime]
--,A.[InterfacePatientDiagnosisDiagnosisCodeId],A.[DiagnosisCode],A.[DiagnosisCodeId]
--,A.[DiagnosisCodingSystemName],A.[DiagnosisCodingSystemTypeId],A.[DiagnosisDescription]
--,A.[IPDDC_CreateDateTime],A.[PatientDiagnosisID],A.[EncounterId]
--,GetDate(),'Duplicacy'
--FROM PatientDiagnosisProcessQueue  A 
--LEFT JOIN [dbo].[PatientDiagnosisProcessQueue_HoldingBay] B ON A.[InterfacePatientDiagnosisId] = B.[InterfacePatientDiagnosisId]
--WHERE ISNULL(A.ProcessedInd,0) <> 1 AND  B.[InterfacePatientDiagnosisId] IS NULL 

END
GO
