SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ======================================================================================
-- Author:		Mike Hoxter
-- Create date: 2015-12-18
-- Description:	set patient provider attribution from MSSP TIN data
-- Modified By: Lan Ma
-- Modified Date: 2016-04-05
-- Modified By: Youping
-- Modified Date: 2016-04-21  add LbPatientId IS NOT NULL
-- ======================================================================================

CREATE PROCEDURE [dbo].[internalAttributionMSSPTIN]

AS
BEGIN	

	exec InternalDropTempTables 

	UPDATE vwPortal_ContractAttributionPriority 
	       set SourceSystemId = 1 
	WHERE AttributiontypeID = 9 AND SourceSystemId <> 1

	UPDATE MemberRosterArchives SET FileDate = TRY_CONVERT(DATE, Timeline) FROM MemberRosterArchives WHERE FileDate IS NULL

	--STEP 1: PICK THE LATEST MEMBER ROSTER ARCHIVE FILEDATE FOR EACH MEMBER
	SELECT HICNO, MAX(Timeline) AS Timeline
	INTO #PickLatestDate
	FROM MemberRosterArchives
	GROUP BY HICNO

--Step 2: load TIN/CCN providers for null NPIs
--using the vwportal_ProviderActive to exclude the provides without healthcare org assigned, updated on 04/05/2016
	INSERT INTO [dbo].[vwPortal_PatientProvider]
        ([PatientID]
        ,[ProviderID]
        ,[SourceSystemID]
        ,[ExternalReferenceIdentifier]
        ,[OtherReferenceIdentifier]
        ,[ProviderRoleTypeID]
        ,[DeleteInd]
        ,[CreateDateTime]
        ,[ModifyDateTime]
        ,[CreateLBUserId]
        ,[ModifyLBUserId]
        ,[AttributionTypeId]
		)
	SELECT DISTINCT
	MRA.LbPatientId AS PatientId
	,MAX(pp.providerID) AS ProviderId
	,1 AS SourceSystemId
	,MAX(MRA.HICNO) AS ExternalReferenceIdentifier
	,MAX(COALESCE(TRY_CONVERT(INT,MRA.[ACO Participant TIN Number]),'0')) AS OtherReferenceIdentifier 
	,1 AS [ProviderRoleTypeID]
	,0 AS [DeleteInd]
	,GETDATE() AS [CreateDateTime]
	,GETDATE() AS [ModifyDateTime]
	,1 AS [CreateLBUserId]
	,1 AS [ModifyLBUserId]
	,9 AS AttributionTypeId
	FROM MemberRosterArchives MRA
	JOIN #PickLatestDate PLD 
	ON MRA.HICNO = PLD.HICNO and MRA.Timeline = PLD.Timeline
	JOIN vwPortal_ProviderActive PP 
	ON TRY_CONVERT(INT,MRA.[ACO Participant TIN Number]) = TRY_CONVERT(INT,PP.NationalProviderIdentifier)
	--JOIN PatientIdReference IDR 
	--ON MRA.HICNO = IDR.ExternalId
	--AND IDR.IdTypeDesc = 'Bene_Hic_Num'
	--AND IDR.DeleteInd = 0
	WHERE MRA.LbPatientId IS NOT NULL and mra.TimeLine = (SELECT MAX(TimeLine) FROM MemberRosterArchives)
        and MRA.StatusDesc <> 'Dropped'
	GROUP BY MRA.LbPatientId

--INSERT INTO vwportal_patientProvider 
--(PatientID, ProviderID,SourcesystemID,[ExternalReferenceIdentifier], [OtherReferenceIdentifier],[DeleteInd],[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId],[AttributionTypeId]) 
--	 SELECT S.PatientID, S.ProviderID,1,S.[ExternalReferenceIdentifier], S.[OtherReferenceIdentifier],0,GETDATE(),GETDATE(), 1,1, S.AttributionTypeId
--   FROM #patientProvider S;

END 

GO
