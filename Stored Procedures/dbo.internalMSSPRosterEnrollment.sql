SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ==================================================
-- Author:		Alex Tran
-- Create date: 2012-12-18
-- Description:	Import MSSP MemberRosterArchives table INTO PatientRosterEnrollment format
--Modified By:
--Modified Date:
-- =================================================

/*===============================================================
	CREATED BY: 	Alex Tran
	CREATED ON:		11/30/2016
	INITIAL VER:	2.1.1
	MODULE:			NONE
	DESCRIPTION:	Import MSSP MemberRosterArchives table INTO PatientRosterEnrollmentRaw format
	PARAMETERS:		NONE
	RETURN VALUE(s)/OUTPUT:	NONE
	PROGRAMMIN NOTES: NONE
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.0		11/30/2016		Alex			LBAN-3375	Initial version or descrition of the changes
	1.1		12/22/2016		Alex			LBAN-3375	Remove old SPs and add EXEC internalMemberMonthRosterCalc

=================================================================*/
CREATE PROCEDURE [dbo].[internalMSSPRosterEnrollment]
AS
BEGIN


-- ================================================================
-- MSSP Roster - Member Month creation
-- Modified By:		Mike Hoxter
-- Modified Date: 2016-12-07
-- The purpose of this SP is to move data from the MemberRosterArchives table into the
-- PatientRosterMonthsRaw table, treating this as a member-month roster. The reason for this is to establish 
-- start and term dates for a patient's time period as part fo the MSSP program. Downstream processes will 
-- then take this data & convert it into start/term dates & put it into the PatientRosterEnrollment
-- modifications from LBAN-3372: Dynamic Source System Id, No Id Reference Join, Remove ID Update SP
-- ===============================================================
--

DECLARE @SourceSystemId int = ISNULL(
(SELECT TOP 1 SourceSystemId FROM SourceSystemSetting WHERE SettingValue LIKE '%MSSP%' OR SettingValue LIKE 'CMS%'
), 2) --SUB QUERY 

DELETE FROM PatientRosterMonthsRaw where SourceFeed = 'MSSP'

INSERT INTO PatientRosterMonthsRaw (
   SourceSystemID
  ,PatientExternalID
  ,FirstName
  ,LastName
  ,Gender
  ,Birthdate
  ,MemberMonth
  ,DeathDate
  ,FileDate
  ,SourceFeed
  ,LbPatientId
  ,CreateDateTime
) SELECT X.SourceSystemId, HICNO, X.[First Name], X.[Last Name], Sex1, X.[Birth date], X.MemberMonth,X.DateofDeath, 
 X.FileDate, X.SourceFeed, X.LbPatientId, X.CreateDateTime
FROM (
select DISTINCT @SourceSystemId AS SourceSystemId ,HICNO, [First Name], [Last Name], Sex1, [Birth date], --this is the month from the Timeline Value
TimeLine AS MemberMonth, DateofDeath, 
 FileDate, 'MSSP' AS SourceFeed, idr.LbPatientId, GETDATE() AS CreateDateTime
FROM MemberRosterArchives mra
LEFT JOIN PatientIdReference idr ON mra.HICNO = idr.ExternalId and idr.DeleteInd = 0
WHERE StatusDesc in ('CONTINUING', 'NEW','RETURNING')
union all
select DISTINCT @SourceSystemId,HICNO, [First Name], [Last Name], Sex1, [Birth date], dateadd(month,1,Timeline), DateofDeath, --one month after
 FileDate, 'MSSP', idr.LbPatientId, getdate()
FROM MemberRosterArchives mra
LEFT JOIN PatientIdReference idr ON mra.HICNO = idr.ExternalId and idr.DeleteInd = 0
WHERE StatusDesc in ('CONTINUING', 'NEW','RETURNING') and day(mra.Timeline) = 1 
) X
LEFT JOIN PatientRosterMonthsRaw prmw ON X.HICNO = prmw.PatientExternalID and prmw.MemberMonth = X.MemberMonth
WHERE prmw.PatientRosterMonthsRawID IS NULL

delete FROM PatientRosterEnrollmentRaw WHERE SourceFeed = 'mssp'
delete FROM PatientRosterEnrollment WHERE SourceFeed = 'mssp'

EXEC internalMemberMonthRosterCalc 
EXEC internalPatientRosterEnrollmentINSERT 
END
GO
