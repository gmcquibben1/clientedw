SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalPatientIdReferenceBuilder] @ProcessDeleteInd CHAR(1) = 'Y' 
-- EXEC  internalPatientIdReferenceBuilder @ProcessDeleteInd = 'Y' OR EXEC  internalPatientIdReferenceBuilder @ProcessDeleteInd = 'N'
AS
BEGIN
--DROP TABLE #PA 
--DROP TABLE #IdsLot

CREATE TABLE #IdsLot (ExternalId Varchar(100), LbPatientId INT, IdTypeDesc Varchar(100), SourceSystemId INT, PrimaryId BIT, DeleteInd BIT)

SELECT Pat.PatientId , Ind.IndividualID, Party.PartyID, Party.ExternalReferenceIdentifier , Party.OtherReferenceIdentifier 
INTO #PA
From vwPortal_Patient  Pat 
INNER JOIN vwPortal_Individual IND ON Pat.IndividualID = Ind.IndividualID 
INNER JOIN vwPortal_Party Party ON Ind.PartyID = Party.PartyID 

INSERT INTO #IdsLot (ExternalId, LbPatientId, PrimaryId, DeleteInd)
SELECT DISTINCT Medicareno , PA.PatientId, 1, 0    -- DW
FROM MemberExtract ME 
INNER JOIN #PA PA ON ME.MedicareNO = PA.ExternalReferenceIdentifier
WHERE ISNULL(Holdback,0) = 0 

IF @ProcessDeleteInd = 'Y'  
BEGIN
     INSERT INTO #IdsLot (ExternalId, LbPatientId, PrimaryId, DeleteInd)
     SELECT DISTINCT ME.Medicareno , PA.PatientId, 1, 1  -- DL
     FROM MemberExtract ME 
     INNER JOIN #PA PA ON ME.MedicareNO = PA.ExternalReferenceIdentifier
     LEFT JOIN #IdsLot IL ON ME.medicareNo = IL.ExternalId --AND PA.PatientId = IL.LbPatientId 
     WHERE ISNULL(Holdback,0) <> 0 AND IL.ExternalId IS NULL 
END 

INSERT INTO #IdsLot (ExternalId, LbPatientId,PrimaryId, DeleteInd)
SELECT  ME2.MedicareNO , PA.PatientId , 0, 0 -- ID XREFs
FROM MemberExtract ME1 -- WAL
INNER JOIN PatientIdsConflictsXReference CXR on ME1.family_id = CXR.head_id  AND CXR.family_id <> CXR.head_id
INNER JOIN MemberExtract ME2 ON CXR.family_id = ME2.family_id 
INNER JOIN #PA PA ON ME1.MedicareNo = PA.ExternalReferenceIdentifier
LEFT JOIN #IdsLot IL ON  ME2.MedicareNO = IL.ExternalId AND PA.PatientId = IL.LbPatientId 
WHERE ISNULL(ME1.Holdback,0) = 0 AND IL.ExternalId IS NULL 

INSERT INTO #IdsLot (ExternalId, LbPatientId,PrimaryId, DeleteInd)
SELECT ME.Medicareno , PIR1.IdValue , 0, 0 -- INDIRECT1 
FROM MemberExtract ME 
INNER JOIN PatientIdsRegistry PIR1  on PIR1.family_id = ME.family_ID AND PIR1.IDType = 'LbPatientId'
LEFT JOIN #IdsLot IL ON ME.medicareNo = IL.ExternalId --AND PIR1.IdValue = IL.LbPatientId 
WHERE  IL.ExternalId IS NULL 

INSERT INTO #IdsLot (ExternalId, LbPatientId,PrimaryId, DeleteInd)
SELECT ME.Medicareno , PA.PatientId , 0, 0 -- INDIRECT2
FROM MemberExtract ME 
INNER JOIN PatientIdsRegistry PIR1  on PIR1.family_id = ME.family_ID AND PIR1.IDType NOT IN ('LbPatientId','PartyId','IndividualId','Display Nbr','ContractMemberIdentifier')
INNER JOIN #PA PA ON PIR1.IdValue = PA.ExternalReferenceIdentifier 
LEFT JOIN #IdsLot IL ON ME.medicareNo = IL.ExternalId --AND PA.PatientId = IL.LbPatientId 
WHERE  ISNULL(Holdback,0) = 0 AND IL.ExternalId IS NULL  

UPDATE xx 
SET SourceSystemID = ISS.SourceSystemID , IdTypeDesc = 'Utoken'
FROM VwPortal_InterfaceSystem ISS
INNER JOIN vwPortal_InterfacePatientMatched IPM ON ISS.InterfaceSystemId = IPM.InterfaceSystemId 
INNER JOIN #IdsLot xx ON xx.ExternalId = IPM.Utoken
WHERE xx.SourceSystemID IS NULL 

UPDATE xx 
SET SourceSystemID = SSS.SourceSystemID 
FROM SourceSystemSetting SSS (NOLOCK) 
INNER JOIN MemberExtract S ON ISNULL(S.Source,'Default') = SSS.SettingValue AND SSS.SettingName = 'ResolvingCCLFName' 
INNER JOIN #IdsLot xx ON xx.ExternalId = S.MedicareNo
WHERE xx.SourceSystemID IS NULL 

UPDATE xx 
SET SourceSystemID = SSS.SourceSystemID 
FROM SourceSystemSetting SSS (NOLOCK) 
INNER JOIN MemberExtract S ON ISNULL(S.SourceFeed,'Default') = SSS.SettingValue AND SSS.SettingName = 'ResolvingCCLFName' 
INNER JOIN #IdsLot xx ON xx.ExternalId = S.MedicareNo
WHERE xx.SourceSystemID IS NULL 

UPDATE xx 
SET IdTypeDesc = SSS.SettingValue 
FROM SourceSystemSetting SSS (NOLOCK) 
INNER JOIN #IdsLot xx ON xx.SourceSystemID = SSS.SourceSystemID AND SSS.SettingName = 'ClaimFeedsIdTypeName' 
WHERE xx.IdTypeDesc IS NULL 

-- INSERT InterfacePatientIdentifier and InterfacePatientOtherIdentifier
INSERT INTO #IdsLot (ExternalId, LbPatientId,IdTypeDesc,SourceSyStemId, PrimaryId, DeleteInd)
SELECT DISTINCT IPM.PatientIdentifier , IL.LbPatientId , 'InterfacePatientIdentifier',IL.SourceSystemId,IL.PrimaryId, IL.DeleteInd  
FROM #IdsLot IL INNER JOIN vwPortal_InterfacePatientMatched IPM  on IPM.UToken = IL.ExternalId  
WHERE ISNULL(IPM.PatientIdentifier,'') <> '' 

INSERT INTO #IdsLot (ExternalId, LbPatientId,IdTypeDesc,SourceSyStemId, PrimaryId, DeleteInd)
SELECT DISTINCT IPM.PatientOtherIdentifier , IL.LbPatientId , 'InterfacePatientOtherIdentifier',IL.SourceSystemId,IL.PrimaryId, IL.DeleteInd  
FROM #IdsLot IL INNER JOIN vwPortal_InterfacePatientMatched IPM  on IPM.UToken = IL.ExternalId  
WHERE ISNULL(IPM.PatientOtherIdentifier,'') <> '' 

-- RESET IdtypeDesc From SystemSetting Table, if possible
UPDATE xx 
SET IdTypeDesc = SSS.SettingValue 
FROM SourceSystemSetting SSS (NOLOCK) 
INNER JOIN #IdsLot xx ON xx.SourceSystemID = SSS.SourceSystemID AND SSS.SettingName = 'InterfacePatientIdentifierTypeName' 
WHERE xx.IdTypeDesc = 'InterfacePatientIdentifier' 

UPDATE xx 
SET IdTypeDesc = SSS.SettingValue 
FROM SourceSystemSetting SSS (NOLOCK) 
INNER JOIN #IdsLot xx ON xx.SourceSystemID = SSS.SourceSystemID AND SSS.SettingName = 'InterfacePatientOtherIdentifierTypeName' 
WHERE xx.IdTypeDesc = 'InterfacePatientOtherIdentifier' 

UPDATE #IdsLot SET IdTypeDesc = 'Bene_Hic_Num' WHERE IdTypeDesc IS NULL 

-- DEFAULT TO Bene_HIC_Num for first occurence
;WITH CTE1 AS 
( SELECT DISTINCT LbPatientId FROM #IdsLot A 
  WHERE NOT EXISTS (SELECT 1 FROm #IdsLot B WHERE A.LbPatientId = B.LbPatientId AND B.IdTypeDesc = 'Bene_Hic_Num')
) ,
CTE2 AS 
( SELECT B.LbPatientId, IdTypeDesc, RoW_Number() OVER(Partition by B.LbPatientId Order by B.LbPatientId) SNO 
  FROM #IdsLot B INNER JOIN CTE1 ON B.LbPatientId = CTE1.LbPatientId WHERE B.PrimaryId = 1 AND B.IdTypeDesc = 'Utoken'
) 
UPDATE CTE2 SET IdTypeDesc = 'Bene_Hic_Num' WHERE SNO = 1 

UPDATE #IdsLot SET IdTypeDesc = 'Bene_Hic_Num' WHERE IdTypeDesc IS NULL 

-- LOCAL EDW FINALIZTION
TRUNCATE TABLE PatientIdReference 
-- Add trim to remove whitespaces in ExternalId
-- by Sam 2/4/2016
INSERT INTO PatientIdReference 
       (LbPatientId, ExternalId, IdTypeDesc, SourceSystemId,PrimaryId,DeleteInd)
SELECT LbPatientId, LTRIM(RTRIM(ExternalId)), IdTypeDesc, SourceSystemId,PrimaryId,DeleteInd FROM #IdsLot

-- TRANSFER TO PORTAL 
DECLARE @DynSql VARCHAR(8000), @EdwDb VARCHAR(100), @Portaldb VARCHAR(100)
SET @EdwDb = DB_Name()
SET @Portaldb = REPLACE(@EdwDb,'Edw','LbPortal')

SET @DynSql = 'TRUNCATE TABLE  ' + @Portaldb + '.Dbo.PatientIdReference'
EXEC (@DynSql)

SET @DynSql = 'INSERT INTO ' + @Portaldb + '.Dbo.PatientIdReference' + 
       '(LbPatientId, ExternalId, IdTypeDesc, SourceSystemId,PrimaryId,DeleteInd)
     SELECT LbPatientId, ExternalId, IdTypeDesc, SourceSystemId,PrimaryId,DeleteInd FROM PatientIdReference'
EXEC (@DynSql)

END

GO
