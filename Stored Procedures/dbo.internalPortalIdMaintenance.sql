SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPortalIdMaintenance] @RunSections VARCHAR(100) = '4' 
-- USAGE EXEC[dbo].[internalPortalIdMaintenance] @RunSections = '1,2' -- Comma Separated List of sections desired
-- SECTION 1 : DO PROVIDERS, SECTION 2 : DO PATIENTS,SECTION 3 : DO PATIENT-PROVIDER-HEALTHCARE,SECTION 4 : DO ORG HIERARCHY,SECTION 5 : DO PATIENT ID MATCHING,
-- SECTION 6 : DO LEGACY CODE
AS
BEGIN

/********************************************
	-- SECTION 1 : DO PROVIDERS -- 
********************************************/
IF @RunSections LIKE '%1%'
BEGIN
	EXEC [dbo].[ETLInterfaceProvider]	SELECT 'ETLInterfaceProvider Executed' -- INTERFACE SIDE 
	EXEC [dbo].[ETLProviderExtract]		SELECT 'ETLProviderExtract Executed' -- SEND BACK TO PORTAL
END
/********************************************
	-- SECTION 2 : DO PATIENTS -- 
********************************************/
IF @RunSections LIKE '%2%'
BEGIN
	EXEC [dbo].[ETLClaimsPatient]	 SELECT 'ETLClaimsPatient Executed'-- CLAIMS SIDE 
	EXEC [dbo].[ETLInterfacePatient] SELECT 'ETLInterfacePatient Executed'-- INTERFACE SIDE 
	EXEC [dbo].[ETLMemberExtract]    SELECT 'ETLMemberExtract Executed'-- SEND BACK TO PORTAL
END
/********************************************
	-- SECTION 3 : DO PATIENT-PROVIDER-HEALTHCARE ORG ATTRIBUTION 
********************************************/
IF @RunSections LIKE '%3%'
BEGIN
	EXEC [dbo].[internalProviderAttribution] SELECT 'internalProviderAttribution Executed'

	/********************************************
		-- DO HEALTHCARE ORGS -- 
	********************************************/
	DECLARE @HOPTTVP1 AS HealthcareOrgUnitProcessType;
	INSERT @HOPTTVP1
	EXEC internalComposeCustomResultset @RequestSubject = 'HealthcareOrg', @Param1 = 1
	EXEC internalHealthcareOrgInsert @HOPTTVP1;

	DECLARE @HOPTTVP2 AS HealthcareOrgUnitProcessType;
	INSERT @HOPTTVP2
	EXEC internalComposeCustomResultset @RequestSubject = 'HealthcareOrg', @Param1 = 2 --SELECT * FROM @HOPTTVP2
	EXEC internalHealthcareOrgInsert @HOPTTVP2;

	;WITH CTE AS (
	SELECT PatientId, HealthcareOrgID, PatientHealthcareOrgID, DeleteInd, ROW_NUMBER() OVER (PARTITION BY PatientID ORDER BY ModifyDateTime Desc, PatientHealthCareOrgId Desc) SNO
	FROM vwPortal_PatientHealthcareOrg )
	UPDATE CTE SET DeleteInd = 1 WHERE SNO > 1 AND DeleteInd = 0 
END
/********************************************
	-- SECTION 4 : DO ORG HIERARCHY -- 
********************************************/
IF @RunSections LIKE '%4%'
BEGIN
	EXEC [internalFlattenPortalHierarchy]  SELECT 'internalFlattenPortalHierarchy Executed'
	EXEC [internalOrgHierarchyMaintenance] SELECT 'internalOrgHierarchyMaintenance Executed'
END
/********************************************
	-- SECTION 5 : DO PATIENT ID MATCHING -- 
********************************************/
IF @RunSections LIKE '%5%'
BEGIN
	EXEC [dbo].[internalPatientIdsConflictDetection]			 SELECT 'internalPatientIdsConflictDetection Executed' -- Match Ids 
	EXEC [dbo].[internalPatientIdsConflictResolution]			 SELECT 'internalPatientIdsConflictResolution Executed' -- Decide Winning and Loosing Ids
	EXEC [dbo].[internalPatientIdsConflictsChangeDataCapture]    SELECT 'internalPatientIdsConflictsChangeDataCapture Executed' -- Reset Id Usage
END
/********************************************
	-- SECTION 6 : DO LEGACY CODE -- 
********************************************/
IF @RunSections LIKE '%6%'
BEGIN
	TRUNCATE TABLE [dbo].[ACO_Hierarchy] 
	INSERT INTO [dbo].[ACO_Hierarchy](
		  [Market],[State] ,[ACO_ID],[ACO_Name],[IPA_ID],[IPA_Name]
		  ,[TIN_Number],[TIN_Name],[CCN_Number],[CCN_Name],[ONPI_Number],[ONPI_Name]
		  ,[Provider_ID],[Provider_Name],[Provider_NPI],[Member_ID]
		  ,[Member_Full__Name],[Member_Provider_Relationship_Effective_Date],[Member_Provider_Relationship_Termination_Date],[BENE_HIC_NUM],[Patient_Status]
	)
	SELECT DISTINCT NULL,OH.State,OH.Level1Id,OH.Level1Name, NULL,NULL,
		  OH.Level2Id,OH.Level2Name,NULL,NULL,NULL,NULL,
		  OH.ProviderNPI,OH.ProviderName,OH.ProviderNPI,1,
		  OH.MemberFullName,NULL,NULL,OH.BENE_HIC_NUM,OH.Patient_Status
	FROM OrgHierarchy OH 
	LEFT JOIN [dbo].[ACO_Hierarchy] ACO ON OH.BENE_HIC_NUM = ACO.BENE_HIC_NUM
	WHERE ACO.BENE_HIC_NUM IS NULL

	TRUNCATE TABLE ACO_Measure_Detail 
	INSERT INTO ACO_Measure_Detail (PatientMemberId,MeasureId,measure,MeasureAbbrev,MeasureTarget,Value,Caregapruletypeid,measureyear,caregaptypeid,ProviderID,ProviderNPI)
	SELECT DISTINCT PatientMemberid, CMD.MeasureId,cdef.MeasureName,cdef.MeasureAbbr,0,cmd.numerator,cgrt.Caregapruletypeid,'2014',cgrt.caregaptypeid, Provider_NPI, Provider_NPI
	FROM CMeasure_Detail CMD 
	INNER JOIN CMeasure_Definitions CDEF ON CMD.MeasureID = CDEF.MeasureID
	INNER JOIN vwPortal_CareGapRuleType CGRT ON CGRT.ExternalReferenceIdentifier = CMD.MeasureID
	LEFT JOIN ACO_Hierarchy ACO ON CMD.PatientMemberID = ACO.BENE_HIC_NUM

	-- TRANSFER TO PORTAL 
	DECLARE @DynSql VARCHAR(8000), @EdwDb VARCHAR(100), @Portaldb VARCHAR(100)
	SET @EdwDb = DB_Name()
	SET @Portaldb = REPLACE(@EdwDb,'Edw','LbPortal')

	SET @DynSql = 'IF OBJECT_ID(''' + @Portaldb + '.Dbo.ACO_MEASURE_DETAIL'') > 1 
					BEGIN DROP TABLE ' + @Portaldb + '.Dbo.ACO_MEASURE_DETAIL END 
					SELECT * INTO ' + @Portaldb + '.Dbo.ACO_MEASURE_DETAIL FROM [ACO_MEASURE_DETAIL]'
	EXEC (@DynSql)

	SET @DynSql = 'IF OBJECT_ID(''' + @Portaldb + '.Dbo.ACO_Hierarchy_New'') > 1 
					BEGIN DROP TABLE ' + @Portaldb + '.Dbo.ACO_Hierarchy_New  END 
					SELECT * INTO ' + @Portaldb + '.Dbo.ACO_Hierarchy_New FROM [ACO_Hierarchy]'
	EXEC (@DynSql)
	SELECT 'LegacyCode Executed'
END

END 


GO
