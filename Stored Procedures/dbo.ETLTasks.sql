SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLTasks] 
	@RebaseHistory CHAR(10) = 'N', 
	@LbuserIdCreate INT = 1, 
	@CareGapRuleTypeIdScope INT = NULL, 
	@MeasureIdScope INT = NULL,
	@lbpatientID int = NULL

AS

  /*===============================================================
	CREATED BY: 	SG
	CREATED ON:		
	INITIAL VER:	2.2.1
	MODULE:			ETL, Tasking
	DESCRIPTION:	This script will generate tasks for all outstanding care gaps
	
		@RebaseHistory CHAR(1) = 'N'		- A value of 'REBASE' will rebuild the entire tasking table
		@LBUserId INT = 1					- Id of the user to generate tasks for
		@CareGapRuleTypeIdScope INT = NULL		-Limit the scope of tasks to generate for only this care gap rule type, should not be used in conjunction with REBASE = 'Y'
		@MeasureIdScope INT = NULL,				-Limit the scope of tasks to generate for only this measure id, should not be used in conjunction with REBASE = 'Y'
		@lbpatientID int = NULL					-Limit the scrope of tasks to genereate for a specific patient, should not be used in conjunction with REBASE = 'Y'
			

	Process flow

		1) update existing master file information in the Care Gap Type and Care Gap Rule Type tables
		2) Compile a list of the various tasks that need to be generated. 
		3) The Measure Detailt table is completely rebuilt every night, we need reapply the tasks back to the original measure
		4) We iterate accross the care gaps and generate the 
		5) Update the Measure detail table with the newly generated tasking values.

		
	Log Tracking:
        1. track table the last run date: [dbo].[EDWTableLoadTracking]
        2. log the procedure running status

	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	
	....
	
	2.2.1		2016-10-23		CJL			LBETL-301	 removed code related to 2.0 releae depreciated tables
														 TaskLbuser and TaskCommunicationGroup
	2.2.1		2017-01-12		UC/CJL		LBETL-463	Integrate fucntionality to call the [ETLCreatePatientTaskReport] stored procedure
														At the end of the task generation. This change is NOT Backward compatible with versions earlier than 2.2.1
	2.2.1		2017-02-08		CJL			LBETL-754	Addressing performance issues with tasking, added Youping's Logging code.
											LBPP-2334	Nevada Case in which tasks are not being generated
	2.2.1		2017-02-14		CJL			LBETL-754	Changed the fetching logic to only generate tasks for measures that are enabled, and to filter the list of selected measures to only include 
														those for which a task has not been generated. 
	2.2.1		2017-02-14		CJL			LBETL-754	Implemented feedback from the dev group on scoping the list of tasks to a measure id or patient, implemented fucntionality to generate tasks for unattributed patients
														Removed code to generate communication task group records
														Changed Rebase value to REBASE to avoid needing to update every single ETL job which is configured for 'Y'
	2.2.1		2017-02-14		CJL			LBETL-754	Corrected issue in which case gaps where not being generated for unattributed patients and task override status not being set correctly.
	2.0.1-2.1.2	2017-06-10		CJL			LBETL-1985	Corrected issue with the clearing of the completed care gaps
	2.0.1-2.1.2	2017-06-14		CJL			LBETL-1985	Corrected issue with the clearing of the completed care gaps, missing deleteind when not rebaseing
																										
					

=================================================================*/
BEGIN
BEGIN TRY


DECLARE	
	--@RebaseHistory CHAR(1) = 'Y', 
	--@LbuserIdCreate INT = 1, 
	--@CareGapRuleTypeIdScope INT = NULL,

	@TaskId INT,				
	@TaskPriorityTypeId INT=1,		--TODO: UPDATE WITH TRUE DEFAULT		
	@TaskStatusTypeId INT=1,		--TODO: UPDATE WITH TRUE DEFAULT
	@SimpleTaskInd BIT=0,			--TODO: UPDATE WITH TRUE DEFAULT
	@Description VARCHAR(255)='',	--TODO: UPDATE WITH MEASURE INFORMATION
	@LBUserId INT=1,				--TODO: UPDATE WITH PHYSICIAN LBUSERID					
	@CareGapActionTypeId INT=NULL,		--TODO: UPDATE WITH TRUE DEFAULT
	@CareGapActionDateTime DATETIME=GETDATE(),
	@SourceSystemId INT = 1,
	@DeleteInd BIT = 0,
	@StartDateTime DATETIME = NULL,
	@DueDateTime DATETIME = NULL,
	@EndDateTime DATETIME = NULL,
	@TaskNote VARCHAR(MAX) = NULL,
	@TaskLBUserIdList VARCHAR(1000) = NULL,				
	@CommunicationGroupIdList VARCHAR(1000) = NULL,
	@CurrentACOMeasureDetailRunToTaskID int=0,
	@PreviousACOMeasureDetailRunToTaskID int=0,
	@intError int=0,
	@PatientID INT=0,
	@ProviderNPI varchar(10)='',
	@CareGapTypeId INT = 1, -- DEFAULTS to ACO Measure Set 2013, Assigned below
	@CareGapRuleTypeId INT = 0 ,
	@MeasureId INT = 0 ,
	@DynSQL NVARCHAR(MAX) = N'' 


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'Task';
	DECLARE @dataSource VARCHAR(20) = 'EDW';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLTasks';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @NUMROWS INT
										--Rather than Query on status names, we are going to query on the type id. The task status id may change from database to databasse. 
	DECLARE @NewTaskStatusTypeId INT = (SELECT TOP 1 TaskStatusTypeId FROM vwPortal_TaskStatusType WITH (NOLOCK) WHERE Name = 'N' OR DisplayValue = 'New' AND DeleteInd = 0 )
	DECLARE @CompleteTaskStatusTypeId INT = (SELECT TOP 1 TaskStatusTypeId FROM vwPortal_TaskStatusType WITH (NOLOCK) WHERE Name = 'C' OR DisplayValue = 'Complete' AND DeleteInd = 0 )
	DECLARE @InprogressTaskStatusTypeId INT = (SELECT TOP 1 TaskStatusTypeId FROM vwPortal_TaskStatusType WITH (NOLOCK) WHERE Name = 'I' OR DisplayValue = 'Inprogress' AND DeleteInd = 0 )
	

	

	

	--1) Insert the various care gap rule type   and care gap type
	------------------------------------------------------------------------
	INSERT INTO vwPortal_CareGapType 
	(Name,DisplayValue,DeleteInd,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUseriD) 
	SELECT DISTINCT measureprogram, measureprogram, 0 , GetUTcDate(), GETUTCDATE(), @LbuserIdCreate, @LbuserIdCreate
	FROM cmeasure_definitions CMD  WITH (NOLOCK)
	LEFT JOIN vwPortal_CareGapType CGT ON CMD.measureprogram = CGT.Name
	WHERE CGT.Name IS NULL 

	UPDATE vwPortal_CareGapRuleType
	SET CareGapTypeId = CGT.CareGapTypeId  
	FROM vwPortal_CareGapRuleType CGRT 
	INNER JOIN cmeasure_definitions CMD  ON CGRT.ExternalReferenceIdentifier = CMD.MeasureID
	INNER JOIN vwPortal_CareGapType CGT ON CMD.measureprogram = CGT.Name  
	WHERE CGRT.CareGapTypeId <> CGT.CareGapTypeId 

	UPDATE vwPortal_CareGapRuleType
	SET Name = CMD.MeasureName ,
		DisplayValue =   CMD.MeasureName -- SELECT * 
	FROM vwPortal_CareGapRuleType CGRT 
	INNER JOIN cmeasure_definitions CMD  ON CGRT.ExternalReferenceIdentifier = CMD.MeasureID
	INNER JOIN vwPortal_CareGapType CGT ON CMD.measureprogram = CGT.Name AND CGT.CareGapTypeId = CGRT.CareGapTypeID 
	WHERE CGRT.Name <> CMD.MeasureName 

	INSERT INTO vwPortal_CareGapRuleType 
	(CareGapTypeID,Name,DisplayValue,RiskFactorDescription,DeleteInd,CreateDateTime,ModifyDateTime,CreateLbUserId,ModifyLbUseriD,ExternalReferenceIdentifier)  
	Select  CGT.CareGapTypeId, MeasureName, MeasureName,MeasureName, 0 , GetUTcDate(), GETUTCDATE(), @LBUserId, @LBUserId, MeasureId 
	FROM cmeasure_definitions CMD 
	INNER JOIN vwPortal_CareGapType CGT ON CMD.measureprogram = CGT.Name
	LEFT JOIN vwPortal_CareGapRuleType CGRT ON CGRT.CareGapTypeId = CGT.CareGapTypeId AND CGRT.Name = CMD.MeasureName
	WHERE CGRT.Name IS NULL 


	--Update descriptions on tasks that changed 
	UPDATE vwPortal_Task 
	SET Description = CGRT.DisplayValue -- SELECT * 
	FROM vwPortal_Task T 
	INNER JOIN vwPortal_CareGap CG ON T.TaskId = CG.TaskId 
	INNER JOIN vwPortal_CareGapRuleType CGRT ON CG.CareGapRuleTypeId = CGRT.CareGapRuleTypeId
	WHERE T.Description <> CGRT.DisplayValue




	--If we are rebasing the history, kill all of the tasking records
	--
	IF @RebaseHistory = 'REBASE'  -- KILL ACTIONS (Rebasing History)
	BEGIN
		UPDATE CMeasure_Detail SET LbTaskId = NULL 
	END



	--Pull in the care gap rule types into the queue table
	--We are going to batch the task generation according to care gap rule id.  We want to 
	--create a queueu table that contains only those care gap records that we need to generate tasks for.
	--We should only generate care gaps for measures that are enabled and that need tasking generated for
	 SELECT CaregapRuleTypeId 
		 INTO  #CareGapRuleTypeQueue 
		 FROM vwPortal_CareGapRuleType CGRT WITH(NOLOCK), 
			CMeasure_Definitions CDEF  WITH (NOLOCK)
		 WHERE deleteInd =0  AND CGRT.ExternalReferenceIdentifier = CDEF.MeasureID
		 AND ( @CareGapRuleTypeIdScope IS NULL OR ( @CareGapRuleTypeIdScope = CaregapRuleTypeId))
		 AND ( @MeasureIdScope IS NULL OR ( @MeasureIdScope = MeasureId))
		 AND CGRT.ExternalReferenceIdentifier = CDEF.MeasureID
		AND CDEF.TaskingNeeded = 'Y' AND CDEF.EnableFlag = 'Y' 
	


	WHILE  1= 1
	BEGIN 

		--Pop the next care gap rule off of the next o		
		SET  @CaregapRuleTypeId = NULL
		SET @CaregapRuleTypeId = (SELECT TOP 1 CaregapRuleTypeId FROM #CareGapRuleTypeQueue) 
		IF @CaregapRuleTypeId IS NOT NULL
		BEGIN

		
		

				--Every night the cmeasure detail table is completely killed and filled and the original assigned task ids are dropped. 
				--We need to match up and update the task id back to what  is should have been. The following statment will 
				--match up the tasks based on the caregap id and the measure that it is associated with
				-------------------------------------------------------------------------------------------------
				UPDATE CMeasure_Detail
				SET LbTaskId = CG.TASKID, TaskStatus = T.TaskStatusTypeId, ModifyTimestamp = getutcdate()
				FROM 
					CMeasure_Detail cmd  WITH (NOLOCK),
					vwPortal_CareGapRuleType CGRT  WITH (NOLOCK) ,
					vwPortal_CareGap CG WITH (NOLOCK),
					vwPortal_Task T WITH (NOLOCK) 
				WHERE CMD.LbTaskId IS NULL  
				AND CGRT.ExternalReferenceIdentifier = CMD.MeasureID AND CGRT.CareGapTypeId = CG.CareGapTypeId AND CGRT.caregapruletypeid = CG.CareGapRuleTypeId
				AND CGRT.CareGapRuleTypeId = @CareGapRuleTypeId
				AND CG.TaskID = T.TaskId AND Cmd.LbPatientId = T.PatientId and t.DeleteInd = 0
				AND (@lbpatientID IS NULL OR ( @lbpatientID = CMD.lbpatientID))	


	
		
				PRINT @CaregapRuleTypeId


				--DELETE #MeasureView -- SELECT * 
				--FROM #MeasureView MV
				--INNER JOIN #KillMeasures KM ON  MV.LbPatientId = KM.LbPatientId AND KM.MeasureID = MV.MeasureID



			--3) If  We want to completely rebase the task list, purge the existing lists
				-------------------------------------------------------------
				IF @RebaseHistory = 'REBASE'  -- KILL ACTIONS (Rebasing History)
				BEGIN

				   PRINT 'Interior Rebase'
				

					UPDATE vwPortal_CareGap
						SET DeleteInd = 1
					FROM 
						vwPortal_CareGap CG WITH (NOLOCK),
						vwPortal_Task T
					WHERE 
					 CG.CareGapRuleTypeId = @CareGapRuleTypeId
					AND CG.TaskID = T.TaskId AND  T.TaskStatusTypeId = @NewTaskStatusTypeId 
					

						
					UPDATE  vwPortal_Task 
						SET DeleteInd = 1
					FROM 
						vwPortal_CareGap CG  WITH (NOLOCK),
						vwPortal_Task T
					WHERE  CG.CareGapRuleTypeId = @CareGapRuleTypeId  AND
					CG.TaskID = T.TaskId  AND  T.TaskStatusTypeId = @NewTaskStatusTypeId 
					


				
					DELETE vwPortal_TaskCommunicationGroup 
					FROM 
						vwPortal_CareGap CG  WITH (NOLOCK),
						vwPortal_TaskCommunicationGroup TCG WITH (NOLOCK), 
						vwPortal_Task T WITH (NOLOCK)
					WHERE 
					 CG.CareGapRuleTypeId = @CareGapRuleTypeId AND  CG.TaskID = T.TaskId AND TCG.TaskId = T.TaskId AND  T.TaskStatusTypeId = @NewTaskStatusTypeId
					



		
					DELETE vwPortal_TaskLbUser 
					FROM 
						vwPortal_CareGap CG  WITH (NOLOCK),
						vwPortal_TaskLbUser TCG WITH (NOLOCK), 
						vwPortal_Task T WITH (NOLOCK)
					WHERE 
					 CG.CareGapRuleTypeId = @CareGapRuleTypeId AND  CG.TaskID = T.TaskId AND TCG.TaskId = T.TaskId AND  T.TaskStatusTypeId = @NewTaskStatusTypeId
					

			
					DELETE vwPortal_TaskNote 
					FROM 
						vwPortal_CareGap CG  WITH (NOLOCK),
						vwPortal_TaskNote TCG WITH (NOLOCK), 
						vwPortal_Task T WITH (NOLOCK)
					WHERE 
					 CG.CareGapRuleTypeId = @CareGapRuleTypeId AND  CG.TaskID = T.TaskId AND TCG.TaskId = T.TaskId AND  T.TaskStatusTypeId = @NewTaskStatusTypeId
					

							

					
					IF  EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'vwPortal_TaskFileItem')
					BEGIN

					

						DELETE vwPortal_TaskFileItem 
						FROM 
							vwPortal_CareGap CG  WITH (NOLOCK),
							vwPortal_TaskNote TCG WITH (NOLOCK), 
							vwPortal_Task T WITH (NOLOCK)
						WHERE 
							CG.CareGapRuleTypeId = @CareGapRuleTypeId AND  CG.TaskID = T.TaskId AND TCG.TaskId = T.TaskId AND  T.TaskStatusTypeId = @NewTaskStatusTypeId
							
					END
					
					
		
	
									

					UPDATE vwPortal_Task
						SET TaskStatusTypeId = @CompleteTaskStatusTypeId, ModifyDateTime = GETUTCDATE(), ModifyLBUserId = @LbuserIdCreate
					FROM 
						CMeasure_Detail cmd  WITH (NOLOCK),
						vwPortal_CareGapRuleType CGRT  WITH (NOLOCK) ,
						vwPortal_Task T
					WHERE CMD.LbTaskId IS NOT NULL  
					AND CGRT.ExternalReferenceIdentifier = CMD.MeasureID AND CGRT.CareGapRuleTypeId = @CareGapRuleTypeId AND 
					CMD.lbTaskID = T.TaskId AND Cmd.LbPatientId = T.PatientId AND  T.TaskStatusTypeId = @InprogressTaskStatusTypeId 
				
					
					 
					--DELETE FROM vwPortal_CareGap WHERE DeleteInd = 1
					--DELETE FROM vwPortal_Task WHERE DeleteInd = 1


					
					PRINT 'End Rebase'
				END 



						--Pull together a temp table of all of the tasks we are going to need to generate.
			--We are going to walk through the measures table, determine if tasking is needed,
			--link the user id of the provider that is to be tasked for the measure
			--For.  IF we want to rebase the tasking list, pull back the entire list of tasks as we are going to regenerate them all.  If we
			--are not rebasing the tasking list, only pull back those measures that need a task associated with them. 
			--If there is no task associated with a measure, it will have a null task id in the measure record
	
				SELECT DISTINCT
				T.TaskId, T.PatientId as TaskPatientId, T.TaskStatusTypeId,
				CG.CareGapID,
				 CGRT.CaregapRuleTypeId, CGRT.CareGapTypeId, 
				CGRT.ExternalReferenceIdentifier,
			
				cmd.MeasureID,    cmd.LbPatientId, 	LbTaskId, CGRT.DisplayValue
				INTO #TaskView
				FROM 
					CMeasure_Detail CMD WITH (NOLOCK)
					INNER JOIN  CMeasure_Definitions CDEF  WITH (NOLOCK) ON CMD.MeasureID = CDEF.MeasureID
					LEFT OUTER JOIN vwPortal_CareGapRuleType CGRT WITH (NOLOCK) ON CGRT.ExternalReferenceIdentifier = CMD.MeasureID AND CGRT.DeleteInd = 0
					LEFT OUTER JOIN vwPortal_Task T WITH (NOLOCK) ON  T.TaskId = ISNULL(CMD.LbTaskID, -1) 
					LEFT OUTER jOIN vwPortal_CareGap CG WITH (NOLOCK) ON CG.CaregapRuleTypeId = CGRT.CaregapRuleTypeId  AND  CG.TaskId = ISNULL(CMD.LbTaskID, -1)  AND CG.DeleteInd = 0 					
				WHERE  
					
					CGRT.CaregapRuleTypeId = @CaregapRuleTypeId  AND 
					CASE WHEN CDEF.Highlow = 1 AND CMD.Numerator = 0 AND CMD.Exclusion = 0 THEN 1 
					WHEN CDEF.Highlow = 0 AND CMD.Numerator = 1 AND CMD.Exclusion = 0 THEN 1 END = 1 
					AND CDEF.TaskingNeeded = 'Y' AND
  				    CDEF.EnableFlag = 'Y' 
					AND ((@RebaseHistory = 'REBASE' AND T.TaskId IS NULL)  OR ( @RebaseHistory <> 'REBASE'))	
					AND (@lbpatientID IS NULL OR ( @lbpatientID = CMD.lbpatientID))		--If we are not rebassing the history, then only pull null tasking values


				DECLARE @cnt inT =  (SELECT COUNT(1) froM #TaskView)
				pRINT @CNT


					
	

			
				--Generate a cursor to iterate accross the tasks that we need to generate
				------------------------------------------------------------------------
				DECLARE MyCur CURSOR
				LOCAL FOR 
				SELECT  MeasureID, 
						LbPatientId, 
						CareGapRuleTypeId, 
						CareGapTypeId, 
						DisplayValue  
				FROM #TaskView 
				WHERE TaskId IS NULL



				OPEN MyCur
				FETCH NEXT FROM MyCur INTO @MeasureId, @PatientID, @CareGapRuleTypeId, @CareGapTypeId , @Description
				WHILE @@FETCH_STATUS = 0
				BEGIN

							

						SET @TaskLBUserIdList = NULL

						--LBETL-310
						--SELECT  @TaskLBUserIdList =  COALESCE(@TaskLBUserIdList + '|', '') + CAST( LBUserId as varchar(25))
						--FROM 
						--	(SELECT DISTINCT LBUserId FROM #TaskView WITH (NOLOCK) WHERE LBUserId IS NOT NULL 
						--		AND MeasureId = @MeasureId AND LbPatientId = @PatientID AND CareGapRuleTypeId = @CareGapRuleTypeId AND CareGapTypeId = @CareGapTypeId
						--	) A
			

					  --IF @TaskLBUserIdList IS NOT NULL
					  --BEGIN 
						  EXEC [dbo].[PortalCareGapSave]
							@TaskId=@TaskID OUTPUT,				
							@TaskPriorityTypeId=0,		
							@TaskStatusTypeId=1,
							@SimpleTaskInd=0,
							@Description= @Description,
							@LBUserId=@LbuserIdCreate,					
							@CareGapActionTypeId=NULL,
							@CareGapActionDateTime=@CareGapActionDateTime,
							@SourceSystemId=@SourceSystemId,
							@DeleteInd=@DeleteInd,
							@StartDateTime=@StartDateTime,
							@DueDateTime=@DueDateTime,
							@EndDateTime=@EndDateTime,
							@TaskNote=@TaskNote,
							@TaskLBUserIdList  = NULL, 
							@CommunicationGroupIdList = NULL, 
							--@TaskLBUserIdList=@TaskLBUserIdList,				
							--@CommunicationGroupIdList=@CommunicationGroupIdList,
							@PatientId=@PatientID,
							@CareGapTypeId=@CareGapTypeId,
							@CareGapRuleTypeId=@CareGapRuleTypeId
						--END
						  SELECT @TASKID = NULL 

						  SET @InsertedRecord = @InsertedRecord + 1
					FETCH NEXT FROM MyCur INTO  @MeasureId, @PatientID, @CareGapRuleTypeId, @CareGapTypeId , @Description 
				END
				CLOSE MyCur 
				DEALLOCATE MyCur 
	
				

				
				

				--Set the task value id for the newly generated tasks
				UPDATE CMeasure_Detail
				SET LbTaskId = CG.TASKID, TaskStatus = T.TaskStatusTypeId, ModifyTimeStamp = getutcdate()
				FROM 
					CMeasure_Detail cmd  WITH (NOLOCK),
					vwPortal_CareGapRuleType CGRT  WITH (NOLOCK) ,
					vwPortal_CareGap CG WITH (NOLOCK),
					vwPortal_Task T
				WHERE 
				--((@RebaseHistory = 'REBASE' )  OR ( @RebaseHistory <> 'REBASE' AND CMD.lbTaskId IS NULL))	
				--AND 
				CGRT.ExternalReferenceIdentifier = CMD.MeasureID AND CGRT.caregapruletypeid = CG.CareGapRuleTypeId
				AND CGRT.CareGapRuleTypeId = @CareGapRuleTypeId AND t.DeleteInd = 0  AND cg.DeleteInd = 0 
				AND CG.TaskID = T.TaskId AND Cmd.LbPatientId = T.PatientId
				

				
					--Set the override status of the tasks that have been compelted for this care gap type.
				UPDATE CMeasure_Detail
				SET TaskOverrideStatus = 1, Numerator =  CASE WHEN def.HighLow = 0  THEN 0 ELSE 1 END , ModifyTimeStamp = getutcdate(),  TaskStatus = T.TaskStatusTypeId
				FROM 
					CMeasure_Detail cmd  WITH (NOLOCK),
					CMeasure_Definitions def  WITH (NOLOCK),
					vwPortal_CareGap CG WITH (NOLOCK),
					vwPortal_Task T
				WHERE CMD.LbTaskId IS NOT NULL  AND  CMD.MeasureID = def.MeasureID --AND TaskOverrideStatus <> 1
				AND CG.CareGapRuleTypeId = @CareGapRuleTypeId AND t.DeleteInd = 0  AND cg.DeleteInd = 0 
				AND CG.TaskID = T.TaskId  AND   CMD.lbTaskID = T.TaskId AND T.TaskStatusTypeId = @CompleteTaskStatusTypeId 




	

				UPDATE vwPortal_CareGap
				SET DeleteInd = 1 FROM 
				vwPortal_CareGap cg
				LEFT JOIN vwportal_task t ON cg.TaskID = t.TaskId
				LEFT JOIN CMeasure_Detail cd ON cd.LbTaskID = t.TaskId
				WHERE t.DeleteInd = 0 and cg.DeleteInd = 0 AND cd.LbTaskID IS NULL
				AND cg.CareGapRuleTypeId = @CareGapRuleTypeId
		
				--@NewTaskStatusTypeId

				DROP TABLE #TaskView


			END		--IF @CaregapRuleTypeId IS NOT NULL
		
		


			--Are we at then end of the queue list? If so break it
			IF @CaregapRuleTypeId IS NULL
			BEGIN
				--Break out of the while loop
				BREAK	
			END

				DELETE FROM #CareGapRuleTypeQueue WHERE CareGapRuleTypeId = @CaregapRuleTypeId OR CareGapRuleTypeId IS NULL
		END  


		DROP TABLE #CareGapRuleTypeQueue -- Clean up temp tables

	
		Update vwportal_task
		SET DeleteInd = 1 
		FROM
		vwportal_task t
		LEFT JOIN CMeasure_Detail cd ON cd.LbTaskID = t.TaskId
		WHERE t.DeleteInd = 0  AND cd.LbTaskID IS  NULL

--564126703
--Type conversion in expression (CONVERT_IMPLICIT(int,[NracoLbportalTest].[dbo].[CareGapRuleType].[ExternalReferenceIdentifier],0)) may affect "CardinalityEstimate" in query plan choice, 
--Type conversion in expression ([CDEF].[MeasureID]=CONVERT_IMPLICIT(int,[NracoLbportalTest].[dbo].[CareGapRuleType].[ExternalReferenceIdentifier],0)) may affect "SeekPlan" in query plan choice, 
--Type conversion in expression (CONVERT_IMPLICIT(int,[NracoLbportalTest].[dbo].[CareGapRuleType].[ExternalReferenceIdentifier],0)=[CMD].[MeasureID]) may affect "SeekPlan" in query plan choice



		--LBETL-310
		--INSERT INTO vwPortal_TaskCommunicationGroup (TaskId, CommunicationGroupId)
		--EXEC internalComposeCustomResultset @RequestSubject = 'TaskCommunicationGroup', @Param1 = 2

		
		--LBETL-463, call the portal stored stored procedure if it existings
		DECLARE @LportalDbName VARCHAR(50) = REPLACE(DB_NAME(),'Edw','Lbportal')  -- Deriving the Edw Name
		DECLARE @sql1 VARCHAR(max) 
		SET  @sql1 =  'exec ' + @LportalDbName  + '..ETLCreatePatientTaskReport';
		EXEC( @sql1)



		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;


	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
END

GO
