SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[uspDedupeCCLF7]
AS

-- ===========================================================================
-- Source Code Location:
--     BitBucket /...?
--
-- EDW Base
-- ---------------------------------------------------------------------------
--
-- Object Type:  SQL Server Stored Procedure
--
--
-- Name:  [dbo].[uspDedupeCCLF7]
--
--
-- Description:
-- 		
-- 		Takes incoming data from the [dbo].[CCLF_7_PartD_raw] table, deduplicates that data based
-- 		upon the [CUR_CLM_UNIQ_ID] column value (keeping the rows with the most recent
-- 		[fileDate]), and uses a MERGE statement to effect inserts or updates to the
-- 		[dbo].[CCLF_7_PartD] table as appropriate.
-- 		
-- 		When complete, the procedure truncates the [dbo].[CCLF_7_PartD_raw] table.
--
--
-- Input Variables:  None.
--
--
-- Output Variables:  None.
--
--
-- Calls:  None.
--
--
-- Return Status Values:
-- 		
-- 		    0 - Successful Completion.
-- 		51001 - Selection of most recent version of staging table rows FAILED.
-- 		51002 - Deleting rows with a bad SSNK from temp staging table FAILED.
-- 		51003 - Addition of PK to temp staging table FAILED.
-- 		51004 - Merge from indexed temp staging table source to target FAILED.
-- 		51005 - Commit of Merge transaction FAILED.
-- 		51006 - Delete of processed incoming source records FAILED.
-- 		51007 - Commit of Truncate transaction FAILED.
--
--
-- ---------------------------------------------------------------------------
-- Modification History:
--
-- Date        Modified by       Description
-- ---------------------------------------------------------------------------
-- ??/??/2014  ?????             Initial creation.
-- 09/18/2015  Bill Gibney       Rewritten to implement incremental data staging.
-- ===========================================================================
--

-- ===========================================================================
--
--                           B E G I N   S C R I P T
--
-- ===========================================================================
--

-- ---------------------------------------------------------------------------
--
--                         S E S S I O N   S E T - U P
--
-- ---------------------------------------------------------------------------
--

-- Turn off row counts.
--
SET NOCOUNT ON

-- Reset Row Limit.
--
SET ROWCOUNT 0

-- Session settings.
--
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

-- ---------------------------------------------------------------------------
--
--                           D E C L A R A T I O N S
--
-- ---------------------------------------------------------------------------
--

-- For Error handling / output messages / return status.
--
DECLARE
	@Error INTEGER,
	@RowCount INTEGER,
	@Return INTEGER = 0,
	@ErrorNumber INTEGER = 0,
	@ErrorSeverity INTEGER = 0,
	@ErrorState INTEGER = 0,
	@ScriptName VARCHAR( 80 ) = QuoteName( OBJECT_SCHEMA_NAME( @@PROCID, DB_ID() ) ) + '.' + QuoteName( OBJECT_NAME( @@PROCID, DB_ID() ) ),
	@ErrorProcedure VARCHAR(200),
	@ErrorLine INTEGER = 0,
	@ErrorMessage VARCHAR(MAX)

-- Working variables.
--
DECLARE
	@SQL VARCHAR(1024)


SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Executing ' + @ScriptName + '...'
RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
--
BEGIN TRY
	-- ---------------------------------------------------------------------------
	--
	--                         I N I T I A L I Z A T I O N
	--
	-- ---------------------------------------------------------------------------
	--
	
	-- ---------------------------------------------------------------------------
	-- Initialize Script Variables
	-- ---------------------------------------------------------------------------
	-- n/a
	
	-- ---------------------------------------------------------------------------
	-- Validate Script Arguments
	-- ---------------------------------------------------------------------------
	--
	-- n/a
	
	
	
	-- ---------------------------------------------------------------------------
	-- Other Initializations
	-- ---------------------------------------------------------------------------
	--
	-- n/a
	
	
	
	-- ---------------------------------------------------------------------------
	--
	--                           E X E C U T I O N
	--
	-- ---------------------------------------------------------------------------
	--

	-- ---------------------------------------------------------------------------
	-- Select most recent version of staging table rows.
	-- ---------------------------------------------------------------------------
	-- 
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Selecting most recent version of staging table rows...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51001
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Selection of most recent version of staging table rows FAILED.'
	--
	;WITH
	[ids] AS
	(
	SELECT
		IsNull( LTrim( RTrim( r.[CUR_CLM_UNIQ_ID] ) ), '' ) AS [CUR_CLM_UNIQ_ID],
		IsNull( r.[fileDate], GetUTCDate() ) AS [fileDate],
		r.[ID],
		Row_Number() OVER (
			PARTITION BY
				r.[CUR_CLM_UNIQ_ID]
			ORDER BY
				r.[fileDate] DESC
			) AS [SequenceNum]
	FROM
		[dbo].[CCLF_7_PartD_raw] r WITH (TABLOCK)
	),
	[Sorted] AS
	(
	SELECT
		ids.[CUR_CLM_UNIQ_ID],
		ids.[fileDate],
		ids.[ID],
		ids.[SequenceNum],
		--r.[PREV_CLM_UNIQ_ID],
		r.[BENE_HIC_NUM],
		r.[CLM_LINE_NDC_CD],
		r.[CLM_TYPE_CD],
		r.[CLM_LINE_FROM_DT],
		r.[PRVDR_SRVC_ID_QLFYR_CD],
		r.[CLM_SRVC_PRVDR_GNRC_ID_NUM],
		r.[CLM_DSPNSNG_STUS_CD],
		r.[CLM_DAW_PROD_SLCTN_CD],
		r.[CLM_LINE_SRVC_UNIT_QTY],
		r.[CLM_LINE_DAYS_SUPLY_QTY],
		r.[PRVDR_PRSBNG_ID_QLFYR_CD],
		r.[CLM_PRSBNG_PRVDR_GNRC_ID_NUM],
		r.[CLM_LINE_BENE_PMT_AMT],
		r.[CLM_ADJSMT_TYPE_CD],
		r.[CLM_EFCTV_DT],
		r.[CLM_IDR_LD_DT],
		r.[CLM_LINE_RX_SRVC_RFRNC_NUM],
		r.[CLM_LINE_RX_FILL_NUM],
		--r.[Generic],
		--r.[Formulary],
		--r.[Retail],
		--r.[CLM_LINE_PAYER_PAID_AMT],
		r.[SOURCEFEED]
	FROM
		[ids] WITH (TABLOCK)
		JOIN [dbo].[CCLF_7_PartD_raw] r WITH (TABLOCK)
			ON  r.[id] = ids.[id]
	)
	SELECT
		[CUR_CLM_UNIQ_ID],
		[fileDate],
		[ID],
		--[PREV_CLM_UNIQ_ID],
		[BENE_HIC_NUM],
		[CLM_LINE_NDC_CD],
		[CLM_TYPE_CD],
		[CLM_LINE_FROM_DT],
		[PRVDR_SRVC_ID_QLFYR_CD],
		[CLM_SRVC_PRVDR_GNRC_ID_NUM],
		[CLM_DSPNSNG_STUS_CD],
		[CLM_DAW_PROD_SLCTN_CD],
		[CLM_LINE_SRVC_UNIT_QTY],
		[CLM_LINE_DAYS_SUPLY_QTY],
		[PRVDR_PRSBNG_ID_QLFYR_CD],
		[CLM_PRSBNG_PRVDR_GNRC_ID_NUM],
		[CLM_LINE_BENE_PMT_AMT],
		[CLM_ADJSMT_TYPE_CD],
		[CLM_EFCTV_DT],
		[CLM_IDR_LD_DT],
		[CLM_LINE_RX_SRVC_RFRNC_NUM],
		[CLM_LINE_RX_FILL_NUM],
		--[Generic],
		--[Formulary],
		--[Retail],
		--[CLM_LINE_PAYER_PAID_AMT],
		[SOURCEFEED]
	INTO
		[#Source]
	FROM
		[Sorted] WITH (TABLOCK)
	WHERE
		    [SequenceNum] = 1
	;
	SET @RowCount = @@RowCount;
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' +
		'Selection of most recent version of staging table rows complete.' + ':  ' +
		'Rows Selected: ' + Replace( Convert( VARCHAR, Cast( @RowCount AS MONEY ), 1 ), '.00', '' )
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	

	-- ---------------------------------------------------------------------------
	-- Delete rows with a bad SSNK.
	-- ---------------------------------------------------------------------------
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Deleting rows with a bad SSNK from temp staging table...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51002
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Deleting rows with a bad SSNK from temp staging table FAILED.'
	--
	DELETE [#Source] WHERE NullIf( [CUR_CLM_UNIQ_ID], '' ) IS NULL;
	SET @RowCount = @@RowCount;
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' +
		'Deleting rows with a bad SSNK from temp staging table complete.' + ':  ' +
		'Rows Deleted: ' + Replace( Convert( VARCHAR, Cast( @RowCount AS MONEY ), 1 ), '.00', '' )
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;


	-- ---------------------------------------------------------------------------
	-- Add PK to temp staging table.
	-- ---------------------------------------------------------------------------
	-- 
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Adding PK to temp staging table...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51003
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Addition of PK to temp staging table FAILED.'
	--
	SET @SQL = '
	ALTER TABLE [#Source] ADD CONSTRAINT [PK_Source_' + Cast( NEWID() AS VARCHAR( 36 ) ) + '] PRIMARY KEY
	(
		[CUR_CLM_UNIQ_ID]
	);'
	EXECUTE( @SQL );
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Addition of PK to temp staging table complete.'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	

	-- ---------------------------------------------------------------------------
	-- Update the statistics on the temp table
	-- See OPTION (RECOMPILE) on MERGE statement below, too.
	-- ---------------------------------------------------------------------------
	--	
	UPDATE STATISTICS [#Source] WITH FULLSCAN;
	
	
	-- ---------------------------------------------------------------------------
	-- Begin a transaction.
	-- ---------------------------------------------------------------------------
	--
	BEGIN TRAN;
	
	
	-- ---------------------------------------------------------------------------
	-- Merge from indexed temp staging table source to target.
	-- ---------------------------------------------------------------------------
	-- 
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Merging from indexed temp staging table source to target...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51004
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Merge from indexed temp staging table source to target FAILED.'
	--
	MERGE INTO [dbo].[CCLF_7_PartD] tgt
	USING [#Source] src
		ON  tgt.[CUR_CLM_UNIQ_ID] = src.[CUR_CLM_UNIQ_ID]
	WHEN MATCHED THEN
		UPDATE
		SET
--			[CUR_CLM_UNIQ_ID] = src.[CUR_CLM_UNIQ_ID],
			[fileDate] = src.[fileDate],
--			[ID] = src.[ID],
			--[PREV_CLM_UNIQ_ID] = src.[PREV_CLM_UNIQ_ID],
			[BENE_HIC_NUM] = src.[BENE_HIC_NUM],
			[CLM_LINE_NDC_CD] = src.[CLM_LINE_NDC_CD],
			[CLM_TYPE_CD] = src.[CLM_TYPE_CD],
			[CLM_LINE_FROM_DT] = src.[CLM_LINE_FROM_DT],
			[PRVDR_SRVC_ID_QLFYR_CD] = src.[PRVDR_SRVC_ID_QLFYR_CD],
			[CLM_SRVC_PRVDR_GNRC_ID_NUM] = src.[CLM_SRVC_PRVDR_GNRC_ID_NUM],
			[CLM_DSPNSNG_STUS_CD] = src.[CLM_DSPNSNG_STUS_CD],
			[CLM_DAW_PROD_SLCTN_CD] = src.[CLM_DAW_PROD_SLCTN_CD],
			[CLM_LINE_SRVC_UNIT_QTY] = src.[CLM_LINE_SRVC_UNIT_QTY],
			[CLM_LINE_DAYS_SUPLY_QTY] = src.[CLM_LINE_DAYS_SUPLY_QTY],
			[PRVDR_PRSBNG_ID_QLFYR_CD] = src.[PRVDR_PRSBNG_ID_QLFYR_CD],
			[CLM_PRSBNG_PRVDR_GNRC_ID_NUM] = src.[CLM_PRSBNG_PRVDR_GNRC_ID_NUM],
			[CLM_LINE_BENE_PMT_AMT] = src.[CLM_LINE_BENE_PMT_AMT],
			[CLM_ADJSMT_TYPE_CD] = src.[CLM_ADJSMT_TYPE_CD],
			[CLM_EFCTV_DT] = src.[CLM_EFCTV_DT],
			[CLM_IDR_LD_DT] = src.[CLM_IDR_LD_DT],
			[CLM_LINE_RX_SRVC_RFRNC_NUM] = src.[CLM_LINE_RX_SRVC_RFRNC_NUM],
			[CLM_LINE_RX_FILL_NUM] = src.[CLM_LINE_RX_FILL_NUM],
			--[Generic] = src.[Generic],
			--[Formulary] = src.[Formulary],
			--[Retail] = src.[Retail],
			--[CLM_LINE_PAYER_PAID_AMT] = src.[CLM_LINE_PAYER_PAID_AMT],
			[SOURCEFEED] = src.[SOURCEFEED]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
		(
			[CUR_CLM_UNIQ_ID],
			[fileDate],
--			[ID],
			--[PREV_CLM_UNIQ_ID],
			[BENE_HIC_NUM],
			[CLM_LINE_NDC_CD],
			[CLM_TYPE_CD],
			[CLM_LINE_FROM_DT],
			[PRVDR_SRVC_ID_QLFYR_CD],
			[CLM_SRVC_PRVDR_GNRC_ID_NUM],
			[CLM_DSPNSNG_STUS_CD],
			[CLM_DAW_PROD_SLCTN_CD],
			[CLM_LINE_SRVC_UNIT_QTY],
			[CLM_LINE_DAYS_SUPLY_QTY],
			[PRVDR_PRSBNG_ID_QLFYR_CD],
			[CLM_PRSBNG_PRVDR_GNRC_ID_NUM],
			[CLM_LINE_BENE_PMT_AMT],
			[CLM_ADJSMT_TYPE_CD],
			[CLM_EFCTV_DT],
			[CLM_IDR_LD_DT],
			[CLM_LINE_RX_SRVC_RFRNC_NUM],
			[CLM_LINE_RX_FILL_NUM],
			--[Generic],
			--[Formulary],
			--[Retail],
			--[CLM_LINE_PAYER_PAID_AMT],
			[SOURCEFEED]
		)
		VALUES
		(
			src.[CUR_CLM_UNIQ_ID],
			src.[fileDate],
--			src.[ID],
			--src.[PREV_CLM_UNIQ_ID],
			src.[BENE_HIC_NUM],
			src.[CLM_LINE_NDC_CD],
			src.[CLM_TYPE_CD],
			src.[CLM_LINE_FROM_DT],
			src.[PRVDR_SRVC_ID_QLFYR_CD],
			src.[CLM_SRVC_PRVDR_GNRC_ID_NUM],
			src.[CLM_DSPNSNG_STUS_CD],
			src.[CLM_DAW_PROD_SLCTN_CD],
			src.[CLM_LINE_SRVC_UNIT_QTY],
			src.[CLM_LINE_DAYS_SUPLY_QTY],
			src.[PRVDR_PRSBNG_ID_QLFYR_CD],
			src.[CLM_PRSBNG_PRVDR_GNRC_ID_NUM],
			src.[CLM_LINE_BENE_PMT_AMT],
			src.[CLM_ADJSMT_TYPE_CD],
			src.[CLM_EFCTV_DT],
			src.[CLM_IDR_LD_DT],
			src.[CLM_LINE_RX_SRVC_RFRNC_NUM],
			src.[CLM_LINE_RX_FILL_NUM],
			--src.[Generic],
			--src.[Formulary],
			--src.[Retail],
			--src.[CLM_LINE_PAYER_PAID_AMT],
			src.[SOURCEFEED]
		)
	OPTION (RECOMPILE)
	;
	SET @RowCount = @@RowCount;
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' +
		'Merge from indexed temp staging table source to target complete.' + ':  ' +
		'Rows Merged:  ' + Replace( Convert( VARCHAR, Cast( @RowCount AS MONEY ), 1 ), '.00', '' )
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	
	
	-- ---------------------------------------------------------------------------
	-- Commit the Merge transaction.
	-- ---------------------------------------------------------------------------
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Committing Merge transaction...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51005
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Commit of Merge transaction FAILED.'
	--
	COMMIT;
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Commit of Merge transaction complete.'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	
	
	-- ---------------------------------------------------------------------------
	-- Begin another transaction.
	-- ---------------------------------------------------------------------------
	--
	BEGIN TRAN;
	
	
	-- ---------------------------------------------------------------------------
	-- Truncate the [dbo].[CCLF_7_PartD_raw] table.
	-- ---------------------------------------------------------------------------
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Truncating the [dbo].[CCLF_7_PartD_raw] table...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51006
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Truncation of the [dbo].[CCLF_7_PartD_raw] table FAILED.'
	--
	TRUNCATE TABLE [dbo].[CCLF_7_PartD_raw];
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Truncation of the [dbo].[CCLF_7_PartD_raw] table complete.'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	
	
	-- ---------------------------------------------------------------------------
	-- Commit the Truncate transaction.
	-- ---------------------------------------------------------------------------
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Committing Truncate transaction...'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	--
	SET @ErrorNumber = 51007
	SET @ErrorMessage = 'ERROR #' + CAST( @ErrorNumber AS VARCHAR ) + ':  ' + 'Commit of Truncate transaction FAILED.'
	--
	COMMIT;
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Commit of Truncate transaction complete.'
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
	
	
	
	-- ---------------------------------------------------------------------------
	--
	--                                W R A P - U P
	--
	-- ---------------------------------------------------------------------------
	--
	
	SUCCESS:
	-- Success.
	--
	SET @ErrorNumber = 0
	-- PRINT ''
	-- PRINT '***************************************************************************'
	-- PRINT '*                                                                         *'
	-- PRINT '*                     S C R I P T   S U C C E E D E D                     *'
	-- PRINT '*                                                                         *'
	-- PRINT '***************************************************************************'
	-- PRINT ''
END TRY


BEGIN CATCH
	-- Capture outstanding Error information prior to doing any ROLLBACK.
	--
	SELECT
		@ErrorNumber = Coalesce( ERROR_NUMBER(), 50099 ),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = Coalesce( ERROR_PROCEDURE(), @ScriptName, 'SQL Script' ),
		@ErrorMessage = ERROR_MESSAGE()
	;
	-- Build a message string that contains error information.
	--
	SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' +
		    'Error # ' + Cast( @ErrorNumber AS VARCHAR ) +
		' :: Severity ' + Cast( @ErrorSeverity AS VARCHAR ) +
		' :: State ' + Cast( ERROR_STATE() AS VARCHAR ) +
		' :: Procedure ' + @ErrorProcedure + ' (Line ' + Cast( @ErrorLine AS VARCHAR ) + ')' +
		' :: Message:  ' + @ErrorMessage
	
	-- Rollback any outstanding or uncommittable transaction.
	--
	IF XACT_STATE() <> 0
	BEGIN
		WHILE XACT_STATE() <> 0 ROLLBACK TRANSACTION;
		SET @ErrorMessage = @ErrorMessage + ' :: ROLLBACK TRANSACTION has occurred.'
	END
	
	RAISERROR( '', 0, 0 ) WITH NOWAIT;
	RAISERROR( '***************************************************************************', 0, 0 ) WITH NOWAIT;
	RAISERROR( '*                                                                         *', 0, 0 ) WITH NOWAIT;
	RAISERROR( '*                        S C R I P T   F A I L E D                        *', 0, 0 ) WITH NOWAIT;
	RAISERROR( '*                                                                         *', 0, 0 ) WITH NOWAIT;
	RAISERROR( '***************************************************************************', 0, 0 ) WITH NOWAIT;
	RAISERROR( '', 0, 0 ) WITH NOWAIT;
	
	-- Raise the appropriate error using RAISERRROR.
	--
	RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
END CATCH


SCRIPT_EXIT:
-- ===========================================================================
--
--                         E N D   S C R I P T
--
-- ===========================================================================
--
-- Return status.
--
SET @Return = @ErrorNumber
--
SET @ErrorMessage = Convert( VARCHAR, CURRENT_TIMESTAMP, 121 ) + ':  ' + 'Executing ' + @ScriptName + ' Complete.  Return Value:  (' + Cast( @Return AS VARCHAR ) + ').'
RAISERROR( @ErrorMessage, 0, 0 ) WITH NOWAIT;
RAISERROR( '', 0, 0 ) WITH NOWAIT;
--
RETURN @Return


GO
