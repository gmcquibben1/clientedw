SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ClinicalDecisionPatientDiagnosisLoad]
@PatientId INT		--ID of the patient
AS
 
--This stored procedure will return all the anonymous diagnosis information for the clinical decisions 
--Evaluation Request
--LBPP - 1463 - Introduced in version 2.01
--CJL - 02.17.2016  -- First Cut


select 
	pd.PatientDiagnosisId, dc.DiagnosisDescription, dc.DiagnosisCodeRaw, pd.DiagnosisDateTime, dcst.Name as 'CodeSystem',
	 pd.ActiveInd, pd.DiagnosisConfidentialityInd, pd.DiagnosisResolvedDate, ISNULL(dst.Name, 'UNKNOWN') as 'DiagnosisStatus'

FROM 
	PatientDiagnosis pd WITH (NOLOCK)
	LEFT JOIN DiagnosisStatusType dst WITH (NOLOCK) ON  pd.DiagnosisStatusTypeId = dst.DiagnosisStatusTypeId, 
	PatientDiagnosisDiagnosisCode pddc WITH (NOLOCK), 
	DiagnosisCode dc WITH (NOLOCK), 
	DiagnosisCodingSystemType dcst WITH (NOLOCK)
WHERE
	pd.DeleteInd = 0 AND pd.PatientId = @PatientId AND
	pd.PatientDiagnosisId = pddc.PatientDiagnosisId  AND
	pddc.DiagnosisCodeId = dc.DiagnosisCodeId AND dc.DiagnosisCodingSystemTypeId = dcst.DiagnosisCodingSystemTypeID 
	

GO
