SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-07-25
-- Description:	parse patient Rxmg codes from JhacgPatientDetail 
-- and load into normilized table
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[JhacgPatientRxmgCodeLoad]

AS
BEGIN	

SELECT DISTINCT [patient_id], rxmg_codes
INTO #PatientRxmgCode
FROM [dbo].[JhacgPatientDetail]
WHERE ISNULL(rxmg_codes,'')<> ''

SELECT 
patient_id AS LbpatientId, 
t2.[rxmgCode], 
t2.majorRxmgCode
INTO #Jhacg_PatientRxmgCode
FROM(
SELECT p.patient_id, e.data as rxmg_code
FROM #PatientRxmgCode p
CROSS APPLY  [EdwReferenceData].[dbo].[udf_SplitByXml](rxmg_codes,' ')AS e
) t1
JOIN [dbo].[vwJhacgRxmgCode] t2
ON t1.rxmg_code = t2.rxmgcode

MERGE [dbo].[JhacgPatientRxmgCode] T
USING #Jhacg_PatientrxmgCode S
ON (T.LbpatientId = S.LbpatientId
    AND T.rxmgCode = S.rxmgCode )
WHEN MATCHED
     THEN UPDATE SET T.MajorRxmgCode = S.MajorRxmgCode

WHEN NOT MATCHED BY TARGET
	 THEN INSERT(LbpatientId, rxmgCode,MajorRxmgCode) VALUES(S.LbpatientId, S.rxmgCode,S.MajorRxmgCode)
WHEN NOT MATCHED BY SOURCE
    THEN DELETE
;
 
DROP TABLE  #PatientRxmgCode;
DROP TABLE  #Jhacg_PatientRxmgCode;

END
GO
