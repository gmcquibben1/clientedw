SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ===============================================================================================================
-- Author:		Mike Hoxter
-- Create date: 2015-09-01
-- Description: pick patient contracts and status to run, then run process for each contract and patient status
-- select distinct ContractName, Patient_Status FROM OrgHierarchy
-- where Patient_Status not like '%Deceased%'
-- Modified By: Lan Ma
-- Modified Date: 2016-05-26
-- Description:	Set the JHACG patient population group by column as parameter, user can define any group by columns
-- the default value for these 2 parameters are set as 1, means using the whole patient population
-- ===============================================================================================================



CREATE PROCEDURE [dbo].[JhacgInputDataLoad] 
(
 @patientGroupByColumn VARCHAR(128) = '1' ,--'ContractName + patient_status',
 @patientGroupByColumnValue VARCHAR(255) = '1', --'CommercialClinical Only (NoClaims)''
 @clientName VARCHAR(100),
 @acgRav VARCHAR(10) = 'US-ALLAGE' OUTPUT
)

AS
BEGIN

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'JhacgPatientListStaging';
	DECLARE @ProcedureName VARCHAR(128) = 'JhacgInputDataLoad';
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @contractNameCount INT=0;
	DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY


	--DECLARE @patientGroupByColumn VARCHAR(128) = 'ContractName + patient_status'
	--DECLARE @patientGroupByColumnValue VARCHAR(255) = 'CommercialClinical Only (NoClaims)'
	--drop table JhacgPatientListStaging
	 --DECLARE @clientName VARCHAR(100)
     --DECLARE @acgRav VARCHAR(10) 

	IF  OBJECT_ID('JhacgPatientListStaging') IS NULL 

	BEGIN

	CREATE TABLE JhacgPatientListStaging
	(LbPatientId  INT UNIQUE, 
	 BirthDate DATE,
	 GenderId TINYINT,
	 ProviderNPI VARCHAR(50),
	 ProviderName VARCHAR(150),
     ContractName VARCHAR(100)
	)

	END 

	TRUNCATE TABLE JhacgPatientListStaging

	DECLARE @SQL NVARCHAR(2000) 

	SET @SQL = 'INSERT INTO JhacgPatientListStaging
	SELECT DISTINCT LbPatientId, BirthDate, GenderId, ProviderNPI, ProviderName, ContractName
	FROM OrgHierarchy
	WHERE LbPatientId IS NOT NULL  
	AND Patient_Status NOT LIKE ''' + '%Deceased%''' + '
	AND ' + @patientGroupByColumn + ' = ''' + @patientGroupByColumnValue + ''''

	--PRINT @SQL 

	EXECUTE sp_executesql @SQL
	
	SET  @contractNameCount = (SELECT COUNT(DISTINCT ContractName) FROM JhacgPatientListStaging)

	IF @contractNameCount = 1
	 BEGIN
		SELECT @acgRav =   [RavValue]
		FROM EdwReferenceData.dbo.JhacgClientRavSetting
		WHERE [Client] = @ClientName 
		AND [ContractName] = (SELECT DISTINCT [ContractName] FROM JhacgPatientListStaging)
     END

    
	ELSE 
	  SELECT @acgRav =   'US-ALLAGE' ;


	-- get total records from JHACG_PatientListStaging after insert 
	SELECT @RecordCountAfter=count(1)
	FROM [dbo].JhacgPatientListStaging;

    -- Calculate records inserted 
	SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
    
	SET  @EndTime=GETUTCDATE();
	EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	

	END TRY
	BEGIN CATCH
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

	RETURN 
END

GO
