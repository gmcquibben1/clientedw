SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ETLInterfacePatient] @FullLoad CHAR(1) = 'N' 
/*===============================================================
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.1.1		2016-11-2		SP						Removed PrimaryCareProviderNPI column from PatientIdsRegistryStage Insert.

=================================================================*/
AS
BEGIN
-- Default mode : incremental load.
-- NOTE Utoken Defintion is used thrice in this SP

-- if full load. we'll load everything from InterfacePatientMatched table.
IF @FullLoad = 'Y'  
  BEGIN
	/*
	 * This part is from old SP : 
		DECLARE @DITVP1 AS DepositingIdProcessType;
		INSERT @DITVP1
		EXEC internalComposeCustomResultset @RequestSubject = 'DepositingId', @Param1 = 'Interface'
	 */

	 		SELECT PatientIdentifier AS DepositingId , 
			       'Utoken' AS DepositingIdType, 
				   'Commercial' AS DepositingIdSourceType,
                   'Interface' AS DepositingIdSourceFeed, 
				   'Internal' AS DepositingIdMetaType

	       into #DITVP1

	       FROM vwPortal_InterfacePatientMatched IPM 
		  

	INSERT INTO DBO.PatientIdsRegistryStage 
	(
	[LastName],
	[FirstName],
	[MiddleName],
	Gender,
	DobDateTime, 
	[ZIP],
	DepositingId,
	DepositingIdType,
    DepositingIdMetaType,
	DepositingIdSourceType,
	DepositingIdSourceFeed, 
	SourceSystemId , 
	PatientOtherIdentifier
	)

	SELECT IPM.LastName, 
	       ISNULL(IPM.FirstName,'') FirstName, 
		   IPM.MiddleName, 
		   CASE WHEN GenderID = 'M' THEN 1 
				WHEN GenderID = 'F' THEN 2
				WHEN GenderID = 'U' THEN 3
				WHEN GenderID = 'UN' THEN 4
				ELSE  3 
			END,  
	       IPM.[DOBDateTime],
		   IPM.ZIP, 
	       -- Utoken, 
	       IPM.PatientIdentifier,
	       DP.DepositingIdType, 
           DP.DepositingIdMetaType, 
	       DP.DepositingIdSourceType,
		   DP.DepositingIdSourceFeed,
		   VSS.SourceSystemId,
		   IPM.PatientOtherIdentifier

	FROM       vwPortal_InterfacePatientMatched IPM 
	INNER JOIN #DITVP1 DP ON IPM.PatientIdentifier = DP.DepositingId
	INNER JOIN [dbo].[vwPortal_InterfaceSystem] VSS on IPM.InterfaceSystemId = VSS.InterfaceSystemId
	WHERE IPM.FirstName IS NOT NULL and IPM.LastName IS NOT NULL AND IPM.[DOBDateTime] IS NOT NULL 

	
  END

ELSE  /* nightly incremental load */

  BEGIN

             INSERT INTO vwPortal_InterfacePatientMatched (UToken)
             SELECT A.UToken
             FROM  (   SELECT DISTINCT '|' + CAST(InterfaceSystemId AS VARCHAR(4)) + '|' + PatientIdentifier + '|' As UToken
						FROM vwPortal_InterfacePatient
						WHERE PatientIdentifier IS NOT NULL
					)A
				
				LEFT JOIN vwPortal_InterfacePatientMatched B ON A.UToken = B.UToken
				WHERE B.UToken IS NULL

				SELECT * INTO #IPMTEMP FROM vwPortal_InterfacePatientMatchedHistory WHERE 1 = 2 


				ALTER TABLE #IPMTEMP ADD InterfacePatientID INT , RecNum INT , CSUM INT , Utoken VARCHAR(100) --,NPINum INT
				,NationalProviderIdentifier Varchar(10)
--TRUNCATE TABLE #IPMTEMP

				INSERT INTO #IPMTEMP
					(
						InterfacePatientMatchedId,	
						InterfaceSystemId,	
						PatientIdentifier,
						PatientOtherIdentifier,
						SSN,	
						FirstName,	
						MiddleName,	
						LastName,
						PrefixName,	
						SuffixName,	
						Degree,	
						NameTypeId,
						DOBDateTime,	
						Address1,	
						Address2,	
						City,
						State,	
						ZIP,	
						Phone1,	
						Phone1Description,
						Phone2,	
						Phone2Description,	
						GenderId,	
						RaceCode,
						RaceDescription,	
						RaceSystemId,	
						MaritalCode,
						MaritalDescription,
						MaritalSystemId,
						LanguageCode,
						LanguageDescription,
						LanguageeSystemId,
						EthnicCode,	
						EthnicDescription,	
						EthnicSystemId,	
						ReligionCode,
						ReligionDescription,
						ReligionSystemId,
						StatusId,
						CreateDateTime,
						ModifyDateTime,
						InterfacePatientID, 
						RecNum, 
						CSum , 
						Utoken,
						--NpiNum,
						NationalProviderIdentifier
						)

					SELECT	 InterfacePatientMatchedId,	
					         InterfaceSystemId,	
							 PatientIdentifier,
							 PatientOtherIdentifier,
							 SSN,	
							 FirstName,	
							 MiddleName,	
							 LastName,
							 PrefixName,	
							 SuffixName,	
							 Degree,	
							 NameTypeId,
							 DOBDateTime,	
							 Address1,	
							 Address2,	
							 City,
							 State,	
							 ZIP,	
							 Phone1,	
							 Phone1Description,
							 Phone2,	
							 Phone2Description,	
							 GenderId,	
							 RaceCode,
							 RaceDescription,	
							 RaceSystemId,	
							 MaritalCode,
							 MaritalDescription,
							 MaritalSystemId,
							 LanguageCode,
							 LanguageDescription,
							 LanguageeSystemId,
							 EthnicCode,	
							 EthnicDescription,	
							 EthnicSystemId,	
							 ReligionCode,
							 ReligionDescription,
							 ReligionSystemId,
							 StatusId,
							 CreateDateTime,
							 ModifyDateTime,
							 InterfacePatientID, 
							 RecNum, 
							 CSum ,
							 Utoken,
							-- NpiNum,
							 PrimaryCareProviderNPI

                    FROM
					(
						MERGE vwPortal_InterfacePatientMatched AS target

							USING (	
										SELECT '|' + CAST(InterfaceSystemId AS VARCHAR(4)) + '|' + PatientIdentifier + '|' As Utoken, 
										       [InterfaceSystemId], 
											   [PatientIdentifier], 
											   [PatientOtherIdentifier], 
											   [SSN], 
											   [FirstName], 
											   [MiddleName], 
											   [LastName], 
											   [PrefixName], 
											   [SuffixName], 
											   [Degree], 
											   [NameTypeId], 
											   [DOBDateTime], 
											   [Address1], 
											   [Address2], 
											   [City], 
											   [State], 
											   [ZIP], 
											   [Phone1], 
											   [Phone1Description], 
											   [Phone2], 
											   [Phone2Description], 
											   [GenderId], 
											   [RaceCode], 
											   [RaceDescription], 
											   [RaceSystemId], 
											   [MaritalCode], 
											   [MaritalDescription], 
											   [MaritalSystemId], 
											   [LanguageCode], 
											   [LanguageDescription], 
											   [LanguageeSystemId], 
											   [EthnicCode], 
											   [EthnicDescription], 
											   [EthnicSystemId], 
											   [ReligionCode], 
											   [ReligionDescription], 
											   [ReligionSystemId], 
											   [StatusId], 
											   [CreateDateTime], 
											   GetUTCDate() AS [ModifyDateTime],
											   BINARY_CHECKSUM(InterfaceSystemId,	PatientIdentifier,	PatientOtherIdentifier,	SSN,	FirstName,	MiddleName,	LastName,	PrefixName,	SuffixName,	Degree,	NameTypeId,	DOBDateTime,	Address1,	Address2,	City,	State,	ZIP,	Phone1,	Phone1Description,	Phone2,	Phone2Description,	GenderId,	RaceCode,	RaceDescription,	RaceSystemId,	MaritalCode,	MaritalDescription,	MaritalSystemId,	LanguageCode,	LanguageDescription,	LanguageeSystemId,	EthnicCode,	EthnicDescription,	EthnicSystemId,	ReligionCode,	ReligionDescription,	ReligionSystemId) AS CSUM,
											   InterfacePatientId, 
											   Row_Number() Over (Partition by '|' + CAST(InterfaceSystemId AS VARCHAR(4)) + '|' + PatientIdentifier + '|' Order by createdatetime,InterfacePatientId desc) RecNum,
											  -- ROW_NUMBER() Over (Partition by InterfaceSystemId,PatientIdentifier order by createdatetime desc ) NpiNum,
											   PrimaryCareProviderNPI

											FROM vwPortal_InterfacePatient IP 
																	
										    Where ProcessedInd IS NULL 
	                            ) AS source 
	  
	     (     [Utoken],[InterfaceSystemId], [PatientIdentifier], [PatientOtherIdentifier], [SSN], [FirstName], [MiddleName], 
		[LastName], [PrefixName], [SuffixName], [Degree], [NameTypeId], [DOBDateTime], 
		[Address1], [Address2], [City], [State], [ZIP], [Phone1], 
		[Phone1Description], [Phone2], [Phone2Description], [GenderId], [RaceCode], [RaceDescription], 
		[RaceSystemId], [MaritalCode], [MaritalDescription], [MaritalSystemId], [LanguageCode], [LanguageDescription], 
		[LanguageeSystemId], [EthnicCode], [EthnicDescription], [EthnicSystemId], [ReligionCode], [ReligionDescription], 
		[ReligionSystemId], [StatusId], [CreateDateTime], [ModifyDateTime],CSUM,InterfacePatientId,RecNum--,NpiNum
		,PrimaryCareProviderNPI)
ON (target.Utoken = source.Utoken ANd source.RecNum = 1 --and source.NpiNum=1 
)
WHEN MATCHED AND source.CSUM  <> 
	BINARY_CHECKSUM(target.InterfaceSystemId,	target.PatientIdentifier,	target.PatientOtherIdentifier,	target.SSN,	target.FirstName,	target.MiddleName,	target.LastName,	target.PrefixName,	target.SuffixName,	target.Degree,	target.NameTypeId,	target.DOBDateTime,	target.Address1,	target.Address2,	target.City,	target.State,	target.ZIP,	target.Phone1,target.Phone1Description,	target.Phone2,	target.Phone2Description,	target.GenderId,	target.RaceCode,	target.RaceDescription,	target.RaceSystemId,	target.MaritalCode,	target.MaritalDescription,	target.MaritalSystemId,	target.LanguageCode,	target.LanguageDescription,	target.LanguageeSystemId,	target.EthnicCode,	target.EthnicDescription,	target.EthnicSystemId,	target.ReligionCode,	
	target.ReligionDescription,	target.ReligionSystemId ,target.PrimaryCareProviderNPI)
	AND source.RecNum = 1 -- and  source.NpiNum=1
THEN 
    UPDATE SET 
	[InterfaceSystemId] = source.[InterfaceSystemId] , 
	[PatientIdentifier] = source.[PatientIdentifier] , 
	[PatientOtherIdentifier] = source.[PatientOtherIdentifier] , 
	[SSN] = source.[SSN] , 
	[FirstName] = source.[FirstName] , 
	[MiddleName] = source.[MiddleName] , 
	[LastName] = source.[LastName] , 
	[PrefixName] = source.[PrefixName] , 
	[SuffixName] = source.[SuffixName] , 
	[Degree] = source.[Degree] , 
	[NameTypeId] = source.[NameTypeId] , 
	[DOBDateTime] = source.[DOBDateTime] , 
	[Address1] = source.[Address1] , 
	[Address2] = source.[Address2] , 
	[City] = source.[City] , 
	[State] = source.[State] , 
	[ZIP] = source.[ZIP] , 
	[Phone1] = source.[Phone1] , 
	[Phone1Description] = source.[Phone1Description] , 
	[Phone2] = source.[Phone2] , 
	[Phone2Description] = source.[Phone2Description] , 
	[GenderId] = source.[GenderId] , 
	[RaceCode] = source.[RaceCode] , 
	[RaceDescription] = source.[RaceDescription] , 
	[RaceSystemId] = source.[RaceSystemId] , 
	[MaritalCode] = source.[MaritalCode] , 
	[MaritalDescription] = source.[MaritalDescription] , 
	[MaritalSystemId] = source.[MaritalSystemId] , 
	[LanguageCode] = source.[LanguageCode] , 
	[LanguageDescription] = source.[LanguageDescription] , 
	[LanguageeSystemId] = source.[LanguageeSystemId] , 
	[EthnicCode] = source.[EthnicCode] , 
	[EthnicDescription] = source.[EthnicDescription] , 
	[EthnicSystemId] = source.[EthnicSystemId] , 
	[ReligionCode] = source.[ReligionCode] , 
	[ReligionDescription] = source.[ReligionDescription] , 
	[ReligionSystemId] = source.[ReligionSystemId] , 
	[StatusId] = source.[StatusId] , 
	[CreateDateTime] = source.[CreateDateTime] , 
	[ModifyDateTime] = GetUtcDate(),
	PrimaryCareProviderNPI =source.PrimaryCareProviderNPI
WHEN NOT MATCHED AND  source.RecNum = 1 THEN
INSERT ([Utoken],[InterfaceSystemId], [PatientIdentifier], [PatientOtherIdentifier], [SSN], [FirstName], [MiddleName], 
		[LastName], [PrefixName], [SuffixName], [Degree], [NameTypeId], [DOBDateTime], 
		[Address1], [Address2], [City], [State], [ZIP], [Phone1], 
		[Phone1Description], [Phone2], [Phone2Description], [GenderId], [RaceCode], [RaceDescription], 
		[RaceSystemId], [MaritalCode], [MaritalDescription], [MaritalSystemId], [LanguageCode], [LanguageDescription], 
		[LanguageeSystemId], [EthnicCode], [EthnicDescription], [EthnicSystemId], [ReligionCode], [ReligionDescription], 
		[ReligionSystemId], [StatusId], [CreateDateTime], [ModifyDateTime],PrimaryCareProviderNPI)
VALUES (source.[Utoken],source.[InterfaceSystemId], source.[PatientIdentifier], source.[PatientOtherIdentifier], source.[SSN], source.[FirstName], source.[MiddleName], 
		source.[LastName], source.[PrefixName], source.[SuffixName], source.[Degree], source.[NameTypeId], source.[DOBDateTime], 
		source.[Address1], source.[Address2], source.[City], source.[State], source.[ZIP], source.[Phone1], 
		source.[Phone1Description], source.[Phone2], source.[Phone2Description], source.[GenderId], source.[RaceCode], source.[RaceDescription], 
		source.[RaceSystemId], source.[MaritalCode], source.[MaritalDescription], source.[MaritalSystemId], source.[LanguageCode], source.[LanguageDescription], 
		source.[LanguageeSystemId], source.[EthnicCode], source.[EthnicDescription], source.[EthnicSystemId], source.[ReligionCode], source.[ReligionDescription], 
		source.[ReligionSystemId], source.[StatusId], source.[CreateDateTime], source.[ModifyDateTime],source.PrimaryCareProviderNPI)
OUTPUT $ACTION MergeAction,  
	    ISNULL(INSERTED.InterfacePatientMatchedId,DELETED.InterfacePatientMatchedId) InterfacePatientMatchedId,
		ISNULL(INSERTED.InterfaceSystemId,DELETED.InterfaceSystemId) InterfaceSystemId,
		ISNULL(INSERTED.PatientIdentifier,DELETED.PatientIdentifier) PatientIdentifier,
		ISNULL(INSERTED.PatientOtherIdentifier,DELETED.PatientOtherIdentifier) PatientOtherIdentifier,
		ISNULL(INSERTED.SSN,DELETED.SSN) SSN,
		ISNULL(INSERTED.FirstName,DELETED.FirstName) FirstName,
		ISNULL(INSERTED.MiddleName,DELETED.MiddleName) MiddleName,
		ISNULL(INSERTED.LastName,DELETED.LastName) LastName,
		ISNULL(INSERTED.PrefixName,DELETED.PrefixName) PrefixName,
		ISNULL(INSERTED.SuffixName,DELETED.SuffixName) SuffixName,
		ISNULL(INSERTED.Degree,DELETED.Degree) Degree,
		ISNULL(INSERTED.NameTypeId,DELETED.NameTypeId) NameTypeId,
		ISNULL(INSERTED.DOBDateTime,DELETED.DOBDateTime) DOBDateTime,
		ISNULL(INSERTED.Address1,DELETED.Address1) Address1,
		ISNULL(INSERTED.Address2,DELETED.Address2) Address2,
		ISNULL(INSERTED.City,DELETED.City) City,
		ISNULL(INSERTED.State,DELETED.State) State,
		ISNULL(INSERTED.ZIP,DELETED.ZIP) ZIP,
		ISNULL(INSERTED.Phone1,DELETED.Phone1) Phone1,
		ISNULL(INSERTED.Phone1Description,DELETED.Phone1Description) Phone1Description,
		ISNULL(INSERTED.Phone2,DELETED.Phone2) Phone2,
		ISNULL(INSERTED.Phone2Description,DELETED.Phone2Description) Phone2Description,
		ISNULL(INSERTED.GenderId,DELETED.GenderId) GenderId,
		ISNULL(INSERTED.RaceCode,DELETED.RaceCode) RaceCode,
		ISNULL(INSERTED.RaceDescription,DELETED.RaceDescription) RaceDescription,
		ISNULL(INSERTED.RaceSystemId,DELETED.RaceSystemId) RaceSystemId,
		ISNULL(INSERTED.MaritalCode,DELETED.MaritalCode) MaritalCode,
		ISNULL(INSERTED.MaritalDescription,DELETED.MaritalDescription) MaritalDescription,
		ISNULL(INSERTED.MaritalSystemId,DELETED.MaritalSystemId) MaritalSystemId,
		ISNULL(INSERTED.LanguageCode,DELETED.LanguageCode) LanguageCode,
		ISNULL(INSERTED.LanguageDescription,DELETED.LanguageDescription) LanguageDescription,
		ISNULL(INSERTED.LanguageeSystemId,DELETED.LanguageeSystemId) LanguageeSystemId,
		ISNULL(INSERTED.EthnicCode,DELETED.EthnicCode) EthnicCode,
		ISNULL(INSERTED.EthnicDescription,DELETED.EthnicDescription) EthnicDescription,
		ISNULL(INSERTED.EthnicSystemId,DELETED.EthnicSystemId) EthnicSystemId,
		ISNULL(INSERTED.ReligionCode,DELETED.ReligionCode) ReligionCode,
		ISNULL(INSERTED.ReligionDescription,DELETED.ReligionDescription) ReligionDescription,
		ISNULL(INSERTED.ReligionSystemId,DELETED.ReligionSystemId) ReligionSystemId,
		ISNULL(INSERTED.StatusId,DELETED.StatusId) StatusId,
		ISNULL(INSERTED.CreateDateTime,DELETED.CreateDateTime) CreateDateTime,
		ISNULL(INSERTED.ModifyDateTime,DELETED.ModifyDateTime) ModifyDateTime,
		ISNULL(INSERTED.PrimaryCareProviderNPI,DELETED.PrimaryCareProviderNPI) PrimaryCareProviderNPI,
		Source.InterfacePatientId , Source.Csum, Source.RecNum, Source.Utoken--,source.npiNum
) x
WHERE RecNum = 1 -- and NpiNum =1; 

--Select * from #IPMTEMP WHERE PatientIdentifier = '7876800'
/*
 * Removed. No need. By Sam. 5/5/2016
 *
INSERT INTO vwPortal_InterfacePatientMatchedHistory
		(
		InterfacePatientMatchedId,	InterfaceSystemId,	PatientIdentifier,PatientOtherIdentifier,
		SSN,	FirstName,	MiddleName,	LastName,
		PrefixName,	SuffixName,	Degree,	NameTypeId,
		DOBDateTime,	Address1,	Address2,	City,
		State,	ZIP,	Phone1,	Phone1Description,
		Phone2,	Phone2Description,	GenderId,	RaceCode,
		RaceDescription,	RaceSystemId,	MaritalCode,MaritalDescription,
		MaritalSystemId,LanguageCode,LanguageDescription,LanguageeSystemId,
		EthnicCode,	EthnicDescription,	EthnicSystemId,	ReligionCode,
		ReligionDescription,ReligionSystemId,StatusId,CreateDateTime,ModifyDateTime
		)
SELECT 
		InterfacePatientMatchedId,	InterfaceSystemId,	PatientIdentifier,PatientOtherIdentifier,
		SSN,	FirstName,	MiddleName,	LastName,
		PrefixName,	SuffixName,	Degree,	NameTypeId,
		DOBDateTime,	Address1,	Address2,	City,
		State,	ZIP,	Phone1,	Phone1Description,
		Phone2,	Phone2Description,	GenderId,	RaceCode,
		RaceDescription,	RaceSystemId,	MaritalCode,MaritalDescription,
		MaritalSystemId,LanguageCode,LanguageDescription,LanguageeSystemId,
		EthnicCode,	EthnicDescription,	EthnicSystemId,	ReligionCode,
		ReligionDescription,ReligionSystemId,StatusId,CreateDateTime,ModifyDateTime
FROM #IPMTEMP WHERE #IPMTEMP.InterfacePatientMatchedId IS NOT NULL 
 */

-- AUGMENT IPMTEMP 
INSERT INTO #IPMTEMP (InterfacePatientMatchedId,InterfacePatientID )
SELECT 
		IT.InterfacePatientMatchedId,	 IP.InterfacePatientID  
FROM vwPortal_InterfacePatient IP
INNER JOIN #IPMTEMP IT ON IT.Utoken = '|' + CAST(IP.InterfaceSystemId AS VARCHAR(4)) + '|' + IP.PatientIdentifier + '|'
AND IT.Csum = BINARY_CHECKSUM(IP.InterfaceSystemId,	IP.PatientIdentifier,	IP.PatientOtherIdentifier,	IP.SSN,	IP.FirstName,	IP.MiddleName,	IP.LastName,	IP.PrefixName,	IP.SuffixName,	IP.Degree,	IP.NameTypeId,	IP.DOBDateTime,	IP.Address1,	IP.Address2,	IP.City,	IP.State,	IP.ZIP,	IP.Phone1,IP.Phone1Description,	IP.Phone2,	IP.Phone2Description,	IP.GenderId,	IP.RaceCode,	IP.RaceDescription,	IP.RaceSystemId,	IP.MaritalCode,	IP.MaritalDescription,	IP.MaritalSystemId,	IP.LanguageCode,	IP.LanguageDescription,	IP.LanguageeSystemId,	IP.EthnicCode,	IP.EthnicDescription,	IP.EthnicSystemId,	IP.ReligionCode,	IP.ReligionDescription,	IP.ReligionSystemId)
AND IT.InterfacePatientId <> IP.InterfacePatientId AND IP.ProcessedInd IS NULL --AND It.PatientIdentifier = '7876800'

INSERT INTO #IPMTEMP (InterfacePatientMatchedId,InterfacePatientID )
SELECT 
		IPM.InterfacePatientMatchedId,	 IP.InterfacePatientID  
FROM vwPortal_InterfacePatient IP
INNER JOIN vwPortal_InterfacePatientMatched IPM ON IPM.Utoken = '|' + CAST(IP.InterfaceSystemId AS VARCHAR(4)) + '|' + IP.PatientIdentifier + '|'
AND BINARY_CHECKSUM(IPM.InterfaceSystemId,	IPM.PatientIdentifier,	IPM.PatientOtherIdentifier,	IPM.SSN,	IPM.FirstName,	IPM.MiddleName,	IPM.LastName,	IPM.PrefixName,	IPM.SuffixName,	IPM.Degree,	IPM.NameTypeId,	IPM.DOBDateTime,	IPM.Address1,	IPM.Address2,	IPM.City,	IPM.State,	IPM.ZIP,	IPM.Phone1,IPM.Phone1Description,	IPM.Phone2,	IPM.Phone2Description,	IPM.GenderId,	IPM.RaceCode,	IPM.RaceDescription,	IPM.RaceSystemId,	IPM.MaritalCode,	IPM.MaritalDescription,	IPM.MaritalSystemId,	IPM.LanguageCode,	IPM.LanguageDescription,	IPM.LanguageeSystemId,	IPM.EthnicCode,	IPM.EthnicDescription,	IPM.EthnicSystemId,	IPM.ReligionCode,	IPM.ReligionDescription,	IPM.ReligionSystemId)
	= BINARY_CHECKSUM(IP.InterfaceSystemId,	IP.PatientIdentifier,	IP.PatientOtherIdentifier,	IP.SSN,	IP.FirstName,	IP.MiddleName,	IP.LastName,	IP.PrefixName,	IP.SuffixName,	IP.Degree,	IP.NameTypeId,	IP.DOBDateTime,	IP.Address1,	IP.Address2,	IP.City,	IP.State,	IP.ZIP,	IP.Phone1,IP.Phone1Description,	IP.Phone2,	IP.Phone2Description,	IP.GenderId,	IP.RaceCode,	IP.RaceDescription,	IP.RaceSystemId,	IP.MaritalCode,	IP.MaritalDescription,	IP.MaritalSystemId,	IP.LanguageCode,	IP.LanguageDescription,	IP.LanguageeSystemId,	IP.EthnicCode,	IP.EthnicDescription,	IP.EthnicSystemId,	IP.ReligionCode,	IP.ReligionDescription,	IP.ReligionSystemId)
AND IP.ProcessedInd IS NULL --AND It.PatientIdentifier = '7876800'
LEFT JOIN #IPMTEMP IT ON IT.InterfacePatientMatchedId = IPM.InterfacePatientMatchedId AND IT.InterfacePatientId = IP.InterfacePatientId
WHERE IT.InterfacePatientID IS NULL  

UPDATE vwPortal_InterfacePatient 
SET ProcessedInd = 1
FROM #IPMTEMP IPT INNER JOIN vwPortal_InterfacePatient IP ON IPT.InterfacePatientId = IP.InterfacePatientId

/*
 * Removed. No need. 
 *
INSERT INTO vwPortal_InterfacePatientIdReference (InterfacePatientMatchedId,InterfacePatientId)
SELECT IPT.InterfacePatientMatchedId,IPT.InterfacePatientId
FROM #IPMTEMP IPT LEFT JOIN vwPortal_InterfacePatientIdReference  IPIR ON IPT.InterfacePatientId = IPIR.InterfacePatientId
WHERE IPIR.InterfacePatientId IS NULL 

INSERT INTO vwPortal_InterfacePatientArchive 
(
InterfaceSystemId,InterfacePatientId,PatientIdentifier,PatientOtherIdentifier,SSN,FirstName,
MiddleName,LastName,PrefixName,SuffixName,Degree,NameTypeId,
DOBDateTime,Address1,Address2,City,State,ZIP,
Phone1,Phone1Description,Phone2,Phone2Description,GenderId,RaceCode,
RaceDescription,RaceSystemId,MaritalCode,MaritalDescription,MaritalSystemId,LanguageCode,
LanguageDescription,LanguageeSystemId,EthnicCode,EthnicDescription,EthnicSystemId,ReligionCode,
ReligionDescription,ReligionSystemId,StatusId,CreateDateTime
)
SELECT
A.InterfaceSystemId,A.InterfacePatientId,A.PatientIdentifier,A.PatientOtherIdentifier,A.SSN,A.FirstName,
A.MiddleName,A.LastName,A.PrefixName,A.SuffixName,A.Degree,A.NameTypeId,
A.DOBDateTime,A.Address1,A.Address2,A.City,A.State,A.ZIP,
A.Phone1,A.Phone1Description,A.Phone2,A.Phone2Description,A.GenderId,A.RaceCode,
A.RaceDescription,A.RaceSystemId,A.MaritalCode,A.MaritalDescription,A.MaritalSystemId,A.LanguageCode,
A.LanguageDescription,A.LanguageeSystemId,A.EthnicCode,A.EthnicDescription,A.EthnicSystemId,A.ReligionCode,
A.ReligionDescription,A.ReligionSystemId,A.StatusId,A.CreateDateTime
FROM vwPortal_InterfacePatient A
LEFT JOIN vwPortal_InterfacePatientArchive B ON A.InterfacePatientId = B.InterfacePatientId
WHERE A.ProcessedInd = 1 AND B.InterfacePatientId IS NULL 
 */
 		SELECT IPM.PatientIdentifier AS DepositingId , 'Utoken' AS DepositingIdType, 'Commercial' AS DepositingIdSourceType,
       'Interface' AS DepositingIdSourceFeed, 'Internal' AS DepositingIdMetaType
	   into #DITVP2
		FROM vwPortal_InterfacePatientMatched IPM 
		WHERE IPM.LbPatientId IS NULL   


	INSERT INTO DBO.PatientIdsRegistryStage ([LastName],[FirstName],[MiddleName],Gender,DobDateTime, [ZIP],DepositingId,DepositingIdType,
									     DepositingIdMetaType,DepositingIdSourceType,DepositingIdSourceFeed
										 , SourceSystemId , PatientOtherIdentifier)
	SELECT IPM.LastName, 
	ISNULL(IPM.FirstName,'') FirstName, 
	IPM.MiddleName, 
	CASE WHEN GenderID = 'M' THEN 1 
		WHEN GenderID = 'F' THEN 2
		WHEN GenderID = 'U' THEN 3
		WHEN GenderID = 'UN' THEN 4
		ELSE  3 end , 
	IPM.[DOBDateTime],IPM.ZIP, 
	-- Utoken, 
	IPM.PatientIdentifier,
	DP.DepositingIdType, 
	DP.DepositingIdMetaType, DP.DepositingIdSourceType,DP.DepositingIdSourceFeed
	,VSS.SourceSystemId
	,IPM.PatientOtherIdentifier

	FROM vwPortal_InterfacePatientMatched IPM 
	INNER JOIN #DITVP2 DP ON IPM.PatientIdentifier = DP.DepositingId
	INNER JOIN [dbo].[vwPortal_InterfaceSystem] VSS on IPM.InterfaceSystemId = VSS.InterfaceSystemId
	LEFT JOIN PatientIdsRegistryStage  Stage ON IPM.PatientIdentifier = Stage.DepositingId AND Stage.DepositingIdType = 'Utoken'
	WHERE IPM.LbPatientId IS NULL AND Stage.DepositingId IS NULL 
		AND ( IPM.FirstName IS NOT NULL AND IPM.LastName IS NOT NULL AND IPM.[DOBDateTime] IS NOT NULL )
		
	END

END




GO
