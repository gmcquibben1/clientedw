SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalAllergyStageArchival] 
    @LookbackDays INT = 0  ,
	@LookbackSince DateTime = NULL,
	@ClearStage BIT = 0
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

    SELECT @LookbackSince = CASE WHEN @LookbackSince IS NULL THEN GETDATE() ELSE @LookbackSince END 

	MERGE InterfacePatientAllergyArchive AS TARGET
	USING (
			SELECT 
			   [InterfacePatientAllergyID]
			  ,[InterfacePatientID]
			  ,[InterfaceSystemId]
			  ,[AllergyType]
			  ,[AllergyDescription]
			  ,[AllergySeverity]
			  ,[AllergyReaction]
			  ,[AllergyComment]
			  ,[OnsetDate]
			  ,[CreateDateTime]
		    FROM [dbo].[vwPortal_InterfacePatientAllergy] (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			([InterfacePatientAllergyID],[InterfacePatientID],[InterfaceSystemId],[AllergyType],[AllergyDescription]
			 ,[AllergySeverity],[AllergyReaction],[AllergyComment],[OnsetDate],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientAllergyID] = SOURCE.[InterfacePatientAllergyID] 
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientAllergyID] = Source.[InterfacePatientAllergyID],
			[InterfacePatientID]		= Source.[InterfacePatientID],
			[InterfaceSystemId]			= Source.[InterfaceSystemId],
			[AllergyType]				= Source.[AllergyType],
			[AllergyDescription]		= Source.[AllergyDescription],
			[AllergySeverity]			= Source.[AllergySeverity],
			[AllergyReaction]			= Source.[AllergyReaction],
			[AllergyComment]			= Source.[AllergyComment],
			[OnsetDate]					= Source.[OnsetDate],
			[CreateDateTime]			= Source.[CreateDateTime] 
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientAllergyID],[InterfacePatientID],[InterfaceSystemId],[AllergyType],[AllergyDescription]
			 ,[AllergySeverity],[AllergyReaction],[AllergyComment],[OnsetDate],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientAllergyID],Source.[InterfacePatientID],Source.[InterfaceSystemId],Source.[AllergyType],Source.[AllergyDescription]
			 ,Source.[AllergySeverity],Source.[AllergyReaction],Source.[AllergyComment],Source.[OnsetDate],Source.[CreateDateTime]) ;

	IF @ClearStage = 1
	BEGIN
		DELETE [vwPortal_InterfacePatientAllergy] FROM [vwPortal_InterfacePatientAllergy] A
		INNER JOIN InterfacePatientAllergyArchive B ON A.[InterfacePatientAllergyID] = B.[InterfacePatientAllergyID] 
		WHERE  A.[CreateDateTime] >= GetDate() - @LookbackDays OR  A.[CreateDateTime] > @LookbackSince
	END
		


GO
