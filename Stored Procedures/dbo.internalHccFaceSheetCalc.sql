SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
-- ================================================================
	Version		Date		Author	Change
	-------		----------	------	------
	1.6.1		
	2.1.1       2016-10-26   YL     Add report sorting LBAN-3229 Primary sort Uncovered/ Covered; Uncovered to show first. Within each grouping: order by weight - highest to show first
    2.1.1       2016-11-21   YL     Change sorting Uncovered/Recommendation/Covered; Uncovered to show first. Within each grouping: order by weight - highest to show first
              

*/

Create procedure [dbo].[internalHccFaceSheetCalc] @LbPatientId int=9995, @ShowSuspects int=1,  @MaxDetailRecords int=1,
@GreenDesc varchar(100) = 'Valid', @ShowValid int=1
as 

-- Modified to remove code that specify for usmdedwtest; 
Begin


--DECLARE @ShowSuspects int = 0
--DECLARE @MaxDetailRecords int = 4
--DECLARE @GreenDesc varchar(100) = 'Valid'
--DECLARE @ShowValid int = 1
DECLARE @HCCNoExclude varchar(1000) = '10,19,20';

IF OBJECT_ID('vwPortal_SystemSettings')>0 

	BEGIN


		IF EXISTS (SELECT 1 FROM vwPortal_SystemSettings  where [SettingType]='SSRSHccFaceSheet')

			BEGIN
				SET @ShowSuspects=(select top 1 convert(int,SetValue)    
				  FROM vwPortal_SystemSettings
				  where [SettingType]='SSRSHccFaceSheet' and SettingParameter='ShowSuspects')

				SET @MaxDetailRecords=(select top 1 convert(int,SetValue)    
				  FROM vwPortal_SystemSettings
				  where [SettingType]='SSRSHccFaceSheet' and SettingParameter='MaxDetailRecords')

				SET @GreenDesc=(select top 1 left(SetValue,100)    
				  FROM vwPortal_SystemSettings
				  where [SettingType]='SSRSHccFaceSheet' and SettingParameter='GreenDesc')

				SET @ShowValid=(select top 1 convert(int,SetValue)    
				  FROM vwPortal_SystemSettings
				  where [SettingType]='SSRSHccFaceSheet' and SettingParameter='ShowValid')

				SET @HCCNoExclude=(select top 1 left(SetValue,100)    
				  FROM vwPortal_SystemSettings
				  where [SettingType]='SSRSHccFaceSheet' and SettingParameter='HCCNoExclude')

			END

	END
---test 

--select @ShowSuspects , @MaxDetailRecords,@GreenDesc , @ShowValid , @HCCNoExclude 

--exec InternalDropTempTables

CREATE TABLE #HCCAGING 
(HCCNo VARCHAR(10), 
DiseaseGroup VARCHAR(190), 
ValidFlag VARCHAR(100), 
LbPatientId INT)

if @ShowSuspects = 1
BEGIN
insert into #HCCAGING (HCCNo, DiseaseGroup, ValidFlag, LbPatientId)
SELECT 
HCCNo, DiseaseGroup, ValidFlag, PatientId
FROM HCCPatAging
where ValidFlag <> CASE WHEN @ShowValid = 1 THEN @GreenDesc ELSE 'NULL' END
AND ValidFlag <> 'Another Diagnosis Takes Presidence'
AND PatientId=@LbPatientId
AND HCCNo not in (SELECT convert(INT,Data) FROM [dbo].[udf_SplitDelimitedStringToColumns](@HCCNoExclude,','))
END
else if @ShowSuspects = 0
BEGIN
insert into #HCCAGING (HCCNo, DiseaseGroup, ValidFlag, LbPatientId)
SELECT 
HCCNo, DiseaseGroup, ValidFlag, PatientId
FROM HCCPatAging
where ValidFlag <> CASE WHEN @ShowValid = 1 THEN @GreenDesc ELSE 'NULL' END
AND ValidFlag <> 'Another Diagnosis Takes Presidence'
  AND ValidFlag NOT LIKE '%(LB%'
 AND PatientId=@LbPatientId
 AND HCCNo not in (SELECT convert(INT,Data) FROM [dbo].[udf_SplitDelimitedStringToColumns](@HCCNoExclude,','))
END


SELECT ICD9, [Description], HCCNo, Factor, DiseaseGroup, PatientID, MAX(DateOfService) AS DOS, Max([RenderingProviderNPI]) [RenderingProviderNPI]
      ,MAX([SourceFeed]) [SourceFeed]
INTO #hcc_diag_mostrecent
FROM HCCPatDiags
WHERE PatientId=@LbPatientId
GROUP BY ICD9, [Description], HCCNo, Factor, DiseaseGroup, PatientID




SELECT DISTINCT PHD.ICD9, PHD.[Description], PHD.HCCNo, replace(PHD.DiseaseGroup,char(10),' ') as DiseaseGroup, PHD.DOS, PHD.Factor, PHD.PatientID, hcc.ValidFlag, NPI.[NAME] as Provider, [SourceFeed] as SourceFeed,
ROW_NUMBER() OVER (PARTITION BY PHD.PatientId, PHD.HCCNo order by DOS desc) AS RowNumber,
CASE WHEN hcc.ValidFlag='Not Covered' THEN 1 WHEN hcc.ValidFlag='Covered' THEN 3 WHEN LEFT(hcc.ValidFlag,14)='Recommendation' THEN 2 ELSE 4 END as SortOrd
INTO #HccDiag_All
FROM #hcc_diag_mostrecent PHD
JOIN #HccAging HCC ON PHD.HCCNo = HCC.HCCNo AND PHD.PatientId = HCC.LbPatientId
LEFT OUTER JOIN [dbo].[NPI_Lookup] NPI ON NPI.NPI=PHD.RenderingProviderNPI
--select * from 
--#HccDiag_All

SELECT DISTINCT ICD9, [Description], HCCNo, DiseaseGroup, DOS, Factor, PatientID,ValidFlag, isnull(Provider,'') AS Provider, isnull(SourceFeed,'') AS SourceFeed, SortOrd

FROM #HccDiag_All
WHERE RowNumber <= @MaxDetailRecords
--ORDER bY PatientID, HCCNo, DOS
ORDER BY SortOrd,Factor DESC, HCCNo, DOS



DROP TABLE  #HccDiag_All,#hcc_diag_mostrecent ,#HccAging;

END

GO
