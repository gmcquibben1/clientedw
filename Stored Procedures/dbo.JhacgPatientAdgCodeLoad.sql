SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-07-25
-- Description:	parse patient Adg codes from JhacgPatientDetail 
-- and load into normilized table
-- Modified By:
-- Modified Date:
-- ===============================================================

CREATE PROCEDURE [dbo].[JhacgPatientAdgCodeLoad]

AS
BEGIN	

SELECT DISTINCT [patient_id], adg_codes
INTO #PatientAdgCode
FROM [dbo].[JhacgPatientDetail]
WHERE ISNULL(adg_codes,'')<> ''

SELECT 
patient_id AS LbpatientId, 
t2.[adgCode]
INTO #Jhacg_PatientAdgCode
FROM(
SELECT p.patient_id, e.data as adg_code
FROM #PatientAdgCode p
CROSS APPLY  [EdwReferenceData].[dbo].[udf_SplitByXml](adg_codes,' ')AS e
) t1
JOIN [dbo].[vwJhacgAdgCode] t2
ON t1.adg_code = t2.[AdgCode]

MERGE [dbo].[JhacgPatientAdgCode] T
USING #Jhacg_PatientAdgCode S
ON (T.LbpatientId = S.LbpatientId
    AND T.AdgCode = S.AdgCode )
WHEN NOT MATCHED BY TARGET
	 THEN INSERT(LbpatientId, AdgCode) VALUES(S.LbpatientId, S.AdgCode)
WHEN NOT MATCHED BY SOURCE
    THEN DELETE
;
 
DROP TABLE  #PatientAdgCode;
DROP TABLE  #Jhacg_PatientAdgCode;

END
GO
