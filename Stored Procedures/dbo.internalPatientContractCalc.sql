SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[internalPatientContractCalc] @NoContractName VARCHAR(50) = 'Commercial'
AS
BEGIN

/*===============================================================
	CREATED BY: 	Youping
	CREATED ON:		2016-06-27
	INITIAL VER:	2.0.1
	MODULE:			Patient Questionnaires and Assessments
	Description:	

	 Main Steps: 
		Step 1: Pick Most Recent claim from Part-A
		Step 2: Pick Most Recent Claim from Part-B
		Step 3: Pick Most Recent Overall from claims
		Step 4 Pick Most Recent from PatientRosterEnrollment
		Step 5 Combine together to pick Most Recent 
		Step 6: get latest patient from Interface
		Step 7: get list of deceased patients
		Step 8: Insert missing patient from interface
              Step 8b.  Assignable --
		Step 9: Get all current patients and assign the ContractStatus, ClaimStatus and insert to PatientContract

	 Log Tracking:
        1. track table the last run date: [dbo].[EDWTableLoadTracking]
        2. log the procedure running status


	PARAMETERS:		
		 @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
		 @fullLoadFlag =0 Merge date to the existing table

	RETURN VALUE(s)/OUTPUT:




	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========					
	2.0.1		2016-04-13		YL						remove all unnecessary updating process 
	2.0.1		2016-04-26		YL						Move assignable before inActive.Code change reverse back
	2.0.1		2016-05-12		YL						MSSP Roster overwrite others: If patient in Roster, contractName is 'MSSP', status determined by the status in Roster
	2.0.1		2016-09-12		YL			LBAN-3077	PatientRosterEnrollment pick the latest using filedate	DESC, TermDate DESC
	2.1.2		2016-11-30		YL			LBETL-265	include CCLF8 into SP internalPatientContract	(add step 5b)
	2.1.2		2016-10-26		LM			LBAN-3077	PatientRosterEnrollment pick the latest using filedate	DESC, TermDate DESC
	2.1.2		2016-10-26		LM			LBAN-2785   when using Roster patient overwritten same patient from other source, if dropped and assignable 
														patients are deceased, their status should be deceased instead of InActive. And also using @NoContractName variable 
														instead of hardcoded 'commercial' in filter when reset contract Name because some clients use 'No Contract'


	2.1.2		2016-10-26		CL			LBETL-243	Check to the see if the patient was flagged as deleted from on one of the clinical interfaces also when setting the
														the active status for MSSP Patients

	2.1.2		2017-01-17		CL			LBETL-484   Compensation change for Date values in the [Deceased Beneficiary Flag2] field
	2.2.1       2017-01-09  YL      LBAN-3400  Update logic for Assignable; eliminate update process, correction on decease logic
	2.2.1       2017-01-13  YL      LBAN-3400  Modified logic for Assignable
	2.2.1       2017-01-27  YL      LBAN-3400 (LBETL-599) exclude NULL values patientID from CCLF8
	2.2.1		2017-02-01	MH	  	LBAN-3400 Modified to use latest Timeline per ACO instead of latest global
	2.2.1       2017-02-08	YL	  	LBAN-3400 Modified to select ONLY one record per patientID in #AssignablePat
	2.3.1       2017-04-12  YL      LBETL-1176 Remove References to vwPatientDemographics 

	=================================================================*/

	SET NOCOUNT ON

	DECLARE @EDWName VARCHAR(128) = DB_NAME();
	DECLARE @EDWtableName VARCHAR(128) = 'PatientContract';
	DECLARE @dataSource VARCHAR(20) = 'internal';
	DECLARE @ProcedureName VARCHAR(128) = 'internalPatientContractCalc';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = 0;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT = 0;
	DECLARE @UpdatedRecord INT = 0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT = 0;
	DECLARE @ErrorState INT = 0;
	DECLARE @ErrorSeverity INT = 0;
	DECLARE @ErrorLine INT = 0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;
	DECLARE @Today DATE = GETUTCDATE();

 BEGIN TRY

		--DELETE ALL GREATER THAN 13 DAYS, NOT ON A FRIDAY
		DELETE PatientContract
		WHERE (
				DATEDIFF(day, RunDateTime, getdate()) > 13
				AND DatePart(WeekDay, RunDateTime) <> 6
				)

		--and IsCurrentInd = 0
		--UPDATE ALL TO NOT CURRENT
		UPDATE PatientContract
		SET IsCurrentInd = 0
		WHERE IsCurrentInd = 1

		--SET @UpdatedRecord = @@Rowcount;

		--Step 1: Pick Most Recent claim from Part-A
		SELECT LbPatientId
			,CLM_FROM_DT
			,filedate
			,SourceFeed
			,ROW_NUMBER() OVER (
				PARTITION BY LbPatientID ORDER BY CLM_FROM_DT DESC
					,filedate DESC
				) row_nbr
		INTO #tmp_Paitient_ClaimA
		FROM CCLF_1_PartA_Header
		WHERE LbPatientId IS NOT NULL

		SELECT LbPatientId
			,CLM_FROM_DT
			,convert(VARCHAR(50), CASE 
					WHEN ISNULL(SourceFeed, '') = ''
						THEN 'MSSP'
					ELSE SourceFeed
					END) AS SourceFeed
			,convert(VARCHAR(50), CASE 
					WHEN ISNULL(SourceFeed, '') = ''
						THEN 'MSSP'
					ELSE SourceFeed
					END) AS [ContractName]
			,convert(VARCHAR(50), 'Claim CCLFPartA') AS ContractSource
			,filedate
		INTO #Paitient_Claim
		FROM #tmp_Paitient_ClaimA
		WHERE row_nbr = 1

		----Step 2: Pick Most Recent Claim from Part-B
		SELECT LbPatientId
			,CLM_FROM_DT
			,filedate
			,SourceFeed
			,ROW_NUMBER() OVER (
				PARTITION BY LbPatientID ORDER BY CLM_FROM_DT DESC
					,filedate DESC
				) row_nbr
		INTO #tmp_Paitient_ClaimB
		FROM CCLF_5_PartB_Physicians
		WHERE LbPatientId IS NOT NULL;

		INSERT INTO #Paitient_Claim (
			LbPatientId
			,CLM_FROM_DT
			,sourcefeed
			,[ContractName]
			,ContractSource
			,filedate
			)
		SELECT LbPatientId
			,CLM_FROM_DT
			,CASE 
				WHEN ISNULL(SourceFeed, '') = ''
					THEN 'MSSP'
				ELSE SourceFeed
				END AS SourceFeed
			,CASE 
				WHEN ISNULL(SourceFeed, '') = ''
					THEN 'MSSP'
				ELSE SourceFeed
				END AS [ContractName]
			,'Claim CCLFPartB' AS ContractSource
			,filedate
		FROM #tmp_Paitient_ClaimB
		WHERE row_nbr = 1

		----Step 2.1: Pick Most Recent Claim from Part-D
		SELECT LbPatientId
			,CLM_LINE_FROM_DT CLM_FROM_DT
			,filedate
			,SourceFeed
			,ROW_NUMBER() OVER (
				PARTITION BY LbPatientID ORDER BY CLM_LINE_FROM_DT DESC
					,filedate DESC
				) row_nbr
		INTO #tmp_Paitient_ClaimD
		FROM CCLF_7_PartD
		WHERE LbPatientId IS NOT NULL;

		INSERT INTO #Paitient_Claim (
			LbPatientId
			,CLM_FROM_DT
			,sourcefeed
			,[ContractName]
			,ContractSource
			,filedate
			)
		SELECT LbPatientId
			,CLM_FROM_DT
			,CASE 
				WHEN ISNULL(SourceFeed, '') = ''
					THEN 'MSSP'
				ELSE SourceFeed
				END AS SourceFeed
			,CASE 
				WHEN ISNULL(SourceFeed, '') = ''
					THEN 'MSSP'
				ELSE SourceFeed
				END AS [ContractName]
			,'Claim CCLFPartD' AS ContractSource
			,filedate
		FROM #tmp_Paitient_ClaimD
		WHERE row_nbr = 1

		----Step 2.2: Pick Most Recent Claim from Part-DME
		SELECT LbPatientId
			,CLM_FROM_DT
			,filedate
			,SourceFeed
			,ROW_NUMBER() OVER (
				PARTITION BY LbPatientID ORDER BY CLM_FROM_DT DESC
					,filedate DESC
				) row_nbr
		INTO #tmp_Paitient_ClaimDME
		FROM CCLF_6_PartB_DME
		WHERE LbPatientId IS NOT NULL;

		INSERT INTO #Paitient_Claim (
			LbPatientId
			,CLM_FROM_DT
			,sourcefeed
			,[ContractName]
			,ContractSource
			,filedate
			)
		SELECT LbPatientId
			,CLM_FROM_DT
			,CASE 
				WHEN ISNULL(SourceFeed, '') = ''
					THEN 'MSSP'
				ELSE SourceFeed
				END AS SourceFeed
			,CASE 
				WHEN ISNULL(SourceFeed, '') = ''
					THEN 'MSSP'
				ELSE SourceFeed
				END AS [ContractName]
			,'Claim CCLFPartB' AS ContractSource
			,filedate
		FROM #tmp_Paitient_ClaimDME
		WHERE row_nbr = 1

		----Step 3: Pick Most Recent Overall from claims
		SELECT LbPatientId
			,CLM_FROM_DT
			,sourcefeed
			,[ContractName]
			,ContractSource
			,filedate
			,ROW_NUMBER() OVER (
				PARTITION BY LbPatientID ORDER BY CLM_FROM_DT DESC
					,ContractSource
				) row_nbr
		INTO #tmp_MOSTRECENT
		FROM #Paitient_Claim

		SELECT LbPatientId
			,CLM_FROM_DT
			,convert(VARCHAR(50), sourcefeed) sourcefeed
			,convert(VARCHAR(50), [ContractName]) ContractName
			,convert(VARCHAR(50), 'Claim') AS ContractSource
			,convert(DATETIME, NULL) AS TermDate
		INTO #MOSTRECENT
		FROM #tmp_MOSTRECENT
		WHERE row_nbr = 1

		CREATE INDEX idx_patient_source ON #MOSTRECENT (LbPatientID) INCLUDE (
			CLM_FROM_DT
			,ContractSource
			,TermDate
			);

		----Step 4 Pick Most Recent from PatientRosterEnrollment
		SELECT LbPatientId
			,filedate AS CLM_FROM_DT
			,SourceFeed
			,TermDate
			,ROW_NUMBER() OVER (
				PARTITION BY LbPatientID ORDER BY TermDate DESC
					,filedate DESC
				) row_nbr
		INTO #tmp_PatientRoster
		FROM PatientRosterEnrollment
		WHERE LbPatientId IS NOT NULL

		INSERT INTO #MOSTRECENT (
			LbPatientId
			,CLM_FROM_DT
			,sourcefeed
			,[ContractName]
			,ContractSource
			,TermDate
			)
		SELECT LbPatientId
			,CLM_FROM_DT
			,ISNULL(SourceFeed, 'MSSP')
			,ISNULL(SourceFeed, 'MSSP') ContractName
			,'Roster' AS ContractSource
			,TermDate
		FROM #tmp_PatientRoster
		WHERE row_nbr = 1

		----Step 5 Combine together to pick Most Recent 
		SELECT LbPatientId
			,CLM_FROM_DT
			,sourcefeed
			,[ContractName]
			,ContractSource
			,ROW_NUMBER() OVER (
				PARTITION BY LbPatientID ORDER BY CLM_FROM_DT DESC
					,ContractSource DESC
				) row_nbr
		INTO #tmpMOSTRECENT_COMBINE
		FROM #MOSTRECENT

		SELECT LbPatientId
			,CLM_FROM_DT
			,sourcefeed
			,[ContractName]
			,ContractSource
		INTO #MOSTRECENT_COMBINE
		FROM #tmpMOSTRECENT_COMBINE
		WHERE row_nbr = 1

		---Step 5b CCLF8 patient  YL 2016/11/30
		SELECT cclf.[LbPatientId]
			,cclf.[fileDate] AS CLM_FROM_DT
			,ISNULL(cclf.[SourceFeed], 'MSSP') [SourceFeed]
			,ISNULL(cclf.[SourceFeed], 'MSSP') AS [ContractName]
			,'Claim' AS ContractSource
			,ROW_NUMBER() OVER (
				PARTITION BY cclf.LbPatientID ORDER BY cclf.[fileDate] DESC
				) row_nbr
		INTO #CCLF8
		FROM [dbo].[CCLF_8_BeneDemo] cclf
		LEFT JOIN #MOSTRECENT_COMBINE mc
			ON mc.[LbPatientId] = cclf.[LbPatientId]
		WHERE mc.[LbPatientId] IS NULL --AND cclf.[SourceFeed]='MSSP'
			AND cclf.[LbPatientId] IS NOT NULL;

		SELECT *
		INTO #CCLF8P
		FROM #CCLF8
		WHERE row_nbr = 1;

		--select * from #CCLF8P
		CREATE CLUSTERED INDEX CIDX_PatientIDCCLF ON #CCLF8P (LbPatientId);

		MERGE INTO #MOSTRECENT_COMBINE AS target
		USING #CCLF8P AS s
			ON s.LbPatientId = target.LbPatientId
		WHEN NOT MATCHED BY TARGET
			THEN
				INSERT (
					LbPatientId
					,CLM_FROM_DT
					,sourcefeed
					,[ContractName]
					,ContractSource
					)
				VALUES (
					s.LbPatientId
					,s.CLM_FROM_DT
					,s.sourcefeed
					,s.[ContractName]
					,s.ContractSource
					);

		--Step 6: get latest patient from Interface
		SELECT PatientId AS LbPatientId
			,cast(CreateDateTime AS DATE) CreateDate
			,ROW_NUMBER() OVER (
				PARTITION BY PatientId ORDER BY CreateDateTime DESC
				) row_nbr
		INTO #Interface_A
		FROM vwPortal_Patient
		WHERE deleteind = 0
			AND PatientId IS NOT NULL

		SELECT LbPatientId
			,CreateDate
			,cast(@NoContractName AS VARCHAR(50)) AS [ContractName]
			,cast(@NoContractName AS VARCHAR(50)) AS ContractSource
		INTO #Interface
		FROM #Interface_A
		WHERE row_nbr = 1

		CREATE INDEX idx_Portal_Patient ON #Interface (LbPatientID) INCLUDE (
			CreateDate
			,ContractSource
			);

		--Step 7: get list of deceased patients
		--select distinct PatientId AS LbPatientId
		--into #DeceasedPat
		--from vwPortal_Patient pc
		--where (PC.PatientId in (SELECT isNUll(LbPatientId,0) FROM CCLF_8_BeneDemo WHERE isnull(BENE_DEATH_DT,'01/01/2099') < GETDATE() and BENE_DEATH_DT > '1/1/1900')
		--              OR PC.PatientId in (SELECT isNUll(LbPatientId,0) FROM PatientRosterEnrollment WHERE isnull(DeathDate,'01/01/2099') < GETDATE() and DeathDate > '1/1/1900'))
		--              OR PC.PatientId in (select isNUll(PatientId,0)  FROM vwPatientDemographic WHERE DeceasedInd = 1 or (isnull(DeathDate,'01/01/2099') < getdate() and DeathDate > '1/1/1900')) 
		--			  OR PC.PatientId in (Select LbPatientId  FROM [dbo].[MemberRosterArchives] WHERE [Deceased Beneficiary Flag2]='1' AND LbPatientId  IS NOT NULL)
		SELECT DISTINCT LbPatientId
		INTO #DeceasedPat
		FROM CCLF_8_BeneDemo
		WHERE isnull(BENE_DEATH_DT, '01/01/2099') < GETDATE()
			AND BENE_DEATH_DT > '1/1/1900'
			AND LbPatientId IS NOT NULL

		INSERT INTO #DeceasedPat (LbPatientId)
		SELECT DISTINCT a.LbPatientId
		FROM PatientRosterEnrollment a
		LEFT JOIN #DeceasedPat b
			ON a.LbPatientId = b.LbPatientId
		WHERE (
				isnull(DeathDate, '01/01/2099') < GETDATE()
				AND DeathDate > '1/1/1900'
				)
			AND b.LbPatientId IS NULL;

		INSERT INTO #DeceasedPat (LbPatientId)
		SELECT DISTINCT a.PatientId
                --FROM vwPatientDemographic a 
		FROM ( SELECT p.PatientId
		       FROM [dbo].[vwPortal_Patient] p
	           INNER JOIN [dbo].[vwPortal_Individual] i ON i.IndividualId = p.IndividualId
			   WHERE p.DeleteInd = 0 AND  DeceasedInd = 1
				OR ( isnull(DeathDate, '01/01/2099') < getdate()
					AND DeathDate > '1/1/1900') 
				) a
		LEFT JOIN #DeceasedPat b
			ON a.PatientId = b.LbPatientId
		WHERE  b.LbPatientId IS NULL;

		INSERT INTO #DeceasedPat (LbPatientId)
		SELECT DISTINCT a.LbPatientId
		FROM [dbo].[MemberRosterArchives] a
		LEFT JOIN #DeceasedPat b
			ON a.LbPatientId = b.LbPatientId
		WHERE (
				[Deceased Beneficiary Flag2] = '1'
				AND a.LbPatientId IS NOT NULL
				)
			AND b.LbPatientId IS NULL

		CREATE CLUSTERED INDEX IDX_DeceasedPat ON #DeceasedPat (LbPatientId);

		--Step 8: Insert missing patients from interface
		MERGE INTO #MOSTRECENT_COMBINE AS target
		USING #Interface AS source
			ON source.LbPatientId = target.LbPatientId
		WHEN NOT MATCHED BY TARGET
			THEN
				INSERT (
					LbPatientId
					,CLM_FROM_DT
					,sourcefeed
					,[ContractName]
					,ContractSource
					)
				VALUES (
					source.LbPatientId
					,source.CreateDate
					,'Interface'
					,source.[ContractName]
					,source.ContractSource
					);

		--  Step 8b.  Assignable --
		--WITH CTE_Assignable AS (  SELECT 
		--		[LbPatientId], 
		--		[StatusDesc],
		--		ROW_NUMBER() OVER (PARTITION BY [LbPatientId] ORDER BY [TimeLine] DESC) AS rownbr
		--		FROM [dbo].[MemberRosterArchives]
		--	)	
		--SELECT [LbPatientId]
		--into #AssignablePat
		--FROM CTE_Assignable
		-- WHERE rownbr = 1 AND [StatusDesc] = 'Assignable';
		--CREATE CLUSTERED INDEX IDX_#AssignablePat ON #AssignablePat(LbPatientId);
		/* YL 2017-01-09 */
		SELECT ACO
			,MAX(TimeLine) AS MaxTimeLine
		INTO #MaxTimeLines
		FROM MemberRosterArchives
		GROUP BY ACO

		SELECT DISTINCT LbPatientId
		INTO #LbPatientIdtmp
		FROM MemberRosterArchives a 
    INNER JOIN #MaxTimeLines max
			ON a.ACO = max.ACO
		WHERE  [StatusDesc] IN (
				'New'
				,'Returning'
				,'Continuing'
				)

		CREATE CLUSTERED INDEX IDX_LbPatientId_NRC ON #LbPatientIdtmp (LbPatientId);

		SELECT DISTINCT a.LbPatientId
			,ISNULL(att.DisplayValue, 'MSSP') AS ContractName
		INTO #tmpAssignablePat
		FROM MemberRosterArchives a
		LEFT JOIN #LbPatientIdtmp b
			ON a.LbPatientId = b.LbPatientId
		LEFT JOIN vwPortal_AllTypeTables att
			ON a.ACO = att.[Name]
				AND att.TableName = 'MsspContractType'
		INNER JOIN #MaxTimeLines max
			ON a.ACO = max.ACO
				AND a.TimeLine = max.MaxTimeLine
		WHERE [StatusDesc] = 'Assignable'
			AND b.LbPatientId IS NULL;

                 ----YL 2017-02-08 pick one record per patient
	         WITH AssignablePat AS	( 
                        SELECT LbPatientId
			,ContractName
			,ROW_NUMBER() OVER (PARTITION BY [LbPatientId] ORDER BY CASE WHEN ContractName='MSSP' THEN 2 ELSE 1 END) AS rownbr
                        FROM  #tmpAssignablePat )
		 SELECT LbPatientId
			,ContractName
		 INTO #AssignablePat
		 FROM  AssignablePat
		 WHERE rownbr=1;

		 DROP TABLE #MaxTimeLines,#tmpAssignablePat,#LbPatientIdtmp


		CREATE CLUSTERED INDEX IDX_AssignablePat ON #AssignablePat (LbPatientId);

		--Get roster members	
		--   SELECT 
		--[LbPatientId], 
		--[StatusDesc],
		--[Deceased Beneficiary Flag2]
		--INTO #RosterMember
		--FROM(
		--      SELECT 
		--[LbPatientId], 
		--[StatusDesc],
		--      [Deceased Beneficiary Flag2],
		--ROW_NUMBER() OVER (PARTITION BY [LbPatientId] ORDER BY CASE WHEN [StatusDesc] IN('New','Returning','Continuing') THEN 1 ELSE 2 END) AS rownbr
		--FROM [dbo].[MemberRosterArchives]
		--WHERE TimeLine = (SELECT MAX(TimeLine) FROM [MemberRosterArchives])
		--) t WHERE rownbr = 1
		--CREATE CLUSTERED INDEX IDX_#RosterMember ON #RosterMember(LbPatientId);
		--Step 9: Get all current patients and assign the ContractStatus, ClaimStatus and insert to PatientContract
		INSERT INTO [dbo].[PatientContract] (
			[LbPatientId]
			,[ContractName]
			,[RunDateTime]
			,[ContractStatus]
			,[ClaimStatus]
			,[IsCurrentInd]
			,[LbUserId]
			,[ContractSource]
			)
		SELECT M.LbPatientId
			--,M.ContractName
			,CASE 
				WHEN M.sourcefeed = 'Interface'
					AND A.LbPatientId IS NOT NULL
					THEN A.ContractName
				ELSE M.ContractName
				END ContractName
			--CASE WHEN A.LbPatientId IS NOT NULL AND M.ContractName = 'Commercial'
			--     THEN 'MSSP'
			--              ELSE M.ContractName
			--         END AS ContractName
			,GETDATE() AS RunDateTime
			,CASE --WHEN A.LbPatientId is not NULL and m.[ContractName] IN ('MSSP','Commercial') AND A.[StatusDesc] IN('New','Returning','Continuing')  THEN 'Active'
				--WHEN A.LbPatientId is not NULL and m.[ContractName] IN ('MSSP','Commercial') AND A.[StatusDesc] IN('Assignable')  THEN 'Assignable'
				--WHEN A.LbPatientId is not NULL and m.[ContractName] IN ('MSSP','Commercial') AND A.[StatusDesc] IN('Dropped')  THEN 'Inactive' 
				WHEN (
						d.LbPatientId IS NULL
						AND MR2.TermDate > getdate()
						)
					THEN 'Active'
				WHEN d.LbPatientId IS NOT NULL
					AND MR2.TermDate > getdate()
					THEN 'Deceased/Active'
				WHEN d.LbPatientId IS NOT NULL
					THEN 'Deceased'
				WHEN A.LbPatientId IS NOT NULL
					AND (
						CASE 
							WHEN M.sourcefeed = 'Interface'
								AND A.LbPatientId IS NOT NULL
								THEN A.ContractName
							ELSE M.ContractName
							END = 'MSSP'
						OR M.ContractName IN (
							SELECT DisplayValue
							FROM vwPortal_AllTypeTables
							WHERE TableName = 'MsspContractType'
							)
						)
					THEN 'Assignable'
				WHEN (MR2.LbPatientId IS NOT NULL)
					THEN 'InActive'
				WHEN M.SourceFeed = 'Interface'
					THEN 'Clinical Only'
				ELSE 'No Roster'
				END AS ContractStatus
			-- ,CASE  WHEN d.LbPatientId is null and MR2.TermDate>getdate() THEN 'Active' 
			--   WHEN d.LbPatientId is not null and MR2.TermDate>getdate() THEN 'Deceased/Active'
			--   WHEN d.LbPatientId is not null  THEN 'Deceased' 
			--   WHEN A.LbPatientId is not NULL THEN  'Assignable' 
			--   WHEN  MR2.LbPatientId is not null THEN 'InActive' 
			--   WHEN  M.SourceFeed='Interface' THEN 'Clinical Only'  ELSE 'No Roster'  END AS ContractStatus
			,CASE 
				WHEN MR.LbPatientId IS NOT NULL
					THEN 'WithClaims'
				ELSE 'NoClaims'
				END AS ClaimStatus
			,1 AS IsCurrentInd
			,1 AS LbUserId
			,M.ContractSource ContractSource
		FROM #MOSTRECENT_COMBINE M
		LEFT JOIN #AssignablePat A
			ON m.LbPatientId = A.LbPatientId
		--LEFT OUTER JOIN  #RosterMember A ON m.LbPatientId=A.LbPatientId
		LEFT JOIN #DeceasedPat d
			ON m.LbPatientId = d.LbPatientId
		LEFT JOIN #MOSTRECENT MR
			ON MR.LbPatientId = M.LbPatientId
				AND MR.ContractSource = 'Claim'
		LEFT JOIN #MOSTRECENT MR2
			ON MR2.LbPatientId = M.LbPatientId
				AND MR2.ContractSource = 'Roster'
				AND MR2.ContractName = m.ContractName

		--      UPDATE t1
		--SET t1.[ContractName] = 'MSSP'
		--FROM [PatientContract] t1
		--JOIN #RosterMember t2
		--ON t1.LbPatientId = t2.LbPatientId
		--AND t1.IsCurrentInd = 1 AND t1.ContractName = @NoContractName --'Commercial'
		----CJL LBETL-243 -- Check to the see if the patient was flagged as deleted from on one of the clinical interfaces also
		--UPDATE t1
		--SET t1.ContractStatus = CASE 
		--                             WHEN t2.[StatusDesc] IN('New','Returning','Continuing') AND ([Deceased Beneficiary Flag2] = 0 and d.LbPatientId IS NULL) THEN 'Active'
		--                             WHEN t2.[StatusDesc] IN('New','Returning','Continuing') AND ([Deceased Beneficiary Flag2] = 1 OR d.LbPatientId IS NOT NULL) THEN 'Deceased/Active'
		--							 WHEN t2.[StatusDesc] IN('Dropped','Assignable') AND ([Deceased Beneficiary Flag2] = 1  OR d.LbPatientId IS NOT NULL) THEN 'Deceased'
		--							 WHEN t2.[StatusDesc] IN('Assignable') AND [Deceased Beneficiary Flag2] = 0 THEN 'Assignable'
		--                             ELSE  'InActive' END
		--FROM [PatientContract] t1
		--JOIN #RosterMember t2 ON t1.LbPatientId = t2.LbPatientId	AND t1.IsCurrentInd = 1 AND 
		--                (t1.ContractName = 'MSSP' or t1.ContractName in (select DisplayValue FROM vwPortal_AllTypeTables 
		--  where TableName = 'MsspContractType')) --MH: LBAN-3400: Treating contracts in 'MsspContractType' table as MSSP
		--LEFT OUTER JOIN  #DeceasedPat d ON t2.LbPatientId=d.LbPatientId
		-----Log Information
		SET @InsertedRecord = @@ROWCOUNT;
		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET @EndTime = GETUTCDATE();
	   
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName
			,@ProcedureName
			,@StartTime
			,@EndTime
			,@InsertedRecord
			,@UpdatedRecord
			,@ErrorNumber
			,@ErrorState
			,@ErrorSeverity
			,@ErrorLine
			,@Comment
			,@EDWtableName;

		----Clean temp tables
		DROP TABLE #tmpMOSTRECENT_COMBINE
			,#Interface
			,#DeceasedPat
			,#MOSTRECENT_COMBINE
			,#MOSTRECENT
			,#Interface_A
			,#Paitient_Claim
			,#tmp_Paitient_ClaimA
			,#tmp_Paitient_ClaimB
			,#tmp_PatientRoster
			,#tmp_MOSTRECENT

	

	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

  END

GO
