SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLOrghierarchyOverwrite]
/*===============================================================================================================
	CREATED BY: 	Lan
	CREATED ON:		2016-12-09
	INITIAL VER:	2.2.1
	MODULE:			(Optional) Module it applies to 		
	DESCRIPTION:	Description of what stored procedure does and / or why it was created.
	PARAMETERS:		(Optional) All non-obvious parameters must be described
	RETURN VALUE(s)/OUTPUT:	(Optional) Especially when returning specific values or error codes.
	PROGRAMMIN NOTES: (Optional) This is sometimes helpful to explain some implementation details that may not be obvious.
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2016-12-09		LM			LBAN-3227	overwrite an unused level of hierarchy with a value 
	                                                    from the patient attribute table
===============================================================================================================*/
AS
BEGIN
	
	SET NOCOUNT ON

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'Orghierarchy';
	DECLARE @dataSource VARCHAR(20) = 'vwPortal_PatientAttribute';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLOrghierarchyOverwrite';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();

	BEGIN TRY

		DECLARE @AttributeTypeId int
		DECLARE @AttributeTitle varchar(255)
		DECLARE @defaultLevelName varchar(255) = ''

--		UPDATE vwPortal_BusinessUnitType
--		SET Name = Replace(Name, 'Attribute:', '')
--		WHERE PatientAttributeTypeId IS NOT NULL AND Name LIKE 'Attribute:%'

		SELECT 
		t1.Name,
		t1.DisplayValue,
		t1.HierarchyLevelNumber,
		t1.PatientAttributeTypeId
		INTO #HierarchyOverwrite
		FROM vwPortal_BusinessUnitType t1
		WHERE t1.DeleteInd = 0 AND t1.PatientAttributeTypeId IS NOT NULL

		IF EXISTS (SELECT 1 FROM #HierarchyOverwrite WHERE HierarchyLevelNumber =1)
		BEGIN
			SELECT @AttributeTypeId = PatientAttributeTypeId,
				   @AttributeTitle = DisplayValue
			FROM #HierarchyOverwrite 
			WHERE HierarchyLevelNumber =1
			
			SET @defaultLevelName = (SELECT ISNULL(SetValue, '') FROM [dbo].[vwPortal_SystemSettings]
			                        WHERE [SettingType] = 'OrgHierarchy' AND [SettingParameter] = 'Level1 Default Name')

			SELECT
			PatientId,
			Value
			INTO #Level1
			FROM
			(
			SELECT 
			PatientId,
			Value,
			ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ModifyDateTime DESC) AS rowNbr
			FROM vwPortal_PatientAttribute t1
			WHERE t1.PatientAttributeTypeId = @AttributeTypeId
			) t WHERE t.rowNbr = 1

			CREATE INDEX IX_#LevelPatientId ON #Level1(PatientId)

			UPDATE O
			SET Level1TypeName = @AttributeTitle, 
				Level1Name = ISNULL(t2.Value, @defaultLevelName)
			FROM OrgHierarchy O
			LEFT JOIN #Level1 t2
			ON O.LbPatientId = t2.PatientId
			
			SET @UpdatedRecord= @UpdatedRecord + @@ROWCOUNT

			DROP TABLE #Level1
		END

		IF EXISTS (SELECT 1 FROM #HierarchyOverwrite WHERE HierarchyLevelNumber =2)
		BEGIN
			SELECT @AttributeTypeId = PatientAttributeTypeId,
				   @AttributeTitle = DisplayValue
			FROM #HierarchyOverwrite 
			WHERE HierarchyLevelNumber =2

			SET @defaultLevelName = (SELECT ISNULL(SetValue, '') FROM [dbo].[vwPortal_SystemSettings]
			                        WHERE [SettingType] = 'OrgHierarchy' AND [SettingParameter] = 'Level2 Default Name')
			SELECT
			PatientId,
			Value
			INTO #Level2
			FROM
			(
			SELECT 
			PatientId,
			Value,
			ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ModifyDateTime DESC) AS rowNbr
			FROM vwPortal_PatientAttribute t1
			WHERE t1.PatientAttributeTypeId = @AttributeTypeId
			) t WHERE t.rowNbr = 1

			CREATE INDEX IX_#LevelPatientId ON #Level2(PatientId)

			UPDATE O
			SET Level2TypeName = @AttributeTitle, 
				Level2Name = ISNULL(t2.Value, @defaultLevelName)
			FROM OrgHierarchy O
			LEFT JOIN #Level2 t2
			ON O.LbPatientId = t2.PatientId
			
			SET @UpdatedRecord= @UpdatedRecord + @@ROWCOUNT

			DROP TABLE #Level2
		END

		IF EXISTS (SELECT 1 FROM #HierarchyOverwrite WHERE HierarchyLevelNumber =3)
		BEGIN
			SELECT @AttributeTypeId = PatientAttributeTypeId,
				   @AttributeTitle = DisplayValue
			FROM #HierarchyOverwrite 
			WHERE HierarchyLevelNumber =3

			SET @defaultLevelName = (SELECT ISNULL(SetValue, '') FROM [dbo].[vwPortal_SystemSettings]
			                        WHERE [SettingType] = 'OrgHierarchy' AND [SettingParameter] = 'Level3 Default Name')

			SELECT
			PatientId,
			Value
			INTO #Level3
			FROM
			(
			SELECT 
			PatientId,
			Value,
			ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ModifyDateTime DESC) AS rowNbr
			FROM vwPortal_PatientAttribute t1
			WHERE t1.PatientAttributeTypeId = @AttributeTypeId
			) t WHERE t.rowNbr = 1

			CREATE INDEX IX_#LevelPatientId ON #Level3(PatientId)

			UPDATE O
			SET Level3TypeName = @AttributeTitle, 
				Level3Name = ISNULL(t2.Value, @defaultLevelName)
			FROM OrgHierarchy O
			LEFT JOIN #Level3 t2
			ON O.LbPatientId = t2.PatientId
			
			SET @UpdatedRecord= @UpdatedRecord + @@ROWCOUNT

			DROP TABLE #Level3
		END

		IF EXISTS (SELECT 1 FROM #HierarchyOverwrite WHERE HierarchyLevelNumber =4)
		BEGIN
			SELECT @AttributeTypeId = PatientAttributeTypeId,
				   @AttributeTitle = DisplayValue
			FROM #HierarchyOverwrite 
			WHERE HierarchyLevelNumber =4

			SET @defaultLevelName = (SELECT ISNULL(SetValue, '') FROM [dbo].[vwPortal_SystemSettings]
			                        WHERE [SettingType] = 'OrgHierarchy' AND [SettingParameter] = 'Level4 Default Name')

			SELECT
			PatientId,
			Value
			INTO #Level4
			FROM
			(
			SELECT 
			PatientId,
			Value,
			ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ModifyDateTime DESC) AS rowNbr
			FROM vwPortal_PatientAttribute t1
			WHERE t1.PatientAttributeTypeId = @AttributeTypeId
			) t WHERE t.rowNbr = 1

			CREATE INDEX IX_#LevelPatientId ON #Level4(PatientId)

			UPDATE O
			SET Level4TypeName = @AttributeTitle, 
				Level4Name = ISNULL(t2.Value, @defaultLevelName)
			FROM OrgHierarchy O
			LEFT JOIN #Level4 t2
			ON O.LbPatientId = t2.PatientId
			
			SET @UpdatedRecord= @UpdatedRecord + @@ROWCOUNT

			DROP TABLE #Level4
		END

		IF EXISTS (SELECT 1 FROM #HierarchyOverwrite WHERE HierarchyLevelNumber =5)
		BEGIN
			SELECT @AttributeTypeId = PatientAttributeTypeId,
				   @AttributeTitle = DisplayValue
			FROM #HierarchyOverwrite 
			WHERE HierarchyLevelNumber =5

			SET @defaultLevelName = (SELECT ISNULL(SetValue, '') FROM [dbo].[vwPortal_SystemSettings]
			                        WHERE [SettingType] = 'OrgHierarchy' AND [SettingParameter] = 'Level5 Default Name')

			SELECT
			PatientId,
			Value
			INTO #Level5
			FROM
			(
			SELECT 
			PatientId,
			Value,
			ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ModifyDateTime DESC) AS rowNbr
			FROM vwPortal_PatientAttribute t1
			WHERE t1.PatientAttributeTypeId = @AttributeTypeId
			) t WHERE t.rowNbr = 1

			CREATE INDEX IX_#LevelPatientId ON #Level5(PatientId)

			UPDATE O
			SET Level5TypeName = @AttributeTitle, 
				Level5Name = ISNULL(t2.Value, @defaultLevelName)
			FROM OrgHierarchy O
			LEFT JOIN #Level5 t2
			ON O.LbPatientId = t2.PatientId
			
			SET @UpdatedRecord= @UpdatedRecord + @@ROWCOUNT

			DROP TABLE #Level5
		END

		IF EXISTS (SELECT 1 FROM #HierarchyOverwrite WHERE HierarchyLevelNumber =6)
		BEGIN
			SELECT @AttributeTypeId = PatientAttributeTypeId,
				   @AttributeTitle = DisplayValue
			FROM #HierarchyOverwrite 
			WHERE HierarchyLevelNumber =6

			SET @defaultLevelName = (SELECT ISNULL(SetValue, '') FROM [dbo].[vwPortal_SystemSettings]
			                        WHERE [SettingType] = 'OrgHierarchy' AND [SettingParameter] = 'Level6 Default Name')
			SELECT
			PatientId,
			Value
			INTO #Level6
			FROM
			(
			SELECT 
			PatientId,
			Value,
			ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ModifyDateTime DESC) AS rowNbr
			FROM vwPortal_PatientAttribute t1
			WHERE t1.PatientAttributeTypeId = @AttributeTypeId
			) t WHERE t.rowNbr = 1

			CREATE INDEX IX_#LevelPatientId ON #Level6(PatientId)

			UPDATE O
			SET Level6TypeName = @AttributeTitle, 
				Level6Name = ISNULL(t2.Value, @defaultLevelName)
			FROM OrgHierarchy O
			LEFT JOIN #Level6 t2
			ON O.LbPatientId = t2.PatientId
			
			SET @UpdatedRecord= @UpdatedRecord + @@ROWCOUNT

			DROP TABLE #Level6
		END
		
		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	

	END TRY

	BEGIN CATCH
	    --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		SET  @ErrorNumber =ERROR_NUMBER();
		SET  @ErrorState =ERROR_STATE();
		SET  @ErrorSeverity=ERROR_SEVERITY();
		SET  @ErrorLine=ERROR_LINE();
		SET  @Comment =left(ERROR_MESSAGE(),400);
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
	
END
GO
