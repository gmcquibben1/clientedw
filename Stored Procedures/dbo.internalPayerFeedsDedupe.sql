SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPayerFeedsDedupe] @TableName varchar(100), @UniqueId varchar(1000)
AS


EXEC InternalDropTempTables 
--declare @TableName varchar(100) = 'PayerFeedsStage_Iowa_Clinic_pr_dx'
--declare @UniqueId varchar(100) = '[Claim_ID]+[Seq_Num]'
DECLARE @SQL NVARCHAR(MAX)
DECLARE @ADDSQL NVARCHAR(1000)

SET @ADDSQL = 'IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '''+@TableName+''' AND COLUMN_NAME = ''DuplicateFlag'')
BEGIN
	ALTER TABLE '+@TableName+' ADD DuplicateFlag bit NULL;
END'

EXEC (@ADDSQL)

set @sql = '
update '+@TableName+' set DuplicateFlag = 0

select '+@UniqueId+' as [UniqueId], FileDate, ROW_NUMBER() OVER (PARTITION BY '+@UniqueId+' ORDER BY FileDate DESC) as [Rank]
into #dupTemp
from '+@TableName+'

update '+@TableName+' set DuplicateFlag = 1 
FROM '+@TableName+'  t1
join #dupTemp t2 on t1.'+@UniqueId+' = t2.UniqueId and t2.FileDate = t1.FileDate and t2.Rank > 1'

EXEC (@sql)

GO
