SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalComposeCustomResultset] (@RequestSubject VARCHAR(100), @Param1 VARCHAR(100) = NULL, @Param2 VARCHAR(100) = NULL)
-- EXEC internalComposeCustomResultset @RequestSubject = 'HealthcareOrg', @Param1 = 2
--EXEC sp_executeSQL N'SELECT ''Patient'', PIRS.DepositingId, '' '', ''''ROM PatientIdsRegistryStage PIRS '
AS
BEGIN

IF @RequestSubject = 'ContractMemberIdentifier' -- CHANGE FOR CUSTOM IMPLEMENTATION OF CONTRACT MEMBER IDENTIFIER AS APPROPRIATE
BEGIN	
	SELECT -- ContractMemberIdentifier
		Family_id,
		'ContractMemberIdentifier' AS IdType,
		CASE WHEN SourceFeed = 'Interface' THEN 
			 (SELECT LTRIM(RTRIM(PatientOtherIdentifier)) FROM vwPortal_InterfacePatientMatched  IPM WHERE UToken = ME.medicareNo)
			 ELSE MedicareNO END AS IdValue ,
		'' As IdMetaType, Me.Source, Me.SourceFeed
	FROM MemberExtract ME
END

IF @RequestSubject = 'DepositingId'
BEGIN
	IF @Param1 = 'Interface'
	BEGIN
		SELECT Utoken AS DepositingId , 'Utoken' AS DepositingIdType, 'Commercial' AS DepositingIdSourceType,
       'Interface' AS DepositingIdSourceFeed, 'Internal' AS DepositingIdMetaType
		FROM vwPortal_InterfacePatientMatched IPM 
		WHERE IPM.LbPatientId IS NULL  
	END 
END

IF @RequestSubject = 'DisplayFriendlyIds'
BEGIN	
	CREATE TABLE #CMI (family_id UNIQUEIDENTIFIER, IdType VARCHAR(100), IdValue VARCHAR(100), IdMetaType VARCHAR(50), Source VARCHAR(50), SourceFeed VARCHAR(50)) 
	
	INSERT INTO  #CMI (family_id,IdType,IdValue,IdMetaType,Source,SourceFeed) -- DIRECT 
	SELECT Me.family_id, 'Display Nbr' AS IDType, LTRIM(RTRIM(IPM.PatientOtherIdentifier)) AS IdValue,'Person Nbr' AS IdMetaType,'Nextgen' AS IdSource,'Interface' AS IdFeed
	FROM  MemberExtract Me 
	INNER JOIN vwPortal_InterfacePatientMatched IPM ON me.medicareNo = IPM.UToken 

	INSERT INTO  #CMI (family_id,IdType,IdValue,IdMetaType,Source,SourceFeed) -- INDIRECT , IN CASE MORE THAN ONE MATCH
	SELECT Me.family_id, 'Display Nbr' AS IDType, MIN(LTRIM(RTRIM(IPM1.PatientOtherIdentifier))) AS IdValue,'Person Nbr' AS IdMetaType,'Nextgen' AS IdSource,'Interface' AS IdFeed
	FROM  MemberExtract Me 
	INNER JOIN PatientIdsConflictsXReference PIR ON Me.family_id = pir.Head_Id AND pir.Head_Id <> pir.Family_Id
	INNER JOIN MemberExtract Me2 ON PIR.Family_Id = ME2.Family_Id 
	INNER JOIN vwPortal_InterfacePatientMatched  IPM1 ON ME2.MedicareNO = IPM1.UToken AND (Me2.Holdback IS NOT NULL OR Me2.Holdback <> 0 )
	LEFT JOIN #CMI IPM2 ON me.family_id = IPM2.family_id 
	WHERE IPM2.family_id IS NULL  GROUP BY Me.family_id

	INSERT INTO  #CMI (family_id,IdType,IdValue,IdMetaType,Source,SourceFeed) -- INDIRECT 
	SELECT Me.family_id, 'Display Nbr' AS IDType, LTRIM(RTRIM(Me.medicareNo)) AS IdValue,'ContractMemberIdentifier' AS IdMetaType,'Nextgen' AS IdSource,'Interface' AS IdFeed
	FROM  MemberExtract Me 
	LEFT JOIN #CMI IPM2 ON me.family_id = IPM2.family_id
	WHERE IPM2.family_id IS NULL 

	SELECT family_id,IdType,IdValue,IdMetaType,Source,SourceFeed FROM #CMI
END

IF @RequestSubject = 'DisambiguationRulesAdditionalSets'
BEGIN
		SELECT 
		A.rooted_id, A.Group_Id, A.Family_Id,Dr.GropuDisambiguationRuleId,DR.IntraGroupPriorityOrder
	FROM PatientIdsConflictsXReference A
	INNER JOIN 	( -- 'Preference for MSSP'
		SELECT Me.family_id
		FROM MemberExtract Me INNER JOIN PatientIdsConflictsXReference PIXR ON ME.family_id = PIXR.Family_Id
		WHERE Me.Source = 'MSSP' AND Me.SourceFeed <> 'Interface'
	) B ON A.Family_Id = B.Family_id   
	INNER JOIN PatientIdsConflictsMergeDisambiguationRules DR ON A.GroupFormationRuleId = DR.GroupFormationRuleId AND DR.GroupDisambiguationRule = 'Preference for MSSP'
END

IF @RequestSubject = 'HealthcareOrg'
BEGIN
	IF @Param1 = '1' -- PROVIDER
	BEGIN	
		SELECT 'Provider' AS EntityName , Prov.ProviderID AS EntityId,
						BU.ExternalCode , 'ExternalCode' AS QueryValueType
		FROM providerextract  p2
		INNER JOIN vwPortal_Provider Prov on p2.ProviderNPI = Prov.NationalProviderIdentifier
		INNER JOIN vwPortal_BusinessUnit BU ON P2.TIn = BU.Name AND BU.DeleteInd = 0 
	END 
	IF @Param1 = '2' -- PATIENT
	BEGIN
		SELECT 'Patient' AS EntityName , PIR.IdValue AS EntityId,
		CAST(Prov.ProviderID AS VARCHAR(100)) AS QueryValue , CAST('Provider' AS VARCHAR(100)) AS QueryValueType
		INTO #X
		FROM MemberExtract ME
		INNER JOIN PatientIdsRegistry PIR ON ME.family_id = PIR.Family_Id AND PIR.IDType = 'Lbpatientid'
		INNER JOIN vwPortal_Provider Prov on Me.ProviderId = Prov.NationalProviderIdentifier
		WHERE EXISTS (SELECT 1 FROM vwPortal_ProviderHealthcareOrg PHO WHERE  Prov.ProviderID = PHO.ProviderID)

		INSERT INTO #X(EntityName,EntityId,QueryValue,QueryValueType)
		SELECT 'Patient' AS EntityName , PIR.IdValue AS EntityId,
				'TOPLEVELIDCOMESHERE' AS QueryValue , 'ExternalCode' AS QueryValueType
		FROM MemberExtract ME
		INNER JOIN PatientIdsRegistry PIR ON ME.family_id = PIR.Family_Id AND PIR.IDType = 'Lbpatientid'
		LEFT JOIN #X X ON PIR.IdValue = X.EntityId WHERE X.EntityId IS NULL  

		SELECT EntityName,EntityId,QueryValue,QueryValueType FROM #X
	END 
END

IF @RequestSubject = 'MSSPCCLFFeeds' -- POST PROCESSING ACTIVITY FOR PAYER SPECIFIC FEED FORMATS
BEGIN
	DECLARE @FilePath VARCHAR(100), @FilePathScrubbed VARCHAR(100), @ACO VARCHAR(100),  @FileSuffix VARCHAR(100), @FileDate VARCHAR(100), @DynSql VARCHAR(8000)	
	SET @FilePath = @Param2
	SET @FilePath = CASE WHEN CHARINDEX('\',@FilePath) = 0 THEN '\' + @FilePath ELSE @FilePath END 
	SET @FilePathScrubbed = REVERSE(SUBSTRING(REVERSE(@FilePath),1,CHARINDEX('\',REVERSE(@FilePath))-1))
	SET @ACO		= SUBSTRING (@FilePathScrubbed,3,5) --SUBSTRING (@FilePathScrubbed,3,CHARINDEX('.',@FilePathScrubbed,3)-1)
	SET @FileSuffix = SUBSTRING (@FilePathScrubbed,27,100)

	SET @DynSql = 'UPDATE ' + @Param1 + ' SET ACO = ''' +  @ACO + ''' WHERE ACO IS NULL ' 
	EXEC (@DynSQL) 
	SET @DynSql = 'UPDATE ' + @Param1 + ' SET fileSuffix = ''' +  @FileSuffix + ''' WHERE fileSuffix IS NULL ' 
	EXEC (@DynSQL) 
END

--IF @RequestSubject = 'OrgHierarhy'
--BEGIN
--	;WITH 
--	CTE1 AS (SELECT ProviderId, PatientId, ROW_NUMBER() OVER(PARTITION BY PatientId ORDER BY ModifyDateTime DESC) SNO1 FROM vwPortal_patientprovider PP), 
--	CTE2 AS (SELECT CTE1.PatientId, CTE1.ProviderId, PrHO.HealthcareOrgID, ROW_NUMBER() OVER(PARTITION BY CTE1.PatientId, CTE1.ProviderId ORDER BY PrHO.ModifyDateTime DESC) SNO2 
--			 FROM vwPortal_ProviderHealthcareOrg PrHO INNER JOIN CTE1 ON PrHo.ProviderID = CTE1.ProviderID WHERE CTE1.SNO1 = 1 ), 
--	CTE3 AS (SELECT PHO.PatientId, PHO.HealthcareOrgID, ROW_NUMBER() OVER(PARTITION BY PHO.PatientId ORDER BY CASE WHEN CTE2.HealthCareOrgID = PHO.HealthCareOrgId THEN 999999 ELSE DATEDIFF(DD,0,PHO.ModifyDateTime) END DESC ) SNO3
--			 FROM vwPortal_PatientHealthcareOrg PHO LEFT JOIN CTE2 ON PHO.PatientId = CTE2.PatientID AND CTE2.SNO2 = 1 )
--	SELECT PHO.PatientId, HO.BusinessUnitId INTO #PatientBU FROM CTE3  PHO INNER JOIN vwPortal_HealthcareOrg HO ON PHO.HealthcareOrgID = HO.HealthcareOrgId WHERE SNO3 = 1 

--	------- <<<<<< WOULD NEED HEAVY CUSTOMIZATION OF THE BELOW >>>>>>>>>><
--	SELECT DISTINCT medicareNo AS BENE_HIC_NUM, Me.mailState AS STATE,  Me.mailCity AS City, Me.mailZip AS Zip,
--		   pfh.BusinessUnitId1 AS Level1Id,ISNULL([BusinessUnitName1],'Unassigned') AS Level1Name,'Enterprise' AS Level1TypeName, 
--		   pfh.BusinessUnitId2 AS Level2Id,ISNULL([BusinessUnitName2],'Unassigned') AS Level2Name ,'IPA/LOCATION' AS Level2TypeName,
--		   pfh.BusinessUnitId3 AS Level3Id,ISNULL([BusinessUnitName3],'Unassigned') AS Level3Name,'IPA/LOCATION' AS Level3TypeName, 
--		   pfh.BusinessUnitId4 AS Level4Id,ISNULL([BusinessUnitName4],'Unassigned') AS Level4Name, 'IPA/LOCATION'  AS Level4TypeName,
--		   pfh.BusinessUnitId5 AS Level5Id,ISNULL([BusinessUnitName5],'Unassigned') AS Level5Name,'IPA/LOCATION' AS Level5TypeName,
--		   ISNULL(CAST(pfh.BusinessUnitId6 AS VARCHAR),'TOPLEVELIDCOMESHERE') AS Level6Id,ISNULL([BusinessUnitName6],'Unassigned') AS Level6Name,'IPA/LOCATION' AS Level6TypeName
--	FROM MemberExtract ME
--	INNER JOIN PatientIdsRegistry PIR ON ME.family_id = PIR.Family_Id AND PIR.IDType = 'Lbpatientid'
--	LEFT JOIN #PatientBU PBU ON PIR.IdValue = PBU.PatientID
--	LEFT JOIN [dbo].[PortalFlattenedHierachy] pfh 	ON pbu.BusinessUnitId = pfh.businessunitid

--END

IF @RequestSubject = 'TaskCommunicationGroup'
BEGIN
	SELECT DISTINCT t.TaskId, cg.CommunicationGroupId
	FROM vwPortal_Task t 
	JOIN vwPortal_PatientHealthcareOrg pho on t.PatientId = pho.PatientID
	JOIN vwPortal_HealthcareOrg ho ON ho.HealthcareOrgId = pho.HealthcareOrgID
	JOIN vwPortal_CommunicationGroup cg ON cg.BusinessUnitId = ho.BusinessUnitId 
	LEFT JOIN vwPortal_TaskCommunicationGroup TCG ON T.taskid = tcg.taskid AND tcg.CommunicationGroupId = cg.CommunicationGroupId
	WHERE TCG.taskid IS NULL  AND cg.DeleteInd = 0
END

END  






GO
