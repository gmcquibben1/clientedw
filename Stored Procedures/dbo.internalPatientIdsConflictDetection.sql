SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPatientIdsConflictDetection] @FuzzyMatch CHAR(1) = 'N' 
AS
BEGIN
SET NOCOUNT  ON 
DECLARE @rooted_id VARCHAR(100),  @groupid UNIQUEIDENTIFIER, @GroupFormationRuleId INT , 
		@Comments Varchar(100), @CreateDatetime Datetime, @RaisedById INT = 1  , @ManualOverride INT, @CountRec INT = 1

CREATE TABLE #Numbers  (Number INT)
;WITH
L0   AS(SELECT 1 AS c UNION ALL SELECT 1 UNION ALL SELECT 1), 
L1   AS(SELECT 1 AS c FROM L0 AS A, L0 AS B, L0 AS C), 
L2   AS(SELECT 1 AS c FROM L1 AS A, L1 AS B, L1 AS C), 
Nums AS(SELECT ROW_NUMBER() OVER(ORDER BY c) AS n FROM L2)
INSERT INTO #Numbers(Number)
SELECT n AS Number  FROM Nums ;

-- RAISE FLAG  ::: One Patient By MatchCombination, One Patient by Id, One Contract, Many Names
;WITH CTE1 AS (
	SELECT Source, MedicareNO FROM MemberExtract GROUP BY Source, MedicareNo HAVING COUNT(MEDICARENO) > 1 
)
INSERT INTO PatientIdsConflictsXReferenceProcessQueue (GroupFormationRuleId,rooted_id,family_id,Comments,CreateDateTime,RaisedById)
SELECT 1,Me.MedicareNo , ME.Family_Id , 'Auto Discovery',Getdate(), @RaisedById
FROM MemberExtract ME 
INNER JOIN CTE1 C1 ON ME.medicareNo = C1.medicareNo AND ME.SOURCE = C1.Source
LEFT JOIN PatientIdsConflictsXReferenceProcessQueue XRPQ ON ME.family_id = XRPQ.Family_Id
LEFT JOIN PatientIdsConflictsXReference XR ON ME.family_id = XR.Family_Id
WHERE XR.Family_Id IS NULL AND XRPQ.Family_Id IS NULL 
ORDER BY Me.MedicareNo 

-- RAISE FLAG  ::: 'Many HICNOs, One Patient'
;WITH CTE1 AS (
	SELECT Source, LKey FROM (
	SELECT  ME.Source, ME.family_id, 
			LTRIM(RTRIM(CAST(ISNULL(ME.FirstName,'') AS VARCHAR(100)))) + '|' +
			LTRIM(RTRIM(CAST(ISNULL(ME.LastName,'') AS VARCHAR(100)))) + '|' +
			LTRIM(RTRIM(CONVERT(VARCHAR(10),ISNULL(ME.birthDate,'2000-01-01'),110))) + '|' +
			LTRIM(RTRIM(CAST(ISNULL(ME.sex,'') AS VARCHAR(100)))) AS LKey
	FROM MemberExtract  ME --WHERE SOURCE = 'MSSP' 
	) X  GROUP BY Source, LKey HAVING COUNT(LKey) > 1 
)
INSERT INTO PatientIdsConflictsXReferenceProcessQueue (GroupFormationRuleId,rooted_id,family_id,Comments,CreateDateTime,RaisedById)
SELECT 2,C1.LKey , Me.Family_Id , 'Auto Discovery',Getdate(), @RaisedById
FROM (
	SELECT  Me.Source , ME.family_id, 
			LTRIM(RTRIM(CAST(ISNULL(ME.FirstName,'') AS VARCHAR(100)))) + '|' +
			LTRIM(RTRIM(CAST(ISNULL(ME.LastName,'') AS VARCHAR(100)))) + '|' +
			LTRIM(RTRIM(CONVERT(VARCHAR(10),ISNULL(ME.birthDate,'2000-01-01'),110))) + '|' +
			LTRIM(RTRIM(CAST(ISNULL(ME.sex,'') AS VARCHAR(100)))) AS LKey
	FROM MemberExtract  ME --	WHERE SOURCE = 'MSSP 
	)  ME 
INNER JOIN CTE1 C1 ON ME.LKey = C1.LKey  AND Me.Source = C1.Source
LEFT JOIN PatientIdsConflictsXReferenceProcessQueue XRPQ ON ME.family_id = XRPQ.Family_Id
LEFT JOIN PatientIdsConflictsXReference XR ON ME.family_id = XR.Family_Id
LEFT JOIN PatientIdsConflictsDemergeMandate XRDM ON ME.family_id = XRDM.KeepOut_Family_Id AND ME.LKey = XRDM.rooted_id 
WHERE XR.Family_Id IS NULL AND XRPQ.Family_Id IS NULL AND XRDM.KeepOut_Family_Id IS NULL 
ORDER BY C1.LKey

-- RAISE FLAG  ::: 'Goes acrross contracts'
;WITH CTE1 AS (
SELECT LKey FROM (SELECT Source, LKey FROM (
	SELECT  ME.Source, ME.family_id, 
			LTRIM(RTRIM(CAST(ISNULL(ME.FirstName,'') AS VARCHAR(100)))) + '|' +
			LTRIM(RTRIM(CAST(ISNULL(ME.LastName,'') AS VARCHAR(100)))) + '|' +
			LTRIM(RTRIM(CONVERT(VARCHAR(10),ISNULL(ME.birthDate,'2000-01-01'),110))) + '|' +
			LTRIM(RTRIM(CAST(ISNULL(ME.sex,'') AS VARCHAR(100)))) AS LKey
	FROM MemberExtract  ME --WHERE SOURCE = 'MSSP' 
	) X  GROUP BY Source, LKey ) X  GROUP BY Lkey HAVING COUNT(LKey) > 1 
)
INSERT INTO PatientIdsConflictsXReferenceProcessQueue (GroupFormationRuleId,rooted_id,family_id,Comments,CreateDateTime,RaisedById)
SELECT 3,C1.LKey , Me.Family_Id , 'Auto Discovery',Getdate(), @RaisedById
FROM (
	SELECT  Me.Source , ME.family_id, 
			LTRIM(RTRIM(CAST(ISNULL(ME.FirstName,'') AS VARCHAR(100)))) + '|' +
			LTRIM(RTRIM(CAST(ISNULL(ME.LastName,'') AS VARCHAR(100)))) + '|' +
			LTRIM(RTRIM(CONVERT(VARCHAR(10),ISNULL(ME.birthDate,'2000-01-01'),110))) + '|' +
			LTRIM(RTRIM(CAST(ISNULL(ME.sex,'') AS VARCHAR(100)))) AS LKey
	FROM MemberExtract  ME --	WHERE SOURCE = 'MSSP 
	)  ME 
INNER JOIN CTE1 C1 ON ME.LKey = C1.LKey  --AND Me.Source = C1.Source
LEFT JOIN PatientIdsConflictsXReferenceProcessQueue XRPQ ON ME.family_id = XRPQ.Family_Id
LEFT JOIN PatientIdsConflictsXReference XR ON ME.family_id = XR.Family_Id
LEFT JOIN PatientIdsConflictsDemergeMandate XRDM ON ME.family_id = XRDM.KeepOut_Family_Id AND ME.LKey = XRDM.rooted_id 
WHERE XR.Family_Id IS NULL AND XRPQ.Family_Id IS NULL AND XRDM.KeepOut_Family_Id IS NULL 
ORDER BY C1.LKey

IF @FuzzyMatch = 'Y' 
BEGIN
	TRUNCATE TABLE [PatientIdFuzzyMatchProcessQueue] ;
	TRUNCATE TABLE [PatientIdFuzzyMatchQueueResults] ;
	SELECT @GroupFormationRuleId = GroupFormationRuleId FROM PatientIdsConflictsMergeFormationRules WHERE GroupFormationRule = 'Fuzzy Match'

	;WITH CTE1 AS (
	SELECT  Me1.Source Source1, Me1.SourceFeed SourceFeed1, Me1.medicareNo AS medicareNo1,
			Me1.FirstName FirstName1, Me1.LastName LastName1, Cast(Me1.birthDate as Datetime) DOBDateTime1, Me1.Sex AS Sex1,
			Me1.mailAddrLine1 mailAddrLine11 ,Me1.mailAddrLine2 mailAddrLine21, Me1.mailCity mailCity1, Me1.mailZip mailZip1, Me1.family_id family_id1,
			Me2.Source Source2, Me2.SourceFeed SourceFeed2, Me2.medicareNo AS medicareNo2,
			LTRIM(RTRIM(CAST(ISNULL(Me1.FirstName,'') AS VARCHAR(100)))) + '|' +
				LTRIM(RTRIM(CAST(ISNULL(Me1.LastName,'') AS VARCHAR(100)))) + '|' +
				LTRIM(RTRIM(CONVERT(VARCHAR(10),ISNULL(Me1.birthDate,'2000-01-01'),110))) + '|' +
				LTRIM(RTRIM(CAST(ISNULL(Me1.sex,'') AS VARCHAR(100)))) AS LKey1,
			Me2.FirstName FirstName2, Me2.LastName LastName2, Cast(Me2.birthDate as Datetime) DOBDateTime2, Me2.Sex AS Sex2,
			Me2.mailAddrLine1 mailAddrLine12 ,Me2.mailAddrLine2 mailAddrLine22, Me2.mailCity mailCity2, Me2.mailZip mailZip2, Me2.family_id family_id2,
			LTRIM(RTRIM(CAST(ISNULL(Me2.FirstName,'') AS VARCHAR(100)))) + '|' +
				LTRIM(RTRIM(CAST(ISNULL(Me2.LastName,'') AS VARCHAR(100)))) + '|' +
				LTRIM(RTRIM(CONVERT(VARCHAR(10),ISNULL(Me2.birthDate,'2000-01-01'),110))) + '|' +
				LTRIM(RTRIM(CAST(ISNULL(Me2.sex,'') AS VARCHAR(100)))) AS LKey2		 
	FROM MemberExtract Me1 
	INNER JOIN MemberExtract Me2 ON Me1.birthDate = ME2.birthDate AND Me1.sex = ME2.sex 
		AND Me1.medicareNo > me2.medicareNo 
		AND CHECKSUM(Me1.FirstName,Me1.LastName) <> CHECKSUM(Me2.FirstName,Me2.LastName)
	),
	CTE2 AS (
		SELECT CTE1.family_id1, CTE1.family_id2 
		FROM CTE1 INNER JOIN PatientIdsConflictsXReference XR1 ON CTE1.family_id1 = XR1.Family_Id 
				  INNER JOIN PatientIdsConflictsXReference XR2 ON CTE1.family_id2 = XR2.Family_Id AND XR1.Group_Id = XR2.Group_Id 
	) 
	INSERT INTO [PatientIdFuzzyMatchProcessQueue] ([MedicareNo],[FirstName],[LastName],[DOBDateTime],[Sex],[Address1],[Address2],[City],[ZIP],[Family_Id],[Rooted_id])
	SELECT medicareNo1,
		   FirstName1, LastName1, DOBDateTime1,Sex1,
		   mailAddrLine11 ,mailAddrLine21,mailCity1,mailZip1, family_id1, LKey1
	FROM 
	(SELECT CTE1.medicareNo1,
		   CTE1.FirstName1, CTE1.LastName1, CTE1.DOBDateTime1,CTE1.Sex1,
		   CTE1.mailAddrLine11 ,CTE1.mailAddrLine21,CTE1.mailCity1,CTE1.mailZip1, CTE1.family_id1, CTE1.LKey1  
	FROM CTE1 LEFT JOIN CTE2 ON CTE1.family_id1 = CTE2.family_id1 WHERE CTE2.family_id1 IS NULL 
	UNION
	SELECT CTE1.medicareNo2,
		   CTE1.FirstName2, CTE1.LastName2, CTE1.DOBDateTime2,CTE1.Sex2,
		   CTE1.mailAddrLine12 ,CTE1.mailAddrLine22,CTE1.mailCity2,CTE1.mailZip2, CTE1.family_id2, CTE1.LKey2  
	FROM CTE1 LEFT JOIN CTE2 ON CTE1.family_id2 = CTE2.family_id2 WHERE CTE2.family_id2 IS NULL 
	) X --LEFT JOIN [PatientIdFuzzyMatchProcessQueue] PIF ON X.family_id1 = PIF.[Family_Id] WHERE PIF.[Family_Id] IS NULL 

	;WITH CTE1 AS ( SELECT Rooted_Id, ROW_NUMBER() OVER(PARTITION BY Rooted_ID ORDER BY Rooted_ID) SNO FROM [PatientIdFuzzyMatchProcessQueue] )
	DELETE FROM CTE1 WHERE SNO > 1 

	-- DECLARE AND SET VARIABLES
	DECLARE @execution_id bigint, @EdwOleConnection sql_variant
	SET @EdwOleConnection = N'Data Source=' +CAST(@@ServerName AS NVARCHAR(1000)) + N';Initial Catalog=' + CAST(db_name() AS NVARCHAR(100)) + ';Provider=SQLNCLI11.1;Integrated Security=SSPI;'
	-- CALLS SSIS PACKAGE   BitbucketInteractor BELOW ::: CHANGES TO THE BELOW CODE NOT TYPICALLY NEEDED
	EXEC [SSISDB].[catalog].[create_execution] @package_name=N'FuzzyMatch.dtsx', @execution_id=@execution_id OUTPUT, @folder_name=N'PatientFuzzyMatcher', @project_name=N'PatientFuzzyMatcher', @use32bitruntime=False, @reference_id=Null
	EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=30, @parameter_name=N'EdwOleConnection', @parameter_value=@EdwOleConnection
	EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'LOGGING_LEVEL', @parameter_value=1
	EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'SYNCHRONIZED', @parameter_value=1
	EXEC [SSISDB].[catalog].[start_execution] @execution_id

	SELECT PQ1.Rooted_Id, PQ1.family_id family_id1 , PQ2.family_id family_id2
	into #FuzzyMatch -- DROP TABLE #FuzzyMatch 
	FROM [PatientIdFuzzyMatchProcessQueue] PQ1 
	INNER JOIN [PatientIdFuzzyMatchQueueResults] PQR1 ON PQ1.ProcessQueueId = PQR1.ProcessQueueId AND PQR1._Key_in <> PQR1._key_out
	INNER JOIN [PatientIdFuzzyMatchQueueResults] PQR2 ON PQR1._key_out = PQR2._Key_in  
	INNER JOIN [PatientIdFuzzyMatchProcessQueue] PQ2  ON PQR2.ProcessQueueId = PQ2.ProcessQueueId 
	WHERE  PQR1._score > 0.7 AND PQR1._score <> 1 -- PERFECT SCORE OF 1 ALREADY COVERED BY PREVIOUS ALGORITHMS

	INSERT INTO PatientIdsConflictsXReferenceProcessQueue (GroupFormationRuleId,rooted_id,family_id,Comments)
	SELECT @GroupFormationRuleId,X.Rooted_Id,x.family_id1, 'Fuzzy Match'
	FROM (SELECT Rooted_Id, family_id1 FROM #FuzzyMatch UNION
		  SELECT Rooted_Id, family_id2 FROM #FuzzyMatch ) X 
	--LEFT JOIN PatientIdsConflictsXReferenceProcessQueue PIF ON X.family_id1 = PIF.[Family_Id]
END

SELECT DISTINCT Rooted_ID INTO #RID FROM PatientIdsConflictsXReferenceProcessQueue P2
INSERT INTO PatientIdsConflictsXReferenceProcessQueue (GroupFormationRuleId,rooted_id,family_id,Comments,CreateDateTime,RaisedById)
SELECT DISTINCT  P.GroupFormationRuleId, P.Rooted_id, P.family_id, P.Comments, P.CreateDateTime, P.RaisedbyId 
FROM PatientIdsConflictsXReference P INNER JOIN #RID P2 ON P.Rooted_Id = P2.Rooted_Id 
LEFT JOIN  PatientIdsConflictsXReferenceProcessQueue P3 ON P.rooted_id = P3.rooted_id AND P.family_id = P3.Family_Id 
WHERE P3.family_id IS NULL 

--PatientIdsConflictsXReference
CREATE TABLE #T1 (Rooted_Id VARCHAR(500), Family_id Varchar(100), GroupFormationRuleId VARCHAR(4))
CREATE TABLE #T2 (Rooted_Id VARCHAR(500), Family_id Varchar(100), GroupFormationRuleId VARCHAR(4))

INSERT INTO #T1 (Rooted_Id, Family_id, GroupFormationRuleId ) 
SELECT Rooted_Id, Family_id, GroupFormationRuleId FROM PatientIdsConflictsXReferenceProcessQueue A
WHERE EXISTS (SELECT 1 FROM PatientIdsConflictsXReferenceProcessQueue TINNER WHERE TINNER.Family_id = A.Family_id and TINNER.Rooted_Id <> A.Rooted_Id);

SET @CountRec = 1 
WHILE @CountRec > 0 
BEGIN
WITH CTE1 AS (SELECT Rooted_Id, Family_id, GroupFormationRuleId 
			   FROM #T1 A WHERE EXISTS (SELECT 1 FROM #T1 TINNER WHERE TINNER.Family_id = A.Family_id and TINNER.Rooted_Id <> A.Rooted_Id)), 
CTE2 AS (SELECT A.Rooted_Id, C.Family_id , A.GroupFormationRuleId 
		 FROM CTE1 A INNER JOIN #T1 B ON A.Family_ID = B.Family_ID AND A.Rooted_ID <> B.Rooted_ID  
		 INNER JOIN #T1 C ON B.Rooted_Id = C.Rooted_Id  AND C.Family_id <> B.Family_id ), 
CTE3 AS ( SELECT  * FROM CTE1 UNION SELECT  * FROM CTE2 ),
CTE4 AS ( SELECT  DISTINCT Rooted_Id, GroupFormationRuleId  FROM CTE1 ),
CTE5 AS ( SELECT x.Rooted_id,	(SELECT '|' + Family_id  FROM CTE3 WHERE CTE3.Rooted_Id = x.Rooted_id FOR XML PATH('')) AS CC, x.GroupFormationRuleId  FROM CTE4 x ),
CTE6 AS ( SELECT CC, SUBSTRING(MAX(CAST(GroupFormationRuleId AS VARCHAR(4)) + '~' + Rooted_ID),CHARINDEX('~',MAX(CAST(GroupFormationRuleId AS VARCHAR(4)) + '~' + Rooted_ID)) +1,1000) Rooted_ID, 
				 MAX(GroupFormationRuleId) GroupFormationRuleId  FROM CTE5 GROUP BY CC)
INSERT INTO #T2 (Rooted_Id, Family_id, GroupFormationRuleId )
SELECT ROOTEd_ID , x.Family_Id,GroupFormationRuleId   FROM CTE6
CROSS APPLY (SELECT SUBSTRING(CC,N.Number + 1,CHARINDEX('|', CC + '|', N.Number + 1) - N.Number - 1) Family_Id FROM #Numbers  N WHERE SUBSTRING(CC,N.Number,1) = '|') X

SELECT @CountRec = COUNT(*) FROM (
SELECT Rooted_Id, Family_Id FROM #T2 EXCEPT 
SELECT Rooted_Id, Family_Id FROM #T1 ) X 

DELETE FROM #T1 
INSERT INTO #T1 (Rooted_Id, Family_ID,GroupFormationRuleId) SELECT Rooted_Id, Family_Id,GroupFormationRuleId FROM #T2 
DELETE FROM #T2 
END 

UPDATE B SET rooted_id = A.Rooted_Id 
FROM #T1 A INNER JOIN PatientIdsConflictsXReferenceProcessQueue B ON A.Family_id = B.Family_id
WHERE  A.rooted_id <> B.Rooted_Id 

;WITH CTE AS (SELECT Rooted_Id, Family_Id, ROW_NUMBER() OVER(PARTITION BY Rooted_Id, Family_Id ORDER BY Family_Id) SNO FROM PatientIdsConflictsXReferenceProcessQueue )
DELETE FROM CTE WHERE SNO > 1 

-- DROP TABLE #XRiFIP1
SELECT Rooted_id,
	(SELECT '|' + CAST(Xri.Family_id AS VARCHAR(100))  FROM PatientIdsConflictsXReferenceProcessQueue Xri WHERE Xri.Rooted_Id = x.Rooted_id FOR XML PATH(''))  AS FIP
INTO #XRiFIP1
FROM PatientIdsConflictsXReferenceProcessQueue x 
GROUP BY Rooted_ID 

UPDATE PatientIdsConflictsXReferenceProcessQueue
SET FamilyIdsPack = xx.FIP 
FROM  PatientIdsConflictsXReferenceProcessQueue x  INNER JOIN #XRiFIP1 xx ON x.rooted_id = xx.rooted_id 

-- DROP TABLE #XRiFIP2
SELECT Rooted_id,
	(SELECT '|' + CAST(Xri.Family_id AS VARCHAR(100))  FROM PatientIdsConflictsXReference Xri WHERE Xri.Rooted_Id = x.Rooted_id FOR XML PATH(''))  AS FIP
INTO #XRiFIP2
FROM PatientIdsConflictsXReference x 
GROUP BY Rooted_ID 

UPDATE PatientIdsConflictsXReference
SET FamilyIdsPack = xx.FIP 
FROM  PatientIdsConflictsXReference x  INNER JOIN #XRiFIP2 xx ON x.rooted_id = xx.rooted_id 

-- Matching on FIP and awarding it out -- DROP TABLE #TransferOver
CREATE TABLE #TransferOver (Rooted_Id VARCHAR(500), Family_Id UniqueIdentifier, Group_Id UniqueIdentifier)

;WITH CTE0 AS (SELECT DISTINCT A.Rooted_Id , ISNULL(B.FamilyIdsPack,A.FamilyIdsPack) FamilyIdsPack
				FROM PatientIdsConflictsXReferenceProcessQueue A LEFT JOIN PatientIdsConflictsXReference B ON A.Family_id = B.Family_id ),
CTE1 AS ( 
SELECT ROOTEd_ID , x.Family_Id  
FROM CTE0 CROSS APPLY (SELECT SUBSTRING(FamilyIdsPack,N.Number + 1,CHARINDEX('|', FamilyIdsPack + '|', N.Number + 1) - N.Number - 1) Family_Id FROM #Numbers  N WHERE SUBSTRING(FamilyIdsPack,N.Number,1) = '|') X)
INSERT INTO #TransferOver (Rooted_Id,Family_Id)
SELECT DISTINCT Rooted_Id, Family_ID FROM CTE1 

--PatientIdsConflictsXReference
TRUNCATE TABLE #T1  
TRUNCATE TABLE #T2 
INSERT INTO #T1 (Rooted_Id, Family_id, GroupFormationRuleId ) 
SELECT Rooted_Id, Family_id, 0 FROM #TransferOver A
WHERE EXISTS (SELECT 1 FROM #TransferOver TINNER WHERE TINNER.Family_id = A.Family_id and TINNER.Rooted_Id <> A.Rooted_Id) -- DECLARE @CountRec INT 

SET @CountRec = 1 
WHILE @CountRec > 0 
BEGIN
;WITH CTE1 AS (SELECT Rooted_Id, Family_id, GroupFormationRuleId 
			   FROM #T1 A WHERE EXISTS (SELECT 1 FROM #T1 TINNER WHERE TINNER.Family_id = A.Family_id and TINNER.Rooted_Id <> A.Rooted_Id)), -- BASE LAYER S1 and S2 
CTE2 AS (SELECT A.Rooted_Id, C.Family_id , A.GroupFormationRuleId 
		 FROM CTE1 A INNER JOIN #T1 B ON A.Family_ID = B.Family_ID AND A.Rooted_ID <> B.Rooted_ID  
		 INNER JOIN #T1 C ON B.Rooted_Id = C.Rooted_Id  AND C.Family_id <> B.Family_id ), -- S2 MORPHS TO S1 SELECT 
CTE3 AS ( SELECT  * FROM CTE1 UNION SELECT  * FROM CTE2 ),
CTE4 AS ( SELECT  DISTINCT Rooted_Id, GroupFormationRuleId  FROM CTE1 ),
CTE5 AS ( SELECT Rooted_id,
			(SELECT '|' + Family_id  FROM CTE3 WHERE CTE3.Rooted_Id = x.Rooted_id FOR XML PATH(''))  AS CC
		  FROM CTE4 x ),
CTE6 AS ( SELECT CC, MIN(Rooted_ID) Rooted_ID FROM CTE5 GROUP BY CC)
INSERT INTO #T2 (Rooted_Id, Family_id)
SELECT ROOTEd_ID , x.Family_Id  FROM CTE6
CROSS APPLY (SELECT SUBSTRING(CC,N.Number + 1,CHARINDEX('|', CC + '|', N.Number + 1) - N.Number - 1) Family_Id FROM #Numbers  N WHERE SUBSTRING(CC,N.Number,1) = '|') X 

SELECT @CountRec = COUNT(*) FROM (
SELECT Rooted_Id, Family_Id FROM #T2
EXCEPT 
SELECT Rooted_Id, Family_Id FROM #T1 ) X 

TRUNCATE TABLE #T1 
INSERT INTO #T1 (Rooted_Id, Family_ID) SELECT Rooted_Id, Family_Id FROM #T2 
TRUNCATE TABLE #T2 
END 

UPDATE B SET rooted_id = A.Rooted_Id 
FROM #T1 A INNER JOIN #TransferOver B ON A.Family_id = B.Family_id
WHERE  A.rooted_id <> B.Rooted_Id 

;WITH CTE AS (SELECT Rooted_Id, Family_Id, ROW_NUMBER() OVER(PARTITION BY Rooted_Id, Family_Id ORDER BY Family_Id) SNO FROM #TransferOver )
DELETE FROM CTE WHERE SNO > 1 

;WITH CTE0 AS (SELECT DISTINCT Rooted_ID FROM #TransferOver)
SELECT NEWID() Group_Id, Rooted_ID  INTO #GroupIds FROM CTE0;

UPDATE #TransferOver SET Group_Id = #GroupIds.Group_Id
FROm #TransferOver INNER JOIN #GroupIds ON #TransferOver.Rooted_ID = #GroupIds.Rooted_ID

-- Final arrangement 
SELECT DISTINCT A.rooted_id OUTRID, B.Rooted_Id INRID INTO #TransferredRootIds 
FROM PatientIdsConflictsXReference A JOIN #TransferOver  B ON A.Family_Id = B.Family_Id AND A.rooted_id <> B.rooted_id

DELETE PatientIdsConflictsXReference
FROM PatientIdsConflictsXReference JOIN #TransferOver ON PatientIdsConflictsXReference.Family_Id = #TransferOver.Family_Id

INSERT INTO PatientIdsConflictsXReference (GroupFormationRuleId, rooted_id, Group_Id, Family_Id, Comments, CreateDateTime, RaisedById,ManualOverride)
SELECT A.GroupFormationRuleId, A.rooted_id, B.Group_Id, A.family_id, A.Comments , A.CreateDateTime, A.RaisedById, A.ManualOverride 
FROM   PatientIdsConflictsXReferenceProcessQueue A INNER JOIN #TransferOver B ON A.rooted_id = B.rooted_id AND A.family_id = B.Family_Id WHERE B.Family_Id IS NOT NULL 
UNION
SELECT 0, A.rooted_id, A.Group_Id, A.family_id, 'Cross Groups Pull' , GetDate(), 0, 0 
FROM  #TransferOver A LEFT JOIN PatientIdsConflictsXReferenceProcessQueue B ON A.rooted_id = B.rooted_id AND A.family_id = B.Family_Id WHERE B.Family_Id IS NULL 

-- CLEANUP 
UPDATE MemberExtract SET Holdback = 1 FROM MemberExtract ME INNER JOIN PatientIdsConflictsXReference F ON ME.family_id = F.family_id AND f.Head_Id IS NULL 
UPDATE PatientIdsConflictsDemergeMandate SET rooted_id = #TransferredRootIds.INRID 
FROM PatientIdsConflictsDemergeMandate INNER JOIN #TransferredRootIds ON PatientIdsConflictsDemergeMandate.rooted_id = #TransferredRootIds.OUTRID
TRUNCATE TABLE PatientIdsConflictsXReferenceProcessQueue
END 
GO
