SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =====================================================================================
-- Author:		Sam Guo
-- Create date: 
-- Description:	this SP load the patient information into PatientIdsRegistryStage
-- Modified By: Lan
-- Description:	changed the logic on how to get the patient sourceSystemID
-- Modified Date: 2016-08-30  
-- =========================================================================================
/*
-- ================================================================
	Version		Date		Author	Change
	-------		----------	------	------		
	2.1.2      2016-11-11   YL      LBETL-224 fix gender replace value "0" with "3"
    2.2.1      2017-01-23   YL      LBETL-576 fix address that exceed 50 chars

*/

CREATE PROCEDURE [dbo].[ETLClaimsPatient]
@FullLoad CHAR(1) = 'N'
WITH EXEC AS CALLER
AS
BEGIN
-- FROM ADHOC PAYER FEEDS
-- removed.

	DECLARE @vSourceSystemID INT 


IF @FullLoad = 'Y'  
  BEGIN
	------------------------------------------------------
	-- Load CCLFS Claims from CCLF_8 table
	------------------------------------------------------

	--remove this part of getting sourceSystemID for CCLF8 members, get the sourceSystemID by matching the 'sourceFeed' in CCLF8 with 'settingValue' 
	--in SourceSystemSetting, changed by Lan 8/30/2016
	--SET @vSourceSystemID = (SELECT MIN(SourceSystemId)  	FROM SourceSystemSetting SSS WHERE
	--	SSS.SettingValue = 'Bene_Hic_Num' AND SSS.SettingName = 'ClaimFeedsIdTypeName' )

	INSERT INTO DBO.PatientIdsRegistryStage ([LastName],[FirstName],[MiddleName],Gender,DobDateTime, [ZIP],DepositingId,DepositingIdType,
									     DepositingIdMetaType,DepositingIdSourceType,DepositingIdSourceFeed
										 , SourceSystemId)
	SELECT DISTINCT Bene.BENE_LAST_NAME, Bene.BENE_1ST_NAME, Bene.BENE_MIDL_NAME, Replace(Bene.BENE_SEX_CD,'0','3'), Bene.Bene_DOB,Bene.BENE_ZIP_CD, BENE_HIC_NUM, 'BENE_HIC_NUM' AS DepositingIdType, 
       '3rd Party' AS DepositingIdMetaType, ISNULL(Bene.SourceFeed,'MSSP') AS DepositingIdSourceType,'CCLF' AS DepositingIdSourceFeed 
	   --, CAST(@vSourceSystemID AS INT)
	   ,SourceSystemId 
	FROM CCLF_8_BeneDemo Bene (NOLOCK)
	LEFT JOIN [dbo].[SourceSystemSetting] t2  --get the sourceSystemID by matching the 'sourceFeed' in CCLF8 with 'settingValue' in SourceSystemSetting, changed by Lan 8/30/2016
    ON Bene.SourceFeed = [SettingValue]
	--LEFT JOIN PatientIdsRegistryStage  Stage ON Bene.BENE_HIC_NUM = Stage.DepositingId AND Stage.DepositingIdType = 'BENE_HIC_NUM'
	--WHERE Stage.DepositingId IS NULL AND Bene.BENE_LAST_NAME IS NOT NULL 
	--AND NOT EXISTS (SELECT 1 FROM MemberExtract ME (NOLOCK) WHERE Bene.BENE_HIC_NUM = ME.medicareNo )
	
	------------------------------------------------------
	-- FROM MRA
	------------------------------------------------------
	SET @vSourceSystemID = (SELECT MIN(SourceSystemId) 	FROM SourceSystemSetting SSS WHERE
		SSS.SettingValue in( 'MSSP','NGACO') )

	INSERT INTO DBO.PatientIdsRegistryStage ([LastName],[FirstName],[MiddleName],Gender,DobDateTime, [ZIP],DepositingId,DepositingIdType,
									     DepositingIdMetaType,DepositingIdSourceType,DepositingIdSourceFeed , SourceSystemId)
	SELECT DISTINCT MRA.[LAST NAME], MRA.[FIRST NAME], NULL AS BENE_MIDL_NAME, Replace(MRA.Sex1,'0','3'), MRA.[Birth DATE],NULL AS BENE_ZIP_CD, MRA.HICNO, 'BENE_HIC_NUM' AS  DepositingIdType, 
       '3rd Party' AS DepositingIdMetaType, 'MSSP' AS DepositingIdSourceType,'MRA' AS  DepositingIdSourceFeed
	   , CAST(@vSourceSystemID AS INT)
	FROM MemberRosterArchives MRA  (NOLOCK)
	--LEFT JOIN PatientIdsRegistryStage  Stage ON MRA.HICNO = Stage.DepositingId 
	--WHERE Stage.DepositingId IS NULL AND MRA.[Last Name] IS NOT NULL 
	--AND NOT EXISTS (SELECT 1 FROM MemberExtract ME (NOLOCK) WHERE MRA.HICNO = ME.medicareNo )

	------------------------------------------------------
	-- FROM PatientRosterEnrollmentRaw
	------------------------------------------------------
	INSERT INTO dbo.PatientIdsRegistryStage (
	LastName  ,FirstName  ,Gender  ,DOBDateTime  ,ZIP  ,DepositingId  ,DepositingIdType  ,CreateTimestamp
	,DepositingIdSourceFeed  ,DepositingIdMetaType  ,DepositingIdSourceType
	,homeAddrLine1  ,homeCity  ,homeState  ,homeZIP  ,homePhone  ,mailAddrLine1
	,mailCity  ,mailState  ,mailZIP  ,homeEMail, SourceSystemId) 
	SELECT DISTINCT r.LastName, r.FirstName,Replace(r.Gender,'0','3') , r.Birthdate, r.Zip, PatientExternalID, 'Cardholder ID', getdate() 
		, SourceFeed, '3rd Party', SourceFeed, LEFT(Address,50) as [Address], City, State, r.Zip, Phone, LEFT(Address,50) as [Address], City, State, r.Zip, Email , 
		CAST(r.SourceSystemID AS INT)
	FROM PatientRosterEnrollmentRaw r
	--LEFT JOIN PatientIdsRegistryStage s on r.PatientExternalID = s.DepositingId
	--WHERE s.DepositingId IS NULL

	------------------------------------------------------
	-- FROM PatientRosterMonthsRaw
	------------------------------------------------------
	INSERT INTO dbo.PatientIdsRegistryStage (
	LastName  ,FirstName  ,Gender  ,DOBDateTime  ,ZIP  ,DepositingId  ,DepositingIdType  ,CreateTimestamp
	,DepositingIdSourceFeed  ,DepositingIdMetaType  ,DepositingIdSourceType
	,homeAddrLine1  ,homeCity  ,homeState  ,homeZIP  ,homePhone  ,mailAddrLine1
	,mailCity  ,mailState  ,mailZIP  ,homeEMail, SourceSystemId) 
	SELECT DISTINCT r.LastName, r.FirstName,Replace(r.Gender,'0','3'), r.Birthdate, r.Zip, PatientExternalID, 'Cardholder ID', getdate() 
		, SourceFeed, '3rd Party', SourceFeed, LEFT(Address,50) as [Address], City, State, r.Zip, Phone, LEFT(Address,50) as [Address], City, State, r.Zip, Email , 
		CAST(r.SourceSystemID AS INT)
	FROM PatientRosterMonthsRaw r

  END  -- end-of-full-load

ELSE

  BEGIN	-- start of incremental load
	------------------------------------------------------
	-- Load CCLFS Claims from CCLF_8 table
	------------------------------------------------------
	
	--remove this part of getting sourceSystemID for CCLF8 members, get the sourceSystemID by matching the 'sourceFeed' in CCLF8 with 'settingValue' 
	--in SourceSystemSetting, changed by Lan 8/30/2016
	--SET @vSourceSystemID = (SELECT MIN(SourceSystemId) 	FROM SourceSystemSetting SSS WHERE
	--	SSS.SettingValue = 'Bene_Hic_Num' AND SSS.SettingName = 'ClaimFeedsIdTypeName' )

	INSERT INTO DBO.PatientIdsRegistryStage ([LastName],[FirstName],[MiddleName],Gender,DobDateTime, [ZIP],DepositingId,DepositingIdType,
									     DepositingIdMetaType,DepositingIdSourceType,DepositingIdSourceFeed
										 , SourceSystemId)
	SELECT DISTINCT Bene.BENE_LAST_NAME, Bene.BENE_1ST_NAME, Bene.BENE_MIDL_NAME,Replace(Bene.BENE_SEX_CD,'0','3'), Bene.Bene_DOB,Bene.BENE_ZIP_CD, BENE_HIC_NUM Utoken, 'BENE_HIC_NUM' AS DepositingIdType, 
       '3rd Party' AS DepositingIdMetaType, ISNULL(Bene.SourceFeed,'MSSP') AS DepositingIdSourceType,'CCLF' AS DepositingIdSourceFeed 
	   --, CAST(@vSourceSystemID AS INT)
		,t2.SourceSystemId 
	FROM CCLF_8_BeneDemo Bene (NOLOCK)
	LEFT JOIN [dbo].[SourceSystemSetting] t2
    ON Bene.SourceFeed = [SettingValue]
	LEFT JOIN PatientIdsRegistryStage  Stage ON Bene.BENE_HIC_NUM = Stage.DepositingId AND Stage.DepositingIdType = 'BENE_HIC_NUM'
	AND t2.SourceSystemId = Stage.SourceSystemId -- sourceSystemID should be added for patient matching because source patient ID can be same from different system, changed by Lan 8/30/2016
	WHERE Stage.DepositingId IS NULL AND Bene.BENE_LAST_NAME IS NOT NULL 
	AND NOT EXISTS (SELECT 1 FROM MemberExtract ME (NOLOCK) WHERE Bene.BENE_HIC_NUM = ME.medicareNo )
	
	------------------------------------------------------
	-- FROM MRA
	------------------------------------------------------
	SET @vSourceSystemID = (SELECT MIN(SourceSystemId) 	FROM SourceSystemSetting SSS WHERE
		SSS.SettingValue in( 'MSSP','NGACO'))

	INSERT INTO DBO.PatientIdsRegistryStage ([LastName],[FirstName],[MiddleName],Gender,DobDateTime, [ZIP],DepositingId,DepositingIdType,
									     DepositingIdMetaType,DepositingIdSourceType,DepositingIdSourceFeed
										 , SourceSystemId)
	SELECT DISTINCT MRA.[LAST NAME], MRA.[FIRST NAME], NULL AS BENE_MIDL_NAME, Replace(MRA.Sex1,'0','3'), MRA.[Birth DATE],NULL AS BENE_ZIP_CD, MRA.HICNO Utoken, 'BENE_HIC_NUM' AS  DepositingIdType, 
       '3rd Party' AS DepositingIdMetaType, 'MSSP' AS DepositingIdSourceType,'MRA' AS  DepositingIdSourceFeed
	   , CAST(@vSourceSystemID AS INT)
	FROM MemberRosterArchives MRA  (NOLOCK)
	LEFT JOIN PatientIdsRegistryStage  Stage ON MRA.HICNO = Stage.DepositingId 
	AND Stage.SourceSystemId = @vSourceSystemID -- sourceSystemID should be added for patient matching because source patient ID can be same from different system, changed by Lan 8/30/2016
	WHERE Stage.DepositingId IS NULL AND MRA.[LAST NAME] IS NOT NULL 
	AND NOT EXISTS (SELECT 1 FROM MemberExtract ME (NOLOCK) WHERE MRA.HICNO = ME.medicareNo )

	------------------------------------------------------
	-- FROM PatientRosterEnrollmentRaw
	------------------------------------------------------
	INSERT INTO dbo.PatientIdsRegistryStage (
	LastName  ,FirstName  ,Gender  ,DOBDateTime  ,ZIP  ,DepositingId  ,DepositingIdType  ,CreateTimestamp
	,DepositingIdSourceFeed  ,DepositingIdMetaType  ,DepositingIdSourceType
	,homeAddrLine1  ,homeCity  ,homeState  ,homeZIP  ,homePhone  ,mailAddrLine1
	,mailCity  ,mailState  ,mailZIP  ,homeEMail, SourceSystemId) 
	SELECT DISTINCT r.LastName, r.FirstName, Replace(r.Gender,'0','3'), r.Birthdate, r.Zip, PatientExternalID, 'Cardholder ID', getdate() 
		, SourceFeed, '3rd Party', SourceFeed, LEFT(Address,50) as [Address], City, State, r.Zip, Phone, LEFT(Address,50) as [Address], City, State, r.Zip, Email , 
		CAST(r.SourceSystemID AS INT)
	FROM PatientRosterEnrollmentRaw r
	LEFT JOIN PatientIdsRegistryStage s ON r.PatientExternalID = s.DepositingId
	AND r.SourceSystemId = s.SourceSystemId -- -- sourceSystemID should be added for patient matching because source patient ID can be same from different system, changed by Lan 8/30/2016
	WHERE s.DepositingId IS NULL

	------------------------------------------------------
	-- FROM PatientRosterMonthsRaw
	------------------------------------------------------
	INSERT INTO dbo.PatientIdsRegistryStage (
	LastName  ,FirstName  ,Gender  ,DOBDateTime  ,ZIP  ,DepositingId  ,DepositingIdType  ,CreateTimestamp
	,DepositingIdSourceFeed  ,DepositingIdMetaType  ,DepositingIdSourceType
	,homeAddrLine1  ,homeCity  ,homeState  ,homeZIP  ,homePhone  ,mailAddrLine1
	,mailCity  ,mailState  ,mailZIP  ,homeEMail, SourceSystemId) 
	SELECT DISTINCT r.LastName, r.FirstName, Replace(r.Gender,'0','3'), r.Birthdate, r.Zip, PatientExternalID, 'Cardholder ID', getdate() 
		, SourceFeed, '3rd Party', SourceFeed, LEFT(Address,50) as [Address], City, State, r.Zip, Phone, LEFT(Address,50) as [Address], City, State, r.Zip, Email , 
		CAST(r.SourceSystemID AS INT)
	FROM PatientRosterMonthsRaw r
	LEFT JOIN PatientIdsRegistryStage s ON r.PatientExternalID = s.DepositingId
	AND r.SourceSystemId = s.SourceSystemId -- -- sourceSystemID should be added for patient matching because source patient ID can be same from different system, changed by Lan 8/30/2016
	WHERE s.DepositingId IS NULL

   END  -- else-end

 END





GO
