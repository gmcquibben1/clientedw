SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLStagedPatient_Depricated] 
AS
-- UPDATE PatientIdsRegistryStage SET processedind = NULL 
CREATE Table #ProcessedTable (DepositingId Varchar(50),	DepositingIdType Varchar(15), DepositingAction varchar(50))

--SELECT * FROM #ProcessedTable
--SELECT * FROM PatientIdsRegistry
-- TRUNCATE TABLE PatientIdsRegistry
--INSERT INTO PatientIdsRegistry (Family_Id,IdMetaType,IdType,IdValue,IdFeed)
--Select me.Family_Id,'Internal',  Me.Source ,MedicareNo,Me.SourceFeed
--FROM MemberExtract ME 
--LEFT JOIN PatientIdsRegistry PIR ON Me.family_ID = PIR.Family_Id AND ME.medicareNo = PIR.IdValue AND PIR.IDType = Me.Source
--WHERE PIR.Family_Id IS NULL 

-- 1 ::: READY TO GO
UPDATE PatientIdsRegistryStage
SET ProcessedInd = 1 -- SELECT *
FROM PatientIdsRegistryStage PIRS 
INNER JOIN MemberExtract Me 
ON PIRS.FirstName = Me.FirstName and Pirs.LastName = Me.lastName and pirs.DOBDateTime = me.birthDate and pirs.Gender = me.sex 
INNER JOIN PatientIdsRegistry PIR ON Me.family_id = PIR.Family_id AND PIR.IdType = Pirs.DepositingIdType AND PIR.IdValue = Pirs.DepositingId
WHERE PIRS.ProcessedInd IS NULL 

INSERT INTO PatientIdsRegistry (Family_Id,IdType,IdValue,IdMetaType,IdFeed,IdSource,IdComments)
OUTPUT INSERTED.IDType, INSERTED.IdValue INTO #ProcessedTable (DepositingIdType,DepositingId)
SELECT me.family_id, pirs.DepositingIdType, Pirs.DepositingId,Pirs.DepositingIdMetaType,Pirs.DepositingIdSourceFeed,PIRS.DepositingIdSourceType ,'High Match'
FROM  PatientIdsRegistryStage PIRS
INNER JOIN MemberExtract Me 
ON PIRS.FirstName = Me.FirstName and Pirs.LastName = Me.lastName and pirs.DOBDateTime = me.birthDate and pirs.Gender = me.sex 
LEFT JOIN PatientIdsRegistry PIR ON Me.family_id = PIR.Family_id AND PIR.IdType = Pirs.DepositingIdType AND PIR.IdValue = Pirs.DepositingId
WHERE PIR.Family_Id IS NULL AND PIRS.ProcessedInd IS NULL 

UPDATE PatientIdsRegistryStage
SET ProcessedInd = 1
FROM PatientIdsRegistryStage PIRS INNER JOIN #ProcessedTable PT ON PIRS.DepositingId = PT.DepositingId AND PIRS.DepositingIdType = PT.DepositingIDType

TRUNCATE TABLE [PatientIDMatchMuster]

INSERT INTO  [PatientIDMatchMuster]
(IdType,IdValue,[FirstName],[LastName],[DOBDateTime],[Address1],[Address2],[City],[ZIP])
SELECT  'Incoming', Pirs.DepositingId,
PIRS.FirstName, PIRS.LastName, Cast(PIRS.DOBDateTime as Datetime) DOBDateTime,NULL,NULL, NULL, PIRS.ZIP 
FROM PatientIdsRegistryStage PIRS
WHERE ProcessedInd IS NULL 

INSERT INTO [PatientIDMatchMuster]
(IdType,IdValue,[FirstName],[LastName],[DOBDateTime],[Address1],[Address2],[City],[ZIP])
SELECT  'Existing', Me.medicareNo,
Me.FirstName, Me.LastName, Cast(Me.birthDate as Datetime) DOBDateTime,NULL,NULL, NULL, PIRS.ZIP 
FROM MemberExtract Me INNER JOIN 
[PatientIDMatchMuster] PIRS ON ME.birthDate = PIRS.DOBDateTime

--INSERT INTO PatientIdsRegistry (Family_Id,IdType,IdValue,IdComments,IdMetaType,IdFeed)
--OUTPUT INSERTED.IDType, INSERTED.IdValue INTO #ProcessedTable (DepositingIdType,DepositingId)
--Select me.family_id, pirs.DepositingIdType, Pirs.DepositingId, 'Fuzzy matched',Pirs.DepositingIdMetaType,Pirs.DepositingIdSourceFeed
--FROM PatientIdsRegistryStage Pirs INNER JOIN 
--(SELECT DISTINCT PM1.Idtype AS Idtype1 , PM1.IdValue as IdValue1,PM2.IdType As IdType2 , PM2.IdValue As IdValue2
--			 FROM [dbo].[PatientIDMatchMuster]  PM1 
--			INNER JOIN [dbo].[PatientIDMatchMuster]  PM2 ON PM1._key_out = PM2._key_in
--			WHERE pm1.Idtype = 'Incoming' AND pm2.Idtype = 'Existing' AND PM1._score > 0.7 ) PIM 
-- ON PIM.Idtype1 = 'Incoming' AND PIM.IdValue1 = PIrs.DepositingId 
--INNER JOIN MemberExtract ME ON PIM.Idtype2 = 'Existing'  AND PIM.IdValue2 = Me.medicareNo
--WHERE Pirs.ProcessedInd IS NULL 

UPDATE PatientIdsRegistryStage
SET ProcessedInd = 1
FROM PatientIdsRegistryStage PIRS INNER JOIN #ProcessedTable PT ON PIRS.DepositingId = PT.DepositingId AND PIRS.DepositingIdType = PT.DepositingIDType

INSERT INTO [dbo].[MemberExtract] (
		   [lastName] ,[midInit],[firstName]
		  ,[sex] ,[birthDate] ,[medicareNo]
		  ,[homeZip],[family_id],[source]
		  ,[SourceFeed]	)
OUTPUT INSERTED.SourceFeed, INSERTED.[medicareNo], 'New Id' INTO #ProcessedTable (DepositingIdType,DepositingId, DepositingAction)
SELECT 
		([LastName]) , ([MiddleName]),([FirstName])  , 
		[Gender] ,     [DOBDateTime]  , ([DepositingId]),
	    ([ZIP])  ,	NEWID(),PIRS.DepositingIdSourceType,
		PIRS.DepositingIdSourceFeed
FROM PatientIdsRegistryStage PIRS 
WHERE PIRS.processedInd IS NULL

INSERT INTO PatientIdsRegistry (Family_Id,IdType,IdValue,IdMetaType,IdFeed,IdSource,IdComments)
SELECT DISTINCT  Me.Family_id, PIRS.DepositingIdtype, PIRS.DepositingId,PIRS.DepositingIdMetaType, PIRS.DepositingIdSourceFeed, PIRS.DepositingIdSourceType,'New Utoken'
FROM PatientIdsRegistryStage PIRS  
INNER JOIN  MemberExtract me on me.medicareno = pirs.DepositingId
WHERE PIRS.processedInd IS NULL 

-- INSERT BACK PREFERRED IDS BLOCK
INSERT INTO PatientIdsRegistry (Family_Id,IDType,IdValue,IdMetaType,IdFeed,IdSource)
EXEC internalComposeCustomResultset @RequestSubject = 'DisplayFriendlyIds', @Param1 = ''

DELETE FROM PatientIdsRegistryStage --WHERE ProcessedInd = 1

UPDATE MemberExtract 
Set Holdback = 1 
FROM MemberExtract ME 
INNER JOIN (
SELECT family_id FROM MemberExtract 
GROUP BY family_id having count(*) > 1 ) X ON ME.family_id = x.family_id 

UPDATE MemberExtract 
Set Holdback = 1 
FROM MemberExtract ME 
INNER JOIN (
SELECT medicareNo FROM MemberExtract 
GROUP BY medicareNo having count(*) > 1 ) X ON ME.medicareNo = x.medicareNo 


GO
