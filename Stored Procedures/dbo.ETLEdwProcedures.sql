SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwProcedures] 
AS
BEGIN
SET NOCOUNT ON

-- TRIVIAL UPDATES FOR NULL
UPDATE [PatientProcedureProcessQueue] SET PerformedByClinician = '' WHERE PerformedByClinician IS NULL
UPDATE [PatientProcedureProcessQueue] SET ProcedureCodeModifier1 = '' WHERE ProcedureCodeModifier1 IS NULL
UPDATE [PatientProcedureProcessQueue] SET ProcedureCodeModifier2 = '' WHERE ProcedureCodeModifier2 IS NULL
UPDATE [PatientProcedureProcessQueue] SET ProcedureCodeModifier3 = '' WHERE ProcedureCodeModifier3 IS NULL
UPDATE [PatientProcedureProcessQueue] SET ProcedureCodeModifier4 = '' WHERE ProcedureCodeModifier4 IS NULL
UPDATE [PatientProcedureProcessQueue] SET EncounterId = 0 WHERE EncounterId IS NULL

-- SCRUB OUTPUT ROW TO PROCESS
;With CTE AS 
(
	SELECT 
		ScrubbedRowNum,
		ROW_NUMBER() 
		OVER ( PARTITION BY -- DEFINES UNIQUENESS OF ROW
				InterfaceSystemId,LbPatientID, Proceduredatetime,  ProcedureCodingSystemTypeId, ProcedureCodeId --, PerformedByClinician , OrderedByClinician,   EncounterId, 
				--ProcedureCodeModifier1, ProcedureCodeModifier2, ProcedureCodeModifier3, ProcedureCodeModifier4 
				ORDER BY 	-- DEFINES ORDER OF THE ROW IN UNIQUENESS
				--LEN(ISNULL(ProcedureComment,'')) + -- THE BIGGER THE PROCEDURECOMMENT THE BETTER IT IS 
				--LEN(ISNULL(ProcedureCodeModifier1,'')) + -- THE BIGGER THE ProcedureCodeModifier1 THE BETTER IT IS 
				--LEN(ISNULL(ProcedureCodeModifier2,'')) + -- THE BIGGER THE ProcedureCodeModifier2 THE BETTER IT IS 
				--LEN(ISNULL(ProcedureCodeModifier3,'')) + -- THE BIGGER THE ProcedureCodeModifier3 THE BETTER IT IS 
				--LEN(ISNULL(ProcedureCodeModifier4,''))   -- THE BIGGER THE ProcedureCodeModifier4 THE BETTER IT IS 
				InterfacePatientProcedureId
				DESC 
			) SNO
	FROM PatientProcedureProcessQueue
)
UPDATE CTE 
SET ScrubbedRowNum = SNO;

UPDATE  PatientProcedureProcessQueue SET ProcessedInd = 0 WHERE ScrubbedRowNum > 1

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientProcedureID INT, PatientID INT,ProcedureDateTime DateTime, 
						   PerformedByClinician VARCHAR(150),OrderedByClinician VARCHAR(150),SourceSystemId Int,
						   EncounterId BigInt,ExternalReferenceIdentifier Int)

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientProcedureID, PatientID, ProcedureDateTime,
					      PerformedByClinician, OrderedByClinician, SourceSystemId,
						  EncounterId, ExternalReferenceIdentifier)
SELECT
						  MergeAction,OrigPatientID,PatientProcedureID, PatientID, ProcedureDateTime,
						  PerformedByClinician, OrderedByClinician, SourceSystemId,
						  EncounterId, ExternalReferenceIdentifier
--INTO #ResultsTemp
FROM
(
MERGE [DBO].[PatientProcedure] AS target
USING (	
		SELECT DISTINCT LbPatientID As OrigPatientId, ISNULL(SS.SwitchInId,LbPatientID) as PatientID,InterfaceSystemId as SourceSystemID,ProcedureFunctionTypeID,ProcedureCodeModifier1,ProcedureCodeModifier2,ProcedureCodeModifier3,ProcedureCodeModifier4,
		ProcedureDateTime ProcedureDateTime,ProcedureComment,EncounterID,NULL AS PerformedByProviderID,PerformedByClinician,NULL AS OrderedByProviderID,OrderedByClinician,0 AS DeleteInd,
		TotalCharges,InternalChargeCode,ProcedureResult,EncounterIdentifier,
		GETDATE() AS CreateDateTime,GETDATE() AS ModifyDateTime,1 AS CreateLBUserId,1 AS ModifyLBUserId,
		-- NON TARGET RECORDS
		BINARY_CHECKSUM(ProcedureCodeId,ProcedureCodingSystemTypeId) AS ExternalReferenceIdentifier
		FROM [dbo].PatientProcedureProcessQueue PPQ 
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.LbPatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
		WHERE ScrubbedRowNum = 1 
	  ) AS source 
	  (OrigPatientId,PatientID,SourceSystemID,ProcedureFunctionTypeID,ProcedureCodeModifier1,ProcedureCodeModifier2,ProcedureCodeModifier3,ProcedureCodeModifier4
		,ProcedureDateTime,ProcedureComment,EncounterID,PerformedByProviderID,PerformedByClinician,OrderedByProviderID,OrderedByClinician,DeleteInd
		,TotalCharges,InternalChargeCode,ProcedureResult,EncounterIdentifier
		,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,ExternalReferenceIdentifier)
ON   (source.SourceSystemID = target.SourceSystemId AND target.PatientID = source.PatientID 
	  AND ISNULL(target.ProcedureDateTime,'2099-12-31') = ISNULL(source.ProcedureDateTime,'2099-12-31') AND target.ExternalReferenceIdentifier = source.ExternalReferenceIdentifier 
	  --AND ISNULL(target.PerformedByClinician,'') = ISNULL(source.PerformedByClinician,'') AND ISNULL(target.OrderedByClinician,'') = ISNULL(source.OrderedByClinician,'')  AND ISNULL(target.EncounterId,0) = ISNULL(source.EncounterId,0) 
     )
WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
	--PatientID	 = source.PatientID,
	SourceSystemID	 = source.SourceSystemID,
	ProcedureFunctionTypeID	 = source.ProcedureFunctionTypeID,
	ProcedureCodeModifier1	 = source.ProcedureCodeModifier1,
	ProcedureCodeModifier2	 = source.ProcedureCodeModifier2,
	ProcedureCodeModifier3	 = source.ProcedureCodeModifier3,
	ProcedureCodeModifier4	 = source.ProcedureCodeModifier4,
	ProcedureDateTime	 = source.ProcedureDateTime,
	ProcedureComment	 = source.ProcedureComment,
	EncounterID	 = source.EncounterID,
	PerformedByProviderID	 = source.PerformedByProviderID,
	PerformedByClinician	 = source.PerformedByClinician,
	OrderedByProviderID	 = source.OrderedByProviderID,
	OrderedByClinician	 = source.OrderedByClinician,
	DeleteInd	 = source.DeleteInd,
	TotalCharges	 = source.TotalCharges,
	InternalChargeCode	 = source.InternalChargeCode,
	ProcedureResult	 = source.ProcedureResult,
	EncounterIdentifier	 = source.EncounterIdentifier,
	--CreateDateTime	 = source.CreateDateTime,
	ModifyDateTime	 = source.ModifyDateTime,
	--CreateLBUserId	 = source.CreateLBUserId,
	ModifyLBUserId	 = source.ModifyLBUserId
WHEN NOT MATCHED THEN
INSERT (PatientID,SourceSystemID,ProcedureFunctionTypeID,ProcedureCodeModifier1,ProcedureCodeModifier2,ProcedureCodeModifier3,ProcedureCodeModifier4
,ProcedureDateTime,ProcedureComment,EncounterID,PerformedByProviderID,PerformedByClinician,OrderedByProviderID,OrderedByClinician,DeleteInd
,TotalCharges,InternalChargeCode,ProcedureResult,EncounterIdentifier
,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,ExternalReferenceIdentifier)
VALUES (source.PatientID,source.SourceSystemID,source.ProcedureFunctionTypeID,source.ProcedureCodeModifier1,source.ProcedureCodeModifier2,
		source.ProcedureCodeModifier3,source.ProcedureCodeModifier4,source.ProcedureDateTime,source.ProcedureComment,source.EncounterID,
		source.PerformedByProviderID,source.PerformedByClinician,source.OrderedByProviderID,source.OrderedByClinician,source.DeleteInd,
		source.TotalCharges,source.InternalChargeCode,source.ProcedureResult,source.EncounterIdentifier,
		source.CreateDateTime,source.ModifyDateTime,source.CreateLBUserId,source.ModifyLBUserId,source.ExternalReferenceIdentifier)
OUTPUT $ACTION MergeAction, source.OrigPatientId, ISNULL(INSERTED.PatientProcedureID,DELETED.PatientProcedureID) PatientProcedureID, source.PatientID,source.ProcedureDateTime,
			   source.PerformedByClinician, Source.OrderedByClinician , Source.SourceSystemID,
			   Source.EncounterId, source.ExternalReferenceIdentifier 
) x;

UPDATE PPPQ
SET PatientProcedureID = RT.PatientProcedureID
FROM PatientProcedureProcessQueue PPPQ
INNER JOIN #ResultsTemp RT  ON PPPQ.InterfaceSystemId = RT.SourceSystemId  AND PPPQ.LbPatientID = RT.OrigPatientId AND PPPQ.ProcedureDateTime =  RT.ProcedureDateTime 
			--AND ISNULL(PPPQ.PerformedByClinician,'') = ISNULL(RT.PerformedByClinician,'') AND ISNULL(PPPQ.EncounterId,0) = ISNULL(RT.EncounterId,0) AND ISNULL(PPPQ.OrderedByClinician,'') = ISNULL(RT.OrderedByClinician,'')
			AND BINARY_CHECKSUM(PPPQ.ProcedureCodeId,PPPQ.ProcedureCodingSystemTypeId) = RT.ExternalReferenceIdentifier 
			AND (RT.MergeAction = 'INSERT' OR RT.OrigPatientId = RT.PatientId)

IF OBJECT_ID('PatientProcedure_PICT') IS NOT NULL
BEGIN
	INSERT INTO PatientProcedure_PICT (PatientProcedureId,PatientId,SwitchDirectionId)
	SELECT RT.PatientProcedureID, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

--SELECT * FROM #ResultsTemp;
MERGE [DBO].[PatientProcedureProcedureCode] AS target
USING (	
		SELECT DISTINCT PatientProcedureId,ProcedureCodingSystemTypeId,ProcedureCodeId
		FROM [dbo].PatientProcedureProcessQueue PPQ WHERE PatientProcedureId IS NOT NULL 
	  ) AS source 
	  (PatientProcedureId,ProcedureCodingSystemTypeId,ProcedureCodeId)
ON (target.PatientProcedureId = source.PatientProcedureId AND Target.ProcedureCodeId = source.ProcedureCodeId AND Target.ProcedureCodingSystemTypeId = source.ProcedureCodingSystemTypeId )
--WHEN MATCHED THEN 
--    UPDATE SET 
--	ProcedureDescription	 = source.ProcedureDescription
WHEN NOT MATCHED THEN
INSERT (PatientProcedureId,ProcedureCodingSystemTypeId,ProcedureCodeId)
VALUES (source.PatientProcedureId,source.ProcedureCodingSystemTypeId,source.ProcedureCodeId);

-- END ZONE UPDATES AND TIMESTAMPING 
UPDATE  PatientProcedureProcessQueue SET ProcessedInd = 1 WHERE ScrubbedRowNum =  1 

INSERT INTO PatientFeedTimestamps (PatientId,FeedType,FeedSubType,FeedTimestamp)
SELECT DISTINCT LbPatientID, NULL, 'Procedures',GetDate()
FROM [PatientProcedureProcessQueue]
WHERE ProcessedInd = 1 

UPDATE ProcedureCode 
SET ProcedureDescription = CPT4.description, ProcedureDescriptionAlternate = CPT4.description
FROM ProcedureCode PC INNER JOIN CPT4Mstr CPT4 ON PC.ProcedureCode = CPT4.cpt4_code_id
WHERE PC.ProcedureDescription LIKE 'DUMMY%' OR PC.ProcedureDescription = ''

UPDATE A 
SET HoldingReleaseInd = 1 
FROM [dbo].[PatientProcedureProcessQueue_HoldingBay] A 
INNER JOIN PatientProcedureProcessQueue B ON A.[InterfacePatientProcedureId] = B.[InterfacePatientProcedureId]
WHERE B.ProcessedInd = 1 

--INSERT INTO [dbo].[PatientProcedureProcessQueue_HoldingBay]
--([InterfacePatientProcedureId],[InterfacePatientID],[LbPatientID]
--,[InterfaceSystemId],[ProcedureFunctionTypeID],[ProcedureCodeModifier1]
--,[ProcedureCodeModifier2],[ProcedureCodeModifier3],[ProcedureCodeModifier4]
--,[ProcedureDateTime],[ProcedureComment],[PerformedByClinician]
--,[OrderedByClinician],[IPP_CreateDateTime],[InterfacePatientProcedureProcedureCodeId]
--,[ProcedureCode],[ProcedureCodeId],[ProcedureCodingSystemName]
--,[ProcedureCodingSystemTypeId],[ProcedureDescription],[IPPPC_CreateDateTime]
--,[PatientProcedureID],[EncounterId],[InterfacePatientProcedureId_HoldingOverride]
--,[InterfacePatientID_HoldingOverride],[LbPatientID_HoldingOverride],[InterfaceSystemId_HoldingOverride]
--,[ProcedureFunctionTypeID_HoldingOverride],[ProcedureCodeModifier1_HoldingOverride],[ProcedureCodeModifier2_HoldingOverride]
--,[ProcedureCodeModifier3_HoldingOverride],[ProcedureCodeModifier4_HoldingOverride],[ProcedureDateTime_HoldingOverride]
--,[ProcedureComment_HoldingOverride],[PerformedByClinician_HoldingOverride],[OrderedByClinician_HoldingOverride]
--,[IPP_CreateDateTime_HoldingOverride],[InterfacePatientProcedureProcedureCodeId_HoldingOverride],[ProcedureCode_HoldingOverride]
--,[ProcedureCodeId_HoldingOverride],[ProcedureCodingSystemName_HoldingOverride],[ProcedureCodingSystemTypeId_HoldingOverride]
--,[ProcedureDescription_HoldingOverride],[IPPPC_CreateDateTime_HoldingOverride],[PatientProcedureID_HoldingOverride]
--,[EncounterId_HoldingOverride],[HoldingStartDate],[HoldingStartReason]
--)
--SELECT
--A.[InterfacePatientProcedureId],A.[InterfacePatientID],A.[LbPatientID]
--,A.[InterfaceSystemId],A.[ProcedureFunctionTypeID],A.[ProcedureCodeModifier1]
--,A.[ProcedureCodeModifier2],A.[ProcedureCodeModifier3],A.[ProcedureCodeModifier4]
--,A.[ProcedureDateTime],A.[ProcedureComment],A.[PerformedByClinician]
--,A.[OrderedByClinician],A.[IPP_CreateDateTime],A.[InterfacePatientProcedureProcedureCodeId]
--,A.[ProcedureCode],A.[ProcedureCodeId],A.[ProcedureCodingSystemName]
--,A.[ProcedureCodingSystemTypeId],A.[ProcedureDescription],A.[IPPPC_CreateDateTime]
--,A.[PatientProcedureID],A.[EncounterId],A.[InterfacePatientProcedureId]
--,A.[InterfacePatientID],A.[LbPatientID],A.[InterfaceSystemId]
--,A.[ProcedureFunctionTypeID],A.[ProcedureCodeModifier1],A.[ProcedureCodeModifier2]
--,A.[ProcedureCodeModifier3],A.[ProcedureCodeModifier4],A.[ProcedureDateTime]
--,A.[ProcedureComment],A.[PerformedByClinician],A.[OrderedByClinician]
--,A.[IPP_CreateDateTime],A.[InterfacePatientProcedureProcedureCodeId],A.[ProcedureCode]
--,A.[ProcedureCodeId],A.[ProcedureCodingSystemName],A.[ProcedureCodingSystemTypeId]
--,A.[ProcedureDescription],A.[IPPPC_CreateDateTime],A.[PatientProcedureID]
--,A.[EncounterId],GetDate(),'Duplicacy'
--FROM PatientProcedureProcessQueue A
--LEFT JOIN [dbo].[PatientProcedureProcessQueue_HoldingBay] B ON A.[InterfacePatientProcedureId] = B.[InterfacePatientProcedureId]
--WHERE ISNULL(A.ProcessedInd,0) <> 1 AND B.[InterfacePatientProcedureId] IS NULL 

END
GO
