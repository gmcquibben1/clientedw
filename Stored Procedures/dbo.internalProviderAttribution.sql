SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[internalProviderAttribution] 
AS
-- STEP 0 :- CLEAN SLATE
UPDATE [MemberExtract] SET ProviderID = NULL, ModifyDateTime = GETUTCDATE() 

-- STEP 1 :- ATTRIBUTE FROM ANY CUSTOM ATTRIBUTION LOGIC
CREATE TABLE #CustomAttribution (MedicareNO VARCHAR(100),ProviderNPI VARCHAR(100))
INSERT INTO #CustomAttribution (MedicareNO,ProviderNPI)
EXEC internalComposeCustomResultset @RequestSubject = 'AdhocAttribution', @Param1 = 2

UPDATE MemberExtract 
SET ProviderId = CA.ProviderNPI, ModifyDateTime = GETUTCDATE() 
FROM MemberExtract ME INNER JOIN #CustomAttribution CA ON ME.medicareNo = CA.MedicareNO WHERE ME.ProviderId IS NULL 

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId], ModifyDateTime = GETUTCDATE() 
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.family_id = pir.Family_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.Head_Id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId], ModifyDateTime = GETUTCDATE() 
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.Family_Id = pir.Head_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.family_id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

-- STEP 2 :- ATTRIBUTE FROM MEMBER ROSTER ARCHIVES DIRECT
;WITH CTE0 AS (
	SELECT HICNO , FileInputName, FileTimeLine, ROW_NUMBER() OVER(PARTITION BY HICNO ORDER BY fileTimeline DESC  ) SNO 
	FROM [MemberRosterArchives_ProviderTab]
)
SELECT MRA.HICNO, [Individual NPI] ProviderNPI, ROW_NUMBER() OVER(PARTITION BY MRA.HICNO ORDER BY MRA.fileTimeline ) CLM_SUM
INTO #MRAProviders
FROM [MemberRosterArchives_ProviderTab] MRA
INNER JOIN  [ProviderExtract] Prov on MRA.[Individual NPI] = Prov.[ProviderNPI]  
INNER JOIN CTE0 ON MRA.HICNO = CTE0.HICNO AND MRA.FileInputName = CTE0.FileInputName AND CTE0.SNO = 1

SELECT BENE_HIC_NUM, NPI, SUM(CLM_SUM) AS CLM_SUM INTO #T1
FROM (
	SELECT 
	   BENE_HIC_NUM ,RNDRG_PRVDR_NPI_NUM AS NPI ,SUM(CLM_LINE_CVRD_PD_AMT) AS CLM_SUM              
	FROM CCLF_5_PartB_Physicians PARTB 
	INNER JOIN  #MRAProviders Prov on PARTB.RNDRG_PRVDR_NPI_NUM = Prov.[ProviderNPI] AND PartB.BENE_HIC_NUM = Prov.HICNO
	GROUP BY BENE_HIC_NUM, RNDRG_PRVDR_NPI_NUM
	UNION
	SELECT 
	   PARTA.BENE_HIC_NUM  ,OPRTG_PRVDR_NPI_NUM AS NPI,SUM(CLM_LINE_CVRD_PD_AMT) AS CLM_SUM             
	FROM CCLF_1_PartA_Header PARTA 
		 INNER JOIN [CCLF_2_PartA_RCDetail] RCD ON PARTA.CUR_CLM_UNIQ_ID = RCD.CUR_CLM_UNIQ_ID
		 INNER JOIN  #MRAProviders Prov on PARTA.OPRTG_PRVDR_NPI_NUM = Prov.[ProviderNPI] AND PARTA.BENE_HIC_NUM = Prov.HICNO
	GROUP BY PARTA.BENE_HIC_NUM, OPRTG_PRVDR_NPI_NUM
) X GROUP BY BENE_HIC_NUM, NPI

INSERT INTO #T1 (BENE_HIC_NUM,NPI,CLM_SUM)
SELECT  HICNO, ProviderNPI, A.CLM_SUM 
FROM #MRAProviders A LEFT JOIN #T1 B ON A.HICNO = B.BENE_HIC_NUM WHERE B.BENE_HIC_NUM IS NULL 

UPDATE [MemberExtract]
SET [ProviderId] =  Prov.[ProviderNPI], ModifyDateTime = GETUTCDATE()  -- NPI SELECT * 
FROM [MemberExtract]
INNER JOIN (
	SELECT BENE_HIC_NUM, NPI FROM
	(
	SELECT 
	BENE_HIC_NUM, NPI, ROW_NUMBER() OVER (PARTITION  BY BENE_HIC_NUM ORDER BY CLM_SUM DESC) AS Providersno
	FROM  #T1
	) X WHERE PRovidersno = 1 
) Y  ON [MemberExtract].MedicareNo = Y.BENE_HIC_NUM
INNER JOIN  [dbo].[ProviderExtract] Prov ON Y.NPI = Prov.[ProviderNPI]
WHERE [MemberExtract].ProviderId IS NULL 

-- STEP 3 :- ATTRIBUTE USING INTERFACE PROVIDERS IF ANY 

UPDATE [MemberExtract] -- 91051
SET [ProviderId] =  Y.ProviderNPI, ModifyDateTime = GETUTCDATE()  -- NPI
FROM [MemberExtract] ME
INNER JOIN (
		SELECT IPM.UToken, IPA.ProviderNPI,
		ROW_NUMBER() OVER(Partition by UToken ORDER BY IPA.CreateDateTime DESC) SNO 
		FROM vwPortal_InterfacePatientAttribution  IPA
		INNER JOIN vwPortal_InterfacePatientMatched IPM ON IPA.PatientIdentifier = IPM.PatientIdentifier
		WHERE IPA.ProviderNPI <> '' 
) Y  ON ME.medicareNo = Y.UToken  WHERE ME.ProviderID IS  NULL AND SNO = 1  -- UNCOMMENT TO NOT DO SO 

--UPDATE [MemberExtract] -- 91051
--SET [ProviderId] =  Y.ProviderNPI -- NPI
--FROM [MemberExtract] ME
--INNER JOIN (
--		SELECT PIR.family_id, IPA.ProviderNPI,
--		ROW_NUMBER() OVER(Partition by UToken ORDER BY IPA.CreateDateTime DESC) SNO 
--		FROM vwPortal_InterfacePatientAttribution  IPA
--		INNER JOIN vwPortal_InterfacePatientMatched IPM ON IPA.PatientIdentifier = IPM.PatientIdentifier
--		INNER JOIN PatientIdsRegistry PIR ON IPM.LbPatientId = PIR.IdValue AND PIR.IDType = 'LbPatientId'
--		WHERE IPA.ProviderNPI <> '' 
--) Y  ON ME.family_id = Y.family_id  WHERE ME.ProviderID IS  NULL AND SNO = 1-- UNCOMMENT TO NOT DO SO 

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId], ModifyDateTime = GETUTCDATE() 
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.family_id = pir.Family_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.Head_Id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId], ModifyDateTime = GETUTCDATE() 
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.Family_Id = pir.Head_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.family_id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

-- STEP 4 :- ATTRIBUTE USING LB CUSTOM LOGIC 
SELECT BENE_HIC_NUM, NPI, SUM(CLM_SUM) AS CLM_SUM INTO #T2
FROM (
	SELECT 
	   BENE_HIC_NUM ,RNDRG_PRVDR_NPI_NUM AS NPI ,SUM(CLM_LINE_CVRD_PD_AMT) AS CLM_SUM              
	FROM CCLF_5_PartB_Physicians PARTB 
		 INNER JOIN  [ProviderExtract] Prov on PARTB.RNDRG_PRVDR_NPI_NUM = Prov.[ProviderNPI]
		 INNER JOIN  RefTable_Wellness W ON PARTB.CLM_LINE_HCPCS_CD = W.code AND W.field = 'hcpc'
	--WHERE DATEDIFF(MONTH, CLM_FROM_DT, '12-31-2013') <= 12
	GROUP BY BENE_HIC_NUM, RNDRG_PRVDR_NPI_NUM
	UNION
	SELECT 
	   PARTA.BENE_HIC_NUM  ,OPRTG_PRVDR_NPI_NUM AS NPI,SUM(CLM_LINE_CVRD_PD_AMT) AS CLM_SUM             
	FROM CCLF_1_PartA_Header PARTA 
		 INNER JOIN [CCLF_2_PartA_RCDetail] RCD ON PARTA.CUR_CLM_UNIQ_ID = RCD.CUR_CLM_UNIQ_ID
		 INNER JOIN  [ProviderExtract] Prov on PARTA.OPRTG_PRVDR_NPI_NUM = Prov.[ProviderNPI]
	WHERE 
		 CLM_LINE_REV_CTR_CD IN (SELECT code FROM RefTable_Wellness WHERE field = 'revcode') 
		 OR CLM_LINE_HCPCS_CD IN (SELECT code FROM RefTable_Wellness WHERE field = 'hcpc') 
	GROUP BY PARTA.BENE_HIC_NUM, OPRTG_PRVDR_NPI_NUM
) X GROUP BY BENE_HIC_NUM, NPI

UPDATE [MemberExtract]
SET [ProviderId] =  Prov.[ProviderNPI], ModifyDateTime = GETUTCDATE()  -- NPI SELECT * 
FROM [MemberExtract]
INNER JOIN (
	SELECT BENE_HIC_NUM, NPI FROM
	(
	SELECT 
	BENE_HIC_NUM, NPI, ROW_NUMBER() OVER (PARTITION  BY BENE_HIC_NUM ORDER BY CLM_SUM DESC) AS Providersno
	FROM  #T2
	) X WHERE PRovidersno = 1 
) Y  ON [MemberExtract].MedicareNo = Y.BENE_HIC_NUM
INNER JOIN  [dbo].[ProviderExtract] Prov ON Y.NPI = Prov.[ProviderNPI]
WHERE [MemberExtract].ProviderId IS NULL 

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId], ModifyDateTime = GETUTCDATE() 
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.family_id = pir.Family_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.Head_Id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId], ModifyDateTime = GETUTCDATE() 
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.Family_Id = pir.Head_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.family_id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

-- STEP 5 :- UPDATE PROVIDER NAMES/ASSOCIATIONS ETC ELSEWHERE 

---- PORTAL SIDE TRANSFERANCE 
UPDATE vwPortal_PatientProvider  SET ProviderRoleTypeId = 0, ModifyDateTime = GETUTCDATE()  WHERE ProviderRoleTypeId IS NULL 

UPDATE vwPortal_PatientProvider  
SET DeleteInd = 0 , ModifyDateTime = GETUTCDATE() -- IF MATCHED RENEW THE DATE
FROM MemberExtract ME 
INNER JOIN PatientIdsRegistry PIR ON ME.Family_Id = PIR.Family_Id and PIR.IdType = 'LbPatientId'
INNER JOIN vwPortal_Provider PROV on ME.ProviderId = Prov.NationalProviderIdentifier 
INNER JOIN vwPortal_PatientProvider PP ON PP.PatientID =  PIR.IdValue AND PP.ProviderID = prov.ProviderID

INSERT INTO vwPortal_PatientProvider
		(PatientID,ProviderID,SourceSystemID
		,ExternalReferenceIdentifier
		,OtherReferenceIdentifier
		,ProviderRoleTypeId
		,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId)
SELECT  
		PIR.IdValue	,prov.ProviderID,2
		,me.medicareNo --MedicareNo (for patient)
		,me.ProviderId --ExternalProvider id (for provider)
		,(SELECT ProviderRoleTypeId FROM vwPortal_ProviderRoleType  WHERE Name = 'unknown')
		,0,GetUtcDate(),GetUtcDate(),1 ,1
FROM MemberExtract ME 
INNER JOIN PatientIdsRegistry PIR ON ME.Family_Id = PIR.Family_Id and PIR.IdType = 'LbPatientId'
INNER JOIN vwPortal_Provider PROV on ME.ProviderId = Prov.NationalProviderIdentifier 
LEFT JOIN vwPortal_PatientProvider PP ON PP.PatientID =  PIR.IdValue AND PP.ProviderID = prov.ProviderID
WHERE PP.PatientProviderID IS  NULL

-- insert any relationships that came in via the interface for primary care provider
INSERT INTO vwPortal_PatientProvider
		(PatientID,ProviderID,SourceSystemID
		,ExternalReferenceIdentifier
		,OtherReferenceIdentifier
		,ProviderRoleTypeId
		,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId)
SELECT  
		idRef.LbPatientId, prov.ProviderID, idRef.SourceSystemId
		,ip.PatientIdentifier
		,PROV.NationalProviderIdentifier --ExternalProvider id (for provider)
		,(SELECT ProviderRoleTypeId FROM vwPortal_ProviderRoleType  WHERE Name = 'PCP')
		,0,GetUtcDate(),GetUtcDate(),1 ,1
FROM vwPortal_InterfacePatient ip 
INNER JOIN PatientIdReference idRef ON idRef.ExternalId = ip.PatientIdentifier AND idRef.IdTypeDesc = 'InterfacePatientIdentifier'
INNER JOIN vwPortal_Provider PROV on ip.PrimaryCareProviderNPI = PROV.NationalProviderIdentifier 
WHERE NOT EXISTS (SELECT 1 FROM vwPortal_PatientProvider pp2 WHERE pp2.PatientId = idRef.LbPatientId AND pp2.ProviderId = PROV.ProviderId)

--update the provider role type for PCP providers found in the interface demographics
UPDATE pp
SET pp.ProviderRoleTypeId = (SELECT ProviderRoleTypeId FROM vwPortal_ProviderRoleType WHERE Name = 'PCP'), pp.ModifyDateTime = GETUTCDATE()
FROM vwPortal_PatientProvider pp
INNER JOIN
(SELECT idRef.LbPatientId AS PatientId, prov.ProviderId AS ProviderId, ROW_NUMBER() OVER (PARTITION BY idRef.LbPatientId ORDER BY ip.CreateDateTime DESC) row
FROM vwPortal_InterfacePatient ip
	INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ip.InterfaceSystemId 
	INNER JOIN PatientIdReference idRef ON idRef.ExternalId = ip.PatientIdentifier AND idRef.SourceSystemId = ifs.SourceSystemId AND idRef.IdTypeDesc = 'InterfacePatientIdentifier'
	INNER JOIN vwPortal_Provider prov ON prov.NationalProviderIdentifier = ip.PrimaryCareProviderNPI) AS npi ON npi.PatientId = pp.PatientId AND npi.ProviderId = pp.ProviderId AND npi.row = 1

;WITH CTE AS (
SELECT PatientId, ProviderId, PatientProviderId, DeleteInd, ROW_NUMBER() OVER (PARTITION BY PatientID ORDER BY ModifyDateTime Desc) SNO
FROM vwPortal_PatientProvider )
UPDATE CTE SET DeleteInd = 1 WHERE SNO > 1 AND DeleteInd = 0 

-- STEP 7 :- ATTRIBUTE BASED ON PCP IF NOT ALREADY ATTRIBUTED
UPDATE ME 
SET ME.ProviderId = prov.NationalProviderIdentifier, ME.ModifyDateTime = GETUTCDATE() 
FROM MemberExtract ME 
	INNER JOIN PatientIdReference idRef ON idRef.ExternalId = ME.medicareNo
	INNER JOIN vwPortal_PatientProvider patProv ON patProv.PatientId = idRef.LbPatientId AND patProv.ProviderRoleTypeId = 1 AND patProv.DeleteInd <> 1
	INNER JOIN vwPortal_Provider prov ON prov.ProviderId = patProv.ProviderId AND prov.DeleteInd <> 1
WHERE ME.ProviderId IS NULL 

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId], ModifyDateTime = GETUTCDATE() 
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.family_id = pir.Family_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.Head_Id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

UPDATE Me -- MATCHED PATIENTS IN POOL IF NOT GOT A PROVIDER
SET [ProviderId] = Me2.[ProviderId], ModifyDateTime = GETUTCDATE() 
From MemberExtract Me 
INNER JOIN PatientIdsConflictsXReference PIR ON Me.Family_Id = pir.Head_Id AND pir.Head_Id <> pir.Family_Id
INNER JOIN MemberExtract Me2 ON PIR.family_id = ME2.Family_Id 
WHERE  ISNULL(Me.[ProviderId],'9999999999') = '9999999999' AND ISNULL(Me2.[ProviderId],'9999999999') <> '9999999999'

-- STEP 8 :- MARK REMAINING PATIENTS AS UNATTRIBUTED

UPDATE [MemberExtract] -- 91051
SET [ProviderId] =  '9999999999', ModifyDateTime = GETUTCDATE()  -- NPI
WHERE [ProviderId] IS NULL 

GO
