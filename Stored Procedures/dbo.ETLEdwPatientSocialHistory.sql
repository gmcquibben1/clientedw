SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ETLEdwPatientSocialHistory] 
AS
BEGIN
SET NOCOUNT ON

-- TRUNCATE AND RELOAD 
TRUNCATE TABLE PatientSocialHistory

CREATE TABLE #ResultsTemp (MergeAction Varchar(20),OrigPatientID INT,PatientSocialHistoryId INT, PatientID INT)	

INSERT INTO #ResultsTemp (MergeAction,OrigPatientID,PatientSocialHistoryId,PatientID)
SELECT
						 MergeAction,OrigPatientID,PatientSocialHistoryId,PatientID
FROM 
(	
MERGE [DBO].PatientSocialHistory AS target
USING (	
		SELECT 	PPQ.PatientID As OrigPatientId, ISNULL(SS.SwitchInId,PPQ.PatientID) as PatientID  , [SourceSystemId] , [SmokingStatus] , 	[MaritalStatus] , [EmploymentStatus] ,
			[TransportationInd] , [LivingArrangementCode] , 	[AlcoholUseInd], [DrugUseInd],	[CaffeineUseInd] , [SexuallyActiveInd],
			[ExerciseHoursPerWeek] , PPQ.[CreateDateTime] 
		FROM [dbo].[PatientSocialHistoryProcessQueue] PPQ
		LEFT JOIN PatientIdsConflictsSwitchSets SS ON PPQ.PatientId = SS.SwitchOutId AND SS.IdType = 'LbPatientId' AND SS.SwitchDirectionId = 0 AND SS.ActiveSet = 0
	  ) AS source 
	  ([OrigPatientId],[PatientID] , [SourceSystemId] , [SmokingStatus] , 	[MaritalStatus] , [EmploymentStatus] ,
	   [TransportationInd] , [LivingArrangementCode] , 	[AlcoholUseInd], [DrugUseInd],	[CaffeineUseInd] , [SexuallyActiveInd],
	   [ExerciseHoursPerWeek] , [CreateDateTime] )
ON ( target.PatientID = source.PatientID )
--WHEN MATCHED AND source.OrigPatientId = source.PatientId THEN UPDATE SET -- OFFERING UPDATE SERVICES FOR NON SWITCH SET ROWS ONLY
--	  --[PatientID]= source.[PatientID] ,
WHEN NOT MATCHED THEN
INSERT ([PatientID] , [SourceSystemId] , [SmokingStatus] , 	[MaritalStatus] , [EmploymentStatus] ,
	   [TransportationInd] , [LivingArrangementCode] , 	[AlcoholUseInd], [DrugUseInd],	[CaffeineUseInd] , [SexuallyActiveInd],
	   [ExerciseHoursPerWeek] , [CreateDateTime]
	   )
VALUES (source.[PatientID],source.[SourceSystemId],source.[SmokingStatus],source.[MaritalStatus],source.[EmploymentStatus],
	   source.[TransportationInd],source.[LivingArrangementCode],source.[AlcoholUseInd],source.[DrugUseInd],source.[CaffeineUseInd],source.[SexuallyActiveInd],
	   source.[ExerciseHoursPerWeek],source.[CreateDateTime])
OUTPUT $ACTION MergeAction, source.OrigPatientId,ISNULL(DELETED.PatientSocialHistoryId,INSERTED.PatientSocialHistoryId) PatientSocialHistoryId, source.PatientID
) x ;

IF OBJECT_ID('PatientSocialHistory_PICT') IS NOT NULL
BEGIN
	TRUNCATE TABLE PatientSocialHistory_PICT
	INSERT INTO PatientSocialHistory_PICT(PatientSocialHistoryId,PatientId,SwitchDirectionId)
	SELECT RT.PatientSocialHistoryId, OrigPatientID, 0 
	FROM #ResultsTemp RT WHERE RT.MergeAction = 'INSERT' AND OrigPatientID <> PatientID;
END

END

GO
