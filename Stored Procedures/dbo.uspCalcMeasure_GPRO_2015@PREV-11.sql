SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@PREV-11] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1
AS
BEGIN
--declare @dataPreference smallint = 1
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	---------------------------------------------------------------------------------------
	-- initialization, setting variables
	---------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];
	DECLARE  @yearend date, @yearBegin date, @sixMonths date;

	SET @yearBegin = '01/01/2015';
	SET @yearend   = '01/01/2016';

	-- determine the clinical-only or clinical-and-claims data sources
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END
    
	--------------------------------------------------------------------------------------------
	--Initial Patient pooling, for patients that have ranking for the BP measure. 
	--------------------------------------------------------------------------------------------
	select r.LBPatientID,
		r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,@yearBegin) as Age,
		r.Birthdate,
		GM.GproMeasureTypeID,
		r.GenderCode,
		GPM.PcBloodPressureConfirmed,
		GPM.PcBloodPressureScreen, 
		GPM.PcBloodPressureNormal, 
		GPM.PcBloodPressureFollowUp
	INTO   #RankedPatients --Drop Table #RankedPatients
	FROM GproPatientRanking r
	JOIN GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
	JOIN GproMeasureType GM	ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
	JOIN GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE Name = 'PREV-11'
		AND (GPM.PcBloodPressureConfirmed IS NULL OR GPM.[PcBloodPressureScreen] IS NULL)
   
	--------------------------------------------------------------------------------------------------------    
    -- Denominator Exclusions: Age, or evidence of ESRD/Dialysis/Renal Transplant during or before MY.
    --                         Evidence of Pregnancy during the MY.
	--------------------------------------------------------------------------------------------------------   		    
    --Age Check
    UPDATE rp
    SET    rp.[PcBloodPressureConfirmed] = '19'
    FROM   #RankedPatients rp
    WHERE (Age < 18)
                                                  
    -- Active Existing conditions from before the Measurement period.
    UPDATE RP
    SET    RP.[PcBloodPressureConfirmed] = '17'
    FROM   #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON RP.LbPatientId = pd.PatientId
			AND pd.DiagnosisDateTime < @yearEnd
			AND  (ActiveInd <> 0  AND  ISNULL(pd.DiagnosisResolvedDate,@YearEnd) > @yearBegin )
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = 'PREV'
			AND gec.[VariableName] = 'HTN_DX_CODE'
		INNER JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PD.[SourceSystemID]
      
    --Updating the patients that are not excluded and in the appropriate age range to confirmed
    UPDATE rp
    SET    rp.[PcBloodPressureConfirmed] = '2'
    FROM   #RankedPatients rp
    WHERE  [PcBloodPressureConfirmed] IS NULL
 
              
	--------------------------------------------------------------------------------------------------------          
   --NUMERATOR 
   --Patients who had a BP test in their last visit. The value check for Systolic/Diastolic is not necessary.
	--------------------------------------------------------------------------------------------------------      
                
    -- Medical or Patient Reason exception check.
	UPDATE RP
	SET   RP.[PcBloodPressureScreen] = (CASE WHEN [VariableName] = 'MEDICAL_REASON' THEN '4' 
					                          WHEN [VariableName] = 'PATIENT_REASON' THEN '5'
				                       END)
	FROM #RankedPatients RP
		INNER JOIN vwPatientDiagnosis pd ON RP.LbPatientId = pd.PatientId
			AND pd.DiagnosisDateTime < @yearEnd
			AND  (ActiveInd <> 0  AND  ISNULL(pd.DiagnosisResolvedDate,@YearEnd) > @yearBegin )
		INNER JOIN GproEvaluationCode gec ON (gec.Code = pd.DiagnosisCode OR gec.Code = pd.DiagnosisCodeRaw OR gec.Code = pd.DiagnosisCodeDisplay)
			AND gec.[ModuleType]   = 'PREV'
			AND gec.[VariableName] IN ('MEDICAL_REASON','PATIENT_REASON')
		INNER JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PD.[SourceSystemID]
	WHERE RP.[PcBloodPressureScreen] is NULL
        

	IF OBJECT_ID( 'tempdb..#BPs' ) IS NOT NULL 
		DROP TABLE #BPs;
  
	--SELECT
	--	RP.[LBPatientID],
	--	ROW_NUMBER () OVER (PARTITION BY RP.[LBPatientID] ORDER BY Cast( pv.[ServiceDateTime] AS DATE ) desc, Cast( pv.[BloodPressureSystolic] AS VARCHAR(15)) asc, Cast( pv.[BloodPressureDiastolic] AS VARCHAR(15) )asc) LatestRec,-- added by kp 11-3-2015
	--	CAST( pv.[BloodPressureSystolic]  AS VARCHAR(15))    AS [Sys],
	--	CAST( pv.[BloodPressureDiastolic] AS VARCHAR(15) )   AS [Dias],
	--	CAST( pv.[ServiceDateTime] AS DATE )  AS [ReadingDate]
	--INTO #BPs --drop table #BPs
	--FROM #RankedPatients RP
	--	JOIN [dbo].[PatientVitals] pv ON pv.[PatientID] = RP.[LbPatientID]
	--		AND pv.[ServiceDateTime] >= @yearBegin 
	--		AND pv.[ServiceDateTime] <  @YearEnd 
	--	JOIN @SourceIdTable SS ON  SS.[SourceSystemId] = PV.[SourceSystemID]    --Vitals
	--WHERE NullIf( pv.[BloodPressureSystolic] , 0 )  IS NOT NULL 
	--	AND NullIf( pv.[BloodPressureDiastolic], 0 )  IS NOT NULL
	--	AND [PcBloodPressureConfirmed] = '2'
	--	AND RP.[PcBloodPressureScreen] IS NULL
	--GROUP BY
	--	RP.[LBPatientID],
	--	pv.[ServiceDateTime], 
	--	pv.[BloodPressureSystolic], 
	--	pv.[BloodPressureDiastolic]
  
	UPDATE rp
	SET	rp.PcBloodPressureScreen = '2',
		rp.PcBloodPressureNormal = '1'
	FROM #RankedPatients rp
		INNER JOIN (SELECT PatientId, ServiceDateTime, BloodPressureSystolic, BloodPressureDiastolic, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ServiceDateTime DESC) r FROM PatientVitals
					WHERE BloodPressureSystolic IS NOT NULL AND BloodPressureSystolic > 0 AND BloodPressureSystolic < 250
						AND BloodPressureDiastolic IS NOT NULL AND BloodPressureDiastolic > 0 AND BloodPressureDiastolic < 250) AS pv ON pv.PatientId = rp.LbPatientId 
			AND pv.r = 1
			AND pv.ServiceDateTime > @yearBegin
			AND pv.serviceDateTime < @yearEnd
			AND pv.BloodPressureSystolic < 120
			AND pv.BloodPressureDiastolic < 80

	---- Updating Patients who's blood pressure was within the control values.       
	--UPDATE RP
	--SET RP.PcBloodPressureNormal    = '2',
	--	RP.PcBloodPressureScreen    = '2'
	--FROM #RankedPatients RP
	--	JOIN #BPs BP ON RP.LBPATIENTID = BP.LBPATIENTID
	--WHERE LatestRec = 1
	--	AND Sys < 120 AND Dias < 80
	--	AND PcBloodPressureConfirmed = '2'
              
	--Updating PCBlood

	---Patients with a BP outside of the normal range, checking to see if a follow-up was performed
	--'intervention_code', 'Diagnostic_Study', 'Laboratory_Test'    
  
	IF OBJECT_ID( 'tempdb..#HighBP' ) IS NOT NULL 
		DROP TABLE #HighBP;

	SELECT
		rp.*,
		pv.BloodPressureSystolic AS Sys,
		pv.BloodPressureDiastolic AS Dias,
		pv.ServiceDateTime AS ReadingDate
	INTO #HighBP
	FROM #RankedPatients rp
		INNER JOIN (SELECT PatientId, ServiceDateTime, BloodPressureSystolic, BloodPressureDiastolic, ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ServiceDateTime DESC) r FROM PatientVitals
					WHERE BloodPressureSystolic IS NOT NULL AND BloodPressureSystolic > 0 AND BloodPressureSystolic < 250
						AND BloodPressureDiastolic IS NOT NULL AND BloodPressureDiastolic > 0 AND BloodPressureDiastolic < 250) AS pv ON pv.PatientId = rp.LbPatientId 
			AND pv.r = 1
			AND pv.ServiceDateTime > @yearBegin
			AND pv.serviceDateTime < @yearEnd
			AND pv.BloodPressureSystolic >= 120
			AND pv.BloodPressureDiastolic >= 80

	UPDATE rp
	SET rp.PcBloodPressureScreen = '2',
		rp.PcBloodPressureNormal = '0'
	FROM #RankedPatients rp
		INNER JOIN #HighBP hbp ON hbp.LbPatientId = rp.LbPatientId
	WHERE rp.PcBloodPressureNormal IS NULL

	--SELECT RP.*,
	--		BP.Sys, 
	--		BP.Dias, 
	--		BP.ReadingDate
	--INTO #HighBP
	--FROM #RankedPatients RP
	--JOIN #BPs BP
	--	ON RP.LBPATIENTID = BP.LBPATIENTID
	--WHERE LatestRec = 1
	--	AND   Sys <= 120 AND Dias <= 80
	--	AND   PcBloodPressureConfirmed = '2'
  
  
	IF OBJECT_ID( 'tempdb..#FollowUP' ) IS NOT NULL 
		DROP TABLE #FollowUP;
	--FollowUp 
  
	SELECT DISTINCT HBP.*
	INTO  #FollowUP
	FROM  #HighBP HBP
		JOIN  [dbo].[PatientDiagnosis] pd ON  pd.[PatientID] = HBP.[LbPatientID]
			AND pd.[DiagnosisDateTime] >= HBP.ReadingDate
		JOIN [dbo].[PatientDiagnosisDiagnosisCode] pddc ON pddc.[PatientDiagnosisId] = pd.[PatientDiagnosisId]
		JOIN [dbo].[DiagnosisCode] dc ON  dc.[DiagnosisCodeID] = pddc.[DiagnosisCodeID]
		JOIN [dbo].[GproEvaluationCode] GEC ON  (gec.Code = dc.DiagnosisCode OR gec.Code = dc.DiagnosisCodeRaw OR gec.Code = dc.DiagnosisCodeDisplay)
			AND GEC.[VariableName] IN ( 'INTERVENTION_CODE', 'Diagnostic_Study', 'Laboratory_Test' )
			AND GEC.[ModuleType] = 'PREV' 
			AND GEC.[ModuleIndicatorGPRO] = '11'
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PD.[SourceSystemID]    --Vitals 
                

	-- BP Follow Up by Procedure Data
	INSERT INTO #FollowUP
	SELECT DISTINCT HBP.*
	FROM  #HighBP HBP
		JOIN [dbo].[PatientProcedure] pp ON  pp.[PatientID] = HBP.[LbPatientID]
			AND pp.procedureDateTime >= HBP.ReadingDate
		JOIN [dbo].[PatientProcedureProcedureCode] pppc ON pppc.[PatientProcedureId] = pp.[PatientProcedureId]
		JOIN [dbo].[ProcedureCode] pc ON  pc.[ProcedureCodeID] = pppc.[ProcedureCodeID]
		JOIN [dbo].[GproEvaluationCode] GEC ON  GEC.[Code] = PC.ProcedureCode
			AND GEC.[VariableName] IN ( 'INTERVENTION_CODE', 'DIAGNOSTIC_STUDY', 'LABORATORY_TEST' )
			AND GEC.[ModuleType] = 'PREV' 
			AND GEC.[ModuleIndicatorGPRO] = '11'
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
        
	-- BP Follow by Drugs
	INSERT INTO #FollowUP
	SELECT DISTINCT HBP.*
	FROM  #HighBP HBP
		JOIN [dbo].[PatientMedication] pm ON  pm.[PatientID] = HBP.[LbPatientID]
		JOIN  [dbo].[Medication] m ON m.[MedicationID] = pm.[MedicationID]
			AND IsNull( pm.[MedicationStartDate], '1900-01-01' ) < @yearEnd
			AND IsNull( pm.[MedicationEndDate], DateAdd( DAY, -1,  @yearEnd ) ) > @yearBegin
			AND IsNull( pm.[MedicationStartDate], HBP.[ReadingDate] ) >= HBP.[ReadingDate] -- Prescription began on or after most recent pre/hypertensive BP Reading
			AND IsNull( pm.[MedicationEndDate],   HBP.[ReadingDate] ) >= HBP.[ReadingDate] -- Prescription use continue to a date on or after most recent pre/hypertensive BP Reading			
		JOIN [DBO].[GPRODRUGCODE] GDC ON  GDC.[Code] IN ( m.[NDCCode], m.[RxNormCode] )
			AND GDC.[VariableName] IN ( 'BP_DRUG_CODE' )
		JOIN @SourceIdTable SS ON SS.SourceSystemId = PM.SourceSystemID        --MEDS

	  -- setting patients that have had their follow up to the required numbers.
	  UPDATE RP
	  SET    RP.PcBloodPressureScreen =  2, 
			 RP.PcBloodPressureFollowUp = 2 , 
			 RP.PcBloodPressureNormal = 0 
	  FROM   #FollowUP FP
		JOIN   #RANKEDPATIENTS RP ON FP.LBPATIENTID = RP.LBPATIENTID

	--------------------------------------------------------------------------------------------------------          
	   -- Updating the required tables
	--------------------------------------------------------------------------------------------------------    

	UPDATE gpm
	SET gpm.[PcBloodPressureConfirmed] = GPR.[PcBloodPressureConfirmed], gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcBloodPressureRank IS NOT NULL 
		AND gpm.PcBloodPressureConfirmed IS NULL

	UPDATE gpm
	SET gpm.PcBloodPressureScreen = GPR.PcBloodPressureScreen, gpm.PcBloodPressureNormal = GPR.PcBloodPressureNormal, gpm.PcBloodPressureFollowUp = GPR.PcBloodPressureFollowUp, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.PcBloodPressureRank IS NOT NULL 
		AND gpm.PcBloodPressureScreen IS NULL

END

     
GO
