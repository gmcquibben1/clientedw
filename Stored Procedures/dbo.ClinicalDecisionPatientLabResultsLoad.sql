SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ClinicalDecisionPatientLabResultsLoad]
@PatientID INT
AS
 
--This stored procedure will return all the anonymous lab result history information for the clinical decisions 
--Evaluation Request
--LBPP - 1463 - Introduced in version 2.01
--CJL - 02.17.2016  -- First Cut




SELECT  PatientLabRestultId, r.ObservationCode1, r.ObservationDescription1, r.ObservationCodingSystemName1, 
		 r.ObservationCode2, r.ObservationDescription2, r.ObservationCodingSystemName2, 
		 Value, Units, ReferenceRange, AbNormalFlag, ObservationDate
FROM 
	 PatientLabResult r WITH (NOLOCK), 
	 PatientLabOrder o WITH (NOLOCK)
WHERE
	o.PatientId = @PatientId AND r.PatientLabOrderId = o.PatientLabOrderID
	AND r.ObservationDate > DATEADD(year,-1,GETDATE()) -- Get Everything for the last year.
	--AND r.ResultStatus = 'F' Filter out the Status of F 
GO
