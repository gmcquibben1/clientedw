SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==================================================
-- Author:		Alex Tran
-- Create date: 2012-12-18
-- Description:	Import Roster TABLE INTO PatientRosterMomths format
--Modified By: Samuel Owusu
--Modified Date: 2016-8-10
-- =================================================



CREATE  PROCEDURE [dbo].[internalMMRosterINSERT]
AS
/*===============================================================
	CREATED BY: 	SG
	CREATED ON:		2016-10-31
	INITIAL VER:	Initial Application Version
	MODULE:			EDW ETL		
	DESCRIPTION:	ETL SP fix old code issue
	PARAMETERS:		
	RETURN VALUE(s)/OUTPUT:	NONE
	
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.0		2016-10-31	SG		PROV-1355	Add code to fix the  SP failure

=================================================================*/
IF OBJECT_ID('tempdb..#pat') IS NOT NULL DROP TABLE #pat
CREATE TABLE [dbo].[#pat](
	[MONTH] datetime NULL,
	[PatientExternalID] [nvarchar](100) NULL,
	[MemberMonthStatus] [nvarchar](100) NULL,
	MemberMonth datetime NULL,
	[Start_date] datetime NULL,
	[Term_date] datetime NULL,
	[LbPatientId] INT
)

-- UPDATE LbPatientID
UPDATE b SET LbPatientId = IDR.LbPatientId 
FROM PatientRosterMonths  B 
JOIN PatientIdReference IDR ON B.[PatientExternalID] = IDR.ExternalId 


--Declare variables for loop
DECLARE @SourceFeed VARCHAR(100)
DECLARE @SourceFeedList TABLE(SourceFeed VARCHAR(100))

--get DISTINCT sourcefeeds
INSERT INTO @SourceFeedList
SELECT
	DISTINCT SourceFeed
FROM PatientRosterMonths
WHERE SourceFeed IS NOT NULL

WHILE (SELECT COUNT(*) FROM @SourceFeedList)>0
BEGIN
	SET @SourceFeed=(SELECT TOP 1 SourceFeed FROM @SourceFeedList)


	TRUNCATE TABLE #pat
	INSERT INTO #pat
	SELECT DISTINCT a.MONTH,a.[PatientExternalID]
	,[MemberMonthStatus],MemberMonth
	,NULL [Start_date],NULL	[Term_date], a.[LbPatientId]
	FROM PatientRosterMonths a
	WHERE SourceFeed=@SourceFeed


	UPDATE #pat SET [Start_date] = MemberMonth
	WHERE [MemberMonthStatus] IN ('NEW','RETURNING')

	UPDATE #pat SET [Term_date] = dateadd(DAY,-1,MONTH)
	WHERE [MemberMonthStatus] IN ('DROPPED_1')


	IF OBJECT_ID('tempdb..#Start') IS NOT NULL 
	BEGIN
		DROP TABLE #Start
	END
	SELECT
		p.[PatientExternalID],
		p.[LbPatientId],
		p.[MemberMonthStatus],
		p.[MemberMonth],
		p.[Start_Date],
		p.[Term_Date]
		INTO #Start
	FROM [#Pat] p
	WHERE p.[Start_Date] IS NOT NULL


	IF OBJECT_ID('tempdb..#Term') IS NOT NULL 
	BEGIN
		DROP TABLE #Term
	END
	SELECT
		p.[PatientExternalID],
		p.[LbPatientId],
		p.[MemberMonthStatus],
		p.[MemberMonth],
		p.[Start_Date],
		p.[Term_Date]
		INTO #Term
	FROM [#Pat] p
	WHERE p.[Term_Date] IS NOT NULL


	IF OBJECT_ID('tempdb..#pat1') IS NOT NULL 
	BEGIN
		DROP TABLE #pat1
	END
	SELECT
		p.[PatientExternalID],
		p.[LbPatientId],
		p.[MemberMonthStatus],
		p.[MemberMonth],
		( SELECT IsNull( MAX( s.[Start_Date] ), '1900-01-01' ) FROM [#Start] s WHERE s.[LbPatientId]= p.[LbPatientId]AND s.[Start_Date] <= p.[MemberMonth] ) AS [START],
		( SELECT IsNull( MIN( t.[Term_Date] ), '2099-12-31' ) FROM [#Term] t WHERE t.[LbPatientId]= p.[LbPatientId]AND t.[Term_Date] >= DateAdd( DAY, -1, p.[MemberMonth] ) ) AS [Term]
	INTO #pat1
	FROM [#Pat] p


	IF OBJECT_ID('tempdb..#term_min') IS NOT NULL 
	BEGIN
		DROP TABLE #term_min
	END
	SELECT p.[LbPatientId],p.[PatientExternalID],	START AS [StartDate],	MIN(Term) [TermDate]
	INTO #term_min
	FROM #pat1 p
	GROUP BY p.[LbPatientId],p.[PatientExternalID],	START


	IF OBJECT_ID('tempdb..#pat_final') IS NOT NULL 
	BEGIN
		DROP TABLE #pat_final
	END
	SELECT DISTINCT p.[LbPatientId],p.[PatientExternalID], p.[MemberMonthStatus], p.MemberMonth, tm.[StartDate], tm.[TermDate]
	INTO #pat_final
	FROM #pat1 p
	JOIN #term_min tm ON p.[LbPatientId] = tm.[LbPatientId] AND p.START = tm.[StartDate] AND p.Term = tm.[TermDate]
	WHERE p.MemberMonth IS NOT NULL


	IF OBJECT_ID('tempdb..#final') IS NOT NULL 
	BEGIN
		DROP TABLE #final
	END
	SELECT DISTINCT memRos.[SourceSystemID], pat.LbPatientId, pat.[StartDate], pat.[TermDate]
	, memRos.[FirstName] , memRos.[LastName], memRos.[Gender] AS [Gender], memRos.[Birthdate], MAX([FileDate]) [FileDate]
	INTO #final
	FROM #pat_final pat
	JOIN PatientRosterMonths memRos ON memRos.[LbPatientId] = pat.[LbPatientId]
	WHERE FileDate IS NOT NULL
	AND memRos.SourceFeed=@SourceFeed
	GROUP BY memRos.[SourceSystemID],pat.LbPatientId, pat.[StartDate], pat.[TermDate]
		, memRos.[FirstName] , memRos.[LastName], memRos.[Gender] , memRos.[Birthdate]


	DELETE FROM PatientRosterEnrollmentRaw
	WHERE SourceFeed=@SourceFeed


	INSERT INTO [dbo].[PatientRosterEnrollmentRaw]
			   ([SourceSystemID]
			   ,[PatientExternalID]
			   ,[FirstName]
			   ,[LastName]
			   ,[Gender]
			   ,[Birthdate]
			   ,[StartDate]
			   ,[TermDate]
			   ,[ProviderNPI]
			   ,[FileDate]
			   ,[SourceFeed]
			   ,[LbPatientId])
	SELECT DISTINCT a.[SourceSystemID]
			   ,prm.[PatientExternalID]
			   ,a.[FirstName]
			   ,a.[LastName]
			   ,a.[Gender]
			   ,a.[Birthdate]
			   ,a.[StartDate]
			   ,a.[TermDate]
			   ,prm.[ProviderNPI]
			   ,a.[FileDate]
			   ,prm.[SourceFeed]
			   ,a.[LbPatientId]
	FROM #final a
	JOIN PatientRosterMonths prm 
		ON a.[LbPatientId] = prm.[LbPatientId] AND a.FileDate = prm.FileDate
	WHERE prm.SourceFeed=@SourceFeed

	DELETE FROM @SourceFeedList WHERE SourceFeed=@SourceFeed
END

GO
