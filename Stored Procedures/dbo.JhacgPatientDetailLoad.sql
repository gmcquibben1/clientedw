SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ===============================================================================================================
-- Author:		Lan Ma
-- Create date: 2016-07-01
-- Description: Archive last frun of JhacgPatientDetail data and move the new Jhacg data from staging to final 
-- table JhacgPatientDetail
-- Modified By: 
-- Modified Date: 
-- Description:	
-- ===============================================================================================================


CREATE PROCEDURE [dbo].[JhacgPatientDetailLoad] 

AS
BEGIN

TRUNCATE TABLE [dbo].[JHACGPatientDetailArchive]

INSERT INTO [dbo].[JHACGPatientDetailArchive]
SELECT *
FROM [JHACGPatientDetail]

TRUNCATE TABLE [dbo].[JHACGPatientDetail]

INSERT INTO [dbo].[JHACGPatientDetail]
SELECT *, GETDATE() 
FROM [JHACGPatientDetailStaging]

END
GO
