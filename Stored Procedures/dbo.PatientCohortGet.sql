SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--CREATE PROC
CREATE PROCEDURE [dbo].[PatientCohortGet] @CohortIds varchar(1000) = '0', @CohortStatusTypes varchar(1000) = '1-2-3-4', @UnionIntersection bit = 0
AS
SET NOCOUNT ON;
/*======================================================================================================================
	CREATED BY: 	Mike Hoxter
	CREATED ON:		2017-01-07
	INITIAL VER:	2.2.2
	MODULE:			PatientCohortGet 		
	DESCRIPTION:	This SP will retrieve the list of patient IDs for a list of cohorts with a default set of statuses, and will either union or intersect the results
	PARAMETERS:		CohortIds - comma seperated list of INTs, to be split into table list of ints
                CohortStatusTypes - comma seperated list of INTs, to be split into table list of ints
                UnionIntersection - bit where 0 will return all patients from the list of cohorts, 1 will return only those in all cohorts
	RETURN VALUE(s)/OUTPUT:	Array of integer values as PatientId
	PROGRAMMIN NOTES: If cohort Ids is defaulted, where it is a 0 only, then the system will return a single value, this is for use by Tableau
    
    
        
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.2		    2017-01-07	  	MH	    		LBAN-3450	 Initial
  2.2.2       2017-01-17      MH          Changing Delimiter to a '-'
============================================================================================================================*/


--output if ran with no/default parameters
IF @CohortIds = '0'
BEGIN
SELECT 0 AS PatientId
END
else 
if @UnionIntersection = 0
begin
SELECT 0 AS PatientId
UNION ALL
SELECT DISTINCT PatientId FROM vwPortal_CohortPatient
join (select try_convert(int,Data) CohortId FROM UDF_SplitDelimitedStringToColumns(@CohortIds, '-')) cohorts
        ON dbo.vwPortal_CohortPatient.CohortId = cohorts.CohortId
join (select try_convert(int,Data) StatusTypetId FROM UDF_SplitDelimitedStringToColumns(@CohortStatusTypes, '-')) StatusTypes
        ON dbo.vwPortal_CohortPatient.CohortEnrollmentStatusTypeId = StatusTypes.StatusTypetId
      
END
ELSE 
BEGIN
--select intersection of all cohort IDs passed
DECLARE @CohortIdCount INT = (SELECT COUNT(*) FROM UDF_SplitDelimitedStringToColumns(@CohortIds, '-') )
SELECT 0 AS PatientId
UNION ALL
SELECT PatientID FROM (
SELECT PatientID, count(1) as [CohortCount] FROM vwPortal_CohortPatient
join (select try_convert(int,Data) CohortId FROM UDF_SplitDelimitedStringToColumns(@CohortIds, '-')) cohorts
        ON dbo.vwPortal_CohortPatient.CohortId = cohorts.CohortId
join (select try_convert(int,Data) StatusTypetId FROM UDF_SplitDelimitedStringToColumns(@CohortStatusTypes, '-')) StatusTypes
        ON dbo.vwPortal_CohortPatient.CohortEnrollmentStatusTypeId = StatusTypes.StatusTypetId
GROUP BY PatientID
having count(*) = @CohortIdCount
) cohorts

END
GO
