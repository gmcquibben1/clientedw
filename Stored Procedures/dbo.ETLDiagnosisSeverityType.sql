SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-03-23
-- Description:	Load DiagnosisPriorityType Data into EDW
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[ETLDiagnosisSeverityType]

AS
BEGIN	
        INSERT INTO [dbo].[DiagnosisSeverityType]
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
        SELECT DISTINCT
		t1.[DiagnosisSeverity],
		t1.[DiagnosisSeverity],
		0,
		GETUTCDATE(),
		GETUTCDATE(),
		1,
		1
        FROM PatientDiagnosisProcessQueue t1
		LEFT JOIN [dbo].[DiagnosisSeverityType] t2
		ON LTRIM(RTRIM(t1.[DiagnosisSeverity])) = LTRIM(RTRIM(t2.[Name]))
		WHERE t1.[DiagnosisSeverity] IS NOT NULL AND  t2.[Name] IS NULL
END
GO
