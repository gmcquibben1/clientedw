SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SystemImportLogLoad]
AS
SELECT SIDT.DisplayValue as [DataType], FORMAT(MAX(SIL.StartDateTime),'d') as [LoadDate], SILM.DisplayValue as [MetricDisplay], SILM.[Value] as [MetricValue]
  FROM SystemImportDataType SIDT
	JOIN SystemImportLog SIL ON SIDT.SystemImportDataTypeId = SIL.SystemImportDataTypeId
	LEFT JOIN SystemImportLogMetric SILM ON SIL.SystemImportLogId = SILM.SystemImportLogId
  WHERE SIDT.DeleteInd = 0
  GROUP BY SIDT.DisplayValue, SILM.DisplayValue, SILM.[Value],  SILM.SystemImportLogMetricId
  ORDER BY SIDT.DisplayValue ASC, SILM.SystemImportLogMetricId ASC
 
GO
