SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	CREATE PROCEDURE [dbo].[uspCalcMeasure_GPRO_2015@MH-1] @LookbackTimestamp DATETIME='2000-01-01 00:00:00',@dataPreference smallint = 1 
AS
BEGIN

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF OBJECT_ID('tempdb..#RANKEDPATIENTS') IS NOT NULL 
		DROP TABLE [#RANKEDPATIENTS];

	DECLARE  @yearend datetime, @measureBeginDate datetime, @start_dt datetime, @end_dt datetime;

	SET @yearend = '01/01/2016';
	SET @measureBeginDate = '01/01/2015';
	SET @start_dt = dateadd(mONth,-1,@measureBeginDate); --dec 1 of prior year
	SET @end_dt = dateadd(day,-1,dateadd(mONth,11,@measureBeginDate)) --nov 30 of measurment year

	--EstablIShing the Clinical Data sourcing indicators.
	DECLARE  @SourceIdTable TABLE 
	(
		SourceSystemId	INT
	)

	IF @dataPreference = 0 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem;
	END
	ELSE 
	BEGIN
		INSERT INTO @SourceIdTable
		(SourceSystemId)
		SELECT SourceSystemId FROM SourceSystem WHERE ClinicalInd = 1;
	END

	--Initial Patient pooling, for patients that have ranking for the mh measure. 
	SELECT r.LBPatientID,
		r.GProPatientRankingId,
		DATEDIFF(year,r.Birthdate,@start_dt) as Age,
		r.Birthdate,
		GM.GproMeasureTypeID,
    	GPM.MHRANK,
		GPM.MHCONFIRMED,
		GPM.MHINDEXPERFORMED, 
		GPM.MHINDEXTEST, 
    	GPM.MHINDEXDATE,
    	GPM.MHINDEXSCORE,
    	GPM.MHFOLLOWUPPERFORMED,
    	GPM.MHFOLLOWUPTEST,
    	GPM.MHFOLLOWUPDATE,
    	GPM.MHFOLLOWUPSCORE,
    	GPM.MHCOMMENTS,
		GETUTCDATE() as ModifyDateTime
	INTO   #RankedPatients --Drop Table #RankedPatients
	FROM   GproPatientRanking r
		JOIN   GproPatientRankingGproMeasureType xref ON xref.GproPatientRankingID = r.GproPatientRankingID
		JOIN   GproMeasureType GM ON XREF.GproMeasureTypeID = GM.GproMEasureTYpeID 
		JOIN   GproPatientMeasure GPM ON GPM.GPROPATIENTRANKINGID = r.GproPatientRankingID
	WHERE  GM.Name = 'MH-1' 
		AND (GPM.MhCONfirmed IS NULL OR GPM.MhIndexTest IS NULL OR GPM.MhIndexPerformed IS NULL)

	--Age not cONfirmed
    UPDATE #RankedPatients
    SET MhCONfirmed = '19' --Not CONfirmed - Age
    FROM #RankedPatients
    WHERE Age < 18
		AND MhCONfirmed IS NULL
	--	select dIStinct age FROM #RankedPatients order by age desc


    IF OBJECT_ID('tempdb..#depressiON') IS NOT NULL 
		DROP TABLE [#depressiON];


    -- check for deperssiON or dsythymia
    SELECT DISTINCT p.*, pp.[ProcedureDateTime] 
    INTO #depressiON -- drop table #depressiON
    FROM #RankedPatients p 
		JOIN [dbo].[PatientProcedure] pp ON p.LbPatientId = pp.PatientID
		JOIN [dbo].[PatientProcedureProcedureCode] pppc ON  pppc.[PatientProcedureID] = pp.[PatientProcedureID]
    		AND pp.[ProcedureDateTime] BETWEEN @start_dt AND @end_dt
		JOIN [dbo].[ProcedureCode] pc ON  pc.[ProcedureCodeId] = pppc.[ProcedureCodeId]
		JOIN [dbo].[GproEvaluatiONCode] hvsc_proc ON  hvsc_proc.[Code] = pc.[ProcedureCode]
			AND hvsc_proc.[ModuleType] = 'MH'
			AND hvsc_proc.[VariableName] IN ('ENCOUNTER_CODE','PSYCH_ENC_CODE')
		JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS

--    
--    INSERT INTO #depressiON -- drop table #depressiON
--    SELECT DISTINCT P.*, PP.[PROCEDUREDATETIME]     
--    FROM #RankedPatients p 
		JOIN [dbo].[PatientDiagnosIS] pd 
    			ON  pd.[PatientID] = p.[LBPatientID] 
    			AND Cast( pd.[DiagnosISDateTime] AS DATE ) = Cast( pp.[ProcedureDateTime] AS DATE ) -- Occurred in the same encounter
		JOIN [dbo].[PatientDiagnosISDiagnosISCode] pddc
    			ON  pddc.[PatientDiagnosISId] = pd.[PatientDiagnosISId]
		JOIN [dbo].[DiagnosISCode] dc
    			ON  dc.[DiagnosISCodeID] = pddc.[DiagnosISCodeID]
		JOIN [dbo].[GproEvaluatiONCode] hvsc_diag
    			ON replace(hvsc_diag.[Code],'.','') = dc.[DiagnosISCode]
    			AND hvsc_diag.[ModuleType] = 'MH'
    			AND hvsc_diag.[VariableName] IN ('DEPRESSION_CODE', 'DYSTHYMIA_CODE')
			AND SS.[SourceSystemId] = PD.[SourceSystemID]   --DIAGS



    --dx cONfirmed
    UPDATE #RankedPatients
    SET    MhCONfirmed = '2' --Include required MH measure elements to complete the patient s data for MH.
    FROM #RankedPatients rp
		INNER JOIN #depressiON dep ON rp.LBPatientId = dep.LBPatientId
    WHERE  rp.MHCONFIRMED IS NULL

    /*
    --dx not cONfrimed
    	UPDATE #RankedPatients
        SET    MhCONfirmed = '8'
        FROM #RankedPatients rp
    	left JOIN #depressiON dep ON rp.LBPatientId = dep.LBPatientId
    	WHERE dep.LBPATIENTID IS NULL
    		AND rp.MHCONFIRMED IS NULL
    */


    --PHA09 TEST
    SELECT *
    , ROW_NUMBER () OVER (PARTITION BY sub.[LbPatientID] ORDER BY sub.[observatiONdate] ASC,  [result]  ASC) earliest_result
    INTO #initial_test
    FROM
    (
    	SELECT DISTINCT stage.[LbPatientID],
    			  plr.[ObservatiONDate],
    			  ISNULL( Try_CONvert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 )  AS [Result]
    	FROM #depressiON  stage
    	JOIN [dbo].[PatientLabOrder] plo
    			ON  plo.[PatientID] = stage.[LbPatientID]
    	JOIN [dbo].[PatientLabResult] plr
    			ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
    			AND plr.ObservatiONDate = stage.ProcedureDateTime -- ON same date
    	JOIN dbo.[GproEvaluatiONCode] GEC
    			ON GEC.[Code] = plr.[ObservatiONCode1]
    			AND [ModuleType] = 'MH'  -- 'DM'  --								--- using dm for testing
    			AND [VariableName] = 'PHQ9_TOOL_CODE'	--	'A1C_CODE' --			--- using dm for testing
      JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PLO.[SourceSystemID]    --LABS
    	GROUP BY  stage.[LbPatientID] ,
    			  plr.[ObservatiONDate],
    			  GEC.[Code],
    			  ISNULL( Try_CONvert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 )
    	HAVING ISNULL( Try_CONvert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 ) > 9
    	UNION
    	SELECT DISTINCT  p.[LbPatientID], 
    			pp.[ProcedureDateTime], 
    			ISNULL( Try_CONvert( DECIMAL(5, 2), pp.procedureresult, 0 ), 0.0 )  AS [Result]
    	FROM #depressiON p 
    	JOIN [dbo].[PatientProcedure] pp 
    			ON p.LbPatientId = pp.PatientID
    	JOIN [dbo].[PatientProcedureProcedureCode] pppc -- Encounter Patient Procedure
    			ON  pppc.[PatientProcedureID] = pp.[PatientProcedureID]
    			AND pp.ProcedureDateTime = p.ProcedureDateTime -- ON same date
    	JOIN [dbo].[ProcedureCode] pc 
    			ON  pc.[ProcedureCodeId] = pppc.[ProcedureCodeId]
    	JOIN [dbo].[GproEvaluatiONCode] hvsc_proc 
    			ON  hvsc_proc.[Code] = pc.[ProcedureCode]
    			AND hvsc_proc.[ModuleType] = 'MH'	-- 		'DM'  --						--- using dm for testing
    			AND hvsc_proc.[VariableName] = 'PHQ9_TOOL_CODE'	--'A1C_CODE' --				--- using dm for testing
      JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
    	GROUP BY p.[LbPatientID] ,
    			pp.[ProcedureDateTime], 
    			ISNULL( Try_CONvert( DECIMAL(5, 2), pp.procedureresult, 0 ), 0.0 ) 
    	HAVING ISNULL( Try_CONvert( DECIMAL(5, 2), pp.procedureresult, 0 ), 0.0 )  > 9
    ) sub

    --mhindex perFROMed 
    UPDATE #RankedPatients
    SET MhIndexPerformed = '2' --yes
    FROM #RankedPatients rp
		INNER JOIN #initial_test id ON rp.LBPatientId = id.LBPatientId
    WHERE rp.MhIndexPerformed IS NULL

    /*
    --mhindex not performed
    UPDATE #RankedPatients
    SET    MhIndexPerformed = '1'-- no
    -- select rp.* , id.lbpatientid
    FROM #RankedPatients rp
    left JOIN #initial_test id ON rp.LBPatientId = id.LBPatientId
    WHERE id.LBPATIENTID IS NULL
    	AND rp.MhIndexPerformed IS NULL
    	*/


    --mhindex test perFROMed 
    UPDATE #RankedPatients
    SET MhIndextest = '2' -- yes
    FROM #RankedPatients rp
		INNER JOIN #initial_test id ON rp.LBPatientId = id.LBPatientId

    /*
    --mhindex not performed
    UPDATE #RankedPatients
    SET    MhIndextest = '1' -- no
    -- select rp.* , id.lbpatientid
    FROM #RankedPatients rp
    left JOIN #initial_test id ON rp.LBPatientId = id.LBPatientId
    WHERE id.LBPATIENTID IS NULL
    	AND rp.MhIndextest IS NULL
    */

    --mhindex date perFROMed 
    UPDATE #RankedPatients
    SET MhIndexdate = CONVERT(VARCHAR(10), id.[observatiONdate], 101)
    FROM #RankedPatients rp
		INNER JOIN #initial_test id ON rp.LBPatientId = id.LBPatientId
    WHERE id.earliest_result = 1 -- pull earliest result > 9

    --mhindex score
    UPDATE #RankedPatients
    SET mhindexscore = id.result --- index score IS not caputring decimal
    FROM #RankedPatients rp
		INNER JOIN #initial_test id ON rp.LBPatientId = id.LBPatientId
    WHERE id.earliest_result = 1 -- pull earliest result > 9


    /* numerator
    RemISsiON IS defined as a PHQ-9 score of less than five.
    Twelve MONths IS defined as the point in time FROM the date in the measurement period that a patient meets the inclusiON criteria (diagnosIS AND elevated PHQ-9 >9)
    extending out twelve mONths AND then allowing a grace period of thirty days prior to AND thirty days after thIS date. Any PHQ-9 less than five obtained during thIS period IS deemed as remISsiON at 12 mONths, 
    values obtained prior to or after thIS period are not counted as numerator compliant (remISsiON).
    */

    SELECT DISTINCT stage.[LbPatientID], stage.earliest_result,
    			  plr.[ObservatiONDate] remISsiON_dt,
    			  ISNULL( Try_CONvert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 )  AS [Result]
    	into #remISsiON -- drop table #remISsiON
    	FROM #initial_test  stage
    	JOIN [dbo].[PatientLabOrder] plo
    			ON  plo.[PatientID] = stage.[LbPatientID]
    	JOIN [dbo].[PatientLabResult] plr
    			ON  plr.[PatientLabOrderID] = plo.[PatientLabOrderID]
    	JOIN dbo.[GproEvaluatiONCode] GEC
    			ON GEC.[Code] = plr.[ObservatiONCode1]
    			   AND [ModuleType] = 'MH'  -- 'DM'  --								
    			   AND [VariableName] = 'PHQ9_TOOL_CODE'	--	'A1C_CODE' --	
    	WHERE plr.[ObservatiONDate] between dateadd(mONth,11,stage.ObservatiONDate) AND dateadd (mONth,13, stage.ObservatiONDate) -- 12 mONth +/- 30 days FROM index date
    	GROUP BY  stage.[LbPatientID] , stage.earliest_result,
    			  plr.[ObservatiONDate],
    			  GEC.[Code],
    			  ISNULL( Try_CONvert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 )
    	HAVING ISNULL( Try_CONvert( DECIMAL(5, 2), plr.[Value], 0 ), 0.0 ) < 5
    	UNION
    	SELECT DISTINCT  p.[LbPatientID] , p.earliest_result,
    			pp.[ProcedureDateTime], 
    			ISNULL( Try_CONvert( DECIMAL(5, 2), pp.procedureresult, 0 ), 0.0 )  AS [Result]
    	FROM #initial_test p 
    	JOIN [dbo].[PatientProcedure] pp 
    			ON p.LbPatientId = pp.PatientID
    	JOIN [dbo].[PatientProcedureProcedureCode] pppc -- Encounter Patient Procedure
    			ON  pppc.[PatientProcedureID] = pp.[PatientProcedureID]
    	JOIN [dbo].[ProcedureCode] pc 
    			ON  pc.[ProcedureCodeId] = pppc.[ProcedureCodeId]
    	JOIN [dbo].[GproEvaluatiONCode] hvsc_proc 
    			ON  hvsc_proc.[Code] = pc.[ProcedureCode]
    			AND hvsc_proc.[ModuleType] = 'MH'	-- 		'DM'  --						
    			AND hvsc_proc.[VariableName] = 'PHQ9_TOOL_CODE'	--'A1C_CODE' --	
      JOIN @SourceIdTable SS ON SS.[SourceSystemId] = PP.[SourceSystemID]    --PROCS
    	WHERE pp.ProcedureDateTime between dateadd(mONth,11,p.ObservatiONDate) AND dateadd (mONth,13, p.ObservatiONDate) -- between 11 AND 13 mONths following the index date
    	GROUP BY p.[LbPatientID] , p.earliest_result,
    			pp.[ProcedureDateTime], 
    			ISNULL( Try_CONvert( DECIMAL(5, 2), pp.procedureresult, 0 ), 0.0 ) 
    	HAVING ISNULL( Try_CONvert( DECIMAL(5, 2), pp.procedureresult, 0 ), 0.0 )  < 5



    --MhFollowUpPerformed 
    UPDATE #RankedPatients
    SET MhFollowUpPerformed = '2' -- yes
    FROM #RankedPatients rp
		INNER JOIN #remISsiON id ON rp.LBPatientId = id.LBPatientId
    WHERE rp.MhFollowUpPerformed IS NULL


    --MhFollowUpTest
    UPDATE #RankedPatients
    SET MhFollowUpTest = '2' -- yes
    FROM #RankedPatients rp
		INNER JOIN #remISsiON id ON rp.LBPatientId = id.LBPatientId
    WHERE rp.MhFollowUpTest IS NULL


    --MhFollowUPDATE
    UPDATE #RankedPatients
    SET MhFollowUPDATE = CONVERT(VARCHAR(10), id.remISsiON_dt, 101)
    FROM #RankedPatients rp
		INNER JOIN #remISsiON id ON rp.LBPatientId = id.LBPatientId
    WHERE id.earliest_result = 1 -- pull earliest result > 9
		AND rp.MhFollowUPDATE IS NULL



    --MhFollowUpScore
    UPDATE #RankedPatients
    SET MhFollowUpScore = id.result --- index score IS not caputring decimal
    FROM #RankedPatients rp
		INNER JOIN #remISsiON id ON rp.LBPatientId = id.LBPatientId
    WHERE id.earliest_result = 1 -- pull earliest result > 9
		AND rp.MhFollowUpScore IS NULL


    --------------------------------------------------------------------------------------------------------          
    -- Updating the required tables
    --------------------------------------------------------------------------------------------------------    

    --select * FROM #RankedPatients
       
    --UPDATE gpm
    --SET   GPM.MhIndexPerformed = gpr.MhIndexPerformed, gpm.MhIndextest = gpr.MhIndextest, gpm.MhIndexdate = gpr.MhIndexdate, gpm.mhindexscore = gpr.mhindexscore
    --	 ,GPM.MhFollowUpPerformed = gpr.MhFollowUpPerformed, gpm.MhFollowUpTest = gpr.MhFollowUpTest, gpm.MhFollowUPDATE = gpr.MhFollowUPDATE, gpm.MhFollowUpScore = gpr.MhFollowUpScore, gpm.ModifyDateTime = GETUTCDATE()
    ----select gpm.*,gpr.PcPneumoShotCONfirmed
    --FROM GproPatientMeasure gpm
    --       JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId

	UPDATE gpm
		SET gpm.MhCONfirmed = gpr.MhCONfirmed, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE gpm.MhCONfirmed IS NULL;
	
	UPDATE gpm
		SET gpm.MhIndexPerformed = gpr.MhIndexPerformed, gpm.MhIndexTest = gpr.MhIndexTest, gpm.MhIndexDate = gpr.MhIndexDate, gpm.MhIndexScore = gpr.MhIndexScore, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE (gpm.MhIndexPerformed IS NULL AND gpr.MhIndexPerformed IS NOT NULL)
		 OR (gpm.MhIndexTest IS NULL AND gpr.MhIndexTest IS NOT NULL)
		 OR (gpm.MhIndexDate IS NULL AND gpr.MhIndexDate IS NOT NULL)
		 OR (gpm.MhIndexScore IS NULL AND gpr.MhIndexScore IS NOT NULL)

	UPDATE gpm
		SET gpm.MhFollowUpPerformed = gpr.MhFollowUpPerformed, gpm.MhFollowUpTest = gpr.MhFollowUpTest, gpm.MhFollowUpDate = gpr.MhFollowUpDate, gpm.MhFollowUpScore = gpr.MhFollowUpScore, gpm.ModifyDateTime = GETUTCDATE(), gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		JOIN #RankedPatients gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
	WHERE (gpm.MhFollowUpPerformed IS NULL AND gpr.MhFollowUpPerformed IS NOT NULL)
		OR (gpm.MhFollowUpTest IS NULL AND gpr.MhFollowUpTest IS NOT NULL)
		OR (gpm.MhFollowUpDate IS NULL AND gpr.MhFollowUpDate IS NOT NULL)
		OR (gpm.MhFollowUpScore IS NULL AND gpr.MhFollowUpScore IS NOT NULL)


END
GO
