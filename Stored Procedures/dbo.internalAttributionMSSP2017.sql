SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalAttributionMSSP2017] 
/*========================================================================================================================
	CREATED BY: 	Lan Ma
	CREATED ON:		2016-12-28
	INITIAL VER:	2.2.2
	MODULE:			Patient Provider Attribution 		
	DESCRIPTION:	NEW MSSP attribution logic by using the latest roster file data and claims amount by CCN/TIN/PCP.
	                Step 1: find each patient's active CCN/TIN and PCP list
					Step 2. get the SUM(paid), visit count, latest DOS of each patient by CCN/TIN/PCP from last 18 month data
					        note: limit cto laims with revenue codes of '0521','0522','0524','0525' or outpatients CPTs; And
							      limit to claims within the CCN/TIN from the latest roster
					Step 3: Pick a PCP or CCN/TIN by (1) highest SUM(Paid) (2) Latest DOS (3)highest visit count (4) minimum NPI/CCN 
	PARAMETERS:		(Optional) All non-obvious parameters must be described
	RETURN VALUE(s)/OUTPUT:	(Optional) Especially when returning specific values or error codes.
	PROGRAMMIN NOTES: (Optional) This is sometimes helpful to explain some implementation details that may not be obvious.
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	#.#.#		YYYY-MM-DD		UC			LBPP-####	Initial version or descrition of the changes

=========================================================================================================================*/

( @LookBackMonths int = 18)
AS
BEGIN

	DECLARE @AttributionTypeId int = 13;
	DECLARE @sourceSystem int = 1;

	--active providers including CCN/TIN
	SELECT DISTINCT RTRIM(LTRIM([NationalProviderIdentifier])) AS ProviderNPI
	INTO #providerList
	FROM [dbo].[vwPortal_ProviderActive]

	CREATE NONCLUSTERED INDEX NCIX_#providerListNPI ON #providerList (ProviderNPI);

	--active PCPs
	SELECT DISTINCT ProviderNPI
	INTO #PCP
	FROM #providerList t2
	JOIN NPI_Lookup t3 
	ON t2.ProviderNPI = t3.NPI 
	AND t3.Specialty IN ('1','01','8','08','11','31', 'FP','GP','IM', '37', 'OBG','OBS','GYN','16','38','FPG','IMG','MDM','50')

	CREATE NONCLUSTERED INDEX NCIX_#PCPNPI ON #PCP(ProviderNPI);

	--patients of each CCN/TIN from the latest Roster( not every CCN/TIN from the latest Roster in the active provider list)
	SELECT DISTINCT [HICNO],ACO, LbpatientID, [ACO Participant TIN Number] AS CCNTIN
	INTO #patientCCN
	FROM [dbo].[MemberRosterArchives] t1
	JOIN #providerList t2
	ON RTRIM(LTRIM(CAST(t1.[ACO Participant TIN Number] AS varchar(10)))) = t2.ProviderNPI
	WHERE [Timeline] = ( SELECT MAX([TimeLine]) FROM [dbo].[MemberRosterArchives] )
	AND ([StatusDesc]  NOT in('Assignable','Dropped'))

	CREATE NONCLUSTERED INDEX NCIX_#patientCCNLbpatientID ON #patientCCN (LbpatientID) INCLUDE(CCNTIN,HICNO);

	--rank patient's provider(CCN/TIN) by pay amount, latest claim date, visit count, NPI(CCN/TIN)
	--DECLARE  @LookBackMonths int = 18
	SELECT
	[LbPatientId],
	[HICNO],
	NPI,
	CCN_TIN_Flag,
	ROW_NUMBER() OVER( PARTITION BY [LbPatientId] ORDER BY SumPaid DESC, LastClaimDate DESC, VisitCount DESC, NPI) as rowNbr
	INTO #patientProvider
	FROM(
	--claim amount, latest claim date and visit count by active CCN/TIN for latest roster patients 
	SELECT 
	t1.[LbPatientId],
	[HICNO],
	CAST([PRVDR_OSCAR_NUM] AS VARCHAR(10)) AS NPI,
	SUM([CLM_LINE_CVRD_PD_AMT])AS SumPaid,
	MAX(CLM_FROM_DT) AS LastClaimDate,
	COUNT(DISTINCT CLM_FROM_DT) AS VisitCount,
	1 AS CCN_TIN_Flag
	FROM [dbo].[CCLF_2_PartA_RCDetail] t1
	LEFT JOIN HEDISValueSetCodes t2 
	ON t1.CLM_LINE_HCPCS_CD = t2.Code 
	AND t2.[Value Set Name] = 'Outpatient'
	--JOIN [dbo].[CCLF_1_PartA_Header] t3
	--ON t1.[CUR_CLM_UNIQ_ID] = t3.[CUR_CLM_UNIQ_ID]
	JOIN  #patientCCN t4
	ON t1.LbPatientId = t4.LbPatientId
	AND ISNULL(TRY_CONVERT(INT,t1.PRVDR_OSCAR_NUM ),0) = t4.CCNTIN
	WHERE  DATEDIFF(Month, [CLM_LINE_FROM_DT], getdate()) <= @LookBackMonths--18 
		   AND ([CLM_LINE_REV_CTR_CD] in('0521','0522','0524','0525') OR t2.Code IS NOT NULL)
			AND t1.LbPatientId IS NOT NULL 
	GROUP BY t1.[LbPatientId],[PRVDR_OSCAR_NUM],[HICNO]

	UNION ALL

	--claim amount, latest claim date and visit count by active PCP for latest roster patients, maybe limit by patient TIN
	SELECT 
	B.LbPatientId, 
	[HICNO],
	CAST(B.RNDRG_PRVDR_NPI_NUM AS VARCHAR(10)) AS NPI,  
	SUM(ISNULL(B.CLM_LINE_CVRD_PD_AMT,0)) AS SumPaid,
	MAX(B.CLM_FROM_DT) AS LastClaimDate,
	COUNT(DISTINCT CLM_FROM_DT) AS VisitCount,
	0 AS CCN_TIN_Flag
	FROM CCLF_5_PartB_Physicians B
	JOIN HEDISValueSetCodes H 
	ON B.CLM_LINE_HCPCS_CD = H.Code 
	AND H.[Value Set Name] = 'Outpatient'
	JOIN  #patientCCN t4
	ON B.LbPatientId = t4.LbPatientId
	JOIN #PCP PP 
	ON CAST(B.RNDRG_PRVDR_NPI_NUM AS VARCHAR(10)) = PP.ProviderNPI
	WHERE DATEDIFF(Month, B.CLM_FROM_DT, getdate()) <= @LookBackMonths -- 18 
		AND ISNULL(TRY_CONVERT(INT,B.CLM_ADJSMT_TYPE_CD),0) = 0  
		AND B.LbPatientId IS NOT NULL 
	GROUP BY B.LbPatientId,B.RNDRG_PRVDR_NPI_NUM,[HICNO] 
	) t
	
	DROP TABLE #patientCCN;
	DROP TABLE #PCP;
	DROP TABLE #providerList;

	INSERT INTO [dbo].[vwPortal_PatientProvider]
	([PatientID]
	,[ProviderID]
	,[SourceSystemID]
	,[ExternalReferenceIdentifier]
	,[OtherReferenceIdentifier]
	,[ProviderRoleTypeID]
	,[DeleteInd]
	,[CreateDateTime]
	,[ModifyDateTime]
	,[CreateLBUserId]
	,[ModifyLBUserId]
	,[AttributionTypeId]
	)
	SELECT 
	LbPatientId AS patientID,
	MAX(COALESCE(providerID,0)) as providerID,
	1 AS SourcesystemID,
	MAX(HICNO) as ExternalReferenceIdentifier,
	MAX(COALESCE(NPI,'0')) AS [OtherReferenceIdentifier],
	1 AS [ProviderRoleTypeID],
	0 AS [DeleteInd],
	GETDATE() AS [CreateDateTime],
	GETDATE() AS [ModifyDateTime],
	1 AS [CreateLBUserId],
	1 AS [ModifyLBUserId],
	@AttributionTypeId AS [AttributionTypeId]
	FROM #patientProvider t1
	JOIN [dbo].[vwPortal_ProviderActive] t3
	ON t1.NPI = RTRIM(LTRIM(t3.[NationalProviderIdentifier]))
	AND rownbr = 1
	GROUP BY LbPatientId

	DROP TABLE #patientProvider
END
GO
