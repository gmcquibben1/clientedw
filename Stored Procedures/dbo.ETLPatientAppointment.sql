SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




Create PROCEDURE [dbo].[ETLPatientAppointment] 	
	@fullLoadFlag bit = 0, 
	@BatchSize INT = 10000, 
	@InterfacePatientId VARCHAR(50) = NULL,
	@overrideMinIndex INT = NULL
AS
BEGIN


  /*===============================================================
	CREATED BY: 	Lan Man
	CREATED ON:		2016-06-27
	INITIAL VER:	2.0.1
	MODULE:			ETL
	DESCRIPTION:	Load Interface Patient Appointment Data into EDW
	PARAMETERS:		
		 @fullLoadFlag =1 will fix the wrong patientID by truncate table and reload full date
		 @fullLoadFlag =0 Merge date to the existing table
		 @BatchSize = Batch size to run.
		 @Scope the routine to a specific patient id 

	
	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.0.1		2016-06-27		LM						Created

	2.1.2		2016-12-31		CJL			LBETL-526	Made changes to the stored procedure to handle the appointments in batches, to reduce the load on the temp tables
														Removed the sorts on create dates. Since the records in the Interface table are never updated and it has a incremental seeded key, we can use that 
														to implicitly sort. higher keys are newer records




=================================================================*/


--DECLARE @fullLoadFlag bit = 1;
--DECLARE 	@BatchSize INT = 10000;
--DECLARE @InterfacePatientId VARCHAR(50) = NULL;
--DECLARE @overrideMinIndex INT = 0;


DECLARE @EDWName VARCHAR(128) = DB_NAME();	
DECLARE @EDWtableName VARCHAR(100) = 'PatientAppointment';
DECLARE @dataSource VARCHAR(20) = 'interface';
DECLARE @ProcedureName VARCHAR(128) = 'ETLPatientAppointment';
DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
DECLARE @maxSourceIdProcessed INT = NULL;
DECLARE @StartTime DATETIME = GETUTCDATE();
DECLARE @EndTime DATETIME = NULL;
DECLARE @InsertedRecord INT=0;
DECLARE @UpdatedRecord INT=0;
DECLARE @Comment VARCHAR(400) = '';
DECLARE @ErrorNumber INT =0;
DECLARE @ErrorState INT =0;
DECLARE @ErrorSeverity INT=0;
DECLARE @ErrorLine INT=0;
DECLARE @RecordCountBefore INT=0;
DECLARE @RecordCountAfter  INT=0;
DECLARE @RecordCountSource INT=0;
DECLARE @RowCount INT =0;
DECLARE @NumRows INT = 0;
DECLARE @lastIndex INT = 0;
DECLARE @maxIndex INT = 0;
DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);


DECLARE @Today date=GETUTCDATE();

  			
   BEGIN TRY	
	


		IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] =@EDWName)
		BEGIN
				EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed
		END


		--1) Retrieve the last run time id values.  We are going to pull both the date and the last run time id. If the last max id 
		--is null, calculate a best guese based on the 
		--We are changing away from using date values for query purposes as it is more efficient to query by the Id value.  Since
		--the id is a seeded, integer greater id values are implicitly newer. It also allows us to pick up records that come in while the 
		--procedure is running the next time it runs
		IF @fullLoadFlag = 1
		BEGIN
			UPDATE [Maintenance].[dbo].[EDWTableLoadTracking] 
				SET [maxSourceTimeStampProcessed] = CAST('1900-01-01' AS DATETIME), maxSourceIdProcessed= 0
				WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface' AND [EDWName] =@EDWName
		END

		 SET @lastDateTime = (SELECT COALESCE([maxSourceTimeStampProcessed],CAST('1900-01-01' AS DATETIME)) 
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);
	
	
		
		SET  @lastIndex = (SELECT maxSourceIdProcessed
									FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
									WHERE [EDWtableName] = @EDWtableName 
									AND [dataSource] ='interface' AND [EDWName] =@EDWName);

		IF @lastIndex IS NULL 
		BEGIN
			SET @lastIndex= (SELECT MIN(InterfacePatientAppointmentId) FROM  vwportal_InterfacePatientAppointment IPV WITH (NOLOCK) WHERE IPV.CreateDateTime >= @lastDateTime ) 
		END
		IF @overrideMinIndex IS NOT NULL
		BEGIN
			SET @lastIndex= @overrideMinIndex
		END


		SET @maxIndex = (SELECT MAX(InterfacePatientAppointmentId) FROM  vwportal_InterfacePatientAppointment IPV WITH (NOLOCK));
	

	


			--2) Get the full list of patient identifiers that we are going to be dealing with
		--We are going to breaking this up into batches along patient id lines.
		SELECT DISTINCT IP.PatientIdentifier, IP.InterfaceSystemId 
		INTO #PatientAppointmentQuePatientIdentifier 
		FROM [dbo].vwportal_InterfacePatientAppointment IPV with (nolock),
				[dbo].[vwPortal_InterfacePatient] IP with (nolock), 
				 [dbo].[vwPortal_InterfaceSystem]  ISS with (nolock) 
				WHERE ( IPV.InterfacePatientAppointmentId >= @lastIndex  AND IPV.InterfacePatientAppointmentId <= @maxIndex )
				AND ( ( @InterfacePatientId IS NULL )  OR ( @InterfacePatientId = IP.InterfacePatientId))
				AND isnull(IPV.[DeleteInd],0) <>  1  AND IPV.InterfacePatientId = IP.InterfacePatientId  
				AND IP.InterfaceSystemId = ISS.[InterfaceSystemId] AND IP.PatientIdentifier IS NOT NULL
	  


		--- Truncate data if we are doing a full load
		IF (@fullLoadFlag=1)
		BEGIN
			Truncate table [dbo].[PatientAppointment];
		END


		SELECT @RecordCountBefore=COUNT(1)
			FROM [dbo].PatientAppointment;





		--3) Loop throught all of the batches to improve memory efficency
		WHILE 1 = 1
		BEGIN
    			SELECT TOP (@BatchSize) PatientIdentifier, InterfaceSystemId
				 INTO #PatientAppointmentQuePatientIdentifieBATCH 
					FROM #PatientAppointmentQuePatientIdentifier;
				Create  NONCLUSTERED INDEX IDX_PatientAppointmentQuePatientIdentifieBATCH_PatientIdentifier
				 ON #PatientAppointmentQuePatientIdentifieBATCH (PatientIdentifier);
			
    			SET @NUMROWS = ( SELECT COUNT(1) FROM #PatientAppointmentQuePatientIdentifieBATCH)

				IF @NUMROWS <> 0 
				BEGIN
	 
    
				 -- load appointment data into temp table
				  SELECT 
					   [InterfacePatientAppointmentId]
					  ,idRef.LbPatientId AS PatientID
					  ,ISS.SourceSystemId
					  ,[SourceAppointmentId]
					  ,[AppointmentDate]
					  ,[BeginTime]
					  ,[EndTime]
					  ,[Duration]
					  ,[AppointmentResource]
					  ,[AppointmentReason]
					  ,LTRIM(RTRIM(ISNULL([AppointmentType] ,''))) AS [AppointmentType]
					  ,[AppointmentDetail]
					  ,[ProviderId] AS ProviderNPI
					  ,[ClinicianName]
					  ,ISNULL([CancelInd],0) CancelInd
					  ,t1.[CreateDateTime]
					  ,[LocationIdentifier]
					  ,ISNULL(t1.[DeleteInd],0) DeleteInd
					  ,[WorkflowStatus]
					  ,[WorkflowRoom]
					  ,IP.[PatientIdentifier]
					  ,[AppointmentKeptInd]
					  ,t1.[ProcessedInd]
				  INTO #PatientAppointmentQue  
				  FROM
						[dbo].[vwPortal_InterfacePatientAppointment] t1 WITH(NOLOCK),
						[dbo].[vwPortal_InterfaceSystem]  ISS WITH(NOLOCK),
						[dbo].[vwPortal_InterfacePatient] IP WITH(NOLOCK),   
						dbo.PatientIdReference idRef WITH(NOLOCK),
						#PatientAppointmentQuePatientIdentifieBATCH batch
				  WHERE 
					(t1.InterfacePatientAppointmentId >= @lastIndex  AND t1.InterfacePatientAppointmentId <= @maxIndex)
					AND t1.InterfacePatientId = IP.InterfacePatientId AND ip.InterfaceSystemId = ISS.[InterfaceSystemId]
					AND idRef.LbPatientId IS NOT NULL AND
					idRef.SourceSystemId = ISS.SourceSystemId AND idRef.ExternalId = IP.PatientIdentifier 
					AND idRef.IdTypeDesc ='InterfacePatientIdentifier'	AND idRef.DeleteInd =0
					 AND IP.PatientIdentifier = batch.PatientIdentifier
					 AND IP.InterfaceSystemId = batch.InterfaceSystemId	  AND isnull(t1.[DeleteInd],0) <>  1
	  
PRINT 'Que Count'
DECLARE @CNT INT =(SELECT COUNT(1) FROM #PatientAppointmentQue)
PRINT @CNT




	  
				 CREATE NONCLUSTERED INDEX NCIX_#patientAppointmenQue_AppointmentType ON #PatientAppointmentQue(AppointmentType) 


	   
					----AppointmentType -- Megere in the appointment type tables
					MERGE INTO [dbo].AppointmentType AS target
					USING (SELECT distinct AppointmentType
							FROM #PatientAppointmentQue  
							where ISNULL(AppointmentType,'') <> ''  ) AS source
					ON source.AppointmentType = target.[Name]
					WHEN NOT MATCHED BY TARGET THEN
						INSERT ([Name],DisplayValue,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId)
						VALUES (source.[AppointmentType],source.[AppointmentType],0,@Today,@Today,1,1);




					SELECT *
					INTO #PatientAppointment
					FROM
					(
					  SELECT  [PatientID]
							,SourceSystemId
							,[SourceAppointmentId]
							,[AppointmentDate]
							,[BeginTime]
							,ISNULL(EndTime,'') AS [EndTime]
							,[Duration]
							,[AppointmentResource]
							,[AppointmentReason]
							,ISNULL([AppointmentTypeId],0) AS [AppointmentTypeId]
							,[AppointmentDetail]
							,[ProviderNPI]
							,[ClinicianName]
							,[CancelInd]
							,t1.[CreateDateTime]
							,[LocationIdentifier]
							,t1.[DeleteInd]
							,[WorkflowStatus]
							,[WorkflowRoom]
							,t1.[PatientIdentifier]
							,[AppointmentKeptInd]
							,t1.[ProcessedInd]
							,ROW_NUMBER() OVER(PARTITION BY SourceSystemId,PatientID, ISNULL([AppointmentTypeId],0), AppointmentDate,BeginTime,ISNULL(EndTime,'')
							ORDER BY InterfacePatientAppointmentID DESC ) AS rowNbr
			
						 FROM #PatientAppointmentQue  t1
						 LEFT JOIN AppointmentType t2 ON t1.AppointmentType = t2.Name
						)  AS tmp
						WHERE rowNbr = 1
					
						SET @RecordCountSource = @RecordCountSource + (SELECT COUNT(1) FROM #PatientAppointment);	             


		

						
						   CREATE CLUSTERED INDEX CIX_#patientAppointment_PatientIDSrcIdAppDate
							 ON #patientAppointment([PatientID],[SourceSystemID],AppointmentTypeId,AppointmentDate,BeginTime,EndTime)
     
						 MERGE PatientAppointment T		 
						 USING #PatientAppointment S
						 ON T.[PatientID] = S.[PatientID]
						 AND T.[SourceSystemID] = S.[SourceSystemID]
						 AND T.AppointmentTypeId = S.AppointmentTypeId
						 AND T.AppointmentDate = S.AppointmentDate
						 AND T.BeginTime = S.BeginTime
						 AND isnull(T.EndTime,'') = S.EndTime
						 WHEN MATCHED 
						  THEN UPDATE SET   T.SourceAppointmentId = S.SourceAppointmentId,
											T.Duration = S.Duration,
											T.AppointmentResource = S.AppointmentResource,
											T.AppointmentReason = ISNULL(S.AppointmentReason, T.AppointmentReason),
											T.AppointmentDetail =  ISNULL(S.AppointmentDetail, T.AppointmentDetail),
											T.ProviderNPI = S.ProviderNPI,
											T.CancelInd = S.CancelInd,
											T.DeleteInd = S.DeleteInd,
											t.[AppointmentTypeId] = s.[AppointmentTypeId],
											T.[ModifyDateTime] = @Today
						 WHEN NOT MATCHED BY TARGET
						   THEN INSERT ( PatientID,SourceSystemId,SourceAppointmentId,AppointmentDate,BeginTime,EndTime,Duration,AppointmentResource,AppointmentReason,AppointmentDetail,
										 ProviderNPI, [AppointmentTypeId], CancelInd,DeleteInd,[CreateDateTime],[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
								VALUES(	S.PatientID,S.SourceSystemId,S.SourceAppointmentId,S.AppointmentDate, S.BeginTime,S.EndTime,S.[Duration],S.AppointmentResource,S.AppointmentReason,S.AppointmentDetail,S.ProviderNPI,S.[AppointmentTypeId],
										 S.CancelInd, S.DeleteInd,@Today,@Today,1,1);


							SET @RecordCountSource = @RecordCountSource + (SELECT COUNT(1) FROM #patientAppointment);	             



	
						
   							--Update the max processed time and drop the temp tables
							SET @tmpmaxSourceTimeStampProcessed = 	(SELECT MAX(CreateDateTime) FROM #PatientAppointmentQue )
							IF @tmpmaxSourceTimeStampProcessed > @maxSourceTimeStampProcessed
							BEGIN
								SET @maxSourceTimeStampProcessed = @tmpmaxSourceTimeStampProcessed
							END


						DROP TABLE #PatientAppointment
						DROP TABLE #PatientAppointmentQue

				END
		
		
			
			--Move onto the next batch
			DELETE FROM #PatientAppointmentQuePatientIdentifier WHERE PatientIdentifier IN (SELECT PatientIdentifier FROM #PatientAppointmentQuePatientIdentifieBATCH)
			DROP TABLE #PatientAppointmentQuePatientIdentifieBATCH
			IF @NUMROWS = 0 BREAK;
  
		END
		


		DROP table #PatientAppointmentQuePatientIdentifier
		
     -- get total records from [dbo].PatientPatientAppointment after merge 
		SELECT @RecordCountAfter=count(1)
		FROM [dbo].PatientAppointment;

				
               		   
		-- update maxSourceTimeStamp with max CreateDateTime of #PatientAppointmentQue
		UPDATE [Maintenance]..[EDWTableLoadTracking]
		SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex, updatedatetime = GETUTCDATE()
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


		-- get total records from PatientAppointment after merge 
		SELECT @RecordCountAfter=COUNT(1)  FROM PatientAppointment WITH (NOLOCK);

		-- Calculate records inserted and updated counts
		SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
		SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
		
	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
END

GO
