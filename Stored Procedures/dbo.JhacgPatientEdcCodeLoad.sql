SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ================================================================
-- Author:		Lan Ma
-- Create date: 2016-07-25
-- Description:	parse patient EDC codes from JhacgPatientDetail 
-- and load into normilized table
-- Modified By:
-- Modified Date:
-- ===============================================================


CREATE PROCEDURE [dbo].[JhacgPatientEdcCodeLoad]

AS
BEGIN	

SELECT DISTINCT [patient_id], edc_codes
INTO #PatientEdcCode
FROM [dbo].[JhacgPatientDetail]
WHERE ISNULL(edc_codes,'')<> ''

SELECT 
patient_id AS LbpatientId, 
t2.[EdcCode], 
t2.[MedcCode]
INTO #Jhacg_PatientEdcCode
FROM(
SELECT p.patient_id, e.data as edc_code
FROM #PatientEdcCode p
CROSS APPLY  [EdwReferenceData].[dbo].[udf_SplitByXml](edc_codes,' ')AS e
) t1
JOIN [dbo].[vwJhacgEdcCode] t2
ON t1.edc_code = t2.[EdcCode]

MERGE [dbo].[JhacgPatientEdcCode] T
USING #Jhacg_PatientEdcCode S
ON (T.LbpatientId = S.LbpatientId
    AND T.EdcCode = S.EdcCode )
WHEN MATCHED
     THEN UPDATE SET T.MedcCode = S.MedcCode

WHEN NOT MATCHED BY TARGET
	 THEN INSERT(LbpatientId, EdcCode,MedcCode) VALUES(S.LbpatientId, S.EdcCode,S.MedcCode)
WHEN NOT MATCHED BY SOURCE
    THEN DELETE
;
 
DROP TABLE  #PatientEdcCode;
DROP TABLE  #Jhacg_PatientEdcCode;

END
GO
