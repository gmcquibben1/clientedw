SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GeneratePayerCCLFViews]
@TargetTable nvarchar(MAX), @StageTable nvarchar(MAX), @PayerFeed nvarchar(MAX), @ViewName nvarchar(MAX)
WITH EXEC AS CALLER
AS
-- EXEC LoadViewer @TargetTable = 'CCLF_5_PartB_Physicians',  @StageTable = 'PayerFeedsStage_CignaClaims', @PayerFeed = 'Cigna' , @ViewName = 'uv_Cigna_CCLF5' 
-- CCLF_1_PartA_Header CCLF_2_PartA_RCDetail CCLF_4_PartA_Diagnosis CCLF_5_PartB_Physicians
-- Aetna, Bcbs, Cigna, Wellmed
-- DECLARE @TargetTable NVARCHAR(MAX) = 'CCLF_5_PartB_Physicians',  @StageTable NVARCHAR(MAX) = 'BCBSPCMH_Claim_stage',
--		@PayerFeed NVARCHAR(MAX) = 'BCBS' , @ViewName NVARCHAR(MAX) = 'uv_Bcbs_CCLF5' 

DECLARE @DynFullSQL NVARCHAR(MAX) = ' ', @DynOuterColNList NVARCHAR(MAX) = '*' , @DynOuterColDList NVARCHAR(MAX) = '',
		@DynInnerSQL NVARCHAR(MAX) = 'SELECT ' + CHAR(10), @DynJOINSQL VARCHAR(MAX) = ' ', @DynWhere NVARCHAR(MAX) = ' '
		
SELECT @DynFullSQL = ' IF (NOT EXISTS(select * FROM sys.views where name = ''' + @ViewName + ''' ))
						BEGIN EXEC (''CREATE VIEW ' + @ViewName + ' AS SELECT 1 AS X  '') END' + CHAR(10) 
EXEC sp_executesql @DynFullSQL

SELECT @DynFullSQL  =	N' ALTER VIEW ' + @ViewName + ' AS ' + CHAR(10) 
SELECT @DynInnerSQL = @DynInnerSQL + 
				 CASE WHEN cols.COLUMN_NAME IS NOT NULL THEN 
					'TRY_CONVERT(' + cols.DATA_TYPE +  CASE WHEN character_maximum_length IS NOT NULL THEN '(' + CAST(character_maximum_length AS VARCHAR(100)) + ')' 
															WHEN cols.DATA_TYPE = 'int' THEN ''
														    WHEN numeric_precision  IS NOT NULL       THEN '(' + CAST(numeric_precision AS VARCHAR(100)) + ',' + CAST(numeric_scale AS VARCHAR(100)) + ')'  
															ELSE '' END     +  ', ' + 
						CASE WHEN cm.SourceColumn IS NOT NULL THEN  REPLACE(REPLACE(REPLACE(REPLACE(dbo.usfLookupDecompose(cm.EvalExpr,3,'Y'),'SUM',''),'MIN',''),'MAX',''),
																	cm.SourceColumn,'Base.'+ cm.SourceColumn)  
						ELSE REPLACE(REPLACE(REPLACE(dbo.usfLookupDecompose(cm.EvalExpr,3,'Y'),'SUM',''),'MIN',''),'MAX','') END 
					 + ') AS ' + cm.TargetColumn  
					  WHEN cols.COLUMN_NAME IS NULL  THEN 
						REPLACE(REPLACE(REPLACE(REPLACE(dbo.usfLookupDecompose(cm.EvalExpr,3,'Y'),'SUM',''),'MIN',''),'MAX',''),
													ISNULL(cm.SourceColumn,''),'Base.'+ ISNULL(cm.SourceColumn,''))  + ' AS ' + ISNULL(cm.TargetColumn,cm.SourceColumn)
				 END + ', ' + CHAR(10) 
FROM PayerFeedsCCLFMapper cm 
LEFT JOIN INFORMATION_SCHEMA.COLUMNS cols on cols.TABLE_NAME = cm.TargetTable AND cols.COLUMN_NAME = cm.TargetColumn
WHERE cm.TargetTable = @TargetTable AND cm.EvalType = 'COLUMN' AND cm.PayerFeed = @PayerFeed
ORDER BY Cols.ORDINAL_POSITION
SELECT @DynInnerSQL
--LOOKUP(specialty_number,Test.specialty_number,Test.x) :: TYPICAL LOOKUP 
SELECT @DynJOINSQL =  @DynJOINSQL + CHAR(10) + DynJOINSQL FROM (
		SELECT DISTINCT 
				 ' LEFT JOIN ' + 
					CASE WHEN CHARINDEX('[',dbo.usfLookupDecompose(cm.EvalExpr,2,'N')) > 0 
							  THEN 
							  SUBSTRING(dbo.usfLookupDecompose(cm.EvalExpr,2,'N'),CHARINDEX('[',dbo.usfLookupDecompose(cm.EvalExpr,2,'N')) ,CHARINDEX( '.', dbo.usfLookupDecompose(cm.EvalExpr,2,'N') ) - CHARINDEX('[',dbo.usfLookupDecompose(cm.EvalExpr,2,'N')))   
						      ELSE 
						SUBSTRING(dbo.usfLookupDecompose(cm.EvalExpr,2,'N'),1,CHARINDEX( '.', dbo.usfLookupDecompose(cm.EvalExpr,2,'N') ) - 1)   
					END +
					CASE WHEN CHARINDEX('[',dbo.usfLookupDecompose(cm.EvalExpr,1,'N')) > 0 
							  THEN
									' ON ' +  REPLACE(SUBSTRING(dbo.usfLookupDecompose(cm.EvalExpr,1,'N'),1,1000),'[','BASE.[')  
							  ELSE
								' ON BASE.' + SUBSTRING(dbo.usfLookupDecompose(cm.EvalExpr,1,'N'),1,1000)  
					END +				  
				   ' = ' + 
						SUBSTRING(dbo.usfLookupDecompose(cm.EvalExpr,2,'N'),1,1000)  
				AS DynJOINSQL
FROM PayerFeedsCCLFMapper cm 
WHERE cm.TargetTable = @TargetTable AND cm.EvalExpr LIKE '%LOOKUP(%' AND cm.PayerFeed = @PayerFeed 
									) X 
--SELECT @DynJOINSQL 

SELECT @DynInnerSQL = LEFT(@DynInnerSQL,LEN(@DynInnerSQL) - 3) + CHAR(10) +  ' FROM ' + @StageTable + ' BASE' + @DynJOINSQL -- CUT OFF

SELECT  @DynWhere =  @DynWhere +   cm.EvalExpr  + CHAR(10) 
FROM PayerFeedsCCLFMapper cm 
WHERE cm.TargetTable = @TargetTable AND cm.EvalType = 'FILTERS' AND cm.PayerFeed = @PayerFeed

SELECT @DynOuterColNList = ''
SELECT  @DynOuterColNList =  @DynOuterColNList -- + ISNULL(cm.TargetColumn,cm.SourceColumn) + ',' +  CHAR(10) 
								+ CASE 
									WHEN (cm.EvalExpr LIKE 'MIN(%' OR cm.EvalExpr LIKE 'MAX(%' OR cm.EvalExpr LIKE 'SUM(%' ) 
									THEN
										--REPLACE(cm.EvalExpr,cm.SourceColumn,cm.TargetColumn)  + ' AS ' + ISNULL(cm.TargetColumn,cm.SourceColumn) + ','
										 SUBSTRING(cm.EvalExpr,1,CHARINDEX( '(', cm.EvalExpr) - 1) + '('
										 + ISNULL(cm.TargetColumn,cm.SourceColumn) + ') AS ' + ISNULL(cm.TargetColumn,cm.SourceColumn)   + ' ,' 
									ELSE
										 ISNULL(cm.TargetColumn,cm.SourceColumn) + ' ,'
									END
									
									-- +  CHAR(10) 
FROM PayerFeedsCCLFMapper cm 
INNER JOIN INFORMATION_SCHEMA.COLUMNS cols on cols.TABLE_NAME = cm.TargetTable AND cols.COLUMN_NAME = cm.TargetColumn
WHERE cm.TargetTable = @TargetTable AND cm.EvalType = 'COLUMN' AND cm.PayerFeed = @PayerFeed
ORDER BY cols.ORDINAL_POSITION
SELECT @DynOuterColNList = LEFT(@DynOuterColNList,LEN(@DynOuterColNList) - 2) -- CUT OFF
--SELECT @DynOuterColNList

IF (NOT EXISTS(select * FROM PayerFeedsCCLFMapper cm  WHERE cm.TargetTable = @TargetTable AND cm.EvalType = 'COLUMN' AND cm.PayerFeed = @PayerFeed 
						AND (cm.EvalExpr LIKE 'MIN(%' OR cm.EvalExpr LIKE 'MAX(%' OR cm.EvalExpr LIKE 'SUM(%' ) ))
BEGIN  
	SELECT @DynOuterColDList = ''
END
ELSE
BEGIN
	SELECT @DynOuterColDList = ' GROUP BY '
	SELECT  @DynOuterColDList =  @DynOuterColDList + CASE 
									WHEN (cm.EvalExpr LIKE 'MIN(%' OR cm.EvalExpr LIKE 'MAX(%' OR cm.EvalExpr LIKE 'SUM(%' ) 
									THEN
										''
									ELSE
										 ISNULL(cm.TargetColumn,cm.SourceColumn) + ',' +  CHAR(10) 
									END
	FROM PayerFeedsCCLFMapper cm 
	INNER JOIN INFORMATION_SCHEMA.COLUMNS cols on cols.TABLE_NAME = cm.TargetTable AND cols.COLUMN_NAME = cm.TargetColumn
	WHERE cm.TargetTable = @TargetTable AND cm.EvalType = 'COLUMN' AND cm.PayerFeed = @PayerFeed
	SELECT @DynOuterColDList = LEFT(@DynOuterColDList,LEN(@DynOuterColDList) - 2) -- CUT OFF
END 
--SELECT @DynOuterColDList

SELECT @DynFullSQL = @DynFullSQL + N' SELECT ' + CHAR(10) + @DynOuterColNList + CHAR(10) + N'  FROM (' + CHAR(10) + @DynInnerSQL + CHAR(10) + ' ) X ' +  CHAR(10) +  @DynWhere +  CHAR(10) +  @DynOuterColDList
SELECT @DynFullSQL
EXEC sp_executesql  @DynFullSQL
GO
