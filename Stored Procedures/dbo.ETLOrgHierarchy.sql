SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[ETLOrgHierarchy] 
AS
BEGIN


SET NOCOUNT ON;
  /*===============================================================
	CREATED BY: 	Youping
	CREATED ON:		2016
	INITIAL VER:	2.0.1
	MODULE:			ETL
	DESCRIPTION:	This stored procedure will populate the org hierarchy  table
	PARAMETERS:		
		 @rebuildAll  Setting this value to 1 will rebuild the entire table as opposed to only differential records

	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	            2016-06-01								Modification: Add up to 4 level of Id and IDtype description
	2.1.2	    2017-01-20		CJL			LBETL-531	Added trim statements to the id values, added logging and instrumentation
	2.2.1		2016-12-30	    YL		LBETL-393 add procedure run log to OrgHierarchy related procedures
	2.2.1		2017-01-03	    YL		expand Level1Id to Level6Id from varchar(30) to varchar(50)
	                                    expand Level1TypeName to Level6TypeName from varchar(30) to varchar(100)
    2.3.1       2017-04-07      YL          LBETL-1174  Eliminate Kill and Fill
=================================================================*/
	SET NOCOUNT ON

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
		DECLARE @EDWtableName VARCHAR(100) = 'orgHierarchy';
		DECLARE @dataSource VARCHAR(20) = 'interface';
		DECLARE @ProcedureName VARCHAR(128) = 'ETLOrgHierarchy';
		DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
		DECLARE @maxSourceIdProcessed INT = NULL;
		DECLARE @StartTime DATETIME = GETUTCDATE();
		DECLARE @EndTime DATETIME = NULL;
		DECLARE @InsertedRecord INT=0;
		DECLARE @UpdatedRecord INT=0;
		DECLARE @Comment VARCHAR(400) = '';
		DECLARE @ErrorNumber INT =0;
		DECLARE @ErrorState INT =0;
		DECLARE @ErrorSeverity INT=0;
		DECLARE @ErrorLine INT=0;
		DECLARE @RecordCountBefore INT=0;
		DECLARE @RecordCountAfter  INT=0;
		DECLARE @RecordCountSource INT=0;

		DECLARE @Today DATE=GETUTCDATE();


		
	BEGIN TRY


			--set @fullLoadFlag =1
	
			IF NOT EXISTS ( SELECT 1 FROM [Maintenance].[dbo].[EDWTableLoadTracking] 
					WHERE [EDWtableName] = @EDWtableName AND [dataSource] = 'interface' AND [EDWName] =@EDWName)
			  BEGIN
			  
					 EXEC [Maintenance].[dbo].[EDWTableLoadTrackingInsert] @EDWName,@EDWtableName,@dataSource,@maxSourceTimeStampProcessed,@maxSourceIdProcessed

			  END

			   		      -- get total records from PatientVitals after merge 
		       			SELECT @RecordCountBefore=COUNT(1)
			            FROM orgHierarchy WITH (NOLOCK);

					SELECT 
					t1.patientID as LbPatientId,
					t10.[Name] AS [State], 
					t9.City , 
					t9.[PostalCode],
					t2.[level1_buCode],
					t2.[level1_buName],
					t2.[level1_buType],
					t2.[level2_buCode],
					t2.[level2_buName],
					t2.[level2_buType],
					t2.[level3_buCode],
					t2.[level3_buName],
					t2.[level3_buType],
					t2.[level4_buCode],
					t2.[level4_buName],
					t2.[level4_buType],
					t2.[level5_buCode],
					t2.[level5_buName],
					t2.[level5_buType],
					t2.[level6_buCode],
					t2.[level6_buName],
					t2.[level6_buType],
					--CASE WHEN t1.sourceFeed = 'interface' then 'NoContract' else sourceFeed END,
					--NULL,
					LTRIM(RTRIM(t11.[ContractName]))  ContractName,
					t1.medicareNo,
					LTRIM(RTRIM(t6.lastName)) + ', ' + LTRIM(RTRIM(t6.firstName)) MemberFullName,
					--NULL,
					--NULL,
					--t1.medicareNo,
					--'Active (WithClaims)',
					t11.[ContractStatus] + ' (' + [ClaimStatus] + ')' Patient_Status,
					COALESCE(t3.ProviderID,0) AS providerID,
					COALESCE(LTRIM(RTRIM(t4.[FirstName]))  + ' ' + LTRIM(RTRIM(t4.[LastName])), 'unattributed') AS ProviderName,
					COALESCE(t3.[NationalProviderIdentifier], '9999999999') AS ProviderNPI,
					t7.BirthDate, 
					t7.[GenderTypeID], 
					GETDATE() LastUpdatedTimestamp 
					INTO #OrgHierarchy
					FROM [dbo].[patientProviderOrgHierarchy] t1
					JOIN [dbo].[orgHierarchyStructure] t2
					ON t1.[BusinessUnitId] = t2.[buId]
					LEFT JOIN [dbo].[vwPortal_Provider] t3
					ON t1.ProviderID = t3.ProviderID
					AND t3.[DeleteInd] = 0
					LEFT JOIN [dbo].[vwPortal_IndividualName] t4
					ON t3.[IndividualID] = t4.IndividualID
					AND t4.DeleteInd = 0
					LEFT JOIN [dbo].[vwPortal_Patient] t5
					ON t1.PatientID = t5.PatientId
					AND t5.[DeleteInd] = 0
					Left JOIN [dbo].[vwPortal_IndividualName] t6
					ON t5.[IndividualID] = t6.IndividualID
					--and t6.DeleteInd = 0
					LEFT JOIN [dbo].[vwPortal_Individual] t7
					ON t5.IndividualID = t7.IndividualID
					--And t7.DeleteInd = 0
					--LEFT JOIN [dbo].[vwPortal_IndividualStreetAddress] t8
					--ON t7.IndividualID = t8.IndividualID

					LEFT JOIN 
					 (SELECT IndividualID, MAX(StreetAddressID) AS StreetAddressID
					 FROM [dbo].[vwPortal_IndividualStreetAddress]
					 WHERE DeleteInd = 0 
					 GROUP BY IndividualID ) t8
					 ON t7.IndividualID = t8.IndividualID

					--And t8.DeleteInd = 0
					Left JOIN [dbo].[vwPortal_StreetAddress] t9
					ON t8.StreetAddressID = t9.StreetAddressID
					--And t9.DeleteInd = 0
					LEFT JOIN [dbo].[vwPortal_TerritoryType] t10
					ON t9.[TerritoryTypeID] = t10.[TerritoryTypeID]
					LEFT JOIN [dbo].[PatientContract] t11
					ON t1.[PatientID] = t11.[LbPatientId]
					AND [IsCurrentInd] = 1

					--UPDATE O SET BENE_HIC_NUM = IDR.ExternalId  FROM OrgHierarchy O
					--JOIN PatientIdReference IDR ON O.LbPatientId = IDR.LbPatientId

					 CREATE CLUSTERED INDEX IDX_PatientID_OrgHierarchy ON #OrgHierarchy (LbPatientId);



					----get up to 4 level 
					Create table #IdTypeDesc (
					ID int identity(1,1),
					IdTypeDesc Varchar(100)

					);



					DECLARE @IdTypeValue varchar(200)
					SET  @IdTypeValue=( SELECT TOP 1 [SetValue]  FROM vwPortal_SystemSettings WHERE [SettingType]='Patient');

					DECLARE @IdTypeDescCnt int
					select @IdTypeDescCnt =1+len(@IdTypeValue)- len(replace(@IdTypeValue,',',''));

					---select  @IdTypeValue,@IdTypeDescCnt;
					DECLARE @I INT=1;

					WHILE @I<= @IdTypeDescCnt
					BEGIN
					   INSERT INTO #IdTypeDesc (IdTypeDesc)
					   SELECT dbo.UFN_SEPARATES_COLUMNS(@IdTypeValue,@I,',');
					   SET @I=@I+1;
					END
	
					   SELECT  DISTINCT  a.LbPatientId,a.[ExternalId],left(a.IdTypeDesc + ' - '+s.Name,125) IdTypeDesc, b.ID as sortvalue
					   INTO #tmpPatientIdType
						  FROM [dbo].[PatientIdReference] a
						  INNER JOIN #IdTypeDesc b on a.IdTypeDesc=LTRIM(b.IdTypeDesc)
						  INNER JOIN [dbo].[SourceSystem] s on s.SourceSystemId=a.SourceSystemId


					  SELECT LbPatientId,[ExternalId],IdTypeDesc, RowNbr
					  INTO #PatientIdType
					  FROM (
					  SELECT LbPatientId,[ExternalId],IdTypeDesc,sortvalue, row_number() OVER(PARTITION BY LbPatientID ORDER BY sortvalue ) AS RowNbr
					  FROM #tmpPatientIdType
					  ) a
					  WHERE a.RowNbr<=4;

					  CREATE CLUSTERED INDEX IDX_PatientIdType ON #PatientIdType (LbPatientId,RowNbr);


					  SELECT a.LbPatientId, a.ExternalId,a.IdTypeDesc
					  , b.ExternalId AS ExternalId2 ,b.IdTypeDesc AS IdTypeDesc2
					  , c.ExternalId AS ExternalId3 ,c.IdTypeDesc AS IdTypeDesc3
					  , d.ExternalId AS ExternalId4 ,d.IdTypeDesc AS IdTypeDesc4
					  INTO #PatientIdTypeQue
					  FROM #PatientIdType a
					  left outer join #PatientIdType b on b.LbPatientId=a.LbPatientId and b.rownbr=2 and  a.RowNbr=1
					  Left outer join #PatientIdType c on c.LbPatientId=a.LbPatientId and c.rownbr=3 and  a.RowNbr=1
					  Left outer join #PatientIdType d on d.LbPatientId=a.LbPatientId and d.rownbr=4 and  a.RowNbr=1
					  WHERE a.RowNbr=1

					  CREATE CLUSTERED INDEX IDX_PatientIdType ON #PatientIdTypeQue (LbPatientId);


				--TRUNCATE TABLE OrgHierarchy

				MERGE [dbo].[OrgHierarchy] AS T
				USING ( SELECT o.[State], o.City , o.[PostalCode],
						LEFT(o.[level1_buCode], 30) [Level1Id] , LEFT(o.[level1_buName], 100) [Level1Name], LEFT(o.[level1_buType], 30) [Level1TypeName],
						LEFT(o.[level2_buCode], 30) [Level2Id] , LEFT(o.[level2_buName], 100) [Level2Name], LEFT(o.[level2_buType], 30) [Level2TypeName],
						LEFT(o.[level3_buCode], 30) [Level3Id] , LEFT(o.[level3_buName], 100) [Level3Name], LEFT(o.[level3_buType], 30) [Level3TypeName],
						LEFT(o.[level4_buCode], 30) [Level4Id] , LEFT(o.[level4_buName], 100) [Level4Name], LEFT(o.[level4_buType], 30) [Level4TypeName],
						LEFT(o.[level5_buCode], 30) [Level5Id] , LEFT(o.[level5_buName], 100) [Level5Name], LEFT(o.[level5_buType], 30) [Level5TypeName],
						LEFT(o.[level6_buCode], 30) [Level6Id] , LEFT(o.[level6_buName], 100) [Level6Name], LEFT(o.[level6_buType], 30) [Level6TypeName],
						o.ContractName,o.medicareNo,o.MemberFullName
						,NULL [Member_Provider_Relationship_Effective_Date]
						,NULL [Member_Provider_Relationship_Termination_Date]
						,ISNULL(p.ExternalId,o.medicareNo) [BENE_HIC_NUM]
						,o.Patient_Status
						,o.providerID
						,o.ProviderName
						,o.ProviderNPI
						,o.BirthDate 
 						,o.[GenderTypeID] 
						,o.LastUpdatedTimestamp
						,o.LbPatientId
						,p.IdTypeDesc
						,p.ExternalId
						,p.IdTypeDesc2
						,p.ExternalId2 
						,p.IdTypeDesc3
						,p.ExternalId3 
						,p.IdTypeDesc4
						,p.ExternalId4
				  FROM #OrgHierarchy o 
				  LEFT OUTER JOIN #PatientIdTypeQue p ON o.LbPatientId=p.LbPatientId) AS S
				  ON T.LbPatientId=S.LbPatientId
				  WHEN MATCHED THEN
				  UPDATE SET  T.[State] = S.[State]
							,T.[City] =S.[City] 
                            ,T.[Zip] = S.PostalCode
                           ,T.[Level1Id] = S.Level1Id
                           ,T.[Level1Name] = S.Level1Name
                           ,T.[Level1TypeName] =S.Level1TypeName
                           ,T.[Level2Id] = S.Level2Id
                           ,T.[Level2Name] = S.Level2Name
                           ,T.[Level2TypeName] =S.Level2TypeName
                           ,T.[Level3Id] = S.Level3Id
                           ,T.[Level3Name] = S.Level3Name
                           ,T.[Level3TypeName] =S.Level3TypeName
                           ,T.[Level4Id] = S.Level4Id
                           ,T.[Level4Name] = S.Level4Name
                           ,T.[Level4TypeName] =S.Level4TypeName
                           ,T.[Level5Id] = S.Level5Id
                           ,T.[Level5Name] = S.Level5Name
                           ,T.[Level5TypeName] =S.Level5TypeName
                           ,T.[Level6Id] = S.Level6Id
                           ,T.[Level6Name] = S.Level6Name
                           ,T.[Level6TypeName] =S.Level6TypeName
						   ,T.[ContractName] = S.ContractName
						   ,T.[MemberID] = S.medicareNo
                           ,T.[MemberFullName] = S.MemberFullName
                           ,T.[Member_Provider_Relationship_Effective_Date] = S.Member_Provider_Relationship_Effective_Date
                           ,T.[Member_Provider_Relationship_Termination_Date] =S.Member_Provider_Relationship_Termination_Date
                           ,T.[BENE_HIC_NUM] = S.[BENE_HIC_NUM]
						   ,T.[Patient_Status]=S.[Patient_Status]
                           ,T.[ProviderId] = S.ProviderId
                           ,T.[ProviderName] = S.ProviderName
                           ,T.[ProviderNPI] = S.ProviderNPI
                           ,T.[BirthDate] = S.BirthDate
                           ,T.[GenderId] = S.[GenderTypeID]
                           ,T.[LastUpdatedTimestamp] = S.LastUpdatedTimestamp
	                       ,T.[PatientId1TypeName] = S.IdTypeDesc
                           ,T.[PatientId1] = S.ExternalId
	                       ,T.[PatientId2TypeName] = S.IdTypeDesc2
                           ,T.[PatientId2] = S.ExternalId2
	                       ,T.[PatientId3TypeName] = S.IdTypeDesc3
                           ,T.[PatientId3] = S.ExternalId3
 	                       ,T.[PatientId4TypeName] = S.IdTypeDesc4
                           ,T.[PatientId4] = S.ExternalId4
					WHEN NOT MATCHED BY Target THEN
						INSERT   ([State] ,[City] ,[Zip]
						   ,[Level1Id] ,[Level1Name],[Level1TypeName]
						   ,[Level2Id] ,[Level2Name],[Level2TypeName]
						   ,[Level3Id]  ,[Level3Name],[Level3TypeName]
						   ,[Level4Id] ,[Level4Name] ,[Level4TypeName]
						   ,Level5Id,Level5Name,Level5TypeName
						   ,Level6Id,Level6Name,Level6TypeName
						   ,[ContractName]
						   ,[MemberID]
						   ,[MemberFullName]
						   ,[Member_Provider_Relationship_Effective_Date]
						   ,[Member_Provider_Relationship_Termination_Date]
						   ,[BENE_HIC_NUM]
						   ,[Patient_Status]
						   ,[ProviderId]
						   ,[ProviderName]
						   ,[ProviderNPI]
						   ,[BirthDate]
						   ,[GenderId]
						   ,[LastUpdatedTimestamp]
						   ,[LbPatientId]
						   ,[PatientId1TypeName]
						   ,[PatientId1]
						   ,[PatientId2TypeName]
						   ,[PatientId2]
						   ,[PatientId3TypeName]
						   ,[PatientId3]
						   ,[PatientId4TypeName]
						   ,[PatientId4])
						   VALUES ([State], City , [PostalCode],
						[Level1Id], [Level1Name], Level1TypeName,
						[Level2Id], [Level2Name], Level2TypeName,
						[Level3Id], [Level3Name], Level3TypeName,
						[Level4Id], [Level4Name], Level4TypeName,
						[Level5Id], [Level5Name], Level5TypeName,
						[Level6Id], [Level6Name], Level6TypeName,
						ContractName, medicareNo, MemberFullName
						,Member_Provider_Relationship_Effective_Date,Member_Provider_Relationship_Termination_Date
						,[BENE_HIC_NUM],Patient_Status
						,providerID
						,ProviderName
						,ProviderNPI
						,BirthDate 
 						,[GenderTypeID] 
						,LastUpdatedTimestamp
						,LbPatientId
						,IdTypeDesc
						,ExternalId
						,IdTypeDesc2
						,ExternalId2 
						,IdTypeDesc3
						,ExternalId3 
						,IdTypeDesc4
						,ExternalId4)
						WHEN NOT MATCHED BY SOURCE THEN
						DELETE
						;


		    -- update lastdate
               		   
			 -- update maxSourceTimeStamp with max CreateDateTime of #PatientVitalsQue
			 UPDATE [Maintenance]..[EDWTableLoadTracking]
			 SET [maxSourceTimeStampProcessed] = null,
				 [updateDateTime] =GETUTCDATE()
			WHERE [EDWtableName] = @EDWtableName AND [dataSource] ='interface'  AND [EDWName] = @EDWName


			 		      -- get total records from PatientVitals after merge 
		       			SELECT @RecordCountAfter=COUNT(1)
			            FROM orgHierarchy WITH (NOLOCK);

						SET @RecordCountSource = @RecordCountAfter 
              -- Calculate records inserted and updated counts
	             SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
	             SET @UpdatedRecord = @RecordCountSource - @InsertedRecord

              -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			   SET  @EndTime=GETUTCDATE();
			   EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	

END TRY

		BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
END

GO
