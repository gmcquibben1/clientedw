SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE  PROCEDURE [dbo].[ETLProviderExtract] @MaxRecordsToProcess INT=-1,
@DEBUG INT=2
AS
BEGIN



/*===============================================================
	CREATED BY: 	SG
	CREATED ON:		2016-10-31
	INITIAL VER:	Initial Application Version
	MODULE:			EDW ETL		
	DESCRIPTION:	ETL SP to create new Providers.  This procedure will extract records from the ProviderExtract table and create the 
					Corresponding records int the Provider, Individual, LBUserPhone, etc.... 
	PARAMETERS:		
					@MaxRecordsToProcess INT=-1  Maximum number of records to processs, -1 = no limit
					@DEBUG INT=2				 If this value is set to 1 then the stored procedure will print debug statements during execution

	RETURN VALUE(s)/OUTPUT:	NONE
	
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.0		    2016-10-31  	SG		    PROV-1355	Add code to fix the provider SP failure
	2.0.1       2016-11-21      CJL			PB-2377		Removed constraint on the LBPhoneUser insert SQL to prevent multiple phone records being created
														The portal UI only supports a single phone number for the time being

	2.0.1       2016-11-21      CJL			LEBTL-611	Added Youping's Auditing Code, hardened routine against issues in which the telephone id and address id values are null in the claims table
	2.2.1       2017-02-13      CJL			LBETL-766	Addressed issue with Princeton Provider Extract,  there was an issue with the External id that was causing a recursive join issue.  Can only join on the the provider id
	2.2.1       2017-02-16      CJL			LBETL-879	Providers with NPI 999999999 shoudl be excluded from importation from the interface table

														
=================================================================*/


--THESE MOVE TO THE parms
--DECLARE @MaxRecordsToProcess int=4
--DECLARE @Debug int=1


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'InterfaceProvider';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLInterfaceProvider';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @Today date=GETUTCDATE();
	DECLARE @RowCount INT =0;
	DECLARE @NumRows INT = 0;
	DECLARE @BatchSize INT = 10000;
 	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);



	BEGIN TRY


DECLARE @LBUSERID            INT=1
DECLARE @CurrentDateTime     DATETIME=GETDATE()
DECLARE @DeleteInd           BIT=0
DECLARE @PartyDescription    VARCHAR(50)='Patient'
DECLARE @PartyStartDateTime  DATETIME=GETDATE()
DECLARE @PartyEndDateTime    DATETIME=NULL
DECLARE @PartyTypeID         INT=3--Provider
DECLARE @SourceSystemID      INT=1--Internal Feeds
DECLARE @ProviderTypeID      INT=0--UNKNOWN
DECLARE @ProviderRoleTypeID INT=0 --UNKNOWN
DECLARE @EducationDegreeTypeID INT=0 --UNKNOWN
DECLARE @ProviderSpecialtyTypeID INT=0 --UNKNOWN
DECLARE @ProviderStatusTypeID INT=0 --UNKNOWN


--lbUSER FIELDS
DECLARE @ModuleId INT=1--not saved
DECLARE @PersonTitleTypeId INT=0--UNKNOWN
DECLARE @ActiveInd bit=0--not saved
DECLARE @SecondaryEmailAddress VARCHAR(8000)=NULL--
DECLARE @USERNAME VARCHAR(8000)=NULL--not saved

--source table fields
DECLARE @ProviderEffDate                   DATE--party
DECLARE @ProviderTermDate                  DATE--party
DECLARE @DOB                               DATE--individual
DECLARE @PrimarySpecialtyStartDate         DATE--not saved
DECLARE @PrimarySpecialtyEndDate      DATE--not saved
DECLARE @SecondarySpecialtyStartDate  DATE--not saved
DECLARE @SecondarySpecialtyEndDate         DATE--not saved
DECLARE @fromFile                          DATE--not saved
DECLARE @ID                                     INT--not saved
DECLARE @PublicationDTM                    datetime--not saved
DECLARE @extProviderID                     VARCHAR(8000)--ExternalReferenceIdentifier
DECLARE @PrimarySpecialty                  VARCHAR(8000)--not saved --TODO: this is a coded value, so we need to load the code mapping
DECLARE @SecondarySpecialty                VARCHAR(8000)--not saved
DECLARE @ProviderFirstName                 VARCHAR(8000)--individualname
DECLARE @ProviderLastName                  VARCHAR(8000)--individualname
DECLARE @ProviderMidInit                   VARCHAR(8000)--individualname
DECLARE @IPA                               VARCHAR(8000)--not saved
DECLARE @ProviderStatus                    VARCHAR(8000)--not saved  --TODO:  Value seems to be 1 in most cases.  Is this active?
DECLARE @TIN                               VARCHAR(8000)--not saved
DECLARE @ProviderNPI                       VARCHAR(8000)--provider
DECLARE @Gender                                 VARCHAR(8000)--individual
DECLARE @Email                             VARCHAR(8000)--not saved
DECLARE @ACO                               VARCHAR(8000)--not saved


DECLARE @IndividualID   INT=0
DECLARE @PartyID        INT=0
DECLARE @IndividualNameID INT=0
DECLARE @ProviderID     INT=0

DECLARE @PreviousID INT=0

DECLARE @RecordsProcessed INT=0
DECLARE @RecordsToProcess INT=0

SELECT @RecordsToProcess=COUNT(*)     
FROM ProviderExtract me LEFT JOIN vwPortal_Provider i ON me.ProviderNPI = i.NationalProviderIdentifier
WHERE I.IndividualID IS NULL

--Set it to process all
IF @MaxRecordsToProcess=-1 BEGIN SET @MaxRecordsToProcess = @RecordsToProcess END
PRINT 'Number of Records to Process=' + CAST(@RecordsToProcess AS VARCHAR(20)) 
--Setup list to process
     SELECT TOP (@MaxRecordsToProcess)
          [ID]
     INTO #T
          FROM (SELECT *, ROW_NUMBER() OVER (PARTITION BY PROVIDERNPI ORDER BY PROVIDERNPI) SNO FROM  ProviderExtract ) pe 
          LEFT JOIN vwPortal_Provider i ON pe.ProviderNPI = i.NationalProviderIdentifier
     WHERE I.IndividualID IS NULL AND SNO = 1 
     ORDER BY [ID]

SET @ID=0
WHILE(1=1 AND (@RecordsProcessed<@MaxRecordsToProcess OR @RecordsToProcess=-1))
BEGIN
     SELECT TOP 1 
          @ID=[ID]
     FROM #T
     WHERE [ID]>@PreviousID
     ORDER BY [ID]

     SELECT TOP 1 
     @ProviderEffDate=ProviderEffDate,@ProviderTermDate=ProviderTermDate,@DOB=DOB,@PrimarySpecialtyStartDate=PrimarySpecialtyStartDate,@PrimarySpecialtyEndDate=PrimarySpecialtyEndDate,@SecondarySpecialtyStartDate=SecondarySpecialtyStartDate,
	 @SecondarySpecialtyEndDate=SecondarySpecialtyEndDate,@fromFile=fromFile,@ID=ID,@PublicationDTM=NULL,@extProviderID=pe.ProviderID,@PrimarySpecialty=PrimarySpecialty,@SecondarySpecialty=SecondarySpecialty, 
	 @ProviderFirstName=ProviderFirstName,@ProviderLastName=ProviderLastName,@ProviderMidInit=ProviderMidInit,@IPA=IPA,@ProviderStatus=ProviderStatus,@TIN=TIN,@ProviderNPI=ProviderNPI,@Gender=Gender,@Email=Email,@ACO=ACO
     FROM (SELECT *, ROW_NUMBER() OVER (PARTITION BY PROVIDERNPI ORDER BY PROVIDERNPI) SNO FROM  ProviderExtract ) pe 
     WHERE 
     [ID]=@ID
     AND SNO = 1
     ORDER BY [ID]

     IF @ID = @PreviousID BEGIN BREAK END
     IF @DEBUG=1 OR @DEBUG=2 BEGIN PRINT 'ProviderExtract Current Record: ' + CAST(@ID AS VARCHAR(20)) + '-' + CAST(getdate() AS VARCHAR(50)) END

     BEGIN TRY
     BEGIN TRAN

          INSERT INTO vwPortal_Party
          (PartyTypeID,SourceSystemID,PartyStartDateTime,PartyEndDateTime,PartyDescription,ExternalReferenceIdentifier,OtherReferenceIdentifier
          ,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId
          )
          SELECT
          @PartyTypeID,@SourceSystemID,@ProviderEffDate,@ProviderTermDate,'Provider',@extProviderID,@ProviderNPI
          ,@DeleteInd,@CurrentDateTime,@CurrentDateTime,@LBUSERID,@LBUSERID
     
          SELECT @PartyID=SCOPE_IDENTITY()
          IF @DEBUG=1 BEGIN PRINT '    ProviderExtract PartyID for extProviderID(' + CAST(@extProviderID AS VARCHAR(20)) + '): ' + CAST(@PartyID AS VARCHAR(20)) END

          INSERT INTO vwPortal_Individual
          (BirthDate,SourceSystemID,PartyID,PartyTypeID,MaritalStatusTypeID,GenderTypeID,ReligionTypeID,TobaccoTypeID,ExternalReferenceIdentifier,OtherReferenceIdentifier,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId
          )
          SELECT @DOB,@SourceSystemID,@PartyID,@PartyTypeID
          ,NULL --NO STATUS IN FILE
          ,CASE @Gender WHEN 'M' THEN 1 WHEN 'F' THEN 2 ELSE NULL END,NULL,NULL,@extProviderID,@ProviderNPI
          ,@DeleteInd,@CurrentDateTime,@CurrentDateTime,@LBUSERID,@LBUSERID

          SELECT @IndividualID=SCOPE_IDENTITY()

          IF @DEBUG=1 BEGIN PRINT '    ProviderExtract IndividualID for extProviderID(' + CAST(@extProviderID AS VARCHAR(20)) + '): ' + CAST(@IndividualID AS VARCHAR(20)) END

          INSERT INTO vwPortal_IndividualName
          (IndividualID,SourceSystemID,FirstName,MiddleName,LastName,NamePrefix,NameSuffix,ExternalReferenceIdentifier,OtherReferenceIdentifier
          ,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId)
          SELECT @IndividualID,@SourceSystemID,@ProviderFirstName,ISNULL(@ProviderMidInit,''),@ProviderLastName,NULL,NULL,@extProviderID,NULL
          ,@DeleteInd,@CurrentDateTime,@CurrentDateTime,@LBUSERID,@LBUSERID

          SET @IndividualNameID=SCOPE_IDENTITY()

          IF @DEBUG=1 BEGIN PRINT '    ProviderExtract IndividualNameID for extProviderID(' + CAST(@extProviderID AS VARCHAR(20)) + '): ' + CAST(@IndividualNameID AS VARCHAR(20)) END
          --Now, create the user
          DECLARE @createLBUserID INT=0

          IF EXISTS(SELECT 1 FROM vwPortal_LbUser WHERE EmailAddress=@Email)
          BEGIN
              SET @Email=LTRIM(RTRIM(@Email))+CAST(@ID AS VARCHAR(20)) --hack.  Make it unique.
          END
          INSERT INTO vwPortal_LbUser
          (ModuleId
          ,PersonTitleTypeId
          ,DateOfBirth
          ,ActiveInd
          ,FirstName
          ,LastName
          ,EmailAddress
          ,SecondaryEmailAddress
          ,USERNAME
          ,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId)
          SELECT
          @ModuleID
          ,@PersonTitleTypeID
          ,@DOB
          ,@ActiveInd
          ,@ProviderFirstName
          ,@ProviderLastName
          ,ISNULL(@Email, RTRIM(LTRIM(@ProviderFirstName)) + '_' + RTRIM(LTRIM(@ProviderLastName)) + CAST(@ID AS VARCHAR(20))) --Hack.  Ugo asked I put this in here to force it be valued. --add id to ensure uniqueness
          ,@SecondaryEmailAddress
          ,@USERNAME
          ,@DeleteInd,@CurrentDateTime,@CurrentDateTime,@LBUSERID,@LBUSERID

          SET @createLBUserID = SCOPE_IDENTITY()

          IF @DEBUG=1 BEGIN PRINT '    ProviderExtract LBUser Create ID=' + CAST(@createLBUserID AS VARCHAR(20)) END

          INSERT INTO vwPortal_Provider
          (IndividualID
          ,SourceSystemID
          ,PartyTypeID
          ,ProviderRoleTypeID
          ,EducationDegreeTypeId
          ,ProviderSpecialtyTypeID
          ,ProviderStatusTypeID
          ,LBUserId
          ,NationalProviderIdentifier
          ,ExternalReferenceIdentifier
          ,OtherReferenceIdentifier
          ,DeleteInd,CreateDateTime,ModifyDateTime,ModifyLBUserId,CreateLBUserId
          )
          SELECT
          @IndividualID
          ,@SourceSystemID
          ,@PartyTypeID
          ,@ProviderRoleTypeID
          ,@EducationDegreeTypeID
          ,@ProviderSpecialtyTypeID
          ,@ProviderStatusTypeID
          ,@createLBUserID
          ,@ProviderNPI
          ,@extProviderID
          ,NULL
          ,@DeleteInd,@CurrentDateTime,@CurrentDateTime,@LBUSERID,@LBUSERID

          SET @ProviderID = SCOPE_IDENTITY()

          IF @DEBUG=1 BEGIN PRINT '    ProviderExtract ProviderID for extProviderID(' + CAST(@extProviderID AS VARCHAR(20)) + '): ' + CAST(@ProviderID AS VARCHAR(20)) END

          INSERT INTO vwPortal_LbuserRole (LbUserid, RoleId) 
          SELECT @createLBUserID ,
          (SELECT ROLE.RoleId FROM vwPortal_Role ROLE WHERE NAME = 'NoPermissions' AND Description = 'NoPermissions' AND CreateLBUserId = 1) X

          COMMIT TRAN
     END TRY 
     BEGIN CATCH 
          IF @@TRANCOUNT > 0 
              ROLLBACK TRAN
     END CATCH 

     SET @RecordsProcessed=@RecordsProcessed+1
     SET @PreviousID = @ID
END

DROP TABLE #T

-- ADDING PROVIDER DEMOGRAPHICS 
INSERT INTO vwPortal_LbUserAddress
           ([LBUserId],[AddressTypeId],[Address1]
           ,[Address2],[City],[StateProvince]
           ,[PostalCode],[CountryId],[CreateDateTime]
           ,[ModifyDateTime],[CreateLBUserId],[ModifyLBUserId])
SELECT        X.[LBUserId],X.[AddressTypeId],X.[Address1]
           ,X.[Address2],X.[City],X.[StateProvince]
           ,X.[PostalCode],X.[CountryId],X.[CreateDateTime]
           ,X.[ModifyDateTime],X.[CreateLBUserId],X.[ModifyLBUserId]
FROM 
(SELECT   
          Prov.LBUserId,1 AS [AddressTypeId],ST.[PRIMARY Service Address] AS [Address1],
          ISNULL(ST.Street2,'') AS [Address2],ST.City, ST.State AS [StateProvince],
          st.[Zip Code] AS [PostalCode],1 AS [CountryId], GetUTCDATE() AS [CreateDateTime],
        GetUTCDATE() AS [ModifyDateTime], 1 AS [CreateLBUserId], 1 AS [ModifyLBUserId],
          ROW_NUMBER() OVER (PARTITION BY Prov.NationalProviderIdentifier ORDER BY Prov.ModifyDateTime) SNO 
FROM ProviderExtract ST
INNER JOIN vwPortal_Provider Prov ON LTRIM(RTRIM(ST.ProviderNPI)) = LTRIM(RTRIM(PRov.ExternalReferenceIdentifier))
WHERE ST.[PRIMARY Service Address] <> '' AND ST.ProviderNPI <> '' AND Prov.LBUserId IS NOT NULL
) X 
LEFT JOIN vwPortal_LbUserAddress LBUA ON X.LBUserId = LBUA.LBUserId 
--AND X.[Address1] = LBUA.[Address1] 
WHERE SNO = 1 AND LBUA.LBUserAddressId IS NULL 


INSERT INTO vwPortal_LBUserPhone
           ([LBUserId],[PhoneTypeId],[CountryCode]
           ,[AreaCode],[PhoneNumber],[Extension]
           ,[CreateDateTime],[ModifyDateTime],[CreateLBUserId]
           ,[ModifyLBUserId])
SELECT        X.[LBUserId],X.[PhoneTypeId],X.[CountryCode]
           ,X.[AreaCode],X.[PhoneNumber],X.[Extension]
           ,X.[CreateDateTime],X.[ModifyDateTime],X.[CreateLBUserId]
           ,X.[ModifyLBUserId]
FROM 
(
SELECT
              Prov.LBUserId,2 AS [PhoneTypeId],1 AS [CountryCode],
              --ST.PHone, 
               LEFT(LTRIM(RTRIM(REPLACE(REPLACE(ST.Phone,'(',''),')',''))),3) AS [AreaCode],
              RIGHT(LTRIM(RTRIM(REPLACE(ST.Phone,'-',''))),7) AS [PhoneNumber],
              11 AS [Extension],
            GetUTCDATE() AS [CreateDateTime],GetUTCDATE() AS [ModifyDateTime],1 AS [CreateLBUserId], 1 AS [ModifyLBUserId],
              ROW_NUMBER() OVER (PARTITION BY Prov.NationalProviderIdentifier ORDER BY Prov.ModifyDateTime) SNO 
FROM ProviderExtract ST
INNER JOIN vwPortal_Provider Prov ON LTRIM(RTRIM(ST.ProviderNPI)) = LTRIM(RTRIM(PRov.NationalProviderIdentifier))
WHERE ST.Phone <> '' AND Prov.LBUserId IS NOT NULL
) X 
LEFT JOIN vwPortal_LBUserPhone LBUP ON X.LBUserId = LBUP.LBUserId 
	--2016-11-21   CJL	PB-2377		Removed constraint on the LBPhoneUser insert SQL to prevent multiple phone records being created
	--The portal UI only supports a single phone number for the time being
	--AND X.[AreaCode] = LBUP.[AreaCode] AND X.[PhoneNumber] = LBUP.[PhoneNumber]
WHERE SNO = 1 AND LBUP.LBUserPhoneId IS NULL  










--- NOW UPDATES/INSERTS TO INDIVIDUAL ADDRESS AND PHONE TABLES 
-- GOING AFTER ADDRESSES
CREATE TABLE #AddressTempIN (ExternalReferenceIdentifier VARCHAR(100), StreetAddressId INT, 
                                   StreetAddressTypeId INT, SourceSystemId INT, AddressLine1 VARCHAR(150), AddressLine2 VARCHAR(150),
                                  AddressLine3 VARCHAR(150), DwellingTypeId INT, City VARCHAR(100), TerritoryTypeID INT,
                                  PostalCode VARCHAR(10),PostalCodePlus4 VARCHAR(10), CountryTypeId VARCHAR(10))
CREATE TABLE #AddressTempOUT (ExternalReferenceIdentifier VARCHAR(100), StreetAddressTypeId INT, StreetAddressId INT)


--LBETL-611, Filtered out address where the StreetAddressId is NULL,  Grouping to get the MIN ExternalReferenceIdentifier in case 
--of duplicate values
INSERT INTO #AddressTempIN (ExternalReferenceIdentifier , StreetAddressId , 
                                   StreetAddressTypeId , SourceSystemId , AddressLine1 ,AddressLine2,
                                  AddressLine3 , DwellingTypeId ,City,TerritoryTypeID,
                                  PostalCode,PostalCodePlus4,CountryTypeId )
SELECT   
          MIN(X.ExternalReferenceIdentifier) as ExternalReferenceIdentifier, TARG2.StreetAddressId AS StreetAddressId,
          X.StreetAddressTypeId, X.SourceSystemId, X.AddressLine1,X.AddressLine2,
          X.AddressLine3 , X.DwellingTypeId,  X.City , X.TerritoryTypeID,
          X.PostalCode, X.PostalCodePlus4, X.CountryTypeId
FROM 
(SELECT   
          Prov.NationalProviderIdentifier  AS ExternalReferenceIdentifier, 
          1 AS StreetAddressTypeId, 1 AS SourceSystemId, [PRIMARY Service Address] AS AddressLine1,ISNULL(ST.Street2,'') AS AddressLine2,
          NULL  AS AddressLine3 , 0 AS DwellingTypeId,  ST.City AS City , TT.TerritoryTypeID AS TerritoryTypeID,
          st.[Zip Code] AS  PostalCode, NULL AS PostalCodePlus4, 1 AS CountryTypeId, Prov.IndividualId,
          ROW_NUMBER() OVER (PARTITION BY Prov.NationalProviderIdentifier ORDER BY Prov.ModifyDateTime) SNO 
FROM ProviderExtract ST
INNER JOIN vwPortal_Provider Prov ON LTRIM(RTRIM(ST.ProviderNPI)) = LTRIM(RTRIM(PRov.ExternalReferenceIdentifier))
LEFT JOIN vwPortal_TerritoryType TT ON ST.State = TT.NAME 
WHERE ST.[PRIMARY Service Address] <> ''
) X 
LEFT JOIN vwPortal_IndividualStreetAddress TARG1 ON TARG1.IndividualID = X.IndividualId
LEFT JOIN vwPortal_StreetAddress TARG2 ON TARG2.StreetAddressId = TARG1.StreetAddressId AND TARG2.StreetAddressTypeId = 1
WHERE SNO = 1 AND TARG2.StreetAddressId IS NOT NULL
GROUP BY 
	TARG2.StreetAddressId ,
	X.StreetAddressTypeId, X.SourceSystemId, X.AddressLine1,X.AddressLine2,
	X.AddressLine3 , X.DwellingTypeId,  X.City , X.TerritoryTypeID,
	X.PostalCode, X.PostalCodePlus4, X.CountryTypeId






INSERT INTO #AddressTempOUT (externalreferenceidentifier,StreetAddressTypeId,StreetAddressId)
SELECT
     externalreferenceidentifier, StreetAddressTypeId,StreetAddressId
FROM
(
     MERGE vwPortal_StreetAddress AS TARGET
     USING (
              SELECT 
                   externalreferenceidentifier AS externalreferenceidentifier, StreetAddressId AS StreetAddressId,
                   StreetAddressTypeId AS StreetAddressTypeId, SourceSystemId AS SourceSystemId, LTRIM(RTRIM(AddressLine1)) AS AddressLine1,  LTRIM(RTRIM(AddressLine2))  AS AddressLine2, 
                    LTRIM(RTRIM(AddressLine3)) AS AddressLine3,DwellingTypeId AS DwellingTypeId, LTRIM(RTRIM(City)) AS City, TerritoryTypeID AS TerritoryTypeID,
                   LTRIM(RTRIM(LEFT(PostalCode,5))) AS PostalCode , PostalCodePlus4 AS PostalCodePlus4, CountryTypeId AS CountryTypeId,
                   0 AS DeleteInd, GetDate() AS CreateDatetime, GetDate() AS ModifyDatetime, 1 AS CreateLbUserId, 1 AS ModifyLbUserid
              FROM #AddressTempIN
             ) AS SOURCE
               (externalreferenceidentifier,StreetAddressId,StreetAddressTypeId, SourceSystemId, AddressLine1, AddressLine2, AddressLine3,DwellingTypeId,City,TerritoryTypeID,
               PostalCode,PostalCodePlus4,CountryTypeId,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId) 
            ON TARGET.StreetAddressId = SOURCE.StreetAddressId 
     WHEN MATCHED THEN UPDATE SET
              StreetAddressTypeId = SOURCE.StreetAddressTypeId, 
              SourceSystemId = SOURCE.SourceSystemId,
              AddressLine1 = SOURCE.AddressLine1,
              AddressLine2  = SOURCE.AddressLine2,
              AddressLine3 = SOURCE.AddressLine3,
              DwellingTypeId = SOURCE.DwellingTypeId,
              City = SOURCE.City, 
              TerritoryTypeID = SOURCE.TerritoryTypeID,
              PostalCode = SOURCE.PostalCode,
              PostalCodePlus4  = SOURCE.PostalCodePlus4,
              CountryTypeId = SOURCE.CountryTypeId,
              DeleteInd = SOURCE.DeleteInd,
              ModifyDateTime = SOURCE.ModifyDateTime,
              ModifyLBUserId = SOURCE.ModifyLBUserId
     WHEN NOT MATCHED THEN INSERT 
             (StreetAddressTypeId, SourceSystemId, AddressLine1, AddressLine2, AddressLine3,DwellingTypeId,City,TerritoryTypeID,
               PostalCode,PostalCodePlus4,CountryTypeId,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId) 
     VALUES (SOURCE.StreetAddressTypeId, SOURCE.SourceSystemId, SOURCE.AddressLine1, SOURCE.AddressLine2, SOURCE.AddressLine3,SOURCE.DwellingTypeId,SOURCE.City,SOURCE.TerritoryTypeID,
               SOURCE.PostalCode,SOURCE.PostalCodePlus4,SOURCE.CountryTypeId,SOURCE.DeleteInd,SOURCE.CreateDateTime,SOURCE.ModifyDateTime,SOURCE.CreateLBUserId,SOURCE.ModifyLBUserId) 
OUTPUT $ACTION MergeAction,  ISNULL(INSERTED.StreetAddressId,DELETED.StreetAddressId) StreetAddressId, SOURCE.StreetAddressTypeId,SOURCE.ExternalReferenceIdentifier 
) x;

INSERT INTO vwPortal_IndividualStreetAddress 
          (IndividualID,StreetAddressId,StreetAddressTypeID,DeleteInd,CreateDateTime, ModifyDateTime,
          CreateLbUserId,ModifyLBUserId)
SELECT Prov.IndividualID, TOUT.StreetAddressId,TOUT.StreetAddressTypeId, 0 AS DeleteInd,GETUTCDATE() AS CreateDateTime, GETUTCDATE() AS ModifyDateTime,
        1 AS CreateLbUserId, 1 AS ModifyLbUserId
FROM #AddressTempOUT TOUT
INNER JOIN vwPortal_Provider Prov ON TOUT.ExternalReferenceIdentifier = LTRIM(RTRIM(PRov.ExternalReferenceIdentifier))
LEFT JOIN vwPortal_IndividualStreetAddress TARG ON TOUT.StreetAddressId = TARG.StreetAddressId AND Prov.IndividualID = TARG.IndividualID
WHERE TARG.StreetAddressId IS NULL 

-- GOING AFTER PHONES 



CREATE TABLE #TelephoneTempIN (ExternalReferenceIdentifier VARCHAR(100), TelephoneNumberId INT, 
                                   PhoneTypeId INT, ServiceCarrierId INT, CountryCode VARCHAR(5),
                                  AreaCode VARCHAR(5), PhoneNumber VARCHAR(10),Extension VARCHAR(10))
CREATE TABLE #TelephoneTempOUT (ExternalReferenceIdentifier VARCHAR(100), PhoneTypeId INT, TelephoneNumberId INT)


--LBETL-611, Filtered out address where the TelephoneNumberId is NULL,  Grouping to get the MIN ExternalReferenceIdentifier in case 
--of duplicate values
INSERT INTO #TelephoneTempIN (ExternalReferenceIdentifier , TelephoneNumberId , 
                                   PhoneTypeId , ServiceCarrierId , CountryCode ,
                                  AreaCode , PhoneNumber ,Extension )
SELECT  X.ExternalReferenceIdentifier, TARG2.TelephoneNumberId AS TelephoneNumberId,
          X.PhoneTypeId, X.ServiceCarrierId, X.CountryCode, X.AreaCode, X.PhoneNumber, X.Extension
FROM 
	( 
		SELECT
				  PRov.NationalProviderIdentifier AS ExternalReferenceIdentifier , PRov.IndividualId, 
				   LEFT(LTRIM(RTRIM(REPLACE(REPLACE(ST.Phone,'(',''),')',''))),3) AS [AreaCode],
				  RIGHT(LTRIM(RTRIM(REPLACE(ST.Phone,'-',''))),7) AS [PhoneNumber], 2 AS PhoneTypeId, 0 AS ServiceCarrierId, 1 AS CountryCode,
				  11 AS [Extension], GetUTCDATE() AS [CreateDateTime] ,GetUTCDATE() AS [ModifyDateTime] ,1 AS [CreateLBUserId], 1 AS [ModifyLBUserId],
				  ROW_NUMBER() OVER (PARTITION BY Prov.NationalProviderIdentifier ORDER BY Prov.ModifyDateTime) SNO 
		FROM ProviderExtract ST
			INNER JOIN vwPortal_Provider Prov ON LTRIM(RTRIM(ST.ProviderNPI)) = LTRIM(RTRIM(PRov.NationalProviderIdentifier)) WHERE ST.Phone <> ''
			) X LEFT JOIN  (SELECT TARG1.IndividualId, TARG2.TelephoneNumberId FROM vwPortal_IndividualTelephoneNumber TARG1  
			INNER JOIN vwPortal_TelephoneNumber TARG2 ON TARG2.TelephoneNumberId = TARG1.TelephoneNumberID AND TARG2.PhoneTypeId = 2 ) TARG2 ON TARG2.IndividualID =  X.IndividualID 
		WHERE SNO = 1 AND TARG2.TelephoneNumberID IS NOT NULL
 
		UNION

		SELECT  X.ExternalReferenceIdentifier, TARG2.TelephoneNumberId AS TelephoneNumberId,
			  X.PhoneTypeId, X.ServiceCarrierId, X.CountryCode, X.AreaCode, X.PhoneNumber, X.Extension
		FROM 
		(
				SELECT
					  PRov.NationalProviderIdentifier AS ExternalReferenceIdentifier , PRov.IndividualId, 
					   LEFT(LTRIM(RTRIM(REPLACE(REPLACE(ST.Fax,'(',''),')',''))),3) AS [AreaCode],
					  RIGHT(LTRIM(RTRIM(REPLACE(ST.Fax,'-',''))),7) AS [PhoneNumber], 4 AS PhoneTypeId, 0 AS ServiceCarrierId, 1 AS CountryCode,
					  11 AS [Extension], GetUTCDATE() AS [CreateDateTime] ,GetUTCDATE() AS [ModifyDateTime] ,1 AS [CreateLBUserId], 1 AS [ModifyLBUserId],
					  ROW_NUMBER() OVER (PARTITION BY Prov.NationalProviderIdentifier ORDER BY Prov.ModifyDateTime) SNO 
				FROM ProviderExtract ST
				INNER JOIN vwPortal_Provider Prov ON LTRIM(RTRIM(ST.ProviderNPI)) = LTRIM(RTRIM(PRov.NationalProviderIdentifier)) WHERE ST.FAX <> ''
		) X 
		LEFT JOIN (SELECT TARG1.IndividualId, TARG2.TelephoneNumberId FROM vwPortal_IndividualTelephoneNumber TARG1  
		INNER JOIN vwPortal_TelephoneNumber TARG2 ON TARG2.TelephoneNumberId = TARG1.TelephoneNumberID AND TARG2.PhoneTypeId = 4 ) TARG2 ON TARG2.IndividualID =  X.IndividualID 
		WHERE SNO = 1 AND TARG2.TelephoneNumberID IS NOT NULL 
		


INSERT INTO #TelephoneTempOUT (externalreferenceidentifier,PhoneTypeId,TelephoneNumberId)
SELECT
     externalreferenceidentifier, PhoneTypeId,TelephoneNumberId
FROM
(
     MERGE vwPortal_TelephoneNumber AS TARGET
     USING (
              SELECT 
                   min(externalreferenceidentifier) AS externalreferenceidentifier, TelephoneNumberId AS TelephoneNumberId,
                   PhoneTypeId AS PhoneTypeId, ServiceCarrierId AS ServiceCarrierId, CountryCode AS CountryCode, AreaCode  AS AreaCode, PhoneNumber AS PhoneNumber,Extension AS Extension,
                   0 AS DeleteInd, GetDate() AS CreateDatetime, GetDate() AS ModifyDatetime, 1 AS CreateLbUserId, 1 AS ModifyLbUserid
              FROM #TelephoneTempIN
			  GROUP BY TelephoneNumberId,  PhoneTypeId, ServiceCarrierId, CountryCode, AreaCode, PhoneNumber, Extension

             ) AS SOURCE
               (externalreferenceidentifier,TelephoneNumberId,PhoneTypeId, ServiceCarrierId, CountryCode, AreaCode, PhoneNumber,Extension,
               DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId) 
            ON TARGET.TelephoneNumberId = SOURCE.TelephoneNumberId 
     WHEN MATCHED THEN UPDATE SET
              PhoneTypeId = SOURCE.PhoneTypeId, 
              ServiceCarrierId = SOURCE.ServiceCarrierId,
              CountryCode = SOURCE.CountryCode,
              AreaCode  = SOURCE.AreaCode,
              PhoneNumber = SOURCE.PhoneNumber,
              Extension = SOURCE.Extension,
              DeleteInd = SOURCE.DeleteInd,
              ModifyDateTime = SOURCE.ModifyDateTime,
              ModifyLBUserId = SOURCE.ModifyLBUserId
     WHEN NOT MATCHED THEN INSERT 
             (PhoneTypeId,ServiceCarrierId,CountryCode,AreaCode,PhoneNumber,
               Extension,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId) 
     VALUES (SOURCE.PhoneTypeId,SOURCE.ServiceCarrierId,SOURCE.CountryCode,SOURCE.AreaCode,SOURCE.PhoneNumber,
               SOURCE.Extension,SOURCE.DeleteInd,SOURCE.CreateDateTime,SOURCE.ModifyDateTime,SOURCE.CreateLBUserId,SOURCE.ModifyLBUserId) 
OUTPUT $ACTION MergeAction,  ISNULL(INSERTED.TelephoneNumberId,DELETED.TelephoneNumberId) TelephoneNumberId, SOURCE.PhoneTypeId,SOURCE.ExternalReferenceIdentifier 
) x;

INSERT INTO vwPortal_IndividualTelephoneNumber 
          (IndividualID,TelephoneNumberID,SourceSystemID,DeleteInd,CreateDateTime, ModifyDateTime,
          CreateLbUserId,ModifyLBUserId,ExternalReferenceIdentifier,OtherReferenceIdentifier)
SELECT  Prov.IndividualID, TOUT.TelephoneNumberId, 1 AS SourceSystemId, 0 AS DeleteInd,GETUTCDATE() AS CreateDateTime, GETUTCDATE() AS ModifyDateTime,
        1 AS CreateLbUserId, 1 AS ModifyLbUserId, TOUT.ExternalReferenceIdentifier AS ExternalReferenceIdentifier, TOUT.ExternalReferenceIdentifier AS OtherReferenceIndentifier
FROM #TelephoneTempOUT TOUT
INNER JOIN vwPortal_Provider Prov ON TOUT.ExternalReferenceIdentifier = LTRIM(RTRIM(PRov.ExternalReferenceIdentifier))
LEFT JOIN vwPortal_IndividualTelephoneNumber TARG ON TOUT.TelephoneNumberId = TARG.TelephoneNumberID AND Prov.IndividualID = TARG.IndividualID
WHERE TARG.TelephoneNumberID IS NULL  

UPDATE Prov
SET ProviderSpecialtyTypeID = PST.ProviderSpecialtyTypeID
FROM ProviderExtract ST
INNER JOIN vwPortal_Provider Prov ON LTRIM(RTRIM(ST.ProviderNPI)) = LTRIM(RTRIM(PRov.NationalProviderIdentifier)) 
INNER JOIN vwPortal_ProviderSpecialtyType PST ON LTRIM(RTRIM(ST.PrimarySpecialty)) = LTRIM(RTRIM(PST.DisplayValue))   

-- BRING ANY PROVIDERS MANUALLY ENTERED IN PORTAL TO EDW SIDE
INSERT INTO ProviderExtract (ProviderLastName, ProviderFirstName,ProviderId,ProviderNPI,DepositingFeed)
SELECT 
     INM.LastName, INM.FirstName, LTRIM(RTRIM(P.NationalProviderIdentifier)), LTRIM(RTRIM(P.NationalProviderIdentifier)),'PortalEntry'
FROM vwPortal_Provider P 
INNER JOIN vwPortal_Individual I ON P.IndividualID = I.IndividualID 
INNER JOIN vwPortal_IndividualName INM ON I.IndividualID = INM.IndividualID
LEFT JOIN ProviderExtract Pe ON P.NationalProviderIdentifier  = pe.ProviderNPI  
WHERE pe.ProviderNPI IS NULL 

--Reset Provider Names in Edw Provider Extract, if changed in portal Provider manager 
UPDATE PE 
SET ProviderFirstName = INM.FirstName, 
ProviderLastName = INM.LastName -- SELECT *
FROM ProviderExtract PE  
INNER JOIN vwPortal_Provider P ON P.NationalProviderIdentifier  = pe.ProviderNPI  
INNER JOIN vwPortal_Individual I ON P.IndividualID = I.IndividualID 
INNER JOIN vwPortal_IndividualName INM ON I.IndividualID = INM.IndividualID
WHERE PE.ProviderFirstName <> INM.FirstName OR PE.ProviderLastName <> INM.LastName 

--Create Provider Healthcare Org records
MERGE INTO vwPortal_ProviderHealthcareOrg AS target
USING (SELECT 
			p.ProviderID,
			ho.HealthcareOrgID,
			ifs.SourceSystemID
		FROM vwPortal_HealthcareOrg ho
			INNER JOIN vwPortal_BusinessUnit bu ON bu.BusinessUnitId = ho.BusinessUnitId
			INNER JOIN (SELECT ProviderNPI, TIN, InterfaceSystemId, ROW_NUMBER() OVER (PARTITION BY ProviderNPI,TIN ORDER BY CreateDateTime DESC) r FROM vwPortal_InterfaceProvider  WHERE ProviderNPI <> '9999999999') AS ip ON ip.TIN = bu.ExternalCode
			INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ip.InterfaceSystemId
			INNER JOIN vwPortal_Provider p ON p.NationalProviderIdentifier = ip.ProviderNPI
		WHERE ip.r = 1 
			AND ho.DeleteInd = 0
			AND bu.DeleteInd = 0
			AND p.DeleteInd = 0
	  ) as source
	ON source.ProviderId = target.ProviderId
		AND source.HealthcareOrgId = target.HealthcareOrgId
WHEN NOT MATCHED THEN
	INSERT (ProviderID,HealthcareOrgID,SourceSystemID,ExternalReferenceIdentifier,OtherReferenceIdentifier,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,DefaultReportPrinterId,DefaultInd)
	VALUES (source.ProviderID,source.HealthcareOrgID,source.SourceSystemID,null,null,0,GETUTCDATE(),GETUTCDATE(),1,1,null,0);


	
		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	



	END TRY

		BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
END
GO
