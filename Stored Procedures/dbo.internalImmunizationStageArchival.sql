SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalImmunizationStageArchival] 
    @LookbackDays INT = 0  ,
	@LookbackSince DateTime = NULL,
	@ClearStage BIT = 0
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

    SELECT @LookbackSince = CASE WHEN @LookbackSince IS NULL THEN GETDATE() ELSE @LookbackSince END 

	MERGE InterfacePatientImmunizationArchive AS TARGET
	USING (
			SELECT 
			  [InterfacePatientImmunizationId]
			  ,[InterfacePatientID]
			  ,[InterfaceSystemId]
			  ,[ServiceDate]
			  ,[ImmunizationCode]
			  ,[ImmunizationCodeSystemName]
			  ,[ImmunizationDescription]
			  ,[ImmunizationDose]
			  ,[ImmunizationUnits]
			  ,[MaterialLotNumber]
			  ,[MaterialManufacturer]
			  ,[RouteCode]
			  ,[RouteCodeSystemName]
			  ,[RouteDescription]
			  ,[PerformingClinician]
			  ,[CreateDateTime]
		    FROM [dbo].vwPortal_InterfacePatientImmunization (NOLOCK)
			WHERE [CreateDateTime] >= GetDate() - @LookbackDays OR [CreateDateTime] > @LookbackSince
		   ) AS SOURCE
			([InterfacePatientImmunizationId],[InterfacePatientID],[InterfaceSystemId],[ServiceDate],[ImmunizationCode],[ImmunizationCodeSystemName]
			,[ImmunizationDescription],[ImmunizationDose],[ImmunizationUnits],[MaterialLotNumber],[MaterialManufacturer],[RouteCode],[RouteCodeSystemName]
			,[RouteDescription],[PerformingClinician],[CreateDateTime]) 
		  ON TARGET.[InterfacePatientImmunizationId] = SOURCE.[InterfacePatientImmunizationId] 
	WHEN MATCHED THEN UPDATE SET
			--[InterfacePatientImmunizationId]	=Source.[InterfacePatientImmunizationId],
			[InterfacePatientID]				=Source.[InterfacePatientID],
			[InterfaceSystemId]					=Source.[InterfaceSystemId],
			[ServiceDate]						=Source.[ServiceDate],
			[ImmunizationCode]					=Source.[ImmunizationCode],
			[ImmunizationCodeSystemName]		=Source.[ImmunizationCodeSystemName],
			[ImmunizationDescription]			=Source.[ImmunizationDescription],
			[ImmunizationDose]					=Source.[ImmunizationDose],
			[ImmunizationUnits]					=Source.[ImmunizationUnits],
			[MaterialLotNumber]					=Source.[MaterialLotNumber],
			[MaterialManufacturer]				=Source.[MaterialManufacturer],
			[RouteCode]							=Source.[RouteCode],
			[RouteCodeSystemName]				=Source.[RouteCodeSystemName],
			[RouteDescription]					=Source.[RouteDescription],
			[PerformingClinician]				=Source.[PerformingClinician],
			[CreateDateTime]					=Source.[CreateDateTime]
	WHEN NOT MATCHED THEN INSERT 
		   ([InterfacePatientImmunizationId],[InterfacePatientID],[InterfaceSystemId],[ServiceDate],[ImmunizationCode],[ImmunizationCodeSystemName]
			,[ImmunizationDescription],[ImmunizationDose],[ImmunizationUnits],[MaterialLotNumber],[MaterialManufacturer],[RouteCode],[RouteCodeSystemName]
			,[RouteDescription],[PerformingClinician],[CreateDateTime]) 
	VALUES (Source.[InterfacePatientImmunizationId],Source.[InterfacePatientID],Source.[InterfaceSystemId],Source.[ServiceDate],Source.[ImmunizationCode],Source.[ImmunizationCodeSystemName]
			,Source.[ImmunizationDescription],Source.[ImmunizationDose],Source.[ImmunizationUnits],Source.[MaterialLotNumber],Source.[MaterialManufacturer],Source.[RouteCode],Source.[RouteCodeSystemName]
			,Source.[RouteDescription],Source.[PerformingClinician],Source.[CreateDateTime]) ;

	IF @ClearStage = 1
	BEGIN
		DELETE vwPortal_InterfacePatientImmunization FROM vwPortal_InterfacePatientImmunization A
		INNER JOIN InterfacePatientImmunizationArchive B ON A.[InterfacePatientImmunizationId] = B.[InterfacePatientImmunizationId]
		WHERE A.[CreateDateTime] >= GetDate() - @LookbackDays OR A.[CreateDateTime] > @LookbackSince
	END
		


GO
