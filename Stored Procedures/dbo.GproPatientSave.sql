SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GproPatientSave]
/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		2015-01-06
	INITIAL VER:	1.6.1
	MODULE:			GPRO		
	DESCRIPTION:	Import the raw patient data to the permanent GPRO tables
	PARAMETERS:		none
	RETURN VALUE(s)/OUTPUT:	none
	PROGRAMMIN NOTES: (Optional) 
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.0 		2015-01-06		BR						Initial Version
	2.2.1		2017-01-04		BR			LBPP-2070	Including code to set PREV-13, set provider 1 name to ''
=================================================================*/
(
	@ImportSetId int
)
AS
BEGIN
    SET NOCOUNT ON;

	DECLARE @GProStatusTypeId AS INT
	DECLARE @MeasureProgramTypeId AS INT

	SELECT @GProStatusTypeId = GProStatusTypeId FROM GProStatusType WHERE NAME = 'Incomplete'
	-- commented by JKSELECT @MeasureProgramTypeId = MeasureProgramTypeId FROM MeasureProgramType WHERE NAME = 'ACO Measures Program-2014'  -- to be updated

	set @MeasureProgramTypeId = 1

	--------------------------------------------------------------------------------
	-- Load the data from the raw table (which holds raw data imported from the patient XML file) into the GproPatientRanking table
	--------------------------------------------------------------------------------
	MERGE INTO [GproPatientRanking] AS target
	USING (SELECT rr.[group-tin],
		   rr.[medicare-id],
		   rr.[patient-first-name],
		   rr.[patient-last-name],
		   rr.[gender],
		   rr.[birth-date],
		   rr.[provider-npi],
		   rr.[provider-npi-two],
		   rr.[provider-npi-three],
		   rr.[clinic-identifier],
		   rr.[carefalls-rank],
		   rr.[cad-rank],
		   rr.[dm-rank],
		   rr.[hf-rank],
		   rr.[htn-rank],
		   rr.[ivd-rank],
		   rr.[mh-rank],
		   rr.[pcmammogram-rank],
		   rr.[pccolorectal-rank],
		   rr.[pcflushot-rank],
		   rr.[pcpneumoshot-rank],
		   rr.[pcbmiscreen-rank],
		   rr.[pctobaccouse-rank],
		   rr.[pcbloodpressure-rank],
		   rr.[pcdepression-rank]
		   FROM [GPROPatient_Raw] rr 
		   WHERE rr.[ImportSetId] = @ImportSetId) AS source
		ON source.[patient-first-name] = target.FirstName AND source.[patient-last-name] = target.LastName 
			AND source.[medicare-id] = target.MedicareHicn AND source.[birth-date] = target.BirthDate
	WHEN NOT MATCHED THEN
		INSERT ([MeasureProgramTypeId]
			   ,[GroupTin]
			   ,[MedicareHicn]
			   ,[FirstName]
			   ,[LastName]
			   ,[GenderCode]
			   ,[BirthDate]
			   ,[ProviderNpi1]
			   ,[ProviderFirstName1]
			   ,[ProviderLastName1]
			   ,[ProviderNpi2]
			   ,[ProviderFirstName2]
			   ,[ProviderLastName2]
			   ,[ProviderNpi3]
			   ,[ProviderFirstName3]
			   ,[ProviderLastName3]
			   ,[ClinicIdentifier]
			   ,[CareMeddocRank]
			   ,[CareFallsRank]
			   ,[CadRank]
			   ,[DmRank]
			   ,[HfRank]
			   ,[HtnRank]
			   ,[IvdRank]
			   ,[MhRank]
			   ,[PcMammogramRank]
			   ,[PcColorectalRank]
			   ,[PcFlushotRank]
			   ,[PcPneumoshotRank]
			   ,[PcbMiscreenRank]
			   ,[PcTobaccouseRank]
			   ,[PcBloodPressureRank]
			   ,[PcDepressionRank]
			   ,[GProStatusTypeId]
			   ,[CreateDateTime]
			   ,[ModifyDateTime])
		VALUES (@MeasureProgramTypeId,
			   source.[group-tin],
			   source.[medicare-id],
			   source.[patient-first-name],
			   source.[patient-last-name],
			   source.[gender],
			   source.[birth-date],
			   source.[provider-npi],
			   '',
			   '',
			   source.[provider-npi-two],
			   null,
			   null,
			   source.[provider-npi-three],
			   null,
			   null,
			   source.[clinic-identifier],
			   null,
			   source.[carefalls-rank],
			   source.[cad-rank],
			   source.[dm-rank],
			   source.[hf-rank],
			   source.[htn-rank],
			   source.[ivd-rank],
			   source.[mh-rank],
			   source.[pcmammogram-rank],
			   source.[pccolorectal-rank],
			   source.[pcflushot-rank],
			   source.[pcpneumoshot-rank],
			   source.[pcbmiscreen-rank],
			   source.[pctobaccouse-rank],
			   source.[pcbloodpressure-rank],
			   source.[pcdepression-rank], 
			   @GProStatusTypeId,
			   GETUTCDATE(),   
			   GETUTCDATE());

	--------------------------------------------------------------------------------
	-- Load the data from the raw table (which holds raw data imported from the patient XML file) into the GproPatientMeasure table
	--------------------------------------------------------------------------------
	MERGE INTO [GproPatientMeasure] AS target
	USING (SELECT 
				gpr.GproPatientRankingId,
				r.[medical-record-found] AS MedicalRecordFound,
				r.[medical-not-qualified-reason] AS MedicalNotQualifiedReason,
				r.[medical-not-qualified-date] AS MedicalNotQualifiedDate,
				r.[carefalls-rank] AS CareFallsRank,
				r.[carefalls-confirmed] AS CareFallsConfirmed,
				r.[falls-screening] AS FallsScreening,
				r.[cad-rank] AS CadRank,
				r.[cad-confirmed] AS CadConfirmed,
				r.[cad-diabetes-lvsd] AS CadDiabetesLvsd,
				r.[cad-acearb] AS CadAceArb,
				r.[cad-comments] AS CadComments,
				r.[dm-rank] AS DmRank,
				r.[dm-confirmed] AS DmConfirmed,
				r.[dm-hba1c-test] AS DmHba1cTest,
				r.[dm-hba1c-date] AS DmHba1cDate,
				r.[dm-hba1c-value] AS DmHba1cValue,
				r.[dm-eye-exam] AS DmEyeExam,
				r.[dm-comments] AS DmComments,
				r.[hf-rank] AS HfRank,
				r.[hf-confirmed] AS HfConfirmed,
				r.[hf-lvsd] AS HfLvsd,
				r.[hf-beta-blocker] AS HfBetaBlocker,
				r.[hf-comments] AS HfComments,
				r.[htn-rank] AS HtnRank,
				r.[htn-confirmed] AS HtnConfirmed,
				r.[htn-recent-bp] AS HtnRecentBp,
				r.[htn-bp-date] AS HtnBpDate,
				r.[htn-bp-systolic] AS HtnBpSystolic,
				r.[htn-bp-diastolic] AS HtnBpDiastolic,
				r.[htn-comments] AS HtnComments,
				r.[ivd-rank] AS IvdRank,
				r.[ivd-confirmed] AS IvdConfirmed,
				r.[ivd-antithrombotic] AS IvdAntithrombotic,
				r.[ivd-comments] AS IvdComments,
				r.[mh-rank] AS MhRank,
				r.[mh-confirmed] AS MhConfirmed,
				r.[mh-index-performed] AS MhIndexPerformed,
				r.[mh-index-test] AS MhIndexTest,
				r.[mh-index-date] AS MhIndexDate,
				r.[mh-index-score] AS MhIndexScore,
				r.[mh-followup-performed] AS MhFollowupPerformed,
				r.[mh-followup-test] AS MhFollowupTest,
				r.[mh-followup-date] AS MhFollowupDate,
				r.[mh-followup-score] AS MhFollowupScore,
				r.[mh-comments] AS MhComments,
				r.[pcmammogram-rank] AS PcMammogramRank,
				r.[pcmammogram-confirmed] AS PcMammogramConfirmed,
				r.[pcmammogram] AS PcMammogramPerformed,
				r.[pcmammogram-comments] AS PcMammogramComments,
				r.[pccolorectal-rank] AS PcColorectalRank,
				r.[pccolorectal-confirmed] AS PcColorectalConfirmed,
				r.[pccolorectal] AS PcColorectalPerformed,
				r.[pccolorectal-comments] AS PcColorectalComments,
				r.[pcflushot-rank] AS PcFluShotRank,
				r.[pcflushot-confirmed] AS PcFluShotConfirmed,
				r.[pcflushot] AS PcFluShotReceived,
				r.[pcflushot-comments] AS PcFluShotComments,
				r.[pcpneumoshot-rank] AS PcPneumoShotRank,
				r.[pcpneumoshot-confirmed] AS PcPneumoShotConfirmed,
				r.[pcpneumoshot] AS PcPneumoShotReceived,
				r.[pcpneumoshot-comments] AS PcPneumoShotComments,
				r.[pcbmiscreen-rank] AS PcBmiScreenRank,
				r.[pcbmiscreen-confirmed] AS PcBmiScreenConfirmed,
				r.[pcbmicalculated] AS PcBmiCalculated,
				r.[pcbminormal] AS PcBmiNormal,
				r.[pcbmiplan] AS PcBmiPlan,
				r.[pcbmi-comments] AS PcBmiComments,
				r.[pctobaccouse-rank] AS PcTobaccoRank,
				r.[pctobaccouse-confirmed] AS PcTobaccoConfirmed,
				r.[pctobaccoscreen] AS PcTobaccoScreen,
				r.[pctobaccocounsel] AS PcTobaccoCounsel,
				r.[pctobaccouse-comments] AS PcTobaccoComments,
				r.[pcbloodpressure-rank] AS PcBloodPressureRank,
				r.[pcbloodpressure-confirmed] AS PcBloodPressureConfirmed,
				r.[pcbloodpressure] AS PcBloodPressureScreen,
				r.[pcbpnormal] AS PcBloodPressureNormal,
				r.[pcbpfollowup] AS PcBloodPressureFollowUp,
				r.[pcbp-comment] AS PcBloodPressureComments,
				r.[pcdepression-rank] AS PcDepressionRank,
				r.[pcdepression] AS PcDepressionScreen,
				r.[pcdepression-confirmed] AS PcDepressionConfirmed,
				r.[pcdepression-positive] AS PcDepressionPositive,
				r.[pcdepression-plan] AS PcDepressionPlan,
				r.[pcdepression-comments] AS PcDepressionComments
		   FROM GproPatient_Raw r
				INNER JOIN GproPatientRanking gpr ON gpr.MedicareHicn = r.[medicare-id]
		   WHERE r.[ImportSetId] = @ImportSetId) AS source
		ON source.GproPatientRankingId = target.GproPatientRankingId
	WHEN NOT MATCHED THEN
		INSERT (GProPatientRankingId,
				MedicalRecordFound,
				MedicalNotQualifiedReason,
				MedicalNotQualifiedDate,
				CareFallsRank,
				CareFallsConfirmed,
				FallsScreening,
				CareFallsComments,
				CareMeddocRank,
				CareMeddocConfirmed,
				CareMeddocComments,
				CadRank,
				CadConfirmed,
				CadDiabetesLvsd,
				CadAceArb,
				CadComments,
				DmRank,
				DmConfirmed,
				DmHba1cTest,
				DmHba1cDate,
				DmHba1cValue,
				DmEyeExam,
				DmComments,
				HfRank,
				HfConfirmed,
				HfLvsd,
				HfBetaBlocker,
				HfComments,
				HtnRank,
				HtnConfirmed,
				HtnRecentBp,
				HtnBpDate,
				HtnBpSystolic,
				HtnBpDiastolic,
				HtnComments,
				IvdRank,
				IvdConfirmed,
				IvdAntithrombotic,
				IvdComments,
				MhRank,
				MhConfirmed,
				MhIndexPerformed,
				MhIndexTest,
				MhIndexDate,
				MhIndexScore,
				MhFollowupPerformed,
				MhFollowupTest,
				MhFollowupDate,
				MhFollowupScore,
				MhComments,
				PcMammogramRank,
				PcMammogramConfirmed,
				PcMammogramPerformed,
				PcMammogramComments,
				PcColorectalRank,
				PcColorectalConfirmed,
				PcColorectalPerformed,
				PcColorectalComments,
				PcFluShotRank,
				PcFluShotConfirmed,
				PcFluShotReceived,
				PcFluShotComments,
				PcPneumoShotRank,
				PcPneumoShotConfirmed,
				PcPneumoShotReceived,
				PcPneumoShotComments,
				PcBmiScreenRank,
				PcBmiScreenConfirmed,
				PcBmiCalculated,
				PcBmiNormal,
				PcBmiPlan,
				PcBmiComments,
				PcTobaccoRank,
				PcTobaccoConfirmed,
				PcTobaccoScreen,
				PcTobaccoCounsel,
				PcTobaccoComments,
				PcBloodPressureRank,
				PcBloodPressureConfirmed,
				PcBloodPressureScreen,
				PcBloodPressureNormal,
				PcBloodPressureFollowUp,
				PcBloodPressureComments,
				PcDepressionRank,
				PcDepressionScreen,
				PcDepressionConfirmed,
				PcDepressionPositive,
				PcDepressionPlan,
				PcDepressionComments,
				CreateDateTime,
				ModifyDateTime,
				CreateLbUserId,
				ModifyLbUserId)
		VALUES (source.GProPatientRankingId,
				source.MedicalRecordFound,
				source.MedicalNotQualifiedReason,
				source.MedicalNotQualifiedDate,
				source.CareFallsRank,
				source.CareFallsConfirmed,
				source.FallsScreening,
				null,
				null,
				null,
				null,
				source.CadRank,
				source.CadConfirmed,
				source.CadDiabetesLvsd,
				source.CadAceArb,
				source.CadComments,
				source.DmRank,
				source.DmConfirmed,
				source.DmHba1cTest,
				source.DmHba1cDate,
				source.DmHba1cValue,
				source.DmEyeExam,
				source.DmComments,
				source.HfRank,
				source.HfConfirmed,
				source.HfLvsd,
				source.HfBetaBlocker,
				source.HfComments,
				source.HtnRank,
				source.HtnConfirmed,
				source.HtnRecentBp,
				source.HtnBpDate,
				source.HtnBpSystolic,
				source.HtnBpDiastolic,
				source.HtnComments,
				source.IvdRank,
				source.IvdConfirmed,
				source.IvdAntithrombotic,
				source.IvdComments,
				source.MhRank,
				source.MhConfirmed,
				source.MhIndexPerformed,
				source.MhIndexTest,
				source.MhIndexDate,
				source.MhIndexScore,
				source.MhFollowupPerformed,
				source.MhFollowupTest,
				source.MhFollowupDate,
				source.MhFollowupScore,
				source.MhComments,
				source.PcMammogramRank,
				source.PcMammogramConfirmed,
				source.PcMammogramPerformed,
				source.PcMammogramComments,
				source.PcColorectalRank,
				source.PcColorectalConfirmed,
				source.PcColorectalPerformed,
				source.PcColorectalComments,
				source.PcFluShotRank,
				source.PcFluShotConfirmed,
				source.PcFluShotReceived,
				source.PcFluShotComments,
				source.PcPneumoShotRank,
				source.PcPneumoShotConfirmed,
				source.PcPneumoShotReceived,
				source.PcPneumoShotComments,
				source.PcBmiScreenRank,
				source.PcBmiScreenConfirmed,
				source.PcBmiCalculated,
				source.PcBmiNormal,
				source.PcBmiPlan,
				source.PcBmiComments,
				source.PcTobaccoRank,
				source.PcTobaccoConfirmed,
				source.PcTobaccoScreen,
				source.PcTobaccoCounsel,
				source.PcTobaccoComments,
				source.PcBloodPressureRank,
				source.PcBloodPressureConfirmed,
				source.PcBloodPressureScreen,
				source.PcBloodPressureNormal,
				source.PcBloodPressureFollowUp,
				source.PcBloodPressureComments,
				source.PcDepressionRank,
				source.PcDepressionScreen,
				source.PcDepressionConfirmed,
				source.PcDepressionPositive,
				source.PcDepressionPlan,
				source.PcDepressionComments,
				GETUTCDATE(),   
				GETUTCDATE(),
				1,
				1);

	
	--------------------------------------------------------------------------------
	-- update the additional fields if they aren't already populated
	--------------------------------------------------------------------------------
	UPDATE gpm
		SET gpm.MedicalRecordFound = r.[medical-record-found],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MedicalRecordFound IS NULL
		AND r.[medical-record-found] IS NOT NULL;

	UPDATE gpr
		SET gpr.MedicalRecordNumber = r.[medical-record-number],
		gpr.ModifyDateTime = GETUTCDATE()
	FROM GproPatientRanking gpr 
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpr.MedicalRecordNumber IS NULL
		AND r.[medical-record-number] IS NOT NULL;

	UPDATE gpm
		SET gpm.MedicalNotQualifiedReason = r.[medical-not-qualified-reason],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MedicalNotQualifiedReason IS NULL
		AND r.[medical-not-qualified-reason] IS NOT NULL;

	UPDATE gpm
		SET gpm.MedicalNotQualifiedDate = r.[medical-not-qualified-date],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MedicalNotQualifiedDate IS NULL
		AND r.[medical-not-qualified-date] IS NOT NULL;

	UPDATE gpm
		SET gpm.CareFallsConfirmed = r.[carefalls-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.CareFallsConfirmed IS NULL
		AND r.[carefalls-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
	SET gpm.FallsScreening = r.[falls-screening],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.FallsScreening IS NULL
		AND r.[falls-screening] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.CadConfirmed = r.[cad-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.CadConfirmed IS NULL
		AND r.[cad-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.CadDiabetesLvsd = r.[cad-diabetes-lvsd],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.CadDiabetesLvsd IS NULL
		AND r.[cad-diabetes-lvsd] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.CadAceArb = r.[cad-acearb],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.CadAceArb IS NULL
		AND r.[cad-acearb] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.CadComments = r.[cad-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.CadComments IS NULL
		AND r.[cad-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.DmConfirmed = r.[dm-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.DmConfirmed IS NULL
		AND r.[dm-confirmed] IS NOT NULL;

	UPDATE gpm
		SET gpm.DmHba1cTest = r.[dm-hba1c-test],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.DmHba1cTest IS NULL
		AND r.[dm-hba1c-test] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.DmHba1cDate = r.[dm-hba1c-date],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.DmHba1cDate IS NULL
		AND r.[dm-hba1c-date] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.DmHba1cValue = r.[dm-hba1c-value],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.DmHba1cValue IS NULL
		AND r.[dm-hba1c-value] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.DmEyeExam = r.[dm-eye-exam],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.DmEyeExam IS NULL
		AND r.[dm-eye-exam] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.DmComments = r.[dm-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.DmComments IS NULL
		AND r.[dm-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;


	UPDATE gpm
		SET gpm.DmComments = r.[dm-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.DmComments IS NULL
		AND r.[dm-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.HfConfirmed = r.[hf-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HfConfirmed IS NULL
		AND r.[hf-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.HfLvsd = r.[hf-lvsd],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HfLvsd IS NULL
		AND r.[hf-lvsd] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.HfBetaBlocker = r.[hf-beta-blocker],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HfBetaBlocker IS NULL
		AND r.[hf-beta-blocker] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.HfComments = r.[htn-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HfComments IS NULL
		AND r.[hf-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.HtnConfirmed = r.[htn-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HtnConfirmed IS NULL
		AND r.[htn-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;
		
	UPDATE gpm
		SET gpm.HtnRecentBp = r.[htn-recent-bp],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HtnRecentBp IS NULL
		AND r.[htn-recent-bp] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.HtnBpDate = r.[htn-bp-date],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HtnBpDate IS NULL
		AND r.[htn-bp-date] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.HtnBpSystolic = r.[htn-bp-systolic],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HtnBpSystolic IS NULL
		AND r.[htn-bp-systolic] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.HtnBpDiastolic = r.[htn-bp-diastolic],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HtnBpDiastolic IS NULL
		AND r.[htn-bp-diastolic] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.HtnComments = r.[htn-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.HtnComments IS NULL
		AND r.[htn-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.IvdConfirmed = r.[ivd-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.IvdConfirmed IS NULL
		AND r.[ivd-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.IvdAntithrombotic = r.[ivd-antithrombotic],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.IvdAntithrombotic IS NULL
		AND r.[ivd-antithrombotic] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.IvdComments = r.[ivd-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.IvdComments IS NULL
		AND r.[ivd-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhConfirmed = r.[mh-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhConfirmed IS NULL
		AND r.[mh-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhIndexPerformed = r.[mh-index-performed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhIndexPerformed IS NULL
		AND r.[mh-index-performed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhIndexTest = r.[mh-index-test],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhIndexTest IS NULL
		AND r.[mh-index-test] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhIndexDate = r.[mh-index-date],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhIndexDate IS NULL
		AND r.[mh-index-date] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhIndexScore = r.[mh-index-score],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhIndexScore IS NULL
		AND r.[mh-index-score] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhFollowupPerformed = r.[mh-followup-performed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhFollowupPerformed IS NULL
		AND r.[mh-followup-performed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhFollowupTest = r.[mh-followup-test],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhFollowupTest IS NULL
		AND r.[mh-followup-test] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhFollowupDate = r.[mh-followup-date],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhFollowupDate IS NULL
		AND r.[mh-followup-date] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhFollowupScore = r.[mh-followup-score],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhFollowupScore IS NULL
		AND r.[mh-followup-score] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.MhComments = r.[mh-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.MhComments IS NULL
		AND r.[mh-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcMammogramConfirmed = r.[pcmammogram-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcMammogramConfirmed IS NULL
		AND r.[pcmammogram-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcMammogramPerformed = r.[pcmammogram],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcMammogramPerformed IS NULL
		AND r.[pcmammogram] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcMammogramComments = r.[pcmammogram-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcMammogramComments IS NULL
		AND r.[pcmammogram-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcColorectalConfirmed = r.[pccolorectal-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcColorectalConfirmed IS NULL
		AND r.[pccolorectal-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcColorectalPerformed = r.[pccolorectal],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcColorectalPerformed IS NULL
		AND r.[pccolorectal] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcColorectalComments = r.[pccolorectal-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcColorectalComments IS NULL
		AND r.[pccolorectal-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcFluShotConfirmed = r.[pcflushot-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcFluShotConfirmed IS NULL
		AND r.[pcflushot-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcFluShotReceived = r.[pcflushot],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcFluShotReceived IS NULL
		AND r.[pcflushot] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcFluShotComments = r.[pcflushot-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcFluShotComments IS NULL
		AND r.[pcflushot-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcPneumoShotConfirmed = r.[pcpneumoshot-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcPneumoShotConfirmed IS NULL
		AND r.[pcpneumoshot-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcPneumoShotReceived = r.[pcpneumoshot],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcPneumoShotReceived IS NULL
		AND r.[pcpneumoshot] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcPneumoShotComments = r.[pcpneumoshot-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcPneumoShotComments IS NULL
		AND r.[pcpneumoshot-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcBmiScreenConfirmed = r.[pcbmiscreen-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBmiScreenConfirmed IS NULL
		AND r.[pcbmiscreen-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcBmiCalculated = r.[pcbmicalculated],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBmiCalculated IS NULL
		AND r.[pcbmicalculated] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcBmiNormal = r.[pcbminormal],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBmiNormal IS NULL
		AND r.[pcbminormal] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcBmiPlan = r.[pcbmiplan],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBmiPlan IS NULL
		AND r.[pcbmiplan] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcBmiComments = r.[pcbmi-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBmiComments IS NULL
		AND r.[pcbmi-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcTobaccoConfirmed = r.[pctobaccouse-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcTobaccoConfirmed IS NULL
		AND r.[pctobaccouse-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcTobaccoScreen = r.[pctobaccoscreen],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcTobaccoScreen IS NULL
		AND r.[pctobaccoscreen] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcTobaccoCounsel = r.[pctobaccocounsel],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcTobaccoCounsel IS NULL
		AND r.[pctobaccocounsel] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcTobaccoComments = r.[pctobaccouse-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcTobaccoComments IS NULL
		AND r.[pctobaccouse-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcBloodPressureConfirmed = r.[pcbloodpressure-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBloodPressureConfirmed IS NULL
		AND r.[pcbloodpressure-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;
	
	UPDATE gpm
		SET gpm.PcBloodPressureScreen = r.[pcbloodpressure],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBloodPressureScreen IS NULL
		AND r.[pcbloodpressure] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcBloodPressureNormal = r.[pcbpnormal],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBloodPressureNormal IS NULL
		AND r.[pcbpnormal] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;
	
	UPDATE gpm
		SET gpm.PcBloodPressureFollowUp = r.[pcbpfollowup],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBloodPressureFollowUp IS NULL
		AND r.[pcbpfollowup] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcBloodPressureComments = r.[pcbp-comment],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcBloodPressureComments IS NULL
		AND r.[pcbp-comment] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcDepressionScreen = r.[pcdepression],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcDepressionScreen IS NULL
		AND r.[pcdepression] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcDepressionConfirmed = r.[pcdepression-confirmed],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcDepressionConfirmed IS NULL
		AND r.[pcdepression-confirmed] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcDepressionPositive = r.[pcdepression-positive],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcDepressionPositive IS NULL
		AND r.[pcdepression-positive] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcDepressionPlan = r.[pcdepression-plan],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcDepressionPlan IS NULL
		AND r.[pcdepression-plan] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET gpm.PcDepressionComments = r.[pcdepression-comments],
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE gpm.PcDepressionComments IS NULL
		AND r.[pcdepression-comments] IS NOT NULL
		AND r.ImportSetId = @ImportSetId;

	UPDATE gpm
		SET 
		gpm.PcStatinAscvd = (CASE WHEN gpm.PcStatinAscvd IS NULL THEN r.[pcstatin-ascvd] ELSE gpm.PcStatinAscvd END),
		gpm.PcStatinLdlc = (CASE WHEN gpm.PcStatinLdlc IS NULL THEN r.[pcstatin-ldlc] ELSE gpm.PcStatinLdlc END),
		gpm.PcStatinDiabetes = (CASE WHEN gpm.PcStatinDiabetes IS NULL THEN r.[pcstatin-diabetes] ELSE gpm.PcStatinDiabetes END),
		gpm.PcStatinLdlcReading = (CASE WHEN gpm.PcStatinLdlcReading IS NULL THEN r.[pcstatin-ldlcreading] ELSE gpm.PcStatinLdlcReading END),
		gpm.PcStatinPrescribed = (CASE WHEN gpm.PcStatinPrescribed IS NULL THEN r.[pcstatin-prescribed] ELSE gpm.PcStatinPrescribed END),
		gpm.PcStatinComments = (CASE WHEN PcStatinComments IS NULL AND r.[pcstatin-comments] IS NOT NULL THEN r.[pcstatin-comments] ELSE PcStatinComments END),
		gpm.ModifyDateTime = GETUTCDATE(),
		gpm.ModifyLbUserId = 1
	FROM GproPatientMeasure gpm
		INNER JOIN GproPatientRanking gpr ON gpr.GproPatientRankingId = gpm.GproPatientRankingId
		INNER JOIN GproPatient_Raw r ON r.[medicare-id] = gpr.MedicareHicn AND r.ImportSetId = @ImportSetId
	WHERE r.ImportSetId = @ImportSetId;

END



GO
