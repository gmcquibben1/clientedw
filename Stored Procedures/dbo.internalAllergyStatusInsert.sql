SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalAllergyStatusInsert] 
    @AllergyStatusTypeId INT OUTPUT ,
	@AllergyStatus VARCHAR(255)  = NULL
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE 
	DECLARE @intError INTEGER, 
	        @DeleteInd BIT = 0,
			@LBUserId INT = 1
    --DECLARE @DiagnosisCodeId INT

	IF @AllergyStatus IS NOT NULL 
	BEGIN

		BEGIN TRAN

		SELECT @AllergyStatusTypeId = AllergyStatusTypeId FROM AllergyStatusType
		WHERE Name = @AllergyStatus

		IF @AllergyStatusTypeId IS NULL
		BEGIN
			INSERT INTO [dbo].AllergyStatusType
				   (Name
				   ,DisplayValue
				   ,[DeleteInd]
				   ,[CreateDateTime]
				   ,[ModifyDateTime]
				   ,[CreateLBUserId]
				   ,[ModifyLBUserId])
			VALUES 
			(
				@AllergyStatus,
				@AllergyStatus,
				@DeleteInd,
				GetDate(),
				GetDate(),
				@LBUserId,
				@LBUserId
			)
			SELECT @intError = @@ERROR,
				@AllergyStatusTypeId = SCOPE_IDENTITY()
		END

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
		END
		ELSE
		BEGIN
			COMMIT TRAN
		END

	END
		
	RETURN @intError



GO
