SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ETLUpdatePatientPCPNPI]
/*===============================================================
	CREATED BY: 	Youping
	CREATED ON:		2017-01-16
	INITIAL VER:	2.2.1
	MODULE:			Patient		
	DESCRIPTION:	Populate Patient PrimaryCareProviderNPI into Patient table
	PARAMETERS:		@fullload 
	RETURN VALUE(s)/OUTPUT:	
	PROGRAMMIN NOTES: @fullload default value N, value Y for full load only
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2017-01-16		YL		    LBETL-444	Populate Patient PrimaryCareProviderNPI into Patient table

=================================================================*/
(
    @fullload CHAR(1)='N'
)

AS
BEGIN

    SET NOCOUNT ON;
	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(100) = 'Patient';
	DECLARE @dataSource VARCHAR(20) = 'interface';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLUpdatePatientPCPNPI';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @Today DATE=GETUTCDATE();

	--DECLARE @fullload CHAR(1)='Y';

	DECLARE @DateFrom datetime;

	SET @DateFrom=CASE @fullload WHEN  'N' THEN DATEADD(day,-5,GETDATE()) ELSE '1900-01-01' END

   BEGIN TRY

			SELECT [InterfaceSystemId]
			  ,[PatientIdentifier]
			  ,[PrimaryCareProviderNPI]
			  ,[CreateDateTime]
			  , Row_Number() OVER ( PARTITION BY [InterfaceSystemId]
			  ,[PatientIdentifier]  ORDER BY  [CreateDateTime] DESC) rnum
			  INTO #tmpPrimaryCareProviderNPI
		  FROM [dbo].[vwportal_InterfacePatient]
		  WHERE ISNULL([PrimaryCareProviderNPI],'')<> '' AND [CreateDateTime]> @DateFrom;

		;
		 CREATE INDEX IDX_rnum_PrimaryCareProviderNPI ON #tmpPrimaryCareProviderNPI (rnum);

		  SELECT ISS.SourceSystemId 
			  ,[PatientIdentifier]
			  ,[PrimaryCareProviderNPI]
			  ,t1.[CreateDateTime]
			  INTO #PatientPrimaryCareProviderNPI
		  FROM #tmpPrimaryCareProviderNPI t1
		  INNER JOIN [dbo].[vwPortal_InterfaceSystem]  ISS ON t1.InterfaceSystemId = ISS.[InterfaceSystemId]
		  WHERE rnum=1;

		   CREATE INDEX IDX_PatientIdentifier_PatientPrimaryCareProviderNPI ON #PatientPrimaryCareProviderNPI ([SourceSystemId],[PatientIdentifier]);


		   SELECT idRef.LbPatientId AS PatientID,
				   t1.[PrimaryCareProviderNPI],
				   Row_Number() OVER ( PARTITION BY idRef.LbPatientId ORDER BY t1.[CreateDateTime] DESC) rnum
		   INTO #tmpPatientPCPNPI
		   FROM #PatientPrimaryCareProviderNPI t1
		   INNER JOIN dbo.PatientIdReference idRef ON idRef.SourceSystemId = t1.SourceSystemId 
						  AND idRef.ExternalId = t1.PatientIdentifier 
						  AND idRef.IdTypeDesc ='InterfacePatientIdentifier'
						  AND idRef.DeleteInd =0;

    
			SELECT * INTO #PatientPCPNPI FROM #tmpPatientPCPNPI WHERE rnum=1; 

			CREATE CLUSTERED INDEX IDX_PatientI_PatientPCPNPI ON #PatientPCPNPI (PatientID);

			 MERGE vwPortal_Patient T		 
				 USING #PatientPCPNPI S
				 ON T.[PatientID] = S.[PatientID]
				 WHEN MATCHED AND  ISNULL(T.[PcpNationalProviderIdentifier],'') != S.[PrimaryCareProviderNPI]
				  THEN UPDATE SET   T.[PcpNationalProviderIdentifier] = S.[PrimaryCareProviderNPI]
			;

	
		 -- get total records from  #PatientPCPNPI
		       	SELECT @UpdatedRecord=count(1)
				FROM  #PatientPCPNPI;

				-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
		
                DROP TABLE #PatientPCPNPI, #tmpPatientPCPNPI, #tmpPrimaryCareProviderNPI, #PatientPrimaryCareProviderNPI;


	END TRY
	
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

 END

GO
