SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FaceSheetMeasureCalculation]
	@PatientId INTEGER
AS
BEGIN
	--This temp table will hold all of the rows to be displayed.
	DECLARE @FacesheetMeasure TABLE 
	(
		MeasureName		VARCHAR(20),
		MeasureValue	VARCHAR(20),
		MeasureDate		DATETIME,
		SecondaryMeasureValue VARCHAR(7),
		ReturnFlag		VARCHAR(10) NULL
	)	

	--first find the transactional db name, we need it to get the age and sex
	DECLARE @DBName VARCHAR(50);
	SET @DBName = (SELECT db_name());
	SET @DBName = SUBSTRING(@DBName, 1, CHARINDEX('Edw',@DBName)-1) + 'Lbportal' + SUBSTRING(@DBName, CHARINDEX('Edw',@DBName) + 3, 100);

	--calculate BP
	IF EXISTS (SELECT 1 FROM dbo.PatientVitals WHERE PatientId = @PatientId AND 
					BloodPressureSystolic IS NOT NULL AND BloodPressureSystolic <> 0 
					AND BloodPressureDiastolic IS NOT NULL AND BloodPressureDiastolic <> 0)
	BEGIN
		INSERT INTO @FacesheetMeasure
			(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue, ReturnFlag)
		SELECT TOP 2
			'BP' AS MeasureName,
			CAST(dbo.PatientVitals.BloodPressureSystolic AS varchar(3)) + '/' + CAST(dbo.PatientVitals.BloodPressureDiastolic AS varchar(3)) AS MeasureValue,
			dbo.PatientVitals.ServiceDateTime AS MeasureDate,
			'' AS SecondaryMeasureValue, 
			CASE WHEN (BloodPressureSystolic >= 140) THEN 'HIGH'
				WHEN (BloodPressureDiastolic  >= 90) THEN 'HIGH'
				ELSE NULL
			END AS ReturnFlag
		FROM dbo.PatientVitals
		WHERE dbo.PatientVitals.PatientId = @PatientId
			 AND BloodPressureSystolic IS NOT NULL AND BloodPressureSystolic <> 0
			 AND BloodPressureDiastolic IS NOT NULL AND BloodPressureDiastolic <> 0
		ORDER BY dbo.PatientVitals.ServiceDateTime DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetMeasure
			(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue)
		VALUES
			('BP', '', NULL, '');
	END
	
	IF EXISTS (SELECT 1 FROM dbo.PatientVitals WHERE PatientId = @PatientId)
	BEGIN
		INSERT INTO @FacesheetMeasure
			(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue)
		VALUES
			('ASCVD', '', NULL, '');
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetMeasure
			(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue)
		VALUES
			('ASCVD', '', NULL, '');
	END

	IF EXISTS (SELECT TOP 1 * FROM dbo.PatientVitals WHERE PatientId = @PatientId  
			AND PatientVitals.WeightKg IS NOT NULL AND PatientVitals.WeightKg > 0 
			AND PatientVitals.HeightCM IS NOT NULL AND PatientVitals.HeightCM > 0
			AND PatientVitals.HeightCM > PatientVitals.WeightKg
		ORDER BY dbo.PatientVitals.ServiceDateTime DESC )
	BEGIN
		INSERT INTO @FacesheetMeasure
			(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue, ReturnFlag)
		SELECT TOP 1
			'BMI' + ' (' + CAST(CAST(ROUND(dbo.PatientVitals.WeightKg * 2.20462, 2) as numeric(20,0)) as varchar(10)) + ' lbs)' AS MeasureName,
			CAST(CAST(ROUND((((dbo.PatientVitals.WeightKg * 2.20462) / ((dbo.PatientVitals.HeightCM * 0.393701) * (dbo.PatientVitals.HeightCM * 0.393701))) * 703), 2) AS numeric(7,2)) AS varchar(7)) AS MeasureValue,
			dbo.PatientVitals.ServiceDateTime AS MeasureDate,
			CAST(CAST(ROUND(dbo.PatientVitals.WeightKg * 2.20462, 2) as numeric(20,2)) as varchar(10)) AS SecondaryMeasureValue,
			CASE WHEN (ROUND((((dbo.PatientVitals.WeightKg * 2.20462) / ((dbo.PatientVitals.HeightCM * 0.393701) * (dbo.PatientVitals.HeightCM * 0.393701))) * 703), 2) >= 30) THEN 'HIGH'
				WHEN (ROUND((((dbo.PatientVitals.WeightKg * 2.20462) / ((dbo.PatientVitals.HeightCM * 0.393701) * (dbo.PatientVitals.HeightCM * 0.393701))) * 703), 2)  <= 18.9) THEN 'LOW'
				ELSE NULL
			END AS ReturnFlag
		FROM dbo.PatientVitals
		WHERE dbo.PatientVitals.PatientId = @PatientId 
			AND PatientVitals.WeightKg IS NOT NULL AND PatientVitals.WeightKg > 0 
			AND PatientVitals.HeightCM IS NOT NULL AND PatientVitals.HeightCM > 0
			AND PatientVitals.HeightCM > PatientVitals.WeightKg
		ORDER BY dbo.PatientVitals.ServiceDateTime DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetMeasure
			(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue)
		VALUES
			('BMI', '', NULL, '');
	END
	

	IF EXISTS (SELECT 1 FROM dbo.PatientLabResult plr INNER JOIN dbo.PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId 
		AND (plr.ObservationCode1 IN ('GFR', 'GFRAA', '25000210', '25000220', '100791', '100797', '100784', '100786', '100792', 'GFRESTMATE', '48642-3', '48643-1', '33914-3') 
			OR plr.ObservationCode2 IN ('GFR', 'GFRAA', '25000210', '25000220', '100791', '100797', '100784', '100786', '100792', 'GFRESTMATE', '48642-3', '48643-1', '33914-3')) 
		AND (ISNUMERIC(plr.Value) = 1 OR (plr.Value LIKE '>%' AND ISNUMERIC(SUBSTRING(plr.Value, 2, 100)) = 1)))
	BEGIN
		--Check if this patient is african-american
		DECLARE @IsAfricanAmerican INT;
		DECLARE @RaceSQL NVARCHAR(4000);
		SET @RaceSQL = N'SELECT @IsAfricanAmerican = (CASE WHEN EXISTS (SELECT 1 
							FROM ' + @DBName + '.dbo.Patient p 
								INNER JOIN ' + @DBName + '.dbo.Individual i ON i.IndividualId = p.IndividualId
								INNER JOIN ' + @DBName + '.dbo.IndividualRace ir ON ir.IndividualId = i.IndividualId
								INNER JOIN ' + @DBName + '.dbo.RaceType rt ON rt.RaceTypeID = ir.RaceTypeID
							WHERE rt.Name LIKE ''%african%''
								AND p.PatientId = @PatientId) THEN 1 ELSE 0 END)';
		EXEC sp_executesql @RaceSQL, N'@PatientId INT,@IsAfricanAmerican INT OUTPUT', @PatientId = @PatientId, @IsAfricanAmerican = @IsAfricanAmerican OUTPUT;

		IF @IsAfricanAmerican = 1 AND EXISTS (SELECT 1 FROM PatientLabResult plr INNER JOIN PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId AND (plr.ObservationCode1 IN ('GFRAA', '25000220', '100797', '100786', '100792', '48643-1') 
					OR plr.ObservationCode2 IN ('GFRAA', '25000220', '100797', '100786', '100792', '48643-1')))
		BEGIN
			--if AA and has AA codes display those
			INSERT INTO @FacesheetMeasure
				(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue)
			SELECT TOP 1
				'GFR w/ CKD' AS MeasureName,
				(CASE WHEN ISNUMERIC(plr.Value) = 0 THEN 
						(CASE WHEN plr.Value LIKE '>%' THEN
						(CASE WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) > 89 THEN RTRIM(plr.Value) + '  [ Stage 1 ]'
						WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) >= 60 AND CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) <= 89 THEN RTRIM(plr.Value) + '  [ Stage 2 ]'
						WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) >= 30 AND CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) <= 59 THEN RTRIM(plr.Value) + '  [ Stage 3 ]'
						WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) >= 15 AND CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) <= 29 THEN RTRIM(plr.Value) + '  [ Stage 4 ]'
						WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) < 15 THEN RTRIM(plr.Value) + '  [ Stage 5 ]'
						ELSE ''
						END) ELSE '' END)
				ELSE (CASE WHEN CAST(plr.Value AS Decimal) > 89 THEN RTRIM(plr.Value) + '  [ Stage 1 ]'
						WHEN CAST(plr.Value AS Decimal) >= 60 AND CAST(plr.Value AS Decimal) <= 89 THEN RTRIM(plr.Value) + '  [ Stage 2 ]'
						WHEN CAST(plr.Value AS Decimal) >= 30 AND CAST(plr.Value AS Decimal) <= 59 THEN RTRIM(plr.Value) + '  [ Stage 3 ]'
						WHEN CAST(plr.Value AS Decimal) >= 15 AND CAST(plr.Value AS Decimal) <= 29 THEN RTRIM(plr.Value) + '  [ Stage 4 ]'
						WHEN CAST(plr.Value AS Decimal) < 15 THEN RTRIM(plr.Value) + '  [ Stage 5 ]'
						ELSE ''
						END) END) AS ResultValue,
				plr.ObservationDate AS MeasureDate,
				RTRIM(plr.Value) AS SecondaryMeasureValue
			FROM dbo.PatientLabResult plr INNER JOIN dbo.PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId
				AND (plr.ObservationCode1 IN ('GFRAA', '25000220', '100797', '100786', '100792', '48643-1') 
					OR plr.ObservationCode2 IN ('GFRAA', '25000220', '100797', '100786', '100792', '48643-1'))
				AND (ISNUMERIC(plr.Value) = 1 OR (plr.Value LIKE '>%' AND ISNUMERIC(SUBSTRING(plr.Value, 2, 100)) = 1))
			ORDER BY plr.ObservationDate DESC
		END
		ELSE IF @IsAfricanAmerican = 0 AND EXISTS (SELECT 1 FROM dbo.PatientLabResult plr INNER JOIN dbo.PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId AND (plr.ObservationCode1 IN ('GFR', '25000210', '100791', '100784', 'GFRESTMATE', '33914-3') 
					OR plr.ObservationCode2 IN ('GFR', '25000210', '100791', '100784', 'GFRESTMATE', '33914-3')))
		BEGIN
			--else if not AA and has non-AA codes display those
			INSERT INTO @FacesheetMeasure
				(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue)
			SELECT TOP 1
				'GFR w/ CKD' AS MeasureName,
				(CASE WHEN ISNUMERIC(plr.Value) = 0 THEN 
						(CASE WHEN plr.Value LIKE '>%' THEN
						(CASE WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) > 89 THEN RTRIM(plr.Value) + '  [ Stage 1 ]'
						WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) >= 60 AND CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) <= 89 THEN RTRIM(plr.Value) + '  [ Stage 2 ]'
						WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) >= 30 AND CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) <= 59 THEN RTRIM(plr.Value) + '  [ Stage 3 ]'
						WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) >= 15 AND CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) <= 29 THEN RTRIM(plr.Value) + '  [ Stage 4 ]'
						WHEN CAST(SUBSTRING(plr.Value, 2, 10)  AS Decimal) < 15 THEN RTRIM(plr.Value) + '  [ Stage 5 ]'
						ELSE ''
						END) ELSE '' END)
				ELSE (CASE WHEN CAST(plr.Value AS Decimal) > 89 THEN RTRIM(plr.Value) + '  [ Stage 1 ]'
						WHEN CAST(plr.Value AS Decimal) >= 60 AND CAST(plr.Value AS Decimal) <= 89 THEN RTRIM(plr.Value) + '  [ Stage 2 ]'
						WHEN CAST(plr.Value AS Decimal) >= 30 AND CAST(plr.Value AS Decimal) <= 59 THEN RTRIM(plr.Value) + '  [ Stage 3 ]'
						WHEN CAST(plr.Value AS Decimal) >= 15 AND CAST(plr.Value AS Decimal) <= 29 THEN RTRIM(plr.Value) + '  [ Stage 4 ]'
						WHEN CAST(plr.Value AS Decimal) < 15 THEN RTRIM(plr.Value) + '  [ Stage 5 ]'
						ELSE ''
						END) END) AS ResultValue,
				plr.ObservationDate AS MeasureDate,
				RTRIM(plr.Value) AS SecondaryMeasureValue
			FROM dbo.PatientLabResult plr INNER JOIN dbo.PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
			WHERE plo.PatientId = @PatientId
				AND (plr.ObservationCode1 IN ('GFR', '25000210', '100791', '100784', 'GFRESTMATE', '33914-3') 
					OR plr.ObservationCode2 IN ('GFR', '25000210', '100791', '100784', 'GFRESTMATE', '33914-3'))
				AND (ISNUMERIC(plr.Value) = 1 OR (plr.Value LIKE '>%' AND ISNUMERIC(SUBSTRING(plr.Value, 2, 100)) = 1))
			ORDER BY plr.ObservationDate DESC
		END
		ELSE
		BEGIN
			INSERT INTO @FacesheetMeasure
				(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue)
			VALUES
				('GFR w/ CKD Levels', '', NULL, '');
		END
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetMeasure
			(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue)
		VALUES
			('GFR w/ CKD Levels', '', NULL, '');
	END
--
	--IF EXISTS (	SELECT TOP 1 plr.Value AS ResultValue, plr.Units AS ResultUnit, plr.ObservationDate AS ResultDate
	--	FROM PatientLabResult plr INNER JOIN PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
	--	WHERE plo.PatientId = @PatientId
	--	AND (plr.ObservationCode1 IN ('GFR', 'GFRAA', '25000210', '25000220', '100791', '100797', '100784', '100786', '100792', 'GFRESTMATE') OR plr.ObservationCode2 IN ('GFR', 'GFRAA', '25000210', '25000220', '100791', '100797', '100784', '100786', '100792', 'GFRESTMATE')) 
	--)
	--BEGIN
	--	INSERT INTO @FacesheetMeasure
	--		(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue, ReturnFlag)
	--	SELECT TOP 1
	--		'GFR',
	--		RTRIM(plr.Value) AS ResultValue,
	--		plr.ObservationDate AS MeasureDate,
	--		NULL,
	--		NULL
	--	FROM PatientLabResult plr INNER JOIN PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
	--	WHERE plo.PatientId = @PatientId
	--	AND (plr.ObservationCode1 IN ('GFR', 'GFRAA', '25000210', '25000220', '100791', '100797', '100784', '100786', '100792', 'GFRESTMATE') OR plr.ObservationCode2 IN ('GFR', 'GFRAA', '25000210', '25000220', '100791', '100797', '100784', '100786', '100792', 'GFRESTMATE'))
	--	ORDER BY plr.ObservationDate DESC
	--END
	--ELSE
	--BEGIN
	--	INSERT INTO @FacesheetMeasure
	--		(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue, ReturnFlag)
	--	VALUES
	--		('GFR', '', NULL, '', NULL);
	--END

	IF EXISTS (	SELECT 1 FROM PatientLabResult plr INNER JOIN PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
		WHERE plo.PatientId = @PatientId
		AND (plr.ObservationCode1 IN ('2160-0') OR plr.ObservationCode2 IN ('2160-0')) )
		AND EXISTS (SELECT 1 FROM PatientVitals pv 
		WHERE pv.PatientId = @PatientId
		AND pv.WeightKg IS NOT NULL AND pv.HeightCM IS NOT NULL)
	BEGIN
		--declare everything we need
		DECLARE @Gender VARCHAR(10);
		DECLARE @AgeText VARCHAR(10);
		DECLARE @Age INT;
		DECLARE @GenderSQL NVARCHAR(512);
		DECLARE @AgeSQL NVARCHAR(512);
		DECLARE @LastHeight DECIMAL;
		DECLARE @LastWeight DECIMAL;
		DECLARE @IdealBodyWeight DECIMAL;
		DECLARE @AdjustedBodyWeight DECIMAL;
		DECLARE @BMI DECIMAL;
		DECLARE @EstimatedCrClearance NUMERIC(7,2);
		DECLARE @LastCr NUMERIC(7,2);

		--get the age and sex
		SET @GenderSQL = N'SELECT @Sex = g.Name FROM ' + @DBName + '.dbo.Individual i 
			INNER JOIN ' + @DBName + '.dbo.Patient p ON p.IndividualId = i.IndividualId 
			INNER JOIN ' + @DBName + '.dbo.GenderType g ON g.GenderTypeId = i.GenderTypeId AND p.PatientId = @PatientId'
		EXEC sp_executesql @GenderSQL, N'@PatientId INT,@Sex VARCHAR(10) OUTPUT', @PatientId = @PatientId, @Sex = @Gender OUTPUT

		SET @AgeSQL = N'SELECT @AgeText = ' + @DBName + '.dbo.fnGetAge(p.BirthDate) FROM ' + @DBName + '.dbo.vwPatients p WHERE p.PatientId = @PatientId';
		EXEC sp_executesql @AgeSQL, N'@PatientId INT,@AgeText VARCHAR(10) OUTPUT', @PatientId = @PatientId, @AgeText = @AgeText OUTPUT
		SET @Age = CAST(SUBSTRING(@AgeText,1, CHARINDEX(' ', @AgeText) - 1) AS Int);

		--calculate the weights
		SET @LastHeight = (SELECT TOP 1 CAST((pv.HeightCM * 0.393701) AS NUMERIC(7,2)) FROM PatientVitals pv 
				WHERE pv.PatientId = @PatientId AND pv.HeightCM IS NOT NULL
				ORDER BY pv.ServiceDateTime DESC);
		SET @LastWeight = (SELECT TOP 1 CAST(pv.WeightKg AS NUMERIC(7,2))
				FROM PatientVitals pv 
				WHERE pv.PatientId = @PatientId AND pv.WeightKG IS NOT NULL
				ORDER BY pv.ServiceDateTime DESC);

		IF (@LastWeight > 0)
		BEGIN
			IF (@LastHeight > 0)
			BEGIN
				--if the height is available we can calculate out ideal body weights for use in the formula
				IF (SUBSTRING(@Gender, 1, 1) = 'F')
					SET @IdealBodyWeight = 45.5 + (2.3 * (@LastHeight - 60))
				IF (SUBSTRING(@Gender, 1, 1) = 'M')
					SET @IdealBodyWeight = 50 + (2.3 * (@LastHeight - 60));

				SET @BMI = (SELECT TOP 1 CAST(ROUND((((pv.WeightKg * 2.20462) / ((pv.HeightCM * 0.393701) * (pv.HeightCM * 0.393701))) * 703), 2) AS numeric(7,2)) 
							FROM PatientVitals pv 
							WHERE pv.PatientId = @PatientId 
							AND pv.WeightKg IS NOT NULL AND pv.WeightKg <> 0
							AND pv.HeightCM IS NOT NULL AND pv.HeightCM <> 0
							ORDER BY pv.ServiceDateTime DESC);

				SET @AdjustedBodyWeight = @IdealBodyWeight + (0.4 * (@LastWeight - @IdealBodyWeight));

				--set the last weight equal to the calculated out ideal or adjusted weight
				IF (@BMI IS NOT NULL)
				BEGIN
					IF (@BMI > 18.5 AND @BMI < 22.9)
						SET @LastWeight = @IdealBodyWeight;
					IF (@BMI > 22.9)
						SET @LastWeight = @AdjustedBodyWeight;
				END
			END

			SET @EstimatedCrClearance = (140-@Age) * @LastWeight;
			IF (SUBSTRING(@Gender, 1, 1) = 'F')
				SET @EstimatedCrClearance = @EstimatedCrClearance * 0.85;

			--get the most recent creatinine level and calculate CrClearance
			SET @LastCr = (SELECT TOP 1 
						CASE WHEN plr.Value LIKE '% %' THEN CAST(SUBSTRING(plr.Value,1,CHARINDEX(' ',plr.Value)-1) AS NUMERIC(7,2))
							ELSE CAST(plr.Value AS NUMERIC(7,2)) END
						FROM PatientLabResult plr INNER JOIN PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
						WHERE plo.PatientId = @PatientId AND (plr.ObservationCode1 IN ('2160-0') OR plr.ObservationCode2 IN ('2160-0')) 
						AND plr.Units LIKE '%mg/DL%' AND ISNUMERIC(plr.Value) = 1
						ORDER BY plr.ObservationDate DESC);

			SET @LastCr = 72 * @LastCr;
			IF (@LastCr > 0)
			BEGIN
				SET @EstimatedCrClearance = @EstimatedCrClearance / @LastCr;

				--add it to the return set
				INSERT INTO @FacesheetMeasure
					(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue, ReturnFlag)
				SELECT TOP 1
					'Est CrClearance',
					RTRIM(@EstimatedCrClearance) AS ResultValue,
					plr.ObservationDate AS MeasureDate,
					NULL,
					NULL
				FROM PatientLabResult plr INNER JOIN PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
				WHERE plo.PatientId = @PatientId
				AND (plr.ObservationCode1 IN ('2160-0') OR plr.ObservationCode2 IN ('2160-0')) 
				ORDER BY plr.ObservationDate DESC
			END
			ELSE
			BEGIN
				--the CR value was zero, that will break the sp with divide by zero so we have to display nothing
				INSERT INTO @FacesheetMeasure
					(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue, ReturnFlag)
				VALUES
					('Est CrClearance', '', NULL, '', NULL);
			END
		END
		ELSE
		BEGIN
			--no weight, we can't calculate this CrClearance value
			INSERT INTO @FacesheetMeasure
				(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue, ReturnFlag)
			VALUES
				('Est CrClearance', '', NULL, '', NULL);
		END
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetMeasure
			(MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue, ReturnFlag)
		VALUES
			('Est CrClearance', '', NULL, '', NULL);
	END


--
	--now spit out whatever is in the temp table
	SELECT MeasureName, MeasureValue, MeasureDate, SecondaryMeasureValue, ReturnFlag FROM @FacesheetMeasure;
END
GO
