SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalPatinetRosterResearch] @LatestRosterDate date = '12/1/2015', @ComparisonRosterDate date = null
AS

if @ComparisonRosterDate = '1/1/1900'
BEGIN
set @ComparisonRosterDate = null
end

SET NOCOUNT ON;

EXEC InternalDropTempTables
--step 1: pull all people in the roster prior to December Roster
SELECT LbPatientId, SourceFeed, max(FileDate) as LastestFileDate, try_convert(date,'1/1/1900') as TermDate
into #temp1
FROM PatientRosterEnrollmentRaw
where FileDate < @LatestRosterDate AND LbPatientId IS NOT NULL
GROUP BY LbPatientId, SourceFeed, TermDate

UPDATE T1 SET TermDate = R.TermDate FROM #temp1 T1
JOIN PatientRosterEnrollmentRaw R ON R.FileDate = T1.LastestFileDate and R.LbPatientId = T1.LbPatientId
                                  AND R.SourceFeed = T1.SourceFeed


SELECT SourceFeed, 
CASE 
WHEN @ComparisonRosterDate IS NULL THEN MAX(FileDate)
ELSE @ComparisonRosterDate
END AS FileDate
INTO #RosterCompareDate
FROM PatientRosterEnrollmentRaw
GROUP BY SourceFeed

SELECT t.* 
into #temp2
FROM #temp1 t
join #RosterCompareDate rcd on t.SourceFeed = rcd.SourceFeed and t.TermDate > rcd.FileDate


SELECT R.LbPatientId, R.SourceFeed
INTO #RosterCompare
FROM PatientRosterEnrollmentRaw R
JOIN #RosterCompareDate RCD ON R.FileDate = RCD.FileDate AND R.SourceFeed = RCD.SourceFeed

SET NOCOUNT OFF;

SELECT T.*, rcd.FileDate as [ComparisonDate], ContractName, ContractStatus, ContractSource
--into #ReportReady
FROM #temp2 T
left JOIN #RosterCompareDate rcd on T.SourceFeed = rcd.SourceFeed
LEFT JOIN #RosterCompare R ON R.LbPatientId = T.LbPatientId AND R.SourceFeed = T.SourceFeed
left join PatientContract PC ON PC.LbPatientId = T.LbPatientId and IsCurrentInd = 1
WHERE R.LbPatientId is null and T.TermDate > getdate()
GO
