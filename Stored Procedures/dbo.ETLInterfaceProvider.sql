SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLInterfaceProvider]
AS
BEGIN

/*===============================================================
	CREATED BY: 	BR
	CREATED ON:		01.14.2016
	INITIAL VER:	Initial Application Version
	MODULE:			EDW ETL		
	DESCRIPTION:	ETL SP to create new Providers.  This procedure will extract records from the Interface Provider table and create the 
					Corresponding records int the Provider, Individual, LBUserPhone, etc.... 
	PARAMETERS:		
					None

	RETURN VALUE(s)/OUTPUT:	NONE
	
 
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	1.6.1		01.14.2016		BR						Rewrote this to get rid of the inner join to attribution
	2.2.1       2017-02-16      CJL			LBETL-879	Providers with NPI 999999999 shoudl be excluded from importation from the interface table

														
=================================================================*/




	MERGE INTO ProviderExtract AS TARGET
	USING (SELECT * FROM
				(SELECT DISTINCT 
				   IP.*
				   ,ROW_NUMBER() OVER (PARTITION BY IP.ProviderNPI ORDER BY IP.CreateDateTime DESC) r
				FROM vwPortal_InterfaceProvider IP
				WHERE IP.[ProviderNPI] <> '') intrfP
			WHERE intrfP.r = 1 AND ProviderNPI <> '9999999999'
			)AS SOURCE
		ON TARGET.ProviderNPI = SOURCE.ProviderNPI
	WHEN MATCHED THEN 
		UPDATE SET TARGET.ProviderFirstName = SOURCE.FirstName, 
				   TARGET.ProviderMidInit = SOURCE.MiddleName,
				   TARGET.ProviderLastname = SOURCE.LastName,
				   TARGET.TIN = SOURCE.TIN,
				   TARGET.PrimarySpecialty = ISNULL(SOURCE.Specialty1,TARGET.PrimarySpecialty),
				   TARGET.SecondarySpecialty = ISNULL(SOURCE.Specialty2,TARGET.SecondarySpecialty),
				   TARGET.[PRIMARY Service Address] = SOURCE.Address1 ,
				   TARGET.Street2 =  SOURCE.Address2,
				   TARGET.City = ISNULL(SOURCE.City, ''),
				   TARGET.State = ISNULL(SOURCE.State, ''),
				   TARGET.[Zip Code] = ISNULL(SOURCE.[Zip], ''),
				   TARGET.phone = ISNULL(SOURCE.Phone1, ''), 
				   TARGET.Fax = SOURCE.Phone2
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([ProviderID]
			   ,[PrimarySpecialty]
			   ,[SecondarySpecialty]
			   ,[ProviderFirstName]
			   ,[ProviderLastName]
			   ,[ProviderMidInit]
			   ,[ProviderStatus]
			   ,[ProviderEffDate]
			   ,[ProviderTermDate]
			   ,[TIN]
			   ,[ProviderNPI]
			   ,[DOB]
			   ,[Gender]
			   ,[Email]
			   ,[ACO]
			   ,[IPA]
			   ,[PrimarySpecialtyStartDate]
			   ,[PrimarySpecialtyEndDate]
			   ,[SecondarySpecialtyStartDate]
			   ,[SecondarySpecialtyEndDate]
			   ,[fromFile]
			   ,[DepositingFeed]
			   ,[PRIMARY Service Address] 
			   ,Street2
				,City
				, [State]
				,[Zip Code]
				,phone
				,Fax
			   )
		VALUES (SOURCE.[ProviderNPI] --<ProviderID, nchar(50),>
			   ,SOURCE.[Specialty1] --<PrimarySpecialty, nchar(50),>
			   ,SOURCE.[Specialty2] --<SecondarySpecialty, nchar(50),>
			   ,SOURCE.[FirstName] -- <ProviderFirstName, nchar(50),>
			   ,SOURCE.[LastName] --<ProviderLastName, nchar(50),>
			   ,SOURCE.[MiddleName] -- <ProviderMidInit, nchar(50),>
			   ,NULL -- <ProviderStatus, nchar(50),>
			   ,NULL -- <ProviderEffDate, date,>
			   ,NULL -- <ProviderTermDate, date,>
			   ,SOURCE.[TIN]  -- <TIN, varchar(50),>
			   ,SOURCE.[ProviderNPI] --<ProviderNPI, varchar(50),>
			   ,NULL -- <DOB, date,>
			   ,NULL -- <Gender, nchar(50),>
			   ,'' -- <Email, nchar(50),>
			   ,NULL -- <ACO, varchar(50),>
			   ,NULL -- <IPA, nchar(50),>
			   ,NULL --<PrimarySpecialtyStartDate, date,>
			   ,NULL --<PrimarySpecialtyEndDate, date,>
			   ,NULL -- <SecondarySpecialtyStartDate, date,>
			   ,NULL -- <SecondarySpecialtyEndDate, date,>
			   ,GetDate()
			   ,'Interface'
			   , ISNULL(SOURCE.Address1, '')
			   , ISNULL(SOURCE.Address2, '')
			   , ISNULL(SOURCE.City, '')
			   , ISNULL(SOURCE.[State], '')
			   , ISNULL(SOURCE.[Zip], '')
			   , SOURCE.Phone1
			   , SOURCE.Phone2
			   );

	-- This is missing from 1.6.8 release.
	-- added to insert info into ProviderHealthcareOrg
	-- 
	MERGE INTO dbo.vwPortal_ProviderHealthcareOrg AS TARGET
	USING (SELECT 
				p.ProviderID,
				ho.HealthcareOrgID,
				ifs.SourceSystemID
			FROM vwPortal_HealthcareOrg ho
				INNER JOIN vwPortal_BusinessUnit bu ON bu.BusinessUnitId = ho.BusinessUnitId
				INNER JOIN (SELECT ProviderNPI, TIN, InterfaceSystemId, ROW_NUMBER() OVER (PARTITION BY ProviderNPI,TIN ORDER BY CreateDateTime DESC) r FROM vwPortal_InterfaceProvider WHERE ProviderNPI <> '9999999999') AS ip ON ip.TIN = bu.ExternalCode
				INNER JOIN vwPortal_InterfaceSystem ifs ON ifs.InterfaceSystemId = ip.InterfaceSystemId
				INNER JOIN vwPortal_Provider p ON p.NationalProviderIdentifier = ip.ProviderNPI
			WHERE ip.r = 1 
				AND ho.DeleteInd = 0
				AND bu.DeleteInd = 0
				AND p.DeleteInd = 0
		  ) AS SOURCE
		ON SOURCE.ProviderId = TARGET.ProviderId
			AND SOURCE.HealthcareOrgId = TARGET.HealthcareOrgId
	WHEN NOT MATCHED THEN
		INSERT (ProviderID,HealthcareOrgID,SourceSystemID,ExternalReferenceIdentifier,OtherReferenceIdentifier,DeleteInd,CreateDateTime,ModifyDateTime,CreateLBUserId,ModifyLBUserId,DefaultReportPrinterId,DefaultInd)
		VALUES (SOURCE.ProviderID,SOURCE.HealthcareOrgID,SOURCE.SourceSystemID,NULL,NULL,0,GETUTCDATE(),GETUTCDATE(),1,1,NULL,0);

END


GO
