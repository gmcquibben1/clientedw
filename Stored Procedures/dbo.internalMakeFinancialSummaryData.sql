SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[internalMakeFinancialSummaryData]
AS
BEGIN


SET NOCOUNT ON


	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'CCLF_CLM_SUMMARY';
	DECLARE @dataSource VARCHAR(20) = 'internalFinancial';
	DECLARE @ProcedureName VARCHAR(128) = 'internalMakeFinancialSummaryData';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	--DECLARE @RecordCountBefore INT=0;
	--DECLARE @RecordCountAfter  INT=0;
	--DECLARE @RecordCountSource INT=0;

	DECLARE @Today DATE=GETUTCDATE();





BEGIN TRY 

	--TRUNCATE DATA CURRENTLY IN TABLE
	TRUNCATE TABLE DBO.CCLF_CLM_SUMMARY

	/* Fill Database w/ Part-A Claims X Claim Types for all possible combinations
	*/
	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	--,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Inpatient' AS CLAIM_FEED,
	--'10' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth

	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	 -- ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Outpatient' AS CLAIM_FEED,
	--'20' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth

	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	 -- ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Professional' AS CLAIM_FEED,
	--'30' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth


	/* Professional Cost */
	UPDATE CCLF_CLM_SUMMARY SET
	  CCLF_CLM_SUMMARY.PAID = b.[Professional Cost] -- decimal(8, 0)
	FROM CCLF_CLM_SUMMARY AS a -- bigint
	JOIN MM_Metics_Pivot as b
	on a.LbPatientId = b.LbPatientId AND
	a.DOS_First = b.DOS_First
	WHERE a.CLAIM_FEED = 'Professional'

	/* Inpatient Cost */
	UPDATE CCLF_CLM_SUMMARY SET
	  PAID = b.[Inpatient Cost] -- decimal(8, 0)
	FROM CCLF_CLM_SUMMARY AS a -- bigint
	JOIN MM_Metics_Pivot as b
	on a.LbPatientId = b.LbPatientId AND
	a.DOS_First = b.DOS_First
	WHERE a.CLAIM_FEED = 'Inpatient'

	/* OLD CODE
	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part A Claims' AS CLAIM_FEED,
	'40' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth

	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part A Claims' AS CLAIM_FEED,
	'50' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth

	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part A Claims' AS CLAIM_FEED,
	'60' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth

	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part A Claims' AS CLAIM_FEED,
	'61' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth
	*/

	/* Truncate Temp Table */
	TRUNCATE TABLE CCLF_PARTA_TEMP_SUMMARY


	UPDATE CCLF_1_PartA_Header SET
	  CLM_FROM_DT_1ST = DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) -- date

	INSERT INTO CCLF_PARTA_TEMP_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	--  ,CLM_TYPE_CD
	  ,DOS_First
	  ,Paid
	  )
	SELECT
	membersMonth.id as MemberMonths_id,
	membersMonth.LbPatientId as LbPatientId, 
	'Outpatient' AS CLAIM_FEED,
	--CLM_TYPE_CD,
	--PRNCPL_DGNS_CD AS DIAG_CD, 
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate as DOS_First,
	SUM(CLM_PMT_AMT) AS Paid
	--FROM CCLF_1_PartA_Header
	FROM membersMonth
	LEFT JOIN CCLF_1_PartA_Header 
	ON dbo.membersMonth.LbPatientId = dbo.CCLF_1_PartA_Header.LbPatientId AND
	 dbo.membersMonth.visitdate = dbo.CCLF_1_PartA_Header.CLM_FROM_DT_1ST
	WHERE ISNULL(TRY_CONVERT(INT,CLM_ADJSMT_TYPE_CD),0) = 0 and 
	ISNULL(TRY_CONVERT(INT,CLM_TYPE_CD),0) IN (20,30,40,50,71,72,81,82)
	GROUP BY 
	membersMonth.id,
	membersMonth.LbPatientId, 
	--CLM_TYPE_CD,
	--PRNCPL_DGNS_CD, 
	--DATEPART("yyyy", visitdate), 
	--DATEPART("mm", visitdate), 
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) -- new field, for MemberMonth join
	visitdate

	/* Old version

	--INSERT PART-A CLAIMS INTO SUMMARY TABLE
	INSERT INTO CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	  ,[YEAR]
	  ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	) 
	SELECT
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part A Claims' AS CLAIM_FEED,
	CLM_TYPE_CD,
	--PRNCPL_DGNS_CD AS DIAG_CD, 
	DATEPART("yyyy", visitdate) AS YR, 
	DatePart("mm", visitdate) AS MTH ,
	DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	SUM(CLM_PMT_AMT) AS Paid
	--FROM CCLF_1_PartA_Header
	FROM membersMonth
	LEFT JOIN CCLF_1_PartA_Header 
	ON dbo.membersMonth.LbPatientId = dbo.CCLF_1_PartA_Header.LbPatientId AND
	 dbo.membersMonth.visitdate = dbo.CCLF_1_PartA_Header.CLM_FROM_DT_1ST
	WHERE CLM_ADJSMT_TYPE_CD = 0
	GROUP BY 
	membersMonth.id,
	LbPatientId, 
	CLM_TYPE_CD,
	--PRNCPL_DGNS_CD, 
	DATEPART("yyyy", visitdate), 
	DATEPART("mm", visitdate), 
	DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) -- new field, for MemberMonth join
	*/
	/* 
	UNION 
	SELECT 
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part A Claims' AS CLAIM_FEED,
	CLM_TYPE_CD,
	DATEPART("yyyy", visitdate) AS YR, 
	DatePart("mm", visitdate) AS MTH ,
	DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	0 AS Paid
	FROM membersMonth JOIN CLAIM_TYPE_CODES ON 1 = 1

	*/

	UPDATE CCLF_CLM_SUMMARY SET
	CCLF_CLM_SUMMARY.Paid = CCLF_PARTA_TEMP_SUMMARY.Paid
	FROM CCLF_CLM_SUMMARY JOIN CCLF_PARTA_TEMP_SUMMARY 
	ON 
	 CCLF_CLM_SUMMARY.MemberMonths_id = CCLF_PARTA_TEMP_SUMMARY.MemberMonths_id
	WHERE CCLF_CLM_SUMMARY.CLAIM_FEED = 'Outpatient'

	/* OLD PART-B CLAIMS SUMMARY QUERIES


	--INSERT PART-B PHYSICIAN CLAIMS INTO SUMMARY TABLE
	INSERT INTO CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	  ,[YEAR]
	  ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	)
	SELECT
	LbPatientId as LbPatientId, 
	'Part-B Physician Claims' AS CLAIM_FEED,  
	CLM_TYPE_CD,
	--CLM_LINE_DGNS_CD AS DIAG_CD, 
	DATEPART("yyyy", CLM_FROM_DT) AS YR, 
	DATEPART("mm", CLM_FROM_DT) AS MTH,
	DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First,  -- new field, for MemberMonth join 
	SUM(CLM_LINE_CVRD_PD_AMT) AS Paid
	--FROM CCLF_5_PartB_Physicians
	FROM membersMonth LEFT JOIN CCLF_1_PartA_Header 
	ON dbo.membersMonth.LbPatientId = dbo.CCLF_5_PartB_Physicians.LbPatientId AND
	 dbo.membersMonth.visitdate = dbo.CCLF_5_PartB_Physicians.CLM_FROM_DT_1ST
	WHERE CLM_ADJSMT_TYPE_CD = 0
	GROUP BY LbPatientId, 
	CLM_TYPE_CD,
	--CLM_LINE_DGNS_CD, 
	DATEPART("yyyy", CLM_FROM_DT), 
	DATEPART("mm", CLM_FROM_DT),
	DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) -- new field, for MemberMonth join


	--INSERT PART-B DME CLAIMS INTO SUMMARY TABLE
	INSERT INTO CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	  ,[YEAR]
	  ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	)
	SELECT 
	LbPatientId as LbPatientId, 
	'Part-B DME Claims' AS CLAIM_FEED,  
	CLM_TYPE_CD,
	DATEPART("yyyy", CLM_FROM_DT) AS YR, 
	DATEPART("mm", CLM_FROM_DT) AS MTH, 
	DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) AS DOS_First,  -- new field, for MemberMonth join
	SUM(CLM_LINE_CVRD_PD_AMT) AS Paid
	--FROM CCLF_6_PartB_DME
	FROM membersMonth LEFT JOIN CCLF_1_PartA_Header 
	ON dbo.membersMonth.LbPatientId = dbo.CCLF_6_PartB_DME.LbPatientId AND
	 dbo.membersMonth.visitdate = dbo.CCLF_6_PartB_DME.CLM_FROM_DT_1ST 
	GROUP BY LbPatientId, 
	CLM_TYPE_CD,
	DATEPART("yyyy", CLM_FROM_DT), 
	DATEPART("mm", CLM_FROM_DT),
	DATEADD(DAY, -(DAY(CLM_FROM_DT) - 1), CLM_FROM_DT) -- new field, for MemberMonth join

	-- END OLD CODE
	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part B Claims' AS CLAIM_FEED,
	'72' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth


	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part B Claims' AS CLAIM_FEED,
	'71' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth


	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part B Claims' AS CLAIM_FEED,
	'81' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth


	INSERT INTO 
	CCLF_CLM_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLAIM_TYPE
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	  )
	  SELECT --TOP 1000000  --FOR TESTING
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part B Claims' AS CLAIM_FEED,
	'82' as CLM_TYPE_CD,
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate,
	0 AS Paid
	FROM membersMonth

	--TRUNCATE PART-B SUMMARY TABLE
	TRUNCATE TABLE CCLF_PARTB_TEMP_SUMMARY

	--INSERT PART-B PHYSICIAN CLAIMS INTO SUMMARY TABLE
	INSERT INTO CCLF_PARTB_TEMP_SUMMARY (
	   MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLM_TYPE_CD
	  --,DIAG_CD
	  --,[YEAR]
	  --,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	)
	SELECT --TOP 1000
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part B Claims' AS CLAIM_FEED,
	CLM_TYPE_CD,
	--PRNCPL_DGNS_CD AS DIAG_CD, 
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate as DOS_First,
	SUM(CLM_LINE_CVRD_PD_AMT) AS Paid
	--FROM CCLF_1_PartA_Header
	FROM membersMonth
	LEFT JOIN CCLF_5_PartB_Physicians 
	ON dbo.membersMonth.LbPatientId = dbo.CCLF_5_PartB_Physicians.LbPatientId AND
	 dbo.membersMonth.visitdate = dbo.CCLF_5_PartB_Physicians.CLM_FROM_DT_1ST
	WHERE CLM_ADJSMT_TYPE_CD = 0
	GROUP BY 
	membersMonth.id,
	LbPatientId, 
	CLM_TYPE_CD,
	--PRNCPL_DGNS_CD, 
	--DATEPART("yyyy", visitdate), 
	--DATEPART("mm", visitdate), 
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) -- new field, for MemberMonth join
	visitdate


	--INSERT PART-B DME CLAIMS INTO SUMMARY TABLE
	INSERT INTO CCLF_PARTB_TEMP_SUMMARY (
	  MemberMonths_id
	  ,LbPatientId
	  ,CLAIM_FEED
	  ,CLM_TYPE_CD
	  --,DIAG_CD
	 -- ,[YEAR]
	 -- ,[MONTH]
	  ,DOS_First -- new field, for MemberMonth join
	  ,PAID
	)
	SELECT --TOP 1000
	membersMonth.id as MemberMonths_id,
	LbPatientId as LbPatientId, 
	'Part B Claims' AS CLAIM_FEED,
	CLM_TYPE_CD,
	--PRNCPL_DGNS_CD AS DIAG_CD, 
	--DATEPART("yyyy", visitdate) AS YR, 
	--DatePart("mm", visitdate) AS MTH ,
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) AS DOS_First,  -- new field, for MemberMonth join
	visitdate as DOS_First,
	SUM(CLM_LINE_CVRD_PD_AMT) AS Paid
	--FROM CCLF_1_PartA_Header
	FROM membersMonth
	LEFT JOIN CCLF_6_PartB_DME 
	ON dbo.membersMonth.LbPatientId = dbo.CCLF_6_PartB_DME.LbPatientId AND
	dbo.membersMonth.visitdate = dbo.CCLF_6_PartB_DME.CLM_FROM_DT_1ST
	WHERE CLM_ADJSMT_TYPE_CD = 0
	GROUP BY 
	membersMonth.id,
	LbPatientId, 
	CLM_TYPE_CD,
	--PRNCPL_DGNS_CD, 
	--DATEPART("yyyy", visitdate), 
	--DATEPART("mm", visitdate), 
	--DATEADD(DAY, -(DAY(visitdate) - 1), visitdate) -- new field, for MemberMonth join
	visitdate



	UPDATE CCLF_CLM_SUMMARY SET
	CCLF_CLM_SUMMARY.Paid = CCLF_PARTB_TEMP_SUMMARY.Paid
	FROM CCLF_CLM_SUMMARY JOIN CCLF_PARTB_TEMP_SUMMARY 
	ON 
	 CCLF_CLM_SUMMARY.MemberMonths_id = CCLF_PARTB_TEMP_SUMMARY.MemberMonths_id AND
	 CCLF_CLM_SUMMARY.CLAIM_TYPE = CCLF_PARTB_TEMP_SUMMARY.CLM_TYPE_CD

	*/


	-----Log Information
		SELECT @InsertedRecord=count(1)
		FROM [CCLF_CLM_SUMMARY] ;
		
	 
          -- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
			  SET  @EndTime=GETUTCDATE();
			  EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
	
  END TRY

	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH

END
GO
