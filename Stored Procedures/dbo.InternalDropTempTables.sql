SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:        <khoa pham>
-- Create date: <10/4/2012>
-- Description:   <drop all temporary table on the sql connection sp call>
-- =============================================
create PROCEDURE [dbo].[InternalDropTempTables]
as
/*
select left(name, charindex('__',name)-1)[table]
, name
into #table --drop table #table
from tempdb..sysobjects
where charindex('__',name) > 0 
and xtype = 'u' and not object_id('tempdb..'+name) is null
order by [table]

---declare parameter
DECLARE @listCol VARCHAR(max)
DECLARE @query VARCHAR(max)
SELECT  @listCol = STUFF(( SELECT distinct  '], [' + [table]
                          FROM     #table 
                        FOR
                          XML PATH('')
                        ), 1, 2, '') + ']'
                        
                        --print @listCol
  set @query = 'drop table '+ @listCol+'     '
  --print      @query     
  exec (@Query) 
 
----for global tempory table  
--declare @sql nvarchar(max)
--select @sql = isnull(@sql+';', '') + 'drop table ' + quotename(name)
--from tempdb..sysobjects
--where name like '##%'
--exec (@sql) 
*/
GO
