SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLMemberExtract] 

/*===============================================================
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.2.1		2017-01-16		YL		    LBETL-444	add new step to call ETLUpdatePatientPCPNPI
        2.2.1           2017-01-17              YL                  LBETL-500   skip step 4 as It already has a job step run that procedure

=================================================================*/
AS

BEGIN
  
-- 1 pull new members from PatientIdsRegistryStage
EXECUTE [dbo].[ETLMemberExtractInsertFromStage];

-- 2 update multiple portal tables - vwPortal_PARTY, vwPortal_Individual, vwPortal_Patient, vwPortal_IndividualName
-- , PatientIdsRegistry, and  vwPortal_InterfacePatientMatched.  
EXECUTE [ETLMemberExtractMergePortalTables]; 

-- 3 email, comunication method, death date, language update;
-- plus update the lbPatientID
EXECUTE [dbo].[ETLMemberExtractDemographicsUpdate];

-- 4 Update PatientAttribute
--EXECUTE [dbo].[ETLMemberExtractPatientAttribute];


-- 5 Rebuild ETLPatientIdReference
EXECUTE [dbo].[ETLPatientIdReferenceBuild]


-- 6 update address
EXECUTE [dbo].[ETLUpdateAddress]

-- 7 update phone number
EXECUTE [dbo].[ETLUpdatePhoneNumber]

--8 update PCP NPI  (added 2017-01-16)
IF EXISTS(SELECT 1 FROM sysobjects WHERE NAME = 'ETLUpdatePatientPCPNPI' AND xtype = 'P')
BEGIN
      EXECUTE [dbo].[ETLUpdatePatientPCPNPI]
END
END
GO
