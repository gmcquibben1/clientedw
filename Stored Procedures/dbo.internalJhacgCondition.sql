SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[internalJhacgCondition] 
AS
--=============================================================
--              LbPatientId Maintenance
--             Initial Date: 10/30/2015
--             Initial Author: Mike Hoxter
-- Modified By: Lan
-- Modified Date: 2016-09-06
-- Description: switched to Jhacg11 table JhacgPatientDetail  
--=============================================================
--Truncate Table first
TRUNCATE TABLE JhacgCondition

--ADD DIABETES
INSERT INTO JhacgCondition (
   LbPatientId
  ,Condition
  ,ConditionAbbreviation
) 
SELECT O.LbPatientId, 'Diabetes' AS [Condition], 'Diab' AS [ConditionAbbreviation]  
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.diabetes_condition <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Congestive Heart Failure' as [Condition], 'CHF' AS [ConditionAbbreviation] 
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.[cong_heart_fail_condition] <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Chronic Obstructive Pulmonary Disease' AS [Condition], 'COPD' as [ConditionAbbreviation]
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.[chronic_obs_pul_dis_condition] <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Asthma' AS [Condition], 'ASM' AS [ConditionAbbreviation] 
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.persistent_asthma_condition <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Human Immunodeficiency Virus' AS [Condition], 'HIV' AS [ConditionAbbreviation] 
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.[human_imm_virus_condition] <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Hypertension' AS [Condition], 'HYT' AS [ConditionAbbreviation] 
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.hypertension_condition <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Chronic Renal Failure' AS [Condition], 'ESRD' AS [ConditionAbbreviation] 
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.[chronic_renal_fail_condition] <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Ischemic Heart Disease ' AS [Condition], 'IHD' AS [ConditionAbbreviation] 
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.[ischemic_hrt_dis_condition] <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Osteoporosis' AS [Condition], 'OST' AS [ConditionAbbreviation] 
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.osteoporosis_condition <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Rheumatoid Arthritis' AS [Condition], 'RA' AS [ConditionAbbreviation] 
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.[rheumatoid_arthr_condition] <> 'NP'
UNION ALL
SELECT O.LbPatientId, 'Low Back Pain' AS [Condition], 'LBP' AS [ConditionAbbreviation] 
FROM JhacgPatientDetail JD JOIN OrgHierarchy O ON JD.patient_id = O.LbPatientId WHERE JD.low_back_pain_condition <> 'NP'

insert into JhacgCondition (
   LbPatientId
  ,Condition
  ,ConditionAbbreviation
) SELECT O.LbPatientId, 'NA','NA' FROM OrgHierarchy O
LEFT JOIN JhacgCondition JC ON O.LbPatientId = JC.LbPatientId 
WHERE JC.JhacgConditionId IS NULL and O.LbPatientId is not null


GO
