SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientClaimPartDPharmacyLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT DISTINCT
		pa.[ClaimId], 
		pa.[PatientId],
		pa.[MedicationId], 
		pa.[FillDate], 
		pa.[QTY], 
		pa.[DaySupply], 
		pa.[PharmacyId], 
		pa.[PrescriberId], 
		pa.[PaidAmt], 
		pa.[AdjustmentTypeCode],
		pa.[RxNum], 
		pa.[PharmacyName], 
		pa.[PrescriberName], 
		pa.[MedicationName]
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientClaimPartDPharmacy pa ON pa.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (pa.FillDate > @dtmStartDate)
 
END
GO
