SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[internalProcedureValueCalc] @SourceSystemId int = 0, 
	@FullRebase int = 1, 
	@FeeScheduleValueMultiplier decimal (5,2) = 1.5,
	@NonFacility int = 1

	WITH EXEC AS CALLER
AS
BEGIN
  /*===============================================================
	CREATED BY: 	MH
	CREATED ON:		
	INITIAL VER:	2.1.1
	MODULE:			ETL
	DESCRIPTION:	This stored procedure is a custom activiate
	
			@FullRebase int = 1				--Conduct a full refresh
			@FeeScheduleValueMultiplier decimal (5,2) = 1.5,  --Fee Schedule
			@NonFacility int = 1			-- Include non facility codes



		
	Log Tracking:
        1. track table the last run date: [dbo].[EDWTableLoadTracking]
        2. log the procedure running status

	MODIFICATIONS
	
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	
	2.2.1						MH						 Initial

	2.2.1		2017-02-23		CJL			LBETL-929	 Corrected issues with null procedure code
																											
					

=================================================================*/

	 BEGIN TRY

	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'Task';
	DECLARE @dataSource VARCHAR(20) = 'EDW';
	DECLARE @ProcedureName VARCHAR(128) = 'internalProcedureValueCalc';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @NUMROWS INT




		IF (@FullRebase = 1)
		BEGIN 
		TRUNCATE TABLE PatientProcedureInternalValue
		END

		INSERT INTO PatientProcedureInternalValue (
		   HCPC
		  ,LbPatientId
		  ,ProcedureDate
		  ,ProcedureValue
		  ,RawValue
		  ,SourceSystemId
		  ,SourceSystemName
		) 
		SELECT PC.ProcedureCode, PP.PatientID, PP.ProcedureDateTime,
		  @FeeScheduleValueMultiplier*TRY_CONVERT( decimal(8,2), 
			CASE WHEN @NonFacility = 1 THEN FEE.[NON-FACILITY PRICE]
			  ELSE FEE.[FACILITY PRICE]
			END
		  ),--end try-convert
		  CASE WHEN @NonFacility = 1 THEN FEE.[NON-FACILITY PRICE]
			  ELSE FEE.[FACILITY PRICE]
			END, PP.SourceSystemID, SS.[Description]
		FROM PatientProcedure PP
		JOIN PatientProcedureProcedureCode PPPC ON PP.PatientProcedureId = PPPC.PatientProcedureId
		JOIN ProcedureCode PC ON PPPC.ProcedureCodeId = PC.ProcedureCodeId
		LEFT JOIN SourceSystem SS ON PP.SourceSystemID = SS.SourceSystemId
		LEFT JOIN Ref_TXFeeSchedule FEE ON PC.ProcedureCode = FEE.[HCPCS CODE]
		LEFT JOIN PatientProcedureInternalValue PPIV 
			ON PP.PatientID = PPIV.LbPatientId
			AND PPIV.HCPC = PC.ProcedureCode
			AND PP.ProcedureDateTime = PPIV.ProcedureDate
		where PPIV.PatientProcedureInternalValueId IS NULL 
			  AND PP.SourceSystemID = @SourceSystemId
			  and PP.ProcedureDateTime is not null



		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;

	

	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH
END
GO
