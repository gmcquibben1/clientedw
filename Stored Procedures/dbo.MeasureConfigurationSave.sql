SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE  [dbo].[MeasureConfigurationSave]
@ContractTypeId INT,
@MeasureID INT,  
@MeasureProgram VARCHAR(200),
@MeasureName VARCHAR(2000),
@MeasureAbbr VARCHAR(10),
@MeasureCategory VARCHAR(100),
@MeasureSubcategory VARCHAR(100),
@HighLow INT, 
@StoredProcedure VARCHAR(150),
@EnableFlag CHAR(1),
@MeasureContractID INT,
@TaskingNeeded CHAR(1),
@EnforceEligibilty INT, 
--

@ValueSetMeasureAbbr varchar(10),
@MeasureYear VARCHAR(4),
@UseClinicalDataOnlyInd bit ,
@UseLBSupplementCodesInd bit, 

@avg	decimal,
@nat30	decimal,
@nat90	decimal, 
@userId INT

AS

--This stored procedure is used to save the measure reference data from the EDW reference master database
--
--
--CJL 07.14.16
--LBPP--1669
--
DECLARE @TSQL  NVARCHAR(MAX)
DECLARE @ParmDefinition nvarchar(2000); 
DECLARE @Cnt INT
DECLARE @tmpMeasureID INT






SET @CNT = (SELECT COUNT(1) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CMeasure_Definitions' AND COLUMN_NAME = 'MeasureYear' )
SET @tmpMeasureID = ( SELECT MeasureID FROM  CMeasure_Definitions WITH (NOLOCK) 
WHERE MeasureProgram = @MeasureProgram AND MeasureName = @MeasureName
 AND MeasureAbbr = @MeasureAbbr AND MeasureCategory = @MeasureCategory AND MeasureSubcategory = @MeasureSubcategory)
SET @CNT = (SELECT COUNT(1) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CMeasure_Definitions' AND COLUMN_NAME = 'MeasureYear' )

print @tmpMeasureID

IF @tmpMeasureID IS NULL AND @CNT = 0 
BEGIN

	PRINT 'A'
	SET @TSQL = 'INSERT INTO CMeasure_Definitions 
		( MeasureProgram, MeasureName, MeasureAbbr, MeasureCategory, MeasureSubcategory, HighLow, StoredProcedure, EnableFlag, TaskingNeeded, EnforceEligibilty,MeasureContractID) 
		VALUES
		( @MeasureProgram, @MeasureName, @MeasureAbbr, @MeasureCategory, @MeasureSubcategory, @HighLow, @StoredProcedure, @EnableFlag, @TaskingNeeded, @EnforceEligibilty, @MeasureContractID)' 
	
	SET @ParmDefinition = N'@MeasureProgram	varchar(200), @MeasureName varchar(200), @MeasureAbbr varchar(10), @MeasureCategory varchar(100), @MeasureSubcategory varchar(100), @HighLow int, @StoredProcedure varchar(150), @EnableFlag char(1), @MeasureContractID int, @TaskingNeeded char(1), @EnforceEligibilty int'
	exec sp_executesql  @TSQL ,@ParmDefinition,   @MeasureProgram, @MeasureName, @MeasureAbbr,@MeasureCategory,@MeasureSubcategory, @HighLow, @StoredProcedure,@EnableFlag,@MeasureContractID,@TaskingNeeded, @EnforceEligibilty
	SET @tmpMeasureID = ( SELECT MeasureID FROM  CMeasure_Definitions WITH (NOLOCK) WHERE MeasureProgram = @MeasureProgram AND MeasureName = @MeasureName AND MeasureAbbr = @MeasureAbbr AND MeasureCategory = @MeasureCategory AND MeasureSubcategory = @MeasureSubcategory)
END




IF @tmpMeasureID IS NULL AND @CNT <> 0 
BEGIN

	PRINT 'B'
	
	SET @TSQL = 'INSERT INTO CMeasure_Definitions 
		( MeasureProgram, MeasureName, MeasureAbbr, MeasureCategory, MeasureSubcategory, HighLow, StoredProcedure, EnableFlag, TaskingNeeded, EnforceEligibilty, MeasureContractID,
		ValueSetMeasureAbbr, MeasureYear, UseClinicalDataOnlyInd, UseLBSupplementCodesInd)
		VALUES
		( @MeasureProgram, @MeasureName, @MeasureAbbr, @MeasureCategory, @MeasureSubcategory, @HighLow, @StoredProcedure, @EnableFlag, @TaskingNeeded, @EnforceEligibilty, @MeasureContractID, 
		@ValueSetMeasureAbbr, @MeasureYear, @UseClinicalDataOnlyInd, @UseLBSupplementCodesInd)'
	
	SET @ParmDefinition = N'@MeasureProgram	varchar(200), @MeasureName varchar(200), @MeasureAbbr varchar(10), @MeasureCategory varchar(100), @MeasureSubcategory varchar(100), @HighLow int, @StoredProcedure varchar(150), @EnableFlag char(1), MeasureContractID int, TaskingNeeded char(1), EnforceEligibilty int,	@ValueSetMeasureAbbr varchar(10), @MeasureYear varchar(4), @UseClinicalDataOnlyInd bit, @UseLBSupplementCodesInd bit'
	exec sp_executesql  @TSQL ,@ParmDefinition,   @MeasureProgram, @MeasureName, @MeasureAbbr,@MeasureCategory,@MeasureSubcategory, @HighLow, @StoredProcedure,@EnableFlag,@MeasureContractID,@TaskingNeeded, @EnforceEligibilty, @ValueSetMeasureAbbr, @MeasureYear, @UseClinicalDataOnlyInd, @UseLBSupplementCodesInd
	SET @tmpMeasureID = ( SELECT MeasureID FROM  CMeasure_Definitions WITH (NOLOCK) WHERE MeasureProgram = @MeasureProgram AND MeasureName = @MeasureName AND MeasureAbbr = @MeasureAbbr AND MeasureCategory = @MeasureCategory AND MeasureSubcategory = @MeasureSubcategory)
END


IF NOT @tmpMeasureID IS NULL AND @CNT = 0 
BEGIN

	PRINT 'C'
	SET @TSQL = 'UPDATE CMeasure_Definitions SET
		 HighLow = @HighLow, StoredProcedure = @StoredProcedure, EnableFlag = @EnableFlag, TaskingNeeded = @TaskingNeeded, EnforceEligibilty = @EnforceEligibilty, MeasureContractID = @MeasureContractID 
		WHERE MeasureId = @tmpMeasureID'
	
	SET @ParmDefinition = N'@tmpMeasureID INT,  @HighLow int, @StoredProcedure varchar(150), @EnableFlag char(1), @MeasureContractID int, @TaskingNeeded char(1), @EnforceEligibilty int'
	exec sp_executesql  @TSQL ,@ParmDefinition,  @tmpMeasureID,  @HighLow, @StoredProcedure,@EnableFlag,@MeasureContractID,@TaskingNeeded, @EnforceEligibilty
	

END


IF NOT @tmpMeasureID IS NULL AND @CNT <> 0 
BEGIN

	PRINT 'D'
	SET @TSQL = 'UPDATE CMeasure_Definitions SET
		 HighLow = @HighLow, StoredProcedure = @StoredProcedure, EnableFlag = @EnableFlag, TaskingNeeded = @TaskingNeeded, EnforceEligibilty = @EnforceEligibilty, MeasureContractID = @MeasureContractID, 
		 ValueSetMeasureAbbr = @ValueSetMeasureAbbr, MeasureYear = @MeasureYear, UseClinicalDataOnlyInd = @UseClinicalDataOnlyInd, UseLBSupplementCodesInd = @UseLBSupplementCodesInd
		WHERE MeasureId = @tmpMeasureID'
	
	SET @ParmDefinition = N'@tmpMeasureID INT, @HighLow int, @StoredProcedure varchar(150), @EnableFlag char(1), @MeasureContractID int, @TaskingNeeded char(1), @EnforceEligibilty int,	@ValueSetMeasureAbbr varchar(10), @MeasureYear varchar(4), @UseClinicalDataOnlyInd bit, @UseLBSupplementCodesInd bit'
	exec sp_executesql  @TSQL ,@ParmDefinition,  @tmpMeasureID,  @HighLow, @StoredProcedure,@EnableFlag,@MeasureContractID,@TaskingNeeded, @EnforceEligibilty, @ValueSetMeasureAbbr, @MeasureYear, @UseClinicalDataOnlyInd, @UseLBSupplementCodesInd

END


SET @CNT = (SELECT COUNT(1) FROM ACO_Measure_NatAvgTargets WHERE MeasureId = @tmpMeasureID)
IF @CNT = 0 
BEGIN 
	INSERT INTO ACO_Measure_NatAvgTargets
	( MeasureID, avg, nat30,nat90, Name) 
	VALUES
	( @tmpMeasureID, @avg, @nat30, @nat90, @MeasureAbbr) 

END
ELSE
BEGIN 
	UPDATE ACO_Measure_NatAvgTargets 
	SET avg = @avg, nat30 =@nat30, nat90 = @nat90, name = @MeasureAbbr
	WHERE MeasureId = @tmpMeasureID
	
END









DECLARE @tmpId INT
SET @tmpMeasureID = ( SELECT MeasureID FROM  CMeasure_Definitions WITH (NOLOCK) 
WHERE MeasureProgram = @MeasureProgram AND MeasureName = @MeasureName
 AND MeasureAbbr = @MeasureAbbr AND MeasureCategory = @MeasureCategory AND MeasureSubcategory = @MeasureSubcategory)



SET @CNT = (SELECT COUNT(1) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CMeasure_DefinitionsContractType'  )
IF @CNT <> 0 
BEGIN 
	DECLARE @DeleteInd BIT 
	IF @EnableFlag = 'Y' 
		BEGIN
			SET @DeleteInd =0
		END
	ELSE
		BEGIN
			SET @DeleteInd =1
		END


	PRINT 'F'
	PRINT @userId
	SET @TSQL = 'exec CMeasure_DefinitionsContractTypeSave @ContractTypeId, @tmpMeasureID, @HighLow, @EnableFlag, @TaskingNeeded, @EnforceEligibilty, @UseClinicalDataOnlyInd, @UseLBSupplementCodesInd, @AVG, @NAT30, @NAT90, @userId, @DeleteInd'


	SET @ParmDefinition = N'@ContractTypeId int, @tmpMeasureID INT, @HighLow int, @EnableFlag char(1),  @TaskingNeeded char(1), @EnforceEligibilty int,	 @UseClinicalDataOnlyInd bit, @UseLBSupplementCodesInd bit,  @avg	decimal, @nat30	decimal, @nat90	decimal, @userId INT, @DeleteInd BIT'

	exec sp_executesql  @TSQL, @ParmDefinition, @tmpMeasureID, @ContractTypeId, @HighLow, @EnableFlag, @TaskingNeeded, @EnforceEligibilty, @UseClinicalDataOnlyInd, @UseLBSupplementCodesInd, 	@AVG, @NAT30, @NAT90,  @userId,  @DeleteInd


END



GO
