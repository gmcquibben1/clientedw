SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[CMeasure_DefinitionsContractTypeSave]
@ContractTypeId INT,
@MeasureID INT,  
@HighLow INT, 
@EnableFlag CHAR(1),
@TaskingNeeded CHAR(1),
@EnforceEligibilty INT, 

@UseClinicalDataOnlyInd bit ,
@UseLBSupplementCodesInd bit, 

@avg	decimal,
@nat30	decimal,
@nat90	decimal, 
@userId INT,
@DeleteInd BIT

AS

DECLARE @MeasureDefinitionContractTypeId INT 

SET @MeasureDefinitionContractTypeId = (SELECT MeasureDefinitionContractTypeId FROM CMeasure_DefinitionsContractType WITH (NOLOCK) WHERE ContractTypeId =@ContractTypeId AND MeasureID = @MeasureId)
IF @MeasureDefinitionContractTypeId IS NULL 
BEGIN 

	INSERT INTO CMeasure_DefinitionsContractType 
			( MeasureID, ContractTypeId, HighLow, EnableFlag, TaskingNeeded, 
	EnforceEligibilty, UseClinicalDataOnlyInd, UseLBSupplementCodesInd, TARGET,
	AVG, NAT30, NAT90, 
	DeleteInd, CreateDateTime, ModifyDateTime, CreateLBUserId, ModifyLBUserId) 
			VALUES
			(@MeasureID, @ContractTypeId, @HighLow, @EnableFlag, @TaskingNeeded, 
	@EnforceEligibilty, @UseClinicalDataOnlyInd, @UseLBSupplementCodesInd, @AVG,
	@AVG, @NAT30,@NAT90, 
	@DeleteInd, GetUTCDate(), GetUTCDate(), @userId, @userId)

END
ELSE
BEGIN
	UPDATE CMeasure_DefinitionsContractType 
	SET 
		HighLow  = @HighLow, 
		EnableFlag = @EnableFlag, 
		TaskingNeeded = @TaskingNeeded, 
		EnforceEligibilty = @EnforceEligibilty, 
		UseClinicalDataOnlyInd = @UseClinicalDataOnlyInd, 
		UseLBSupplementCodesInd = @UseLBSupplementCodesInd, 
		TARGET = @AVG,
		AVG = @AVG, 
		NAT30 = @NAT30, 
		NAT90 = @NAT90, 
		DeleteInd = @DeleteInd, 
		 ModifyDateTime = GetUTCDate(), 
		  ModifyLBUserId = @userId
	WHERE  MeasureDefinitionContractTypeId = @MeasureDefinitionContractTypeId
		

END


GO
