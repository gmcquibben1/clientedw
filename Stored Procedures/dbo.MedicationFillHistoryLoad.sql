SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[MedicationFillHistoryLoad] @PatientID INT
AS
/*

	Version		Date		Author	Change
	-------		----------	------	------	
	2.1.2		2016-12-09	YL	     New  LBETL-242 Medication Fill History from CCLF7
	
*/

BEGIN

SELECT DISTINCT [CLM_LINE_FROM_DT]
      ,[CLM_LINE_DAYS_SUPLY_QTY]
      ,[CLM_LINE_BENE_PMT_AMT]  
      , CASE WHEN ISNULL(m.GenericName,'')='' THEN [MedicationBrandName] ELSE m.GenericName END AS GenericName
	  ,[CLM_DAW_PROD_SLCTN_CD] as DAW
	  ,[CLM_LINE_SRVC_UNIT_QTY]
	  ,n.[NAME] as ProviderName
	  ,n2.NAME as PharmacyName
  FROM [dbo].[CCLF_7_PartD] a
  INNER JOIN [dbo].[Medication] m on a.CLM_LINE_NDC_CD=m.NDCCode  AND ISNULL(m.RxNormCode,'')=''
  LEFT OUTER JOIN [EdwReferenceData].[dbo].[FdbMedication] fdb on fdb.[NDC]=a.CLM_LINE_NDC_CD 
  LEFT OUTER JOIN [dbo].[NPI_Lookup] n on n.NPI=a.CLM_PRSBNG_PRVDR_GNRC_ID_NUM
  LEFT OUTER JOIN [dbo].[NPI_Lookup] n2 on n2.NPI=a.[CLM_SRVC_PRVDR_GNRC_ID_NUM]
WHERE [LbPatientId]=@PatientID
ORDER BY CASE WHEN ISNULL(m.GenericName,'')='' THEN [MedicationBrandName] ELSE m.GenericName END , [CLM_LINE_FROM_DT] DESC


END
GO
