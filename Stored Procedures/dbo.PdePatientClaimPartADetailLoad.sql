SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PdePatientClaimPartADetailLoad]
	@PatientSetId INTEGER,
	@StartDate DATETIME = NULL 
AS
BEGIN
	DECLARE @dtmStartDate  AS DATETIME

	IF @StartDate IS NULL
		SET @dtmStartDate = '1900-01-01 12:00:00.000'
	ELSE
		SET @dtmStartDate = @StartDate

	SET NOCOUNT ON

	SELECT DISTINCT
		pa.PatientId,
		pa.ClaimId,
		pa.ClaimLineNum,
		pa.FromDate,
		pa.ThruDate,
		pa.RevenueCode,
		pa.ProcedureCode,
		pa.ProcedureDesc,
		pa.BETOSCat1,
		pa.BETOSCat2,
		pa.BETOSCat3,
		pa.Modifier1,
		pa.Modifier2,
		pa.Modifier3,
		pa.Modifier4,
		pa.Modifier5,
		pa.LinePaidAmt,
		pa.Quantity
	FROM PatientDataExportList pdel
		 INNER JOIN vwPatientClaimPartADetail pa ON pa.PatientId = pdel.LbPatientId 
	WHERE pdel.BackgroundProcessRequestQueueId = @PatientSetId 
 		AND (pa.FromDate > @dtmStartDate)
 
END
GO
