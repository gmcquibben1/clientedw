SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FaceSheetImmunizationLoad] 
	@PatientId INTEGER
AS
BEGIN
	--This temp table will hold all of the rows to be displayed.
	DECLARE  @FacesheetImmunization TABLE 
	(
		VaccineName			VARCHAR(100),
		VaccinationDate		VARCHAR(20)
	)	

	--fill in the temp table one immunization type at a time
	IF EXISTS (SELECT 1 FROM dbo.vwPatientImmunization 
		WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90714','90718','90703') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Td'))
		AND dbo.vwPatientImmunization.ServiceDate < GETDATE() AND dbo.vwPatientImmunization.PatientID = @PatientId )
		OR EXISTS (SELECT 1 FROM dbo.vwPatientProcedure 
		WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90714','90718','90703') OR dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Td'))
		AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE() AND dbo.vwPatientProcedure.PatientID = @PatientId )
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		SELECT TOP 1 v.VaccineName, RIGHT('0' + RTRIM(month(v.VaccinationDate)),2) + '/' + RIGHT('0' + RTRIM(day(v.VaccinationDate)),2) + '/' + RTRIM(year(v.VaccinationDate)) AS VaccinationDate FROM
			(SELECT 
				'Tetanus' AS VaccineName,
				dbo.vwPatientImmunization.ServiceDate AS VaccinationDate
			FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90714','90718','90703') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Td'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE()
			AND dbo.vwPatientImmunization.PatientId = @PatientId
			UNION
			SELECT 
				'Tetanus' AS VaccineName,
				dbo.vwPatientProcedure.ProcedureDateTime AS VaccinationDate
			FROM dbo.vwPatientProcedure 
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90714','90718','90703') OR dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Td'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE()
			AND dbo.vwPatientProcedure.PatientID = @PatientId) AS v
		ORDER BY v.VaccinationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		VALUES ('Tetanus', '');
	END
	--------------------------------------------------------------------------------------
	IF EXISTS (SELECT 1 FROM dbo.vwPatientImmunization 
			WHERE dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Tdap')
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE() AND dbo.vwPatientImmunization.PatientID = @PatientId )
		OR EXISTS (SELECT 1 FROM dbo.vwPatientProcedure 
			WHERE dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Tdap')
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE() AND dbo.vwPatientProcedure.PatientID = @PatientId )
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		SELECT TOP 1 v.VaccineName, RIGHT('0' + RTRIM(month(v.VaccinationDate)),2) + '/' + RIGHT('0' + RTRIM(day(v.VaccinationDate)),2) + '/' + RTRIM(year(v.VaccinationDate)) AS VaccinationDate FROM
			(SELECT 
				'Tdap' AS VaccineName,
				dbo.vwPatientImmunization.ServiceDate AS VaccinationDate
			FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Tdap'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE()
			AND dbo.vwPatientImmunization.PatientId = @PatientId
			UNION
			SELECT 
				'Tdap' AS VaccineName,
				dbo.vwPatientProcedure.ProcedureDateTime AS VaccinationDate
			FROM dbo.vwPatientProcedure 
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Tdap'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE()
			AND dbo.vwPatientProcedure.PatientID = @PatientId) AS v
		ORDER BY v.VaccinationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		VALUES ('Tdap', '');
	END
	--------------------------------------------------------------------------------------
	IF EXISTS (SELECT 1 FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90732', 'G0009') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Pneumococcal Conjugate'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE() AND dbo.vwPatientImmunization.PatientID = @PatientId )
		OR EXISTS (SELECT 1 FROM dbo.vwPatientProcedure
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90732', 'G0009') OR dbo.vwPatientProcedure.ProcedureCode  IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Pneumococcal Conjugate'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE() AND dbo.vwPatientProcedure.PatientID = @PatientId )
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		SELECT TOP 1 v.VaccineName, RIGHT('0' + RTRIM(month(v.VaccinationDate)),2) + '/' + RIGHT('0' + RTRIM(day(v.VaccinationDate)),2) + '/' + RTRIM(year(v.VaccinationDate)) AS VaccinationDate FROM
			(SELECT 
				'Pneumovax' AS VaccineName,
				dbo.vwPatientImmunization.ServiceDate AS VaccinationDate
			FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90732', 'G0009') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Pneumococcal Conjugate'))
			AND dbo.vwPatientImmunization.ImmunizationCode <> '90670'
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE()
			AND dbo.vwPatientImmunization.PatientId = @PatientId
			UNION
			SELECT 
				'Pneumovax' AS VaccineName,
				dbo.vwPatientProcedure.ProcedureDateTime AS VaccinationDate
			FROM dbo.vwPatientProcedure 
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90732', 'G0009') OR dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Pneumococcal Conjugate'))
			AND dbo.vwPatientProcedure.ProcedureCode <> '90670'
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE()
			AND dbo.vwPatientProcedure.PatientID = @PatientId) AS v
		ORDER BY v.VaccinationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		VALUES ('Pneumovax', '');
	END
	--------------------------------------------------------------------------------------
	IF EXISTS (SELECT 1 FROM dbo.vwPatientImmunization 
			WHERE dbo.vwPatientImmunization.ImmunizationCode IN ('90670')
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE() AND dbo.vwPatientImmunization.PatientID = @PatientId )
		OR EXISTS (SELECT 1 FROM dbo.vwPatientProcedure
			WHERE dbo.vwPatientProcedure.ProcedureCode IN ('90670')
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE() AND dbo.vwPatientProcedure.PatientID = @PatientId )
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		SELECT TOP 1 v.VaccineName, RIGHT('0' + RTRIM(month(v.VaccinationDate)),2) + '/' + RIGHT('0' + RTRIM(day(v.VaccinationDate)),2) + '/' + RTRIM(year(v.VaccinationDate)) AS VaccinationDate FROM
			(SELECT 
				'Prevnar' AS VaccineName,
				dbo.vwPatientImmunization.ServiceDate AS VaccinationDate
			FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90670'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE()
			AND dbo.vwPatientImmunization.PatientId = @PatientId
			UNION
			SELECT 
				'Prevnar' AS VaccineName,
				dbo.vwPatientProcedure.ProcedureDateTime AS VaccinationDate
			FROM dbo.vwPatientProcedure 
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90670'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE()
			AND dbo.vwPatientProcedure.PatientID = @PatientId) AS v
		ORDER BY v.VaccinationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		VALUES ('Prevnar 13', '');
	END
	--------------------------------------------------------------------------------------
	IF EXISTS (SELECT 1 FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90724', '90663', 'G0008') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Influenza'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE() AND dbo.vwPatientImmunization.PatientID = @PatientId )
		OR EXISTS (SELECT 1 FROM dbo.vwPatientProcedure
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90724', '90663', 'G0008') OR dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Influenza'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE() AND dbo.vwPatientProcedure.PatientID = @PatientId )
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		SELECT TOP 1 v.VaccineName, RIGHT('0' + RTRIM(month(v.VaccinationDate)),2) + '/' + RIGHT('0' + RTRIM(day(v.VaccinationDate)),2) + '/' + RTRIM(year(v.VaccinationDate)) AS VaccinationDate FROM
			(SELECT 
				'Flu' AS VaccineName,
				dbo.vwPatientImmunization.ServiceDate AS VaccinationDate
			FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90724', '90663', 'G0008') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Influenza'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE()
			AND dbo.vwPatientImmunization.PatientId = @PatientId
			UNION
			SELECT 
				'Flu' AS VaccineName,
				dbo.vwPatientProcedure.ProcedureDateTime AS VaccinationDate
			FROM dbo.vwPatientProcedure 
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90724', '90663', 'G0008') OR dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Influenza'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE()
			AND dbo.vwPatientProcedure.PatientID = @PatientId) AS v
		ORDER BY v.VaccinationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		VALUES ('Flu', '');
	END
	--------------------------------------------------------------------------------------
	IF EXISTS (SELECT 1 FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90736') )
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE() AND dbo.vwPatientImmunization.PatientID = @PatientId )
		OR EXISTS (SELECT 1 FROM dbo.vwPatientProcedure
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90736') )
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE() AND dbo.vwPatientProcedure.PatientID = @PatientId )
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		SELECT TOP 1 v.VaccineName, RIGHT('0' + RTRIM(month(v.VaccinationDate)),2) + '/' + RIGHT('0' + RTRIM(day(v.VaccinationDate)),2) + '/' + RTRIM(year(v.VaccinationDate)) AS VaccinationDate FROM
			(SELECT 
				'Zostavax' AS VaccineName,
				dbo.vwPatientImmunization.ServiceDate AS VaccinationDate
			FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90736') )
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE()
			AND dbo.vwPatientImmunization.PatientId = @PatientId
			UNION
			SELECT 
				'Zostavax' AS VaccineName,
				dbo.vwPatientProcedure.ProcedureDateTime AS VaccinationDate
			FROM dbo.vwPatientProcedure 
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90736') )
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE()
			AND dbo.vwPatientProcedure.PatientID = @PatientId) AS v
		ORDER BY v.VaccinationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		VALUES ('Zostavax', '');
	END
	--------------------------------------------------------------------------------------
	IF EXISTS (SELECT 1 FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90650', '90649') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'HPV'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE() AND dbo.vwPatientImmunization.PatientID = @PatientId )
		OR EXISTS (SELECT 1 FROM dbo.vwPatientProcedure
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90650', '90649') OR dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'HPV'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE() AND dbo.vwPatientProcedure.PatientID = @PatientId )
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		SELECT TOP 1 v.VaccineName, RIGHT('0' + RTRIM(month(v.VaccinationDate)),2) + '/' + RIGHT('0' + RTRIM(day(v.VaccinationDate)),2) + '/' + RTRIM(year(v.VaccinationDate)) AS VaccinationDate FROM
			(SELECT 
				'HPV' AS VaccineName,
				dbo.vwPatientImmunization.ServiceDate AS VaccinationDate
			FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90650', '90649') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'HPV'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE()
			AND dbo.vwPatientImmunization.PatientId = @PatientId
			UNION
			SELECT 
				'HPV' AS VaccineName,
				dbo.vwPatientProcedure.ProcedureDateTime AS VaccinationDate
			FROM dbo.vwPatientProcedure 
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90650', '90649') OR dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'HPV'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE()
			AND dbo.vwPatientProcedure.PatientID = @PatientId) AS v
		ORDER BY v.VaccinationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		VALUES ('HPV', '');
	END
	--------------------------------------------------------------------------------------
	IF EXISTS (SELECT 1 FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90731', 'G0010') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Hepatitis B'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE() AND dbo.vwPatientImmunization.PatientID = @PatientId )
		OR EXISTS (SELECT 1 FROM dbo.vwPatientProcedure
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90731', 'G0010') OR dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Hepatitis B'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE() AND dbo.vwPatientProcedure.PatientID = @PatientId )
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		SELECT TOP 1 v.VaccineName, RIGHT('0' + RTRIM(month(v.VaccinationDate)),2) + '/' + RIGHT('0' + RTRIM(day(v.VaccinationDate)),2) + '/' + RTRIM(year(v.VaccinationDate)) AS VaccinationDate FROM
			(SELECT 
				'HepB' AS VaccineName,
				dbo.vwPatientImmunization.ServiceDate AS VaccinationDate
			FROM dbo.vwPatientImmunization 
			WHERE (dbo.vwPatientImmunization.ImmunizationCode IN ('90731', 'G0010') OR dbo.vwPatientImmunization.ImmunizationCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Hepatitis B'))
			AND dbo.vwPatientImmunization.ServiceDate < GETDATE()
			AND dbo.vwPatientImmunization.PatientId = @PatientId
			UNION
			SELECT 
				'HepB' AS VaccineName,
				dbo.vwPatientProcedure.ProcedureDateTime AS VaccinationDate
			FROM dbo.vwPatientProcedure 
			WHERE (dbo.vwPatientProcedure.ProcedureCode IN ('90731', 'G0010') OR dbo.vwPatientProcedure.ProcedureCode IN (SELECT Code FROM HEDISValueSetCodes WHERE [Value Set Name] = 'Hepatitis B'))
			AND dbo.vwPatientProcedure.ProcedureDateTime < GETDATE()
			AND dbo.vwPatientProcedure.PatientID = @PatientId) AS v
		ORDER BY v.VaccinationDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @FacesheetImmunization (VaccineName, VaccinationDate)
		VALUES ('HepB', '');
	END

	--now spit out whatever is in the temp table
	SELECT VaccineName, VaccinationDate FROM @FacesheetImmunization;
END
GO
