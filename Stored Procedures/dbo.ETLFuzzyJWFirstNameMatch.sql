SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ETLFuzzyJWFirstNameMatch] @MatchScoreMin FLOAT=0.85
AS
BEGIN


/*=================================================================================================
	CREATED BY: 	Mike H
	CREATED ON:		2017-01-18
	INITIAL VER:	2.2.2
	MODULE:			Fuzzy Match Type for External First Name 
	DESCRIPTION: 


	MODIFICATIONS
	Version     Date            Author		  JIRA  		Change
	=======     ==========      ======      =========	=========	
  2.2.2       9/07/2016         SG        LBAN-3490   Initial Version
  2.2.2       03.07.2017		CJL		  LBAN-3490   Added logging, removed hardcoded value type of 1, handled issues in which duplicate records would be inserted. Performance tuned the data extraction queries


====================================================================================================*/



	DECLARE @EDWName VARCHAR(128) = DB_NAME();	
	DECLARE @EDWtableName VARCHAR(128) = 'ETLFuzzyJWFirstNameMatch';
	DECLARE @dataSource VARCHAR(20) = 'EDW';
	DECLARE @ProcedureName VARCHAR(128) = 'ETLFuzzyJWFirstNameMatch';
	DECLARE @maxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @tmpmaxSourceTimeStampProcessed DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @maxSourceIdProcessed INT = NULL;
	DECLARE @StartTime DATETIME = GETUTCDATE();
	DECLARE @EndTime DATETIME = NULL;
	DECLARE @InsertedRecord INT=0;
	DECLARE @UpdatedRecord INT=0;
	DECLARE @Comment VARCHAR(400) = '';
	DECLARE @ErrorNumber INT =0;
	DECLARE @ErrorState INT =0;
	DECLARE @ErrorSeverity INT=0;
	DECLARE @ErrorLine INT=0;
	DECLARE @RecordCountBefore INT=0;
	DECLARE @RecordCountAfter  INT=0;
	DECLARE @RecordCountSource INT=0;
	DECLARE @lastIndex INT = 0;
	DECLARE @maxIndex INT = 0;
	DECLARE @lastDateTime DATETIME = CAST('1900-01-01' AS DATETIME);
	DECLARE @Today DATE=GETUTCDATE();
	DECLARE @NUMROWS INT


		DECLARE @FuzzyMatchTypeId int = (SELECT TOP 1 FuzzyMatchTypeId FROM FuzzyMatchType WHERE SPName like '%ETLFuzzyJWFirstNameMatch%')  
		
		-- get total records from PatientMedication after merge 
		SELECT @RecordCountBefore= (SELECT COUNT(1)  FROM FuzzyMatchResult WITH (NOLOCK));

	

	
	BEGIN TRY






				SELECT 
					inn.LastName,
					i.BirthDate,
					gt.DisplayValue as Gender, 
					COUNT(DISTINCT PatientId) AS CNT
					INTO #T
				FROM 
					vwportal_Patient  p WITH (NOLOCK) 
					INNER JOIN vwportal_Individual i  WITH (NOLOCK) ON i.IndividualId = p.IndividualId 
					INNER JOIN vwportal_IndividualName inn  WITH (NOLOCK) ON inn.IndividualId = i.IndividualId
					LEFT OUTER JOIN  vwPortal_GenderType gt  WITH (NOLOCK) ON gt.GenderTypeId = i.GenderTypeId
				WHERE p.DeleteInd = 0 
				GROUP BY  LastName,  BirthDate , gt.DisplayValue 
				HAVING COUNT(DISTINCT PatientId) > 1
			
				
			--Get a list of the patients that are potential candidates for matching. 
			SELECT   
			  PT.PatientId, 
			  inn.FirstName,
			  inn.LastName,
			  i.BirthDate, 
			  gt.DisplayValue as  Gender,
			  i.ExternalReferenceIdentifier,
			  i.OtherReferenceIdentifier,
			  I.SourceSystemId
			INTO #TMP2 
			 FROM 
				#T T, 
				vwportal_Patient  PT WITH (NOLOCK),  vwportal_IndividualName inn  WITH (NOLOCK) ,vwportal_Individual i  WITH (NOLOCK) 
				LEFT OUTER JOIN  vwPortal_GenderType gt  WITH (NOLOCK) ON gt.GenderTypeId = i.GenderTypeId
				WHERE i.IndividualId = PT.IndividualId  AND inn.IndividualId = i.IndividualId
				AND T.LastName = inn.LastName AND T.Gender = gt.DisplayValue AND T.BirthDate = i.BirthDate AND PT.DeleteInd = 0 


				
	  Create  NonCLUSTERED INDEX IDX_TMP2 ON #TMP2 (LastName, Gender, BirthDate, PatientId);

		
	
		--Out of the all of the candidate values, calculate the Levenstein distance 

		SELECT * INTO #FuzzyPassLNameSexDOB FROM (
		SELECT ME.FirstName inFName , ME.LastName inLastName, ME.BirthDate inDOB, ME.PatientId inPid,
			ISNULL(ME.ExternalReferenceIdentifier, ME.OtherReferenceIdentifier) AS inExternalId , 
			ME.SourceSystemId AS inSourceSystemId,
		  [dbo].[usfFuzzyCalculateJWMaster]( ME.FirstName,  inn.FirstName )  similarScore , 
		  PT.PatientId, 
		  inn.FirstName,
		  inn.LastName,
		  i.BirthDate, 
		  gt.DisplayValue as Gender,	
		  i.ExternalReferenceIdentifier,
		  i.SourceSystemID
		  FROM #TMP2 ME ,
		  	vwportal_Patient  PT WITH (NOLOCK),  vwportal_IndividualName inn  WITH (NOLOCK) ,vwportal_Individual i  WITH (NOLOCK) 
				LEFT OUTER JOIN  vwPortal_GenderType gt  WITH (NOLOCK) ON gt.GenderTypeId = i.GenderTypeId
				WHERE i.IndividualId = PT.IndividualId  AND inn.IndividualId = i.IndividualId
				AND ME.LastName = inn.LastName AND ME.Gender = gt.DisplayValue AND ME.BirthDate = i.BirthDate AND PT.DeleteInd = 0 
				AND ME.PatientId < PT.PatientId
		) A 
		WHERE A.similarScore > @MatchScoreMin 
		--ORDER BY A.LastName + CAST(A.BirthDate AS VARCHAR(10)) DESC , A.FirstName DESC , inPid DESC

	



		DROP TABLE #T
		DROP TABLE #TMP2

		Create  NonCLUSTERED INDEX IDX_FuzzyPassLNameSexDOB ON #FuzzyPassLNameSexDOB (inPid, PatientId);


		SELECT  @FuzzyMatchTypeId As FuzzyMatchTypeId,
					 [inFirstName]
				   ,[inLastName]
				   ,[inPid]
				   ,[inExternalId]
				   ,[inSourceSystemId]
				   ,[matchedPid]
				   ,[matchedFirstName]
				   ,[MatchedLastName]
				   ,[matchedDOB]
				   ,[Gender]
				   ,[ExternalId]
				   ,[matchedSourceSystemId]
				   ,[similarityScore]
		INTO #FuzzyMatchResultsLastNameSexDob
		FROM (
		SELECT inFName AS inFirstName, inLastName , inPid, inExternalId, inSourceSystemId,
		PatientId AS matchedPid, 
		FirstName AS matchedFirstName, LastName AS MatchedLastName, 
		BirthDate AS matchedDOB, Gender , 
		T.ExternalReferenceIdentifier AS ExternalId, 
		T.SourceSystemId AS matchedSourceSystemId,
		T.similarScore AS similarityScore,
		ROW_NUMBER() OVER (PARTITION BY inPid, PatientId ORDER BY inPid DESC) AS r 
		FROM #FuzzyPassLNameSexDOB T
		) A WHERE A.r = 1

		DROP TABLE #FuzzyPassLNameSexDOB 


		Create  NonCLUSTERED INDEX IDX_FuzzyMatchResultsLastNameSexDob ON #FuzzyMatchResultsLastNameSexDob (matchedPid, inPid, FuzzyMatchTypeId);


	

		INSERT INTO [dbo].[FuzzyMatchResult]
				   ([FuzzyMatchTypeId]
				   ,[inFirstName]
				   ,[inLastName]
				   ,[inPid]
				   ,[inExternalId]
				   ,[inSourceSystemId]
				   ,[matchedPid]
				   ,[matchedFirstName]
				   ,[MatchedLastName]
				   ,[matchedDOB]
				   ,[matchedSourceSystemId]
				   ,[Gender]
				   ,[ExternalId]
				   ,[isMerged]
				   ,[similarityScore])
				   (
			SELECT @FuzzyMatchTypeId, 
				r.[inFirstName]
			  ,r.[inLastName]
			  ,r.[inPid]
			  ,r.[inExternalId]
			  ,r.[inSourceSystemId]
			  ,r.[matchedPid]
			  ,r.[matchedFirstName]
			  ,r.[MatchedLastName]
			  ,r.[matchedDOB]
			  ,r.[matchedSourceSystemId]
			  ,r.[Gender]
			  ,r.[ExternalId]
			  ,0
			  ,r.[similarityScore]
			FROM #FuzzyMatchResultsLastNameSexDob r
			LEFT JOIN FuzzyMatchResult mr WITH (NOLOCK) ON mr.matchedPid =  r.matchedPid AND mr.inPid =  r.inPid AND r.FuzzyMatchTypeId = mr.FuzzyMatchTypeId
			WHERE mr.matchedPid IS NULL  --Do the left join to exclude duplicates from the lists
			)


			DROP TABLE #FuzzyMatchResultsLastNameSexDob




		-- update maxSourceTimeStamp with max CreateDateTime of #PatientMedicationQue
		UPDATE [Maintenance]..[EDWTableLoadTracking]
		SET [maxSourceTimeStampProcessed] = @maxSourceTimeStampProcessed, maxSourceIdProcessed = @maxIndex, updatedatetime = GETUTCDATE()
		WHERE [EDWtableName] = @EDWtableName AND [dataSource] =@dataSource  AND [EDWName] = @EDWName


		-- get total records from PatientMedication after merge 
		SELECT @RecordCountAfter= (SELECT COUNT(1)  FROM FuzzyMatchResult WITH (NOLOCK));

		-- Calculate records inserted and updated counts
		SET @InsertedRecord= @RecordCountAfter -@RecordCountBefore
		SET @UpdatedRecord = 0

		-- insert log information to [Maintenance].[dbo].[EdwProcedureRunLog]
		SET  @EndTime=GETUTCDATE();
		EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;



	END TRY
	BEGIN CATCH
	           --- insert error log information to [Maintenance].[dbo].[EdwProcedureRunLog]
				SET  @EndTime=GETUTCDATE();
				SET  @ErrorNumber =ERROR_NUMBER();
				SET  @ErrorState =ERROR_STATE();
				SET  @ErrorSeverity=ERROR_SEVERITY();
				SET  @ErrorLine=ERROR_LINE();
				SET  @Comment =left(ERROR_MESSAGE(),400);
				EXEC [Maintenance].[dbo].[EdwInsertProcedureRunLog] @EDWName,@ProcedureName,@StartTime,@EndTime,@InsertedRecord,@UpdatedRecord,@ErrorNumber,@ErrorState,@ErrorSeverity,@ErrorLine,@Comment,@EDWtableName;
				
	END CATCH


END
GO
