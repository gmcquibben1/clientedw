CREATE TYPE [dbo].[DepositingIdProcessType] AS TABLE
(
[DepositingId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepositingIdType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepositingIdSourceType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepositingIdSourceFeed] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepositingIdMetaType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
