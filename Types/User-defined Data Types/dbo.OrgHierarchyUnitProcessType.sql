CREATE TYPE [dbo].[OrgHierarchyUnitProcessType] AS TABLE
(
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1Name] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1TypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2Name] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2TypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3Name] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3TypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level4Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level4Name] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level4TypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level5Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level5Name] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level5TypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level6Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level6Name] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level6TypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
