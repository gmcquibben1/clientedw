CREATE TYPE [dbo].[HealthcareOrgUnitProcessType] AS TABLE
(
[EntityName] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EntityId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueryValue] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueryValueType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
