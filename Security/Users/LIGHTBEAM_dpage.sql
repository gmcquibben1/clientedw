IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LIGHTBEAM\dpage')
CREATE LOGIN [LIGHTBEAM\dpage] FROM WINDOWS
GO
CREATE USER [LIGHTBEAM\dpage] FOR LOGIN [LIGHTBEAM\dpage]
GO
