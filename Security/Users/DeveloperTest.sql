IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'DeveloperTest')
CREATE LOGIN [DeveloperTest] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [DeveloperTest] FOR LOGIN [DeveloperTest]
GO
GRANT EXECUTE TO [DeveloperTest]
