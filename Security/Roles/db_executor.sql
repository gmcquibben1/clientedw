CREATE ROLE [db_executor]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'db_executor', N'LIGHTBEAM\SQL Developers'
GO
GRANT ALTER TO [db_executor]
GRANT CREATE FUNCTION TO [db_executor]
GRANT CREATE PROCEDURE TO [db_executor]
GRANT CREATE TABLE TO [db_executor]
GRANT CREATE VIEW TO [db_executor]
GRANT EXECUTE TO [db_executor]
GRANT SHOWPLAN TO [db_executor]
