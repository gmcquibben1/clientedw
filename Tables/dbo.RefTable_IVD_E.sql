CREATE TABLE [dbo].[RefTable_IVD_E]
(
[ndc_code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[brand_name] [nvarchar] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[generic_product_name] [nvarchar] (94) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[route] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[category] [nvarchar] (61) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
