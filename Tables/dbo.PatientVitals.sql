CREATE TABLE [dbo].[PatientVitals]
(
[PatientVitalsId] [int] NOT NULL IDENTITY(1, 1),
[PatientID] [int] NOT NULL,
[SourceSystemId] [int] NOT NULL,
[ServiceDateTime] [datetime] NOT NULL,
[HeightCM] [decimal] (5, 2) NULL,
[HeightComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WeightKG] [decimal] (5, 2) NULL,
[WeightComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemperatureCelcius] [decimal] (5, 2) NULL,
[TemperatureComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pulse] [smallint] NULL,
[PulseComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Respiration] [smallint] NULL,
[RespirationComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BloodPressureSystolic] [smallint] NULL,
[BloodPressureDiastolic] [smallint] NULL,
[BloodPressureComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OxygenSaturationSP02] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OxygenSaturationComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Clinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientVitalsComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterIdentifier] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RenderingProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientIVitals_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientVitals_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientVitals_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientVitals] ADD CONSTRAINT [PK_PatientVitals] PRIMARY KEY CLUSTERED  ([PatientVitalsId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientVitals_PatientId] ON [dbo].[PatientVitals] ([PatientID]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientVitals_PatientIdSSServiceDate] ON [dbo].[PatientVitals] ([PatientID], [ServiceDateTime], [SourceSystemId], [EncounterIdentifier]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
