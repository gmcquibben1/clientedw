CREATE TABLE [dbo].[JhacgPatientListStaging]
(
[LbPatientId] [int] NOT NULL,
[BirthDate] [date] NULL,
[GenderId] [tinyint] NULL,
[ProviderNPI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JhacgPatientListStaging] ADD CONSTRAINT [PK__JhacgPat__B4C8BCB48BA73370] PRIMARY KEY CLUSTERED  ([LbPatientId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JhacgPatientListStaging] ADD CONSTRAINT [UQ__JhacgPat__B4C8BCB59A12B855] UNIQUE NONCLUSTERED  ([LbPatientId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
