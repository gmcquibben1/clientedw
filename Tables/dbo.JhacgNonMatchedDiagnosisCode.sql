CREATE TABLE [dbo].[JhacgNonMatchedDiagnosisCode]
(
[patient_id] [int] NULL,
[dx_version] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dx_cd] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
