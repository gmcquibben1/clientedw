CREATE TABLE [dbo].[MemberRosterTable16History]
(
[HICNO] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sex] [int] NULL,
[Birthdate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateofDeath] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [date] NULL,
[FileTimeLine] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileInputName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[loadDateTime] [datetime] NULL
) ON [PRIMARY]
GO
