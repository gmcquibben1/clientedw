CREATE TABLE [dbo].[MemberRosterArchives]
(
[HICNO] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[First Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sex1] [smallint] NULL,
[Birth date] [datetime] NULL,
[Deceased Beneficiary Flag2] [smallint] NULL,
[ACO Participant TIN Number] [int] NULL,
[Count of Primary Care Services3] [smallint] NULL,
[StatusDesc] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[medicare_status] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[ACO] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileTimeline] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileInputName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO Participant Type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timeline] [date] NULL,
[TimelineID] [int] NULL,
[InitialRoster] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__MemberRos__Initi__339FAB6E] DEFAULT ('N'),
[DateofDeath] [date] NULL,
[AssignmentFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrevAssignmentFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndividualNPI] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PluralityPrimaryCare] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartAPartB] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupHealthPlan] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NotResideInUS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludedSharedSavingsInitiative] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoPhysicianVisitACO] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HCCRiskScoreESRD] [decimal] (10, 3) NULL,
[HCCRiskScoreDisabled] [decimal] (10, 3) NULL,
[HCCRiskScoreAgedDual] [decimal] (10, 3) NULL,
[HCCRiskScoreAgedNonDual] [decimal] (10, 3) NULL,
[ReasonTypeID] [int] NULL
) ON [PRIMARY]
GO
