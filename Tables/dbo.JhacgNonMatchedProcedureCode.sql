CREATE TABLE [dbo].[JhacgNonMatchedProcedureCode]
(
[patient_id] [int] NULL,
[procedure_code_type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[procedure_code] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
