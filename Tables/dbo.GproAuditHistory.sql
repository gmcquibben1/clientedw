CREATE TABLE [dbo].[GproAuditHistory]
(
[GproAuditHistoryId] [int] NOT NULL IDENTITY(1, 1),
[GproPatientRankingId] [int] NOT NULL,
[GProPatientMeasureId] [int] NOT NULL,
[GproMeasureTypeId] [int] NOT NULL,
[Comment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_GproAuditHistory_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL,
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GproAuditHistory] ADD CONSTRAINT [PK_GproAuditHistory] PRIMARY KEY CLUSTERED  ([GproAuditHistoryId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
