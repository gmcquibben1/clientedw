CREATE TABLE [dbo].[PatientProcedureInternalValue]
(
[PatientProcedureInternalValueId] [int] NOT NULL IDENTITY(1, 1),
[HCPC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LbPatientId] [int] NOT NULL,
[ProcedureDate] [datetime] NOT NULL,
[ProcedureValue] [decimal] (8, 2) NULL,
[RawValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceSystemId] [int] NULL,
[SourceSystemName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientProcedureInternalValue] ADD CONSTRAINT [PK_PatientProcedureInternalValue] PRIMARY KEY CLUSTERED  ([PatientProcedureInternalValueId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
