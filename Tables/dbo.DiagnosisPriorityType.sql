CREATE TABLE [dbo].[DiagnosisPriorityType]
(
[DiagnosisPriorityTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_DiagnosisPriorityType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisPriorityType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisPriorityType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisPriorityType] ADD CONSTRAINT [PK_DiagnosisPriorityType] PRIMARY KEY CLUSTERED  ([DiagnosisPriorityTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
