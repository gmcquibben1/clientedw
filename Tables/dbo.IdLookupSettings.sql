CREATE TABLE [dbo].[IdLookupSettings]
(
[IdLookupSettingsId] [int] NOT NULL IDENTITY(1, 1),
[TargetTableName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LookupTableName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetFieldName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LookupFieldName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetIdFieldName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LookupIdFieldName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NULL,
[CreateDateTime] [datetime] NULL,
[CreateLbUserId] [int] NULL,
[ModifyDateTime] [datetime] NULL,
[ModifyLbUserId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IdLookupSettings] ADD CONSTRAINT [PK__IdLookup__08C77E68D0C89302] PRIMARY KEY CLUSTERED  ([IdLookupSettingsId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
