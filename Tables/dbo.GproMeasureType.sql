CREATE TABLE [dbo].[GproMeasureType]
(
[GproMeasureTypeId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RankColumnName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_GProMeasureType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_GProMeasureType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GproMeasureType] ADD CONSTRAINT [PK_GproMeasureType] PRIMARY KEY CLUSTERED  ([GproMeasureTypeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
