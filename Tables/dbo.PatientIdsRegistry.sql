CREATE TABLE [dbo].[PatientIdsRegistry]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Family_Id] [uniqueidentifier] NULL,
[IDType] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdValue] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateTimestamp] [datetime] NULL CONSTRAINT [DF_PatientIdsRegistry_CreateTimestamp] DEFAULT (getdate()),
[ModifyTimestamp] [datetime] NULL CONSTRAINT [DF_PatientIdsRegistry_ModifyTimestamp] DEFAULT (getdate()),
[IdRank] [int] NULL,
[IdComments] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdMetaType] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdFeed] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdSource] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
