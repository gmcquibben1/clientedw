CREATE TABLE [dbo].[GproPatientRankingGproMeasureType]
(
[GproPatientRankingGproMeasureTypeId] [int] NOT NULL IDENTITY(1, 1),
[GproPatientRankingId] [int] NOT NULL,
[GproMeasureTypeId] [int] NOT NULL,
[MeasureRank] [int] NOT NULL,
[DeleteInd] [bit] NOT NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_GproPatientRankingGproMeasureType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_GproPatientRankingGproMeasureType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GproPatientRankingGproMeasureType] ADD CONSTRAINT [PK_GproPatientRankingGproMeasureType] PRIMARY KEY CLUSTERED  ([GproPatientRankingGproMeasureTypeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
