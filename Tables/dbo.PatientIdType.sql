CREATE TABLE [dbo].[PatientIdType]
(
[PatientId] [int] NOT NULL,
[PatientId1TypeName] [varchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId2TypeName] [varchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId3TypeName] [varchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId4TypeName] [varchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientIdType] ADD CONSTRAINT [PK_patientIdType] PRIMARY KEY CLUSTERED  ([PatientId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
