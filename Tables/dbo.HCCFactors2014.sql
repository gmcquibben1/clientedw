CREATE TABLE [dbo].[HCCFactors2014]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Variable] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HCCNo] [int] NULL,
[Disease Group] [varchar] (129) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2013 Factor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2014 Factor] [decimal] (28, 10) NULL,
[Avg_BidRate] [decimal] (28, 12) NULL,
[Normalized_Score] [decimal] (28, 16) NULL,
[Coding_Intensity] [decimal] (28, 16) NULL,
[New_Rate] [decimal] (28, 13) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HCCFactors2014] ADD CONSTRAINT [PK__HCCFacto__3214EC27500BD478] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
