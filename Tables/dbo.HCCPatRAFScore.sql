CREATE TABLE [dbo].[HCCPatRAFScore]
(
[PatientId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgeFactor] [decimal] (28, 10) NULL,
[TotalHCCScore] [decimal] (38, 10) NULL,
[RAFScore] [decimal] (38, 10) NULL
) ON [PRIMARY]
GO
