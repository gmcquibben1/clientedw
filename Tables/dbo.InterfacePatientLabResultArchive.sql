CREATE TABLE [dbo].[InterfacePatientLabResultArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientLabResultId] [int] NOT NULL,
[InterfacePatientLabOrderId] [int] NOT NULL,
[ObservationCode1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ObservationDescription1] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ObservationCodingSystemName1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ObservationCode2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationDescription2] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationCodingSystemName2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Units] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReferenceRange] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AbnormalFlag] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultComment] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationDate] [datetime] NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
