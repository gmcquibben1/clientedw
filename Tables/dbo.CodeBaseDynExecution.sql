CREATE TABLE [dbo].[CodeBaseDynExecution]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[MainFilter] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubFilter1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubFilter2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodeDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code2Execute] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MainFilterExecutionOrderSno] [int] NULL,
[SubFilter1ExecutionOrderSno] [int] NULL,
[SubFilter2ExecutionOrderSno] [int] NULL,
[EnableFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
