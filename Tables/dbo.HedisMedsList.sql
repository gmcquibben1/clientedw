CREATE TABLE [dbo].[HedisMedsList]
(
[ndc_code] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[brand_name] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[generic_product_name] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[route] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[category] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[drug_id] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[list] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ListYear] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[drug] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dose] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[unit of measure] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[form] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[package_size] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HedisMedsList_ListNdcCode] ON [dbo].[HedisMedsList] ([list]) INCLUDE ([ndc_code]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
