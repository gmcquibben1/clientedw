CREATE TABLE [dbo].[ClinicalDataLastUpdate]
(
[ClinicalDataLastUpdateId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[ClinicalDataTypeID] [int] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClinicalDataLastUpdate] ADD CONSTRAINT [PK_ClinicalDataLastUpdate] PRIMARY KEY CLUSTERED  ([ClinicalDataLastUpdateId]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ClinicalDataLastUpdate_ModifyDate] ON [dbo].[ClinicalDataLastUpdate] ([ClinicalDataTypeID], [ModifyDateTime]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ClinicalDataLastUpdate_PatientId] ON [dbo].[ClinicalDataLastUpdate] ([PatientId], [ClinicalDataTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
