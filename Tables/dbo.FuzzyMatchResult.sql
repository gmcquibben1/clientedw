CREATE TABLE [dbo].[FuzzyMatchResult]
(
[FuzzyMatchResultId] [bigint] NOT NULL IDENTITY(1, 1),
[FuzzyMatchTypeId] [int] NOT NULL,
[inFirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[inLastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[inPid] [int] NOT NULL,
[inExternalId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[inSourceSystemId] [int] NULL,
[matchedPid] [int] NOT NULL,
[matchedFirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MatchedLastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[matchedDOB] [date] NULL,
[matchedSourceSystemId] [int] NULL,
[Gender] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExternalId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[similarityScore] [float] NULL,
[isMerged] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FuzzyMatchResult] ADD CONSTRAINT [PK__FuzzyMat__4EFFF17CDEB7E1FC] PRIMARY KEY CLUSTERED  ([FuzzyMatchResultId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
