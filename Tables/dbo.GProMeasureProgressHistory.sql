CREATE TABLE [dbo].[GProMeasureProgressHistory]
(
[GProMeasureProgressHistoryId] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitId] [int] NOT NULL,
[BusinessUnitName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeasureTypeId] [int] NOT NULL,
[MeasureTypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeasureTypeDescription] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RankingTableColumnName] [varchar] (140) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TotalPatients] [int] NOT NULL,
[TotalComplete] [int] NOT NULL,
[ConsecutiveCount] [int] NOT NULL,
[TotalSkipped] [int] NOT NULL,
[PercentConsecutive] [float] NOT NULL,
[MeasureStatus] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeasureYear] [int] NULL,
[CMSBenchmark] [int] NULL,
[CompletedBenchmark] [int] NULL,
[CompletedTotalEligible] [int] NULL,
[CompletedDenominatorException] [int] NULL,
[CompletedDenominator] [int] NULL,
[CompletedMeasureNotMet] [int] NULL,
[CompletedMeasureMet] [int] NULL,
[CompletedPercentMeasureMet] [float] NULL,
[CMSTotalEligible] [int] NULL,
[CMSDenominatorException] [int] NULL,
[CMSDenominator] [int] NULL,
[CMSMeasureNotMet] [int] NULL,
[CMSMeasureMet] [int] NULL,
[CMSPercentMeasureMet] [float] NULL,
[GetMeasureRankResultsTotalSkipped] [float] NULL,
[GetMeasureRankResultsFirstIncomplete] [float] NULL,
[ConsecutiveTarget] [int] NULL,
[RemainingToTarget] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GProMeasureProgressHistory] ADD CONSTRAINT [PK__GProMeas__1F36EA7AC882B45C] PRIMARY KEY CLUSTERED  ([GProMeasureProgressHistoryId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
