CREATE TABLE [dbo].[PhysicianSummary]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[ProviderNPI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1Id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2Id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3Id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level4Id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level4Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level5Id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level5Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level6Id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level6Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [decimal] (10, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhysicianSummary] ADD CONSTRAINT [PK__Physicia__3213E83F9EF04A34] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
