CREATE TABLE [dbo].[EdmMaster]
(
[EdmMasterId] [int] NOT NULL IDENTITY(1, 1),
[EdmLibraryId] [int] NOT NULL,
[OrderNum] [int] NULL,
[DeleteInd] [bit] NULL,
[CreateLbUserId] [int] NULL,
[ModifyLbUserId] [int] NULL,
[CreateDateTime] [datetime] NULL,
[ModifyDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EdmMaster] ADD CONSTRAINT [PK__EdmMaste__1D8A761870168BF3] PRIMARY KEY CLUSTERED  ([EdmMasterId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
