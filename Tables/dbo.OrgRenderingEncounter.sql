CREATE TABLE [dbo].[OrgRenderingEncounter]
(
[OrgRenderingEncounterId] [int] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NOT NULL,
[RenderingProviderNPI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RenderingProviderName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterDate] [date] NULL,
[CPT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrgRenderingEncounter] ADD CONSTRAINT [PK__OrgRende__26858C7EA7F14435] PRIMARY KEY CLUSTERED  ([OrgRenderingEncounterId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
