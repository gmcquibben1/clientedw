CREATE TABLE [dbo].[PatientProcedure]
(
[PatientProcedureId] [int] NOT NULL IDENTITY(1, 1),
[PatientID] [int] NOT NULL,
[SourceSystemID] [int] NULL,
[ProcedureFunctionTypeID] [int] NULL,
[ProcedureCodeId] [int] NULL,
[ProcedureCodingSystemTypeId] [int] NULL,
[ProcedureCodeModifier1] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureCodeModifier2] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureCodeModifier3] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureCodeModifier4] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureDateTime] [datetime] NULL,
[ProcedureComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterID] [bigint] NULL,
[PerformedByProviderID] [int] NULL,
[PerformedByClinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderedByProviderID] [int] NULL,
[OrderedByClinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExternalReferenceIdentifier] [int] NULL,
[TotalCharges] [decimal] (10, 2) NULL,
[InternalChargeCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureResult] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterIdentifier] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaceOfService] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaceOfServiceCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultCodingSystemName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RenderingProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceIdentifier] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevenueCode] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientProcedure_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientProcedure_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientProcedure_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL,
[OtherReferenceIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientProcedure] ADD CONSTRAINT [PK_PatientProcedureID] PRIMARY KEY CLUSTERED  ([PatientProcedureId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140905-151751] ON [dbo].[PatientProcedure] ([PatientID]) INCLUDE ([ProcedureDateTime]) WITH (FILLFACTOR=90, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientProcedure_PatientIdDeleteInd] ON [dbo].[PatientProcedure] ([PatientID], [DeleteInd]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NCIX_PatientProcedure_PPPS] ON [dbo].[PatientProcedure] ([PatientID], [SourceSystemID], [ProcedureCodeId], [ProcedureDateTime]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
