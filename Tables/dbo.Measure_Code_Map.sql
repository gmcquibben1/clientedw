CREATE TABLE [dbo].[Measure_Code_Map]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[BusinessDescriptor] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessFlag] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourcedFrom] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodeValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodeType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodeValueRaw] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifierValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[enabled_flag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureAbbr] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140930-124604] ON [dbo].[Measure_Code_Map] ([enabled_flag], [BusinessDescriptor], [MeasureAbbr]) INCLUDE ([CodeType], [CodeValue], [ModifierValue], [ProcessFlag]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
