CREATE TABLE [dbo].[PatientDataExportList]
(
[PatientDataExportListId] [int] NOT NULL IDENTITY(1, 1),
[BackgroundProcessRequestQueueId] [int] NOT NULL,
[LBPatientId] [int] NOT NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientDataExportList_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientDataExportList_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientDataExportList] ADD CONSTRAINT [PK_PatientDataExportList] PRIMARY KEY CLUSTERED  ([PatientDataExportListId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
