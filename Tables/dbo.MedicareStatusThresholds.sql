CREATE TABLE [dbo].[MedicareStatusThresholds]
(
[BENE_MDCR_STUS_CD] [int] NULL,
[BENE_DUAL_STUS_CD] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TruncationAmt] [int] NULL,
[Year] [int] NULL
) ON [PRIMARY]
GO
