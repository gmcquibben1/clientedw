CREATE TABLE [dbo].[InterfacePatientLabOrderArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientLabOrderId] [int] NOT NULL,
[InterfacePatientId] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[OrderNumber] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceCodingSystemName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderingProviderId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderingProviderIdType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderingProviderName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
