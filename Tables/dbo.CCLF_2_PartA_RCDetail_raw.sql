CREATE TABLE [dbo].[CCLF_2_PartA_RCDetail_raw]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[CUR_CLM_UNIQ_ID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LINE_NUM] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_TYPE_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LINE_FROM_DT] [date] NULL,
[CLM_LINE_THRU_DT] [date] NULL,
[CLM_LINE_REV_CTR_CD] [nchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LINE_INSTNL_REV_CTR_DT] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LINE_HCPCS_CD] [nchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_EQTBL_BIC_HICN_NUM] [nchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRVDR_OSCAR_NUM] [nchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_FROM_DT] [date] NULL,
[CLM_THRU_DT] [date] NULL,
[CLM_LINE_ALOWD_UNIT_QTY] [decimal] (9, 2) NULL,
[CLM_LINE_CVRD_PD_AMT] [decimal] (15, 2) NULL,
[HCPCS_1_MDFR_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HCPCS_2_MDFR_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HCPCS_3_MDFR_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HCPCS_4_MDFR_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HCPCS_5_MDFR_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fileDate] [date] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCLF_2_PartA_RCDetail_raw] ADD CONSTRAINT [PK_CCLF_2_PartA_RCDetail_raw] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
