CREATE TABLE [dbo].[SystemImportLogMetricType]
(
[SystemImportLogMetricTypeId] [int] NOT NULL,
[SystemImportDataTypeId] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EvalExpression] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateLbUserId] [int] NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
