CREATE TABLE [dbo].[JHACG_MEDICAL]
(
[patient_id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dx_version_1] [int] NULL,
[dx_cd_1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[service_begin_date] [date] NULL,
[service_end_date] [date] NULL,
[service_place] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[revenue_code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[procedure_code] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[provider_id] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[provider_specialty] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
