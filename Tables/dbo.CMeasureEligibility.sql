CREATE TABLE [dbo].[CMeasureEligibility]
(
[CMeasureEligibilityId] [int] NOT NULL IDENTITY(1, 1),
[CMeasureEligibilityTypeId] [int] NULL,
[LbPatientId] [int] NULL,
[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMeasureEligibility] ADD CONSTRAINT [PK__CMeasure__F82A9038172C0B8C] PRIMARY KEY CLUSTERED  ([CMeasureEligibilityId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
