CREATE TABLE [dbo].[InterfacePatientImmunizationArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientImmunizationId] [int] NOT NULL,
[InterfacePatientID] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[ServiceDate] [datetime] NULL,
[ImmunizationCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImmunizationCodeSystemName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationDose] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationUnits] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaterialLotNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaterialManufacturer] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteCodeSystemName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PerformingClinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
