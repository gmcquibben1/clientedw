CREATE TABLE [dbo].[PatientIdFuzzyMatchQueueResults]
(
[ProcessResultsID] [int] NOT NULL IDENTITY(1, 1),
[ProcessQueueID] [int] NULL,
[_key_in] [int] NULL,
[_key_out] [int] NULL,
[_score] [real] NULL,
[FirstName_clean] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName_clean] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScLastName] [real] NULL,
[ScFirstName] [real] NULL,
[CreateDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Creat__55F4C372] DEFAULT (getdate())
) ON [PRIMARY]
GO
