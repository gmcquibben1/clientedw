CREATE TABLE [dbo].[PatientProcedureProcedureCode]
(
[PatientProcedureProcedureCodeId] [int] NOT NULL IDENTITY(1, 1),
[PatientProcedureId] [int] NOT NULL,
[ProcedureCodingSystemTypeId] [int] NOT NULL,
[ProcedureCodeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientProcedureProcedureCode] ADD CONSTRAINT [PK_PatientProcedureProcedureCode] PRIMARY KEY CLUSTERED  ([PatientProcedureProcedureCodeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientProcedureProcedureCode_PatientProcedureId] ON [dbo].[PatientProcedureProcedureCode] ([PatientProcedureId]) INCLUDE ([ProcedureCodeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NCIX_PPPC_ProcedureCodeId] ON [dbo].[PatientProcedureProcedureCode] ([ProcedureCodeId], [PatientProcedureId]) WITH (FILLFACTOR=80, ALLOW_PAGE_LOCKS=OFF) ON [FG_INDEX1]
GO
ALTER TABLE [dbo].[PatientProcedureProcedureCode] ADD CONSTRAINT [FK_PatientProcedureProcedureCode_PatientProcedureId] FOREIGN KEY ([PatientProcedureId]) REFERENCES [dbo].[PatientProcedure] ([PatientProcedureId])
GO
ALTER TABLE [dbo].[PatientProcedureProcedureCode] ADD CONSTRAINT [FK_PatientProcedureProcedureCode_ProcedureCodingSystemTypeId] FOREIGN KEY ([ProcedureCodingSystemTypeId]) REFERENCES [dbo].[ProcedureCodingSystemType] ([ProcedureCodingSystemTypeID])
GO
