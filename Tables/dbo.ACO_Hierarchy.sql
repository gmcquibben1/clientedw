CREATE TABLE [dbo].[ACO_Hierarchy]
(
[Market] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO_Name] [nvarchar] (71) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IPA_ID] [int] NULL,
[IPA_Name] [nvarchar] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIN_Number] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIN_Name] [nvarchar] (69) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCN_Number] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCN_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ONPI_Number] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ONPI_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Provider_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Provider_Name] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Provider_NPI] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member_Full__Name] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member_Provider_Relationship_Effective_Date] [datetime] NULL,
[Member_Provider_Relationship_Termination_Date] [datetime] NULL,
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[patient_status] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fromFile] [datetime] NULL,
[Division] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PortalBuID] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
