CREATE TABLE [dbo].[InterfacePatientMedicationArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientMedicationId] [int] NOT NULL,
[InterfacePatientID] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[MedicationCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MedicationCodeName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MedicationName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationDose] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationForm] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationRoute] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationStartDate] [date] NULL,
[MedicationEndDate] [date] NULL,
[SIGCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationInstructions] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationQuantity] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationRefills] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SampleInd] [bit] NULL,
[GenericAllowedInd] [bit] NULL,
[PrescribedByNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
