CREATE TABLE [dbo].[PatientAppointmentProcessQueue]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientAppointmentID] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InterfacePatientID] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[AppointmentDate] [datetime] NULL,
[BeginTime] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndTime] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DurationInMinutes] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppointmentResource] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppointmentReason] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppointmentTypeId] [int] NULL,
[AppointmentType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppointmentDetail] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderClinicianName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[ProcessedInd] [int] NULL,
[ScrubbedRowNum] [int] NULL
) ON [PRIMARY]
GO
