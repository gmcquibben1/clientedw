CREATE TABLE [dbo].[SystemImportLogMetric]
(
[SystemImportLogMetricId] [int] NOT NULL,
[SystemImportLogId] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SqlUserName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateLbUserId] [int] NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_SystemImportLogMetric_CreateDateTime] DEFAULT (sysutcdatetime())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SystemImportLogMetric] ADD CONSTRAINT [PK_SystemImportLogMetric] PRIMARY KEY CLUSTERED  ([SystemImportLogMetricId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
