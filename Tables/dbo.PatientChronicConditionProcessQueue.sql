CREATE TABLE [dbo].[PatientChronicConditionProcessQueue]
(
[PatientChronicConditionId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[DiagnosisCodeId] [int] NOT NULL,
[ProviderId] [int] NULL,
[DisplayOrder] [int] NULL,
[DeleteInd] [bit] NOT NULL,
[CreateDateTime] [datetime] NULL,
[ModifyDateTime] [datetime] NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
