CREATE TABLE [dbo].[CMeasure_DefinitionsContractType]
(
[MeasureDefinitionContractTypeId] [int] NOT NULL IDENTITY(1, 1),
[MeasureID] [int] NULL,
[ContractTypeId] [int] NULL,
[HighLow] [int] NULL,
[EnableFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaskingNeeded] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnforceEligibilty] [int] NULL,
[UseClinicalDataOnlyInd] [bit] NULL,
[UseLBSupplementCodesInd] [bit] NULL,
[TARGET] [decimal] (18, 3) NULL,
[AVG] [decimal] (18, 3) NULL,
[NAT30] [decimal] (18, 3) NULL,
[NAT90] [decimal] (18, 3) NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_CMeasure_DefinitionsContractType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_CMeasure_DefinitionsContractType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_CMeasure_DefinitionsContractType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMeasure_DefinitionsContractType] ADD CONSTRAINT [PK_CMeasure_DefinitionsContractType] PRIMARY KEY CLUSTERED  ([MeasureDefinitionContractTypeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ClinicalDecisionEngineTypeClient_ContractTypeId] ON [dbo].[CMeasure_DefinitionsContractType] ([ContractTypeId], [DeleteInd]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
