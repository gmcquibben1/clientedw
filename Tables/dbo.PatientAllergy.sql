CREATE TABLE [dbo].[PatientAllergy]
(
[PatientAllergyID] [int] NOT NULL IDENTITY(1, 1),
[PatientID] [int] NOT NULL,
[SourceSystemID] [int] NULL,
[AllergyTypeID] [int] NULL,
[AllergyDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AllergySeverityTypeID] [int] NULL,
[AllergyReactionTypeID] [int] NULL,
[AllergyStatusTypeId] [int] NULL,
[AllergyComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterID] [int] NULL,
[OnsetDate] [date] NULL,
[ActiveInd] [bit] NOT NULL CONSTRAINT [DF_PatientAllergy_ActiveInd] DEFAULT ((1)),
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientAllergy_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientAllergy_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL,
[ExternalReferenceIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherReferenceIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientAllergy] ADD CONSTRAINT [PK_PatientAllergy] PRIMARY KEY CLUSTERED  ([PatientAllergyID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientAllergy] ADD CONSTRAINT [FK_PatientAllergy_AllergyReactionType] FOREIGN KEY ([AllergyReactionTypeID]) REFERENCES [dbo].[AllergyReactionType] ([AllergyReactionTypeID])
GO
ALTER TABLE [dbo].[PatientAllergy] ADD CONSTRAINT [FK_PatientAllergy_AllergySeverityType] FOREIGN KEY ([AllergySeverityTypeID]) REFERENCES [dbo].[AllergySeverityType] ([AllergySeverityTypeID])
GO
ALTER TABLE [dbo].[PatientAllergy] ADD CONSTRAINT [FK_PatientAllergy_AllergyStatusType] FOREIGN KEY ([AllergyStatusTypeId]) REFERENCES [dbo].[AllergyStatusType] ([AllergyStatusTypeID])
GO
ALTER TABLE [dbo].[PatientAllergy] ADD CONSTRAINT [FK_PatientAllergy_AllergyType] FOREIGN KEY ([AllergyTypeID]) REFERENCES [dbo].[AllergyType] ([AllergyTypeID])
GO
