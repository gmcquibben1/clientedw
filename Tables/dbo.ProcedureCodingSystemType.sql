CREATE TABLE [dbo].[ProcedureCodingSystemType]
(
[ProcedureCodingSystemTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_ProcedureCodingSystemType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ProcedureCodingSystemType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_ProcedureCodingSystemType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProcedureCodingSystemType] ADD CONSTRAINT [PK_ProcedureCodingSystemType] PRIMARY KEY CLUSTERED  ([ProcedureCodingSystemTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_ProcedureCodingSystemType_Name] ON [dbo].[ProcedureCodingSystemType] ([Name]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
