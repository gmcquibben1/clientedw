CREATE TABLE [dbo].[ATI_PatFactorHistory]
(
[ATI_PatFactors_ID] [int] NOT NULL IDENTITY(1, 1),
[LBPatientID] [int] NULL,
[MemberID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorCount] [int] NULL,
[ATIWeightSum] [decimal] (23, 5) NULL,
[VARWeightSum] [decimal] (23, 5) NULL,
[ATIWeightFactor] [decimal] (23, 5) NULL,
[ATIScore] [decimal] (10, 2) NULL,
[WriteDate] [date] NULL,
[ArchiveDate] [date] NULL,
[Column_11] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATI_PatFactorHistory] ADD CONSTRAINT [PK__ATI_PatF__5C8D901A6E81A761] PRIMARY KEY CLUSTERED  ([ATI_PatFactors_ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
