CREATE TABLE [dbo].[PatientSocialHistory]
(
[PatientSocialHistoryId] [int] NOT NULL IDENTITY(1, 1),
[PatientID] [int] NOT NULL,
[SourceSystemId] [int] NOT NULL,
[SmokingStatus] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaritalStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmploymentStatus] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransportationInd] [bit] NULL,
[LivingArrangementCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlcoholUseInd] [bit] NULL,
[DrugUseInd] [bit] NULL,
[CaffeineUseInd] [bit] NULL,
[SexuallyActiveInd] [bit] NULL,
[ExerciseHoursPerWeek] [int] NULL,
[RecordedDateTime] [datetime] NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_InterfacePatientSocialHistory_CreateDateTime] DEFAULT (sysutcdatetime())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientSocialHistory] ADD CONSTRAINT [PK_InterfacePatientSocialHistory] PRIMARY KEY CLUSTERED  ([PatientSocialHistoryId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
