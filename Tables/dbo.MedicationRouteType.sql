CREATE TABLE [dbo].[MedicationRouteType]
(
[MedicationRouteTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_MedicationRouteType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_MedicationRouteType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MedicationRouteType] ADD CONSTRAINT [PK_MedicationRouteTypeID] PRIMARY KEY CLUSTERED  ([MedicationRouteTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_MedicationRouteType_Name] ON [dbo].[MedicationRouteType] ([Name]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
