CREATE TABLE [dbo].[DiagnosisCode]
(
[DiagnosisCodeId] [int] NOT NULL IDENTITY(1, 1),
[DiagnosisCodingSystemTypeId] [int] NOT NULL,
[DiagnosisCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiagnosisCodeRaw] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiagnosisCodeDisplay] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiagnosisDescriptionAlternate] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_DiagnosisCode_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisCode_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisCode_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisCode] ADD CONSTRAINT [PK_DiagnosisCode] PRIMARY KEY CLUSTERED  ([DiagnosisCodeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisCode] ADD CONSTRAINT [FK_DiagnosisCode_DiagnosisCodingSystemTypeId] FOREIGN KEY ([DiagnosisCodingSystemTypeId]) REFERENCES [dbo].[DiagnosisCodingSystemType] ([DiagnosisCodingSystemTypeID])
GO
