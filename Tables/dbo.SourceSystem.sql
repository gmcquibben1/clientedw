CREATE TABLE [dbo].[SourceSystem]
(
[SourceSystemId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClinicalInd] [bit] NOT NULL CONSTRAINT [DF_SourceSystem_ClinicalInd] DEFAULT ((0)),
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_SourceSystem_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_SourceSystem_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_SourceSystem_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL,
[ExternalReferenceIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherReferenceIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SourceSystem] ADD CONSTRAINT [PK_SourceSystem] PRIMARY KEY CLUSTERED  ([SourceSystemId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
