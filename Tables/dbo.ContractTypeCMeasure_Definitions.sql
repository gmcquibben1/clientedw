CREATE TABLE [dbo].[ContractTypeCMeasure_Definitions]
(
[ContractTypeCMeasure_DefinitionsId] [int] NOT NULL IDENTITY(1, 1),
[ContractTypeId] [int] NOT NULL,
[CMeasure_DefinitionsId] [int] NOT NULL,
[HighLowFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetValue] [decimal] (18, 5) NULL,
[GenerateTaskInd] [bit] NOT NULL CONSTRAINT [DF_ContractTypeCMeasure_Definitions_GenerateTaskInd] DEFAULT ((1)),
[EnableInd] [bit] NOT NULL CONSTRAINT [DF_ContractTypeCMeasure_Definitions_EnableInd] DEFAULT ((1)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ContractTypeCMeasure_Definitions_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_ContractTypeCMeasure_Definitions_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContractTypeCMeasure_Definitions] ADD CONSTRAINT [PK_ContractTypeCMeasure_Definitions] PRIMARY KEY CLUSTERED  ([ContractTypeCMeasure_DefinitionsId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContractTypeCMeasure_Definitions] ADD CONSTRAINT [FK_ContractTypeCMeasure_Definitions_CMeasure_DefinitionsId] FOREIGN KEY ([CMeasure_DefinitionsId]) REFERENCES [dbo].[CMeasure_Definitions] ([MeasureID])
GO
