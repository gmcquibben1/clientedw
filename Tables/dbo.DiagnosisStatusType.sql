CREATE TABLE [dbo].[DiagnosisStatusType]
(
[DiagnosisStatusTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_DiagnosisStatusType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisStatusType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisStatusType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisStatusType] ADD CONSTRAINT [PK_DiagnosisStatusType] PRIMARY KEY CLUSTERED  ([DiagnosisStatusTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
