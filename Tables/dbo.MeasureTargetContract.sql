CREATE TABLE [dbo].[MeasureTargetContract]
(
[MeasureTargetContractId] [int] NOT NULL IDENTITY(1, 1),
[MeasureTargetId] [int] NOT NULL,
[ContractId] [int] NOT NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL,
[DeleteInd] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MeasureTargetContract] ADD CONSTRAINT [PK_MeasureTargetContract] PRIMARY KEY CLUSTERED  ([MeasureTargetContractId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
