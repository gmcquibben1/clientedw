CREATE TABLE [dbo].[JhacgCondition]
(
[JhacgConditionId] [int] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NOT NULL,
[Condition] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConditionAbbreviation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JhacgCondition] ADD CONSTRAINT [PK_JhacgConditionId] PRIMARY KEY CLUSTERED  ([JhacgConditionId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
