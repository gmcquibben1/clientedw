CREATE TABLE [dbo].[JhacgNonMatchedPharmacyCode]
(
[patient_id] [int] NULL,
[rx_code_type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rx_cd] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
