CREATE TABLE [dbo].[PatientInsurance]
(
[PatientInsuranceId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[SourceSystemId] [int] NOT NULL,
[InsuranceName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FinancialClass] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinancialClassTypeId] [int] NOT NULL CONSTRAINT [DF_PatientInsurance_FinancialClassTypeId] DEFAULT ((0)),
[InsurancePriority] [int] NOT NULL,
[EffectiveDate] [datetime] NULL,
[ExpirationDate] [datetime] NULL,
[GroupNumber] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNumber] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientInsurance_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientInsurance_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientInsurance_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientInsurance] ADD CONSTRAINT [PK_PatientInsurance] PRIMARY KEY CLUSTERED  ([PatientInsuranceId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientInsurance] ADD CONSTRAINT [FK_PatientInsurance_FinancialClassTypeId] FOREIGN KEY ([FinancialClassTypeId]) REFERENCES [dbo].[FinancialClassType] ([FinancialClassTypeId])
GO
