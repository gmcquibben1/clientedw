CREATE TABLE [dbo].[HEDISValueSetCodes]
(
[Value Set Name] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value Set OID] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value Set Version] [datetime] NULL,
[Code] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Definition] [varchar] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code System] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code System OID] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code System Version] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code_Raw] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF__HEDISValu__Delet__379037E3] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF__HEDISValu__Creat__38845C1C] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF__HEDISValu__Modif__39788055] DEFAULT (sysutcdatetime())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NCIX_HEDISValueSetCodes_Code_Value Set Name] ON [dbo].[HEDISValueSetCodes] ([Code], [Value Set Name]) WITH (FILLFACTOR=96, PAD_INDEX=ON, ALLOW_PAGE_LOCKS=OFF) ON [FG_INDEX1]
GO
CREATE NONCLUSTERED INDEX [NCIX_HEDISValueSetCodes_Value Set Name] ON [dbo].[HEDISValueSetCodes] ([Value Set Name]) INCLUDE ([Code]) WITH (FILLFACTOR=96) ON [FG_INDEX1]
GO
