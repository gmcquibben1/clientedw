CREATE TABLE [dbo].[RefTable_CDC_L]
(
[ndc_code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[brand_name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[generic_product_name] [nvarchar] (103) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[route] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[category] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
