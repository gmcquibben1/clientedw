CREATE TABLE [dbo].[InterfacePatientAllergyArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientAllergyID] [int] NOT NULL,
[InterfacePatientID] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[AllergyType] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllergyDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AllergySeverity] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllergyReaction] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllergyComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OnsetDate] [date] NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
