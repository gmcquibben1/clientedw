CREATE TABLE [dbo].[PatientContract]
(
[PatientContractId] [int] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NOT NULL,
[ContractName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractId] [int] NULL,
[RunDateTime] [datetime] NOT NULL,
[ContractStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCurrentInd] [bit] NULL CONSTRAINT [DF__PatientCo__IsCur__3A179ED3] DEFAULT ((0)),
[LbUserId] [int] NULL,
[ContractSource] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientContract] ADD CONSTRAINT [PK__PatientC__0C810311B4E5BF58] PRIMARY KEY CLUSTERED  ([PatientContractId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
