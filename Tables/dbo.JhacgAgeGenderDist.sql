CREATE TABLE [dbo].[JhacgAgeGenderDist]
(
[age_band] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[males] [int] NULL,
[male_percent] [decimal] (6, 2) NULL,
[females] [int] NULL,
[female_percent] [decimal] (6, 2) NULL,
[total] [int] NULL,
[total_percent] [decimal] (6, 2) NULL,
[patientGroup] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
