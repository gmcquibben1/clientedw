CREATE TABLE [dbo].[CCLF_Bene_Pairs_Rooted]
(
[HIC] [nchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[family_id] [uniqueidentifier] NULL,
[ROOT_HIC] [nvarchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140520-110516] ON [dbo].[CCLF_Bene_Pairs_Rooted] ([HIC], [ROOT_HIC]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
