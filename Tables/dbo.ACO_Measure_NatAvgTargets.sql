CREATE TABLE [dbo].[ACO_Measure_NatAvgTargets]
(
[MeasureID] [int] NULL,
[avg] [decimal] (28, 10) NULL,
[nat30] [decimal] (28, 10) NULL,
[nat90] [decimal] (28, 10) NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
