CREATE TABLE [dbo].[PatientEvent]
(
[PatientEventId] [int] NOT NULL IDENTITY(1, 1),
[LBPatientId] [int] NOT NULL,
[SourceSystemId] [int] NOT NULL,
[PatientEventTypeId] [int] NOT NULL,
[EventDateTime] [datetime] NULL,
[EventDetail] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientEvent_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientEvent_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientEvent_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientEvent] ADD CONSTRAINT [PK_PatientEvent] PRIMARY KEY CLUSTERED  ([PatientEventId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientEvent] ADD CONSTRAINT [FK_PatientEvent_PatientEventTypeId] FOREIGN KEY ([PatientEventTypeId]) REFERENCES [dbo].[PatientEventType] ([PatientEventTypeId])
GO
