CREATE TABLE [dbo].[SystemImportDataType]
(
[SystemImportDataTypeId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SystemImportDataType] ADD CONSTRAINT [PK__SystemIm__F0DDE15F07CE5C1D] PRIMARY KEY CLUSTERED  ([SystemImportDataTypeId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
