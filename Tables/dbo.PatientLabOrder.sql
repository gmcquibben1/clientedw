CREATE TABLE [dbo].[PatientLabOrder]
(
[PatientLabOrderId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[SourceSystemId] [int] NOT NULL,
[OrderNumber] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderDate] [datetime] NULL,
[ServiceId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceCodingSystemName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderingProviderId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderingProviderIdType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderingProviderName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterIdentifier] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL,
[ExternalReferenceIdentifier] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientLabOrder] ADD CONSTRAINT [PK_PatientLabOrder] PRIMARY KEY CLUSTERED  ([PatientLabOrderId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientLabOrder_PatientId] ON [dbo].[PatientLabOrder] ([PatientId]) INCLUDE ([PatientLabOrderId]) WITH (FILLFACTOR=90, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientLabOrder_PatientId_Combined] ON [dbo].[PatientLabOrder] ([PatientId], [SourceSystemId], [OrderNumber]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
