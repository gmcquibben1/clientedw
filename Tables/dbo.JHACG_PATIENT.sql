CREATE TABLE [dbo].[JHACG_PATIENT]
(
[patient_id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[age] [int] NULL,
[sex] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[line_of_business] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[company] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Product] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[benefit_plan] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[health_system] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[pharmacy_cost] [decimal] (18, 0) NULL,
[total_cost] [decimal] (18, 0) NULL,
[inpatient_hospitalization_count] [int] NULL,
[emergency_visit_count] [int] NULL
) ON [PRIMARY]
GO
