CREATE TABLE [dbo].[CCLF_0_PartA_CLM_TYPES]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[CUR_CLM_UNIQ_ID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_FROM_DT] [date] NULL,
[CLM_THRU_DT] [date] NULL,
[LbPatientId] [int] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRNCPL_DGNS_CD] [nchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICD_TYPE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ERMainClaimInd] [bit] NULL,
[ERTotalPaid] [decimal] (18, 2) NULL,
[ERMasterClaimID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveIndicator] [bit] NULL,
[ActiveTotalPaid] [decimal] (18, 2) NULL,
[ActiveMasterClaimID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[30Day_ReAdmit] [int] NULL,
[60Day_ReAdmit] [int] NULL,
[90Day_ReAdmit] [int] NULL,
[LOS] [int] NULL,
[INP] [bit] NULL,
[SNF] [bit] NULL,
[OUT] [bit] NULL,
[HOSPICE] [bit] NULL,
[HH] [bit] NULL,
[ER] [bit] NULL,
[ER_INP] [bit] NULL,
[ER_POS] [bit] NULL,
[ER_REV] [bit] NULL,
[Chronic] [bit] NULL,
[RehabRev] [bit] NULL,
[Observation] [bit] NULL,
[Rehab] [bit] NULL,
[NonPay] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCLF_0_PartA_CLM_TYPES] ADD CONSTRAINT [PK__CCLF_0_P__3213E83F3SKIPPY] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=96, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCLF_0_PartA_CLM_TYPES] ADD CONSTRAINT [UNIQ_SKIPPY_CCLF_0] UNIQUE NONCLUSTERED  ([CUR_CLM_UNIQ_ID], [SourceFeed]) WITH (FILLFACTOR=96, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
