CREATE TABLE [dbo].[JHACG_PHARMACY]
(
[patient_id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rx_fill_date] [date] NULL,
[rx_code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rx_code_type] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rx_days_supply] [int] NULL
) ON [PRIMARY]
GO
