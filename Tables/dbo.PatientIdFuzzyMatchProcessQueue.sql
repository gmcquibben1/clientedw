CREATE TABLE [dbo].[PatientIdFuzzyMatchProcessQueue]
(
[ProcessQueueID] [int] NOT NULL IDENTITY(1, 1),
[MedicareNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOBDateTime] [datetime] NULL,
[Sex] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rooted_id] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Family_Id] [uniqueidentifier] NULL,
[CreateDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Creat__540C7B00] DEFAULT (getdate())
) ON [PRIMARY]
GO
