CREATE TABLE [dbo].[CPT4Mstr]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[cpt4_code_id] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[description] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[description_medium] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[description_friendly] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[type_of_service] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[cpt4_reimburs_code] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[cpt4_profile_ind] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[upin_ind] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[note] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[delete_ind] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[active_ind] [bit] NOT NULL CONSTRAINT [DF__CPT4Mstr__active__7B5B524B] DEFAULT ('True'),
[create_timestamp] [datetime] NULL CONSTRAINT [DF__CPT4Mstr__create__7C4F7684] DEFAULT (getdate()),
[created_by] [int] NULL,
[modify_timestamp] [datetime] NULL CONSTRAINT [DF__CPT4Mstr__modify__7D439ABD] DEFAULT (getdate()),
[modified_by] [int] NULL,
[row_timestamp] [timestamp] NULL
) ON [PRIMARY]
GO
