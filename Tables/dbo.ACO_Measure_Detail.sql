CREATE TABLE [dbo].[ACO_Measure_Detail]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ACO_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO] [nchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO_ABBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Practice_tin] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Practice] [nchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderID] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderNPI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Provider] [nchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientMemberID] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureSupercategory] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureCategory] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureID] [int] NULL,
[Measure] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureAbbrev] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureTarget] [decimal] (12, 8) NULL,
[Value] [tinyint] NULL,
[MeasureYear] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CareGapTypeId] [int] NULL,
[caregapruletypeid] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ACO_Measure_Detail] ADD CONSTRAINT [PK__ACO_Meas__3214EC276B318A83] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
