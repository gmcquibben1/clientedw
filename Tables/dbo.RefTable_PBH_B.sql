CREATE TABLE [dbo].[RefTable_PBH_B]
(
[ndc_code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[brand_name] [nvarchar] (61) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[generic_product_name] [nvarchar] (87) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[route] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[category] [nvarchar] (62) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
