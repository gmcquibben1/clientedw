CREATE TABLE [dbo].[CCLF_7_PartD_raw]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[CUR_CLM_UNIQ_ID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LINE_NDC_CD] [nchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_TYPE_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LINE_FROM_DT] [date] NULL,
[PRVDR_SRVC_ID_QLFYR_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_SRVC_PRVDR_GNRC_ID_NUM] [nchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_DSPNSNG_STUS_CD] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_DAW_PROD_SLCTN_CD] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LINE_SRVC_UNIT_QTY] [decimal] (24, 4) NULL,
[CLM_LINE_DAYS_SUPLY_QTY] [int] NULL,
[PRVDR_PRSBNG_ID_QLFYR_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_PRSBNG_PRVDR_GNRC_ID_NUM] [nchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LINE_BENE_PMT_AMT] [decimal] (12, 2) NULL,
[CLM_ADJSMT_TYPE_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_EFCTV_DT] [date] NULL,
[CLM_IDR_LD_DT] [date] NULL,
[CLM_LINE_RX_SRVC_RFRNC_NUM] [nchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LINE_RX_FILL_NUM] [int] NULL,
[fileDate] [date] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCLF_7_PartD_raw] ADD CONSTRAINT [PK_CCLF_7_PartD_raw] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
