CREATE TABLE [dbo].[PatientPregnancyStat]
(
[PatientPregnancyStat_ID] [int] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NULL,
[EpisiotomyInd] [int] NULL,
[DeliveryDate] [datetime] NULL,
[PrenatalVisits_FirstTrimester] [int] NULL,
[PrenatalVisits_SecondTrimester] [int] NULL,
[PrenatalVisits_ThirdTrimester] [int] NULL,
[ERVisits_FirstTrimester] [int] NULL,
[ERVisits_SecondTrimester] [int] NULL,
[ERVisits_ThirdTrimester] [int] NULL,
[PartA_Cost_FirstTrimester] [decimal] (10, 2) NULL,
[PartA_Cost_SecondTrimester] [decimal] (10, 2) NULL,
[PartA_Cost_ThirdTrimester] [decimal] (10, 2) NULL,
[PartB_Cost_FirstTrimester] [decimal] (10, 2) NULL,
[PartB_Cost_SecondTrimester] [decimal] (10, 2) NULL,
[PartB_Cost_ThirdTrimester] [decimal] (10, 2) NULL,
[PartA_MEOC_Cost_FirstTrimester] [decimal] (10, 2) NULL,
[PartA_MEOC_Cost_SecondTrimester] [decimal] (10, 2) NULL,
[PartA_MEOC_Cost_ThirdTrimester] [decimal] (10, 2) NULL,
[PartB_MEOC_Cost_FirstTrimester] [decimal] (10, 2) NULL,
[PartB_MEOC_Cost_SecondTrimester] [decimal] (10, 2) NULL,
[PartB_MEOC_Cost_ThirdTrimester] [decimal] (10, 2) NULL,
[EddDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientPregnancyStat] ADD CONSTRAINT [PK__PatientP__B0066D0C6823646E] PRIMARY KEY CLUSTERED  ([PatientPregnancyStat_ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
