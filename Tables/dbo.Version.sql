CREATE TABLE [dbo].[Version]
(
[VersionId] [int] NOT NULL IDENTITY(1, 1),
[ProductTypeId] [int] NOT NULL,
[ProductVersion] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VersionDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_Version_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_Version_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_Version_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Version] ADD CONSTRAINT [PK_Version] PRIMARY KEY CLUSTERED  ([VersionId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
