CREATE TABLE [dbo].[membersMonth]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NULL,
[medicare] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[visitdate] [date] NULL,
[enrollment] [int] NULL,
[ContractName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[membersMonth] ADD CONSTRAINT [PK__membersM__3213E83FCD0014E0] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
