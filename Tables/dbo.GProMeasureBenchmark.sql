CREATE TABLE [dbo].[GProMeasureBenchmark]
(
[GProMeasureBenchmarkId] [int] NOT NULL IDENTITY(1, 1),
[GProMeasureTypeId] [int] NOT NULL,
[MeasureRateLowBound] [float] NULL,
[MeasureRateHighBound] [float] NULL,
[MeasureYear] [int] NOT NULL,
[Benchmark] [int] NOT NULL,
[QualityPoints] [decimal] (9, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GProMeasureBenchmark] ADD CONSTRAINT [PK__GProMeas__6B7DF282AC67EBED] PRIMARY KEY CLUSTERED  ([GProMeasureBenchmarkId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
