CREATE TABLE [dbo].[REV_CODE_DESC]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CLM_LINE_REV_CTR_CD] [int] NULL,
[REV_CODE_DESC] [nvarchar] (109) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
