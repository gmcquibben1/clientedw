CREATE TABLE [dbo].[MemberRosterArchiveCcpPlus]
(
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicareHICN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [date] NULL,
[AttributedPriorQuarter] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonthlyCMFAmountforCurrentQuarter] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuarterlyCMFAmountforCurrentQuarter] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonthlyCPCPAmountforCurrentQuarter] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuarterlyCPCPAmountforCurrentQuarter] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonthlyPBIPAmountforCurrentYear] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AnnualPBIPAmountforCurrentYear] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitMadethisQuarter] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeLine] [date] NULL,
[PracticeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CcpPracticeId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PracticeAddress] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [date] NULL,
[LbPatientId] [int] NULL,
[MemberRosterArchiveCcpPlusId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MemberRosterArchiveCcpPlus] ADD CONSTRAINT [PK__MemberRo__9367178D23F1C131] PRIMARY KEY CLUSTERED  ([MemberRosterArchiveCcpPlusId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
