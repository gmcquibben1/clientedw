CREATE TABLE [dbo].[CCLF_3_PartA_ProcCd_raw]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[CUR_CLM_UNIQ_ID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_TYPE_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_VAL_SQNC_NUM] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_PRCDR_CD] [nchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_PRCDR_PRFRM_DT] [date] NULL,
[BENE_EQTBL_BIC_HICN_NUM] [nchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRVDR_OSCAR_NUM] [nchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_FROM_DT] [date] NULL,
[CLM_THRU_DT] [date] NULL,
[ICD_TYPE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fileDate] [date] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCLF_3_PartA_ProcCd_raw] ADD CONSTRAINT [PK_CCLF_3_PartA_ProcCd_raw] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
