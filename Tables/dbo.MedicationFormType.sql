CREATE TABLE [dbo].[MedicationFormType]
(
[MedicationFormTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_MedicationFormType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_MedicationFormType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MedicationFormType] ADD CONSTRAINT [PK_MedicationFormType] PRIMARY KEY CLUSTERED  ([MedicationFormTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_MedicationFormType_Name] ON [dbo].[MedicationFormType] ([Name]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
