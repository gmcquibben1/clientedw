CREATE TABLE [dbo].[PatientFacilityClaimStaging]
(
[PatientFacilityClaimStagingId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientFacilityClaimStagingId] [int] NOT NULL,
[LbPatientId] [int] NOT NULL,
[InterfacePatientId] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[OrgHierarchyPatientIdentifier] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PatientIdentifier] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UniqueClaimId] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FromDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThroughDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalDiagnosisCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdmittingDiagnosisCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaceOfService] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FacilityNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OperatingProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttendingProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DrgCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevenueCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureModifier1] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureModifier2] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureModifier3] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureModifier4] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdmissionTypeCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdmissionSourceCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdjustmentReasonCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientFacilityClaimStaging] ADD CONSTRAINT [PK__PatientF__7FBDD8423E12C452] PRIMARY KEY CLUSTERED  ([PatientFacilityClaimStagingId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
