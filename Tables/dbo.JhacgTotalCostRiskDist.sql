CREATE TABLE [dbo].[JhacgTotalCostRiskDist]
(
[high_total_cost_probability] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[number] [int] NULL,
[percent] [decimal] (6, 2) NULL,
[cumulative_percent] [decimal] (6, 2) NULL,
[patientGroup] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
