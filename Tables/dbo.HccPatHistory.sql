CREATE TABLE [dbo].[HccPatHistory]
(
[HccPatHistoryId] [int] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NULL,
[Year] [int] NULL,
[RiskAdjustmentFactor] [decimal] (38, 10) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HccPatHistory] ADD CONSTRAINT [PK__HccPatHi__4CEDAFCEE55DB0CD] PRIMARY KEY CLUSTERED  ([HccPatHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
