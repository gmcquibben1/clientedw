CREATE TABLE [dbo].[GproPatientMedication_Raw]
(
[GProPatientMedicationRawId] [int] NOT NULL IDENTITY(1, 1),
[ImportSetId] [int] NOT NULL,
[group-tin] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[medicare-id] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[patient-first-name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[patient-last-name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[gender] [tinyint] NULL,
[birth-date] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[medical-record-number] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[provider-npi] [bigint] NULL,
[provider-npi-two] [bigint] NULL,
[provider-npi-three] [bigint] NULL,
[clinic-identifier] [bigint] NULL,
[general-comments] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[medical-record-found] [int] NULL,
[medical-not-qualified-reason] [int] NULL,
[medical-not-qualified-date] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[caremeddoc-rank] [int] NULL,
[caremeddoc-confirmed] [int] NULL,
[caremeddoc-visit-date] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[caremeddoc-visit-confirmation] [int] NULL,
[medications-documented] [int] NULL,
[caremeddoc-comments] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GproPatientMedication_Raw] ADD CONSTRAINT [PK_GProPatientMedication_Raw] PRIMARY KEY CLUSTERED  ([GProPatientMedicationRawId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
