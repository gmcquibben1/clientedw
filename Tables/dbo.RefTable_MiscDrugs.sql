CREATE TABLE [dbo].[RefTable_MiscDrugs]
(
[NDC_Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GEN_CLASS_NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRADE_NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DRUG_STRN_QTY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GEN_IND] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOS_FORM_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DRUG_END_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
