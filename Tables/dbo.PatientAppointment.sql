CREATE TABLE [dbo].[PatientAppointment]
(
[PatientAppointmentId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[SourceSystemID] [int] NULL,
[SourceAppointmentId] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppointmentDate] [date] NOT NULL,
[BeginTime] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndTime] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Duration] [int] NULL,
[AppointmentResource] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppointmentReason] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppointmentTypeId] [int] NULL,
[AppointmentDetail] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderNPI] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CancelInd] [bit] NOT NULL CONSTRAINT [DF__PatientAp__Cance__536D5C82] DEFAULT ((0)),
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF__PatientAp__Delet__546180BB] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF__PatientAp__Creat__5555A4F4] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF__PatientAp__Modif__5649C92D] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
