CREATE TABLE [dbo].[PatientMedication]
(
[PatientMedicationID] [int] NOT NULL IDENTITY(1, 1),
[PatientID] [int] NOT NULL,
[MedicationID] [int] NOT NULL,
[SourceSystemID] [int] NULL,
[EncounterID] [bigint] NULL,
[ProviderID] [int] NULL,
[MedicationStartDate] [date] NULL,
[MedicationEndDate] [date] NULL,
[Quantity] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Refills] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SIGCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Instructions] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SampleInd] [bit] NULL,
[GenericAllowedInd] [bit] NULL,
[MedicationNameFromSource] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationFormFromSource] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationRouteFromSource] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationDoseFromSource] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NDCCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RxNormCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientMedication_DeleteInd] DEFAULT ((0)),
[Comment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusDescription] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterIdentifier] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientMedication_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL,
[ExternalReferenceIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherReferenceIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientMedication] ADD CONSTRAINT [PK_PatientMedication] PRIMARY KEY CLUSTERED  ([PatientMedicationID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NCIX_PatMed_LbPatientID] ON [dbo].[PatientMedication] ([PatientID]) INCLUDE ([MedicationEndDate], [MedicationID], [MedicationStartDate], [SIGCode], [SourceSystemID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientMedication_PatientIdDeleteInd] ON [dbo].[PatientMedication] ([PatientID], [DeleteInd]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientMedication] ADD CONSTRAINT [FK_PatientMedication_Medication] FOREIGN KEY ([MedicationID]) REFERENCES [dbo].[Medication] ([MedicationID])
GO
