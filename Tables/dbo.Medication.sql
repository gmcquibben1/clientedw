CREATE TABLE [dbo].[Medication]
(
[MedicationID] [int] NOT NULL IDENTITY(1, 1),
[BrandName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GenericName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MedicationFormTypeID] [int] NOT NULL,
[MedicationRouteTypeID] [int] NOT NULL,
[MedicationDose] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NDCCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RxNormCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RetiredInd] [bit] NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_Medication_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_Medication_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Medication] ADD CONSTRAINT [PK_Medication] PRIMARY KEY CLUSTERED  ([MedicationID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Medication] ADD CONSTRAINT [FK_Medication_MedicationFormType] FOREIGN KEY ([MedicationFormTypeID]) REFERENCES [dbo].[MedicationFormType] ([MedicationFormTypeID])
GO
ALTER TABLE [dbo].[Medication] ADD CONSTRAINT [FK_Medication_MedicationRouteType] FOREIGN KEY ([MedicationRouteTypeID]) REFERENCES [dbo].[MedicationRouteType] ([MedicationRouteTypeID])
GO
