CREATE TABLE [dbo].[InterfacePatientProcedureArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientProcedureId] [int] NOT NULL,
[InterfacePatientID] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[ProcedureFunctionTypeID] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureCodeModifier1] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureCodeModifier2] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureCodeModifier3] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureCodeModifier4] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureDateTime] [datetime] NULL,
[ProcedureComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PerformedByClinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderedByClinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
