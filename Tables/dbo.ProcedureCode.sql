CREATE TABLE [dbo].[ProcedureCode]
(
[ProcedureCodeId] [int] NOT NULL IDENTITY(1, 1),
[ProcedureCodingSystemTypeId] [int] NOT NULL,
[ProcedureCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcedureDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcedureDescriptionAlternate] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_ProcedureCode_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ProcedureCode_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_ProcedureCode_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProcedureCode] ADD CONSTRAINT [PK_ProcedureCode] PRIMARY KEY CLUSTERED  ([ProcedureCodeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ProcedureCode_DeleteInd] ON [dbo].[ProcedureCode] ([DeleteInd]) INCLUDE ([ProcedureCode], [ProcedureCodeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ProcedureCode_ProcedureCode] ON [dbo].[ProcedureCode] ([ProcedureCode]) INCLUDE ([ProcedureCodeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ProcedureCode_ProcedureCodeDeleteInd] ON [dbo].[ProcedureCode] ([ProcedureCode], [DeleteInd]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProcedureCode] ADD CONSTRAINT [FK_ProcedureCode_ProcedureCodingSystemTypeId] FOREIGN KEY ([ProcedureCodingSystemTypeId]) REFERENCES [dbo].[ProcedureCodingSystemType] ([ProcedureCodingSystemTypeID])
GO
