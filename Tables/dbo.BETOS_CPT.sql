CREATE TABLE [dbo].[BETOS_CPT]
(
[HCPC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BETOSGroupID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
