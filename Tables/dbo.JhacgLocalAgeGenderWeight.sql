CREATE TABLE [dbo].[JhacgLocalAgeGenderWeight]
(
[age_gender] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[patient_count] [int] NULL,
[total_cost] [float] NULL,
[local_age_gender_weight] [float] NULL,
[patientGroup] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
