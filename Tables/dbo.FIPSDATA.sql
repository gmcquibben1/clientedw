CREATE TABLE [dbo].[FIPSDATA]
(
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State ANSI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[County ANSI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[County Name] [varchar] (63) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ANSI Cl] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
