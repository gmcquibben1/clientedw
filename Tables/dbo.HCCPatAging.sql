CREATE TABLE [dbo].[HCCPatAging]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[PatientID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HCCNo] [int] NULL,
[DiseaseGroup] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Factor] [decimal] (28, 10) NULL,
[DateOfService] [date] NULL,
[Count] [int] NULL,
[ValidFlag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HCCPatAging] ADD CONSTRAINT [PK__HCCPatAg__3213E83FA7DC8BE2] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
