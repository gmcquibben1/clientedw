CREATE TABLE [dbo].[HEDISValueSetNames]
(
[Measure ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Measure Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value Set Name] [nvarchar] (83) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value Set OID] [nvarchar] (63) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
