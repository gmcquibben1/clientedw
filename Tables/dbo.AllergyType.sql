CREATE TABLE [dbo].[AllergyType]
(
[AllergyTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_AllergyType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_AllergyType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AllergyType] ADD CONSTRAINT [PK_AllergyType] PRIMARY KEY CLUSTERED  ([AllergyTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_AllergyType_Name] ON [dbo].[AllergyType] ([Name]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
