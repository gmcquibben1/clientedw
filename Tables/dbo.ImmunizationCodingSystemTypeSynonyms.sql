CREATE TABLE [dbo].[ImmunizationCodingSystemTypeSynonyms]
(
[ImmunizationCodingSystemTypeSynonymsId] [int] NOT NULL IDENTITY(1, 1),
[ImmunizationCodingSystemTypeID] [int] NOT NULL,
[SynonymName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
