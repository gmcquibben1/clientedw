CREATE TABLE [dbo].[PatientRosterMonthsRaw]
(
[PatientRosterMonthsRawID] [int] NOT NULL IDENTITY(1, 1),
[SourceSystemID] [int] NULL,
[PatientExternalID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Gender] [smallint] NULL,
[Birthdate] [datetime] NOT NULL,
[Address] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemberMonth] [datetime] NULL,
[DeathDate] [datetime] NULL,
[ProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF__PatientRo__Creat__67DE6983] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientRosterMonthsRaw] ADD CONSTRAINT [PK__PatientR__48CAD51A9D7DE58B] PRIMARY KEY CLUSTERED  ([PatientRosterMonthsRawID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
