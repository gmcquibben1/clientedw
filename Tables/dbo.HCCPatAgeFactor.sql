CREATE TABLE [dbo].[HCCPatAgeFactor]
(
[PatientId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenderId] [int] NULL,
[Age] [int] NULL,
[AgeGroup] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AgeFactor] [decimal] (28, 10) NULL
) ON [PRIMARY]
GO
