CREATE TABLE [dbo].[ecqmValueSetCodes]
(
[Value Set Name] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value Set OID] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Definition Version] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Expansion Version] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code System] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code System OID] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code System Version] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Expansion ID] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
