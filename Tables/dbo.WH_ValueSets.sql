CREATE TABLE [dbo].[WH_ValueSets]
(
[Value Set] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hysterectomy Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
