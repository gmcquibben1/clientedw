CREATE TABLE [dbo].[InterfacePatientDiagnosisArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientDiagnosisId] [int] NOT NULL,
[InterfacePatientID] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[DiagnosisType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisStatus] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisSeverity] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisConfidentialityInd] [bit] NULL,
[DiagnosisPriority] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisClassification] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisDateTime] [datetime] NULL,
[DiagnosisOnsetDate] [date] NULL,
[DiagnosisResolvedDate] [date] NULL,
[Clinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
