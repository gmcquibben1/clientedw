CREATE TABLE [dbo].[GproAuditHistoryDataSource]
(
[GproAuditHistoryDataSourceId] [int] NOT NULL IDENTITY(1, 1),
[GproAuditHistoryId] [int] NOT NULL,
[ClinicalTable] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryKeyName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryKeyId] [int] NOT NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_GproAuditHistoryDataSource_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GproAuditHistoryDataSource] ADD CONSTRAINT [PK_GproAuditHistoryDataSource] PRIMARY KEY CLUSTERED  ([GproAuditHistoryDataSourceId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
