CREATE TABLE [dbo].[PatientLatestValue]
(
[PatientLatestValueId] [int] NOT NULL IDENTITY(1, 1),
[PatientMemberId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[LatestVitalsDate] [date] NULL,
[LatestBpSystolic] [int] NULL,
[LatestBpDiastolic] [int] NULL,
[LatestBMI] [decimal] (8, 3) NULL,
[LatestHbA1CDate] [date] NULL,
[LatestHbA1CValue] [decimal] (8, 3) NULL,
[AceArbLastFill] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientLatestValue] ADD CONSTRAINT [PK__PatientL__64E8BA38A6EF519B] PRIMARY KEY CLUSTERED  ([PatientLatestValueId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
