CREATE TABLE [dbo].[JhacgPatientRav]
(
[patient_Id] [int] NOT NULL,
[acgRavValue] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JhacgPatientRav] ADD CONSTRAINT [PK_JhacgPatientRav] PRIMARY KEY CLUSTERED  ([patient_Id]) WITH (FILLFACTOR=96, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
