CREATE TABLE [dbo].[SystemImportLog]
(
[SystemImportLogId] [int] NOT NULL IDENTITY(1, 1),
[SourceSystemId] [int] NOT NULL,
[SystemImportDataTypeId] [int] NOT NULL,
[StartDateTime] [datetime] NOT NULL,
[FinishDateTime] [datetime] NULL,
[RowsAffected] [int] NULL,
[LbUserId] [int] NULL,
[CreateDateTime] [datetime] NOT NULL,
[SqlUserName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SystemImportLog] ADD CONSTRAINT [PK__SystemIm__C6FCCD1150E6B258] PRIMARY KEY CLUSTERED  ([SystemImportLogId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
