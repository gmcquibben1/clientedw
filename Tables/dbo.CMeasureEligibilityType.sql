CREATE TABLE [dbo].[CMeasureEligibilityType]
(
[CMeasureEligibilityTypeId] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplayValue] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL,
[ModifyDateTime] [datetime] NULL,
[CreateLbUserId] [int] NULL,
[ModifyLbUserId] [int] NULL,
[EvalExpression] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMeasureEligibilityType] ADD CONSTRAINT [PK__CMeasure__5815BF0E6AB8C5F7] PRIMARY KEY CLUSTERED  ([CMeasureEligibilityTypeId]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
