CREATE TABLE [dbo].[OrgHierarchy]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[State] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeoCodeLong] [decimal] (12, 6) NULL,
[GeoCodeLat] [decimal] (12, 6) NULL,
[Level1Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1TypeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2TypeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level3TypeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level4Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level4Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level4TypeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level5Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level5Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level5TypeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level6Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level6Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level6TypeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemberID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemberFullName] [varchar] (350) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member_Provider_Relationship_Effective_Date] [datetime] NULL,
[Member_Provider_Relationship_Termination_Date] [datetime] NULL,
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[PatientId1TypeName] [varchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId2TypeName] [varchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId3TypeName] [varchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId4TypeName] [varchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Patient_Status] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderNPI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [date] NULL,
[GenderId] [tinyint] NULL,
[County] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastUpdatedTimestamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrgHierarchy] ADD CONSTRAINT [PK__OrgHiera__3214EC2776E6D6CB] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrgHierarchy_LbPatientId] ON [dbo].[OrgHierarchy] ([LbPatientId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_Nonclustered_OrgHierarchy_Level6Id] ON [dbo].[OrgHierarchy] ([Level6Id]) INCLUDE ([BENE_HIC_NUM], [Level2Id], [Level2Name], [Level6Name], [Patient_Status], [ProviderName], [ProviderNPI]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
