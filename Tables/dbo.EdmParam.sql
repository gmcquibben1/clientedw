CREATE TABLE [dbo].[EdmParam]
(
[EdmParam] [int] NOT NULL IDENTITY(1, 1),
[EdmMasterId] [int] NOT NULL,
[EdmLibraryParamId] [int] NOT NULL,
[ParamValue] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NULL,
[CreateLbUserId] [int] NULL,
[ModifyLbUserId] [int] NULL,
[CreateDateTime] [datetime] NULL,
[ModifyDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EdmParam] ADD CONSTRAINT [PK__EdmParam__40066FD68415C3CE] PRIMARY KEY CLUSTERED  ([EdmParam]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
