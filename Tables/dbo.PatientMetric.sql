CREATE TABLE [dbo].[PatientMetric]
(
[PatientMetricId] [int] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NULL,
[MetricName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MetricType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MetricValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientMetric] ADD CONSTRAINT [PK__PatientM__C0C6E087B8B63F9E] PRIMARY KEY CLUSTERED  ([PatientMetricId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
