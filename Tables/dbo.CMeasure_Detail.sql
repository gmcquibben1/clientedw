CREATE TABLE [dbo].[CMeasure_Detail]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MeasureID] [int] NOT NULL,
[PatientMemberID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LbPatientID] [int] NOT NULL,
[Denominator] [int] NOT NULL CONSTRAINT [DF__CMeasure___Denom__6D0D32F4] DEFAULT ((1)),
[Exclusion] [int] NOT NULL,
[Numerator] [int] NOT NULL,
[MeasureOverrideStatus] [int] NULL CONSTRAINT [DF__CMeasure___Measu__6E01572D] DEFAULT ((0)),
[LbTaskID] [int] NULL,
[TaskStatus] [int] NULL,
[TaskOverrideStatus] [int] NULL CONSTRAINT [DF__CMeasure___TaskO__6EF57B66] DEFAULT ((0)),
[GPRO] [int] NULL,
[CreateTimestamp] [datetime] NULL CONSTRAINT [DF__CMeasure___Creat__6FE99F9F] DEFAULT (getdate()),
[ModifyTimestamp] [datetime] NULL CONSTRAINT [DF__CMeasure___Modif__70DDC3D8] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMeasure_Detail] ADD CONSTRAINT [PK__CMeasure__ID] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
