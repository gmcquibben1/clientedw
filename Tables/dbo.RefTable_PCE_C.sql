CREATE TABLE [dbo].[RefTable_PCE_C]
(
[ndc_code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[brand_name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[generic_product_name] [nvarchar] (90) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[route] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[category] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
