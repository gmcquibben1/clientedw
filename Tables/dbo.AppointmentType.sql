CREATE TABLE [dbo].[AppointmentType]
(
[AppointmentTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF__Appointme__Delet__5832119F] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF__Appointme__Creat__592635D8] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
