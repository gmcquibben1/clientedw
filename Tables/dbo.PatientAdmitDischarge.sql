CREATE TABLE [dbo].[PatientAdmitDischarge]
(
[PatientAdmitDischargeId] [int] NOT NULL IDENTITY(1, 1),
[LBPatientId] [int] NULL,
[AdmitPatientEventId] [int] NOT NULL,
[DischargePatientEventId] [int] NULL,
[AdmitType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdmitReason] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BedType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NotificationDate] [date] NULL,
[AdmitDate] [datetime] NULL,
[DischargeDate] [datetime] NULL,
[PostDischargeVisitDate] [date] NULL,
[DischargeDisposition] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnsosisDescription] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FacilityName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCPName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OnDailyCensusInd] [bit] NOT NULL CONSTRAINT [DF_PatientAdmitDischarge_OnDailyCensusInd] DEFAULT ((1)),
[DischargeToOtherFacility] [bit] NULL,
[SNFRehab] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcuteInd] [bit] NULL CONSTRAINT [DF_PatientAdmitDischarge_AcuteInd] DEFAULT ((0)),
[SourceSystemId] [int] NOT NULL,
[PatientClass] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientAdmitDischarge_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientAdmitDischarge_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientAdmitDischarge_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientAdmitDischarge] ADD CONSTRAINT [PK_PatientAdmitDischarge] PRIMARY KEY CLUSTERED  ([PatientAdmitDischargeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
