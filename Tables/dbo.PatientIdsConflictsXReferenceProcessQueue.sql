CREATE TABLE [dbo].[PatientIdsConflictsXReferenceProcessQueue]
(
[GroupFormationRuleId] [int] NULL,
[rooted_id] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[family_id] [uniqueidentifier] NULL,
[ManualOverride] [int] NULL,
[Comments] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Creat__6AEFE058] DEFAULT (getdate()),
[RaisedById] [int] NULL,
[FamilyIdsPack] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
