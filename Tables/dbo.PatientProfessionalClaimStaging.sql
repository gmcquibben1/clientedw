CREATE TABLE [dbo].[PatientProfessionalClaimStaging]
(
[PatientProfessionalClaimStagingId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientProfessionalClaimStagingId] [int] NOT NULL,
[LbPatientId] [int] NOT NULL,
[InterfacePatientId] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[OrgHierarchyPatientIdentifier] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PatientIdentifier] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UniqueClaimId] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FromDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThroughDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderSpecialtyCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderTaxonomyCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaceOfService] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureModifier1] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureModifier2] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureModifier3] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcedureModifier4] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalDiagnosisCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderTaxId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RenderingProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdjustmentReasonCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalChargeAmount] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitQuantity] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisCode2] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisCode3] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisCode4] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisCode5] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisCode6] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisCode7] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisCode8] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisCode9] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientProfessionalClaimStaging] ADD CONSTRAINT [PK__PatientP__40DAE2610709CAC7] PRIMARY KEY CLUSTERED  ([PatientProfessionalClaimStagingId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
