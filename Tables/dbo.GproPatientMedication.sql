CREATE TABLE [dbo].[GproPatientMedication]
(
[GproPatientMedicationId] [int] NOT NULL IDENTITY(1, 1),
[GproPatientRankingId] [int] NOT NULL,
[GproPatientMeasureId] [int] NOT NULL,
[CareMeddocVisitDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CareMeddocVisitConfirmed] [int] NULL,
[MedicationDocumented] [int] NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_GproPatientMedication_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_GproPatientMedication_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GproPatientMedication] ADD CONSTRAINT [PK_GproPatientMedication] PRIMARY KEY CLUSTERED  ([GproPatientMedicationId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GproPatientMedication] ADD CONSTRAINT [FK_GProPatientMedication_GProPatientMeasureId] FOREIGN KEY ([GproPatientMeasureId]) REFERENCES [dbo].[GProPatientMeasure] ([GProPatientMeasureId])
GO
ALTER TABLE [dbo].[GproPatientMedication] ADD CONSTRAINT [FK_GproPatientMedication_GProPatientRankingId] FOREIGN KEY ([GproPatientRankingId]) REFERENCES [dbo].[GProPatientRanking] ([GProPatientRankingId])
GO
