CREATE TABLE [dbo].[MedicationMedicationCodeSystemType]
(
[MedicationMedicationCodeSystemTypeId] [int] NOT NULL IDENTITY(1, 1),
[MedicationID] [int] NOT NULL,
[MedicationCode] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MedicationCodeSystemTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MedicationMedicationCodeSystemType] ADD CONSTRAINT [PK_MedicationMedicationCodeSystemType] PRIMARY KEY CLUSTERED  ([MedicationMedicationCodeSystemTypeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
