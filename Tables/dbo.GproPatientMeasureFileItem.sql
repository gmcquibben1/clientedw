CREATE TABLE [dbo].[GproPatientMeasureFileItem]
(
[GproPatientMeasureFileItemId] [int] NOT NULL IDENTITY(1, 1),
[GproPatientMeasureId] [int] NOT NULL,
[GproMeasureTypeId] [int] NOT NULL,
[FileItemId] [int] NOT NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GproPatientMeasureFileItem] ADD CONSTRAINT [PK_GproPatientMeasureFileItem] PRIMARY KEY CLUSTERED  ([GproPatientMeasureFileItemId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
