CREATE TABLE [dbo].[patientProviderOrgHierarchy]
(
[PatientID] [int] NULL,
[ProviderID] [int] NULL,
[ProviderNPI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[medicareNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HealthcareOrgID] [int] NULL,
[BusinessUnitId] [int] NULL,
[SourceSystemID] [int] NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [CIX_patientProviderOrgHierarchy_PatientID] ON [dbo].[patientProviderOrgHierarchy] ([PatientID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
