CREATE TABLE [dbo].[InterfacePatientProcedureProcedureCodeArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientProcedureProcedureCodeId] [int] NOT NULL,
[InterfacePatientProcedureId] [int] NOT NULL,
[ProcedureCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcedureCodingSystemName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcedureDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
