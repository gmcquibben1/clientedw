CREATE TABLE [dbo].[PatientLabResult]
(
[PatientLabRestultId] [int] NOT NULL IDENTITY(1, 1),
[PatientLabOrderId] [int] NOT NULL,
[ObservationCode1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ObservationDescription1] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ObservationCodingSystemName1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ObservationCode2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationDescription2] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationCodingSystemName2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Units] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReferenceRange] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AbnormalFlag] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultComment] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationDate] [datetime] NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientLabResult] ADD CONSTRAINT [PK_LabResult] PRIMARY KEY CLUSTERED  ([PatientLabRestultId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientLabResult_Combined] ON [dbo].[PatientLabResult] ([PatientLabOrderId]) INCLUDE ([ObservationCode1], [ObservationDate]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex_LabLookup1] ON [dbo].[PatientLabResult] ([PatientLabOrderId], [ObservationCode1]) INCLUDE ([AbnormalFlag], [ObservationCode2], [ObservationDate], [Units], [Value]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientLabResult_Rollup] ON [dbo].[PatientLabResult] ([PatientLabOrderId], [ObservationCode1], [ObservationDescription1], [ObservationCodingSystemName1], [ObservationCode2], [ObservationDescription2], [ObservationCodingSystemName2], [ObservationDate]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientLabResult] ADD CONSTRAINT [FK_PatientLabResult_PatientLabOrder] FOREIGN KEY ([PatientLabOrderId]) REFERENCES [dbo].[PatientLabOrder] ([PatientLabOrderId]) ON DELETE CASCADE
GO
