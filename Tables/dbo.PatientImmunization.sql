CREATE TABLE [dbo].[PatientImmunization]
(
[PatientImmunizationId] [int] NOT NULL IDENTITY(1, 1),
[PatientID] [int] NOT NULL,
[SourceSystemId] [int] NOT NULL,
[ServiceDate] [datetime] NULL,
[ImmunizationCodeId] [int] NOT NULL,
[ImmunizationDose] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationUnits] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaterialLotNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaterialManufacturer] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationRouteTypeID] [int] NULL,
[PerformedByClinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterIdentifier] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RenderingProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientImmunization_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientImmunization_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientImmunization_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientImmunization] ADD CONSTRAINT [PK_PatientImmunization] PRIMARY KEY CLUSTERED  ([PatientImmunizationId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientImmunization_PatientIdDeleteIndServiceDate] ON [dbo].[PatientImmunization] ([PatientID], [DeleteInd], [ServiceDate]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientImmunization] ADD CONSTRAINT [FK_PatientImmunization_ImmunizationType] FOREIGN KEY ([ImmunizationCodeId]) REFERENCES [dbo].[ImmunizationCode] ([ImmunizationCodeId])
GO
ALTER TABLE [dbo].[PatientImmunization] ADD CONSTRAINT [FK_PatientImmunization_MedicationRouteType] FOREIGN KEY ([MedicationRouteTypeID]) REFERENCES [dbo].[MedicationRouteType] ([MedicationRouteTypeID])
GO
