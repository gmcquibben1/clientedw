CREATE TABLE [dbo].[PatientIdsConflictsMergeDisambiguationRules]
(
[GropuDisambiguationRuleId] [int] NOT NULL IDENTITY(1, 1),
[GroupFormationRuleId] [int] NULL,
[GroupDisambiguationRule] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IntraGroupPriorityOrder] [int] NULL,
[ActiveSet] [bit] NULL CONSTRAINT [DF__PatientId__Activ__59C55456] DEFAULT ((0)),
[CreateDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Creat__5AB9788F] DEFAULT (getdate()),
[ModifyDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Modif__5BAD9CC8] DEFAULT (getdate())
) ON [PRIMARY]
GO
