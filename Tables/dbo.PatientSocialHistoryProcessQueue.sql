CREATE TABLE [dbo].[PatientSocialHistoryProcessQueue]
(
[PatientSocialHistoryId] [int] NOT NULL IDENTITY(1, 1),
[PatientID] [int] NOT NULL,
[SourceSystemId] [int] NOT NULL,
[SmokingStatus] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaritalStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmploymentStatus] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransportationInd] [bit] NULL,
[LivingArrangementCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlcoholUseInd] [bit] NULL,
[DrugUseInd] [bit] NULL,
[CaffeineUseInd] [bit] NULL,
[SexuallyActiveInd] [bit] NULL,
[ExerciseHoursPerWeek] [int] NULL,
[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
