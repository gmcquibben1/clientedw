CREATE TABLE [dbo].[JhacgNonMatchedDrgCode]
(
[patient_id] [int] NULL,
[drg_cd] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[drg_version] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
