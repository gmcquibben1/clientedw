CREATE TABLE [dbo].[JHACG_PatientEdcCode]
(
[LbpatientId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EDC] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EDCDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JHACG_EdcCodeId] [int] NOT NULL,
[JHACG_MedcCodeId] [tinyint] NULL,
[MEDC] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MEDCDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
