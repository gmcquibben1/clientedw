CREATE TABLE [dbo].[NPINotMatch]
(
[srv_prvdr_id] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[srv_print_nm] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NPI] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[_Similarity] [real] NULL,
[_Confidence] [real] NULL,
[_Similarity_NPI] [real] NULL
) ON [PRIMARY]
GO
