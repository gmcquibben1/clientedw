CREATE TABLE [dbo].[MemberRosterTable14History]
(
[HICNO] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sex1] [int] NULL,
[Birth Date] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateofDeath] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO Participant TIN Number] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Individual NPI] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [date] NULL,
[FileTimeline] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileInputName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[loadDateTime] [datetime] NULL
) ON [PRIMARY]
GO
