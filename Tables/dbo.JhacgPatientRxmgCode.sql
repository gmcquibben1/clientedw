CREATE TABLE [dbo].[JhacgPatientRxmgCode]
(
[LbpatientId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RxmgCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MajorRxmgCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JhacgPatientRxmgCode] ADD CONSTRAINT [PK__JhacgPat__F0726163CB22EEED] PRIMARY KEY CLUSTERED  ([LbpatientId], [RxmgCode]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
