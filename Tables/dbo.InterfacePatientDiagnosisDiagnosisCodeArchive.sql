CREATE TABLE [dbo].[InterfacePatientDiagnosisDiagnosisCodeArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientDiagnosisDiagnosisCodeId] [int] NOT NULL,
[InterfacePatientDiagnosisId] [int] NOT NULL,
[DiagnosisCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiagnosisCodingSystemName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiagnosisDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
