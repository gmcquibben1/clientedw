CREATE TABLE [dbo].[MemberExtract]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[publicationDate] [datetime] NULL,
[memberCK] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrACOID] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrIPAID] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurreffectiveDate] [date] NULL,
[CurrTermDate] [date] NULL,
[lastName] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[firstName] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[midInit] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sex] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[maritalStatus] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[birthDate] [date] NULL,
[langCode] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[medicareNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[homeAddrLine1] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[homeAddrLine2] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[homeCity] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[homeState] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[homeZip] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[homePhone] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[homeEMail] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mailAddrLine1] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mailAddrLine2] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mailCity] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mailState] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mailZip] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDeceased] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimShare] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DrugAlcShare] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderId] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[prvEffDate] [date] NULL,
[prvTermDate] [date] NULL,
[fromFile] [date] NULL,
[family_id] [uniqueidentifier] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Holdback] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RaceDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EthnicDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeathDate] [date] NULL,
[LbPatientId] [int] NULL,
[SourceSystemId] [int] NULL,
[PatientOtherIdentifier] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDatetime] [datetime] NULL CONSTRAINT [DF_MemberExtract_CreateDatetime] DEFAULT (getdate()),
[ModifyDatetime] [datetime] NULL CONSTRAINT [DF_MemberExtract_ModifyDatetime] DEFAULT (getdate())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_MemberExtract_LBPatientId] ON [dbo].[MemberExtract] ([LbPatientId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
