CREATE TABLE [dbo].[PatientDiagnosis]
(
[PatientDiagnosisId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[SourceSystemID] [int] NULL,
[DiagnosisTypeID] [int] NULL,
[DiagnosisCodeId] [int] NULL,
[DiagnosisCodingSystemTypeId] [int] NULL,
[DiagnosisStatusTypeID] [int] NULL,
[DiagnosisSeverityTypeID] [int] NULL,
[DiagnosisConfidentialityInd] [bit] NULL,
[DiagnosisPriorityTypeId] [int] NULL,
[DiagnosisClassificationTypeId] [int] NULL,
[DiagnosisDateTime] [datetime] NULL,
[DiagnosisOnsetDate] [date] NULL,
[DiagnosisResolvedDate] [date] NULL,
[DiagnosisComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterID] [bigint] NULL,
[EncounterIdentifier] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderID] [int] NULL,
[Clinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveInd] [bit] NULL CONSTRAINT [DF_PatientDiagnosis_ActiveInd] DEFAULT ((1)),
[ExternalReferenceIdentifier] [int] NULL,
[RenderingProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DrgCode] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChronicInd] [bit] NULL CONSTRAINT [DF_PatientDiagnosis_ChronicInd] DEFAULT ((0)),
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientDiagnosis_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientDiagnosis_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientDiagnosis_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL,
[OtherReferenceIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientDiagnosis] ADD CONSTRAINT [PK_PatientDiagnosis] PRIMARY KEY CLUSTERED  ([PatientDiagnosisId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-PatientId] ON [dbo].[PatientDiagnosis] ([PatientId]) INCLUDE ([DiagnosisDateTime]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientDiagnosis_PatientIdActiveIndDeleteInd] ON [dbo].[PatientDiagnosis] ([PatientId], [ActiveInd], [DeleteInd]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NCIX_PatientDiagnosis_PDDC] ON [dbo].[PatientDiagnosis] ([PatientId], [SourceSystemID], [DiagnosisCodeId]) INCLUDE ([DiagnosisDateTime], [DiagnosisOnsetDate]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientDiagnosis] ADD CONSTRAINT [FK_PatientDiagnosis_DiagnosisClassificationTypeId] FOREIGN KEY ([DiagnosisClassificationTypeId]) REFERENCES [dbo].[DiagnosisClassificationType] ([DiagnosisClassificationTypeID])
GO
ALTER TABLE [dbo].[PatientDiagnosis] ADD CONSTRAINT [FK_PatientDiagnosis_DiagnosisPriorityTypeId] FOREIGN KEY ([DiagnosisPriorityTypeId]) REFERENCES [dbo].[DiagnosisPriorityType] ([DiagnosisPriorityTypeID])
GO
ALTER TABLE [dbo].[PatientDiagnosis] ADD CONSTRAINT [FK_PatientDiagnosis_DiagnosisSeverityTypeId] FOREIGN KEY ([DiagnosisSeverityTypeID]) REFERENCES [dbo].[DiagnosisSeverityType] ([DiagnosisSeverityTypeID])
GO
ALTER TABLE [dbo].[PatientDiagnosis] ADD CONSTRAINT [FK_PatientDiagnosis_DiagnosisStatusTypeId] FOREIGN KEY ([DiagnosisStatusTypeID]) REFERENCES [dbo].[DiagnosisStatusType] ([DiagnosisStatusTypeID])
GO
ALTER TABLE [dbo].[PatientDiagnosis] ADD CONSTRAINT [FK_PatientDiagnosis_DiagnosisTypeId] FOREIGN KEY ([DiagnosisTypeID]) REFERENCES [dbo].[DiagnosisType] ([DiagnosisTypeID])
GO
