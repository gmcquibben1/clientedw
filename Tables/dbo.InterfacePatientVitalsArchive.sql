CREATE TABLE [dbo].[InterfacePatientVitalsArchive]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientVitalsId] [int] NOT NULL,
[InterfacePatientID] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[ServiceDateTime] [datetime] NOT NULL,
[HeightCM] [decimal] (5, 2) NULL,
[HeightComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WeightKG] [decimal] (5, 2) NULL,
[WeightComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemperatureCelcius] [decimal] (5, 2) NULL,
[TemperatureComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pulse] [smallint] NULL,
[PulseComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Respiration] [smallint] NULL,
[RespirationComments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BloodPressureSystolic] [smallint] NULL,
[BloodPressureDiastolic] [smallint] NULL,
[BloodPressureComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OxygenSaturationSP02] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OxygenSaturationComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Clinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientVitalsComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
