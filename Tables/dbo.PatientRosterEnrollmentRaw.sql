CREATE TABLE [dbo].[PatientRosterEnrollmentRaw]
(
[PatientRosterEnrollmentRawID] [int] NOT NULL IDENTITY(1, 1),
[SourceSystemID] [int] NULL,
[PatientExternalID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Gender] [smallint] NULL,
[Birthdate] [datetime] NOT NULL,
[Address] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[TermDate] [datetime] NULL,
[DeathDate] [datetime] NULL,
[ProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF__PatientRo__Creat__41B8C09B] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientRosterEnrollmentRaw] ADD CONSTRAINT [PK__PatientR__5E87F5273CA9A851] PRIMARY KEY CLUSTERED  ([PatientRosterEnrollmentRawID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
