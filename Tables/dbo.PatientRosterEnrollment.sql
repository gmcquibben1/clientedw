CREATE TABLE [dbo].[PatientRosterEnrollment]
(
[PatientRosterEnrollmentID] [int] NOT NULL IDENTITY(1, 1),
[SourceSystemID] [int] NULL,
[PatientExternalID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Gender] [smallint] NULL,
[Birthdate] [datetime] NOT NULL,
[StartDate] [datetime] NULL,
[TermDate] [datetime] NULL,
[DeathDate] [datetime] NULL,
[ProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF__PatientRo__Creat__3EDC53F0] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientRosterEnrollment] ADD CONSTRAINT [PK__PatientR__87B50C5B41C3E354] PRIMARY KEY CLUSTERED  ([PatientRosterEnrollmentID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
