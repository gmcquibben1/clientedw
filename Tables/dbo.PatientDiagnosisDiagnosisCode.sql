CREATE TABLE [dbo].[PatientDiagnosisDiagnosisCode]
(
[PatientDiagnosisDiagnosisCodeId] [int] NOT NULL IDENTITY(1, 1),
[PatientDiagnosisId] [int] NOT NULL,
[DiagnosisCodingSystemTypeId] [int] NOT NULL,
[DiagnosisCodeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientDiagnosisDiagnosisCode] ADD CONSTRAINT [PK_PatientDiagnosisDiagnosisCode] PRIMARY KEY CLUSTERED  ([PatientDiagnosisDiagnosisCodeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NCIX_PDDC_DiagnosisCodeId] ON [dbo].[PatientDiagnosisDiagnosisCode] ([DiagnosisCodeId], [PatientDiagnosisId]) WITH (FILLFACTOR=80, ALLOW_PAGE_LOCKS=OFF) ON [FG_INDEX1]
GO
CREATE NONCLUSTERED INDEX [PDDC2] ON [dbo].[PatientDiagnosisDiagnosisCode] ([PatientDiagnosisId]) INCLUDE ([DiagnosisCodeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientDiagnosisDiagnosisCode] ADD CONSTRAINT [FK_PatientDiagnosisDiagnosisCode_DiagnosisCodingSystemTypeId] FOREIGN KEY ([DiagnosisCodingSystemTypeId]) REFERENCES [dbo].[DiagnosisCodingSystemType] ([DiagnosisCodingSystemTypeID])
GO
ALTER TABLE [dbo].[PatientDiagnosisDiagnosisCode] ADD CONSTRAINT [FK_PatientDiagnosisDiagnosisCode_PatientDiagnosisId] FOREIGN KEY ([PatientDiagnosisId]) REFERENCES [dbo].[PatientDiagnosis] ([PatientDiagnosisId])
GO
