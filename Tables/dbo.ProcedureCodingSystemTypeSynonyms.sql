CREATE TABLE [dbo].[ProcedureCodingSystemTypeSynonyms]
(
[ProcedureCodingSystemTypeSynonymsId] [int] NOT NULL IDENTITY(1, 1),
[ProcedureCodingSystemTypeID] [int] NOT NULL,
[SynonymName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProcedureCodingSystemTypeSynonyms] ADD CONSTRAINT [PK_ProcedureCodingSystemTypeSynonyms] PRIMARY KEY CLUSTERED  ([ProcedureCodingSystemTypeSynonymsId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
