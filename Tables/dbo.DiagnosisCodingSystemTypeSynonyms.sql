CREATE TABLE [dbo].[DiagnosisCodingSystemTypeSynonyms]
(
[DiagnosisCodingSystemTypeSynonymsId] [int] NOT NULL IDENTITY(1, 1),
[DiagnosisCodingSystemTypeID] [int] NOT NULL,
[SynonymName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisCodingSystemTypeSynonyms] ADD CONSTRAINT [PK_DiagnosisCodingSystemTypeSynonyms] PRIMARY KEY CLUSTERED  ([DiagnosisCodingSystemTypeSynonymsId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
