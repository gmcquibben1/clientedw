CREATE TABLE [dbo].[MemberRosterArchives_ProviderTab]
(
[HICNO] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sex1] [int] NULL,
[Birth Date] [datetime] NULL,
[Deceased Beneficiary Flag2] [int] NULL,
[ACO Participant TIN Number] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Individual NPI] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[ACO] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileTimeline] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileInputName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL
) ON [PRIMARY]
GO
