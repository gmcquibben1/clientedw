CREATE TABLE [dbo].[DedupeTables]
(
[id] [bigint] NULL,
[DbLocation] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetTable] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdatingColumn] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryKeyName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
