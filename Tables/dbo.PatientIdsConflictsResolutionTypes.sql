CREATE TABLE [dbo].[PatientIdsConflictsResolutionTypes]
(
[ResolutionTypeId] [int] NULL,
[ResolutionType] [varchar] (900) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseCaseScenarious] [varchar] (900) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
