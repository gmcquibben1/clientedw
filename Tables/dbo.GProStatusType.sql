CREATE TABLE [dbo].[GProStatusType]
(
[GProStatusTypeId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_GProStatusType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_GProStatusType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GProStatusType] ADD CONSTRAINT [PK_GProStatusType] PRIMARY KEY CLUSTERED  ([GProStatusTypeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
