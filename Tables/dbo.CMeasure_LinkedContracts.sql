CREATE TABLE [dbo].[CMeasure_LinkedContracts]
(
[LinkedContractId] [int] NOT NULL IDENTITY(1, 1),
[MeasureContractId] [int] NULL,
[ContractId] [int] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-LinkedContracts] ON [dbo].[CMeasure_LinkedContracts] ([MeasureContractId], [ContractId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
