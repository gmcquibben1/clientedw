CREATE TABLE [dbo].[EdmLibraryParam]
(
[EdmLibraryParamId] [int] NOT NULL IDENTITY(1, 1),
[EdmLibraryId] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Display] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Param] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NULL,
[CreateLbUserId] [int] NULL,
[ModifyLbUserId] [int] NULL,
[CreateDateTime] [datetime] NULL,
[ModifyDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EdmLibraryParam] ADD CONSTRAINT [PK__EdmLibra__D0335D16621552FD] PRIMARY KEY CLUSTERED  ([EdmLibraryParamId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
