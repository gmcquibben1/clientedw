CREATE TABLE [dbo].[MM_Metrics_Pivot_ACO]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[ACO_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOS_First] [date] NULL,
[Enrollment] [int] NULL,
[MemberMonths] [int] NULL,
[IP Admits] [int] NULL,
[SNF Admits] [int] NULL,
[Acute IP Admits] [int] NULL,
[ER Visits] [int] NULL,
[IP 30 Day ReAdmits] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MM_Metrics_Pivot_ACO] ADD CONSTRAINT [PK__MM_Metri__3213E83FC489A3EB] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
