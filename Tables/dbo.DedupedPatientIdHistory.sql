CREATE TABLE [dbo].[DedupedPatientIdHistory]
(
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BirthDate] [date] NULL,
[Gender] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientId] [int] NOT NULL,
[newPatientId] [int] NULL,
[createTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
