CREATE TABLE [dbo].[CustomValueSet]
(
[ValueSetName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValueSetOID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModuleType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModuleIndicator] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodeSystem] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Year] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LightBeamInd] [bit] NOT NULL CONSTRAINT [DF__CustomVal__Light__15A53433] DEFAULT ((0)),
[MeasureSteward] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF__CustomVal__Delet__1699586C] DEFAULT ((0)),
[EnabledInd] [bit] NOT NULL CONSTRAINT [DF__CustomVal__Enabl__178D7CA5] DEFAULT ((1)),
[Comment] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CustomValueSet] ON [dbo].[CustomValueSet] ([ValueSetName], [CodeSystem], [Code], [ModuleType], [ModuleIndicator], [Year]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
