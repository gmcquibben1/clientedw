CREATE TABLE [dbo].[ClinicalDataType]
(
[ClinicalDataTypeID] [int] NOT NULL IDENTITY(1, 1),
[NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ClinicalDataType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_ClinicalDataType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClinicalDataType] ADD CONSTRAINT [PK_ClinicalDataType] PRIMARY KEY CLUSTERED  ([ClinicalDataTypeID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ClinicalDataType_Name] ON [dbo].[ClinicalDataType] ([NAME], [DeleteInd]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
