CREATE TABLE [dbo].[ATI_PatFactor]
(
[ATI_PatFactors_ID] [int] NOT NULL IDENTITY(1, 1),
[LBPatientID] [int] NULL,
[MemberID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorCount] [int] NULL,
[ATIWeightSum] [decimal] (23, 5) NULL,
[VARWeightSum] [decimal] (23, 5) NULL,
[ATIWeightFactor] [decimal] (23, 5) NULL,
[ATIScore] [decimal] (10, 2) NULL,
[WriteDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATI_PatFactor] ADD CONSTRAINT [PK__ATI_PatF__5C8D901A5C91CBDA] PRIMARY KEY CLUSTERED  ([ATI_PatFactors_ID]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
