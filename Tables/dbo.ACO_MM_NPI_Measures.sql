CREATE TABLE [dbo].[ACO_MM_NPI_Measures]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[ACO_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO_Name] [nvarchar] (71) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIN_Number] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIN_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Provider_NPI] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Provider_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOS_First] [date] NULL,
[MM] [int] NULL,
[A] [decimal] (38, 0) NULL,
[PRO] [decimal] (38, 0) NULL,
[DME] [decimal] (38, 0) NULL,
[Total] [decimal] (38, 0) NULL,
[PPPM] [decimal] (38, 0) NULL,
[PPPM_BiC] [decimal] (38, 0) NULL,
[PPPM_ACO_Avg] [decimal] (38, 0) NULL,
[PPPM_TIN_Avg] [decimal] (38, 0) NULL,
[PPPM_Target] [decimal] (18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ACO_MM_NPI_Measures] ADD CONSTRAINT [PK__ACO_MM_N__3213E83F6F065939] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
