CREATE TABLE [dbo].[PatientIdsConflictsXReference]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[rooted_id] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Group_Id] [uniqueidentifier] NULL,
[Family_Id] [uniqueidentifier] NULL,
[Head_Id] [uniqueidentifier] NULL,
[GroupFormationRuleId] [int] NULL,
[GroupDisambiguationRuleId] [int] NULL,
[ManualOverride] [int] NULL,
[Comments] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Creat__690797E6] DEFAULT (getdate()),
[ModifyDateTime] [datetime] NULL,
[RaisedById] [int] NULL,
[ResolvedById] [int] NULL,
[FamilyIdsPack] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
