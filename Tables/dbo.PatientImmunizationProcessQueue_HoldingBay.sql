CREATE TABLE [dbo].[PatientImmunizationProcessQueue_HoldingBay]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientImmunizationId] [int] NULL,
[InterfacePatientID] [int] NULL,
[LbPatientId] [int] NULL,
[InterfaceSystemId] [int] NULL,
[ServiceDate] [datetime] NULL,
[ImmunizationCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationCodeId] [int] NULL,
[ImmunizationCodeSystemName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationDose] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationUnits] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaterialLotNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaterialManufacturer] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteCodeSystemName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationRouteTypeID] [int] NULL,
[PerformingClinician] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL,
[InterfacePatientImmunizationId_HoldingOverride] [int] NULL,
[InterfacePatientID_HoldingOverride] [int] NULL,
[LbPatientId_HoldingOverride] [int] NULL,
[InterfaceSystemId_HoldingOverride] [int] NULL,
[ServiceDate_HoldingOverride] [datetime] NULL,
[ImmunizationCode_HoldingOverride] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationCodeId_HoldingOverride] [int] NULL,
[ImmunizationCodeSystemName_HoldingOverride] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationDescription_HoldingOverride] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationDose_HoldingOverride] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImmunizationUnits_HoldingOverride] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaterialLotNumber_HoldingOverride] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaterialManufacturer_HoldingOverride] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteCode_HoldingOverride] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteCodeSystemName_HoldingOverride] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteDescription_HoldingOverride] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicationRouteTypeID_HoldingOverride] [int] NULL,
[PerformingClinician_HoldingOverride] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime_HoldingOverride] [datetime] NULL,
[HoldingStartDate] [datetime] NULL,
[HoldingStartReason] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoldingEndDate] [datetime] NULL,
[HoldingEndReason] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoldingReleaseInd] [int] NULL
) ON [PRIMARY]
GO
