CREATE TABLE [dbo].[JhacgPatientAdgCode]
(
[LbpatientId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AdgCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JhacgPatientAdgCode] ADD CONSTRAINT [PK__JhacgPat__E2743A229968D6FB] PRIMARY KEY CLUSTERED  ([LbpatientId], [AdgCode]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
