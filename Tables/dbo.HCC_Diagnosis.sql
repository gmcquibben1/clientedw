CREATE TABLE [dbo].[HCC_Diagnosis]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[DIAGNOSIS CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHORT DESCRIPTION] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2013HCC] [int] NULL,
[CMS-HCC PACE/ESRD Model Category] [int] NULL,
[2014HCC] [int] NULL,
[RxHCC Model Category] [int] NULL,
[2013 CMS-HCC Model for 2014 Payment Year] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CMS-HCC PACE/ESRD Model for 2014 Payment Year] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2014 CMS-HCC Model for 2014 Payment Year] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RxHCC Model for 2014 Payment Year] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HCC_Diagnosis] ADD CONSTRAINT [PK__HCC_Diag__3214EC27B3EE95E3] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
