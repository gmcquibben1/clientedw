CREATE TABLE [dbo].[CCLF_9_BeneXref_raw]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[CRNT_HIC_NUM] [nchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRVS_HIC_NUM] [nchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRVS_HICN_EFCTV_DT] [date] NULL,
[PRVS_HICN_OBSLT_DT] [date] NULL,
[BENE_RRB_NUM] [nchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fileDate] [date] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCLF_9_BeneXref_raw] ADD CONSTRAINT [PK_CCLF_9_BeneXref_raw] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
