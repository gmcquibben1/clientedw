CREATE TABLE [dbo].[EdmLibrary]
(
[EdmLibraryId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Display] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EvalCode] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NULL,
[CreateLbUserId] [int] NULL,
[ModifyLbUserId] [int] NULL,
[CreateDateTime] [datetime] NULL,
[ModifyDateTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[EdmLibrary] ADD CONSTRAINT [PK__EdmLibra__FEDD7E9B9CF57964] PRIMARY KEY CLUSTERED  ([EdmLibraryId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
