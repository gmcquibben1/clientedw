CREATE TABLE [dbo].[PatientEncounterProcessQueue]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientEncounterId] [int] NOT NULL,
[InterfacePatientId] [int] NOT NULL,
[InterfaceSystemId] [int] NULL,
[SourceEncounterId] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterDate] [date] NOT NULL,
[EncounterTime] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterTypeID] [int] NULL,
[EncounterType] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Clinician] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReasonForVisit_ICD9] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReasonForVisit_ICD10] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReasonForVisit_LOINC] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReasonForVisit_SNOMED] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[ProcessedInd] [int] NULL,
[ScrubbedRowNum] [int] NULL,
[ReasonForVisit] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
