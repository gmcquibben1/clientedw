CREATE TABLE [dbo].[PatientIdsConflictsDemergeMandate]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[Head_Id] [uniqueidentifier] NULL,
[rooted_id] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KeepOut_Family_Id] [uniqueidentifier] NULL,
[DemergeComments] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Creat__57DD0BE4] DEFAULT (getdate()),
[ModifyDateTime] [datetime] NULL,
[DeleteInd] [int] NULL,
[DemergedById] [int] NULL
) ON [PRIMARY]
GO
