CREATE TABLE [dbo].[MemMonth_Metrics]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[MembMonth_id] [int] NULL,
[LbPatientId] [int] NULL,
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOS_First] [date] NULL,
[Title] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MemMonth_Metrics] ADD CONSTRAINT [PK__MemMonth__3213E83F83FD01EB] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
