CREATE TABLE [dbo].[JhacgNonMatchedRevenueCode]
(
[patient_id] [int] NULL,
[revenue_code] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[revenue_code_type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
