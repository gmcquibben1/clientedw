CREATE TABLE [dbo].[PatientSummary]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NULL,
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUM_DETAIL] [decimal] (19, 3) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientSummary] ADD CONSTRAINT [PK__PatientS__3213E83FF6CCB203] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
