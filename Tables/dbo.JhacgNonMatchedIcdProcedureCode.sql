CREATE TABLE [dbo].[JhacgNonMatchedIcdProcedureCode]
(
[patient_id] [int] NULL,
[icd_proc_version] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[icd_proc_cd] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
