CREATE TABLE [dbo].[CMeasure_Detail_Archive]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MeasureID] [int] NOT NULL,
[PatientMemberID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[Denominator] [int] NOT NULL,
[Exclusion] [int] NOT NULL,
[Numerator] [int] NOT NULL,
[MeasureOverrideStatus] [int] NULL CONSTRAINT [DF__CMeasure___Measu__73BA3083] DEFAULT ((0)),
[LbTaskId] [int] NULL,
[TaskStatus] [tinyint] NULL,
[TaskOverrideStatus] [int] NULL CONSTRAINT [DF__CMeasure___TaskO__74AE54BC] DEFAULT ((0)),
[GPRO] [int] NULL,
[CreateTimestamp] [datetime] NULL CONSTRAINT [DF__CMeasure___Creat__75A278F5] DEFAULT (getdate()),
[ModifyTimestamp] [datetime] NULL CONSTRAINT [DF__CMeasure___Modif__76969D2E] DEFAULT (getdate()),
[ArchiveTimestamp] [datetime] NULL CONSTRAINT [DF__CMeasure___Archi__778AC167] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMeasure_Detail_Archive] ADD CONSTRAINT [PK__CMeasure__3214EC270C18E780] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
