CREATE TABLE [dbo].[PayerFeedsCCLFMapper]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[PayerFeed] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EvalType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetTable] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetColumn] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceColumn] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EvalExpr] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FailedFileDrop] [varchar] (350) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostProcessingCallout] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SystemImportDataTypeId] [int] NULL,
[SourceSystemId] [int] NULL,
[UnFinishedProcessFlag] [bit] NULL CONSTRAINT [DF__PayerFeed__UnFin__02C769E9] DEFAULT ((0))
) ON [PRIMARY]
GO
