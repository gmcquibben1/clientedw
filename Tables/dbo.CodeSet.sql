CREATE TABLE [dbo].[CodeSet]
(
[CodeSetId] [int] NOT NULL IDENTITY(1, 1),
[CodeSetTypeId] [int] NOT NULL,
[CodeValue] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_CodeSet_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_CodeSet_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_CodeSet_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CodeSet] ADD CONSTRAINT [PK_CodeSet] PRIMARY KEY CLUSTERED  ([CodeSetId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
