CREATE TABLE [dbo].[orgHierarchyStructure]
(
[buId] [int] NULL,
[levelId] [int] NULL,
[level1_buId] [int] NULL,
[level1_buCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level1_buName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level1_buType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level2_buId] [int] NULL,
[level2_buCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level2_buName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level2_buType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level3_buId] [int] NULL,
[level3_buCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level3_buName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level3_buType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level4_buId] [int] NULL,
[level4_buCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level4_buName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level4_buType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level5_buId] [int] NULL,
[level5_buCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level5_buName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level5_buType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level6_buId] [int] NULL,
[level6_buCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level6_buName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level6_buType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modified_datetime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[orgHierarchyStructure] ADD CONSTRAINT [UQ__orgHiera__548880A2D7F8847A] UNIQUE NONCLUSTERED  ([buId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
