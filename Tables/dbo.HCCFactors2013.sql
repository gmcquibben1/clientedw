CREATE TABLE [dbo].[HCCFactors2013]
(
[Variable] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HCCNo] [int] NULL,
[Disease Group] [nvarchar] (93) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Community Factors] [decimal] (28, 10) NULL,
[Avg_BidRate] [decimal] (28, 12) NULL,
[Normalized_Score] [decimal] (28, 15) NULL,
[Coding_Intensity] [decimal] (28, 15) NULL,
[New_Rate] [decimal] (28, 13) NULL,
[<NullColumn 9>] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Factors] [nvarchar] (83) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[<NullColumn 11>] [decimal] (28, 12) NULL
) ON [PRIMARY]
GO
