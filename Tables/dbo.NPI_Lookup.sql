CREATE TABLE [dbo].[NPI_Lookup]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[NPI] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO] [int] NULL CONSTRAINT [DF_NPIList_ACO] DEFAULT ((0)),
[adhoc_provider] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Taxonomy] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Specialty] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeoCodeLong] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeoCodeLat] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NPI_Lookup] ADD CONSTRAINT [PK_NPIList] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
