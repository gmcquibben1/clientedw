CREATE TABLE [dbo].[JhacgLocalWeight]
(
[acg_code] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[acg_desc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[patient_count] [int] NULL,
[total_cost] [float] NULL,
[local_acg_weight] [float] NULL,
[patientGroup] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
