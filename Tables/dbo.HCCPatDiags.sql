CREATE TABLE [dbo].[HCCPatDiags]
(
[HCCPatDiags_ID] [int] NOT NULL IDENTITY(1, 1),
[ICD9] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HCCNo] [int] NULL,
[DiseaseGroup] [varchar] (129) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Factor] [decimal] (28, 10) NULL,
[DateOfService] [date] NULL,
[PatientID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[RenderingProviderNPI] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HCCPatDiags] ADD CONSTRAINT [PK__HCCPatDi__BC9BF9820143C39F] PRIMARY KEY CLUSTERED  ([HCCPatDiags_ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
