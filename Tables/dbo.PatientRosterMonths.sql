CREATE TABLE [dbo].[PatientRosterMonths]
(
[PatientRosterMonthsID] [int] NOT NULL IDENTITY(1, 1),
[SourceSystemID] [int] NULL,
[PatientExternalID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [smallint] NULL,
[Birthdate] [datetime] NULL,
[MemberMonth] [datetime] NULL,
[MemberMonthStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeathDate] [datetime] NULL,
[ProviderNPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[Month] [datetime] NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF__PatientRo__Creat__6501FCD8] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientRosterMonths] ADD CONSTRAINT [PK__PatientR__D12635A47494D5C2] PRIMARY KEY CLUSTERED  ([PatientRosterMonthsID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
