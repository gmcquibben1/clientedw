CREATE TABLE [dbo].[CMeasure_ContractNames]
(
[ContractId] [int] NOT NULL IDENTITY(1, 1),
[ContractName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [ClusteredIndex-ContractId] ON [dbo].[CMeasure_ContractNames] ([ContractId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
