CREATE TABLE [dbo].[MeasureTarget]
(
[MeasureTargetId] [int] NOT NULL IDENTITY(1, 1),
[MeasureTargetTypeId] [int] NOT NULL,
[StartDate] [date] NULL,
[EndDate] [date] NULL,
[LevelId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [decimal] (18, 3) NOT NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL,
[DeleteInd] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MeasureTarget] ADD CONSTRAINT [PK_MeasureTarget] PRIMARY KEY CLUSTERED  ([MeasureTargetId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
