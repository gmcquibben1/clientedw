CREATE TABLE [dbo].[AllergyStatusType]
(
[AllergyStatusTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_AllergyStatusType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_AllergyStatusType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AllergyStatusType] ADD CONSTRAINT [PK_AllergyStatusType] PRIMARY KEY CLUSTERED  ([AllergyStatusTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_AllergyStatusType_Name] ON [dbo].[AllergyStatusType] ([Name]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
