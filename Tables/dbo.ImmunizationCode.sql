CREATE TABLE [dbo].[ImmunizationCode]
(
[ImmunizationCodeId] [int] NOT NULL IDENTITY(1, 1),
[ImmunizationCodingSystemTypeId] [int] NOT NULL,
[ImmunizationCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImmunizationDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImmunizationDescriptionAlternate] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_ImmunizationCode_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ImmunizationCode_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_ImmunizationCode_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ImmunizationCode] ADD CONSTRAINT [PK_ImmunizationCode] PRIMARY KEY CLUSTERED  ([ImmunizationCodeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ImmunizationCode] ADD CONSTRAINT [FK_ImmunizationCode_ImmunizationCodingSystemTypeId] FOREIGN KEY ([ImmunizationCodingSystemTypeId]) REFERENCES [dbo].[ImmunizationCodingSystemType] ([ImmunizationCodingSystemTypeID])
GO
