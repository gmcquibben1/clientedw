CREATE TABLE [dbo].[PatientAllergyProcessQueue]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientAllergyID] [int] NULL,
[InterfacePatientID] [int] NOT NULL,
[InterfaceSystemId] [int] NOT NULL,
[AllergyType] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllergyTypeId] [int] NULL,
[AllergyDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AllergySeverity] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllergySeverityTypeId] [int] NULL,
[AllergyReaction] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllergyReactionTypeId] [int] NULL,
[AllergyStatusTypeId] [int] NULL,
[AllergyComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OnsetDate] [date] NULL,
[CreateDateTime] [datetime] NOT NULL,
[EncounterId] [bigint] NULL,
[LbPatientId] [int] NULL,
[ProcessedInd] [int] NULL,
[ScrubbedRowNum] [int] NULL
) ON [PRIMARY]
GO
