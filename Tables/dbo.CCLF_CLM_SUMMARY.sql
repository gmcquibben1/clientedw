CREATE TABLE [dbo].[CCLF_CLM_SUMMARY]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NULL,
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAIM_FEED] [nchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAIM_TYPE] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DIAG_CD] [nchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YEAR] [nchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MONTH] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAID] [decimal] (10, 2) NULL,
[DOS_First] [date] NULL,
[MemberMonths_id] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCLF_CLM_SUMMARY] ADD CONSTRAINT [PK__CCLF_CLM__3213E83FF560AF6F] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
