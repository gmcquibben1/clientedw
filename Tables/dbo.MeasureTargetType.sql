CREATE TABLE [dbo].[MeasureTargetType]
(
[MeasureTargetTypeId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL,
[DeleteInd] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MeasureTargetType] ADD CONSTRAINT [PK_MeasureTargetType] PRIMARY KEY CLUSTERED  ([MeasureTargetTypeId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
