CREATE TABLE [dbo].[PatientPregnancyHistory]
(
[PatientPregnancyHistoryId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[DateRecorded] [date] NOT NULL,
[PregnancyNumber] [smallint] NOT NULL,
[GestationalAge] [smallint] NULL,
[EstimatedDateOfDelivery] [date] NULL,
[FinalDeliveryDate] [date] NULL,
[DeliveryType] [int] NULL,
[BirthWeight] [int] NULL,
[EpisiotomyInd] [bit] NULL,
[EpisiotomyTypeCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EpisiotomyPerinealDegreeCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EpisiotomyLacerationCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientPregnancyHistory_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientPregnancyHistory_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientPregnancyHistory_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientPregnancyHistory] ADD CONSTRAINT [PK_PatientPregnancyHistory] PRIMARY KEY CLUSTERED  ([PatientPregnancyHistoryId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
