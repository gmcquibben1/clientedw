CREATE TABLE [dbo].[DedupePatientIdAudit]
(
[DedupePatientIdAuditId] [int] NOT NULL IDENTITY(1, 1),
[DedupeTableName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DedupeTablePKType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DedupeTablePKValue] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OldPatientId] [int] NOT NULL,
[NewPatientId] [int] NOT NULL,
[Description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DedupePatientIdAudit] ADD CONSTRAINT [PK_DedupePatientIdAuditId] PRIMARY KEY CLUSTERED  ([DedupePatientIdAuditId]) WITH (FILLFACTOR=96, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
