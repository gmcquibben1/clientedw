CREATE TABLE [dbo].[MedicationCodeSystemType]
(
[MedicationCodeSystemTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MedicationCodeSystemType] ADD CONSTRAINT [PK_MedicationCodeSystemTypeID] PRIMARY KEY CLUSTERED  ([MedicationCodeSystemTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
