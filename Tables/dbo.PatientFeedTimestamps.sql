CREATE TABLE [dbo].[PatientFeedTimestamps]
(
[PatientId] [int] NULL,
[FeedType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeedSubType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeedTimestamp] [datetime] NULL
) ON [PRIMARY]
GO
