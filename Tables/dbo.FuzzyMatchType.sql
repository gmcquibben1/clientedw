CREATE TABLE [dbo].[FuzzyMatchType]
(
[FuzzyMatchTypeId] [int] NOT NULL IDENTITY(1, 1),
[SPName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MatchField] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatchMethod] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatchMethodDesc] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatchThreshHold] [float] NULL,
[AutoMatchThreshold] [float] NULL,
[EnableInd] [smallint] NULL,
[CreateTime] [datetime] NULL CONSTRAINT [DF__FuzzyMatc__Creat__10E07F16] DEFAULT (getutcdate()),
[UpdateTime] [datetime] NULL CONSTRAINT [DF__FuzzyMatc__Updat__11D4A34F] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
