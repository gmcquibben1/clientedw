CREATE TABLE [dbo].[PatientIdsConflictsMergeFormationRules]
(
[GroupFormationRuleId] [int] NOT NULL IDENTITY(1, 1),
[MergeDirection] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractNameFiltration] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupFormationRule] [varchar] (800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupFormationRuleDisplay] [varchar] (800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CombinationMatchRequirement] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientIdMatchRequirement] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractNameMatchRequirement] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientMatchCombinationFields] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InterGroupPriorityOrder] [int] NULL,
[ActiveSet] [bit] NULL CONSTRAINT [DF__PatientId__Activ__5D95E53A] DEFAULT ((0)),
[CreateDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Creat__5E8A0973] DEFAULT (getdate()),
[ModifyDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Modif__5F7E2DAC] DEFAULT (getdate())
) ON [PRIMARY]
GO
