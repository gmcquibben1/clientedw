CREATE TABLE [dbo].[JhacgStatisticsSummary]
(
[description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[value] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[patientGroup] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
