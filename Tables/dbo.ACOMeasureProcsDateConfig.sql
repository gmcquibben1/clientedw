CREATE TABLE [dbo].[ACOMeasureProcsDateConfig]
(
[DateMarker] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateValue] [datetime] NULL,
[Comments] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
