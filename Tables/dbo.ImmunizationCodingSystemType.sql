CREATE TABLE [dbo].[ImmunizationCodingSystemType]
(
[ImmunizationCodingSystemTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_ImmunizationCodingSystemType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ImmunizationCodingSystemType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_ImmunizationCodingSystemType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ImmunizationCodingSystemType] ADD CONSTRAINT [PK_ImmunizationCodingSystemType] PRIMARY KEY CLUSTERED  ([ImmunizationCodingSystemTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
