CREATE TABLE [dbo].[JHACG_Metrics]
(
[patient_id] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[pregnant] [int] NULL,
[delivered] [int] NULL,
[pharmacy_cost] [decimal] (18, 0) NULL,
[total_cost] [decimal] (18, 0) NULL,
[inpatient_hospitalization_count] [int] NULL,
[emergency_visit_count] [int] NULL,
[outpatient_visit_count] [int] NULL,
[dialysis_service] [int] NULL,
[nursing_service] [int] NULL,
[major_procedure] [int] NULL,
[cancer_treatment] [int] NULL,
[care_management_program] [int] NULL
) ON [PRIMARY]
GO
