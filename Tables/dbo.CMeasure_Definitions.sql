CREATE TABLE [dbo].[CMeasure_Definitions]
(
[MeasureID] [int] NOT NULL IDENTITY(1, 1),
[MeasureProgram] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureAbbr] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureCategory] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureSubcategory] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighLow] [int] NULL,
[StoredProcedure] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnableFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CMeasure_Definitions_EnableFlag] DEFAULT ('Y'),
[MeasureContractID] [int] NULL,
[TaskingNeeded] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_CMeasure_Definitions_TaskingNeeded] DEFAULT ('Y'),
[EnforceEligibilty] [int] NOT NULL CONSTRAINT [DF_CMeasure_Definitions_EnforceEligibilty] DEFAULT ((0)),
[UseClinicalDataOnlyInd] [bit] NOT NULL CONSTRAINT [DF__CMeasure___UseCl__693CA210] DEFAULT ((0)),
[UseLBSupplementCodesInd] [bit] NOT NULL CONSTRAINT [DF__CMeasure___UseLB__6A30C649] DEFAULT ((1)),
[ValueSetMeasureAbbr] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureYear] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMeasure_Definitions] ADD CONSTRAINT [PK__CMeasure__8C56D7605E2B8CCE] PRIMARY KEY CLUSTERED  ([MeasureID]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMeasure_Definitions] ADD CONSTRAINT [uc_measurenames] UNIQUE NONCLUSTERED  ([MeasureProgram], [MeasureName]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
