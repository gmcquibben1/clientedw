CREATE TABLE [dbo].[DiagnosisCodingSystemType]
(
[DiagnosisCodingSystemTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_DiagnosisCodingSystemType_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisCodingSystemType_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisCodingSystemType_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisCodingSystemType] ADD CONSTRAINT [PK_DiagnosisCodingSystemTypeID] PRIMARY KEY CLUSTERED  ([DiagnosisCodingSystemTypeID]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_DiagnosisCodingSystemType_Name] ON [dbo].[DiagnosisCodingSystemType] ([Name]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
