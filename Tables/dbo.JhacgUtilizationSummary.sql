CREATE TABLE [dbo].[JhacgUtilizationSummary]
(
[utilization_marker] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[utilization_count] [int] NULL,
[utilization_rate] [float] NULL,
[utilization_reference_rate] [float] NULL,
[patientGroup] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
