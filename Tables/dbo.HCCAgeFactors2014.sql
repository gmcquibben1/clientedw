CREATE TABLE [dbo].[HCCAgeFactors2014]
(
[AgeGroup] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Female] [decimal] (28, 10) NULL,
[Male] [decimal] (28, 10) NULL
) ON [PRIMARY]
GO
