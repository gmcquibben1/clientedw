CREATE TABLE [dbo].[CCLF_8_BeneDemo_raw]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[BENE_HIC_NUM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_FIPS_STATE_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_FIPS_CNTY_CD] [nchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_ZIP_CD] [nchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_DOB] [date] NULL,
[BENE_SEX_CD] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_RACE_CD] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_AGE] [int] NULL,
[BENE_MDCR_STUS_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_DUAL_STUS_CD] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_DEATH_DT] [date] NULL,
[BENE_RNG_BGN_DT] [date] NULL,
[BENE_RNG_END_DT] [date] NULL,
[BENE_1ST_NAME] [nchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_MIDL_NAME] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_LAST_NAME] [nchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_ORGNL_ENTLMT_RSN_CD] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENE_ENTLMT_BUYIN_IND] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fileDate] [date] NULL,
[SourceFeed] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCLF_8_BeneDemo_raw] ADD CONSTRAINT [PK_CCLF_8_BeneDemo_raw] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
