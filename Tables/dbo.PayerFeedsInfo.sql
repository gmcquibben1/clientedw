CREATE TABLE [dbo].[PayerFeedsInfo]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[PayerName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnprocessedFilePickup] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessedFileDrop] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FormatFilePickup] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileNamePattern] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimsTable] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDateExp] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HeaderPresent] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FailedFileDrop] [varchar] (350) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostProcessingCallout] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SystemImportDataTypeId] [int] NULL,
[SourceSystemId] [int] NULL,
[UnFinishedProcessFlag] [bit] NULL CONSTRAINT [DF__PayerFeed__UnFin__04AFB25B] DEFAULT ((0)),
[SourceFeed] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileType] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnprocessedFilePickupTest] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessedFileDropTest] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FailedFileDropTest] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
