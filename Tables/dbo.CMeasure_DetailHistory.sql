CREATE TABLE [dbo].[CMeasure_DetailHistory]
(
[CMeasure_DetailHistoryID] [int] NOT NULL IDENTITY(1, 1),
[MeasurePatientID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureID] [int] NOT NULL,
[PatientMemberID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LbPatientID] [int] NOT NULL,
[Denominator] [int] NOT NULL,
[Exclusion] [int] NOT NULL,
[Numerator] [int] NOT NULL,
[MeasureOverrideStatus] [int] NULL,
[LbTaskID] [int] NULL,
[TaskStatus] [int] NULL,
[TaskOverrideStatus] [int] NULL,
[GPRO] [int] NULL,
[CreateTimestamp] [datetime] NULL,
[ModifyTimestamp] [datetime] NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [CMeasure_DetailHistoryIndex] ON [dbo].[CMeasure_DetailHistory] ([LbPatientID], [MeasureID], [CreateTimestamp]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
