CREATE TABLE [dbo].[PatientEncounter]
(
[PatientEncounterId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[SourceSystemID] [int] NULL,
[SourceEncounterId] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterDate] [date] NOT NULL,
[EncounterTime] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EncounterTypeID] [int] NULL,
[Clinician] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReasonForVisit_ICD9] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReasonForVisit_ICD10] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReasonForVisit_LOINC] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReasonForVisit_SNOMED] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF__PatientEn__Creat__3EA749C6] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF__PatientEn__Modif__3F9B6DFF] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_PatientEncounter_PatientIDSourceSystemDate] ON [dbo].[PatientEncounter] ([PatientId], [SourceSystemID], [EncounterTypeID], [EncounterDate]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
