CREATE TABLE [dbo].[PatientIdsConflictsResolveDependencies]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[DbLocation] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DependencyTable] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableView] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TablePK] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TablePKDataType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdatingColumn] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdatingColumnDataType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResolvingForId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResolutionTypeId] [int] NULL,
[ResolutionType] AS (CONVERT([varchar](100),case [ResolutionTypeId] when (1) then 'Merge Case :- Update the column of the source row to merged id if not the same as merged Id; Demerge Case :- Update the column of the source row to demerged Id if the same as demerged Id' when (2) then 'Merge Case :- Delete the source row if not credited to merged Id; Demerge Case :- Not applicable' when (3) then 'Merge Case :- Retire the source row if not credited to merged Id; Demerge Case :- Activate the source row if credited to demerged Id' end,(0))),
[ImplementationType] AS (CONVERT([varchar](100),case [ResolutionTypeId] when (1) then 'Auto managed by Framework' when (2) then 'To be explicitly coded by developer' when (3) then 'Auto managed by Framework' end,(0))),
[ImplementationNotes] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Creat__625A9A57] DEFAULT (getdate()),
[ModifyDateTime] [datetime] NULL
) ON [PRIMARY]
GO
