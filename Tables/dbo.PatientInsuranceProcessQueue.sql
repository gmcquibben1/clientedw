CREATE TABLE [dbo].[PatientInsuranceProcessQueue]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[PatientInsuranceId] [int] NOT NULL,
[InterfacePatientId] [int] NOT NULL,
[LbPatientId] [int] NOT NULL,
[SourceSystemId] [int] NOT NULL,
[InsuranceName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FinancialClass] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsurancePriority] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EffectiveDate] [datetime] NULL,
[ExpirationDate] [datetime] NULL,
[PolicyNumber] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL,
[ScrubbedRowNum] [int] NULL,
[ProcessedInd] [int] NULL
) ON [PRIMARY]
GO
