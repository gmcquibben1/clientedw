CREATE TABLE [dbo].[RulePartDefinition]
(
[RulePartDefinitionId] [int] NOT NULL IDENTITY(1, 1),
[RulePartTypeId] [int] NOT NULL,
[StoredProcedureName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL,
[ModifyDateTime] [datetime] NOT NULL,
[CreateLbUserId] [int] NOT NULL,
[ModifyLbUserId] [int] NOT NULL,
[DeleteInd] [tinyint] NOT NULL,
[SystemInd] [tinyint] NOT NULL
) ON [PRIMARY]
GO
