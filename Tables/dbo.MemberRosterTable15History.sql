CREATE TABLE [dbo].[MemberRosterTable15History]
(
[HICNO] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sex] [int] NULL,
[Birthdate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateofDeath] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PluralityPrimaryCare] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartAPartB] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupHealthPlan] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NotResideInUS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludedSharedSavingsInitiative] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoPhysicianVisitACO] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [date] NULL,
[FileTimeLine] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileInputName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LbPatientId] [int] NULL,
[loadDateTime] [datetime] NULL
) ON [PRIMARY]
GO
