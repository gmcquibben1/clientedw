CREATE TABLE [dbo].[SourceSystemSetting]
(
[SourceSystemSettingId] [int] NOT NULL IDENTITY(1, 1),
[SourceSystemId] [int] NOT NULL,
[SettingName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SettingValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF__SourceSys__Delet__1A9EF37A] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF__SourceSys__Creat__1B9317B3] DEFAULT (getdate()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF__SourceSys__Modif__1C873BEC] DEFAULT (getdate()),
[CreateLBUserId] [int] NOT NULL CONSTRAINT [DF__SourceSys__Creat__1D7B6025] DEFAULT ((1)),
[ModifyLBUserId] [int] NOT NULL CONSTRAINT [DF__SourceSys__Modif__1E6F845E] DEFAULT ((1))
) ON [PRIMARY]
GO
