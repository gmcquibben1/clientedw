CREATE TABLE [dbo].[PatientIdReference]
(
[PatientIdReferenceId] [int] NOT NULL IDENTITY(1, 1),
[LbPatientId] [int] NULL,
[ExternalId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdTypeDesc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceSystemId] [int] NULL,
[PrimaryId] [int] NULL,
[DeleteInd] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientIdReference] ADD CONSTRAINT [PK_PatientIdReference] PRIMARY KEY CLUSTERED  ([PatientIdReferenceId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PID] ON [dbo].[PatientIdReference] ([ExternalId], [IdTypeDesc]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140626-154403] ON [dbo].[PatientIdReference] ([ExternalId], [IdTypeDesc]) INCLUDE ([LbPatientId]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientIdReference_IdTypeDesc_DeleteInd_LbPatientId_Include_ExternalId] ON [dbo].[PatientIdReference] ([IdTypeDesc], [DeleteInd], [LbPatientId]) INCLUDE ([ExternalId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140626-154338] ON [dbo].[PatientIdReference] ([LbPatientId], [IdTypeDesc]) INCLUDE ([ExternalId]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
