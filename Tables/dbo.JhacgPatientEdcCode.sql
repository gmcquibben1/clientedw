CREATE TABLE [dbo].[JhacgPatientEdcCode]
(
[LbpatientId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EdcCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MedcCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JhacgPatientEdcCode] ADD CONSTRAINT [PK__JhacgPat__DD0F3AA6AA4C3AAA] PRIMARY KEY CLUSTERED  ([LbpatientId], [EdcCode]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
