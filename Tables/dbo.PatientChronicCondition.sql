CREATE TABLE [dbo].[PatientChronicCondition]
(
[PatientChronicConditionId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[DiagnosisCodeId] [int] NOT NULL,
[ProviderId] [int] NULL,
[DisplayOrder] [int] NULL,
[DeleteInd] [bit] NOT NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientChronicCondition_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientChronicCondition_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientChronicCondition] ADD CONSTRAINT [PK_PatientChronicCondition] PRIMARY KEY CLUSTERED  ([PatientChronicConditionId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientChronicCondition_PatientIdDeleteInd] ON [dbo].[PatientChronicCondition] ([PatientId], [DeleteInd]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NCIX_PatientID_DisplayOrder] ON [dbo].[PatientChronicCondition] ([PatientId], [DisplayOrder]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
