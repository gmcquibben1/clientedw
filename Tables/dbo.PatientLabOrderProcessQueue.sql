CREATE TABLE [dbo].[PatientLabOrderProcessQueue]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[InterfacePatientLabOrderId] [int] NOT NULL,
[LbPatientId] [int] NULL,
[InterfacePatientId] [int] NULL,
[InterfaceSystemId] [int] NOT NULL,
[OrderNumber] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceDescription] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceCodingSystemName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderingProviderId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderingProviderIdType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderingProviderName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL,
[InterfacePatientLabResultId] [int] NULL,
[ObservationCode1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationDescription1] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationCodingSystemName1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationCode2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationDescription2] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationCodingSystemName2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Units] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReferenceRange] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AbnormalFlag] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultComment] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObservationDate] [datetime] NULL,
[LR_CreateDateTime] [datetime] NULL,
[PatientLabOrderId] [int] NULL,
[ScrubbedRowNum] [int] NULL,
[ProcessedInd] [int] NULL,
[EncounterIdentifier] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientLaborderProcess_Combined] ON [dbo].[PatientLabOrderProcessQueue] ([InterfacePatientId]) INCLUDE ([InterfaceSystemId], [OrderNumber]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientLabOrderProcess_InterfacePatientLabResultId] ON [dbo].[PatientLabOrderProcessQueue] ([InterfacePatientLabResultId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PatientLabOrderProcessQueue_PatIdOrderNumSource] ON [dbo].[PatientLabOrderProcessQueue] ([LbPatientId], [OrderNumber], [InterfaceSystemId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
