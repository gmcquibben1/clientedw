CREATE TABLE [dbo].[ATI_FactorResponse]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Factor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorValue] [bigint] NULL,
[ResponseMean] [decimal] (18, 3) NULL,
[ResponseStDev] [decimal] (18, 3) NULL,
[SampleSize] [int] NULL,
[OverallMean] [decimal] (18, 3) NULL,
[OverallStDev] [decimal] (18, 3) NULL,
[Sb] [decimal] (18, 3) NULL,
[fb] [int] NULL,
[MSb] [decimal] (18, 3) NULL,
[Sw] [decimal] (18, 3) NULL,
[MSw] [decimal] (18, 3) NULL,
[fw] [int] NULL,
[FGroup] [decimal] (18, 3) NULL,
[FResponse] [decimal] (18, 3) NULL,
[FWeight] [decimal] (18, 3) NULL,
[VARWeight] [decimal] (18, 3) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATI_FactorResponse] ADD CONSTRAINT [PK__ATI_Fact__3214EC2720621A36] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100, ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
