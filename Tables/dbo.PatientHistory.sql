CREATE TABLE [dbo].[PatientHistory]
(
[PatientHistoryId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[DateRecorded] [date] NOT NULL,
[HistoryTypeCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NPI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientHistory_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientHistory_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientHistory_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientHistory] ADD CONSTRAINT [PK_PatientHistory] PRIMARY KEY CLUSTERED  ([PatientHistoryId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
