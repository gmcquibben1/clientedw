CREATE TABLE [dbo].[Measure_Definitions]
(
[ACO_MeasureID] [int] NOT NULL,
[ACO_CategoryID] [int] NULL,
[Measure_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Measure_Abbr] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Target] [decimal] (28, 10) NULL,
[Measure_Program] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subcategory] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
