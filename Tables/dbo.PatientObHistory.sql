CREATE TABLE [dbo].[PatientObHistory]
(
[PatientObHistoryId] [int] NOT NULL IDENTITY(1, 1),
[PatientId] [int] NOT NULL,
[DateRecorded] [date] NOT NULL,
[Gravidity] [smallint] NOT NULL,
[Parity1] [smallint] NULL,
[Parity2] [smallint] NULL,
[Parity3] [smallint] NULL,
[Parity4] [smallint] NULL,
[LastMenstrualPeriod] [date] NULL,
[DeleteInd] [bit] NOT NULL CONSTRAINT [DF_PatientObHistory_DeleteInd] DEFAULT ((0)),
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientObHistory_CreateDateTime] DEFAULT (sysutcdatetime()),
[ModifyDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientObHistorym_ModifyDateTime] DEFAULT (sysutcdatetime()),
[CreateLBUserId] [int] NOT NULL,
[ModifyLBUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientObHistory] ADD CONSTRAINT [PK_PatientObHistory] PRIMARY KEY CLUSTERED  ([PatientObHistoryId]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
