CREATE TABLE [dbo].[PatientIdsConflictsSwitchSets]
(
[LocalId] [int] NOT NULL IDENTITY(1, 1),
[IdType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdDataType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SwitchoutId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SwitchInId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SwitchDirectionId] [bit] NULL CONSTRAINT [DF__PatientId__Switc__6442E2C9] DEFAULT ((0)),
[SwitchDirection] AS (CONVERT([varchar](100),case [SwitchDirectionId] when (0) then 'Forward' when (1) then 'Backward' end,(0))),
[ActiveSet] [bit] NULL CONSTRAINT [DF__PatientId__Activ__65370702] DEFAULT ((0)),
[CreateDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Creat__662B2B3B] DEFAULT (getdate()),
[ModifyDateTime] [datetime] NULL CONSTRAINT [DF__PatientId__Modif__671F4F74] DEFAULT (getdate())
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SwitchSets2] ON [dbo].[PatientIdsConflictsSwitchSets] ([SwitchInId], [SwitchoutId], [IdType]) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SwitchSets1] ON [dbo].[PatientIdsConflictsSwitchSets] ([SwitchoutId], [IdType], [ActiveSet]) WHERE ([activeset]=(0)) WITH (FILLFACTOR=96) ON [PRIMARY]
GO
