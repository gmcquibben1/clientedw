SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_Patient] AS
SELECT * FROM [Version22LbportalTemplate].dbo.Patient 
WHERE db_name() = 'Version22EdwTemplate' 
GO
