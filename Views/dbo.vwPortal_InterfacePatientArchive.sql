SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientArchive] AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientArchive 
WHERE db_name() = 'Version22EdwTemplate' 
GO
