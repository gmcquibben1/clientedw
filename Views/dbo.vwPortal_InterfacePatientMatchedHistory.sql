SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientMatchedHistory] AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientMatchedHistory
WHERE db_name() = 'Version22EdwTemplate' 
GO
