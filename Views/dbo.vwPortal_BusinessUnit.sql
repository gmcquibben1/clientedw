SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_BusinessUnit] AS
SELECT * FROM [Version22LbportalTemplate].dbo.BusinessUnit
WHERE db_name() = 'Version22EdwTemplate' 
GO
