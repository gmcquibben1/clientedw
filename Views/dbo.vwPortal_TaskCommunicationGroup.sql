SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_TaskCommunicationGroup] AS
SELECT * FROM [Version22LbportalTemplate].dbo.TaskCommunicationGroup
WHERE db_name() = 'Version22EdwTemplate' 
GO
