SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientObHistory] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientObHistory
WHERE db_name() = 'Version22EdwTemplate' 
GO
