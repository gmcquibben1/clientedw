SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwCMeasurePatientDetailHistory_Agg]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT --[History].[HistoryID] AS [HistoryID],
--[History].[MeasurePatientID] AS [MeasurePatientID],
--[History].[MeasureID] AS [MeasureID],
--[History].[PatientMemberID] AS [PatientMemberID],
--[History].[LbPatientID] AS [LbPatientID],
sum([History].[Denominator]) AS [Denominator],
--sum([History].[Exclusion]) AS [Exclusion],
sum([History].[Numerator]) AS [Numerator],
--[History].[MeasureOverrideStatus] AS [MeasureOverrideStatus],
--[History].[LbTaskID] AS [LbTaskID],
--[History].[TaskStatus] AS [TaskStatus],
--[History].[TaskOverrideStatus] AS [TaskOverrideStatus],
--[History].[GPRO] AS [GPRO],
[History].[CreateTimestamp] AS [CreateTimestamp],
--[History].[ModifyTimestamp] AS [ModifyTimestamp],
---[CMeasure_Definitions].[MeasureID] AS [MeasureID (CMeasure_Definitions)],
[CMeasure_Definitions].[MeasureProgram] AS [MeasureProgram],
[CMeasure_Definitions].[MeasureName] AS [MeasureName],
[CMeasure_Definitions].[MeasureAbbr] AS [MeasureAbbr],
[CMeasure_Definitions].[MeasureCategory] AS [MeasureCategory],
[CMeasure_Definitions].[MeasureSubcategory] AS [MeasureSubcategory],
--[CMeasure_Definitions].[HighLow] AS [HighLow],
--[CMeasure_Definitions].[StoredProcedure] AS [StoredProcedure],
--[CMeasure_Definitions].[EnableFlag] AS [EnableFlag],
--[CMeasure_Definitions].[MeasureContractID] AS [MeasureContractID],
--[CMeasure_Definitions].[TaskingNeeded] AS [TaskingNeeded],
--[CMeasure_Definitions].[EnforceEligibilty] AS [EnforceEligibilty],
--[CMeasure_Definitions].[UseClinicalDataOnlyInd] AS [UseClinicalDataOnlyInd],
--[CMeasure_Definitions].[UseLBSupplementCodesInd] AS [UseLBSupplementCodesInd],
--[CMeasure_Definitions].[ValueSetMeasureAbbr] AS [ValueSetMeasureAbbr],
--[CMeasure_Definitions].[MeasureYear] AS [MeasureYear],
--[OrgHierarchy].[ID] AS [ID],
--[OrgHierarchy].[State] AS [State],
--[OrgHierarchy].[City] AS [City],
--[OrgHierarchy].[Zip] AS [Zip],
--[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
--[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
--[OrgHierarchy].[MemberID] AS [MemberID],
--[OrgHierarchy].[MemberFullName] AS [MemberFullName],
--[OrgHierarchy].[Member_Provider_Relationship_Effective_Date] AS [Member_Provider_Relationship_Effective_Date],
--[OrgHierarchy].[Member_Provider_Relationship_Termination_Date] AS [Member_Provider_Relationship_Termination_Date],
--[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI]--,
--[OrgHierarchy].[BirthDate] AS [BirthDate],
--[OrgHierarchy].[GenderId] AS [GenderId],
--[OrgHierarchy].[County] AS [County],
--[OrgHierarchy].[LastUpdatedTimestamp] AS [LastUpdatedTimestamp],
--[OrgHierarchy].[LbPatientId] AS [LbPatientId1],
--[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
--[OrgHierarchy].[PatientId1] AS [PatientId1],
--[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
--[OrgHierarchy].[PatientId2] AS [PatientId2],
--[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
--[OrgHierarchy].[PatientId3] AS [PatientId3],
--[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
--[OrgHierarchy].[PatientId4] AS [PatientId4]
--FROM CMeasure_DetailHistory [History]
--INNER JOIN [dbo].[CMeasure_Definitions] [CMeasure_Definitions] ON ([History].[MeasureID] = [CMeasure_Definitions].[MeasureID])
--INNER JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([History].[LbPatientID] = [OrgHierarchy].[LbPatientId])
FROM CMeasure_DetailHistory History
INNER JOIN [dbo].[CMeasure_Definitions] [CMeasure_Definitions] ON ([History].[MeasureID] = [CMeasure_Definitions].[MeasureID])
INNER JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([History].[LbPatientID] = [OrgHierarchy].[LbPatientId])
WHERE History.Exclusion = 0
GROUP BY --[History].[HistoryID],
--[History].[MeasurePatientID] AS [MeasurePatientID],
--[History].[MeasureID] AS [MeasureID],
--[History].[PatientMemberID] AS [PatientMemberID],
--[History].[LbPatientID] AS [LbPatientID],
--[History].[MeasureOverrideStatus] AS [MeasureOverrideStatus],
--[History].[LbTaskID] AS [LbTaskID],
--[History].[TaskStatus] AS [TaskStatus],
--[History].[TaskOverrideStatus] AS [TaskOverrideStatus],
--[History].[GPRO] AS [GPRO],
[History].[CreateTimestamp],
--[History].[ModifyTimestamp] AS [ModifyTimestamp],
--[CMeasure_Definitions].[MeasureID],
[CMeasure_Definitions].[MeasureProgram],
[CMeasure_Definitions].[MeasureName],
[CMeasure_Definitions].[MeasureAbbr],
[CMeasure_Definitions].[MeasureCategory],
[CMeasure_Definitions].[MeasureSubcategory],
--[CMeasure_Definitions].[HighLow],
--[CMeasure_Definitions].[StoredProcedure],
--[CMeasure_Definitions].[EnableFlag],
--[CMeasure_Definitions].[MeasureContractID],
--[CMeasure_Definitions].[TaskingNeeded] ,
--[CMeasure_Definitions].[EnforceEligibilty],
--[CMeasure_Definitions].[UseClinicalDataOnlyInd],
--[CMeasure_Definitions].[UseLBSupplementCodesInd] ,
--[CMeasure_Definitions].[ValueSetMeasureAbbr] ,
--[CMeasure_Definitions].[MeasureYear] ,
--[OrgHierarchy].[ID] AS [ID],
--[OrgHierarchy].[State] AS [State],
--[OrgHierarchy].[City] AS [City],
--[OrgHierarchy].[Zip] AS [Zip],
--[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
--[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] ,
[OrgHierarchy].[Level1Name] ,
[OrgHierarchy].[Level1TypeName] ,
[OrgHierarchy].[Level2Id] ,
[OrgHierarchy].[Level2Name],
[OrgHierarchy].[Level2TypeName] ,
[OrgHierarchy].[Level3Id] ,
[OrgHierarchy].[Level3Name] ,
[OrgHierarchy].[Level3TypeName],
[OrgHierarchy].[Level4Id] ,
[OrgHierarchy].[Level4Name] ,
[OrgHierarchy].[Level4TypeName] ,
[OrgHierarchy].[Level5Id] ,
[OrgHierarchy].[Level5Name],-- AS [Level5Name],
[OrgHierarchy].[Level5TypeName],-- AS [Level5TypeName],
[OrgHierarchy].[Level6Id],-- AS [Level6Id],
[OrgHierarchy].[Level6Name] ,--AS [Level6Name],
[OrgHierarchy].[Level6TypeName],-- AS [Level6TypeName],
[OrgHierarchy].[ContractName],-- AS [ContractName],
--[OrgHierarchy].[MemberID] AS [MemberID],
--[OrgHierarchy].[MemberFullName] AS [MemberFullName],
--[OrgHierarchy].[Member_Provider_Relationship_Effective_Date] AS [Member_Provider_Relationship_Effective_Date],
--[OrgHierarchy].[Member_Provider_Relationship_Termination_Date] AS [Member_Provider_Relationship_Termination_Date],
--[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[OrgHierarchy].[Patient_Status] ,
[OrgHierarchy].[ProviderId] ,
[OrgHierarchy].[ProviderName],
[OrgHierarchy].[ProviderNPI] 
--[OrgHierarchy].[BirthDate] AS [BirthDate],
--[OrgHierarchy].[GenderId] AS [GenderId],
--[OrgHierarchy].[County] AS [County],
--[OrgHierarchy].[LastUpdatedTimestamp] AS [LastUpdatedTimestamp],
--[OrgHierarchy].[LbPatientId] AS [LbPatientId1],
--[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
--[OrgHierarchy].[PatientId1] AS [PatientId1],
--[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
--[OrgHierarchy].[PatientId2] AS [PatientId2],
--[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
--[OrgHierarchy].[PatientId3] AS [PatientId3],
--[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
--[OrgHierarchy].[PatientId4] AS [PatientId4]


--SELECT  
--count(*)
--FROM (SELECT LbPatientID, CreateTimestamp, MeasureID, SUM(Denominator) AS Denominator, SUM(Numerator) as Numerator
--            FROM CMeasure_DetailHistory
--            WHERE Exclusion = 0
--            GROUP BY LbPatientID, CreateTimestamp, MeasureID) History
--INNER JOIN [dbo].[CMeasure_Definitions] [CMeasure_Definitions] ON ([History].[MeasureID] = [CMeasure_Definitions].[MeasureID])
--INNER JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([History].[LbPatientID] = [OrgHierarchy].[LbPatientId])
GO
