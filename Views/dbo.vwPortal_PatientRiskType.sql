SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_PatientRiskType] AS
SELECT * FROM [Version22LbportalTemplate].dbo.PatientRiskType 
WHERE db_name() = 'Version22EdwTemplate' 
GO
