SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwReference_MemberMonthMetricsConfig] 
AS 
SELECT * FROM EdwReferenceData..MemberMonthMetricsConfig
GO
