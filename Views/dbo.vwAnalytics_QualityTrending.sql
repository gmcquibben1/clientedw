SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==========================================================
-- Author:    Mike
-- Create date:  09/02/2016
-- Description:  View of Analytics QualityTrending Workbook
-- ===========================================================

CREATE VIEW [dbo].[vwAnalytics_QualityTrending]
AS 
SELECT sum([History].[Denominator]) AS [Denominator],
sum([History].[Numerator]) AS [Numerator],
[History].[CreateTimestamp] AS [CreateTimestamp],
[CMeasure_Definitions].[MeasureProgram] AS [MeasureProgram],
[CMeasure_Definitions].[MeasureName] AS [MeasureName],
[CMeasure_Definitions].[MeasureAbbr] AS [MeasureAbbr],
[CMeasure_Definitions].[MeasureCategory] AS [MeasureCategory],
[CMeasure_Definitions].[MeasureSubcategory] AS [MeasureSubcategory],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI]
FROM CMeasure_DetailHistory History
INNER JOIN [dbo].[CMeasure_Definitions] [CMeasure_Definitions] ON ([History].[MeasureID] = [CMeasure_Definitions].[MeasureID])
INNER JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([History].[LbPatientID] = [OrgHierarchy].[LbPatientId])
WHERE History.Exclusion = 0
GROUP BY 
[History].[CreateTimestamp],
[CMeasure_Definitions].[MeasureProgram],
[CMeasure_Definitions].[MeasureName],
[CMeasure_Definitions].[MeasureAbbr],
[CMeasure_Definitions].[MeasureCategory],
[CMeasure_Definitions].[MeasureSubcategory],
[OrgHierarchy].[Level1Id] ,
[OrgHierarchy].[Level1Name] ,
[OrgHierarchy].[Level1TypeName] ,
[OrgHierarchy].[Level2Id] ,
[OrgHierarchy].[Level2Name],
[OrgHierarchy].[Level2TypeName] ,
[OrgHierarchy].[Level3Id] ,
[OrgHierarchy].[Level3Name] ,
[OrgHierarchy].[Level3TypeName],
[OrgHierarchy].[Level4Id] ,
[OrgHierarchy].[Level4Name] ,
[OrgHierarchy].[Level4TypeName] ,
[OrgHierarchy].[Level5Id] ,
[OrgHierarchy].[Level5Name],-- AS [Level5Name],
[OrgHierarchy].[Level5TypeName],-- AS [Level5TypeName],
[OrgHierarchy].[Level6Id],-- AS [Level6Id],
[OrgHierarchy].[Level6Name] ,--AS [Level6Name],
[OrgHierarchy].[Level6TypeName],-- AS [Level6TypeName],
[OrgHierarchy].[ContractName],-- AS [ContractName],
[OrgHierarchy].[Patient_Status] ,
[OrgHierarchy].[ProviderId] ,
[OrgHierarchy].[ProviderName],
[OrgHierarchy].[ProviderNPI] 

GO
