SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_PatientAttribute] AS
SELECT * FROM [Version22LbportalTemplate].dbo.PatientAttribute
WHERE db_name() = 'Version22EdwTemplate' 
GO
