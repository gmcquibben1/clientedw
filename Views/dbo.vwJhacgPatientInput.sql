SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =====================================================================================================================
-- Author:		Lan
-- Create date: 2016-06-01
-- Description: This view provides patient demographic input data for JHACG, which extracts patient data from 
-- Patient_Summary_Pivot and JHACG_PatientListStaging( a subset data of Orghierarchy)
-- Modified By: 
-- Modified Date: 
-- Description:	
-- =====================================================================================================================


CREATE VIEW [dbo].[vwJhacgPatientInput]
AS
SELECT DISTINCT
 CAST(t1.LbPatientId AS Varchar(100)) AS patient_id--patient identifier
,DateDiff(YEAR,t2.BirthDate,GetUTCDate()) AS age
,BirthDate as date_of_birth
,LTRIM(RTRIM(t2.GenderId)) AS sex
,'Risk Profile' as line_of_business 
,'Lightbeam' as company
,'Risk Profile' as product
--,'Patient Risk Plan' as benefit_plan
,ContractName as benefit_plan
,'Medicare' as health_system
,ProviderNPI  AS pcp_id
,ProviderName AS pcp_name
,CAST(t1.[Pharmacy Cost] AS FLOAT) AS pharmacy_cost 
,CAST([12 Mo Cost] AS FLOAT) AS total_cost
--,t1.[IP Admits] AS inpatient_hospitalization_count
--,t1.[ER Visits] AS emergency_visit_count
FROM Patient_Summary_Pivot t1
JOIN JhacgPatientListStaging t2 
ON t1.LbPatientId = t2.LbPatientId 



GO
