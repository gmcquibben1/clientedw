SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwAnalytics_QualityMainView]
AS 
-- ====================================================================================================
-- Author: MH
-- Create date: 12/09/2016
-- Description: View of vwAnalytics_QualityMainView
/*
	MODIFICATIONS
	Version     Date            Author		JIRA		Change
	=======     ==========      ======      =========	=========	
	2.3.1		2017-03-28		LM			LBAN-3803	reference enableFlag in CMeasure_definiton 
	                                                    for enabled measure
*/
-- =====================================================================================================
SELECT 
[CMeasure_Detail].[MeasureID] AS [MeasureID],
[CMeasure_Detail].[PatientMemberID] AS [PatientMemberID],
[CMeasure_Detail].[LbPatientID] AS [LbPatientID],
[CMeasure_Detail].[Denominator] AS [Denominator],
[CMeasure_Detail].[Exclusion] AS [Exclusion],
[CMeasure_Detail].[Numerator] AS [Numerator],
[CMeasure_Definitions].[MeasureProgram] AS [MeasureProgram],
[CMeasure_Definitions].[MeasureName] AS [MeasureName],
[CMeasure_Definitions].[MeasureAbbr] AS [MeasureAbbr],
[CMeasure_Definitions].[MeasureCategory] AS [MeasureCategory],
[CMeasure_Definitions].[MeasureSubcategory] AS [MeasureSubcategory],
[CMeasure_Definitions].[HighLow] AS [HighLow],
[CMeasure_Definitions].[StoredProcedure] AS [StoredProcedure],
[CMeasure_Definitions].[EnableFlag] AS [EnableFlag],
[CMeasure_Definitions].[MeasureContractID] AS [MeasureContractID],
[CMeasure_Definitions].[TaskingNeeded] AS [TaskingNeeded],
[CMeasure_Definitions].[EnforceEligibilty] AS [EnforceEligibilty],
[CMeasure_Definitions].[UseClinicalDataOnlyInd] AS [UseClinicalDataOnlyInd],
[CMeasure_Definitions].[UseLBSupplementCodesInd] AS [UseLBSupplementCodesInd],
[CMeasure_Definitions].[ValueSetMeasureAbbr] AS [ValueSetMeasureAbbr],
[CMeasure_Definitions].[MeasureYear] AS [MeasureYear],
[ACO_Measure_NatAvgTargets].avg AS [avg],
[ACO_Measure_NatAvgTargets].nat30 AS [nat30],
[ACO_Measure_NatAvgTargets].nat90 AS [nat90],
[ACO_Measure_NatAvgTargets].name AS [name_Measure],
[OrgHierarchy].[State] AS [State],
[OrgHierarchy].[City] AS [City],
[OrgHierarchy].[Zip] AS [Zip],
[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
[OrgHierarchy].[MemberID] AS [MemberID],
[OrgHierarchy].[MemberFullName] AS [MemberFullName],
[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
[OrgHierarchy].[BirthDate] AS [BirthDate],
[OrgHierarchy].[GenderId] AS [GenderId],
[OrgHierarchy].[County] AS [County],
[OrgHierarchy].[LbPatientId] AS [LbPatientId (OrgHierarchy)],
[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
[OrgHierarchy].[PatientId1] AS [PatientId1],
[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
[OrgHierarchy].[PatientId2] AS [PatientId2],
[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
[OrgHierarchy].[PatientId3] AS [PatientId3],
[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
[OrgHierarchy].[PatientId4] AS [PatientId4],
x.[TARGET] AS [TARGET],
x.[Name] AS [ContractName (CMeasure_ContractNames)]
FROM [dbo].[CMeasure_Detail] [CMeasure_Detail]
INNER JOIN [dbo].[CMeasure_Definitions] [CMeasure_Definitions] ON ([CMeasure_Detail].[MeasureID] = [CMeasure_Definitions].[MeasureID] AND [CMeasure_Definitions].EnableFlag = 'Y')
INNER JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([CMeasure_Detail].[LbPatientID] = [OrgHierarchy].[LbPatientId])
LEFT JOIN [dbo].[ACO_Measure_NatAvgTargets] [ACO_Measure_NatAvgTargets] ON ([CMeasure_Detail].[MeasureID] = [ACO_Measure_NatAvgTargets].[MeasureID])
left join (SELECT distinct d.[MeasureId]
,d.[TARGET]
,c.[Name]
FROM [dbo].[CMeasure_DefinitionsContractType] d, [dbo].vwPortal_ContractType c 
where d.EnableFlag='Y' and c.ContractTypeId=d.[ContractTypeId]) x
on orghierarchy.contractname = x.name and x.MeasureID = CMeasure_Detail.MeasureID
GO
