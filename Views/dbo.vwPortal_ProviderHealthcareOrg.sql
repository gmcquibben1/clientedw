SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_ProviderHealthcareOrg] AS
SELECT * FROM [Version22LbportalTemplate].dbo.ProviderHealthcareOrg
WHERE db_name() = 'Version22EdwTemplate' 
GO
