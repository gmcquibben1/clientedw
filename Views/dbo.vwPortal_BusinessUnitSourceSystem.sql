SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_BusinessUnitSourceSystem] AS 
SELECT * FROM [Version22LbportalTemplate].[dbo].[BusinessUnitSourceSystem] 
WHERE db_name() = 'Version22EdwTemplate' 
GO
