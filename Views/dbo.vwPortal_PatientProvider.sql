SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_PatientProvider] AS
SELECT * FROM [Version22LbportalTemplate].dbo.PatientProvider 
WHERE db_name() = 'Version22EdwTemplate' 
GO
