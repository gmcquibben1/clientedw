SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwAnalytics_Attr_ContractAttributionPriority]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT 
[ContractAttributionPriority].[ContractPriorityOrder] AS [ContractPriorityOrder],
[AttributionType].[Name] AS [Name],
[AttributionType].[DisplayValue] AS [DisplayValue],
[AttributionType].[StoredProcedureName] AS [StoredProcedureName],
[AttributionType].[EnabledInd] AS [EnabledInd],
[AttributionType].[DeleteInd] AS [DeleteInd],
[AttributionType].[CreateLBUserId] AS [CreateLBUserId (AttributionType)],
[AttributionType].[CreateDateTime] AS [CreateDateTime (AttributionType)],
[AttributionType].[ModifyLBUserId] AS [ModifyLBUserId (AttributionType)],
[AttributionType].[ModifyDateTime] AS [ModifyDateTime (AttributionType)],
[SourceSystem].[SourceSystemId] AS [SourceSystemId (SourceSystem)],
[SourceSystem].[Name] AS [Name (SourceSystem)],
[SourceSystem].[Description] AS [Description],
[ContractType].[Name] AS [Name (ContractType)],
[ContractType].[DisplayValue] AS [DisplayValue (ContractType)]
FROM vwPortal_ContractAttributionPriority [ContractAttributionPriority]
INNER JOIN vwPortal_AttributionType [AttributionType] ON ([ContractAttributionPriority].[AttributiontypeID] = [AttributionType].[AttributionTypeId])
INNER JOIN SourceSystem [SourceSystem] ON ([ContractAttributionPriority].[SourceSystemId] = [SourceSystem].[SourceSystemId])
INNER JOIN vwPortal_ContractType [ContractType] ON ([ContractAttributionPriority].[ContractTypeID] = [ContractType].[ContractTypeID])
GO
