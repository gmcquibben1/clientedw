SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwClaimPartAHeader]
AS 
SELECT
CUR_CLM_UNIQ_ID AS ClaimId,  
IDR.LbPatientId AS PatientId,
CLM_FROM_DT as ClaimFromDate, 
CLM_THRU_DT as ClaimToDate, 
ADMTG_DGNS_CD as DiagnosisCode, 
ICD.ShortDescription as DiagnosisShortDescription, 
ICD.LongDescription as DiagnosisLongDescription, 
DG.Level1 AS DiagGroup1, 
DG.Level2 as DiagGroup2, 
DG.Level3 as DiagGroup3,
CLM_PMT_AMT as PaidAmt, 
DGNS_DRG_CD as DrgCode, 
FAC_PRVDR_NPI_NUM as FacilityNPI, 
FNPI.[NAME] as FacilityName,
ATNDG_PRVDR_NPI_NUM as AttendingNPI, 
ANPI.[NAME] as AttendingProviderName,
ATC.ADMSN_TYPE_DESC as AdmissionTypeCode, 
SRC.ADMSN_SRC_DESC as AdmissionSourceCode 
FROM CCLF_1_PartA_Header A
LEFT JOIN ADMIT_TYPE_CODES ATC ON A.CLM_ADMSN_TYPE_CD = ATC.CLM_ADMSN_TYPE_CD
LEFT JOIN ADMIT_SRC_CODES SRC on A.CLM_ADMSN_SRC_CD = SRC.CLM_ADMSN_SRC_CD
LEFT JOIN ICD9Mstr ICD ON ICD.ICD9 = A.ADMTG_DGNS_CD
LEFT JOIN NPI_Lookup FNPI ON A.FAC_PRVDR_NPI_NUM = FNPI.NPI
LEFT JOIN NPI_Lookup ANPI ON A.ATNDG_PRVDR_NPI_NUM = ANPI.NPI
JOIN DiagnosisGroup DG ON A.ADMTG_DGNS_CD = DG.DiagnosisCode
JOIN PatientIdReference IDR ON A.BENE_HIC_NUM = IDR.ExternalId
GO
