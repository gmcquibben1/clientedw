SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientLabResult] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientLabResult
WHERE db_name() = 'Version22EdwTemplate' 
GO
