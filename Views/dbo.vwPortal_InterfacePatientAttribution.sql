SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientAttribution] AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientAttribution 
WHERE db_name() = 'Version22EdwTemplate' 
GO
