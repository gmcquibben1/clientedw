SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwPortal_InterfacePatientPregnancyHistory] 
					AS
				  SELECT * 
				  FROM Version22LbportalTemplate.dbo.InterfacePatientPregnancyHistory
				  WHERE db_name() = 'Version22EdwTemplate'
GO
