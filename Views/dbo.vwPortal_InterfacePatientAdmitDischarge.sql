SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientAdmitDischarge] AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientAdmitDischarge
WHERE db_name() = 'Version22EdwTemplate' 
GO
