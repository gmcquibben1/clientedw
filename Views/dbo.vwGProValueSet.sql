SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	CREATE VIEW [dbo].[vwGProValueSet]
	AS
	SELECT VariableName ,  CodeSystem ,  Code,ModuleType,ModuleIndicator,year
	FROM [EdwReferenceData].[dbo].[GPROValueSetCode] WITH (NOLOCK)
	UNION
	SELECT ValueSetName, CodeSystem, Code, ModuleType, ModuleIndicator,year 
	FROM [EdwReferenceData].[dbo].CustomValueSet WITH (NOLOCK)
	WHERE EnabledInd = 1 and deleteind = 0 and LightBeamInd = 1
	UNION
	SELECT ValueSetName, CodeSystem, Code, ModuleType, ModuleIndicator,year 
	FROM CustomValueSet WITH (NOLOCK)
	WHERE EnabledInd = 1 and deleteind = 0
GO
