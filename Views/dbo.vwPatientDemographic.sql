SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientDemographic] 
AS 
SELECT p.PatientId, 
	inn.FirstName,
	inn.MiddleName,
	inn.LastName,
	inn.NamePrefix,
	inn.NameSuffix,
	i.BirthDate,
	i.ExternalReferenceIdentifier,
	i.OtherReferenceIdentifier,
	gt.DisplayValue as Gender,
	ir.Race,
	ie.Ethnicity,
	ptn.PrimaryPhoneNumber,
	ptn.PrimaryPhoneExtension,
	ptn.PrimaryPhoneType,
	stn.SecondaryPhoneNumber,
	stn.SecondaryPhoneExtension,
	stn.SecondaryPhoneType,
	pie.PrimaryEmail,
	sie.SecondaryEmail,
	i.DeceasedInd,
	i.DeathDate,
	pa.AddressType,
	pa.AddressLine1,
	pa.AddressLine2,
	pa.City,
	pa.State,
	pa.PostalCode,
	Risk.RiskScore,
	ATI.AtiScore,
	oh.Patient_Status,
	oh.ProviderName,
	oh.ProviderNPI,
	(CASE WHEN cgc.CareGapCount IS NOT NULL THEN cgc.CareGapCount ELSE 0 END) AS CareGapCount,
	ph.ExternalCode AS BusinessUnitExternalCode,
	p.CreateDateTime,
	p.ModifyDateTime
FROM 
	Version22LbportalTemplate.[dbo].[Patient] p
	INNER JOIN Version22LbportalTemplate.[dbo].[Individual] i ON i.IndividualId = p.IndividualId
	INNER JOIN Version22LbportalTemplate.[dbo].[IndividualName] inn ON inn.IndividualId = i.IndividualId
	LEFT OUTER JOIN Version22EdwTemplate.[dbo].[OrgHierarchy] oh ON oh.LbPatientId = p.PatientId
	LEFT OUTER JOIN Version22LbportalTemplate.[dbo].[GenderType] gt ON gt.GenderTypeId = i.GenderTypeId
	LEFT OUTER JOIN (SELECT ie.IndividualId, ie.Email AS PrimaryEmail
					FROM Version22LbportalTemplate.[dbo].[IndividualEmail] ie
						INNER JOIN Version22LbportalTemplate.[dbo].[EmailType] et ON et.EmailTypeId = ie.EmailTypeId
					WHERE et.Name = 'Primary Email') as pie ON pie.IndividualId = i.IndividualId
	LEFT OUTER JOIN (SELECT ie.IndividualId, ie.Email AS SecondaryEmail
					FROM Version22LbportalTemplate.[dbo].[IndividualEmail] ie
						INNER JOIN Version22LbportalTemplate.[dbo].[EmailType] et ON et.EmailTypeId = ie.EmailTypeId
					WHERE et.Name = 'Secondary Email') as sie ON sie.IndividualId = i.IndividualId
	LEFT OUTER JOIN (SELECT isa.IndividualId, sat.DisplayValue as AddressType, sa.AddressLine1, sa.AddressLine2, sa.City, tt.Name AS State, sa.PostalCode
					FROM Version22LbportalTemplate.[dbo].[IndividualStreetAddress] isa 
						INNER JOIN Version22LbportalTemplate.[dbo].[StreetAddress] sa ON sa.StreetAddressId = isa.StreetAddressId
						INNER JOIN Version22LbportalTemplate.[dbo].[StreetAddressType] sat ON sat.StreetAddressTypeId = sa.StreetAddressTypeId
						LEFT OUTER JOIN Version22LbportalTemplate.[dbo].[TerritoryType] tt ON tt.TerritoryTypeId = sa.TerritoryTypeId
					WHERE sa.DeleteInd <> 1 
						AND sat.Name = 'Mailing') AS pa ON pa.IndividualId = i.IndividualId
	LEFT OUTER JOIN (SELECT itn.IndividualId, tn.CountryCode + '-' + tn.AreaCode + '-' + tn.PhoneNumber AS PrimaryPhoneNumber, tn.Extension AS PrimaryPhoneExtension, pt.DisplayValue AS PrimaryPhoneType
						FROM Version22LbportalTemplate.[dbo].[IndividualTelephoneNumber] itn
						INNER JOIN Version22LbportalTemplate.[dbo].[TelephoneNumber] tn ON tn.TelephoneNumberid = itn.TelephoneNumberid
						INNER JOIN Version22LbportalTemplate.[dbo].[PhoneType] pt ON pt.PhoneTypeId = tn.PhoneTypeId
					WHERE itn.DeleteInd <> 1 
						AND pt.Name = 'Home') AS ptn ON ptn.IndividualId = i.IndividualId
	LEFT OUTER JOIN (SELECT itn.IndividualId, tn.CountryCode + '-' + tn.AreaCode + '-' + tn.PhoneNumber AS SecondaryPhoneNumber, tn.Extension AS SecondaryPhoneExtension, pt.DisplayValue AS SecondaryPhoneType
						FROM Version22LbportalTemplate.[dbo].[IndividualTelephoneNumber] itn
						INNER JOIN Version22LbportalTemplate.[dbo].[TelephoneNumber] tn ON tn.TelephoneNumberid = itn.TelephoneNumberid
						INNER JOIN Version22LbportalTemplate.[dbo].[PhoneType] pt ON pt.PhoneTypeId = tn.PhoneTypeId
					WHERE itn.DeleteInd <> 1 
						AND pt.Name = 'Mobile') AS stn ON stn.IndividualId = i.IndividualId
	LEFT OUTER JOIN (SELECT ir.IndividualId, rt.DisplayValue AS Race
					FROM Version22LbportalTemplate.[dbo].[IndividualRace] ir
						INNER JOIN Version22LbportalTemplate.[dbo].[RaceType] rt ON rt.RaceTypeId = ir.RaceTypeId
					WHERE ir.DeleteInd <> 1) AS ir ON ir.IndividualId = i.IndividualId
	LEFT OUTER JOIN (SELECT ie.IndividualId, et.DisplayValue AS Ethnicity
					FROM Version22LbportalTemplate.[dbo].[IndividualEthnicity] ie
						INNER JOIN Version22LbportalTemplate.[dbo].[EthnicityType] et ON et.EthnicityTypeID = ie.EthnicityTypeID
					WHERE ie.DeleteInd <> 1) AS ie ON ie.IndividualId = i.IndividualId
	LEFT OUTER JOIN (SELECT pr.PatientId, pr.RiskValue AS RiskScore 
					FROM Version22LbportalTemplate.[dbo].[PatientRisk] pr 
						INNER JOIN Version22LbportalTemplate.[dbo].[PatientRiskType] prt ON prt.PatientRiskTypeId = pr.PatientRiskTypeId
					WHERE prt.Name = 'LBRisk') AS Risk ON Risk.PatientId = p.PatientId
	LEFT OUTER JOIN (SELECT pr.PatientId, pr.RiskValue AS AtiScore 
					FROM Version22LbportalTemplate.[dbo].[PatientRisk] pr 
						INNER JOIN Version22LbportalTemplate.[dbo].[PatientRiskType] prt ON prt.PatientRiskTypeId = pr.PatientRiskTypeId
					WHERE prt.Name = 'LBATI') AS ATI ON ATI.PatientId = p.PatientId
	LEFT OUTER JOIN (SELECT t.PatientId, COUNT(*) AS CareGapCount
					FROM Version22LbportalTemplate.[dbo].[Task] t 
						INNER JOIN Version22LbportalTemplate.[dbo].[CareGap] cg ON cg.TaskId = t.TaskId
					GROUP BY t.PatientId) AS cgc ON cgc.PatientId = p.PatientId		
	LEFT OUTER JOIN (SELECT pho.PatientId, bu.ExternalCode 
					FROM Version22LbportalTemplate.[dbo].[PatientHealthCareOrg] pho 
						INNER JOIN Version22LbportalTemplate.[dbo].[HealthcareOrg] ho ON ho.HealthcareOrgId = pho.HealthcareOrgId
						INNER JOIN Version22LbportalTemplate.[dbo].[BusinessUnit] bu ON bu.BusinessUnitId = ho.BusinessUnitId
					GROUP BY pho.PatientId, bu.ExternalCode) AS ph ON ph.PatientId = p.PatientId
WHERE p.DeleteInd = 0 
GO
