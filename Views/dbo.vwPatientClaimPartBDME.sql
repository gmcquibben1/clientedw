SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientClaimPartBDME]
AS
SELECT
	IDR.LbPatientId AS PatientId,
	B.CUR_CLM_UNIQ_ID AS ClaimId, 
	B.BENE_HIC_NUM as CardholderId,
	B.CLM_TYPE_CD as ClaimTypeCode, 
	B.CLM_FROM_DT as FromDate, 
	B.CLM_THRU_DT as ThruDate, 
	B.CLM_LINE_HCPCS_CD as CPT, 
	C.[description] as CPTDesc, 
	B.CLM_LINE_CVRD_PD_AMT as PaidAmt, 
	B.PAYTO_PRVDR_NPI_NUM as RenderingNPI, 
	RendProv.[NAME] as RenderingName, 
	B.ORDRG_PRVDR_NPI_NUM as OrderingNPI, 
	OrdProv.[NAME] as OrderingName, 
	B.CLM_ADJSMT_TYPE_CD as AdjustmentCode
FROM CCLF_6_PartB_DME B
JOIN PatientIdReference IDR ON B.BENE_HIC_NUM = IDR.ExternalId
LEFT JOIN CPT4Mstr C ON B.CLM_LINE_HCPCS_CD = C.cpt4_code_id
LEFT JOIN NPI_Lookup OrdProv ON B.ORDRG_PRVDR_NPI_NUM = OrdProv.NPI
LEFT JOIN NPI_Lookup RendProv ON B.PAYTO_PRVDR_NPI_NUM = RendProv.NPI
GO
