SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_Attr_PatientProvider]
----WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [vwPortal_PatientProvider].[PatientProviderID] AS [PatientProviderID],
[vwPortal_PatientProvider].[PatientID] AS [PatientID],
[vwPortal_PatientProvider].[ProviderID] AS [ProviderID],
[vwPortal_PatientProvider].[SourceSystemID] AS [SourceSystemID],
[vwPortal_PatientProvider].[ExternalReferenceIdentifier] AS [ExternalReferenceIdentifier],
[vwPortal_PatientProvider].[OtherReferenceIdentifier] AS [OtherReferenceIdentifier],
[vwPortal_PatientProvider].[DeleteInd] AS [DeleteInd],
[vwPortal_PatientProvider].[CreateDateTime] AS [CreateDateTime],
[vwPortal_PatientProvider].[ModifyDateTime] AS [ModifyDateTime],
[vwPortal_PatientProvider].[CreateLBUserId] AS [CreateLBUserId],
[vwPortal_PatientProvider].[ModifyLBUserId] AS [ModifyLBUserId],
[vwPortal_PatientProvider].[ProviderRoleTypeId] AS [ProviderRoleTypeId],
[vwPortal_PatientProvider].[OverrideInd] AS [OverrideInd],
[vwPortal_PatientProvider].[AttributionTypeId] AS [AttributionTypeId],
[vwPortal_PatientProvider].[AttributedProviderInd] AS [AttributedProviderInd],
[vwPortal_PatientProvider].[ManuallyAddedInd] AS [ManuallyAddedInd],
[SourceSystem].[SourceSystemId] AS [SourceSystemId1],
[SourceSystem].[Name] AS [Name (SourceSystem)],
[SourceSystem].[Description] AS [Description],
[SourceSystem].[DeleteInd] AS [DeleteInd (SourceSystem)],
[SourceSystem].[CreateDateTime] AS [CreateDateTime (SourceSystem)],
[SourceSystem].[ModifyDateTime] AS [ModifyDateTime (SourceSystem)],
[SourceSystem].[CreateLBUserId] AS [CreateLBUserId (SourceSystem)],
[SourceSystem].[ModifyLBUserId] AS [ModifyLBUserId (SourceSystem)],
[SourceSystem].[ExternalReferenceIdentifier] AS [ExternalReferenceIdentifier (SourceSystem)],
[SourceSystem].[OtherReferenceIdentifier] AS [OtherReferenceIdentifier (SourceSystem)],
[SourceSystem].[ClinicalInd] AS [ClinicalInd],
[vwPortal_AttributionType].[AttributionTypeId] AS [AttributionTypeId (vwPortal_AttributionType)],
[vwPortal_AttributionType].[Name] AS [Name],
[vwPortal_AttributionType].[DisplayValue] AS [DisplayValue],
[vwPortal_AttributionType].[StoredProcedureName] AS [StoredProcedureName],
[vwPortal_AttributionType].[EnabledInd] AS [EnabledInd],
[vwPortal_AttributionType].[DeleteInd] AS [DeleteInd (vwPortal_AttributionType)],
[vwPortal_AttributionType].[CreateLBUserId] AS [CreateLBUserId (vwPortal_AttributionType)],
[vwPortal_AttributionType].[CreateDateTime] AS [CreateDateTime (vwPortal_AttributionType)],
[vwPortal_AttributionType].[ModifyLBUserId] AS [ModifyLBUserId (vwPortal_AttributionType)],
[vwPortal_AttributionType].[ModifyDateTime] AS [ModifyDateTime (vwPortal_AttributionType)],
[PatientContract].[PatientContractId] AS [PatientContractId],
[PatientContract].[LbPatientId] AS [LbPatientId],
[PatientContract].[ContractName] AS [ContractName],
[PatientContract].[ContractId] AS [ContractId],
[PatientContract].[RunDateTime] AS [RunDateTime],
[PatientContract].[ContractStatus] AS [ContractStatus],
[PatientContract].[ClaimStatus] AS [ClaimStatus],
[PatientContract].[IsCurrentInd] AS [IsCurrentInd],
[PatientContract].[LbUserId] AS [LbUserId],
[PatientContract].[ContractSource] AS [ContractSource]
FROM [dbo].[vwPortal_PatientProvider] [vwPortal_PatientProvider]
INNER JOIN [dbo].[SourceSystem] [SourceSystem] ON ([vwPortal_PatientProvider].[SourceSystemID] = [SourceSystem].[SourceSystemId])
INNER JOIN [dbo].[vwPortal_AttributionType] [vwPortal_AttributionType] ON ([vwPortal_PatientProvider].[AttributionTypeId] = [vwPortal_AttributionType].[AttributionTypeId])
INNER JOIN [dbo].[PatientContract] [PatientContract] ON ([vwPortal_PatientProvider].[PatientID] = [PatientContract].[LbPatientId])
GO
