SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwPortal_InterfacePatientCustomAttribute] 
					AS
				  SELECT * 
				  FROM Version22LbportalTemplate.dbo.InterfacePatientCustomAttribute
				  WHERE db_name() = 'Version22EdwTemplate'
GO
