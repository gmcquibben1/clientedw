SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==========================================================
-- Author:    Ismail
-- Create date:  10/05/2016
-- Description:  View of vwAnalytics_MEOC_Pregnancy Workbook
-- ===========================================================
CREATE VIEW [dbo].[vwAnalytics_MEOC_Pregnancy]
AS 
SELECT [OrgHierarchy].[ID] AS [ID],
[OrgHierarchy].[State] AS [State],
[OrgHierarchy].[City] AS [City],
[OrgHierarchy].[Zip] AS [Zip],
[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
[OrgHierarchy].[MemberID] AS [MemberID],
[OrgHierarchy].[MemberFullName] AS [MemberFullName],
[OrgHierarchy].[Member_Provider_Relationship_Effective_Date] AS [Member_Provider_Relationship_Effective_Date],
[OrgHierarchy].[Member_Provider_Relationship_Termination_Date] AS [Member_Provider_Relationship_Termination_Date],
[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[OrgHierarchy].[LbPatientId] AS [LbPatientId],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
[OrgHierarchy].[BirthDate] AS [BirthDate],
[OrgHierarchy].[GenderId] AS [GenderId],
[OrgHierarchy].[County] AS [County],
[OrgHierarchy].[LastUpdatedTimestamp] AS [LastUpdatedTimestamp],
[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
[OrgHierarchy].[PatientId1] AS [PatientId1],
[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
[OrgHierarchy].[PatientId2] AS [PatientId2],
[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
[OrgHierarchy].[PatientId3] AS [PatientId3],
[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
[OrgHierarchy].[PatientId4] AS [PatientId4],
[PatientPregnancyStat].[PatientPregnancyStat_ID] AS [PatientPregnancyStat_ID],
[PatientPregnancyStat].[LbPatientId] AS [LbPatientId (PatientPregnancyStat)],
[PatientPregnancyStat].[EpisiotomyInd] AS [EpisiotomyInd],
[PatientPregnancyStat].[DeliveryDate] AS [DeliveryDate],
[PatientPregnancyStat].[PrenatalVisits_FirstTrimester] AS [PrenatalVisits_FirstTrimester],
[PatientPregnancyStat].[PrenatalVisits_SecondTrimester] AS [PrenatalVisits_SecondTrimester],
[PatientPregnancyStat].[PrenatalVisits_ThirdTrimester] AS [PrenatalVisits_ThirdTrimester],
[PatientPregnancyStat].[ERVisits_FirstTrimester] AS [ERVisits_FirstTrimester],
[PatientPregnancyStat].[ERVisits_SecondTrimester] AS [ERVisits_SecondTrimester],
[PatientPregnancyStat].[ERVisits_ThirdTrimester] AS [ERVisits_ThirdTrimester],
[PatientPregnancyStat].[PartA_Cost_FirstTrimester] AS [PartA_Cost_FirstTrimester],
[PatientPregnancyStat].[PartA_Cost_SecondTrimester] AS [PartA_Cost_SecondTrimester],
[PatientPregnancyStat].[PartA_Cost_ThirdTrimester] AS [PartA_Cost_ThirdTrimester],
[PatientPregnancyStat].[PartB_Cost_FirstTrimester] AS [PartB_Cost_FirstTrimester],
[PatientPregnancyStat].[PartB_Cost_SecondTrimester] AS [PartB_Cost_SecondTrimester],
[PatientPregnancyStat].[PartB_Cost_ThirdTrimester] AS [PartB_Cost_ThirdTrimester],
[PatientPregnancyStat].[PartA_MEOC_Cost_FirstTrimester] AS [PartA_MEOC_Cost_FirstTrimester],
[PatientPregnancyStat].[PartA_MEOC_Cost_SecondTrimester] AS [PartA_MEOC_Cost_SecondTrimester],
[PatientPregnancyStat].[PartA_MEOC_Cost_ThirdTrimester] AS [PartA_MEOC_Cost_ThirdTrimester],
[PatientPregnancyStat].[PartB_MEOC_Cost_FirstTrimester] AS [PartB_MEOC_Cost_FirstTrimester],
[PatientPregnancyStat].[PartB_MEOC_Cost_SecondTrimester] AS [PartB_MEOC_Cost_SecondTrimester],
[PatientPregnancyStat].[PartB_MEOC_Cost_ThirdTrimester] AS [PartB_MEOC_Cost_ThirdTrimester]
FROM [dbo].[OrgHierarchy] [OrgHierarchy]
INNER JOIN [dbo].[PatientPregnancyStat] [PatientPregnancyStat] ON ([OrgHierarchy].[LbPatientId] = [PatientPregnancyStat].[LbPatientId])
GO
