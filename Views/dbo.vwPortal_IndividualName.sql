SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_IndividualName] AS
SELECT * FROM [Version22LbportalTemplate].dbo.IndividualName 
WHERE db_name() = 'Version22EdwTemplate' 
GO
