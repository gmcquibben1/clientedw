SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientVitals] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientVitals
WHERE db_name() = 'Version22EdwTemplate' 
GO
