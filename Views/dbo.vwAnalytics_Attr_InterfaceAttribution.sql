SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwAnalytics_Attr_InterfaceAttribution]
AS 
-- ==========================================================
-- Author:    Mike Hoxter
-- Create date:  11/07/2016
-- Description:  View of missing providers from interface attribution
-- ===========================================================

SELECT distinct 
  [vwPortal_InterfacePatientAttribution].[InterfacePatientId] AS [InterfacePatientId],
  [vwPortal_InterfacePatientAttribution].[PatientIdentifier] AS [PatientIdentifier],
  [vwPortal_InterfacePatientAttribution].[InterfaceProviderId] AS [InterfaceProviderId],
  [vwPortal_InterfacePatientAttribution].[ProviderIdentifier] AS [ProviderIdentifier],
  [vwPortal_InterfacePatientAttribution].[ProviderNPI] AS [ProviderNPI],
  [vwPortal_InterfacePatientAttribution].[ProviderFirstName] AS [ProviderFirstName],
  [vwPortal_InterfacePatientAttribution].[ProviderLastName] AS [ProviderLastName],
  [vwPortal_InterfacePatientAttribution].[ProgramName] AS [ProgramName],
  [vwPortal_ProviderActive].[NationalProviderIdentifier] AS [NationalProviderIdentifier],
  [NPI_Lookup].[NPI] AS [NPI],
  [NPI_Lookup].[NAME] AS [NAME],
  [NPI_Lookup].City AS City,
  [SourceSystem].[Name] AS [SourceName],
  [SourceSystem].[Description] AS [Description]
FROM [dbo].[vwPortal_InterfacePatientAttribution] [vwPortal_InterfacePatientAttribution]
  LEFT JOIN [dbo].[vwPortal_ProviderActive] [vwPortal_ProviderActive] ON ([vwPortal_InterfacePatientAttribution].[ProviderNPI] = [vwPortal_ProviderActive].[NationalProviderIdentifier])
  LEFT JOIN [dbo].[NPI_Lookup] [NPI_Lookup] ON ([vwPortal_InterfacePatientAttribution].[ProviderNPI] = [NPI_Lookup].[NPI])
  INNER JOIN [dbo].[vwPortal_InterfaceSystem] [vwPortal_InterfaceSystem] ON ([vwPortal_InterfacePatientAttribution].[InterfaceSystemId] = [vwPortal_InterfaceSystem].[InterfaceSystemId])
  INNER JOIN [dbo].[SourceSystem] [SourceSystem] ON ([vwPortal_InterfaceSystem].[SourceSystemId] = [SourceSystem].[SourceSystemId])
  WHERE [vwPortal_ProviderActive].ProviderID IS NULL AND [vwPortal_InterfacePatientAttribution].ProviderNPI is not null
          AND [NPI_Lookup].NPI is not null
GO
