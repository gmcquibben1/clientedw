SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_PatientRisk] AS
SELECT * FROM [Version22LbportalTemplate].dbo.PatientRisk 
WHERE db_name() = 'Version22EdwTemplate' 
GO
