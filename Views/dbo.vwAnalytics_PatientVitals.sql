SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_PatientVitals]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
-- ==========================================================
-- Author:    Ismail
-- Create date:  08/26/2016
-- Description:  View of Analytics PatientVitals Workbook
-- ===========================================================

SELECT 
[PatientVitals].[PatientID] AS [PatientID],
[PatientVitals].[SourceSystemId] AS [SourceSystemId],
[PatientVitals].[ServiceDateTime] AS [ServiceDateTime],
[PatientVitals].[HeightCM] AS [HeightCM],
[PatientVitals].[HeightComments] AS [HeightComments],
[PatientVitals].[WeightKG] AS [WeightKG],
[PatientVitals].[WeightComments] AS [WeightComments],
[PatientVitals].[TemperatureCelcius] AS [TemperatureCelcius],
[PatientVitals].[TemperatureComments] AS [TemperatureComments],
[PatientVitals].[Pulse] AS [Pulse],
[PatientVitals].[PulseComments] AS [PulseComments],
[PatientVitals].[Respiration] AS [Respiration],
[PatientVitals].[RespirationComments] AS [RespirationComments],
[PatientVitals].[BloodPressureSystolic] AS [BloodPressureSystolic],
[PatientVitals].[BloodPressureDiastolic] AS [BloodPressureDiastolic],
[PatientVitals].[BloodPressureComment] AS [BloodPressureComment],
TRY_CONVERT(decimal(8,2),[PatientVitals].[OxygenSaturationSP02]) AS [OxygenSaturationSP02],
[PatientVitals].[OxygenSaturationComment] AS [OxygenSaturationComment],
[PatientVitals].[Clinician] AS [Clinician],
[PatientVitals].[PatientVitalsComment] AS [PatientVitalsComment],
[PatientVitals].[EncounterIdentifier] AS [EncounterIdentifier],
[PatientVitals].[RenderingProviderNPI] AS [RenderingProviderNPI],
[OrgHierarchy].[State] AS [State],
[OrgHierarchy].[City] AS [City],
[OrgHierarchy].[Zip] AS [Zip],
[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
[OrgHierarchy].[MemberID] AS [MemberID],
[OrgHierarchy].[MemberFullName] AS [MemberFullName],
[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
[OrgHierarchy].[BirthDate] AS [BirthDate],
[OrgHierarchy].[GenderId] AS [GenderId],
[OrgHierarchy].[County] AS [County],
[OrgHierarchy].[LbPatientId] AS [LbPatientId],
[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
[OrgHierarchy].[PatientId1] AS [PatientId1],
[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
[OrgHierarchy].[PatientId2] AS [PatientId2],
[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
[OrgHierarchy].[PatientId3] AS [PatientId3],
[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
[OrgHierarchy].[PatientId4] AS [PatientId4]
FROM [dbo].[PatientVitals] [PatientVitals]
INNER JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([PatientVitals].[PatientID] = [OrgHierarchy].[LbPatientId])
where [PatientVitals].[DeleteInd]  = 0
GO
