SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientSocialHistory]
AS 

SELECT 
	psh.PatientId, 
	psh.SourceSystemId,
	psh.SmokingStatus,
	psh.MaritalStatus,
	psh.EmploymentStatus,
	psh.TransportationInd,
	psh.LivingArrangementCode,
	psh.AlcoholUseInd,
	psh.DrugUseInd,
	psh.CaffeineUseInd,
	psh.SexuallyActiveInd,
	psh.ExerciseHoursPerWeek,
	psh.CreateDateTime
FROM PatientSocialHistory psh
	   
	
GO
