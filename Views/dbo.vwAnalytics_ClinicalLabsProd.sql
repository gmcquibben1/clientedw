SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ==========================================================
-- Author:    Ismail
-- Create date:  08/23/2016
-- Description:  View of Analytics ClinicalLabs Workbook
-- Edited on 1/16/2016 to make [Value] field try_converted into a float - MH
-- ===========================================================

CREATE VIEW [dbo].[vwAnalytics_ClinicalLabsProd]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT 
[OrgHierarchy].[State] AS [State],
[OrgHierarchy].[City] AS [City],
[OrgHierarchy].[Zip] AS [Zip],
[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
[OrgHierarchy].[MemberFullName] AS [MemberFullName],
[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
[OrgHierarchy].[BirthDate] AS [BirthDate],
[OrgHierarchy].[GenderId] AS [GenderId],
[OrgHierarchy].[County] AS [County],
[OrgHierarchy].[LbPatientId] AS [LbPatientId],
[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
[OrgHierarchy].[PatientId1] AS [PatientId1],
[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
[OrgHierarchy].[PatientId2] AS [PatientId2],
[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
[OrgHierarchy].[PatientId3] AS [PatientId3],
[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
[PatientLabOrder].[ServiceDescription] AS [ServiceDescription],
[PatientLabOrder].[ServiceCodingSystemName] AS [ServiceCodingSystemName],
[PatientLabOrder].[OrderingProviderId] AS [OrderingProviderId],
[PatientLabOrder].[OrderingProviderIdType] AS [OrderingProviderIdType],
[PatientLabOrder].[OrderingProviderName] AS [OrderingProviderName],
[PatientLabResult].[ObservationCode1] AS [ObservationCode1],
[PatientLabResult].[ObservationDescription1] AS [ObservationDescription1],
[PatientLabResult].[ObservationCodingSystemName1] AS [ObservationCodingSystemName1],
[PatientLabResult].[ObservationCode2] AS [ObservationCode2],
[PatientLabResult].[ObservationDescription2] AS [ObservationDescription2],
[PatientLabResult].[ObservationCodingSystemName2] AS [ObservationCodingSystemName2],
TRY_CONVERT(FLOAT,[PatientLabResult].[Value]) AS [Value],
[PatientLabResult].[Units] AS [Units],
[PatientLabResult].[ReferenceRange] AS [ReferenceRange],
[PatientLabResult].[AbnormalFlag] AS [AbnormalFlag],
[PatientLabResult].[ResultStatus] AS [ResultStatus],
[PatientLabResult].[ResultComment] AS [ResultComment],
[PatientLabResult].[ObservationDate] AS [ObservationDate]
FROM [dbo].[OrgHierarchy] [OrgHierarchy]
INNER JOIN [dbo].[PatientLabOrder] [PatientLabOrder] ON [OrgHierarchy].LbPatientId = [PatientLabOrder].PatientId
INNER JOIN [dbo].[PatientLabResult] [PatientLabResult] ON ([PatientLabOrder].[PatientLabOrderId] = [PatientLabResult].[PatientLabOrderId])
WHERE ISNUMERIC([PatientLabResult].[Value]) = 1
GO
