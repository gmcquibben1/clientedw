SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_TaskFileItem] AS
SELECT * FROM [Version22LbportalTemplate].dbo.TaskFileItem
WHERE db_name() = 'Version22EdwTemplate'; 
GO
