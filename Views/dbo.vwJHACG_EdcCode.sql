SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwJHACG_EdcCode] AS
SELECT * FROM EdwReferenceData.dbo.JHACG_EdcCode
WHERE db_name() = 'Version22EdwTemplate'
GO
