SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =====================================================================================================================
-- Author:		Lan
-- Create date: 2016-06-01
-- Description: This view provides patient medical input data for JHACG, which extracts patient medical data from 
-- PatientProcedure, PatientDiagnosis, PatientChronicCondition and JhacgPatientListStaging( a subset data of Orghierarchy)
-- Modified By: Lan
-- Modified Date: 2016-07-29
-- Description:	Add icd_proc_cd_1 and icd_proc_version_1 fields for procedure input
-- =====================================================================================================================

CREATE VIEW [dbo].[vwJhacgPatientMedicalInput]
AS 

SELECT DISTINCT 
PD.PatientId AS patient_id,
--CASE WHEN DCT.Name like '%10%' THEN '10CM' 
--     WHEN DCT.Name like '%9%' THEN '9CM'
--	 ELSE DCT.Name
--END AS dx_version_1,
COALESCE(DCMST.dx_version_1, '10CM') AS dx_version_1,
DC.DiagnosisCode as dx_cd_1, 
CAST(PD.[DiagnosisDateTime] AS DATE) AS service_begin_date, 
CAST(PD.[DiagnosisDateTime] AS DATE) AS service_end_date, 
NULL AS service_place,
NULL AS revenue_code,
NULL AS procedure_code,
RenderingProviderNPI AS provider_id,
NPIL.Taxonomy AS provider_specialty,
DrgCode AS drg_cd,
NPIL.Taxonomy AS provider_Standard_specialty,
NULL AS icd_proc_version_1 ,
NULL AS icd_proc_cd_1
FROM JhacgPatientListStaging P
JOIN PatientDiagnosis PD
ON P.LbPatientId =  PD.PatientID
JOIN DiagnosisCode DC
ON PD.DiagnosisCodeId = DC.DiagnosisCodeId
--JOIN [dbo].[DiagnosisCodingSystemType] DCT
--ON DC.DiagnosisCodingSystemTypeId = DCT.DiagnosisCodingSystemTypeID
LEFT JOIN ( SELECT [DiagnosisCode], '10CM'  AS dx_version_1 FROM [EdwReferenceData].[dbo].[CmsICD10]
       UNION 
	   SELECT [DiagnosisCode], '9'  AS dx_version_1 FROM [EdwReferenceData].[dbo].[CmsICD9] ) DCMST
ON DC.[DiagnosisCode]= DCMST.[DiagnosisCode]  
LEFT JOIN [dbo].[NPI_Lookup] NPIL
ON PD.RenderingProviderNPI = NPIL.[NPI]
WHERE CAST(PD.[DiagnosisDateTime] AS DATE) > DATEADD(MM,-16,GETUTCDATE())
  AND CAST(PD.[DiagnosisDateTime] AS DATE) < DATEADD(MM,-4,GETUTCDATE())

UNION ALL

SELECT DISTINCT 
PCC.PatientId  AS patient_id, 
--CASE WHEN DCT.Name like '%10%' THEN '10CM' 
--     WHEN DCT.Name like '%9%' THEN '9CM'
--	 ELSE DCT.Name
--END AS dx_version_1,
COALESCE(DCMST.dx_version_1, '10CM') AS dx_version_1,
DC.DiagnosisCode AS dx_cd_1, 
CAST(DATEADD(MM,-5,GETUTCDATE()) AS DATE) AS service_begin_date, 
CAST(DATEADD(MM,-5,GETUTCDATE())AS DATE) AS service_end_date, 
NULL AS service_place,
NULL AS revenue_code,
NULL AS procedure_code,
NULL AS provider_id,
NULL AS provider_specialty,
NULL AS drg_cd,
NULL AS provider_Standard_specialty,
NULL AS icd_proc_version_1 ,
NULL AS icd_proc_cd_1
FROM JhacgPatientListStaging P
JOIN PatientChronicCondition PCC
ON P.LbPatientId =  PCC.PatientID
JOIN DiagnosisCode DC 
ON PCC.DiagnosisCodeId = DC.DiagnosisCodeId
--JOIN [dbo].[DiagnosisCodingSystemType] DCT
--ON DC.DiagnosisCodingSystemTypeId = DCT.DiagnosisCodingSystemTypeID
LEFT JOIN ( SELECT [DiagnosisCode], '10CM'  AS dx_version_1 FROM [EdwReferenceData].[dbo].[CmsICD10]
       UNION 
	   SELECT [DiagnosisCode], '9'  AS dx_version_1 FROM [EdwReferenceData].[dbo].[CmsICD9] ) DCMST
ON DC.[DiagnosisCode]= DCMST.[DiagnosisCode]  

UNION ALL

SELECT DISTINCT 
PP.PatientID  AS patient_id,
NULL AS dx_version_1,
NULL AS dx_cd_1,
CAST(PP.ProcedureDateTime AS DATE) AS service_begin_date, 
CAST(PP.ProcedureDateTime AS DATE) service_end_date, 
--LTRIM(RTRIM([PlaceOfService])) AS service_place,
CASE WHEN  TRY_CONVERT(INT,[RevenueCode]) between 100 and 219
THEN '21'
ELSE LTRIM(RTRIM([PlaceOfService]))
END AS service_place,
TRY_CONVERT(INT,[RevenueCode]) AS revenue_code,
--CASE WHEN CHARINDEX('0',LTRIM(RTRIM([RevenueCode]))) = 1
--     THEN RIGHT(LTRIM(RTRIM([RevenueCode])),DATALENGTH(LTRIM(RTRIM([RevenueCode])))-1)
--	 ELSE LTRIM(RTRIM([RevenueCode]))
--END AS revenue_code,
--PC.ProcedureCode AS procedure_code,
CASE WHEN PCMS.CodeType NOT IN ('ICD10PC','ICD9PC')
     THEN PC.ProcedureCode 
END AS procedure_code,
[RenderingProviderNPI] AS provider_id,
NPIL.Taxonomy AS provider_specialty,
NULL AS drg_cd,
NPIL.Taxonomy AS provider_Standard_specialty,
CASE WHEN PCMS.CodeType = 'ICD10PC'
     THEN '10CM' 
	 WHEN PCMS.CodeType = 'ICD9PC'
	 THEN '9'
END AS icd_proc_version_1 ,
CASE WHEN PCMS.CodeType in ('ICD10PC','ICD9PC')
     THEN PC.ProcedureCode 
END AS icd_proc_cd_1
FROM JhacgPatientListStaging P
JOIN PatientProcedure PP
ON P.LbPatientId =  PP.PatientID
JOIN ProcedureCode PC 
ON PP.ProcedureCodeId = PC.[ProcedureCodeId]
LEFT JOIN [EdwReferenceData].[dbo].[vwProcedureCodeMaster] PCMS
ON PC.ProcedureCode = PCMS.ProcedureCode
LEFT JOIN [dbo].[NPI_Lookup] NPIL
ON PP.RenderingProviderNPI = NPIL.[NPI]
WHERE CAST(PP.ProcedureDateTime AS DATE) > DATEADD(MM,-16,GETUTCDATE())
AND CAST(PP.ProcedureDateTime AS DATE) < DATEADD(MM,-4,GETUTCDATE())


UNION ALL

SELECT DISTINCT 
PP.PatientID  AS patient_id,
NULL AS dx_version_1,
NULL AS dx_cd_1,
CAST(PP.ProcedureDateTime AS DATE) AS service_begin_date, 
CAST(PP.ProcedureDateTime AS DATE) service_end_date, 
--[PlaceOfService] AS service_place,
CASE WHEN  TRY_CONVERT(INT,[RevenueCode]) between 100 and 219
THEN '21'
ELSE LTRIM(RTRIM([PlaceOfService]))
END AS service_place,
TRY_CONVERT(INT,[RevenueCode]) AS revenue_code,
--CASE WHEN CHARINDEX('0',LTRIM(RTRIM([RevenueCode]))) = 1
--     THEN RIGHT(LTRIM(RTRIM([RevenueCode])),DATALENGTH(LTRIM(RTRIM([RevenueCode])))-1)
--	 ELSE LTRIM(RTRIM([RevenueCode]))
--END AS revenue_code,
NULL AS procedure_code,
NULL AS provider_id,
NULL AS provider_specialty,
NULL AS drg_cd,
NULL AS provider_Standard_specialty,
NULL AS icd_proc_version_1 ,
NULL AS icd_proc_cd_1
FROM JhacgPatientListStaging P
JOIN PatientProcedure PP
ON P.LbPatientId =  PP.PatientID
WHERE CAST(PP.ProcedureDateTime AS DATE) > DATEADD(MM,-16,GETUTCDATE())
AND CAST(PP.ProcedureDateTime AS DATE) < DATEADD(MM,-4,GETUTCDATE())
AND [RevenueCode] IS NOT NULL
--AND LTRIM(RTRIM([PlaceOfService])) NOT IN ('12','31','32','33','34','41','42','65','81','99','00')

UNION ALL

SELECT DISTINCT 
P.LbPatientId  AS patient_id,
NULL AS dx_version_1,
NULL AS dx_cd_1,
[CLM_LINE_FROM_DT] AS service_begin_date, 
[CLM_LINE_FROM_DT] service_end_date, 
CASE WHEN [INP] = 1
     THEN 'IP'
	 WHEN [ER] =1
	 THEN 'ED'
	 WHEN TRY_CONVERT(INT,CLM_LINE_REV_CTR_CD) IN(526,516,456)
	 THEN 'UC'
	 ELSE 'OP'
END AS service_place,
--CAST([CLM_LINE_REV_CTR_CD] AS VARCHAR(40)) AS revenue_code,
TRY_CONVERT(INT,CLM_LINE_REV_CTR_CD) AS revenue_code,
--CASE WHEN PATINDEX('0',LTRIM(RTRIM(CLM_LINE_REV_CTR_CD))) = 1
--     THEN RIGHT(LTRIM(RTRIM(CLM_LINE_REV_CTR_CD)),DATALENGTH(LTRIM(RTRIM(CLM_LINE_REV_CTR_CD)))-1)
--	 ELSE LTRIM(RTRIM(CLM_LINE_REV_CTR_CD))
--END AS revenue_code,
NULL AS procedure_code,
PA.FAC_PRVDR_NPI_NUM AS provider_id,
NPIL.Taxonomy AS provider_specialty,
PA.DGNS_DRG_CD AS drg_cd,
NPIL.Taxonomy AS provider_Standard_specialty,
NULL AS icd_proc_version_1 ,
NULL AS icd_proc_cd_1
FROM JhacgPatientListStaging P
JOIN [dbo].[CCLF_2_PartA_RCDetail] PD
ON P.LbPatientId =  PD.LbPatientId
JOIN CCLF_1_PartA_Header PA 
ON PD.CUR_CLM_UNIQ_ID = PA.CUR_CLM_UNIQ_ID
Left JOIN [dbo].[CCLF_0_PartA_CLM_TYPES] CT
ON PA.[CUR_CLM_UNIQ_ID] = CT.[CUR_CLM_UNIQ_ID]
LEFT JOIN NPI_Lookup NPIL 
ON PA.FAC_PRVDR_NPI_NUM = NPIL.NPI
WHERE [CLM_LINE_FROM_DT] > DATEADD(MM,-16,GETUTCDATE())
AND [CLM_LINE_FROM_DT] < DATEADD(MM,-4,GETUTCDATE())
AND [CLM_LINE_REV_CTR_CD] IS NOT NULL
GO
