SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_PatientHealthcareOrg] AS
SELECT * FROM [Version22LbportalTemplate].dbo.PatientHealthcareOrg 
WHERE db_name() = 'Version22EdwTemplate' 
GO
