SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatient] AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatient
WHERE db_name() = 'Version22EdwTemplate' 
GO
