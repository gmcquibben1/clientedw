SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_MeasureQADiag_LabsMeasureQA]
----WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT 
  [PatientLabResult].[ObservationCode1] AS [ObservationCode1],
  [PatientLabResult].[ObservationDescription1] AS [ObservationDescription1],
  [PatientLabResult].[ObservationCodingSystemName1] AS [ObservationCodingSystemName1],
  [PatientLabResult].[ObservationCode2] AS [ObservationCode2],
  [PatientLabResult].[ObservationDescription2] AS [ObservationDescription2],
  [PatientLabResult].[ObservationCodingSystemName2] AS [ObservationCodingSystemName2],
  TRY_CONVERT(decimal(18,4),[PatientLabResult].[Value]) AS [Value],
  [PatientLabResult].[Units] AS [Units],
  [PatientLabResult].[ReferenceRange] AS [ReferenceRange],
  [PatientLabResult].[AbnormalFlag] AS [AbnormalFlag],
  [PatientLabResult].[ResultStatus] AS [ResultStatus],
  [PatientLabResult].[ResultComment] AS [ResultComment],
  [PatientLabResult].[ObservationDate] AS [ObservationDate],
  ISNUMERIC([PatientLabResult].[Value]) AS [ValueNumeric],
  [PatientLabOrder].[PatientLabOrderId] AS [PatientLabOrderId (PatientLabOrder)],
  [PatientLabOrder].[SourceSystemId] AS [SourceSystemId],
  [PatientLabOrder].[OrderNumber] AS [OrderNumber],
  [PatientLabOrder].[ServiceDescription] AS [ServiceDescription],
  [PatientLabOrder].[ServiceCodingSystemName] AS [ServiceCodingSystemName],
  [PatientLabOrder].[OrderingProviderIdType] AS [OrderingProviderIdType],
  [PatientLabOrder].[OrderingProviderName] AS [OrderingProviderName],
  [PatientLabOrder].[EncounterIdentifier] AS [EncounterIdentifier],
  [PatientLabOrder].[OrderDate] AS [OrderDate],
  [SourceSystem].[Name] AS [Name],
  [SourceSystem].[Description] AS [Description]
FROM [dbo].[PatientLabResult] [PatientLabResult]
  LEFT JOIN [dbo].[PatientLabOrder] [PatientLabOrder] ON ([PatientLabResult].[PatientLabOrderId] = [PatientLabOrder].[PatientLabOrderId])
  LEFT JOIN [dbo].[SourceSystem] [SourceSystem] ON ([PatientLabOrder].[SourceSystemId] = [SourceSystem].[SourceSystemId])
  where ISNUMERIC([PatientLabResult].[Value]) = 1
GO
