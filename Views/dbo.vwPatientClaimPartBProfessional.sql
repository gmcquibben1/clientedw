SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientClaimPartBProfessional]
AS
SELECT
 B.CUR_CLM_UNIQ_ID as ClaimId, 
 B.CLM_LINE_NUM as ClaimLinenum,IDR.LbPatientId AS PatientId,
 B.CLM_FROM_DT as ClaimFromDate, 
 B.CLM_THRU_DT as ClaimThruDate, 
 B.CLM_PRVDR_SPCLTY_CD as SpecialtyCode, S.[Description] as SpecialtyDescription,
 B.CLM_LINE_HCPCS_CD as CPT, BG.Cat1 as CPTGroup1, BG.Cat2 as CPTGroup2, BG.Cat3 as CPTGroup,
 C.description as CPTDescription,
 B.HCPCS_1_MDFR_CD as CPTMod1, B.HCPCS_2_MDFR_CD  as CPTMod2, B.HCPCS_3_MDFR_CD as CPTMod3, B.HCPCS_4_MDFR_CD as CPTMod4, B.HCPCS_5_MDFR_CD as CPTMod5,
 B.RNDRG_PRVDR_NPI_NUM as RenderingNPI, NPI.[NAME] as RenderingName, B.CLM_LINE_CVRD_PD_AMT as PaidAmt, 
 B.CLM_LINE_DGNS_CD as PrimaryDiag, DG.Level1 as PrimaryDiagGroup1, DG.Level2 as PrimaryDiagGroup2, DG.Level3 as PrimaryDiagGroup3,
 D.LongDescription as DiagnosisDescription
FROM CCLF_5_PartB_Physicians B
LEFT JOIN NPI_Lookup NPI ON B.RNDRG_PRVDR_NPI_NUM = NPI.NPI
LEFT JOIN SPECIALTY_CODES S ON B.CLM_PRVDR_SPCLTY_CD = S.Code
LEFT JOIN CPT4Mstr C ON B.CLM_LINE_HCPCS_CD = C.cpt4_code_id
LEFT JOIN ICD9Mstr D ON B.CLM_LINE_DGNS_CD = D.ICD9
JOIN DiagnosisGroup DG ON B.CLM_LINE_DGNS_CD = DG.DiagnosisCode
LEFT JOIN BETOS_CPT BC ON B.CLM_LINE_HCPCS_CD = BC.HCPC
JOIN BETOS_GROUPS BG ON BC.BETOSGroupID = BG.GroupID
JOIN PatientIdReference IDR ON B.BENE_HIC_NUM = IDR.ExternalId
GO
