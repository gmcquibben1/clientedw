SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_ProviderActive] AS
SELECT DISTINCT t1.*
FROM  [dbo].[vwPortal_Provider] t1
INNER JOIN [dbo].[vwPortal_ProviderHealthcareOrg] t2
ON t1.[ProviderID] = t2.[ProviderID]
AND t1.[DeleteInd] = 0 AND t2.[DeleteInd] = 0 AND IndividualId IS NOT NULL AND [DefaultInd] = 1

GO
