SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwSourceSystem] AS SELECT SourceSystemId, Name, Description, ExternalReferenceIdentifier, OtherReferenceIdentifier FROM Version22LbPortalTemplate.dbo.SourceSystem
GO
