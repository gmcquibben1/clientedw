SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_SystemSettings] AS 
SELECT * FROM Version22LbportalTemplate.dbo.SystemSettings
WHERE db_name() = 'Version22EdwTemplate' 
GO
