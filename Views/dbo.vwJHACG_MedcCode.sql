SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwJHACG_MedcCode] AS
SELECT * FROM EdwReferenceData.dbo.JHACG_MedcCode
WHERE db_name() = 'Version22EdwTemplate'
GO
