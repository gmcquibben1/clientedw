SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientProcedure]
AS 

SELECT pp.PatientId, 
	pp.SourceSystemId, 
	pcs.DisplayValue AS ProcedureCodingSystem,
	pc.ProcedureCode,
	pc.ProcedureDescription ,
	--pp.ProcedureFunctionTypeID,
	pp.ProcedureCodeModifier1,
	pp.ProcedureCodeModifier2,
	pp.ProcedureCodeModifier3,
	pp.ProcedureCodeModifier4,
	pp.ProcedureDateTime,
	pp.ProcedureComment,
	pp.EncounterID,
	pp.PerformedByProviderID,
	pp.PerformedByClinician,
	pp.OrderedByProviderID,
	pp.OrderedByClinician,
	pp.CreateDateTime,
	pp.ModifyDateTime,
	pc.ProcedureDescriptionAlternate, 
  npi.[NAME] as [RenderingProviderName], 
  pp.RenderingProviderNPI, 
  npi.Taxonomy as [RenderingProviderTaxonomy], 
  npi.Specialty as [RenderingProviderSpecialtyCode],
  sc.[Description] as [RenderingProviderSpecialty]

FROM PatientProcedure pp 
	 INNER JOIN PatientProcedureProcedureCode pppc ON pp.PatientProcedureId = pppc.PatientProcedureId 
	 INNER JOIN ProcedureCode pc ON pc.ProcedureCodeId = pppc.ProcedureCodeId 
	 LEFT JOIN ProcedureCodingSystemType pcs ON pcs.ProcedureCodingSystemTypeId = pppc.ProcedureCodingSystemTypeId
   LEFT JOIN NPI_Lookup npi on pp.RenderingProviderNPI = npi.NPI
   LEFT JOIN SPECIALTY_CODES sc on npi.Specialty = sc.Code
WHERE pp.DeleteInd = 0 AND pc.DeleteInd = 0
	
GO
