SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientHistory] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientHistory
WHERE db_name() = 'Version22EdwTemplate' 
GO
