SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientIdReference] 
AS 

SELECT
	pir.LbPatientId,
	pir.IdTypeDesc,
	pir.ExternalId,
	ss.Name
FROM PatientIdReference pir
LEFT OUTER JOIN SourceSystem ss ON ss.SourceSystemId = pir.SourceSystemId
GO
