SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_ProviderRoleType] AS
SELECT * FROM [Version22LbportalTemplate].dbo.ProviderRoleType 
WHERE db_name() = 'Version22EdwTemplate' 
GO
