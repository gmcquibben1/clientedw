SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_EDW_MetaData_PartD]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [CCLF_7_PartD].[id] AS [id],
  [CCLF_7_PartD].[CUR_CLM_UNIQ_ID] AS [CUR_CLM_UNIQ_ID],
  [CCLF_7_PartD].[PREV_CLM_UNIQ_ID] AS [PREV_CLM_UNIQ_ID],
  [CCLF_7_PartD].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
  [CCLF_7_PartD].[CLM_LINE_NDC_CD] AS [CLM_LINE_NDC_CD],
  [CCLF_7_PartD].[CLM_TYPE_CD] AS [CLM_TYPE_CD],
  [CCLF_7_PartD].[CLM_LINE_FROM_DT] AS [CLM_LINE_FROM_DT],
  [CCLF_7_PartD].[PRVDR_SRVC_ID_QLFYR_CD] AS [PRVDR_SRVC_ID_QLFYR_CD],
  [CCLF_7_PartD].[CLM_SRVC_PRVDR_GNRC_ID_NUM] AS [CLM_SRVC_PRVDR_GNRC_ID_NUM],
  [CCLF_7_PartD].[CLM_DSPNSNG_STUS_CD] AS [CLM_DSPNSNG_STUS_CD],
  [CCLF_7_PartD].[CLM_DAW_PROD_SLCTN_CD] AS [CLM_DAW_PROD_SLCTN_CD],
  [CCLF_7_PartD].[CLM_LINE_SRVC_UNIT_QTY] AS [CLM_LINE_SRVC_UNIT_QTY],
  [CCLF_7_PartD].[CLM_LINE_DAYS_SUPLY_QTY] AS [CLM_LINE_DAYS_SUPLY_QTY],
  [CCLF_7_PartD].[PRVDR_PRSBNG_ID_QLFYR_CD] AS [PRVDR_PRSBNG_ID_QLFYR_CD],
  [CCLF_7_PartD].[CLM_PRSBNG_PRVDR_GNRC_ID_NUM] AS [CLM_PRSBNG_PRVDR_GNRC_ID_NUM],
  [CCLF_7_PartD].[CLM_LINE_BENE_PMT_AMT] AS [CLM_LINE_BENE_PMT_AMT],
  [CCLF_7_PartD].[CLM_ADJSMT_TYPE_CD] AS [CLM_ADJSMT_TYPE_CD],
  [CCLF_7_PartD].[CLM_EFCTV_DT] AS [CLM_EFCTV_DT],
  [CCLF_7_PartD].[CLM_IDR_LD_DT] AS [CLM_IDR_LD_DT],
  [CCLF_7_PartD].[CLM_LINE_RX_SRVC_RFRNC_NUM] AS [CLM_LINE_RX_SRVC_RFRNC_NUM],
  [CCLF_7_PartD].[CLM_LINE_RX_FILL_NUM] AS [CLM_LINE_RX_FILL_NUM],
  [CCLF_7_PartD].[fileDate] AS [fileDate],
  [CCLF_7_PartD].[ORG_HIC_NUM] AS [ORG_HIC_NUM],
  [CCLF_7_PartD].[Generic] AS [Generic],
  [CCLF_7_PartD].[Retail] AS [Retail],
  [CCLF_7_PartD].[Formulary] AS [Formulary],
  [CCLF_7_PartD].[CLM_LINE_PAYER_PAID_AMT] AS [CLM_LINE_PAYER_PAID_AMT],
  [CCLF_7_PartD].[CLM_LINE_NUM] AS [CLM_LINE_NUM],
  [CCLF_7_PartD].[LbPatientId] AS [LbPatientId (CCLF_7_PartD)],
  [CCLF_7_PartD].[SourceFeed] AS [SourceFeed],
  [OrgHierarchy].[ID] AS [ID1],
  [OrgHierarchy].[State] AS [State],
  [OrgHierarchy].[City] AS [City],
  [OrgHierarchy].[Zip] AS [Zip],
  [OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
  [OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
  [OrgHierarchy].[Level1Id] AS [Level1Id],
  [OrgHierarchy].[Level1Name] AS [Level1Name],
  [OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
  [OrgHierarchy].[Level2Id] AS [Level2Id],
  [OrgHierarchy].[Level2Name] AS [Level2Name],
  [OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
  [OrgHierarchy].[Level3Id] AS [Level3Id],
  [OrgHierarchy].[Level3Name] AS [Level3Name],
  [OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
  [OrgHierarchy].[Level4Id] AS [Level4Id],
  [OrgHierarchy].[Level4Name] AS [Level4Name],
  [OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
  [OrgHierarchy].[Level5Id] AS [Level5Id],
  [OrgHierarchy].[Level5Name] AS [Level5Name],
  [OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
  [OrgHierarchy].[Level6Id] AS [Level6Id],
  [OrgHierarchy].[Level6Name] AS [Level6Name],
  [OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
  [OrgHierarchy].[ContractName] AS [ContractName],
  [OrgHierarchy].[MemberID] AS [MemberID],
  [OrgHierarchy].[MemberFullName] AS [MemberFullName],
  [OrgHierarchy].[Member_Provider_Relationship_Effective_Date] AS [Member_Provider_Relationship_Effective_Date],
  [OrgHierarchy].[Member_Provider_Relationship_Termination_Date] AS [Member_Provider_Relationship_Termination_Date],
  [OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM (OrgHierarchy)],
  [OrgHierarchy].[Patient_Status] AS [Patient_Status],
  [OrgHierarchy].[ProviderId] AS [ProviderId],
  [OrgHierarchy].[ProviderName] AS [ProviderName],
  [OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
  [OrgHierarchy].[BirthDate] AS [BirthDate],
  [OrgHierarchy].[GenderId] AS [GenderId],
  [OrgHierarchy].[County] AS [County],
  [OrgHierarchy].[LastUpdatedTimestamp] AS [LastUpdatedTimestamp],
  [OrgHierarchy].[LbPatientId] AS [LbPatientId],
  [OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
  [OrgHierarchy].[PatientId1] AS [PatientId1],
  [OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
  [OrgHierarchy].[PatientId2] AS [PatientId2],
  [OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
  [OrgHierarchy].[PatientId3] AS [PatientId3],
  [OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
  [OrgHierarchy].[PatientId4] AS [PatientId4]
FROM [dbo].[CCLF_7_PartD] [CCLF_7_PartD]
  LEFT JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([CCLF_7_PartD].[LbPatientId] = [OrgHierarchy].[LbPatientId])
GO
