SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientLab] 
AS 

SELECT 
	plo.PatientId,
	plo.SourceSystemid,
	plo.OrderNumber,
	plo.ServiceId AS OrderServiceCode,
	plo.ServiceDescription AS OrderDescription,
	plo.OrderingProviderId,
	plo.OrderingProviderName,
	plo.OrderDate,
	plr.ObservationDate,
	plr.ObservationCode1,
	plr.ObservationDescription1,
	plr.ObservationCode2,
	plr.ObservationDescription2,
	plr.Value,
	plr.Units,
	plr.ReferenceRange,
	plr.AbnormalFlag,
	plr.ResultComment
FROM PatientLabOrder plo
	INNER JOIN PatientLabResult plr ON plr.PatientLabOrderId = plo.PatientLabOrderId
GO
