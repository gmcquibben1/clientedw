SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientClaimPartADetail]
AS 
SELECT 
CUR_CLM_UNIQ_ID AS ClaimId, IDR.LbPatientId AS PatientId,
CLM_LINE_NUM as ClaimLineNum,  AD.CLM_LINE_FROM_DT as FromDate, AD.CLM_LINE_THRU_DT as ThruDate, 
CLM_LINE_REV_CTR_CD as RevenueCode, 
CLM_LINE_HCPCS_CD as ProcedureCode, 
C.[description] as ProcedureDesc,
BG.Cat1 AS BETOSCat1, 
BG.Cat2 AS BETOSCat2, 
BG.Cat3 AS BETOSCat3,
HCPCS_1_MDFR_CD as Modifier1, 
HCPCS_2_MDFR_CD as Modifier2, 
HCPCS_3_MDFR_CD as Modifier3, 
HCPCS_4_MDFR_CD as Modifier4, 
HCPCS_5_MDFR_CD as Modifier5, AD.CLM_LINE_CVRD_PD_AMT LinePaidAmt,
CLM_LINE_SRVC_UNIT_QTY as Quantity
FROM CCLF_2_PartA_RCDetail AD
JOIN CPT4Mstr C ON AD.CLM_LINE_HCPCS_CD = C.cpt4_code_id
JOIN BETOS_CPT ON AD.CLM_LINE_HCPCS_CD = dbo.BETOS_CPT.HCPC
JOIN BETOS_GROUPS BG ON dbo.BETOS_CPT.BETOSGroupID = BG.GroupID
JOIN PatientIdReference IDR ON AD.BENE_HIC_NUM = IDR.ExternalId
GO
