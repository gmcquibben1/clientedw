SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_CCLF_5_PartB_Professional]
/*
-- ==========================================================
-- Author:    Ismail
-- Create date:  08/24/2016
-- Description:  View of Analytics CCLF_5_PartB_Professional Workbook
-- ===========================================================
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)


    MODIFICATIONS
	Version     Date            Author      JIRA            Change
	=======     ==========      ======      =========       =========	
	2.3.1	    2017-03-09     YL           LBETL-993       Replace [CPT4Mstr] with [ProcedureCode]
	 
=================================================================*/
AS 
SELECT [CCLF_5_PartB_Physicians].[id] AS [id],
[CCLF_5_PartB_Physicians].[CUR_CLM_UNIQ_ID] AS [CUR_CLM_UNIQ_ID],
[CCLF_5_PartB_Physicians].[PREV_CLM_UNIQ_ID] AS [PREV_CLM_UNIQ_ID],
[CCLF_5_PartB_Physicians].[CLM_LINE_NUM] AS [CLM_LINE_NUM],
[CCLF_5_PartB_Physicians].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[CCLF_5_PartB_Physicians].[CLM_TYPE_CD] AS [CLM_TYPE_CD],
[CCLF_5_PartB_Physicians].[CLM_FROM_DT] AS [CLM_FROM_DT],
[CCLF_5_PartB_Physicians].[CLM_THRU_DT] AS [CLM_THRU_DT],
[CCLF_5_PartB_Physicians].[RNDRG_PRVDR_TYPE_CD] AS [RNDRG_PRVDR_TYPE_CD],
[CCLF_5_PartB_Physicians].[RNDRG_PRVDR_FIPS_ST_CD] AS [RNDRG_PRVDR_FIPS_ST_CD],
[CCLF_5_PartB_Physicians].[CLM_PRVDR_SPCLTY_CD] AS [CLM_PRVDR_SPCLTY_CD],
[CCLF_5_PartB_Physicians].[CLM_FED_TYPE_SRVC_CD] AS [CLM_FED_TYPE_SRVC_CD],
[CCLF_5_PartB_Physicians].[CLM_POS_CD] AS [CLM_POS_CD],
[CCLF_5_PartB_Physicians].[CLM_LINE_FROM_DT] AS [CLM_LINE_FROM_DT],
[CCLF_5_PartB_Physicians].[CLM_LINE_THRU_DT] AS [CLM_LINE_THRU_DT],
[CCLF_5_PartB_Physicians].[CLM_LINE_HCPCS_CD] AS [CLM_LINE_HCPCS_CD],
[CCLF_5_PartB_Physicians].[CLM_LINE_CVRD_PD_AMT] AS [CLM_LINE_CVRD_PD_AMT],
[CCLF_5_PartB_Physicians].[CLM_PRMRY_PYR_CD] AS [CLM_PRMRY_PYR_CD],
[CCLF_5_PartB_Physicians].[CLM_LINE_DGNS_CD] AS [CLM_LINE_DGNS_CD],
[CCLF_5_PartB_Physicians].[CLM_RNDRG_PRVDR_TAX_NUM] AS [CLM_RNDRG_PRVDR_TAX_NUM],
[CCLF_5_PartB_Physicians].[RNDRG_PRVDR_NPI_NUM] AS [RNDRG_PRVDR_NPI_NUM],
[CCLF_5_PartB_Physicians].[CLM_CARR_PMT_DNL_CD] AS [CLM_CARR_PMT_DNL_CD],
[CCLF_5_PartB_Physicians].[CLM_PRCSG_IND_CD] AS [CLM_PRCSG_IND_CD],
[CCLF_5_PartB_Physicians].[CLM_ADJSMT_TYPE_CD] AS [CLM_ADJSMT_TYPE_CD],
[CCLF_5_PartB_Physicians].[CLM_EFCTV_DT] AS [CLM_EFCTV_DT],
[CCLF_5_PartB_Physicians].[CLM_IDR_LD_DT] AS [CLM_IDR_LD_DT],
[CCLF_5_PartB_Physicians].[CLM_CNTL_NUM] AS [CLM_CNTL_NUM],
[CCLF_5_PartB_Physicians].[BENE_EQTBL_BIC_HICN_NUM] AS [BENE_EQTBL_BIC_HICN_NUM],
[CCLF_5_PartB_Physicians].[CLM_LINE_ALOWD_CHRG_AMT] AS [CLM_LINE_ALOWD_CHRG_AMT],
[CCLF_5_PartB_Physicians].[CLM_LINE_ALOWD_UNIT_QTY] AS [CLM_LINE_ALOWD_UNIT_QTY],
[CCLF_5_PartB_Physicians].[HCPCS_1_MDFR_CD] AS [HCPCS_1_MDFR_CD],
[CCLF_5_PartB_Physicians].[HCPCS_2_MDFR_CD] AS [HCPCS_2_MDFR_CD],
[CCLF_5_PartB_Physicians].[HCPCS_3_MDFR_CD] AS [HCPCS_3_MDFR_CD],
[CCLF_5_PartB_Physicians].[HCPCS_4_MDFR_CD] AS [HCPCS_4_MDFR_CD],
[CCLF_5_PartB_Physicians].[HCPCS_5_MDFR_CD] AS [HCPCS_5_MDFR_CD],
[CCLF_5_PartB_Physicians].[CLM_DISP_CD] AS [CLM_DISP_CD],
[CCLF_5_PartB_Physicians].[CLM_DGNS_1_CD] AS [CLM_DGNS_1_CD],
[CCLF_5_PartB_Physicians].[CLM_DGNS_2_CD] AS [CLM_DGNS_2_CD],
[CCLF_5_PartB_Physicians].[CLM_DGNS_3_CD] AS [CLM_DGNS_3_CD],
[CCLF_5_PartB_Physicians].[CLM_DGNS_4_CD] AS [CLM_DGNS_4_CD],
[CCLF_5_PartB_Physicians].[CLM_DGNS_5_CD] AS [CLM_DGNS_5_CD],
[CCLF_5_PartB_Physicians].[CLM_DGNS_6_CD] AS [CLM_DGNS_6_CD],
[CCLF_5_PartB_Physicians].[CLM_DGNS_7_CD] AS [CLM_DGNS_7_CD],
[CCLF_5_PartB_Physicians].[CLM_DGNS_8_CD] AS [CLM_DGNS_8_CD],
[CCLF_5_PartB_Physicians].[fileDate] AS [fileDate],
[CCLF_5_PartB_Physicians].[CLM_FROM_DT_1ST] AS [CLM_FROM_DT_1ST],
[CCLF_5_PartB_Physicians].[ORG_HIC_NUM] AS [ORG_HIC_NUM],
[CCLF_5_PartB_Physicians].[SourceFeed] AS [SourceFeed],
[CCLF_5_PartB_Physicians].[CLM_LINE_SRVC_UNIT_QTY] AS [CLM_LINE_SRVC_UNIT_QTY],
[CCLF_5_PartB_Physicians].[LbPatientId] AS [LbPatientId (CCLF_5_PartB_Physicians)],
[CCLF_5_PartB_Physicians].[CLM_PBP_INCLSN_AMT] AS [CLM_PBP_INCLSN_AMT],
[CCLF_5_PartB_Physicians].[CLM_PBP_RDCTN_AMT] AS [CLM_PBP_RDCTN_AMT],
[BETOS_CPT].[HCPC] AS [HCPC],
[BETOS_CPT].[BETOSGroupID] AS [BETOSGroupID],
[BETOS_GROUPS].[GroupID] AS [GroupID],
[BETOS_GROUPS].[Cat1] AS [Cat1],
[BETOS_GROUPS].[Cat2] AS [Cat2],
[BETOS_GROUPS].[Cat3] AS [Cat3],
[CLAIM_TYPE_CODES].[ID] AS [ID1],
[CLAIM_TYPE_CODES].[CLM_TYPE_CD] AS [CLM_TYPE_CD (CLAIM_TYPE_CODES)],
[CLAIM_TYPE_CODES].[CLAIM_TYPE_DESC] AS [CLAIM_TYPE_DESC],
[ProcedureCode].[ProcedureCode] AS [cpt4_code_id],
[ProcedureCode].[ProcedureDescription] AS [CPTDescription],
[ProcedureCode].[ProcedureDescription] AS [description_medium],
[ProcedureCode].[ProcedureDescriptionAlternate] AS [description_friendly],
[ProcedureCode].[deleteind] AS [delete_ind],
[ProcedureCode].[deleteind] AS [active_ind],
[ICD9Mstr].[ICD9] AS [ICD9],
[ICD9Mstr].[ShortDescription] AS [ShortDescription],
[ICD9Mstr].[LongDescription] AS [LongDescription],
[ICD9Mstr].[DeleteInd] AS [DeleteInd],
[ICD9Mstr].[CreateDateTime] AS [CreateDateTime],
[ICD9Mstr].[ModifyDateTime] AS [ModifyDateTime],
[ICD9Mstr].[CreateLBUserId] AS [CreateLBUserId],
[ICD9Mstr].[ModifyLBUserId] AS [ModifyLBUserId],
[NPI_Lookup].[ID] AS [ID (NPI_Lookup)],
[NPI_Lookup].[NPI] AS [NPI],
[NPI_Lookup].[NAME] AS [NAME],
[NPI_Lookup].[ACO] AS [ACO],
[NPI_Lookup].[adhoc_provider] AS [adhoc_provider],
[NPI_Lookup].[State] AS [State (NPI_Lookup)],
[NPI_Lookup].[Taxonomy] AS [Taxonomy],
[NPI_Lookup].[Specialty] AS [Specialty],
[NPI_Lookup].[City] AS [City (NPI_Lookup)],
[NPI_Lookup].[Zip] AS [Zip (NPI_Lookup)],
[NPI_Lookup].[GeoCodeLong] AS [GeoCodeLong (NPI_Lookup)],
[NPI_Lookup].[GeoCodeLat] AS [GeoCodeLat (NPI_Lookup)],
[OrgHierarchy].[ID] AS [ID (OrgHierarchy)],
[OrgHierarchy].[State] AS [State],
[OrgHierarchy].[City] AS [City],
[OrgHierarchy].[Zip] AS [Zip],
[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
[OrgHierarchy].[MemberID] AS [MemberID],
[OrgHierarchy].[MemberFullName] AS [MemberFullName],
[OrgHierarchy].[Member_Provider_Relationship_Effective_Date] AS [Member_Provider_Relationship_Effective_Date],
[OrgHierarchy].[Member_Provider_Relationship_Termination_Date] AS [Member_Provider_Relationship_Termination_Date],
[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM (OrgHierarchy)],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
[OrgHierarchy].[BirthDate] AS [BirthDate],
[OrgHierarchy].[GenderId] AS [GenderId],
[OrgHierarchy].[County] AS [County],
[OrgHierarchy].[LastUpdatedTimestamp] AS [LastUpdatedTimestamp],
[OrgHierarchy].[LbPatientId] AS [LbPatientId],
[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
[OrgHierarchy].[PatientId1] AS [PatientId1],
[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
[OrgHierarchy].[PatientId2] AS [PatientId2],
[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
[OrgHierarchy].[PatientId3] AS [PatientId3],
[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
[OrgHierarchy].[PatientId4] AS [PatientId4],
[SPECIALTY_CODES].[Code] AS [Code], 
[SPECIALTY_CODES].[Description] AS [Specialty Description]
FROM [dbo].[CCLF_5_PartB_Physicians] [CCLF_5_PartB_Physicians]
LEFT JOIN [dbo].[BETOS_CPT] [BETOS_CPT] ON ([CCLF_5_PartB_Physicians].[CLM_LINE_HCPCS_CD] = [BETOS_CPT].[HCPC])
LEFT JOIN [dbo].[BETOS_GROUPS] [BETOS_GROUPS] ON ([BETOS_CPT].[BETOSGroupID] = [BETOS_GROUPS].[GroupID])
LEFT JOIN [dbo].[CLAIM_TYPE_CODES] [CLAIM_TYPE_CODES] ON ([CCLF_5_PartB_Physicians].[CLM_TYPE_CD] = [CLAIM_TYPE_CODES].[CLM_TYPE_CD])
--LEFT JOIN [dbo].[CPT4Mstr] [CPT4Mstr] ON ([CCLF_5_PartB_Physicians].[CLM_LINE_HCPCS_CD] = [CPT4Mstr].[cpt4_code_id])
LEFT JOIN [dbo].[ProcedureCode] ON ([CCLF_5_PartB_Physicians].[CLM_LINE_HCPCS_CD] = [ProcedureCode].[ProcedureCode]
AND [ProcedureCode].[ProcedureCodingSystemTypeId]=(SELECT TOP 1 [ProcedureCodingSystemTypeID] FROM [dbo].[ProcedureCodingSystemType]
WHERE LEFT([Name], 3) ='CPT')
)
LEFT JOIN [dbo].[ICD9Mstr] [ICD9Mstr] ON [CCLF_5_PartB_Physicians].[CLM_LINE_DGNS_CD] = [ICD9Mstr].[ICD9]
           and CASE --CALCULATE ICD TYPE
                    WHEN try_convert(int,[CCLF_5_PartB_Physicians].ICD_TYPE) = 9 then try_convert(int,[CCLF_5_PartB_Physicians].ICD_TYPE)
                    when try_convert(int,[CCLF_5_PartB_Physicians].ICD_TYPE) = 10 then try_convert(int,[CCLF_5_PartB_Physicians].ICD_TYPE)
                    when try_convert(int,[CCLF_5_PartB_Physicians].ICD_TYPE) = 0 then 10
                    WHEN [CCLF_5_PartB_Physicians].CLM_FROM_DT < '10/1/2015' THEN 9
                    ELSE 10
                  END
                = [ICD9Mstr].icdtype  
LEFT JOIN [dbo].[NPI_Lookup] [NPI_Lookup] ON ([CCLF_5_PartB_Physicians].[RNDRG_PRVDR_NPI_NUM] = [NPI_Lookup].[NPI])
INNER JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([CCLF_5_PartB_Physicians].[LbPatientId] = [OrgHierarchy].[LbPatientId])
LEFT JOIN [dbo].[SPECIALTY_CODES] [SPECIALTY_CODES] ON ([CCLF_5_PartB_Physicians].[CLM_PRVDR_SPCLTY_CD] = [SPECIALTY_CODES].[Code])
GO
