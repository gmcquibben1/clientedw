SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	CREATE VIEW [dbo].[vwHEDISValueSet]
	AS
	select [ValueSetName], [Code]	,[CodeSystem], Year
	FROM [EdwReferenceData].[dbo].[HEDISValueSetCode] WITH (NOLOCK)
	UNION
	select [ValueSetName], [Code]	,[CodeSystem], Year
	FROM [EdwReferenceData].[dbo].CustomValueSet WITH (NOLOCK)
	WHERE MeasureSteward = 'HEDIS'
	AND EnabledInd = 1 AND deleteind = 0 AND LightBeamInd = 1
	UNION
	SELECT ValueSetName, CodeSystem, Code, Year 
	FROM CustomValueSet WITH (NOLOCK)
	WHERE  MeasureSteward = 'HEDIS'
	AND EnabledInd = 1 and deleteind = 0
GO
