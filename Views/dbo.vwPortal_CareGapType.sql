SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_CareGapType] AS
SELECT * FROM [Version22LbportalTemplate].dbo.CareGapType 
WHERE db_name() = 'Version22EdwTemplate' 
GO
