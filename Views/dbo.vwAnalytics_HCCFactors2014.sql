SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_HCCFactors2014]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [HCCFactors2014].[ID] AS [ID],
[HCCFactors2014].[Variable] AS [Variable],
[HCCFactors2014].[HCCNo] AS [HCCNo],
[HCCFactors2014].[Disease Group] AS [Disease Group],
[HCCFactors2014].[2013 Factor] AS [2013 Factor],
[HCCFactors2014].[2014 Factor] AS [2014 Factor],
[HCCFactors2014].[Avg_BidRate] AS [Avg_BidRate],
[HCCFactors2014].[Normalized_Score] AS [Normalized_Score],
[HCCFactors2014].[Coding_Intensity] AS [Coding_Intensity],
[HCCFactors2014].[New_Rate] AS [New_Rate],
[HCC_Diagnosis].[ID] AS [ID (HCC_Diagnosis)],
[HCC_Diagnosis].[DIAGNOSIS CODE] AS [DIAGNOSIS CODE],
[HCC_Diagnosis].[SHORT DESCRIPTION] AS [SHORT DESCRIPTION],
[HCC_Diagnosis].[2013HCC] AS [2013HCC],
[HCC_Diagnosis].[CMS-HCC PACE/ESRD Model Category] AS [CMS-HCC PACE/ESRD Model Category],
[HCC_Diagnosis].[2014HCC] AS [2014HCC],
[HCC_Diagnosis].[RxHCC Model Category] AS [RxHCC Model Category],
[HCC_Diagnosis].[2013 CMS-HCC Model for 2014 Payment Year] AS [2013 CMS-HCC Model for 2014 Payment Year],
[HCC_Diagnosis].[CMS-HCC PACE/ESRD Model for 2014 Payment Year] AS [CMS-HCC PACE/ESRD Model for 2014 Payment Year],
[HCC_Diagnosis].[2014 CMS-HCC Model for 2014 Payment Year] AS [2014 CMS-HCC Model for 2014 Payment Year],
[HCC_Diagnosis].[RxHCC Model for 2014 Payment Year] AS [RxHCC Model for 2014 Payment Year]
FROM [dbo].[HCCFactors2014] [HCCFactors2014]
INNER JOIN [dbo].[HCC_Diagnosis] [HCC_Diagnosis] ON ([HCCFactors2014].[HCCNo] = [HCC_Diagnosis].[2014HCC])
GO
