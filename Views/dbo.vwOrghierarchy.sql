SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =====================================================================================================================
-- Author:		Lan
-- Create date: 2016-09-27
-- Description: This view replaces the physical table Orghierarchy
-- 
-- Modified By: 
-- Modified Date: 
-- Description:	
-- =====================================================================================================================

CREATE VIEW [dbo].[vwOrghierarchy]
AS 
SELECT 
t1.patientID as LbPatientId,
t10.[Name] AS [State], 
t9.City , 
t9.[PostalCode],
t2.[level1_buCode],
t2.[level1_buName],
t2.[level1_buType],
t2.[level2_buCode],
t2.[level2_buName],
t2.[level2_buType],
t2.[level3_buCode],
t2.[level3_buName],
t2.[level3_buType],
t2.[level4_buCode],
t2.[level4_buName],
t2.[level4_buType],
t2.[level5_buCode],
t2.[level5_buName],
t2.[level5_buType],
t2.[level6_buCode],
t2.[level6_buName],
t2.[level6_buType],
LTRIM(RTRIM(t11.[ContractName]))  ContractName,
t1.medicareNo,
LTRIM(RTRIM(t6.lastName)) + ', ' + LTRIM(RTRIM(t6.firstName)) MemberFullName,
t11.[ContractStatus] + ' (' + [ClaimStatus] + ')' Patient_Status,
COALESCE(t3.ProviderID,0) AS providerID,
COALESCE(LTRIM(RTRIM(t4.[LastName]))  + ', ' + LTRIM(RTRIM(t4.[FirstName])), 'unattributed') AS ProviderName,
COALESCE(t3.[NationalProviderIdentifier], '9999999999') AS ProviderNPI,
t7.BirthDate, 
t7.[GenderTypeID], 
GETDATE() LastUpdatedTimestamp 
--INTO #OrgHierarchy
FROM [dbo].[patientProviderOrgHierarchy] t1
JOIN [dbo].[orgHierarchyStructure] t2
ON t1.[BusinessUnitId] = t2.[buId]
LEFT JOIN [dbo].[vwPortal_Provider] t3
ON t1.ProviderID = t3.ProviderID
AND t3.[DeleteInd] = 0
LEFT JOIN [dbo].[vwPortal_IndividualName] t4
ON t3.[IndividualID] = t4.IndividualID
AND t4.DeleteInd = 0
LEFT JOIN [dbo].[vwPortal_Patient] t5
ON t1.PatientID = t5.PatientId
AND t5.[DeleteInd] = 0
Left JOIN [dbo].[vwPortal_IndividualName] t6
ON t5.[IndividualID] = t6.IndividualID
and t6.DeleteInd = 0
LEFT JOIN [dbo].[vwPortal_Individual] t7
ON t5.IndividualID = t7.IndividualID
And t7.DeleteInd = 0
LEFT JOIN 
 (SELECT IndividualID, MAX(StreetAddressID) AS StreetAddressID
 FROM [dbo].[vwPortal_IndividualStreetAddress]
 WHERE DeleteInd = 0 
 GROUP BY IndividualID ) t8
 ON t7.IndividualID = t8.IndividualID
Left JOIN [dbo].[vwPortal_StreetAddress] t9
ON t8.StreetAddressID = t9.StreetAddressID
And t9.DeleteInd = 0
LEFT JOIN [dbo].[vwPortal_TerritoryType] t10
ON t9.[TerritoryTypeID] = t10.[TerritoryTypeID]
LEFT JOIN [dbo].[PatientContract] t11
ON t1.[PatientID] = t11.[LbPatientId]
AND [IsCurrentInd] = 1
LEFT OUTER JOIN PatientIdType t12 
ON t1.PatientId=t12.PatientId
GO
