SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientEncounter] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientEncounter
WHERE db_name() = 'Version22EdwTemplate' 
GO
