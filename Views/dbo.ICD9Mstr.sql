SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[ICD9Mstr] as 
-- ==========================================================
-- Author:    Mike Hoxter
-- Create date:  08/24/2016
-- Description:  View of ICD9Mstr table to Edw Reference data, ensuring only one value per ICD9/icdtype combo
-- ===========================================================
SELECT [ICDCode] as [ICD9]
      ,[LONGDESCRIPTION] as [LongDescription]
      ,[SHORTDESCRIPTION]as [ShortDescription]
      ,[DeleteInd]
      ,[CreateLBUserId]
	  ,[ModifyLBUserId]
       ,[CreateDateTime]
	   ,[ModifyDateTime]
	   ,[ICD9MstrId]
	   ,[icdtype]
  FROM (
  SELECT *, ROW_NUMBER() OVER (PARTITION BY ICDCode, icdtype ORDER BY ICD9MstrId DESC) AS RANK
  FROM
  [EdwReferenceData].[dbo].[ICDMstr]) ICDs
  WHERE RANK = 1
GO
