SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientAllergy] 
AS 

SELECT pa.PatientId, 
	pa.SourceSystemId, 
	pa.AllergyTypeId, 
	atype.DisplayValue as AllergyType, 
	pa.AllergyDescription,
	pa.AllergySeverityTypeID,
	asev.DisplayValue as AllergySeverity, 
	pa.AllergyReactionTypeID,
	arx.DisplayValue as AllergyReaction, 
	pa.AllergyStatusTypeId,
	astat.DisplayValue as AllergyStatus, 
	pa.AllergyComment,
	pa.EncounterID,
	pa.ActiveInd,
	pa.CreateDateTime,
	pa.ModifyDateTime

FROM PatientAllergy pa 
	 LEFT OUTER JOIN AllergyType atype ON atype.AllergyTypeId = pa.AllergyTypeId
	 LEFT OUTER JOIN AllergySeverityType asev ON asev.AllergySeverityTypeId = pa.AllergySeverityTypeId
	 LEFT OUTER JOIN AllergyReactionType arx ON arx.AllergyReactionTypeId = pa.AllergyReactionTypeId
	 LEFT OUTER JOIN AllergyStatusType astat ON astat.AllergyStatusTypeId = pa.AllergyStatusTypeId
WHERE pa.DeleteInd = 0 


GO
