SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_FileProcessHistory] AS
SELECT * FROM [Version22LbportalTemplate].dbo.FileProcessHistory
WHERE db_name() = 'Version22EdwTemplate' 
GO
