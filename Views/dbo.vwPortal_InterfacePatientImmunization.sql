SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientImmunization] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientImmunization
WHERE db_name() = 'Version22EdwTemplate' 
GO
