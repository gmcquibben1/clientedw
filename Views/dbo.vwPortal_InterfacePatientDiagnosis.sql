SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientDiagnosis] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientDiagnosis
WHERE db_name() = 'Version22EdwTemplate' 
GO
