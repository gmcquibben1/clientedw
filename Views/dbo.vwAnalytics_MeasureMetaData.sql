SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_MeasureMetaData]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [CMeasure_Definitions].[MeasureID] AS [MeasureID],
[CMeasure_Definitions].[MeasureProgram] AS [MeasureProgram],
[CMeasure_Definitions].[MeasureName] AS [MeasureName],
[CMeasure_Definitions].[MeasureAbbr] AS [MeasureAbbr],
[CMeasure_Definitions].[MeasureCategory] AS [MeasureCategory],
[CMeasure_Definitions].[MeasureSubcategory] AS [MeasureSubcategory],
[CMeasure_Definitions].[HighLow] AS [HighLow],
[CMeasure_Definitions].[StoredProcedure] AS [StoredProcedure],
[CMeasure_Definitions].[EnableFlag] AS [EnableFlag],
[CMeasure_Definitions].[MeasureContractID] AS [MeasureContractID],
[CMeasure_Definitions].[EnforceEligibilty] AS [EnforceEligibilty],
[CMeasure_LinkedContracts].[LinkedContractId] AS [LinkedContractId],
[CMeasure_LinkedContracts].[MeasureContractId] AS [MeasureContractId1],
[CMeasure_LinkedContracts].[ContractId] AS [ContractId],
[CMeasure_ContractNames].[ContractId] AS [ContractId (CMeasure_ContractNames)],
[CMeasure_ContractNames].[ContractName] AS [ContractName],
[OrgHierarchy].[ID] AS [ID],
[OrgHierarchy].[State] AS [State],
[OrgHierarchy].[City] AS [City],
[OrgHierarchy].[Zip] AS [Zip],
[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName (OrgHierarchy)],
[OrgHierarchy].[MemberID] AS [MemberID],
[OrgHierarchy].[MemberFullName] AS [MemberFullName],
[OrgHierarchy].[Member_Provider_Relationship_Effective_Date] AS [Member_Provider_Relationship_Effective_Date],
[OrgHierarchy].[Member_Provider_Relationship_Termination_Date] AS [Member_Provider_Relationship_Termination_Date],
[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
[OrgHierarchy].[BirthDate] AS [BirthDate],
[OrgHierarchy].[GenderId] AS [GenderId],
[OrgHierarchy].[County] AS [County],
[OrgHierarchy].[LastUpdatedTimestamp] AS [LastUpdatedTimestamp],
[OrgHierarchy].[LbPatientId] AS [LbPatientId],
[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
[OrgHierarchy].[PatientId1] AS [PatientId1],
[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
[OrgHierarchy].[PatientId2] AS [PatientId2],
[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
[OrgHierarchy].[PatientId3] AS [PatientId3],
[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
[OrgHierarchy].[PatientId4] AS [PatientId4]
FROM [dbo].[CMeasure_Definitions] [CMeasure_Definitions]
INNER JOIN [dbo].[CMeasure_LinkedContracts] [CMeasure_LinkedContracts] ON ([CMeasure_Definitions].[MeasureContractID] = [CMeasure_LinkedContracts].[MeasureContractId])
INNER JOIN [dbo].[CMeasure_ContractNames] [CMeasure_ContractNames] ON ([CMeasure_LinkedContracts].[ContractId] = [CMeasure_ContractNames].[ContractId])
RIGHT JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([CMeasure_ContractNames].[ContractName] = [OrgHierarchy].[ContractName])
GO
