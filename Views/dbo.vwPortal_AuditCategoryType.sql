SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_AuditCategoryType] AS
SELECT * FROM [Version22LbportalTemplate].dbo.AuditCategoryType
WHERE db_name() = 'Version22EdwTemplate' 
GO
