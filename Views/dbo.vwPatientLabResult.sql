SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientLabResult]
AS 

SELECT
	plo.PatientId,
	plr.PatientLabOrderId,
	plr.ObservationCode1,
	plr.ObservationDescription1,
	plr.ObservationCodingSystemName1,
	plr.ObservationCode2,
	plr.ObservationDescription2,
	plr.ObservationCodingSystemName2,
	plr.Value,
	plr.Units,
	plr.ReferenceRange,
	plr.AbnormalFlag,
	plr.ResultStatus,
	plr.ResultComment,
	plr.ObservationDate,
	plr.CreateDateTime,
	plr.ModifyDateTime,
	plr.CreateLBUserId,
	plr.ModifyLBUserId
FROM PatientLabResult plr
INNER JOIN PatientLabOrder plo ON plo.PatientLabOrderId = plr.PatientLabOrderId
	
	   
	
GO
