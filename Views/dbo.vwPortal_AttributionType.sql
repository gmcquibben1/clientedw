SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_AttributionType] AS
SELECT * FROM [Version22LbportalTemplate].dbo.AttributionType
WHERE db_name() = 'Version22EdwTemplate' 
GO
