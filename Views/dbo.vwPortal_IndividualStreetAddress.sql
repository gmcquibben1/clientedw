SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_IndividualStreetAddress] AS
SELECT * FROM [Version22LbportalTemplate].dbo.IndividualStreetAddress 
WHERE db_name() = 'Version22EdwTemplate' 
GO
