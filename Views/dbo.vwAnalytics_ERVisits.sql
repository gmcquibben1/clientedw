SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_ERVisits]
		AS 
		-- ==========================================================
		-- Author:    Ismail
		-- Create date:  08/24/2016
		-- Description:  View of Analytics ERVisits Workbook
		-- ===========================================================
		----WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
		--Updated 1/6/2017 Will - Modified to work with new CCLF logic
	SELECT 
		[OrgHierarchy].[State] AS [State],
		[OrgHierarchy].[City] AS [City],
		[OrgHierarchy].[Zip] AS [Zip],
		[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
		[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
		[OrgHierarchy].[Level1Id] AS [Level1Id],
		[OrgHierarchy].[Level1Name] AS [Level1Name],
		[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
		[OrgHierarchy].[Level2Id] AS [Level2Id],
		[OrgHierarchy].[Level2Name] AS [Level2Name],
		[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
		[OrgHierarchy].[Level3Id] AS [Level3Id],
		[OrgHierarchy].[Level3Name] AS [Level3Name],
		[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
		[OrgHierarchy].[Level4Id] AS [Level4Id],
		[OrgHierarchy].[Level4Name] AS [Level4Name],
		[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
		[OrgHierarchy].[Level5Id] AS [Level5Id],
		[OrgHierarchy].[Level5Name] AS [Level5Name],
		[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
		[OrgHierarchy].[Level6Id] AS [Level6Id],
		[OrgHierarchy].[Level6Name] AS [Level6Name],
		[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
		[OrgHierarchy].[ContractName] AS [ContractName],
		[OrgHierarchy].[MemberID] AS [MemberID],
		[OrgHierarchy].[MemberFullName] AS [MemberFullName],
		[OrgHierarchy].[Member_Provider_Relationship_Effective_Date] AS [Member_Provider_Relationship_Effective_Date],
		[OrgHierarchy].[Member_Provider_Relationship_Termination_Date] AS [Member_Provider_Relationship_Termination_Date],
		[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
		[OrgHierarchy].[Patient_Status] AS [Patient_Status],
		[OrgHierarchy].[ProviderId] AS [ProviderId],
		[OrgHierarchy].[ProviderName] AS [ProviderName],
		[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
		[OrgHierarchy].[BirthDate] AS [BirthDate],
		[OrgHierarchy].[GenderId] AS [GenderId],
		[OrgHierarchy].[County] AS [County],
		[OrgHierarchy].[LastUpdatedTimestamp] AS [LastUpdatedTimestamp],
		[OrgHierarchy].[LbPatientId] AS [LbPatientId],
		[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
		[OrgHierarchy].[PatientId1] AS [PatientId1],
		[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
		[OrgHierarchy].[PatientId2] AS [PatientId2],
		[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
		[OrgHierarchy].[PatientId3] AS [PatientId3],
		[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
		[OrgHierarchy].[PatientId4] AS [PatientId4],
		[CCLF_1_PartA_Header].[id] AS [id1],
		[CCLF_1_PartA_Header].[CUR_CLM_UNIQ_ID] AS [CUR_CLM_UNIQ_ID],
		[CCLF_1_PartA_Header].[PREV_CLM_UNIQ_ID] AS [PREV_CLM_UNIQ_ID],
		[CCLF_1_PartA_Header].[PRVDR_OSCAR_NUM] AS [PRVDR_OSCAR_NUM],
		[CCLF_1_PartA_Header].[BENE_HIC_NUM] AS [CCLF_1_PartA_Header_BENE_HIC_NUM],
		[CCLF_1_PartA_Header].[CLM_TYPE_CD] AS [CLM_TYPE_CD],
		[CCLF_1_PartA_Header].[CLM_FROM_DT] AS [CLM_FROM_DT],
		[CCLF_1_PartA_Header].[CLM_THRU_DT] AS [CLM_THRU_DT],
		[CCLF_1_PartA_Header].[CLM_BILL_FAC_TYPE_CD] AS [CLM_BILL_FAC_TYPE_CD],
		[CCLF_1_PartA_Header].[CLM_BILL_CLSFCTN_CD] AS [CLM_BILL_CLSFCTN_CD],
		[CCLF_1_PartA_Header].[PRNCPL_DGNS_CD] AS [PRNCPL_DGNS_CD],
		[CCLF_1_PartA_Header].[ADMTG_DGNS_CD] AS [ADMTG_DGNS_CD],
		[CCLF_1_PartA_Header].[CLM_MDCR_NPMT_RSN_CD] AS [CLM_MDCR_NPMT_RSN_CD],
		[CCLF_0_PartA_CLM_TYPES].[ERTotalpaid] AS [CLM_PMT_AMT],
		[CCLF_1_PartA_Header].[CLM_NCH_PRMRY_PYR_CD] AS [CLM_NCH_PRMRY_PYR_CD],
		[CCLF_1_PartA_Header].[PRVDR_FAC_FIPS_ST_CD] AS [PRVDR_FAC_FIPS_ST_CD],
		[CCLF_1_PartA_Header].[BENE_PTNT_STUS_CD] AS [BENE_PTNT_STUS_CD],
		[CCLF_1_PartA_Header].[DGNS_DRG_CD] AS [DGNS_DRG_CD],
		[CCLF_1_PartA_Header].[CLM_OP_SRVC_TYPE_CD] AS [CLM_OP_SRVC_TYPE_CD],
		[CCLF_1_PartA_Header].[FAC_PRVDR_NPI_NUM] AS [FAC_PRVDR_NPI_NUM],
		[CCLF_1_PartA_Header].[OPRTG_PRVDR_NPI_NUM] AS [OPRTG_PRVDR_NPI_NUM],
		[CCLF_1_PartA_Header].[ATNDG_PRVDR_NPI_NUM] AS [ATNDG_PRVDR_NPI_NUM],
		[CCLF_1_PartA_Header].[OTHR_PRVDR_NPI_NUM] AS [OTHR_PRVDR_NPI_NUM],
		[CCLF_1_PartA_Header].[CLM_ADJSMT_TYPE_CD] AS [CLM_ADJSMT_TYPE_CD],
		[CCLF_1_PartA_Header].[CLM_EFCTV_DT] AS [CLM_EFCTV_DT],
		[CCLF_1_PartA_Header].[CLM_IDR_LD_DT] AS [CLM_IDR_LD_DT],
		[CCLF_1_PartA_Header].[BENE_EQTBL_BIC_HICN_NUM] AS [BENE_EQTBL_BIC_HICN_NUM],
		[CCLF_1_PartA_Header].[CLM_ADMSN_TYPE_CD] AS [CLM_ADMSN_TYPE_CD],
		[CCLF_1_PartA_Header].[CLM_ADMSN_SRC_CD] AS [CLM_ADMSN_SRC_CD],
		[CCLF_1_PartA_Header].[CLM_BILL_FREQ_CD] AS [CLM_BILL_FREQ_CD],
		[CCLF_1_PartA_Header].[CLM_QUERY_CD] AS [CLM_QUERY_CD],
		[CCLF_1_PartA_Header].[fileDate] AS [fileDate],
		[CCLF_1_PartA_Header].[CLM_FROM_DT_1ST] AS [CLM_FROM_DT_1ST],
		[CCLF_1_PartA_Header].[ORG_HIC_NUM] AS [ORG_HIC_NUM],
		[CCLF_1_PartA_Header].[SourceFeed] AS [SourceFeed],
		[CCLF_1_PartA_Header].[LbPatientId] AS [LbPatientId (CCLF_1_PartA_Header)],
		[CCLF_1_PartA_Header].[ICD_TYPE] AS [ICD_TYPE],
		[CCLF_1_PartA_Header].[CLM_PBP_INCLSN_AMT] AS [CLM_PBP_INCLSN_AMT],
		[CCLF_1_PartA_Header].[CLM_PBP_RDCTN_AMT] AS [CLM_PBP_RDCTN_AMT],
		[CCLF_0_PartA_CLM_TYPES].[CUR_CLM_UNIQ_ID] AS [CCLF_0_PartA_CLM_TYPES_CUR_CLM_UNIQ_ID],
		try_convert(int,[CCLF_0_PartA_CLM_TYPES].[ER]) AS [ER],
		try_convert(int,[CCLF_0_PartA_CLM_TYPES].[INP]) AS [INP],
		NULL AS [ER_INP],
		try_convert(int,[CCLF_0_PartA_CLM_TYPES].[SNF]) AS [SNF],
		try_convert(int,[CCLF_0_PartA_CLM_TYPES].[HOSPICE]) AS [HOSPICE],
		try_convert(int,[CCLF_0_PartA_CLM_TYPES].[CHRONIC]) AS [CHRONIC],
		[CCLF_0_PartA_CLM_TYPES].[LOS] AS [LOS],
		[Facility_Lookup].[NPI] AS [NPI],
		[Facility_Lookup].[NAME] AS [NAME],
		[Facility_Lookup].[ACO] AS [ACO],
		[Facility_Lookup].[adhoc_provider] AS [adhoc_provider],
		[Facility_Lookup].[Taxonomy] AS [Taxonomy],
		[Facility_Lookup].[Specialty] AS [Specialty],
		[Facility_Lookup].[State] AS [State (Facility_Lookup)],
		[Facility_Lookup].[City] AS [City (Facility_Lookup)],
		[Facility_Lookup].[Zip] AS [Zip (Facility_Lookup)],
		[Facility_Lookup].[GeoCodeLong] AS [GeoCodeLong (Facility_Lookup)],
		[Facility_Lookup].[GeoCodeLat] AS [GeoCodeLat (Facility_Lookup)],
		[Attending_Lookup].[NPI] AS [NPI_Lookup_NPI],
		[Attending_Lookup].[NAME] AS [NPI_Lookup_NAME],
		[Attending_Lookup].[ACO] AS [NPI_Lookup_ACO],
		[Attending_Lookup].[adhoc_provider] AS [NPI_Lookup_adhoc_provider],
		[Attending_Lookup].[Taxonomy] AS [Taxonomy (Attending_Lookup)],
		[Attending_Lookup].[Specialty] AS [Specialty (Attending_Lookup)],
		[Attending_Lookup].[State] AS [State (Attending_Lookup)],
		[Attending_Lookup].[City] AS [City (Attending_Lookup)],
		[Attending_Lookup].[Zip] AS [Zip (Attending_Lookup)],
		[Attending_Lookup].[GeoCodeLong] AS [GeoCodeLong (Attending_Lookup)],
		[Attending_Lookup].[GeoCodeLat] AS [GeoCodeLat (Attending_Lookup)],
		[DiagnosisGroup].[DiagnosisCode] AS [DiagnosisCode (DiagnosisGroup)],
		[DiagnosisGroup].[Level1Id] AS [Level1Id (DiagnosisGroup)],
		[DiagnosisGroup].[Level1] AS [Level1 (DiagnosisGroup)],
		[DiagnosisGroup].[Level2Id] AS [Level2Id (DiagnosisGroup)],
		[DiagnosisGroup].[Level2] AS [Level2 (DiagnosisGroup)],
		[DiagnosisGroup].[Level3Id] AS [Level3Id (DiagnosisGroup)],
		[DiagnosisGroup].[Level3] AS [Level3 (DiagnosisGroup)],
		[ICD9Mstr].[ICD9] AS [ICD9],
		[ICD9Mstr].[ShortDescription] AS [ShortDescription],
		[ICD9Mstr].[LongDescription] AS [LongDescription]
	FROM [dbo].[OrgHierarchy] [OrgHierarchy]
		INNER JOIN [dbo].[CCLF_1_PartA_Header] [CCLF_1_PartA_Header] ON ([OrgHierarchy].[LbPatientId] = [CCLF_1_PartA_Header].[LbPatientId])
		INNER JOIN [dbo].[CCLF_0_PartA_CLM_TYPES] [CCLF_0_PartA_CLM_TYPES] ON ([CCLF_1_PartA_Header].[CUR_CLM_UNIQ_ID] = [CCLF_0_PartA_CLM_TYPES].[CUR_CLM_UNIQ_ID])
		LEFT JOIN [dbo].[NPI_Lookup] [Facility_Lookup] ON ([CCLF_1_PartA_Header].[FAC_PRVDR_NPI_NUM] = [Facility_Lookup].[NPI])
		LEFT JOIN [dbo].[NPI_Lookup] [Attending_Lookup] ON ([CCLF_1_PartA_Header].[ATNDG_PRVDR_NPI_NUM] = [Attending_Lookup].[NPI])
		LEFT JOIN [dbo].[DiagnosisGroup] [DiagnosisGroup] ON ([CCLF_1_PartA_Header].[PRNCPL_DGNS_CD] = [DiagnosisGroup].[DiagnosisCode])
		LEFT JOIN [dbo].[ICD9Mstr] [ICD9Mstr] ON [CCLF_1_PartA_Header].[PRNCPL_DGNS_CD] = [ICD9Mstr].[ICD9]
					and CASE --CALCULATE ICD TYPE
							WHEN try_convert(int,[CCLF_1_PartA_Header].ICD_TYPE) = 9 then try_convert(int,[CCLF_1_PartA_Header].ICD_TYPE)
							when try_convert(int,[CCLF_1_PartA_Header].ICD_TYPE) = 10 then try_convert(int,[CCLF_1_PartA_Header].ICD_TYPE)
							when try_convert(int,[CCLF_1_PartA_Header].ICD_TYPE) = 0 then 10
							WHEN [CCLF_1_PartA_Header].CLM_FROM_DT < '10/1/2015' THEN 9
							ELSE 10
						  END
						= [ICD9Mstr].icdtype
		--Added to make sure things line up between charts
	WHERE [CCLF_0_PartA_CLM_TYPES].[ERMainClaimInd] = 1

GO
