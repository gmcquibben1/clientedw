SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[CCLF_0_PartA_READMITS]
as select 
	[id] ,
	[CUR_CLM_UNIQ_ID] ,
	[CLM_FROM_DT],
	[CLM_THRU_DT],
	[LbPatientId],
	[SourceFeed] ,
	[PRNCPL_DGNS_CD] ,
	[ICD_TYPE],
	[CHRONIC],
	[30Day_ReAdmit],
	[60Day_ReAdmit] ,
	[90Day_ReAdmit],
	[LOS] 
	from CCLF_0_PartA_CLM_TYPES
	where [INP] =1
GO
