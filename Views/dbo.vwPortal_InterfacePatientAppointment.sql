SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientAppointment] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientAppointment 
WHERE db_name() = 'Version22EdwTemplate' 
GO
