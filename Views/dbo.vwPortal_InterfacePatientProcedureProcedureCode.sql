SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientProcedureProcedureCode] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientProcedureProcedureCode
WHERE db_name() = 'Version22EdwTemplate' 
GO
