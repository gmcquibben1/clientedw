SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientVitals]
AS 

SELECT 
	pv.PatientId, 
	pv.SourceSystemId, 
	pv.ServiceDateTime,
	pv.HeightCM,
	pv.HeightComments,
	pv.WeightKG,
	pv.WeightComments,
	pv.TemperatureCelcius,
	pv.TemperatureComments,
	pv.Pulse,
	pv.PulseComments,
	pv.Respiration,
	pv.RespirationComments,
	pv.BloodPressureSystolic,
	pv.BloodPressureDiastolic,
	pv.BloodPressureComment,
	pv.OxygenSaturationSP02,
	pv.OxygenSaturationComment,
	pv.Clinician,
	pv.PatientVitalsComment,
	pv.CreateDateTime,
	pv.ModifyDateTime
FROM PatientVitals pv
WHERE pv.DeleteInd = 0 
	
GO
