SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientClaimPartDPharmacy]
AS 
SELECT 
  AD.CUR_CLM_UNIQ_ID as [ClaimId], 
  IDR.LbPatientId as [PatientId],
  AD.CLM_LINE_NDC_CD as [MedicationId], 
  AD.CLM_LINE_FROM_DT as [FillDate], 
  AD.CLM_LINE_SRVC_UNIT_QTY as [QTY], 
  AD.CLM_LINE_DAYS_SUPLY_QTY as [DaySupply], 
  AD.CLM_SRVC_PRVDR_GNRC_ID_NUM as [PharmacyId], 
  AD.CLM_PRSBNG_PRVDR_GNRC_ID_NUM as [PrescriberId], 
  AD.CLM_LINE_BENE_PMT_AMT as [PaidAmt], 
  AD.CLM_ADJSMT_TYPE_CD as [AdjustmentTypeCode],
  AD.CLM_LINE_RX_FILL_NUM as [RxNum], 
  Rx.[NAME] as [PharmacyName], 
  DOC.[NAME] as [PrescriberName], 
  MED.MedName AS [MedicationName]
FROM CCLF_7_PartD AD
LEFT JOIN NPI_Lookup Rx ON Rx.NPI = AD.CLM_PRSBNG_PRVDR_GNRC_ID_NUM
LEFT JOIN NPI_Lookup DOC ON DOC.NPI = AD.CLM_SRVC_PRVDR_GNRC_ID_NUM
LEFT JOIN MedMstr MED ON AD.CLM_LINE_NDC_CD = MED.NDCId
JOIN PatientIdReference IDR ON AD.BENE_HIC_NUM = IDR.ExternalId
GO
