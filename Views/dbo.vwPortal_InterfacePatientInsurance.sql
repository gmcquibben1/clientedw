SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientInsurance] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientInsurance
WHERE db_name() = 'Version22EdwTemplate' 
GO
