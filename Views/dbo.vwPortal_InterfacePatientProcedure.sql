SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientProcedure] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientProcedure
WHERE db_name() = 'Version22EdwTemplate' 
GO
