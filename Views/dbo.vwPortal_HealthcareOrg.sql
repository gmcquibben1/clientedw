SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_HealthcareOrg] AS
SELECT * FROM [Version22LbportalTemplate].dbo.HealthcareOrg
WHERE db_name() = 'Version22EdwTemplate' 
GO
