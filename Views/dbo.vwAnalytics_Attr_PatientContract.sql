SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_Attr_PatientContract]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [PatientContract].[PatientContractId] AS [PatientContractId],
[PatientContract].[LbPatientId] AS [LbPatientId],
[PatientContract].[ContractName] AS [ContractName],
[PatientContract].[ContractId] AS [ContractId],
[PatientContract].[RunDateTime] AS [RunDateTime],
[PatientContract].[ContractStatus] AS [ContractStatus],
[PatientContract].[ClaimStatus] AS [ClaimStatus],
[PatientContract].[IsCurrentInd] AS [IsCurrentInd],
[PatientContract].[LbUserId] AS [LbUserId],
[PatientContract].[ContractSource] AS [ContractSource]
FROM [dbo].[PatientContract] [PatientContract]








GO
