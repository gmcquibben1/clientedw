SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_MeasureQADiag_HEDISValueSetCodes]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [HEDISValueSetCodes].[Value Set Name] AS [Value Set Name],
[HEDISValueSetCodes].[Value Set OID] AS [Value Set OID],
[HEDISValueSetCodes].[Value Set Version] AS [Value Set Version],
[HEDISValueSetCodes].[Code] AS [Code],
[HEDISValueSetCodes].[Definition] AS [Definition],
[HEDISValueSetCodes].[Code System] AS [Code System],
[HEDISValueSetCodes].[Code System OID] AS [Code System OID],
[HEDISValueSetCodes].[Code System Version] AS [Code System Version],
[HEDISValueSetCodes].[Code_Raw] AS [Code_Raw],
[HEDISValueSetCodes].[Source] AS [Source],
[HEDISValueSetCodes].[DeleteInd] AS [DeleteInd],
[HEDISValueSetCodes].[CreateDateTime] AS [CreateDateTime],
[HEDISValueSetCodes].[ModifyDateTime] AS [ModifyDateTime]
FROM [dbo].[HEDISValueSetCodes] [HEDISValueSetCodes]
GO
