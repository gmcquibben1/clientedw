SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =====================================================================================================================
-- Author:		Lan
-- Create date: 2016-06-01
-- Description: This view provides patient pharmacy input data for JHACG, which extracts patient pharmacy data from 
-- PatientMedication and JHACG_PatientListStaging( a subset data of Orghierarchy)
-- Modified By: 
-- Modified Date: 
-- Description:	
-- =====================================================================================================================

CREATE VIEW [dbo].[vwJhacgPatientPharmacyInput]
AS 
--SELECT 
--PM.PatientID AS patient_id, 
--PM.[MedicationStartDate] AS rx_fill_date, 
--CASE WHEN PM.NDCCode <> '' THEN PM.NDCCode ELSE PM.RxNormCode END AS rx_cd,
----CASE WHEN PM.NDCCode <> '' THEN 'NDCCode' ELSE 'RxNormCode' END AS rx_code_type,
--'N' AS rx_code_type,
----CASE WHEN TRY_CONVERT(FLOAT,PM.[Quantity]) IS NOT NULL THEN ROUND(PM.[Quantity], 0) ELSE 0 END AS rx_days_supply,
--CASE WHEN PATINDEX('%[^0-9]%',RTRIM(LTRIM([Quantity]))) > 0 THEN LEFT(RTRIM(LTRIM([Quantity])),PATINDEX('%[^0-9]%',RTRIM(LTRIM([Quantity])))-1)
--     ELSE 0
--END AS rx_days_supply,
--CAST(0.0 AS FLOAT) AS rx_cost
--FROM JhacgPatientListStaging P
--JOIN PatientMedication  PM
--ON P.LbPatientId =  PM.PatientID
--WHERE PM.[MedicationStartDate]  > DATEADD(MM,-16,GETUTCDATE())
--AND PM.[MedicationEndDate]    < DATEADD(MM,-4,GETUTCDATE())

SELECT 
P.[LbPatientId] AS patient_id, 
CLM_LINE_FROM_DT AS rx_fill_date,
CLM_LINE_NDC_CD AS rx_cd,
'N' AS rx_code_type,
ISNULL([CLM_LINE_DAYS_SUPLY_QTY],0) AS rx_days_supply,
CAST([CLM_LINE_BENE_PMT_AMT] AS FLOAT) AS rx_cost
FROM  JhacgPatientListStaging P
JOIN [dbo].[CCLF_7_PartD] PM
ON P.LbPatientId =  PM.LbPatientID
WHERE CLM_LINE_FROM_DT  > DATEADD(MM,-16,GETUTCDATE())
AND CLM_LINE_FROM_DT    < DATEADD(MM,-4,GETUTCDATE())
AND [CLM_ADJSMT_TYPE_CD] ='0'

GO
