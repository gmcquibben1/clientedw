SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientSocialHistory] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientSocialHistory
WHERE db_name() = 'Version22EdwTemplate' 
GO
