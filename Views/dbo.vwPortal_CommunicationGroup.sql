SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_CommunicationGroup] AS
SELECT * FROM [Version22LbportalTemplate].dbo.CommunicationGroup
WHERE db_name() = 'Version22EdwTemplate' 
GO
