SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[MedMstr]
as 
-- ==========================================================
-- Modified date:  11/28/2016
-- Description:  View of MedMstr
-- ===========================================================
SELECT x.*,
 0 as DeleteInd,
 getdate() as CreateDateTime, 
 getdate() as ModifyDateTime,
 1 as CreateLbUserId,
 1 as ModifyLbUserId
 FROM (
SELECT 
  1 as MedMstrId,
 NDCID as NDCId,
 MedicationLabelName as MedName,
 MedicationBrandName,
 TheraputicClassification,
 ClassName as TheraputicSubClassification,
 GenericInd, 
 RetiredInd,
 [Route], Dosage,
 row_number() over (partition by NDCID order by NDCID) as RANK
FROM FDBContent.dbo.Medication ) x
where RANK = 1
GO
