SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_ContractType] AS
SELECT * FROM [Version22LbportalTemplate].dbo.ContractType
WHERE db_name() = 'Version22EdwTemplate' 
GO
