SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 CREATE VIEW [dbo].[DiagnosisGroup]
				AS
				SELECT * FROM EdwReferenceData.dbo.DiagnosisGroup
GO
