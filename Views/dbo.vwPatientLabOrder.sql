SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientLabOrder]
AS 

SELECT 
	plo.PatientLabOrderId,
	plo.PatientId,
	plo.SourceSystemId,
	plo.OrderNumber,
	plo.ServiceId,
	plo.ServiceDescription,
	plo.ServiceCodingSystemName,
	plo.OrderingProviderId,
	plo.OrderingProviderIdType,
	plo.OrderingProviderName,
	plo.OrderDate,
	plo.CreateDateTime,
	plo.ModifyDateTime,
	plo.CreateLBUserId,
	plo.ModifyLBUserId
FROM PatientLabOrder plo 
	
	   
	
GO
