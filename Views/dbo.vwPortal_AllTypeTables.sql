SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_AllTypeTables] AS 
SELECT * FROM Version22LbportalTemplate.dbo.vwAllTypeTables
WHERE db_name() = 'Version22EdwTemplate' 
GO
