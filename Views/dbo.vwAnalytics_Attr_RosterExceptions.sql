SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_Attr_RosterExceptions]
----WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [PatientRosterEnrollment].[PatientRosterEnrollmentID] AS [PatientRosterEnrollmentID],
[PatientRosterEnrollment].[SourceSystemID] AS [SourceSystemID],
[PatientRosterEnrollment].[PatientExternalID] AS [PatientExternalID],
[PatientRosterEnrollment].[FirstName] AS [FirstName],
[PatientRosterEnrollment].[LastName] AS [LastName],
[PatientRosterEnrollment].[Gender] AS [Gender],
[PatientRosterEnrollment].[Birthdate] AS [Birthdate],
[PatientRosterEnrollment].[StartDate] AS [StartDate],
[PatientRosterEnrollment].[TermDate] AS [TermDate],
[PatientRosterEnrollment].[DeathDate] AS [DeathDate],
[PatientRosterEnrollment].[ProviderNPI] AS [ProviderNPI],
[PatientRosterEnrollment].[FileDate] AS [FileDate],
[PatientRosterEnrollment].[SourceFeed] AS [SourceFeed],
[PatientRosterEnrollment].[LbPatientId] AS [LbPatientId],
[PatientRosterEnrollment].[CreateDateTime] AS [CreateDateTime],
[vwPortal_ProviderActive].[ProviderID] AS [ProviderID],
[vwPortal_ProviderActive].[IndividualID] AS [IndividualID],
[vwPortal_ProviderActive].[SourceSystemID] AS [SourceSystemID (vwPortal_ProviderActive)],
[vwPortal_ProviderActive].[PartyTypeID] AS [PartyTypeID],
[vwPortal_ProviderActive].[ProviderRoleTypeID] AS [ProviderRoleTypeID],
[vwPortal_ProviderActive].[EducationDegreeTypeId] AS [EducationDegreeTypeId],
[vwPortal_ProviderActive].[ProviderSpecialtyTypeID] AS [ProviderSpecialtyTypeID],
[vwPortal_ProviderActive].[NationalProviderIdentifier] AS [NationalProviderIdentifier],
[vwPortal_ProviderActive].[ProviderStatusTypeID] AS [ProviderStatusTypeID],
[vwPortal_ProviderActive].[LBUserId] AS [LBUserId],
[vwPortal_ProviderActive].[DeleteInd] AS [DeleteInd],
[vwPortal_ProviderActive].[CreateDateTime] AS [CreateDateTime (vwPortal_ProviderActive)],
[vwPortal_ProviderActive].[ModifyDateTime] AS [ModifyDateTime],
[vwPortal_ProviderActive].[CreateLBUserId] AS [CreateLBUserId],
[vwPortal_ProviderActive].[ModifyLBUserId] AS [ModifyLBUserId],
[vwPortal_ProviderActive].[ExternalReferenceIdentifier] AS [ExternalReferenceIdentifier],
[vwPortal_ProviderActive].[OtherReferenceIdentifier] AS [OtherReferenceIdentifier],
[vwPortal_ProviderActive].[AutoFaceSheetPrintInd] AS [AutoFaceSheetPrintInd],
[vwPortal_ProviderActive].[HealthcareOrgDefaultInd] AS [HealthcareOrgDefaultInd],
[NPI_Lookup].[ID] AS [ID],
[NPI_Lookup].[NPI] AS [NPI],
[NPI_Lookup].[NAME] AS [NAME],
[NPI_Lookup].[ACO] AS [ACO],
[NPI_Lookup].[adhoc_provider] AS [adhoc_provider],
[NPI_Lookup].[Taxonomy] AS [Taxonomy],
[NPI_Lookup].[Specialty] AS [Specialty],
[NPI_Lookup].[State] AS [State],
[NPI_Lookup].[City] AS [City],
[NPI_Lookup].[Zip] AS [Zip],
[NPI_Lookup].[GeoCodeLong] AS [GeoCodeLong],
[NPI_Lookup].[GeoCodeLat] AS [GeoCodeLat]
FROM [dbo].[PatientRosterEnrollment] [PatientRosterEnrollment]
LEFT JOIN [dbo].[vwPortal_ProviderActive] [vwPortal_ProviderActive] ON ([PatientRosterEnrollment].[ProviderNPI] = [vwPortal_ProviderActive].[NationalProviderIdentifier])
LEFT JOIN [dbo].[NPI_Lookup] [NPI_Lookup] ON ([PatientRosterEnrollment].[ProviderNPI] = [NPI_Lookup].[NPI])
WHERE [vwPortal_ProviderActive].ProviderID is null
GO
