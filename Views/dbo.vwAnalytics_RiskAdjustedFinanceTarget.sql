SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_RiskAdjustedFinanceTarget]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
-- ==========================================================
-- Author:    Ismail
-- Create date:  08/26/2016
-- Description:  View of Analytics RiskAdjustedFinanceTarget Workbook
-- ===========================================================

SELECT 
[OrgHierarchy].[State] AS [State],
[OrgHierarchy].[City] AS [OrgHierarchy_City],
[OrgHierarchy].[Zip] AS [Zip],
[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
[OrgHierarchy].[MemberID] AS [MemberID],
[OrgHierarchy].[MemberFullName] AS [MemberFullName],[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
[OrgHierarchy].[BirthDate] AS [BirthDate],
[OrgHierarchy].[GenderId] AS [GenderId],
[OrgHierarchy].[County] AS [County],
[OrgHierarchy].[LbPatientId] AS [LbPatientId],
[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
[OrgHierarchy].[PatientId1] AS [PatientId1],
[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
[OrgHierarchy].[PatientId2] AS [PatientId2],
[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
[OrgHierarchy].[PatientId3] AS [PatientId3],
[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
[OrgHierarchy].[PatientId4] AS [PatientId4],
[MM_Metics_Pivot].[ContractName] AS [ContractName (MM_Metics_Pivot)],
[MM_Metics_Pivot].[DOS_First] AS [DOS_First],
[MM_Metics_Pivot].[Enrollment] AS [Enrollment],
[MM_Metics_Pivot].[Professional Cost] AS [Professional Cost],
[MM_Metics_Pivot].[SNF Cost] AS [SNF Cost],
[MM_Metics_Pivot].[DME Cost] AS [DME Cost],
[MM_Metics_Pivot].[Inpatient Cost] AS [Inpatient Cost],
[MM_Metics_Pivot].[PPPM Cost Target] AS [PPPM Cost Target],
[MM_Metics_Pivot].[Total Cost w/out Pharmacy] AS [Total Cost w/out Pharmacy],
[MM_Metics_Pivot].[Part-A Cost] AS [Part-A Cost],
[MM_Metics_Pivot].[Total Cost w/ Pharmacy] AS [Total Cost w/ Pharmacy],
[Patient_Summary_Pivot].[Diabetes] AS [Diabetes],
[Patient_Summary_Pivot].[Hypertension] AS [Hypertension],
[Patient_Summary_Pivot].[Mental Health] AS [Mental Health],
[Patient_Summary_Pivot].[Oncology] AS [Oncology],
[Patient_Summary_Pivot].[Resource_Utilization] AS [Resource_Utilization],
[Patient_Summary_Pivot].[Asthma] AS [Asthma],
[Patient_Summary_Pivot].[ATI] AS [ATI],
[Patient_Summary_Pivot].[Chronic Pain] AS [Chronic Pain],
[Patient_Summary_Pivot].[COPD] AS [COPD],
[Patient_Summary_Pivot].[DualStatus] AS [DualStatus],
[Patient_Summary_Pivot].[IP Re-Admits] AS [IP Re-Admits],
[Patient_Summary_Pivot].[MedicareStatus] AS [MedicareStatus],
[Patient_Summary_Pivot].[Risk Score] AS [Risk Score],
[Patient_Summary_Pivot].[CHF] AS [CHF],
[Patient_Summary_Pivot].[CKD] AS [CKD],
[Patient_Summary_Pivot].[ESRD] AS [ESRD]
FROM [dbo].[OrgHierarchy] [OrgHierarchy]
INNER JOIN [dbo].[MM_Metics_Pivot] [MM_Metics_Pivot] ON ([OrgHierarchy].[LbPatientId] = [MM_Metics_Pivot].[LbPatientId])
INNER JOIN [dbo].[Patient_Summary_Pivot] [Patient_Summary_Pivot] ON ([OrgHierarchy].[LbPatientId] = [Patient_Summary_Pivot].[LbPatientId])
GO
