SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_MeasureQADiag_DiagnosisCodeCount]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [DiagnosisCode].[DiagnosisCodeId] AS [DiagnosisCodeId],
[DiagnosisCode].[DiagnosisCodingSystemTypeId] AS [DiagnosisCodingSystemTypeId],
[DiagnosisCode].[DiagnosisCode] AS [DiagnosisCode],
[DiagnosisCode].[DiagnosisDescription] AS [DiagnosisDescription],
[DiagnosisCode].[DiagnosisDescriptionAlternate] AS [DiagnosisDescriptionAlternate],
[DiagnosisCode].[DeleteInd] AS [DeleteInd (DiagnosisCode)],
[DiagnosisCode].[CreateDateTime] AS [CreateDateTime (DiagnosisCode)],
[DiagnosisCode].[ModifyDateTime] AS [ModifyDateTime (DiagnosisCode)],
[DiagnosisCode].[CreateLBUserId] AS [CreateLBUserId],
[DiagnosisCode].[ModifyLBUserId] AS [ModifyLBUserId],
[DiagnosisCode].[DiagnosisCodeRaw] AS [DiagnosisCodeRaw],
[DiagnosisCode].[DiagnosisCodeDisplay] AS [DiagnosisCodeDisplay],
[PatientDiagnosisDiagnosisCode].[PatientDiagnosisDiagnosisCodeId] AS [PatientDiagnosisDiagnosisCodeId],
[PatientDiagnosisDiagnosisCode].[PatientDiagnosisId] AS [PatientDiagnosisId],
[PatientDiagnosisDiagnosisCode].[DiagnosisCodingSystemTypeId] AS [DiagnosisCodingSystemTypeId (PatientDiagnosisDiagnosisCode)],
[PatientDiagnosisDiagnosisCode].[DiagnosisCodeId] AS [DiagnosisCodeId (PatientDiagnosisDiagnosisCode)],
[PatientDiagnosis].[PatientDiagnosisId] AS [PatientDiagnosisId (PatientDiagnosis)],
[PatientDiagnosis].[PatientId] AS [PatientId],
[PatientDiagnosis].[SourceSystemID] AS [SourceSystemID],
[PatientDiagnosis].[DiagnosisTypeID] AS [DiagnosisTypeID],
[PatientDiagnosis].[DiagnosisStatusTypeID] AS [DiagnosisStatusTypeID],
[PatientDiagnosis].[DiagnosisSeverityTypeID] AS [DiagnosisSeverityTypeID],
[PatientDiagnosis].[DiagnosisConfidentialityInd] AS [DiagnosisConfidentialityInd],
[PatientDiagnosis].[DiagnosisPriorityTypeId] AS [DiagnosisPriorityTypeId],
[PatientDiagnosis].[DiagnosisClassificationTypeId] AS [DiagnosisClassificationTypeId],
[PatientDiagnosis].[DiagnosisDateTime] AS [DiagnosisDateTime],
[PatientDiagnosis].[DiagnosisOnsetDate] AS [DiagnosisOnsetDate],
[PatientDiagnosis].[DiagnosisResolvedDate] AS [DiagnosisResolvedDate],
[PatientDiagnosis].[DiagnosisComment] AS [DiagnosisComment],
[PatientDiagnosis].[EncounterID] AS [EncounterID],
[PatientDiagnosis].[ProviderID] AS [ProviderID],
[PatientDiagnosis].[Clinician] AS [Clinician],
[PatientDiagnosis].[ActiveInd] AS [ActiveInd],
[PatientDiagnosis].[DeleteInd] AS [DeleteInd (PatientDiagnosis)],
[PatientDiagnosis].[CreateDateTime] AS [CreateDateTime (PatientDiagnosis)],
[PatientDiagnosis].[ModifyDateTime] AS [ModifyDateTime (PatientDiagnosis)],
[PatientDiagnosis].[CreateLBUserId] AS [CreateLBUserId (PatientDiagnosis)],
[PatientDiagnosis].[ModifyLBUserId] AS [ModifyLBUserId (PatientDiagnosis)],
[PatientDiagnosis].[ExternalReferenceIdentifier] AS [ExternalReferenceIdentifier],
--[PatientDiagnosis].[PatientIdOrig] AS [PatientIdOrig],
[PatientDiagnosis].[EncounterIdentifier] AS [EncounterIdentifier],
[PatientDiagnosis].[ChronicInd] AS [ChronicInd],
[PatientDiagnosis].[RenderingProviderNPI] AS [RenderingProviderNPI],
[SourceSystem].[SourceSystemId] AS [SourceSystemId1],
[SourceSystem].[Name] AS [Name],
[SourceSystem].[Description] AS [Description],
[SourceSystem].[DeleteInd] AS [DeleteInd (SourceSystem)],
[SourceSystem].[CreateDateTime] AS [CreateDateTime (SourceSystem)],
[SourceSystem].[ModifyDateTime] AS [ModifyDateTime (SourceSystem)],
[SourceSystem].[CreateLBUserId] AS [CreateLBUserId (SourceSystem)],
[SourceSystem].[ModifyLBUserId] AS [ModifyLBUserId (SourceSystem)],
[SourceSystem].[ExternalReferenceIdentifier] AS [ExternalReferenceIdentifier (SourceSystem)],
[SourceSystem].[OtherReferenceIdentifier] AS [OtherReferenceIdentifier]
FROM [dbo].[DiagnosisCode] [DiagnosisCode]
LEFT JOIN [dbo].[PatientDiagnosisDiagnosisCode] [PatientDiagnosisDiagnosisCode] ON ([DiagnosisCode].[DiagnosisCodeId] = [PatientDiagnosisDiagnosisCode].[DiagnosisCodeId])
LEFT JOIN [dbo].[PatientDiagnosis] [PatientDiagnosis] ON ([PatientDiagnosisDiagnosisCode].[PatientDiagnosisId] = [PatientDiagnosis].[PatientDiagnosisId])
LEFT JOIN [dbo].[SourceSystem] [SourceSystem] ON ([PatientDiagnosis].[SourceSystemID] = [SourceSystem].[SourceSystemId])
GO
