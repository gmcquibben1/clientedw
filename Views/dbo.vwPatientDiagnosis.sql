SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientDiagnosis] 
AS 

SELECT pd.PatientId, 
	pd.SourceSystemId, 
	pd.DiagnosisTypeId, 
	dxtype.DisplayValue as DiagnosisType, 
	pd.DiagnosisStatusTypeID,
	dxstat.DisplayValue as DiagnosisStatus, 
	pd.DiagnosisSeverityTypeID, 
	dxsev.DisplayValue as DiagnosisSeverity, 
	pd.DiagnosisConfidentialityInd,
	pd.DiagnosisPriorityTypeId,
	dxprior.DisplayValue  as DiagnosisPriority, 
	pd.DiagnosisClassificationTypeId,
	dxclass.DisplayValue as DiagnosisClassification, 
	pd.DiagnosisDateTime, 
	pd.DiagnosisOnsetDate,
	pd.DiagnosisResolvedDate,
	pd.DiagnosisComment, 
	pd.EncounterID ,
	pd.ProviderID , 
	pd.Clinician ,
	pd.ActiveInd ,
	pd.CreateDateTime,
	pd.ModifyDateTime,
	dc.DiagnosisCodingSystemTypeId,
	dxsys.DisplayValue AS DiagnosisCodingSystem, 
	dc.DiagnosisCode,
	dc.DiagnosisDescription ,
	dc.DiagnosisDescriptionAlternate,
	dc.DiagnosisCodeRaw,
	dc.DiagnosisCodeDisplay

FROM PatientDiagnosis pd 
 	 INNER JOIN PatientDiagnosisDiagnosisCode pddc ON pd.PatientDiagnosisId = pddc.PatientDiagnosisId 
	 INNER JOIN DiagnosisCode dc ON dc.DiagnosisCodeId = pddc.DiagnosisCodeId 
	 INNER JOIN DiagnosisCodingSystemType dxsys ON dxsys.DiagnosisCodingSystemTypeId = dc.DiagnosisCodingSystemTypeId 
	 LEFT OUTER JOIN DiagnosisType dxtype ON dxtype.DiagnosisTypeId = pd.DiagnosisTypeId
	 LEFT OUTER JOIN DiagnosisStatusType dxstat ON dxstat.DiagnosisStatusTypeId = pd.DiagnosisStatusTypeId
	 LEFT OUTER JOIN DiagnosisSeverityType dxsev ON dxsev.DiagnosisSeverityTypeId = pd.DiagnosisSeverityTypeId
	 LEFT OUTER JOIN DiagnosisPriorityType dxprior ON dxprior.DiagnosisPriorityTypeId = pd.DiagnosisPriorityTypeId
	 LEFT OUTER JOIN DiagnosisClassificationType dxclass ON dxclass.DiagnosisClassificationTypeId = pd.DiagnosisClassificationTypeId
WHERE pd.DeleteInd = 0 AND dc.DeleteInd = 0


GO
