SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ==========================================================
-- Author:    Ismail
-- Create date:  08/24/2016
-- Description:  View of Analytics HCCPatAging Workbook
-- ===========================================================

CREATE VIEW [dbo].[vwAnalytics_HCCPatAging]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT 
[OrgHierarchy].[State] AS [OrgHierarchy_State],
[OrgHierarchy].[City] AS [City],
[OrgHierarchy].[Zip] AS [Zip],
[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
[OrgHierarchy].[MemberID] AS [MemberID],
[OrgHierarchy].[MemberFullName] AS [MemberFullName],
[OrgHierarchy].[Member_Provider_Relationship_Effective_Date] AS [OrgHierarchy_Member_Provider_Relationship_Effective_Date],
[OrgHierarchy].[Member_Provider_Relationship_Termination_Date] AS [OrgHierarchy_Member_Provider_Relationship_Termination_Date],
[OrgHierarchy].[BENE_HIC_NUM] AS [OrgHierarchy_BENE_HIC_NUM],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
[OrgHierarchy].[BirthDate] AS [BirthDate],
[OrgHierarchy].[GenderId] AS [GenderId],
[OrgHierarchy].[County] AS [County],
[OrgHierarchy].[LastUpdatedTimestamp] AS [LastUpdatedTimestamp],
[OrgHierarchy].[LbPatientId] AS [LbPatientId],
[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
[OrgHierarchy].[PatientId1] AS [PatientId1],
[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
[OrgHierarchy].[PatientId2] AS [PatientId2],
[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
[OrgHierarchy].[PatientId3] AS [PatientId3],
[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
[OrgHierarchy].[PatientId4] AS [PatientId4],
[HCCPatAging].[id] AS [id1],
[HCCPatAging].[PatientID] AS [PatientID],
[HCCPatAging].[HCCNo] AS [HCCNo],
[HCCPatAging].[DiseaseGroup] AS [DiseaseGroup],
[HCCPatAging].[Factor] AS [Factor],
[HCCPatAging].[DateOfService] AS [DateOfService],
[HCCPatAging].[Count] AS [Count],
[HCCPatAging].[ValidFlag] AS [ValidFlag],
[HCCPatAging].[LbPatientId] AS [LbPatientId (HCCPatAging)]
FROM [dbo].[OrgHierarchy] [OrgHierarchy]
INNER JOIN [dbo].[HCCPatAging] [HCCPatAging] ON ([OrgHierarchy].[LbPatientId] = [HCCPatAging].[LbPatientId])
GO
