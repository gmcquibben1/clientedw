SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientCareGap] 
AS 

SELECT 
	t.PatientId,
	t.TaskId,
	tst.DisplayValue AS TaskStatus,
	cg.CareGapId,
	cgt.DisplayValue AS CareGapType,
	cgr.DisplayValue AS CareGapRuleType,
	cgat.DisplayValue AS CareGapActionType,
	cg.CreateDateTime,
	cg.ModifyDateTime
FROM 
	Version22LbportalTemplate.[dbo].[Task] t
	INNER JOIN Version22LbportalTemplate.[dbo].[TaskStatusType] tst ON tst.TaskStatusTypeId = t.TaskStatusTypeId
	INNER JOIN Version22LbportalTemplate.[dbo].[CareGap] cg ON cg.TaskId = t.TaskId
	LEFT OUTER JOIN Version22LbportalTemplate.[dbo].[CareGapType] cgt ON cgt.CareGapTypeId = cg.CareGapTypeId
	LEFT OUTER JOIN Version22LbportalTemplate.[dbo].[CareGapRuleType] cgr ON cgr.CareGapRuleTypeId = cg.CareGapRuleTypeId
	LEFT OUTER JOIN Version22LbportalTemplate.[dbo].[CareGapActionType] cgat ON cgat.CareGapActionTypeId = cg.CareGapActionTypeId
WHERE cg.DeleteInd <> 1
	AND t.DeleteInd <> 1
AND db_name() = 'Version22EdwTemplate' 
GO
