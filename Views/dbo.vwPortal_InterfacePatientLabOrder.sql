SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientLabOrder] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientLabOrder
WHERE db_name() = 'Version22EdwTemplate' 
GO
