SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfaceProvider] AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfaceProvider
WHERE db_name() = 'Version22EdwTemplate' 
GO
