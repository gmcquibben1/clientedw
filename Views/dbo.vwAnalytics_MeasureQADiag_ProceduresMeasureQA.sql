SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_MeasureQADiag_ProceduresMeasureQA]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [ProcedureCode].[ProcedureCodeId] AS [ProcedureCodeId],
[ProcedureCode].[ProcedureCodingSystemTypeId] AS [ProcedureCodingSystemTypeId],
[ProcedureCode].[ProcedureCode] AS [ProcedureCode],
[ProcedureCode].[ProcedureDescription] AS [ProcedureDescription],
[ProcedureCode].[ProcedureDescriptionAlternate] AS [ProcedureDescriptionAlternate],
[ProcedureCode].[DeleteInd] AS [DeleteInd (ProcedureCode)],
[ProcedureCode].[CreateDateTime] AS [CreateDateTime (ProcedureCode)],
[ProcedureCode].[ModifyDateTime] AS [ModifyDateTime (ProcedureCode)],
[ProcedureCode].[CreateLBUserId] AS [CreateLBUserId],
[ProcedureCode].[ModifyLBUserId] AS [ModifyLBUserId],
[PatientProcedureProcedureCode].[PatientProcedureProcedureCodeId] AS [PatientProcedureProcedureCodeId],
[PatientProcedureProcedureCode].[PatientProcedureId] AS [PatientProcedureId],
[PatientProcedureProcedureCode].[ProcedureCodingSystemTypeId] AS [ProcedureCodingSystemTypeId (PatientProcedureProcedureCode)],
[PatientProcedureProcedureCode].[ProcedureCodeId] AS [ProcedureCodeId (PatientProcedureProcedureCode)],
[PatientProcedure].[PatientProcedureId] AS [PatientProcedureId (PatientProcedure)],
[PatientProcedure].[PatientID] AS [PatientID],
[PatientProcedure].[SourceSystemID] AS [SourceSystemID],
[PatientProcedure].[ProcedureFunctionTypeID] AS [ProcedureFunctionTypeID],
[PatientProcedure].[ProcedureCodeModifier1] AS [ProcedureCodeModifier1],
[PatientProcedure].[ProcedureCodeModifier2] AS [ProcedureCodeModifier2],
[PatientProcedure].[ProcedureCodeModifier3] AS [ProcedureCodeModifier3],
[PatientProcedure].[ProcedureCodeModifier4] AS [ProcedureCodeModifier4],
[PatientProcedure].[ProcedureDateTime] AS [ProcedureDateTime],
[PatientProcedure].[ProcedureComment] AS [ProcedureComment],
[PatientProcedure].[EncounterID] AS [EncounterID],
[PatientProcedure].[PerformedByProviderID] AS [PerformedByProviderID],
[PatientProcedure].[PerformedByClinician] AS [PerformedByClinician],
[PatientProcedure].[OrderedByProviderID] AS [OrderedByProviderID],
[PatientProcedure].[OrderedByClinician] AS [OrderedByClinician],
[PatientProcedure].[DeleteInd] AS [DeleteInd (PatientProcedure)],
[PatientProcedure].[CreateDateTime] AS [CreateDateTime (PatientProcedure)],
[PatientProcedure].[ModifyDateTime] AS [ModifyDateTime (PatientProcedure)],
[PatientProcedure].[CreateLBUserId] AS [CreateLBUserId (PatientProcedure)],
[PatientProcedure].[ModifyLBUserId] AS [ModifyLBUserId (PatientProcedure)],
[PatientProcedure].[ExternalReferenceIdentifier] AS [ExternalReferenceIdentifier],
--[PatientProcedure].[PatientIdOrig] AS [PatientIdOrig],
[PatientProcedure].[EncounterIdentifier] AS [EncounterIdentifier],
[PatientProcedure].[TotalCharges] AS [TotalCharges],
[PatientProcedure].[InternalChargeCode] AS [InternalChargeCode],
[PatientProcedure].[ProcedureResult] AS [ProcedureResult],
[PatientProcedure].[PlaceOfServiceCode] AS [PlaceOfServiceCode],
[PatientProcedure].[PlaceOfService] AS [PlaceOfService],
[PatientProcedure].[ResultCodingSystemName] AS [ResultCodingSystemName],
[PatientProcedure].[RenderingProviderNPI] AS [RenderingProviderNPI],
[PatientProcedure].[InvoiceIdentifier] AS [InvoiceIdentifier],
[PatientProcedure].[RevenueCode] AS [RevenueCode],
[SourceSystem].[SourceSystemId] AS [SourceSystemId1],
[SourceSystem].[Name] AS [Name],
[SourceSystem].[Description] AS [Description],
[SourceSystem].[DeleteInd] AS [DeleteInd (SourceSystem)],
[SourceSystem].[CreateDateTime] AS [CreateDateTime (SourceSystem)],
[SourceSystem].[ModifyDateTime] AS [ModifyDateTime (SourceSystem)],
[SourceSystem].[CreateLBUserId] AS [CreateLBUserId (SourceSystem)],
[SourceSystem].[ModifyLBUserId] AS [ModifyLBUserId (SourceSystem)],
[SourceSystem].[ExternalReferenceIdentifier] AS [ExternalReferenceIdentifier (SourceSystem)],
[SourceSystem].[OtherReferenceIdentifier] AS [OtherReferenceIdentifier]
FROM [dbo].[ProcedureCode] [ProcedureCode]
LEFT JOIN [dbo].[PatientProcedureProcedureCode] [PatientProcedureProcedureCode] ON ([ProcedureCode].[ProcedureCodeId] = [PatientProcedureProcedureCode].[ProcedureCodeId])
LEFT JOIN [dbo].[PatientProcedure] [PatientProcedure] ON ([PatientProcedureProcedureCode].[PatientProcedureId] = [PatientProcedure].[PatientProcedureId])
INNER JOIN [dbo].[SourceSystem] [SourceSystem] ON ([PatientProcedure].[SourceSystemID] = [SourceSystem].[SourceSystemId])
GO
