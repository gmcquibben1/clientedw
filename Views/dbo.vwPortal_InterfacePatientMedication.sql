SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfacePatientMedication] 
AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfacePatientMedication
WHERE db_name() = 'Version22EdwTemplate' 
GO
