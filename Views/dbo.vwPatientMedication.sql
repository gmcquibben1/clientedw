SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientMedication] 
AS 

SELECT pm.PatientId, 
	pm.SourceSystemId, 
	m.NDCCode,
	m.RxNormCode,
	m.BrandName,
	m.GenericName,
	m.MedicationDose,
	mft.DisplayValue AS MedicationForm,
	mrt.DisplayValue AS MedicationRoute,
	pm.NDCCode AS NDCodeCFromSource,
	pm.RxNormCode AS RxNormCodeFromSource,
	pm.MedicationNameFromSource,
	pm.MedicationDoseFromSource,
	pm.MedicationFormFromSource,
	pm.MedicationRouteFromSource,
	pm.Instructions,
	pm.MedicationStartDate,
	pm.MedicationEndDate,
	pm.Comment,
	pm.Quantity,
	pm.Refills,
	pm.SampleInd,
	pm.GenericAllowedInd,
	(CASE WHEN pe.ProviderLastName IS NOT NULL THEN pe.ProviderFirstName + ' ' + pe.ProviderLastName
		  WHEN indN.LastName IS NOT NULL THEN indN.FirstName + ' ' + indN.LastName
		  ELSE null
	END)  as 'Provider Name',
	
--Status Description
--Encounter Unique Id
	pm.CreateDateTime,
	pm.ModifyDateTime

FROM PatientMedication pm 
	 LEFT OUTER JOIN Medication m ON m.MedicationId = pm.MedicationId 
	 LEFT OUTER JOIN MedicationFormType mft ON mft.MedicationFormTypeId = m.MedicationFormTypeId
	 LEFT OUTER JOIN MedicationRouteType mrt ON mrt.MedicationRouteTypeId = m.MedicationRouteTypeId
	 LEFT OUTER JOIN ProviderExtract pe ON pe.ProviderNPI = cast(pm.ProviderId as varchar(20))
	 LEFT OUTER JOIN vwPortal_Provider prov ON prov.ProviderId = pm.ProviderId
	 LEFT OUTER JOIN vwPortal_IndividualName indN ON indN.IndividualId = prov.IndividualId
WHERE pm.DeleteInd = 0
GO
