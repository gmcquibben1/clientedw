SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPatientImmunization]
AS 

SELECT 
	pi.PatientId, 
	pi.SourceSystemId,
	pi.ServiceDate,
	ic.ImmunizationCode,
	icst.DisplayValue as ImmunizationCodingSystem,
	ic.ImmunizationDescription,
	pi.ImmunizationDose,
	pi.ImmunizationUnits,
	pi.MaterialLotNumber,
	pi.MaterialManufacturer,
	pi.MedicationRouteTypeID,
	mrt.DisplayValue as MedicationRouteType,
	pi.PerformedByClinician,
	pi.CreateDateTime,
	pi.ModifyDateTime
FROM PatientImmunization pi
	 INNER JOIN ImmunizationCode ic ON ic.ImmunizationCodeId = pi.ImmunizationCodeId 
	 INNER JOIN ImmunizationCodingSystemType icst ON icst.ImmunizationCodingSystemTypeId = ic.ImmunizationCodingSystemTypeId
	 LEFT OUTER JOIN MedicationRouteType mrt ON mrt.MedicationRouteTypeId = pi.MedicationRouteTypeId  
WHERE pi.DeleteInd = 0 
	
GO
