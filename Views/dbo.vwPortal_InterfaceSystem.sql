SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_InterfaceSystem] AS
SELECT * FROM [Version22LbportalTemplate].dbo.InterfaceSystem
WHERE db_name() = 'Version22EdwTemplate' 
GO
