SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_CommunicationMethodType] AS
SELECT * FROM [Version22LbportalTemplate].dbo.CommunicationMethodType
WHERE db_name() = 'Version22EdwTemplate' 
GO
