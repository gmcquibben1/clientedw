SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_PatientEnrollment] AS
SELECT * FROM [Version22LbportalTemplate].dbo.PatientEnrollment
WHERE db_name() = 'Version22EdwTemplate' 
GO
