SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPortal_PatientAttributeType] AS
SELECT * FROM [Version22LbportalTemplate].dbo.PatientAttributeType
WHERE db_name() = 'Version22EdwTemplate' 
GO
