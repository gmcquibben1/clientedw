SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwAnalytics_JHACG_DETAIL_NEW]
--WITH SCHEMABINDING (Removing Schema Binding, MH - 09/02/2016)
AS 
SELECT [JhacgPatientDetail].[patient_id] AS [patient_id],
[JhacgPatientDetail].[age] AS [age],
[JhacgPatientDetail].[sex] AS [sex],
[JhacgPatientDetail].[line_of_business] AS [line_of_business],
[JhacgPatientDetail].[company] AS [company],
[JhacgPatientDetail].[product] AS [product],
[JhacgPatientDetail].[employer_group_id] AS [employer_group_id],
[JhacgPatientDetail].[employer_group_name] AS [employer_group_name],
[JhacgPatientDetail].[benefit_plan] AS [benefit_plan],
[JhacgPatientDetail].[health_system] AS [health_system],
[JhacgPatientDetail].[pcp_id] AS [pcp_id],
[JhacgPatientDetail].[pcp_name] AS [pcp_name],
[JhacgPatientDetail].[pcp_group_id] AS [pcp_group_id],
[JhacgPatientDetail].[pcp_group_name] AS [pcp_group_name],
[JhacgPatientDetail].[pregnant] AS [pregnant],
[JhacgPatientDetail].[delivered] AS [delivered],
[JhacgPatientDetail].[low_birthweight] AS [low_birthweight],
[JhacgPatientDetail].[pharmacy_cost] AS [pharmacy_cost],
[JhacgPatientDetail].[total_cost] AS [total_cost],
[JhacgPatientDetail].[inpatient_hosp_count] AS [inpatient_hosp_count],
[JhacgPatientDetail].[inpatient_hosp_days] AS [inpatient_hosp_days],
[JhacgPatientDetail].[emergency_visit_count] AS [emergency_visit_count],
[JhacgPatientDetail].[outpatient_visit_count] AS [outpatient_visit_count],
[JhacgPatientDetail].[dialysis_service] AS [dialysis_service],
[JhacgPatientDetail].[nursing_service] AS [nursing_service],
[JhacgPatientDetail].[major_procedure] AS [major_procedure],
[JhacgPatientDetail].[cancer_treatment] AS [cancer_treatment],
[JhacgPatientDetail].[care_management_program] AS [care_management_program],
[JhacgPatientDetail].[date_of_birth] AS [date_of_birth],
[JhacgPatientDetail].[psychotherapy_service] AS [psychotherapy_service],
[JhacgPatientDetail].[mechanical_ventilation] AS [mechanical_ventilation],
[JhacgPatientDetail].[cal_ssa] AS [cal_ssa],
[JhacgPatientDetail].[all_cause_inpatient_hosp_count] AS [all_cause_inpatient_hosp_count],
[JhacgPatientDetail].[unplanned_inpatient_hosp_count] AS [unplanned_inpatient_hosp_count],
[JhacgPatientDetail].[radm_30d_count] AS [radm_30d_count],
[JhacgPatientDetail].[unplanned_radm_30d_count] AS [unplanned_radm_30d_count],
[JhacgPatientDetail].[pharmacy_cost_band] AS [pharmacy_cost_band],
[JhacgPatientDetail].[total_cost_band] AS [total_cost_band],
[JhacgPatientDetail].[age_band] AS [age_band],
[JhacgPatientDetail].[acg_code] AS [acg_code],
[JhacgPatientDetail].[diagnoses_used] AS [diagnoses_used],
[JhacgPatientDetail].[adg_codes] AS [adg_codes],
[JhacgPatientDetail].[adg_vector] AS [adg_vector],
[JhacgPatientDetail].[edc_codes] AS [edc_codes],
[JhacgPatientDetail].[medc_codes] AS [medc_codes],
[JhacgPatientDetail].[rxmg_codes] AS [rxmg_codes],
[JhacgPatientDetail].[medical_rxmg_codes] AS [medical_rxmg_codes],
[JhacgPatientDetail].[pharmacy_rxmg_codes] AS [pharmacy_rxmg_codes],
[JhacgPatientDetail].[major_rxmg_codes] AS [major_rxmg_codes],
[JhacgPatientDetail].[resource_utilization_band] AS [resource_utilization_band],
[JhacgPatientDetail].[unscaled_ACG_concurrent_risk] AS [unscaled_ACG_concurrent_risk],
[JhacgPatientDetail].[rescaled_ACG_concurrent_risk] AS [rescaled_ACG_concurrent_risk],
[JhacgPatientDetail].[local_age_gend_concurrent_risk] AS [local_age_gend_concurrent_risk],
[JhacgPatientDetail].[local_acg_concurrent_risk] AS [local_acg_concurrent_risk],
[JhacgPatientDetail].[rescaled_concurrent_risk] AS [rescaled_concurrent_risk],
[JhacgPatientDetail].[unscaled_concurrent_risk] AS [unscaled_concurrent_risk],
[JhacgPatientDetail].[hosdom_morbidity_types] AS [hosdom_morbidity_types],
[JhacgPatientDetail].[chronic_condition_count] AS [chronic_condition_count],
[JhacgPatientDetail].[major_adg_count] AS [major_adg_count],
[JhacgPatientDetail].[frailty_flag] AS [frailty_flag],
[JhacgPatientDetail].[frailty_concepts] AS [frailty_concepts],
[JhacgPatientDetail].[pregnancy_without_delivery] AS [pregnancy_without_delivery],
[JhacgPatientDetail].[age_rel_macular_deg_condition] AS [age_rel_macular_deg_condition],
[JhacgPatientDetail].[bipolar_disorder_condition] AS [bipolar_disorder_condition],
[JhacgPatientDetail].[bipolar_disorder_rx_gaps] AS [bipolar_disorder_rx_gaps],
[JhacgPatientDetail].[bipolar_disorder_mpr] AS [bipolar_disorder_mpr],
[JhacgPatientDetail].[bipolar_disorder_csa] AS [bipolar_disorder_csa],
[JhacgPatientDetail].[bipolar_disorder_untreated_rx] AS [bipolar_disorder_untreated_rx],
[JhacgPatientDetail].[cong_heart_fail_condition] AS [cong_heart_fail_condition],
[JhacgPatientDetail].[cong_heart_fail_rx_gaps] AS [cong_heart_fail_rx_gaps],
[JhacgPatientDetail].[cong_heart_fail_mpr] AS [cong_heart_fail_mpr],
[JhacgPatientDetail].[cong_heart_fail_csa] AS [cong_heart_fail_csa],
[JhacgPatientDetail].[cong_heart_fail_untreated_rx] AS [cong_heart_fail_untreated_rx],
[JhacgPatientDetail].[depression_condition] AS [depression_condition],
[JhacgPatientDetail].[depression_rx_gaps] AS [depression_rx_gaps],
[JhacgPatientDetail].[depression_mpr] AS [depression_mpr],
[JhacgPatientDetail].[depression_csa] AS [depression_csa],
[JhacgPatientDetail].[depression_untreated_rx] AS [depression_untreated_rx],
[JhacgPatientDetail].[diabetes_condition] AS [diabetes_condition],
[JhacgPatientDetail].[diabetes_rx_gaps] AS [diabetes_rx_gaps],
[JhacgPatientDetail].[diabetes_mpr] AS [diabetes_mpr],
[JhacgPatientDetail].[diabetes_csa] AS [diabetes_csa],
[JhacgPatientDetail].[diabetes_untreated_rx] AS [diabetes_untreated_rx],
[JhacgPatientDetail].[glaucoma_condition] AS [glaucoma_condition],
[JhacgPatientDetail].[glaucoma_rx_gaps] AS [glaucoma_rx_gaps],
[JhacgPatientDetail].[glaucoma_mpr] AS [glaucoma_mpr],
[JhacgPatientDetail].[glaucoma_csa] AS [glaucoma_csa],
[JhacgPatientDetail].[glaucoma_untreated_rx] AS [glaucoma_untreated_rx],
[JhacgPatientDetail].[human_imm_virus_condition] AS [human_imm_virus_condition],
[JhacgPatientDetail].[human_imm_virus_rx_gaps] AS [human_imm_virus_rx_gaps],
[JhacgPatientDetail].[human_imm_virus_mpr] AS [human_imm_virus_mpr],
[JhacgPatientDetail].[human_imm_virus_csa] AS [human_imm_virus_csa],
[JhacgPatientDetail].[human_imm_virus_untreated_rx] AS [human_imm_virus_untreated_rx],
[JhacgPatientDetail].[dis_lipid_metab_condition] AS [dis_lipid_metab_condition],
[JhacgPatientDetail].[dis_lipid_metab_rx_gaps] AS [dis_lipid_metab_rx_gaps],
[JhacgPatientDetail].[dis_lipid_metab_mpr] AS [dis_lipid_metab_mpr],
[JhacgPatientDetail].[dis_lipid_metab_csa] AS [dis_lipid_metab_csa],
[JhacgPatientDetail].[dis_lipid_metab_untreated_rx] AS [dis_lipid_metab_untreated_rx],
[JhacgPatientDetail].[hypertension_condition] AS [hypertension_condition],
[JhacgPatientDetail].[hypertension_rx_gaps] AS [hypertension_rx_gaps],
[JhacgPatientDetail].[hypertension_mpr] AS [hypertension_mpr],
[JhacgPatientDetail].[hypertension_csa] AS [hypertension_csa],
[JhacgPatientDetail].[hypertension_untreated_rx] AS [hypertension_untreated_rx],
[JhacgPatientDetail].[hypothyroidism_condition] AS [hypothyroidism_condition],
[JhacgPatientDetail].[hypothyroidism_rx_gaps] AS [hypothyroidism_rx_gaps],
[JhacgPatientDetail].[hypothyroidism_mpr] AS [hypothyroidism_mpr],
[JhacgPatientDetail].[hypothyroidism_csa] AS [hypothyroidism_csa],
[JhacgPatientDetail].[hypothyroidism_untreated_rx] AS [hypothyroidism_untreated_rx],
[JhacgPatientDetail].[immuno_transplant_condition] AS [immuno_transplant_condition],
[JhacgPatientDetail].[immuno_transplant_rx_gaps] AS [immuno_transplant_rx_gaps],
[JhacgPatientDetail].[immuno_transplant_mpr] AS [immuno_transplant_mpr],
[JhacgPatientDetail].[immuno_transplant_csa] AS [immuno_transplant_csa],
[JhacgPatientDetail].[immuno_transplant_untreated_rx] AS [immuno_transplant_untreated_rx],
[JhacgPatientDetail].[ischemic_hrt_dis_condition] AS [ischemic_hrt_dis_condition],
[JhacgPatientDetail].[ischemic_hrt_dis_rx_gaps] AS [ischemic_hrt_dis_rx_gaps],
[JhacgPatientDetail].[ischemic_hrt_dis_mpr] AS [ischemic_hrt_dis_mpr],
[JhacgPatientDetail].[ischemic_hrt_dis_csa] AS [ischemic_hrt_dis_csa],
[JhacgPatientDetail].[ischemic_hrt_dis_untreated_rx] AS [ischemic_hrt_dis_untreated_rx],
[JhacgPatientDetail].[osteoporosis_condition] AS [osteoporosis_condition],
[JhacgPatientDetail].[osteoporosis_rx_gaps] AS [osteoporosis_rx_gaps],
[JhacgPatientDetail].[osteoporosis_mpr] AS [osteoporosis_mpr],
[JhacgPatientDetail].[osteoporosis_csa] AS [osteoporosis_csa],
[JhacgPatientDetail].[osteoporosis_untreated_rx] AS [osteoporosis_untreated_rx],
[JhacgPatientDetail].[parkinsons_dis_condition] AS [parkinsons_dis_condition],
[JhacgPatientDetail].[parkinsons_dis_rx_gaps] AS [parkinsons_dis_rx_gaps],
[JhacgPatientDetail].[parkinsons_dis_mpr] AS [parkinsons_dis_mpr],
[JhacgPatientDetail].[parkinsons_dis_csa] AS [parkinsons_dis_csa],
[JhacgPatientDetail].[parkinsons_dis_untreated_rx] AS [parkinsons_dis_untreated_rx],
[JhacgPatientDetail].[persistent_asthma_condition] AS [persistent_asthma_condition],
[JhacgPatientDetail].[persistent_asthma_rx_gaps] AS [persistent_asthma_rx_gaps],
[JhacgPatientDetail].[persistent_asthma_mpr] AS [persistent_asthma_mpr],
[JhacgPatientDetail].[persistent_asthma_csa] AS [persistent_asthma_csa],
[JhacgPatientDetail].[persistent_asthma_untreated_rx] AS [persistent_asthma_untreated_rx],
[JhacgPatientDetail].[rheumatoid_arthr_condition] AS [rheumatoid_arthr_condition],
[JhacgPatientDetail].[rheumatoid_arthr_rx_gaps] AS [rheumatoid_arthr_rx_gaps],
[JhacgPatientDetail].[rheumatoid_arthr_mpr] AS [rheumatoid_arthr_mpr],
[JhacgPatientDetail].[rheumatoid_arthr_csa] AS [rheumatoid_arthr_csa],
[JhacgPatientDetail].[rheumatoid_arthr_untreated_rx] AS [rheumatoid_arthr_untreated_rx],
[JhacgPatientDetail].[schizophrenia_condition] AS [schizophrenia_condition],
[JhacgPatientDetail].[schizophrenia_rx_gaps] AS [schizophrenia_rx_gaps],
[JhacgPatientDetail].[schizophrenia_mpr] AS [schizophrenia_mpr],
[JhacgPatientDetail].[schizophrenia_csa] AS [schizophrenia_csa],
[JhacgPatientDetail].[schizophrenia_untreated_rx] AS [schizophrenia_untreated_rx],
[JhacgPatientDetail].[seizure_disorders_condition] AS [seizure_disorders_condition],
[JhacgPatientDetail].[seizure_disorders_rx_gaps] AS [seizure_disorders_rx_gaps],
[JhacgPatientDetail].[seizure_disorders_mpr] AS [seizure_disorders_mpr],
[JhacgPatientDetail].[seizure_disorders_csa] AS [seizure_disorders_csa],
[JhacgPatientDetail].[seizure_disorders_untreated_rx] AS [seizure_disorders_untreated_rx],
[JhacgPatientDetail].[chronic_obs_pul_dis_condition] AS [chronic_obs_pul_dis_condition],
[JhacgPatientDetail].[chronic_renal_fail_condition] AS [chronic_renal_fail_condition],
[JhacgPatientDetail].[low_back_pain_condition] AS [low_back_pain_condition],
[JhacgPatientDetail].[total_rx_gaps] AS [total_rx_gaps],
[JhacgPatientDetail].[unscaled_total_cost_pred_risk] AS [unscaled_total_cost_pred_risk],
[JhacgPatientDetail].[rescaled_total_cost_pred_risk] AS [rescaled_total_cost_pred_risk],
[JhacgPatientDetail].[predicted_total_cost_range] AS [predicted_total_cost_range],
[JhacgPatientDetail].[rank_prob_high_tot_cost] AS [rank_prob_high_tot_cost],
[JhacgPatientDetail].[reference_prob_high_tot_cost] AS [reference_prob_high_tot_cost],
[JhacgPatientDetail].[unscaled_pharm_cost_pred_risk] AS [unscaled_pharm_cost_pred_risk],
[JhacgPatientDetail].[rescaled_pharm_cost_pred_risk] AS [rescaled_pharm_cost_pred_risk],
[JhacgPatientDetail].[predicted_pharmacy_cost_range] AS [predicted_pharmacy_cost_range],
[JhacgPatientDetail].[rank_prob_high_pharm_cost] AS [rank_prob_high_pharm_cost],
[JhacgPatientDetail].[reference_prob_high_pharm_cost] AS [reference_prob_high_pharm_cost],
[JhacgPatientDetail].[high_rsk_unexpected_pharm_cost] AS [high_rsk_unexpected_pharm_cost],
[JhacgPatientDetail].[prob_unexpected_pharm_cost] AS [prob_unexpected_pharm_cost],
[JhacgPatientDetail].[prob_persistent_high_user] AS [prob_persistent_high_user],
[JhacgPatientDetail].[majority_source_of_care_pct] AS [majority_source_of_care_pct],
[JhacgPatientDetail].[majority_source_of_care_provs] AS [majority_source_of_care_provs],
[JhacgPatientDetail].[unique_provider_count] AS [unique_provider_count],
[JhacgPatientDetail].[specialty_count] AS [specialty_count],
[JhacgPatientDetail].[generalist_seen] AS [generalist_seen],
[JhacgPatientDetail].[management_visit_count] AS [management_visit_count],
[JhacgPatientDetail].[coordination_risk] AS [coordination_risk],
[JhacgPatientDetail].[care_density_ratio] AS [care_density_ratio],
[JhacgPatientDetail].[care_density_quantile] AS [care_density_quantile],
[JhacgPatientDetail].[care_density_cost_saving_ratio] AS [care_density_cost_saving_ratio],
[JhacgPatientDetail].[care_density_cost_saving_range] AS [care_density_cost_saving_range],
[JhacgPatientDetail].[care_density_bot_quart_cutoff] AS [care_density_bot_quart_cutoff],
[JhacgPatientDetail].[care_density_top_quart_cutoff] AS [care_density_top_quart_cutoff],
[JhacgPatientDetail].[active_ingredient_count] AS [active_ingredient_count],
[JhacgPatientDetail].[probability_ip_hosp] AS [probability_ip_hosp],
[JhacgPatientDetail].[probability_ip_hosp_6mos] AS [probability_ip_hosp_6mos],
[JhacgPatientDetail].[probability_icu_hosp] AS [probability_icu_hosp],
[JhacgPatientDetail].[probability_injury_hosp] AS [probability_injury_hosp],
[JhacgPatientDetail].[probability_extended_hosp] AS [probability_extended_hosp],
[JhacgPatientDetail].[probability_unplanned_30d_radm] AS [probability_unplanned_30d_radm],
[JhacgPatientDetail].[hhs_market] AS [hhs_market],
[JhacgPatientDetail].[hhs_rating_area] AS [hhs_rating_area],
[JhacgPatientDetail].[hhs_risk_pool] AS [hhs_risk_pool],
[JhacgPatientDetail].[hhs_metal_level] AS [hhs_metal_level],
[JhacgPatientDetail].[hhs_csr_indicator] AS [hhs_csr_indicator],
[JhacgPatientDetail].[hhs_model] AS [hhs_model],
[JhacgPatientDetail].[hhs_excl_from_risk_adjustment] AS [hhs_excl_from_risk_adjustment],
[JhacgPatientDetail].[hhs_cc_codes] AS [hhs_cc_codes],
[JhacgPatientDetail].[hhs_hcc_codes] AS [hhs_hcc_codes],
[JhacgPatientDetail].[hhs_risk_score] AS [hhs_risk_score],
[JhacgPatientDetail].[hhs_csr_adjusted_risk_score] AS [hhs_csr_adjusted_risk_score],
[JhacgPatientDetail].[hhs_member_months] AS [hhs_member_months],
[JhacgPatientDetail].[hhs_billable_member_months] AS [hhs_billable_member_months],
[JhacgPatientDetail].[hhs_tot_member_months] AS [hhs_tot_member_months],
[JhacgPatientDetail].[hhs_tot_billable_member_months] AS [hhs_tot_billable_member_months],
[JhacgPatientDetail].[warning_codes] AS [warning_codes],
[JhacgPatientDetail].[CreateDateTime] AS [CreateDateTime],
[OrgHierarchy].[ID] AS [ID],
[OrgHierarchy].[State] AS [State],
[OrgHierarchy].[City] AS [City],
[OrgHierarchy].[Zip] AS [Zip],
[OrgHierarchy].[GeoCodeLong] AS [GeoCodeLong],
[OrgHierarchy].[GeoCodeLat] AS [GeoCodeLat],
[OrgHierarchy].[Level1Id] AS [Level1Id],
[OrgHierarchy].[Level1Name] AS [Level1Name],
[OrgHierarchy].[Level1TypeName] AS [Level1TypeName],
[OrgHierarchy].[Level2Id] AS [Level2Id],
[OrgHierarchy].[Level2Name] AS [Level2Name],
[OrgHierarchy].[Level2TypeName] AS [Level2TypeName],
[OrgHierarchy].[Level3Id] AS [Level3Id],
[OrgHierarchy].[Level3Name] AS [Level3Name],
[OrgHierarchy].[Level3TypeName] AS [Level3TypeName],
[OrgHierarchy].[Level4Id] AS [Level4Id],
[OrgHierarchy].[Level4Name] AS [Level4Name],
[OrgHierarchy].[Level4TypeName] AS [Level4TypeName],
[OrgHierarchy].[Level5Id] AS [Level5Id],
[OrgHierarchy].[Level5Name] AS [Level5Name],
[OrgHierarchy].[Level5TypeName] AS [Level5TypeName],
[OrgHierarchy].[Level6Id] AS [Level6Id],
[OrgHierarchy].[Level6Name] AS [Level6Name],
[OrgHierarchy].[Level6TypeName] AS [Level6TypeName],
[OrgHierarchy].[ContractName] AS [ContractName],
[OrgHierarchy].[MemberID] AS [MemberID],
[OrgHierarchy].[MemberFullName] AS [MemberFullName],
[OrgHierarchy].[Member_Provider_Relationship_Effective_Date] AS [Member_Provider_Relationship_Effective_Date],
[OrgHierarchy].[Member_Provider_Relationship_Termination_Date] AS [Member_Provider_Relationship_Termination_Date],
[OrgHierarchy].[BENE_HIC_NUM] AS [BENE_HIC_NUM],
[OrgHierarchy].[Patient_Status] AS [Patient_Status],
[OrgHierarchy].[ProviderId] AS [ProviderId],
[OrgHierarchy].[ProviderName] AS [ProviderName],
[OrgHierarchy].[ProviderNPI] AS [ProviderNPI],
[OrgHierarchy].[BirthDate] AS [BirthDate],
[OrgHierarchy].[GenderId] AS [GenderId],
[OrgHierarchy].[County] AS [County],
[OrgHierarchy].[LastUpdatedTimestamp] AS [LastUpdatedTimestamp],
[OrgHierarchy].[LbPatientId] AS [LbPatientId],
[OrgHierarchy].[PatientId1TypeName] AS [PatientId1TypeName],
[OrgHierarchy].[PatientId1] AS [PatientId1],
[OrgHierarchy].[PatientId2TypeName] AS [PatientId2TypeName],
[OrgHierarchy].[PatientId2] AS [PatientId2],
[OrgHierarchy].[PatientId3TypeName] AS [PatientId3TypeName],
[OrgHierarchy].[PatientId3] AS [PatientId3],
[OrgHierarchy].[PatientId4TypeName] AS [PatientId4TypeName],
[OrgHierarchy].[PatientId4] AS [PatientId4],
[CCLF_WELLNESS_AGING].[id] AS [id1],
[CCLF_WELLNESS_AGING].[BENE_HIC_NUM] AS [BENE_HIC_NUM (CCLF_WELLNESS_AGING)],
[CCLF_WELLNESS_AGING].[DAYS_SINCE_VISIT] AS [DAYS_SINCE_VISIT],
[CCLF_WELLNESS_AGING].[LAST_DOS] AS [LAST_DOS],
[CCLF_WELLNESS_AGING].[LAST_WELLNESS_DOS] AS [LAST_WELLNESS_DOS],
[CCLF_WELLNESS_AGING].[DAYS_SINCE_WELLNESS] AS [DAYS_SINCE_WELLNESS],
[CCLF_WELLNESS_AGING].[LbPatientId] AS [LbPatientId (CCLF_WELLNESS_AGING)],
[Patient_Summary_Pivot].[LbPatientId] AS [LbPatientId (Patient_Summary_Pivot)],
[Patient_Summary_Pivot].[2015 Cost] AS [2015 Cost],
[Patient_Summary_Pivot].[Diabetes] AS [Diabetes],
[Patient_Summary_Pivot].[DME Cost] AS [DME Cost],
[Patient_Summary_Pivot].[ER Visits] AS [ER Visits],
[Patient_Summary_Pivot].[Hypertension] AS [Hypertension],
[Patient_Summary_Pivot].[Mental Health] AS [Mental Health],
[Patient_Summary_Pivot].[Oncology] AS [Oncology],
[Patient_Summary_Pivot].[Resource_Utilization] AS [Resource_Utilization],
[Patient_Summary_Pivot].[2013 Cost] AS [2013 Cost],
[Patient_Summary_Pivot].[2014 Cost] AS [2014 Cost],
[Patient_Summary_Pivot].[Asthma] AS [Asthma],
[Patient_Summary_Pivot].[ATI] AS [ATI],
[Patient_Summary_Pivot].[Chronic Pain] AS [Chronic Pain],
[Patient_Summary_Pivot].[COPD] AS [COPD],
[Patient_Summary_Pivot].[DualStatus] AS [DualStatus],
[Patient_Summary_Pivot].[IP Re-Admits] AS [IP Re-Admits],
[Patient_Summary_Pivot].[MedicareStatus] AS [MedicareStatus],
[Patient_Summary_Pivot].[Part-A Cost] AS [Part-A Cost],
[Patient_Summary_Pivot].[Part-B Cost] AS [Part-B Cost],
[Patient_Summary_Pivot].[Risk Score] AS [Risk Score],
[Patient_Summary_Pivot].[12 Mo Cost] AS [12 Mo Cost],
[Patient_Summary_Pivot].[2012 Cost] AS [2012 Cost],
[Patient_Summary_Pivot].[IP Admits] AS [IP Admits],
[Patient_Summary_Pivot].[2016 Cost] AS [2016 Cost],
[Patient_Summary_Pivot].[CHF] AS [CHF],
[Patient_Summary_Pivot].[CKD] AS [CKD],
[Patient_Summary_Pivot].[ESRD] AS [ESRD],
[Patient_Summary_Pivot].[Measure Opps] AS [Measure Opps],
[Patient_Summary_Pivot].[Pharmacy Cost] AS [Pharmacy Cost],
[JhacgCondition].[JhacgConditionId] AS [JhacgConditionId],
[JhacgCondition].[LbPatientId] AS [LbPatientId (JhacgCondition)],
[JhacgCondition].[Condition] AS [Condition],
[JhacgCondition].[ConditionAbbreviation] AS [ConditionAbbreviation]
FROM [dbo].[JhacgPatientDetail] [JhacgPatientDetail]
INNER JOIN [dbo].[OrgHierarchy] [OrgHierarchy] ON ([JhacgPatientDetail].[patient_id] = [OrgHierarchy].[LbPatientId])
INNER JOIN [dbo].[CCLF_WELLNESS_AGING] [CCLF_WELLNESS_AGING] ON ([OrgHierarchy].[LbPatientId] = [CCLF_WELLNESS_AGING].[LbPatientId])
INNER JOIN [dbo].[Patient_Summary_Pivot] [Patient_Summary_Pivot] ON ([OrgHierarchy].[LbPatientId] = [Patient_Summary_Pivot].[LbPatientId])
INNER JOIN [dbo].[JhacgCondition] [JhacgCondition] ON ([OrgHierarchy].[LbPatientId] = [JhacgCondition].[LbPatientId])
GO
